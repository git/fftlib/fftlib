/* 
  xs -f mktag.js {-nrecurse} {-replace} {COMP_ROOT=....} [LABEL=....] comp1,comp2,comp3....
*/

// function mktag(arguments)
function getargs(arguments)
{
var optargs={};
var compargs={};
for each( var arg in arguments )
  {
    if (arg.indexOf("-") == 0)
     {
       if (arg == "-replace")
        {
            replace=" -replace";
        }
       if (arg == "-nrecurse")
        {
            recurse="";
        }
     }
     if (/=/.test(arg))
       {
         optargs=arg.split(/=/);
         print (optargs[0]+" = "+optargs[1]); 
         if (optargs[0]=="COMP_ROOT")
          {
             comproot=optargs[1];
			 var comppath = xdc.findFile("ti/mas/swtools")
			 print("comppath : "+comppath);
             if (comppath)
               {  
                   comppath=comppath.toString().split("\\ti");
				   comproot=comppath[0];
                   continue;
               }
             else
               {
                  print("\n\tERROR: Specified Component root \"" + comproot + "\" doesnot exist. Exiting....\n"); 
                  failure=1;
               }
          }
         if (optargs[0]=="TAG_PREFIX")
          {
             if (/^\s*$/.test(optargs[1]))
               {
                  failure=2;
               }
             else
               {
                   label_prefix=optargs[1].toUpperCase();
                   if (!((label_prefix=="DEV") || (label_prefix=="FCHK")))
                      {
                        failure=4;
                        break;
                      }
               }
          }
         if (optargs[0]=="COMPONENT")
          {
             if (/^\s*$/.test(optargs[1]))
               {
                  failure=3;
               }
             comps=optargs[1];

// The compargs array was created with the idea of labeling multiple components in mind. But we do not have to change the array to make it a variable because the logic still works now with only 1 component being labeled.

             compargs[0]=optargs[1];
             var compname=compargs[0];
             compargs[0]=compname.toString().replace(/\./g, "\\");
             complabelpart=compname.toString().replace(/\./g, "_");
             complabelpart=complabelpart.toUpperCase();
             for (var comp in compargs)
               {
                  var fullcomppath=comproot+"\\"+compargs[comp];
                  if (fileExists(fullcomppath))
                    {
// The following paramarr array is created because the function "formatArgument(rawArg, rule)" in the Ccaseformatargument.js file expects the "rawArg" argument to be an array. If we give a string input, the rawArg.length gives the length of the string (Generally more than 2) and the function fails.
                      var paramarr=[];
                      paramarr[0]=compname;
                      version=readVersion(paramarr);
                      if(checkQuality(paramarr)==false && label_prefix=="DEV")
                      {
                        java.lang.System.exit(1);
                      }
                      if( version == "?.?.?.?" || version == undefined)
                      {
                        print("Error: Version of the package could not be found. Please make sure the package is build before running the script again \n");
                        java.lang.System.exit(1);
                                              
                      }
                      version=version.toString().replace(/\s/g, "");
                      version=version.toString().replace(/\,/g, ".");
                      version=version.toString().replace(/\./g, "_");
                      if (curdir!=comproot)
                        {
                          fullpathcomps[fullpathcomps.length]=fullcomppath;
                        } 
                    }
                  else
                    {
                       print ("\n\tERROR:: COMPONENT "+compargs[comp]+" DOES NOT EXIST UNDER "+comproot+"\n");
                       failure=3;
                    }
               }
               if (curdir!=comproot)
                  {
                     comps=fullpathcomps.join(" ");
                  }
            } 
       }
  } // End main for loop
  
  if ((label_prefix=="") || (complabelpart=="") || (version==""))
    {
       failure=3;
    }
  else
    {
      if(label_prefix != "FCHK")
      {
        label=label_prefix+"_"+complabelpart+"_"+version;
      }
      else
      {
        label=label_prefix+"_"+complabelpart+"_"+version+"_"+year+month+day+myChar;
      }
    }
  
  if (comps=="")
    {
      print ("\n\nERROR:: Valid component not specified...\n");
      failure=3;
    }
  if (label=="")
    {
      print ("\n\nERROR:: Valid TAG not specified...\n");
      failure=2;
    }
  else if (failure==0)
    {
      mklbtype(label,comproot);
    }
  if (comproot=="\.")
    {
       comproot=curdir;
    }
}

function fileExists(filename)
 {
   f = new java.io.File(filename);
   return f.exists(); 
 }

function currentdir()
 {
     dir1 = new java.io.File (".");
     try 
       {
          return(dir1.getCanonicalPath());
       }
     catch(err) 
       {
          print("\n\tERROR IN currentdir() FUNCTION: "+err.description+"\n");
       }
  }
    
function usage()
 {
     print("\n\tUSAGE:\n");
     print("\t\txs -f mktag.js {-nrecurse} {-replace} {COMP_ROOT=<Full path to the root of the component tree>} [LABEL=<Label name>] [COMPONENT=comp]\n");
  }

function mklbtype(labelname,comproot)
{
   var stat={}; 
   var patharr=comproot.split(/\\/);
   vob="\\"+patharr[1];
   var command="git.exe tag "+label;
   print("Creating tag  \""+labelname+"\" in the git tree\n");
   xdc.exec(command,null,stat);
   var pat="fatal: tag '"+labelname+"' already exists";
   if ((stat.output).match(pat) && (replace=="") && (label_prefix == "DEV"))
      {
         failure=5;
      }
   if(label_prefix == "FCHK")  
   {
     while((stat.output).match(pat))
     {
       print("Tag \""+label+"\"already exists in git tree\n");
       myChar=incrementLetter(myChar);
       if(myChar == "A")
       {
         failure=5;
         break;
       }
       else
       {
         label=label_prefix+"_"+complabelpart+"_"+version+"_"+year+month+day+myChar;  
         command="git.exe tag "+label;
         var pat="fatal: tag '"+label+"' already exists";
         print("Creating label type \""+label+"\" in git tree\n");
         xdc.exec(command,null,stat);
         print(stat.output);
       }
       
     }
   }
}

function makeArray() {
     for (i = 0; i<makeArray.arguments.length; i++)
          this[i + 1] = makeArray.arguments[i];
}

function findcheckouts(comproot) 
  {
     var lscocmd="git.exe ls-files "+comproot; 
     print("Checking for modified files under "+comproot+"\n");
     xdc.om.$$bind('$$genflg', 1);
     xdc.exec(lscocmd,null,status);
     if (/Not a git repository/.test(status.output))
       {
          return(-1);
       }
     if (status.output!="")
       {
          print("The following files are checkedout in the current view under "+comproot+":\n\n"+status.output);
          return(1);
       }
     return(0);
  }
  
//create a string containing all letters
var alphaChars = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";

//initialize a test character to 'A'
var myChar = "A";

//this function accepts a character, increments it
//by one, and returns it
function incrementLetter(letterToIncrement){

 //find where the letter is at in the alphaChars string
 var indexOfLetter = alphaChars.search(letterToIncrement);

 //if it's not the last letter, then return the next
 //letter in the string
 if (indexOfLetter+1 < alphaChars.length) {
   return(alphaChars.charAt(indexOfLetter+1))
 }

 //otherwise return the input letter
 else{
   return("A");
 }
}
  

/* MAIN ROUTINE FROM HERE */
/* copy the environment array from the current environment */
    var env   = java.lang.System.getenv();
    var keys  = env.keySet().toArray();
    var key;
    var stat={};
    var env_j=[];
    for (var i = 0; i < keys.length; i++) {
         key = keys[i];
      if (env.get(key) == "") {
             continue;
         }
         env_j = env_j.concat([key + '=' + env.get(key)]);
     }
   var attrs = {
        envs: env_j
   }

var args={};
var params=[];
var replace="";
var recurse=" -recurse";
var comproot=".";
var label="";
var complabelpart="";
var comps="";
var fullpathcomps=[];
var failure=0;
var curdir=currentdir();
var status={};
var out;
var day;
var year;
var month;
var hour;
var minutes;
var versionName;
var date=new java.util.Date();
var months = new makeArray('January','February','March', 'April','May','June','July','August','September', 'October','November','December');
var vob;
var warningBuffer = new Array();
year=date.getYear();
year+=1900;
month=date.getMonth();
month+=1;
if(month < 10)
{
 month = "0"+month;
}
hour=date.getHours();
day=date.getDate();
if(day < 10)
{
 day = "0"+day;
}
minutes=date.getMinutes();
var version;
var outputFileName;
utils.importFile("src/CcasereadVersion.js");
if(arguments.length == 0)
{
  print("USAGE:");
  print("xs -f mktag.js  COMP_ROOT=\\dsps_gtmas COMPONENT=ti.mas.xxx TAG_PREFIX=DEV/FCHK\n");
  java.lang.System.exit(1);
}
for each( var arg in arguments )
  {
     params[params.length]=arg;
  }

print ("Current date is " + java.util.Date());
// mktag(params);
getargs(params);

if (failure==0)
   {
      outputFileName="c:\\temp\\"+label+"_"+months[month]+"_"+day+"_"+year+"_"+hour+"_"+minutes+".txt";
      var tmppath="\\";
      var count=0;
      var paths=[];
      command="git.exe tag -a -m \"           \" " +label;
      xdc.exec(command,attrs,stat);
      if( (stat.output).match("Error:") )
      {
          
             print(stat.output);
             java.lang.System.exit(1);
          
      }    
   }
else if (failure==3)
   {
      print("\n\tERROR: Component is empty. Exiting....\n"); 
   }
else if (failure==4)
   {
     print("ERROR: Illegal Label prefix used.\nPlease use only \"DEV\" or \"FCHK\" label prefixes.");
   }
else if (failure==5)
   {
     print("TAG \""+label+"\" already exists \n");
     print("Please use the -replace option to force taging\n");
   }
else if (failure==6)
   {
     print("Please commit the elements and restart the tagging\n");
   }
else
   {
      usage();
   }
if (fileExists(outputFileName))
print("Log of the labeling is at "+outputFileName+"\n");
