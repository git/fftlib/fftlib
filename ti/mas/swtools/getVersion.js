

 utils.importFile("src/CcaseformatArgument.js");
 utils.importFile("src/CcasereadXdcXml.js");
 utils.importFile("src/CcasefindFilePlus.js");
 var verbose = 0;	//used to turn on/off verbose mode of operation
 var verboseCtr = 0; //used to label line# of verbose output
 var warningBuffer = new Array();

 function getVersion(arguments)
 {
 
 /* verify and format the package name*/
 formaArgs = formatArgument(arguments, "ccase");

 formaArgs = formaArgs.toString().replace(/\./g, "\/");

 /*Read package.xdc.xml of current package of interest*/
 var xmlTreeOfPkg=readXdcXml(formaArgs, "ofPackage"); 
 
 if(xmlTreeOfPkg==undefined)
 {
    print("package.xdc.xml file of "+formaArgs+" (package of interest) could not be loaded.");
	print("Please make sure that "+formaArgs+" (package of interest) is built and its package.xdc.xml is accessible.");
 }
 else
 {
  var pkgName = xmlTreeOfPkg.@name;
  if(arguments != pkgName)
  { 
    print("The package name of XML and source are not same");    
	return -1;
  }
  var Version = xmlTreeOfPkg.@release;
  if(Version=="")
  {
    Version = xmlTreeOfPkg.@version;
  }
  return Version;
 }
}