#  ======== xdcpaths.mk ========
#  definition of XDC paths and commands
#
#  This makefile constructs the "search path" for the XDC tools where it finds
#  numerous components and packages needed to build Codec Engine examples and
#  programs.
#
#  USER NOTE:
#  1) you must specify various <component>_INSTALL_DIRs directores below to
#     reflect your installation
#  2) you must specify compiler path and name in CGTOOLS_* variables below

# (Mandatory) Specify where various components are installed.
# What you need depends on what device(s) you're building for, what type(s) of
# programs you are building for, and whether your Codec Engine distribution
# is a "big" one that contains all the minor components in its "cetools"
# directory. The legend:
# XDC     - XDC tools (Arm and DSP)
# XDAIS   - XDAIS header files (Arm and DSP)
#
# you can omit directory specifications for the components you think you don't
# need (will be warned if you do, based on your DEVICES + PROGRAMS selection
# above).

# XDC_INSTALL_DIR       := _your_XDCTOOLS_installation_directory/xdctools_3_20_05_76
export XDC_INSTALL_DIR       := /opt/ti//xdctools_3_25_05_94

export XDAIS_INSTALL_DIR := /opt/ti/xdais_7_24_00_04

export DSPLIB_INSTALL_DIR := /opt/ti/dsplib_c66x_3_2_0_1

export OPENMP_INSTALL_DIR := /opt/ti/openmp_dsp_2_01_14_00

export OPENEM_INSTALL_DIR := /opt/ti/openem_1_10_0_0

export PDK_INSTALL_DIR := /opt/ti/pdk_keystone2_3_00_04_18

export FC_INSTALL_DIR := /opt/ti/framework_components_3_30_00_06

export BIOS_INSTALL_DIR := /opt/ti/bios_6_37_00_20

export EDMA_INSTALL_DIR := /opt/ti/edma3_lld_02_11_11_15

export IPC_INSTALL_DIR := /opt/ti/ipc_3_00_04_29

export 

export C66CODEGENTOOL := /opt/ti/C6000CGT7.4.2
export C674CODEGENTOOL := /opt/ti/C6000CGT7.4.2
export C64CODEGENTOOL := /opt/ti/C6000CGT7.4.2

export TI_DOXYGEN_TEMPLATES := /opt/ti/doxygen/TI_Templates/10-01-2007

# export DOXYGEN_INSTALL_DIR := /apps/free/doxygen/current/bin

export XDCBUILDCFG :=   /home/a0875221/project/fftlib/ti/mas/swtools/config.bld

export FFTLIB_INSTALL_DIR := /home/a0875221/project/fftlib

# XDC_PATH is complete. Any other components you could add as
# XDC_PATH := <your component>/packages;$(XDC_PATH)
XDC_PATH := $(XDAIS_INSTALL_DIR)/packages
XDC_PATH := $(XDC_INSTALL_DIR)/packages;$(XDC_PATH)
XDC_PATH := $(DOXYGEN_INSTALL_DIR);$(XDC_PATH)
XDC_PATH := $(FFTLIB_INSTALL_DIR);$(XDC_PATH)
XDC_PATH := $(FC_INSTALL_DIR)/packages;$(XDC_PATH)
XDC_PATH := $(OPENMP_INSTALL_DIR)/packages;$(XDC_PATH)
XDC_PATH := $(OPENEM_INSTALL_DIR)/packages;$(XDC_PATH)
XDC_PATH := $(PDK_INSTALL_DIR)/packages;$(XDC_PATH)
XDC_PATH := $(BIOS_INSTALL_DIR)/packages;$(XDC_PATH)
XDC_PATH := $(EDMA_INSTALL_DIR)/packages;$(XDC_PATH)
XDC_PATH := $(IPC_INSTALL_DIR)/packages;$(XDC_PATH)
XDC_PATH := $(XDC_PATH);$(DSPLIB_INSTALL_DIR)/packages

export PATH := $(PATH):$(XDC_INSTALL_DIR):/apps/free/installJammer/current:/apps/free/php/5.3.3b/bin

