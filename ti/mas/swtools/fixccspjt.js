
function usage()
 {
     print("\n\tUSAGE:\n");
     print("\t\txs -f fixccspjt.js input\n");
     java.lang.System.exit(1);
 }



function fixcdt(cdt)
{
     var srcFile=String(cdt);
     var dot = srcFile.lastIndexOf(".");
     var extension = srcFile.substr(dot,srcFile.length);
     /* Check for the right input */
     if (extension == ".cdtbuild" || extension == ".cproject"  )
     {
       var fileModule = xdc.module('xdc.services.io.File');
       var openedFile= fileModule.open(srcFile,"r");
       if(openedFile == null)
       {
          print("Error: Cannot open " +srcFile+" file ");
          usage();
       }
       /* Temporary file to write  */
       var tmpfile = fileModule.open("temp.xml","w");
       var readLine;
       while((readLine=openedFile.readLine()) != null)
        {
            
            /* Fix the include PATH for XDAIS and XDC */
            if(readLine.match("{XDAIS_CG_ROOT}"))
            {
              if(readLine.match(/\${XDAIS_CG_ROOT}/) == null)
              {
                 readLine='<listOptionValue builtIn="false" value="&quot;${XDAIS_CG_ROOT}/packages&quot;"/>';
              }
            }
            if(readLine.match("{XDC_CG_ROOT}"))
            {
              if(readLine.match(/\${XDC_CG_ROOT}/) == null)
              {
                readLine='<listOptionValue builtIn="false" value="&quot;${XDC_CG_ROOT}/packages&quot;"/>';
              }
            }
            
            tmpfile.writeLine(readLine);
        }  
       openedFile.close();
       tmpfile.close();

       /* Replace source file with Temp file */
       /* Copy Module */
       var copy = xdc.loadCapsule('ti/mas/swtools/Copy.xs');
       copy.Move("temp.xml",cdt);
     }
     else
     {
       print(cdt +"is not a valid ccs project file"); 
       java.lang.System.exit(1);
     }
 

}

/* Main function starts here */

if(arguments.length < 1)
{
  usage();
}

fixcdt(arguments[0]);
