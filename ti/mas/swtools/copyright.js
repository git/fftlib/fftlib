
function usage()
 {
     print("\n\tUSAGE:\n");
     print("\t\txs -f copyright.js [-recurse] srcDir copyright.txt\n");
     java.lang.System.exit(1);
 }

 function parseArgs(arguments)
 {

  if(arguments[0] == "-recurse")
  {
    recurse=true;
    inputDir = arguments[1];
    copyright = arguments[2];

  }
  else
  {
    inputDir = arguments[0];
    copyright = arguments[1];
    print(inputDir);
    print(copyright);
  }
 }

function searchFiles(dirs,recurse)
{
  print("dirs :"+dirs);
  var file = new java.io.File(dirs);
  if(!file.isDirectory())
  {
    print("Error: "+dirs+" is not a directory");
    usage();
  }

  var list = file.list();
  var i, j;
  for (i = 0; i < list.length; i++)
  {
     if(recurse == true && java.io.File(dirs+"/"+list[i]).isDirectory())
     {
        searchFiles(dirs+"/"+list[i],true);
     }
     var srcFile=String(list[i]);
     var dot = srcFile.lastIndexOf(".");
     var extension = srcFile.substr(dot,srcFile.length);      
     if ((extension == ".c" || extension == ".h")  && dirs.match("examples"))
     {
        srcArray.push(dirs+"/"+list[i]);
        print(dirs+"/"+list[i]);
     }
      
  }
}

/* Main Function starts here */

var env   = java.lang.System.getenv();
    var keys  = env.keySet().toArray();
    var key;
    var stat={};
    var env_j=[];
    for (var i = 0; i < keys.length; i++) {
         key = keys[i];
      if (env.get(key) == "") {
             continue;
         }
         env_j = env_j.concat([key + '=' + env.get(key)]);
     }
   var attrs = {
        envs: env_j
   }

var inputDir;
var copyright;
var readLine;
var copyrightString = "";
var srcArray = new Array();
var recurse=false;

/* Check for valid number of Arguments */
if(arguments.length < 2)
{
   usage();
}


/* parse Arguments */
parseArgs(arguments);

/* Check for valid Arguments */
if(inputDir == undefined || copyright==undefined)
{
   java.lang.System.exit(1);
}

/*Open copyright file and copy the string to string bugger */
var fileModule = xdc.module('xdc.services.io.File');
/* Copy Module */
var copy = xdc.loadCapsule('ti/mas/swtools/Copy.xs');


print("Opening " + copyright + " file ...");
    
var openedFile= fileModule.open(copyright,"r");
if(openedFile == null)
{
  print("Error: Cannot open " +copyright+" file containing copyright text");
  usage();
}
print("Retreiving copyright information from " + copyright + " file ...");

while((readLine=openedFile.readLine()) != null)
{
  i=0; 
  /* Check for blank Line */
  if(readLine.length==0)
  {
    var space="";
   for(var k=0; k < 70;k++)
	  {
	    space+=" ";
	  }
	 if(copyrightString=="")
    {	
	  copyrightString = "**|  "+space+"  |**";
	}
	else
	{
	  copyrightString += "\n**|  "+space+"  |**";
	}
  }
  while(i < readLine.length)
  {
    var maxLength=70;
	var space="";
	if(readLine.length -i < 70)
	{
	  maxLength = readLine.length; 
	  
	  for(var k=0; k < (70 - (readLine.length-i));k++)
	  {
	    space+=" ";
	  }
	}
	else
	{
	  var fmtLine=readLine.substr(i,maxLength);
	  maxLength = fmtLine.lastIndexOf(" ")+1;
	  for(var k=0; k < (70 - maxLength);k++)
	  {
	    space+=" ";
	  }
	}

	if(copyrightString=="")
    {	
	  copyrightString = "**|  "+readLine.substr(i,maxLength)+space+"  |**";
	}
	else
	{
	  copyrightString += "\n**|  "+readLine.substr(i,maxLength)+space+"  |**";
	}
	i+=maxLength;
  }
  
  
}

copyrightString+= "\n**+--------------------------------------------------------------------------+**";
openedFile.close();

/* Search for C and Header files */
searchFiles(inputDir,recurse);

/* Replace the copyright information */
for each(var file in srcArray)
{
  print("Updating copyright for "+file + " file ...");
  
  var openedFile = fileModule.open(file,"r");
  var tmpfile = fileModule.open("temp.txt","w");
  var replace = false;
  while((readLine=openedFile.readLine()) != null)
  {
      if(readLine.toLowerCase().match("copyright")  && replace == false)
      {
        readLine=openedFile.readLine();
        while(!readLine.match(/\*\//))
        {
           readLine=openedFile.readLine();
        }
        replace = true;
        tmpfile.writeLine(copyrightString);
        tmpfile.writeLine(readLine);
      }
      else
        tmpfile.writeLine(readLine);
  }

  if(replace == false)
  {
     openedFile.close();
     tmpfile.close();
     openedFile = fileModule.open(file,"r");
     tmpfile = fileModule.open("temp.txt","w");
 
    tmpfile.writeLine("/**");
    tmpfile.writeLine(copyrightString);
    tmpfile.writeLine("*/");
    while((readLine=openedFile.readLine()) != null)
    {
      tmpfile.writeLine(readLine);
    }
  }

  openedFile.close();
  tmpfile.close();

  /* Replace source file with Temp file */

  copy.Move("temp.txt",file);
 
} 









