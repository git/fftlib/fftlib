/*
 * This is a little header file which doxygen parses to generate the main
 * documentation page
 */ 
  
/**
 *  @mainpage 
 *  <p align="center"> 
 *  <a href="#Introduction">Introduction</a>                            <br>
 *  <a href="#Documentation">Documentation</a>                          <br>
 *  <a href="#Whats_New">What's New</a>                                 <br>
 *  <a href="#Compatibility">Upgrade and Compatibility Information</a>  <br>
 *  <a href="#Host_Support">Host Support</a>                            <br>
 *  <a href="#Device_Support">Device Support</a>                        <br>
 *  <a href="#Validation">Validation Information</a>                    <br>
 *  <a href="#Known_Issues">Known Issues</a>                            <br>
 *  <a href="#Version">Version Information</a>                          <br>
 *  <a href="#Support">Technical Support</a>                            <br>
 *  </p>
 *
 *  <!--**************************************************************************************************-->
 *  <!--**************************************************************************************************-->
 *  <!--***********************************Introduction***************************************************-->
 *  <!--**************************************************************************************************-->
 *  <!--**************************************************************************************************-->
 *
 *
 *  <hr> 
 *  <h2><a name="General_Info">Introduction</a></h2>
 *  This component contains tools that are shared by other software components during the configuration and build procedures. 
 *  For example, it contains: 
 *  - Perl script support for cygwinxdc.bat files. 
 *  - Version.xdt template file that is to be used by all packages to implement the Version interface.
 *  - JavaScript functions that automate creation of ClearCase related files.
 *  - <b>CommonBld</b> module containing common build utilities that can be shared across the packages to avoid duplication.
 *
 *  <a href="#XDC_TOP">back to top</a>
 *  <!--**************************************************************************************************-->
 *  <!--**************************************************************************************************-->
 *  <!--***********************************Documentation**************************************************-->
 *  <!--**************************************************************************************************-->
 *  <!--**************************************************************************************************-->
 *
 *  <hr>
 *
 *  <h2><a name="Documentation">Documentation</a></h2>
 *  <p>The following documentation is available: </p> 
 *  - N/A
 *
 *  <p>Release notes from previous releases are also available in the
 *  <a href="docs/relnotes_archive">relnotes_archive</a>
 *  directory.</p>
 *
 *  <a href="#XDC_TOP">back to top</a> 
 *
 *  <!--**************************************************************************************************-->
 *  <!--**************************************************************************************************-->
 *  <!--*************************************What's New***************************************************-->
 *  <!--**************************************************************************************************-->
 *  <!--**************************************************************************************************-->
 *  <hr>
 *
 *
 *  <h2><a name="Whats_New">What's New</a></h2>
 *  <p> The following new features were added:  
 *  - GNU Makefile Support
 *  - Bundling Support.
 *  - Packaging Standard 2.0 Support.  
 *  - Bug Fixes.
 *  - C66x Support
 *  - ELF Support for C64x+ and C674x.
 *  - ARM Cortex A8 Support.
 *  - CCSV4 Project Support.
 *  - Eclipse Plugin Support.
 *  
 *  </p>
 *
 *
 *  <a href="#XDC_TOP">back to top</a>
 *
 *  <!--**************************************************************************************************-->
 *  <!--**************************************************************************************************-->
 *  <!--************************Upgrade and Compatibility Information*************************************-->
 *  <!--**************************************************************************************************-->
 *  <!--**************************************************************************************************-->
 *  <hr>
 *
 *  <h2><a name="Compatibility">Upgrade and
 *  Compatibility Information</a></h2>
 *
 *  This <b>ti.mas.swtools</b> version is source compatible with the last released swtools version.  
 *
 *
 *  Please note that the package compatibility keys are independent of XDC product release numbers. 
 * 
 *  Package compatibility keys are intended to: 
 * 
 *   -# Enable tooling to identify incompatibilities between components, and 
 *   -# Convey a level of compatibility between different releases to set end user expectations. 
 * 
 * Package compatibility keys are composed of 4 comma-delimited numbers - M, S, R, P - where: 
 *
 * - <b>M = Major</b> - A difference in M indicates a break in compatibility. The package consumer is required to re-write its source code in order to use the package.
 * - <b>S = Source</b> - A difference in S indicates source compatibility. The package consumerís source code doesn't require change, but does require a recompile. 
 * - <b>R = Radix</b> - A difference in R indicates an introduction of new features, but compatibility with previous interfaces is not broken. If libraries are provided by the package, an application must re-link with the new libraries, but is not required to recompile its source.
 * - <b>P = Patch</b> - A difference in P indicates that only bugs have been fixed in the latest package and no new features have been introduced. If libraries are provided by the package, an application must re-link with the new libraries, but is not required to recompile its source. 
 *
 *  <a href="#XDC_TOP">back to top</a>
 *  <!--**************************************************************************************************-->
 *  <!--**************************************************************************************************-->
 *  <!--****************************************Host Support**********************************************-->
 *  <!--**************************************************************************************************-->
 *  <!--**************************************************************************************************-->

 *  <hr>
 *
 *  <h2><a name="Host_Support">Host Support</a></h2>
 *
 *  The current XDC toolset can be used to build components on the following hosts:
 *   - Windows XP 
 *   - Linux
 *
 *  <a href="#XDC_TOP">back to top</a>
 *  <!--**************************************************************************************************-->
 *  <!--**************************************************************************************************-->
 *  <!--**********************************Dependencies******************************************-->
 *  <!--**************************************************************************************************-->
 *  <!--**************************************************************************************************-->

 *  <hr>
 *
 *  <h2><a name="Dependencies">Dependencies</a></h2>
 *
 *  This release requires the following other software components and tools versions to successfully function
 *  - XDC Tools version 3.20.05.76
 *
 *  <a href="#XDC_TOP">back to top</a>
 *  <!--**************************************************************************************************-->
 *  <!--**************************************************************************************************-->
 *  <!--*****************************************Device Support*******************************************-->
 *  <!--**************************************************************************************************-->
 *  <!--**************************************************************************************************-->
 *  <hr>
 *
 *  <h2><a name="Device_Support">Device Support</a></h2>
 *
 *  This release supports the following device families: 
 *  - C64P, C64, C674, C66 
 *  - C55
 *  - C54 
 *  - ARM
 *  - WIN32
 *
 *  <a href="#XDC_TOP">back to top</a>
 *
 *  <!--**************************************************************************************************-->
 *  <!--**************************************************************************************************-->
 *  <!--**********************************Validation Information******************************************-->
 *  <!--**************************************************************************************************-->
 *  <!--**************************************************************************************************-->
 *

 *  <hr>
 *
 *  <h2><a name="Validation">Validation Information</a></h2>
 *
 *  This release was built and validated against using the following components: 
 *   - ti.mas.types
 *   - ti.mas.fract
 *   - ti.mas.util
 *
 *
 *  <a href="#XDC_TOP">back to top</a>
 *  <!--**************************************************************************************************-->
 *  <!--**************************************************************************************************-->
 *  <!--***********************************Known Issues***************************************************-->
 *  <!--**************************************************************************************************-->
 *  <!--**************************************************************************************************-->

 *  <hr>
 *
 *  <h2><a name="Known_Issues">Known Issues</a></h2>
 *  - Ccase.js script to generate configspec.clearcase is unable to parse test packages if it is not directly under test folder.
 *
 *  - When creating the mCSPEC file mcspecGen.js will not be able to update the existing maintenance branch information properly. It would always assume a new branch is being created from the current version of the package and would use that new label for the branch origin. One has to manually revert back to the original label for the branch origin in order to prevent problems. The reason for this problem is that the mcspecGen.js was initially written to help in creation of new maintenance branches. 
 *
 *  - When creating the iCPSEC file icspecGen.js will look for the VOB name(s) within the label(s) specified for accessing the parent folders of the integrating components. The script has a bug that results in the iCSPEC file which does not remove the VOB name(s) from the label(s) and generates the wrong folder path(s) to be associated with the label(s). Solution is to specify the label(s) the way script wants it and subsequently edit the generated iCSPEC.txt file to substitute away the VOB name(s) from the generated label(s) as well as to fix the wrong path names. An example of a broken path name is /dsps_gtmas/dsps/gtmas/ti/mas. The correct path is /dsps_gtmas/ti/mas. The spurious dsps/gtmas section was inserted by mistake where the script just expanded DEV_DSPS_GTMAS_TI_MAS_<version> label without removing the (capitalized) VOB name. 
 *
 *  - When creating the iCPSEC/mCSPEC file icspecGen.js and mcspecGen.js scripts will not find ti/mas/iface/ or similar folders that group multiple components. The script will assume ti/mas is the only folder grouping other components. In order to get other component grouping folders to be properly referenced you will have to paste the ti/mas folder line and substitute the MAS folder name for the corresponding grouping folder, e.g. /dsps_gtmas/ti/mas/iface. 
 *
 *  <a href="#XDC_TOP">back to top</a>
 *
 *  <hr>
 *
 *  <!--**************************************************************************************************-->
 *  <!--**************************************************************************************************-->
 *  <!--**************************************** Versioning **********************************************-->
 *  <!--**************************************************************************************************-->
 *  <!--**************************************************************************************************-->

 *  <h2><a name="Version">Version Information</a></h2>
 *
 *  This product's version follows a version format, <b>M.m.p.b</b>,
 *  where <b>M</b> is a single digit Major number, <b>m</b> is single digit minor number,
 *  <b>p</b> is a single digit patch number and <b>b</b> is an unrestricted set of digits used as an incrementing build counter. 
 
 *  <p>Please note that version numbers and compatibility keys are
 *  NOT the same. For an explanation of compatibility keys, please refer to
 *  the '<a href="#Compatibility">Upgrade and Compatibility Information</a>' section.
 *
 *  <a href="#XDC_TOP">back to top</a>
 *  <!--**************************************************************************************************-->
 *  <!--**************************************************************************************************-->
 *  <!--*********************************** Technical Support ********************************************-->
 *  <!--**************************************************************************************************-->
 *  <!--**************************************************************************************************-->
 *  <hr>
 *  <h2><a name="Support">Technical Support</a></h2>
 *
 *  Contact local TI Field Application Engineer for technical support.
 *
 *  <a href="#XDC_TOP">back to top</a>
 *
 *
 *
 */
