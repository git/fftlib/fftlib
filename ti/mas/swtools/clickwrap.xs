var zip;
var licensefile;
var licensetext;

function getargs(arguments)
{
var optargs={};
var compargs={};
for each( var arg in arguments )
{
  if (/=/.test(arg))
       {
         optargs=arg.split(/=/);
         print (optargs[0]+" = "+optargs[1]); 
         if (optargs[0]=="zip")
          {
             zip=optargs[1];
          }			 	      
		 if (optargs[0]=="licensefile")
          {
             licensefile=optargs[1];
	      } 
		 if (optargs[0]=="licensetext")
          {
             licensetext=optargs[1];
	      }  
		}
}
}

/*Main syntax */
getargs(arguments);
if(arguments.length < 3 || zip=="" || licensefile=="" || licensetext=="")
{
  print("Syntax:");
  print("xs -f clickwrap.xs zip=vlib_*.zip licensefile=agreement.pdf licensetext=license.txt")
  java.lang.System.exit(1);
}

var dot = zip.lastIndexOf(".");
var extension = zip.substr(dot,zip.length);     
if(extension!=".zip")
{
  print("Incorrect zip file");
  print("Syntax:");
  print("xs -f clickwrap.xs zip=vlib_*.zip licensefile=agreement.pdf licensetext=license.txt")
  java.lang.System.exit(1);
  
}
var zipname=zip.substr(0,dot);
var temp=zip.toString().split("_");
var bundlename=temp[0];
var targString=temp[1];

var Package = xdc.loadCapsule('ti/mas/swtools/CommonBld.xs');
licensestring=Package.getLicenseString(licensetext);

/* Create the MPI file */
var mpiName="MAS-Install.mpi";
var unzip=true;
var license=true;
var tplt = xdc.loadTemplate('ti/mas/swtools/MAS-Install.mpi.xdt');
var Pkg=[];
Pkg.name=bundlename;
Pkg.version=temp[2]+"."+temp[3]+"."+temp[4]+"."+temp[5];
tplt.genFile (mpiName, Pkg, [bundlename.toString().toUpperCase(),targString,unzip,license,licensestring,licensefile]);

 /* copy the environment array from the current environment */
    var env   = java.lang.System.getenv();
    var keys  = env.keySet().toArray();
    var key;
    var stat={};
    var env_j=[];
    for (var i = 0; i < keys.length; i++) {
         key = keys[i];
      if (env.get(key) == "") {
             continue;
         }
         env_j = env_j.concat([key + '=' + env.get(key)]);
     }
   var attrs = {
        envs: env_j
   }
   var xdc_install_dir=java.lang.System.getenv("XDC_INSTALL_DIR");
   
   /* Copy the license agreement to the root of temp folder */
    var copy = xdc.loadCapsule('ti/mas/swtools/Copy.xs');
	copy.File(licensefile,"tmp/"+bundlename);

   
   /* Unzip the archive to the temp folder */
   print("Unzipping the archive ...");
   var command=xdc_install_dir.toString()+"/bin/unzip.exe -uo -qq " + zip + " -d tmp";
   xdc.exec(command,attrs,stat);
   print(stat.output);
	


        /* Create the installer */
		print("Creating the installer ...");
        var InstallJammerVersion = " -DVersion " + Pkg.version;
        var PackageBaseDir = " -DPackageBaseDir " + java.io.File(".//").getPath() + "/tmp/" + bundlename;
        var WinInstallDir = " -DWinInstallDir C:/ti/" + zipname;
        var AppName =" -DAppName " + Pkg.name;
        var linuxInstaller=" -DLinuxInstaller "+ zipname +"_Linux.bin"; 
        var winInstaller=" -DWinInstaller "+ zipname+"_Win32.exe"; 
        var command =  "installjammer.exe " + AppName +linuxInstaller+winInstaller+ InstallJammerVersion + PackageBaseDir + WinInstallDir + 
                          " --output-dir . --build " + mpiName+"\n";
						  
        xdc.exec(command,attrs,stat);
        print(stat.output);
							  
        command="rm.exe -rf " + mpiName + "\n";
		xdc.exec(command,attrs,stat);
        command="rm.exe -rf  tmp\n"; 
		xdc.exec(command,attrs,stat);
        command="rm.exe -rf  build.log\n";
		xdc.exec(command,attrs,stat);
        command="rm.exe -rf  build\n";
		xdc.exec(command,attrs,stat);
