
/* utility function to log all the bundleArtifacts to log file */
function logBundleArtifacts(artifactLog,bundleArtifacts)
{
  /*Open artifact log file for logging */
  if(artifactLog == undefined)
  {
    var fileModule = xdc.module('xdc.services.io.File');
    var openedFile;
    try {
      artifactLog = fileModule.open("buildArtifacts.log", "w");
    } 
    catch (ex) {
      print("buildArtifacts.log cannot be opened for logging");
    }
  }
  /* Log every artifact listed in bundleArtifacts */
  for each (var file in bundleArtifacts)
  {
   	  artifactLog.writeLine(file);
  }

  return artifactLog;

}