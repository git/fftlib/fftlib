function usage()
 {
     print("\n\tUSAGE:\n");
     print("\t\txs -f createRegressXml.xs srcDir test.xml\n");
     java.lang.System.exit(1);
 }

 function parseArgs(arguments)
 {
    inputDir = arguments[0];
    outputFile = arguments[1];
    print("Input Directory:"+inputDir);
    print("Output XML File:"+outputFile);
 }

function searchFiles(dirs,recurse)
{
  var file = new java.io.File(dirs);
  if(!file.isDirectory())
  {
    print("Error: "+dirs+" is not a directory");
    usage();
  }

  var list = file.list();
  var i, j;
  for (i = 0; i < list.length; i++)
  {
     if(recurse == true && java.io.File(dirs+"/"+list[i]).isDirectory())
     {
        searchFiles(dirs+"/"+list[i],true);
     }
     var srcFile=String(list[i]);
     var dot = srcFile.lastIndexOf(".");
     var extension = srcFile.substr(dot,srcFile.length);      
     if (extension == ".out")
     {
        srcArray.push(dirs+"/"+list[i]);
     }
      
  }
}

function testSuite(file,srcFiles,target)
{
  switch(target)
  {
    case "C64Ple":
	 file.write('<testList name="testList_C64Ple" ccxmlConfigFile="regression\\common\\ccxml\\c64x+LE.ccxml" cpu="TMS320C64+" gel="">\n');
	break;
	
	case "C64Pbe":
	 file.write('<testList name="testList_C64Pbe" ccxmlConfigFile="regression\\common\\ccxml\\c64x+BE.ccxml" cpu="TMS320C64+" gel="">\n');
	break;

	case "C66le":
	 file.write('<testList name="testList_C66le" ccxmlConfigFile="regression\\common\\ccxml\\c66LE.ccxml" cpu="TMS320C66x_0" gel="">\n');
	break;
	
	case "C66be":
	 file.write('<testList name="testList_C66be" ccxmlConfigFile="regression\\common\\ccxml\\c66BE.ccxml" cpu="TMS320C66x_0" gel="">\n');
	break;
	
	case "C674le":
	 file.write('<testList name="testList_C66be" ccxmlConfigFile="regression\\common\\ccxml\\C674x.ccxml" cpu="TMS320C64X+_0" gel="">\n');
	break;
	
	default:
	  print("Error: Unrecognized outfile "+file);
   
  }
  file.write('<apps>\n');
  for each (var outfile in srcFiles)
  {
   file.write('<app exec="'+outfile+'" num="1" ref="" />\n');
  }
  
  file.write('</apps>\n');
  file.write('</testList>\n');

}

/* Main Function starts here */

var env   = java.lang.System.getenv();
    var keys  = env.keySet().toArray();
    var key;
    var stat={};
    var env_j=[];
    for (var i = 0; i < keys.length; i++) {
         key = keys[i];
      if (env.get(key) == "") {
             continue;
         }
         env_j = env_j.concat([key + '=' + env.get(key)]);
     }
   var attrs = {
        envs: env_j
   }

var inputDir;
var outputFile;
var readLine;
var outputFileString = "";
var srcArray = new Array();
var recurse=false;

/* Check for valid number of Arguments */
if(arguments.length < 2)
{
   usage();
}


/* parse Arguments */
parseArgs(arguments);

/* Check for valid Arguments */
if(inputDir == undefined || outputFile==undefined)
{
   java.lang.System.exit(1);
}

/*Open outputFile file and copy the string to string bugger */
var fileModule = xdc.module('xdc.services.io.File');
/* Copy Module */
var copy = xdc.loadCapsule('ti/mas/swtools/Copy.xs');

/* Open XML File for writing */
var xml = new java.io.File(outputFile);
var indent = "    ";
xml["delete"]();
var out = new java.io.BufferedWriter(new java.io.FileWriter(xml));
out.write('<?xml version="1.0" encoding="UTF-8"?>\n');
out.write('<testSuite>\n');


/* Search for out files */
searchFiles(inputDir,true);

/* Organize the files based on the target */
var out_64p_le=new Array();
var out_64p_be=new Array();
var out_674_le=new Array();
var out_66_le=new Array();
var out_66_be=new Array();
for each(var file in srcArray)
{
  var split_und= file.toString().split("_");
  switch (split_und[split_und.length-3])
  {
    case "64P":
	if(split_und[split_und.length-2] == "LE")
	    out_64p_le.push(file);
	else
	    out_64p_be.push(file);
	break;

	case "66":
	if(split_und[split_und.length-2] == "LE")
	    out_66_le.push(file);
	else
	    out_66_be.push(file);
	break;
	
	case "674":
	if(split_und[split_und.length-2] == "LE")
	    out_674_le.push(file);
	break;
	
	default:
	  print("Error: Unrecognized outfile "+file);
  }
}
/* Create test suite for C64x+ */
 if(out_64p_le.length > 0)
 {
   testSuite(out, out_64p_le,"C64Ple");
 }
if(out_64p_be.length > 0)
 {
   testSuite(out, out_64p_be,"C64Pbe");
 }
if(out_66_le.length > 0)
 {
   testSuite(out, out_66_le,"C66le");
 }
if(out_66_be.length > 0)
 {
   testSuite(out, out_66_be,"C66be");
 }
if(out_674_le.length > 0)
 {
   testSuite(out, out_674_le,"C674le");
 }

 out.write('</testSuite>\n');
 out.close();





 

