/*
 * Description of Memory Regions used by the OpenMP runtime
 */
metaonly module Platform inherits xdc.platform.IPlatform {

    config ti.platforms.generic.Platform.Instance CPU =
        ti.platforms.generic.Platform.create("CPU", {
            clockRate:      1000,                                       
            catalogName:    "ti.catalog.c6000",
            deviceName:     "TMS320TCI6636",
            customMemoryMap: [
                ["L2SRAM",    
                                {name: "L2SRAM",  base: 0x00800000, 
                                len: 0x000F0000, access: "RWX"}],
                ["OMP_MSMC_NC_VIRT",   
                                {name: "OMP_MSMC_NC_VIRT", base: 0xE0000000, 
                                len: 0x00020000, access: "RW"}],
                ["OMP_MSMC_NC_PHY",   
                               {name: "OMP_MSMC_NC_PHY", base: 0x0C000000, 
                                len: 0x00020000, access: "RW"}],
                ["MSMCSRAM",   
                                {name: "MSMCSRAM", base: 0x0C020000, 
                                len: 0x005E0000, access: "RWX"}],
                ["DDR3",   
                                {name: "DDR3", base: 0xa0000000, 
                                len: 0x20000000, access: "RWX"}],
            ],

            l2Mode:"64k",
            l1PMode:"32k",
            l1DMode:"32k",
    });
    
instance :
    
    override config string codeMemory  = "MSMCSRAM";   
    override config string dataMemory  = "DDR3";
    override config string stackMemory = "L2SRAM";
    
}
