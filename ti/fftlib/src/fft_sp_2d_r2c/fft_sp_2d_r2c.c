/* ======================================================================= */
/*  TEXAS INSTRUMENTS, INC.                                                */
/*                                                                         */
/*  FFTLIB  FFT Library                                                    */
/*                                                                         */
/* Copyright (C) 2013 Texas Instruments Incorporated - http://www.ti.com/  */ 
/*                                                                         */
/*                                                                         */
/*  Redistribution and use in source and binary forms, with or without     */
/*  modification, are permitted provided that the following conditions     */
/*  are met:                                                               */
/*                                                                         */
/*    Redistributions of source code must retain the above copyright       */
/*    notice, this list of conditions and the following disclaimer.        */
/*                                                                         */
/*    Redistributions in binary form must reproduce the above copyright    */
/*    notice, this list of conditions and the following disclaimer in the  */
/*    documentation and/or other materials provided with the               */
/*    distribution.                                                        */
/*                                                                         */
/*    Neither the name of Texas Instruments Incorporated nor the names of  */
/*    its contributors may be used to endorse or promote products derived  */
/*    from this software without specific prior written permission.        */
/*                                                                         */
/*  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS    */
/*  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT      */
/*  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR  */
/*  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT   */
/*  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,  */
/*  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT       */
/*  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,  */
/*  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY  */
/*  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT    */
/*  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE  */
/*  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.   */
/*                                                                         */
/* ======================================================================= */

#include <xdc/std.h>
#include "c6x.h"
#include "stdlib.h"
#include "fft_sp_2d_r2c.h"
#include <ti/dsplib/src/DSPF_sp_fftSPxSP/DSPF_sp_fftSPxSP.h>
#include <ti/fftlib/src/common/fft/fft_sp_mixed_bs.h>
#include <ti/fftlib/src/common/fft/fft_sp_cmplx_notwid_br.h>
#include <ti/fftlib/src/common/fft_edma.h>
#include <ti/dsplib/src/DSPF_sp_mat_trans_cplx/DSPF_sp_mat_trans_cplx.h>

int fft_sp_1d_r2c_ecpy_transpose (
    int   dim,               /* dimension                      */
    int   N1,                /* FFT size                       */
    int   N2,                /* number of lines                */
    int   M,                 /* Padded size of Anysize FFT     */
    int   rad,               /* radix                          */
    int   N_p2,              /* power of 2 size in mixed radix */
    int   s_r3,              /* number stages of radix 3       */
    int   s_r5,              /* number stages of radix 5       */
    float *in,               /* input pointer                  */
    float *out,              /* output pointer                 */
    float *in_w,             /* input work buffer pointer      */
    float *out_w,            /* output working buffer pointer  */
    float *out_t,            /* temp working buffer pointer    */
    float *tw,               /* twiddle pointer                */
    float *Bn_w,
    float *workbuf_bn,
    FFT_EDMA_Struct  *edma,  /* edma resource pointer          */
    int num_of_lines
)
{
    int   error = FFT_NOERR;
    int   j,ch_i,ch_o,ch_i1,ch_o1,dum0,dum1;
    int   insize, instep, outsample = N1,
          linesize  = N2*2*sizeof(float);

    float *restrict in_ptr = in, *restrict out_ptr = out;
#if (FFT_SP_2D_R2C_NUMOFLINEBUFS>1)        
    float *restrict tempout ;
#endif    
    float *restrict input, *restrict output, *restrict in1, *restrict out1, *dum2, *dum3;
    
    if (dim == 1)
    {
      insize = N1*sizeof(float)*num_of_lines;
      instep = N1*num_of_lines;
    }
    else
    {
      insize = 2*N1*sizeof(float)*num_of_lines;
      instep = 2*N1*num_of_lines;
    }

    /* prepare first FFT input buffer */
    EdmaMgr_copy1D1D(
      edma->channel[FFT_XFER_CHANNEL0],
      (void *)in_ptr,
      (void *)in_w,
      insize);
    in_ptr += instep;

    /* set pingpong buffer pointers */
    in1    = in_w;
    out1   = out_w;
    input  = &in_w[N1*dim*num_of_lines];
    output = &out_w[2*N1*num_of_lines];
#if (FFT_SP_2D_R2C_NUMOFLINEBUFS>1)    
    tempout= out_t;
#endif    

    /* prepare second FFT input buffer */
    EdmaMgr_copy1D1D(
      edma->channel[FFT_XFER_CHANNEL2],
      (void *)in_ptr,
      (void *)input,
      insize);
    in_ptr += instep;

    /* wait for first FFT input */
    EdmaMgr_wait(edma->channel[FFT_XFER_CHANNEL0]);

#if (FFT_SP_2D_R2C_NUMOFLINEBUFS>1)
  if (dim == 1) {
    if (N1 == M) {
      if (s_r3 == 0 && s_r5 == 0) {
        for (i=0; i<num_of_lines; i++)
          fft_SPxSP_r2c (N1, &in1[i*N1], tw, &tempout[i*N1*2], N1);
      } else {
        for (i=0; i<num_of_lines; i++)
          fft_SPxSP_mixed_radix_r2c (N1, &in1[i*N1], tw, &tempout[i*N1*2], N_p2, s_r3, s_r5);
      }
    } else {
      for (i=0; i<num_of_lines; i++)
        fft_SPxSP_r2c_bs (N1, M, rad, &in1[i*N1], &tempout[i*N1*2], tw, Bn_w, workbuf_bn);
    }
  } else {
    if (N1 == M) {
      if (s_r3 == 0 && s_r5 == 0) {
        for (i=0; i<num_of_lines; i++)
          fft_sp_cmplx_notwid_br((double *)&in1[i*N1*2], N1, (double *)&tempout[i*N1*2], (double *)tw);
      } else {
        for (i=0; i<num_of_lines; i++)
          fft_SPxSP_mixed_radix_c2c (N1, &in1[i*N1*2], tw, &tempout[i*N1*2], N_p2, s_r3, s_r5);
      }
    } else {
      for (i=0; i<num_of_lines; i++)
        fft_SPxSP_c2c_bs (N1, M, rad, &in1[i*N1*2], &tempout[i*N1*2], tw, Bn_w, workbuf_bn);
    }
  }

  /* local transpose from tempout to out1 */
  DSPF_sp_mat_trans_cplx(tempout, num_of_lines, N1, out1);
#else
    if (dim == 1) {
      if (N1 == M) {
        if (s_r3 == 0 && s_r5 == 0) {
          fft_SPxSP_r2c (N1, in1, tw, out1, N1);
        } else {
          fft_SPxSP_mixed_radix_r2c (N1, in1, tw, out1, N_p2, s_r3, s_r5);
        }
      } else {
        fft_SPxSP_r2c_bs (N1, M, rad, in1, out1, tw, Bn_w, workbuf_bn);
      }
    } else {
      if (N1 == M) {
        if (s_r3 == 0 && s_r5 == 0) {
          fft_sp_cmplx_notwid_br((double *)in1, N1, (double *)out1, (double *)tw);
        } else {
          fft_SPxSP_mixed_radix_c2c (N1, in1, tw, out1, N_p2, s_r3, s_r5);
        }
      } else {
        fft_SPxSP_c2c_bs (N1, M, rad, in1, out1, tw, Bn_w, workbuf_bn);
      }
    }
#endif

    /* export output data of first FFT, transposed */
    EdmaMgr_copy1D2D(
      edma->channel[FFT_XFER_CHANNEL1],
      (void *)out1,
      (void *)out_ptr,
      2*sizeof(float)*num_of_lines,
      outsample,
      linesize);
    out_ptr += 2*num_of_lines;

    /* prepare third FFT input buffer */
    EdmaMgr_copy1D1D(
      edma->channel[FFT_XFER_CHANNEL0],
      (void *)in_ptr,
      (void *)in_w,
      insize);
    in_ptr += instep;

    /* set channel numbers */
    ch_i  = FFT_XFER_CHANNEL2;
    ch_o  = FFT_XFER_CHANNEL3;
    ch_i1 = FFT_XFER_CHANNEL0;
    ch_o1 = FFT_XFER_CHANNEL1;

    /* wait for EDMA */
    EdmaMgr_wait(edma->channel[ch_i]);
    _nassert (N2 >= 4);

    for (j = 1; j < N2/num_of_lines; j++) {
#if (FFT_SP_2D_R2C_NUMOFLINEBUFS>1)
  if (dim == 1) {
    if (N1 == M) {
      if (s_r3 == 0 && s_r5 == 0) {
        for (i=0; i<num_of_lines; i++)
          fft_SPxSP_r2c (N1, &input[i*N1], tw, &tempout[i*N1*2], N1);
      } else {
        for (i=0; i<num_of_lines; i++)
          fft_SPxSP_mixed_radix_r2c (N1, &input[i*N1], tw, &tempout[i*N1*2], N_p2, s_r3, s_r5);
      }
    } else {
      for (i=0; i<num_of_lines; i++)
        fft_SPxSP_r2c_bs (N1, M, rad, &input[i*N1], &tempout[i*N1*2], tw, Bn_w, workbuf_bn);
    }
  } else {
    if (N1 == M) {
      if (s_r3 == 0 && s_r5 == 0) {
        for (i=0; i<num_of_lines; i++)
          fft_sp_cmplx_notwid_br((double *)&input[i*N1*2], N1, (double *)&tempout[i*N1*2], (double *)tw);
      } else {
        for (i=0; i<num_of_lines; i++)
          fft_SPxSP_mixed_radix_c2c (N1, &input[i*N1*2], tw, &tempout[i*N1*2], N_p2, s_r3, s_r5);
      }
    } else {
      for (i=0; i<num_of_lines; i++)
        fft_SPxSP_c2c_bs (N1, M, rad, &input[i*N1*2], &tempout[i*N1*2], tw, Bn_w, workbuf_bn);
    }
  }

  /* local transpose from tempout to out1 */
  DSPF_sp_mat_trans_cplx(tempout, num_of_lines, N1, output);
#else
    if (dim == 1) {
      if (N1 == M) {
        if (s_r3 == 0 && s_r5 == 0) {
          fft_SPxSP_r2c (N1, input, tw, output, N1);
        } else {
          fft_SPxSP_mixed_radix_r2c (N1, input, tw, output, N_p2, s_r3, s_r5);
        }
      } else {
        fft_SPxSP_r2c_bs (N1, M, rad, input, output, tw, Bn_w, workbuf_bn);
      }
    } else {
      if (N1 == M) {
        if (s_r3 == 0 && s_r5 == 0) {
          fft_sp_cmplx_notwid_br((double *)input, N1, (double *)output, (double *)tw);
        } else {
          fft_SPxSP_mixed_radix_c2c (N1, input, tw, output, N_p2, s_r3, s_r5);
        }
      } else {
        fft_SPxSP_c2c_bs (N1, M, rad, input, output, tw, Bn_w, workbuf_bn);
      }
    }
#endif

      /* export output data, transposed */

      EdmaMgr_copy1D2D(
        edma->channel[ch_o],
        (void *)output,
        (void *)out_ptr,
        2*sizeof(float)*num_of_lines,
        outsample,
        linesize);
      out_ptr += 2*num_of_lines;

      /* prepare input data */
      EdmaMgr_copy1D1D(
        edma->channel[ch_i],
        (void *)in_ptr,
        (void *)input,
        insize);
      in_ptr += instep;

      /* swap pingpong buffer pointers and XFER channel */
      dum2   = input;
      input  = in1;
      in1    = dum2;
      dum3   = output;
      output = out1;
      out1   = dum3;

      dum0   = ch_i;
      ch_i   = ch_i1;
      ch_i1  = dum0;
      dum1   = ch_o;
      ch_o   = ch_o1;
      ch_o1  = dum1;

      /* wait for EDMA */
      EdmaMgr_wait(edma->channel[ch_i]);
      EdmaMgr_wait(edma->channel[ch_o]);
    }
    
  /* wait for EDMA */
  EdmaMgr_wait(edma->channel[ch_i1]);
  EdmaMgr_wait(edma->channel[ch_o1]);
   return error;
} /* fft_sp_1d_r2c_ecpy_transpose */

int fft_sp_2d_r2c_ecpy (
    /* int   N1;            FFT size                          */
	/* int   N2;            FFT size                          */
    /* float *data;         data pointer                      */
    /* float *workbuf;      work buffer pointer               */
    /* float *data_w;       data internal pointer             */
    /* float *workbuf_w;    working buffer internal pointer   */
    /* float *tw;           twiddle pointer                   */
    fft_param_u u,
    void *edmaState
)
{
    /* input parameters */
    float *data       = u.sp_2d_r2c_e.data;        /* data pointer                   */
    float *workbuf    = u.sp_2d_r2c_e.workbuf;     /* output pointer                 */
    float *data_w     = u.sp_2d_r2c_e.data_w;      /* input working buffer pointer   */
    float *workbuf_w  = u.sp_2d_r2c_e.workbuf_w;   /* output working buffer pointer  */
    float *workbuf_t  = u.sp_2d_r2c_e.workbuf_t;   /* temp working buffer pointer    */
    float *tw_w       = u.sp_2d_r2c_e.tw_w;        /* twiddle working buffer pointer */
    float *tw         = u.sp_2d_r2c_e.tw;          /* twiddle buffer pointer         */
    float *Bn         = u.sp_2d_r2c_e.Bn;
    float *Bn_w       = u.sp_2d_r2c_e.Bn_w;
    float *workbuf_bn = u.sp_2d_r2c_e.workbuf_bn;
    float *tw_a, *Bn_a;
    
    int   N1          = u.sp_2d_r2c_e.N1;          /* signal size                           */
    int   N2          = u.sp_2d_r2c_e.N2;          /* signal size                           */
    int   M1          = u.sp_2d_r2c_e.para1.M;     /* first dimension FFT size M1           */
    int   M2          = u.sp_2d_r2c_e.para2.M;     /* second dimension FFT size M2          */
    int   rad         = u.sp_2d_r2c_e.para1.rad;   /* radix for N1                          */
    
    int   twsize      = u.sp_2d_r2c_e.para1.twsize;    /* twiddle size in double words          */
    int   N_p2        = u.sp_2d_r2c_e.para1.N_p2;      /* power of 2 size in mixed radix        */
    int   s_r3        = u.sp_2d_r2c_e.para1.s_r3;      /* number stages of radix 3              */
    int   s_r5        = u.sp_2d_r2c_e.para1.s_r5;      /* number stages of radix 5              */
    int   Bnsize      = u.sp_2d_r2c_e.para1.Bnsize;
    int   tw_offset, Bn_offset;
    
    FFT_EDMA_Struct  *edma = (FFT_EDMA_Struct *)edmaState;

    int   error = FFT_NOERR, numLine = FFT_SP_2D_R2C_NUMOFLINEBUFS;

    if (tw != NULL) {
      /* prepare twiddle buffer */
      EdmaMgr_copy1D1D(
        edma->channel[FFT_XFER_CHANNEL3],
        (void *)tw,
        (void *)tw_w,
        twsize*sizeof(float));
      tw_a = tw_w;
    }
    else {
      tw_a = tw;
    }
   
    if (Bn_w != NULL) {
      /* prepare Bn buffer */
      EdmaMgr_copy1D1D(
        edma->channel[FFT_XFER_CHANNEL2],
        (void *)Bn,
        (void *)Bn_w,
        Bnsize*sizeof(float));
      Bn_a = Bn_w;
    } else {
      Bn_a = Bn;
    }
    
    if (tw_w != NULL)
    	EdmaMgr_wait(edma->channel[FFT_XFER_CHANNEL3]);
    if (Bn_w != NULL)
    	EdmaMgr_wait(edma->channel[FFT_XFER_CHANNEL2]);

    /* 1st dimension fft */
    error = fft_sp_1d_r2c_ecpy_transpose (1, N1, N2, M1, rad, N_p2, s_r3, s_r5, data, workbuf,
                                          data_w, workbuf_w, workbuf_t, tw_a, Bn_a, 
                                          workbuf_bn, edma, numLine);
    
    /* prepare for the 2nd dimension */
    twsize = u.sp_2d_r2c_e.para2.twsize;
    Bnsize = u.sp_2d_r2c_e.para2.Bnsize;
    N_p2   = u.sp_2d_r2c_e.para2.N_p2;    /* power of 2 size in mixed radix        */
    s_r3   = u.sp_2d_r2c_e.para2.s_r3;    /* number stages of radix 3              */
    s_r5   = u.sp_2d_r2c_e.para2.s_r5;    /* number stages of radix 5              */
    rad    = u.sp_2d_r2c_e.para2.rad;
    tw_offset = u.sp_2d_r2c_e.para1.twsize;
    Bn_offset = u.sp_2d_r2c_e.para1.Bnsize;    
                                          
    if (tw != NULL) {
      /* prepare twiddle buffer */
      EdmaMgr_copy1D1D(
        edma->channel[FFT_XFER_CHANNEL3],
        (void *)(tw+tw_offset),
        (void *)tw_w,
        twsize*sizeof(float));
      tw_a = tw_w;
    }
    else {
      tw_a = tw + tw_offset;
    }

    if (Bn_w != NULL) {
      /* prepare Bn buffer when doing bluestein */
      EdmaMgr_copy1D1D(
        edma->channel[FFT_XFER_CHANNEL2],
        (void *)(Bn+Bn_offset),
        (void *)Bn_w,
        Bnsize*sizeof(float));
      Bn_a = Bn_w;
    }
    else {
      Bn_a = Bn + Bn_offset;
    }
    if (tw_w != NULL)
    	EdmaMgr_wait(edma->channel[FFT_XFER_CHANNEL3]);
    if (Bn_w != NULL)
    	EdmaMgr_wait(edma->channel[FFT_XFER_CHANNEL2]);                               
    
    /* 2nd dimension fft */
    error = fft_sp_1d_r2c_ecpy_transpose (2, N2, N1, M2, rad, N_p2, s_r3, s_r5, workbuf, data,
                                          workbuf_w, data_w, workbuf_t, tw_a, Bn_a, 
                                          workbuf_bn, edma, numLine);

    return error;
} /* fft_sp_2d_r2c_ecpy */

/* ======================================================================== */
/*  End of file:  fft_sp_2d_r2c.c                                           */
/* ------------------------------------------------------------------------ */
/*            Copyright (c) 2013 Texas Instruments, Incorporated.           */
/*                           All Rights Reserved.                           */
/* ======================================================================== */
