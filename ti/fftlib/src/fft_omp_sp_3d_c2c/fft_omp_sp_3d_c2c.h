/* ======================================================================= */
/*  TEXAS INSTRUMENTS, INC.                                                */
/*                                                                         */
/*  FFTLIB  FFT Library                                                    */
/*                                                                         */
/* Copyright (C) 2013 Texas Instruments Incorporated - http://www.ti.com/  */ 
/*                                                                         */
/*                                                                         */
/*  Redistribution and use in source and binary forms, with or without     */
/*  modification, are permitted provided that the following conditions     */
/*  are met:                                                               */
/*                                                                         */
/*    Redistributions of source code must retain the above copyright       */
/*    notice, this list of conditions and the following disclaimer.        */
/*                                                                         */
/*    Redistributions in binary form must reproduce the above copyright    */
/*    notice, this list of conditions and the following disclaimer in the  */
/*    documentation and/or other materials provided with the               */
/*    distribution.                                                        */
/*                                                                         */
/*    Neither the name of Texas Instruments Incorporated nor the names of  */
/*    its contributors may be used to endorse or promote products derived  */
/*    from this software without specific prior written permission.        */
/*                                                                         */
/*  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS    */
/*  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT      */
/*  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR  */
/*  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT   */
/*  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,  */
/*  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT       */
/*  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,  */
/*  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY  */
/*  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT    */
/*  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE  */
/*  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.   */
/*                                                                         */
/* ======================================================================= */

#include <ti/fftlib/src/common/fft_common.h>

#ifndef FFT_OMP_SP_3D_C2C_H_
#define FFT_OMP_SP_3D_C2C_H_ 1

/** @ingroup 3D */
/* @{ */

/** @defgroup FFT_OMP_SP_3D_C2C */
/** @ingroup FFT_OMP_SP_3D_C2C */
/* @{ */

/**
 *     @param FFT_OMP_SP_3D_C2C_NUMOFLINEBUFS = number of cache lines to process each iteration for 3rd dimension
 */
 
#ifdef FFT_MEM_MODEL_LG
#define FFT_OMP_SP_3D_C2C_NUMOFLINEBUFS 32
#else
# ifdef FFT_MEM_MODEL_SM
# define FFT_OMP_SP_3D_C2C_NUMOFLINEBUFS 8
# else
# define FFT_OMP_SP_3D_C2C_NUMOFLINEBUFS 16
# endif
#endif

/**
 *     @param FFT_OMP_SP_3D_C2C_NUMOFLINES_SM = number of cache lines to process each iteration for 1st and 2nd dimension
 */
 
#define FFT_OMP_SP_3D_C2C_NUMOFLINES_SM 8

 /**
 *     This program initializes one 3D single-precision complex-to-complex FFT.
 *  
 *     @param N1 = FFT size dimension 1        
 *     @param N2 = FFT size dimension 2
 *     @param N3 = FFT size dimension 3
 *     @param mode = only FFT_ECPY is supported
 *     @param fxns = call out functions used to allocated/release memory and EDMA resource
 *     @param p = return plan structure
 *     @param in = input pointer    
 *     @param out = output pointer   
 *     @param tw = twiddle pointer  
 *     @param workbuf = Bluestein scratch pointer
 * 
 * @par Algorithm:
 *      Algorithm can run in radix 2, 3, 4 & 5 based upon value of N1 and N2. <br>
 *
 * @par Assumptions:
 *      N1, N2 & N3 are a multiple of 64 and positive.<br>
 *      All input, output and working buffers are double word aligned.<br>
 *      No input, working or buffers overlap.<br>
 *
 * @par Size Limitations:
 *      Current limitations is 192 for each dimension<br>
 *
 * @par Implementation notes:
 * @b Endian Support: The code supports little endian modes.  <BR> 
 * @b Interruptibility: The code is interrupt-tolerant but not interruptible. <BR>
 *  
 */

/* FFT plan 3D SP API */
void fft_omp_sp_plan_3d_c2c (
    int   N1,       /* FFT size */
    int   N2,       /* FFT size */
    int   N3,       /* FFT size */
    int   mode,     /* FFT_ECPY */
    fft_callout_t fxns,
    fft_plan_t *p,
    float *in,
    float *out,
    float *tw
);

 /**
 *
 *     This program uses info saved from plan function to execute 3D double-precision complex-to-complex FFT.
 *  
 *     @param u = FFT parameter from plan structure      
 *     @param edmastate = EDMA channel allocation
 *     @param actualCoreNum = run-time configured number of cores
 *     @param return value = error status, FFT_NOERR if success
 *
 */

/* FFT execute 3D SP ecpy mode API */
int fft_omp_sp_3d_c2c_ecpy (
    fft_param_u u,
    void *edmaState,
    int   actualCoreNum
);
 
#endif /* FFT_OMP_SP_3D_C2C_H_ */

/* ======================================================================== */
/*  End of file: fft_omp_sp_3d_c2c.h                                        */
/* ------------------------------------------------------------------------ */
/*            Copyright (c) 2013 Texas Instruments, Incorporated.           */
/*                           All Rights Reserved.                           */
/* ======================================================================== */


