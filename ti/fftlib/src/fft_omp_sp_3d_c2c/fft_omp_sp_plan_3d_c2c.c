/* ======================================================================= */
/*  TEXAS INSTRUMENTS, INC.                                                */
/*                                                                         */
/*  FFTLIB  FFT Library                                                    */
/*                                                                         */
/* Copyright (C) 2013 Texas Instruments Incorporated - http://www.ti.com/  */
/*                                                                         */
/*                                                                         */
/*  Redistribution and use in source and binary forms, with or without     */
/*  modification, are permitted provided that the following conditions     */
/*  are met:                                                               */
/*                                                                         */
/*    Redistributions of source code must retain the above copyright       */
/*    notice, this list of conditions and the following disclaimer.        */
/*                                                                         */
/*    Redistributions in binary form must reproduce the above copyright    */
/*    notice, this list of conditions and the following disclaimer in the  */
/*    documentation and/or other materials provided with the               */
/*    distribution.                                                        */
/*                                                                         */
/*    Neither the name of Texas Instruments Incorporated nor the names of  */
/*    its contributors may be used to endorse or promote products derived  */
/*    from this software without specific prior written permission.        */
/*                                                                         */
/*  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS    */
/*  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT      */
/*  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR  */
/*  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT   */
/*  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,  */
/*  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT       */
/*  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,  */
/*  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY  */
/*  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT    */
/*  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE  */
/*  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.   */
/*                                                                         */
/* ======================================================================= */

#include <math.h>
#include <stdio.h>
#include <c6x.h>
#include "fft_omp_sp_3d_c2c.h"
#include <ti/fftlib/src/common/fft/fft_sp_cmplx_notwid_br.h>
#include <ti/csl/csl_cacheAux.h>

/* twiddle generation */
static void twiddle_gen (int N, int M, int rad, int use_bs, int s_r3, int s_r5, int N_p2, float *tw, float *Bn, float *work)
{
  int     k, n, j, i;
  float *bn, *Bk;
  const double PI = 3.14159265358979323846;

  k = 0;
  if (use_bs == 0){
    tw[k    ] = -sin (2 * PI / N_p2);
    tw[k + 1] =  cos (2 * PI / N_p2);

    if ((s_r3 != 0) || (s_r5 != 0)) {
      /* generate twiddles for radix-3 loop */
        k  = 2;
        n = N;
      for (i = 0; i < s_r3; i++) {
          for (j = 0; j < n/3; j++) {
#ifdef _LITTLE_ENDIAN
            tw[k]     = (float) sin (2 * PI * j / n);
            tw[k + 1] = (float) cos (2 * PI * j / n);
            tw[k + 2] = (float) sin (4 * PI * j / n);
            tw[k + 3] = (float) cos (4 * PI * j / n);
#else
            tw[k]     = (float)  cos (2 * PI * j / n);
            tw[k + 1] = (float) -sin (2 * PI * j / n);
            tw[k + 2] = (float)  cos (4 * PI * j / n);
            tw[k + 3] = (float) -sin (4 * PI * j / n);
#endif
            k += 4;
          }
          n /= 3;
        }

        /* generate twiddles for radix-5 loop */
        for (i = 0; i < s_r5; i++) {
          for (j = 0; j < n/5; j++) {
#ifdef _LITTLE_ENDIAN
            tw[k]     = (float) sin (2 * PI * j / n);
            tw[k + 1] = (float) cos (2 * PI * j / n);
            tw[k + 2] = (float) sin (4 * PI * j / n);
            tw[k + 3] = (float) cos (4 * PI * j / n);
            tw[k + 4] = (float) sin (6 * PI * j / n);
            tw[k + 5] = (float) cos (6 * PI * j / n);
            tw[k + 6] = (float) sin (8 * PI * j / n);
            tw[k + 7] = (float) cos (8 * PI * j / n);
#else
            tw[k]     = (float)  cos (2 * PI * j / n);
            tw[k + 1] = (float) -sin (2 * PI * j / n);
            tw[k + 2] = (float)  cos (4 * PI * j / n);
            tw[k + 3] = (float) -sin (4 * PI * j / n);
            tw[k + 4] = (float)  cos (6 * PI * j / n);
            tw[k + 5] = (float) -sin (6 * PI * j / n);
            tw[k + 6] = (float)  cos (8 * PI * j / n);
            tw[k + 7] = (float) -sin (8 * PI * j / n);
#endif
            k += 8;
          }
          n /= 5;
        }
      }
    }
    else
    { /* generate twiddle for c2c fft */

      /*--------------------------------------
       *  TW is organized as follows:
       *     twiddle for fft,  size: 2
       *     Bk,               size: 2*N
       *     twiddle for ifft, size: 2
       *-------------------------------------*/

      tw[k    ] = -sin (2 * PI / M);
      tw[k + 1] =  cos (2 * PI / M);

      k = 2;

      /* phase factor generation */
      for (i = 0; i < N ; i++) {
        tw[k]     = (float)  cos (PI * i * i / N);
        tw[k + 1] = (float)  sin (PI * i * i / N);

        k += 2;
      }

      k = 2*N + 2;

      /* twiddle for ifft */
      tw[k    ] =  sin (2 * PI / M);
      tw[k + 1] =  cos (2 * PI / M);

      /* precalculate FFT of bn */

#pragma MUST_ITERATE(4,,4)
      /* first reset the work buffer to zeros */
      for (i = 0; i<M; i+=1)
      {
         _amem8_f2(&work[2*i]) = 0;
      }

      /* construct bn in work buffer */
      bn = work;
      Bk = tw + 2;

      /*---------------------------------------------*
       *         bn(0)    = Bk(0)
       *         bn(i)    = Bk(i)    0 < i <N
       *         bn(N1-i) = Bk(i)    N <= i <N1
       *  Note: bn(i) = Re(bn(i)) + i* Im(bn(i));
       ------------------------------------------- */
      bn[0] = Bk[0];
      bn[1] = Bk[1];

      for(i = 1; i < N; i++)
      {
        bn[2*i]   = bn[2*(M-i)]   = Bk[2*i];
        bn[2*i+1] = bn[2*(M-i)+1] = Bk[2*i+1];
      }

      /* FFT of the constructed signal Bn = fft(bn) */
      fft_sp_cmplx_notwid_br((double*)bn, M, (double*)Bn, (double*)tw);
    }
} /* twiddle_gen() */

void fft_omp_sp_plan_3d_c2c (
    int   N1,       /* FFT size */
    int   N2,       /* FFT size */
    int   N3,       /* FFT size */
    int   mode,     /* FFT_DIRECT or FFT_ECPY */
    fft_callout_t fxns,
    fft_plan_t *p,
    float *in,
    float *out,
    float *tw
)
{
    int          M1, M2, M3, rad;
    int          use_bs_1 = 0, use_bs_2 = 0, use_bs_3 = 0;
    int          s_r3, s_r5, N_p2, Bnsize, twsize;
    float        *restrict Bn, *restrict work;
    
    /* only support FFT_ECPY */
    p->fftfxn = fft_omp_sp_3d_c2c_ecpy;

    memcpy(&p->fftcout, &fxns, sizeof(fft_callout_t));

    p->u.sp_3d_c2c_e.N1   = N1;
    p->u.sp_3d_c2c_e.N2   = N2;
    p->u.sp_3d_c2c_e.N3   = N3;
    p->u.sp_3d_c2c_e.in   = in;
    p->u.sp_3d_c2c_e.out  = out;
    p->u.sp_3d_c2c_e.tw   = tw;
    
    use_bs_1 = p->u.sp_3d_c2c_e.para1.use_bs;
    use_bs_2 = p->u.sp_3d_c2c_e.para2.use_bs;
    use_bs_3 = p->u.sp_3d_c2c_e.para3.use_bs;

    /* twiddle generation */
    Bn   = p->u.sp_3d_c2c_e.Bn;
    work = p->u.sp_3d_c2c_e.workbuf_bn;

    /* generate twiddle for the first dimension c2c fft */
    /* split factors */
    s_r3 = p->u.sp_3d_c2c_e.para1.s_r3;
    s_r5 = p->u.sp_3d_c2c_e.para1.s_r5;
    N_p2 = p->u.sp_3d_c2c_e.para1.N_p2;
    rad  = p->u.sp_3d_c2c_e.para1.rad;
    M1   = p->u.sp_3d_c2c_e.para1.M;
    twiddle_gen (N1, M1, rad, use_bs_1, s_r3, s_r5, N_p2, tw+4*N1*N2*N3, Bn, work);

    /* generate twiddle for the second dimension c2c fft */
    twsize = p->u.sp_3d_c2c_e.para1.twsize;
    Bnsize = p->u.sp_3d_c2c_e.para1.Bnsize;

    s_r3 = p->u.sp_3d_c2c_e.para2.s_r3;
    s_r5 = p->u.sp_3d_c2c_e.para2.s_r5;
    N_p2 = p->u.sp_3d_c2c_e.para2.N_p2;
    rad  = p->u.sp_3d_c2c_e.para2.rad;
    M2   = p->u.sp_3d_c2c_e.para2.M;
    twiddle_gen (N2, M2, rad, use_bs_2, s_r3, s_r5, N_p2, tw+4*N1*N2*N3+twsize, Bn+Bnsize, work);

    /* generate twiddle for the third dimension c2c fft */
    twsize = p->u.sp_3d_c2c_e.para1.twsize + p->u.sp_3d_c2c_e.para2.twsize;
    Bnsize = p->u.sp_3d_c2c_e.para1.Bnsize + p->u.sp_3d_c2c_e.para2.Bnsize;

    s_r3 = p->u.sp_3d_c2c_e.para3.s_r3;
    s_r5 = p->u.sp_3d_c2c_e.para3.s_r5;
    N_p2 = p->u.sp_3d_c2c_e.para3.N_p2;
    rad  = p->u.sp_3d_c2c_e.para3.rad;
    M3   = p->u.sp_3d_c2c_e.para3.M;
    twiddle_gen (N3, M3, rad, use_bs_3, s_r3, s_r5, N_p2, tw+4*N1*N2*N3+twsize, Bn+Bnsize, work);

    CACHE_wbInvAllL2(CACHE_WAIT);
}

/* ======================================================================== */
/*  End of file:  fft_omp_plan_sp_3d_c2c.c                                  */
/* ------------------------------------------------------------------------ */
/*            Copyright (c) 2013 Texas Instruments, Incorporated.           */
/*                           All Rights Reserved.                           */
/* ======================================================================== */
