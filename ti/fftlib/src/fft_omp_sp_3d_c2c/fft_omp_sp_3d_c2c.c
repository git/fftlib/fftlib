/* ======================================================================= */
/*  TEXAS INSTRUMENTS, INC.                                                */
/*                                                                         */
/*  FFTLIB  FFT Library                                                    */
/*                                                                         */
/* Copyright (C) 2013 Texas Instruments Incorporated - http://www.ti.com/  */
/*                                                                         */
/*                                                                         */
/*  Redistribution and use in source and binary forms, with or without     */
/*  modification, are permitted provided that the following conditions     */
/*  are met:                                                               */
/*                                                                         */
/*    Redistributions of source code must retain the above copyright       */
/*    notice, this list of conditions and the following disclaimer.        */
/*                                                                         */
/*    Redistributions in binary form must reproduce the above copyright    */
/*    notice, this list of conditions and the following disclaimer in the  */
/*    documentation and/or other materials provided with the               */
/*    distribution.                                                        */
/*                                                                         */
/*    Neither the name of Texas Instruments Incorporated nor the names of  */
/*    its contributors may be used to endorse or promote products derived  */
/*    from this software without specific prior written permission.        */
/*                                                                         */
/*  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS    */
/*  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT      */
/*  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR  */
/*  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT   */
/*  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,  */
/*  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT       */
/*  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,  */
/*  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY  */
/*  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT    */
/*  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE  */
/*  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.   */
/*                                                                         */
/* ======================================================================= */

#include <xdc/std.h>
#include "c6x.h"
#include "stdlib.h"
#include <ti/fftlib/src/common/omp/omp_config.h>
#include "fft_omp_sp_3d_c2c.h"
#include <ti/dsplib/src/DSPF_sp_fftSPxSP/DSPF_sp_fftSPxSP.h>
#include <ti/fftlib/src/common/fft/fft_sp_mixed_bs.h>
#include <ti/fftlib/src/common/fft/fft_sp_cmplx_notwid_br.h>
#include <ti/fftlib/src/common/ifft/ifft_sp_cmplx_notwid_br.h>
#include <ti/fftlib/src/common/fft_edma.h>
#include <ti/dsplib/src/DSPF_sp_mat_trans_cplx/DSPF_sp_mat_trans_cplx.h>
#include <ti/runtime/openmp/omp.h>
#include <ti/libarch/libarch.h>

static int fft_omp_sp_1d_c2c_batch_ecpy_transpose (
  int   N1,               /* FFT size                       */
  int   N2,               /* number of lines                */
  int   N3,               /* number of planes               */
  int   M,                /* Padded size of Anysize FFT     */
  int   rad,              /* radix                          */
  int   N_p2,             /* power of 2 size in mixed radix */
  int   s_r3,             /* number stages of radix 3       */
  int   s_r5,             /* number stages of radix 5       */
  float *in,              /* input pointer                  */
  float *out,             /* output pointer                 */
  float *in_w,            /* input work buffer pointer      */
  float *out_w,           /* output working buffer pointer  */
  float *out_t,           /* temp working buffer pointer    */
  float *tw,              /* twiddle pointer                */
  float *Bn_w,
  float *workbuf_bn,
  FFT_EDMA_Struct  *edma, /* edma resource pointer          */
  int num_of_lines        /* number of lines for the 1D FFT */
)
{
    int   error = FFT_NOERR, linecnt = 0;
    int   i,j,ch_i,ch_o,ch_i1,ch_o1,dum0,dum1;
    int   insize, instep,
          outsample = N1,
          linesize, linewrap;
    FFT_EDMA_Struct  *edmall2;

    float *restrict in_ptr = in, *restrict out_ptr = out, *restrict tempout;
    float *restrict input, *restrict output, *restrict in1, *restrict out1, *dum2, *dum3;

    /* if linesize is bigger than the 16 bit limit, use cpu do  the global transpose for 3rd dimension for now */

    edmall2  = edma;
    linesize = N2*2*sizeof(float);
    linewrap = 2*(N1-1)*N2;

    insize = 2*N1*sizeof(float)*num_of_lines;
    instep = 2*N1*num_of_lines;
    
    /* prepare first FFT input buffer */
    lib_emt_copy1D1D(
      edmall2->channel[FFT_XFER_CHANNEL0],
      (void *)in_ptr,
      (void *)in_w,
      insize);
    in_ptr += instep;

    /* set pingpong buffer pointers */
    in1    = in_w;
    out1   = out_w;
    input  = &in_w[instep];
    output = &out_w[instep];
    tempout= out_t;

    /* prepare second FFT input buffer */
    lib_emt_copy1D1D(
      edmall2->channel[FFT_XFER_CHANNEL2],
      (void *)in_ptr,
      (void *)input,
      insize);
    in_ptr += instep;

    /* wait for first FFT input */
    lib_emt_wait(edmall2->channel[FFT_XFER_CHANNEL0]);
    
#if (FFT_OMP_SP_3D_C2C_NUMOFLINEBUFS>1)
    if (N1 == M) {
      if (s_r3 == 0 && s_r5 == 0) {
        for (i=0; i<num_of_lines; i++)
        {
          fft_sp_cmplx_notwid_br((double *)&in1[i*N1*2], N1, (double *)&tempout[i*N1*2], (double *)tw);
        }
      } else
      {
          for (i=0; i<num_of_lines; i++)
            fft_SPxSP_mixed_radix_c2c (N1, &in1[i*N1*2], tw, &tempout[i*N1*2], N_p2, s_r3, s_r5);
      }
    } else
    {
        for (i=0; i<num_of_lines; i++)
          fft_SPxSP_c2c_bs (N1, M, rad, &in1[i*N1*2], &tempout[i*N1*2], tw, Bn_w, workbuf_bn);
    }

    /* local transpose from tempout to out1 */
    DSPF_sp_mat_trans_cplx(tempout, num_of_lines, N1, out1);
#else
    if (N1 == M) {
      if (s_r3 == 0 && s_r5 == 0) {
        fft_sp_cmplx_notwid_br((double *)in1, N1, (double *)out1, (double *)tw);
      } else
      {
          fft_SPxSP_mixed_radix_c2c (N1, in1, tw, out1, N_p2, s_r3, s_r5);
      }
    } else
    {
      fft_SPxSP_c2c_bs (N1, M, rad, in1, out1, tw, Bn_w, workbuf_bn);
    }
#endif

    /* export output data of first FFT*/
    lib_emt_copy1D2D(
      edmall2->channel[FFT_XFER_CHANNEL1],
      (void *)out1,
      (void *)out_ptr,
      2*sizeof(float)*num_of_lines,
      outsample,
      linesize);
    out_ptr += 2*num_of_lines;
    linecnt += num_of_lines;

    /* prepare third FFT input buffer */
    lib_emt_copy1D1D(
      edmall2->channel[FFT_XFER_CHANNEL0],
      (void *)in_ptr,
      (void *)in_w,
      insize);
    in_ptr += instep;

    /* set channel numbers */
    ch_i  = FFT_XFER_CHANNEL2;
    ch_o  = FFT_XFER_CHANNEL3;
    ch_i1 = FFT_XFER_CHANNEL0;
    ch_o1 = FFT_XFER_CHANNEL1;

    /* wait for EDMA */
    lib_emt_wait(edmall2->channel[ch_i]);

    for (j = 1; j < (N3*N2)/num_of_lines; j++)
    {
#if (FFT_OMP_SP_3D_C2C_NUMOFLINEBUFS>1)
      if (N1 == M) {
        if (s_r3 == 0 && s_r5 == 0) {
          for (i=0; i<num_of_lines; i++)
          {
             fft_sp_cmplx_notwid_br((double *)&input[i*N1*2], N1, (double *)&tempout[i*N1*2], (double *)tw);
          }
        } else
        {
            for (i=0; i<num_of_lines; i++)
              fft_SPxSP_mixed_radix_c2c (N1, &input[i*N1*2], tw, &tempout[i*N1*2], N_p2, s_r3, s_r5);
        }
      } else
      {
         for (i=0; i<num_of_lines; i++)
           fft_SPxSP_c2c_bs (N1, M, rad, &input[i*N1*2], &tempout[i*N1*2], tw, Bn_w, workbuf_bn);
      }

      /* local transpose from tempout to output */
      DSPF_sp_mat_trans_cplx(tempout, num_of_lines, N1, output);
#else
      if (N1 == M) {
        if (s_r3 == 0 && s_r5 == 0) {
          fft_sp_cmplx_notwid_br((double *)input, N1, (double *)output, (double *)tw);
        } else
        {
            fft_SPxSP_mixed_radix_c2c (N1, input, tw, output, N_p2, s_r3, s_r5);
        }
      } else
      {
          fft_SPxSP_c2c_bs (N1, M, rad, input, output, tw, Bn_w, workbuf_bn);
      }
#endif

      lib_emt_copy1D2D(
        edmall2->channel[ch_o],
        (void *)output,
        (void *)out_ptr,
        2*sizeof(float)*num_of_lines,
        outsample,
        linesize);
      out_ptr += 2*num_of_lines;
      linecnt += num_of_lines;

      /* re-adjust output pointer when it's time for the next batch */
      if (linecnt >= N2)
      {
        linecnt = 0;
        out_ptr += linewrap;
      }

      /* prepare input data */
      lib_emt_copy1D1D(
        edmall2->channel[ch_i],
        (void *)in_ptr,
        (void *)input,
        insize);
      in_ptr += instep;

      /* swap pingpong buffer pointers and XFER channel */
      dum2   = input;
      input  = in1;
      in1    = dum2;
      dum3   = output;
      output = out1;
      out1   = dum3;

      dum0   = ch_i;
      ch_i   = ch_i1;
      ch_i1  = dum0;
      dum1   = ch_o;
      ch_o   = ch_o1;
      ch_o1  = dum1;

      /* wait for EDMA */
      lib_emt_wait(edmall2->channel[ch_i]);
      lib_emt_wait(edmall2->channel[ch_o]);
    }
    
    lib_emt_wait(edmall2->channel[ch_i1]);
    lib_emt_wait(edmall2->channel[ch_o1]);

    return error;
} /* fft_omp_sp_1d_c2c_batch_ecpy_transpose */

static int fft_omp_sp_1d_c2c_batch_ecpy_transpose_3d (
  int   N1,                /* total number of lines          */
  int   N3,                /* number of planes               */
  int   lines,             /* lines to execute               */
  int   M,                 /* Padded size of Anysize FFT     */
  int   rad,               /* radix                          */
  int   N_p2,              /* power of 2 size in mixed radix */
  int   s_r3,              /* number stages of radix 3       */
  int   s_r5,              /* number stages of radix 5       */
  float *in,               /* input pointer                  */
  float *out,              /* output pointer                 */
  float *in_w,             /* input work buffer pointer      */
  float *out_w,            /* output working buffer pointer  */
  float *out_t,            /* temp working buffer pointer    */
  float *tw,               /* twiddle pointer                */
  float *Bn_w,
  float *workbuf_bn,
  FFT_EDMA_Struct  *edma,  /* edma resource pointer          */
  int num_of_lines         /* number of lines for the 1D FFT */
)
{
    int   error = FFT_NOERR;
    int   i,j,ch_i,ch_o,ch_i1,ch_o1,dum0,dum1;
    int   outsample = N3, linesize = 2*N1*sizeof(float);
    FFT_EDMA_Struct  *edmall2 = edma;

    float *restrict in_ptr = in, *restrict out_ptr = out, *restrict tempout;
    float *restrict input, *restrict output, *restrict in1, *restrict out1, *dum2, *dum3;

    /* prepare first FFT input buffer */
    lib_emt_copy2D1DLarge(
      edmall2->channel[FFT_XFER_CHANNEL0],
      (void *)in_ptr,
      (void *)in_w,
      2*sizeof(float)*num_of_lines,
      outsample,
      linesize);
    in_ptr += 2*num_of_lines;

    /* set pingpong buffer pointers */
    in1    = in_w;
    out1   = out_w;
    input  = &in_w[2*N3*num_of_lines];
    output = &out_w[2*N3*num_of_lines];
    tempout= out_t;

    /* prepare second FFT input buffer */
    lib_emt_copy2D1DLarge(
      edmall2->channel[FFT_XFER_CHANNEL2],
      (void *)in_ptr,
      (void *)input,
      2*sizeof(float)*num_of_lines,
      outsample,
      linesize);
    in_ptr += 2*num_of_lines;

    lib_emt_wait(edmall2->channel[FFT_XFER_CHANNEL0]);

    /* Calculating the FFT */
#if (FFT_OMP_SP_3D_C2C_NUMOFLINEBUFS>1)
    /* local transpose */
    DSPF_sp_mat_trans_cplx(in1, N3, num_of_lines, out1);

    if (N3 == M) {
      if (s_r3 == 0 && s_r5 == 0) {
        for (i=0; i<num_of_lines; i++)
        {
          fft_sp_cmplx_notwid_br((double *)&out1[i*N3*2], N3, (double *)&tempout[i*N3*2], (double *)tw);
        }
      } else
      {
          for (i=0; i<num_of_lines; i++)
            fft_SPxSP_mixed_radix_c2c (N3, &out1[i*N3*2], tw, &tempout[i*N3*2], N_p2, s_r3, s_r5);
      }
    } else
    {
        for (i=0; i<num_of_lines; i++)
          fft_SPxSP_c2c_bs (N3, M, rad, &out1[i*N3*2], &tempout[i*N3*2], tw, Bn_w, workbuf_bn);
    }

    DSPF_sp_mat_trans_cplx(tempout, num_of_lines, N3, out1);
#else
    if (N3 == M) {
      if (s_r3 == 0 && s_r5 == 0) {
        fft_sp_cmplx_notwid_br((float *)in1, N3, (float *)out1, (float *)tw);
      } else
      {
          fft_SPxSP_mixed_radix_c2c (N3, in1, tw, out1, N_p2, s_r3, s_r5);
      }
    } else
    {
      fft_SPxSP_c2c_bs (N3, M, rad, in1, out1, tw, Bn_w, workbuf_bn);
    }
#endif

    /* export output data of first FFT, transposed */
    lib_emt_copy1D2DLarge(
      edmall2->channel[FFT_XFER_CHANNEL1],
      (void *)out1,
      (void *)out_ptr,
      2*sizeof(float)*num_of_lines,
      outsample,
      linesize);
    out_ptr += 2*num_of_lines;
    
    /* prepare third FFT input buffer */
    lib_emt_copy2D1DLarge(
      edmall2->channel[FFT_XFER_CHANNEL0],
      (void *)in_ptr,
      (void *)in_w,
      2*sizeof(float)*num_of_lines,
      outsample,
      linesize);
    in_ptr += 2*num_of_lines;

    /* set channel numbers */
    ch_i  = FFT_XFER_CHANNEL2;
    ch_o  = FFT_XFER_CHANNEL3;
    ch_i1 = FFT_XFER_CHANNEL0;
    ch_o1 = FFT_XFER_CHANNEL1;

    /* wait for EDMA */
    lib_emt_wait(edmall2->channel[ch_i]);

    for (j = num_of_lines; j < lines; j += num_of_lines)
    {
#if (FFT_OMP_SP_3D_C2C_NUMOFLINEBUFS>1)
      /* local transpose */
      DSPF_sp_mat_trans_cplx(input, N3, num_of_lines, output);

      if (N3 == M) {
        if (s_r3 == 0 && s_r5 == 0) {
          for (i=0; i<num_of_lines; i++)
            fft_sp_cmplx_notwid_br((double *)&output[i*N3*2], N3, (double *)&tempout[i*N3*2], (double *)tw);
        } 
        else {
          for (i=0; i<num_of_lines; i++)
            fft_SPxSP_mixed_radix_c2c (N3, &output[i*N3*2], tw, &tempout[i*N3*2], N_p2, s_r3, s_r5);
        }
      } 
      else {
        for (i=0; i<num_of_lines; i++)
          fft_SPxSP_c2c_bs (N3, M, rad, &input[i*N3*2], &tempout[i*N3*2], tw, Bn_w, workbuf_bn);
      }

      /* local transpose from tempout to output */
      DSPF_sp_mat_trans_cplx(tempout, num_of_lines, N3, output);
#else
      if (N3 == M) {
        if (s_r3 == 0 && s_r5 == 0) {
          fft_sp_cmplx_notwid_br((float *)input, N3, (float *)output, (float *)tw);
        } else
        {
          fft_SPxSP_mixed_radix_c2c (N3, input, tw, output, N_p2, s_r3, s_r5);
        }
      } else
      {
        fft_SPxSP_c2c_bs (N3, M, rad, input, output, tw, Bn_w, workbuf_bn);
      }
#endif

      /* export output data, transposed */
      lib_emt_copy1D2DLarge(
        edmall2->channel[ch_o],
        (void *)output,
        (void *)out_ptr,
        2*sizeof(float)*num_of_lines,
        outsample,
        linesize);
      out_ptr += 2*num_of_lines;

      /* prepare input data */
      lib_emt_copy2D1DLarge(
        edmall2->channel[ch_i],
        (void *)in_ptr,
        (void *)input,
        2*sizeof(float)*num_of_lines,
        outsample,
        linesize);
      in_ptr += 2*num_of_lines;

      /* swap pingpong buffer pointers and XFER channel */
      dum2   = input;
      input  = in1;
      in1    = dum2;
      dum3   = output;
      output = out1;
      out1   = dum3;

      dum0   = ch_i;
      ch_i   = ch_i1;
      ch_i1  = dum0;
      dum1   = ch_o;
      ch_o   = ch_o1;
      ch_o1  = dum1;

      /* wait for EDMA */
      lib_emt_wait(edmall2->channel[ch_i]);
      lib_emt_wait(edmall2->channel[ch_o]);
    }

    /* wait for all ch to complete */
    lib_emt_wait(edmall2->channel[ch_i1]);
    lib_emt_wait(edmall2->channel[ch_o1]);

    return error;
} /* fft_omp_sp_1d_c2c_batch_ecpy_transpose_3d */

static float *inLocal, *outLocal, *work1Local, *work2Local, *twLocal, *in_wLocal, *out_wLocal, *out_tLocal, *tw_wLocal;
static int n1Local, n2Local, n3Local, planesLocal;
static FFT_EDMA_Struct *edmaLocal;
#ifndef FFT_MEM_MODEL_SM
#pragma DATA_SECTION (inLocal, ".mem_msm")
#pragma DATA_SECTION (outLocal, ".mem_msm")
#pragma DATA_SECTION (work1Local, ".mem_msm")
#pragma DATA_SECTION (work2Local, ".mem_msm")
#pragma DATA_SECTION (twLocal, ".mem_msm")
#pragma DATA_SECTION (in_wLocal, ".mem_msm")
#pragma DATA_SECTION (out_wLocal, ".mem_msm")
#pragma DATA_SECTION (tw_wLocal, ".mem_msm")
#pragma DATA_SECTION (n1Local, ".mem_msm")
#pragma DATA_SECTION (n2Local, ".mem_msm")
#pragma DATA_SECTION (n3Local, ".mem_msm")
#pragma DATA_SECTION (planesLocal, ".mem_msm")
#pragma DATA_SECTION (edmaLocal, ".mem_msm")
#endif

int fft_omp_sp_3d_c2c_ecpy (
    /* int   N1;       FFT size         */
    /* int   N2;       FFT size         */
    /* int   N3;       FFT size         */
    /* float *in;      input pointer    */
    /* float *out;     output pointer   */
    /* float *in_w;    input pointer    */
    /* float *out_w;   output pointer   */
    /* float *tw;      twiddle pointer  */
    fft_param_u u,
    void *edmaState,
    int   actualCoreNum
)
{
  int   error = FFT_NOERR;
  int   numThread, tid, did, numLine, twsizeMax;

  int   N                = u.sp_3d_c2c_e.N;               /* maximum of N1, N2, N3           */
  int   M1Local          = u.sp_3d_c2c_e.para1.M;         /* first dimension FFT size M1     */
  int   M2Local          = u.sp_3d_c2c_e.para2.M;         /* second dimension FFT size M2    */
  int   M3Local          = u.sp_3d_c2c_e.para3.M;         /* third dimension FFT size M3     */
  int   radLocal         = u.sp_3d_c2c_e.para1.rad;       /* radix for N1                    */

  int   twsizeLocal      = u.sp_3d_c2c_e.para1.twsize;    /* twiddle size in double words    */
  int   N_p2Local        = u.sp_3d_c2c_e.para1.N_p2;      /* power of 2 size in mixed radix  */
  int   s_r3Local        = u.sp_3d_c2c_e.para1.s_r3;      /* number stages of radix 3        */
  int   s_r5Local        = u.sp_3d_c2c_e.para1.s_r5;      /* number stages of radix 5        */
  int   BnsizeLocal      = u.sp_3d_c2c_e.para1.Bnsize;
  int   tw_offsetLocal, Bn_offsetLocal;
  float *BnLocal         = u.sp_3d_c2c_e.Bn;
  float *Bn_wLocal       = NULL;
  float *workbuf_bnLocal = NULL;
  float *tw_aLocal, *Bn_aLocal;
  lib_memdscr_t **fft_mem_handle = fftGetMemHandle();

  /* max of the 3 dimension */
  twsizeMax = (u.sp_3d_c2c_e.para1.twsize > u.sp_3d_c2c_e.para2.twsize) ? u.sp_3d_c2c_e.para1.twsize : u.sp_3d_c2c_e.para2.twsize;
  twsizeMax = (u.sp_3d_c2c_e.para3.twsize > twsizeMax) ? u.sp_3d_c2c_e.para3.twsize : twsizeMax;

  n1Local     = u.sp_3d_c2c_e.N1;         /* signal size              */
  n2Local     = u.sp_3d_c2c_e.N2;         /* signal size              */
  n3Local     = u.sp_3d_c2c_e.N3;         /* signal size              */

  inLocal     = u.sp_3d_c2c_e.in;         /* input pointer            */
  outLocal    = u.sp_3d_c2c_e.out;        /* output pointer           */
  work1Local  = u.sp_3d_c2c_e.tw;         /* working buffer pointer   */
  work2Local  = u.sp_3d_c2c_e.tw          /* working buffer pointer   */
              + 2*n1Local*n2Local*n3Local;
  twLocal     = u.sp_3d_c2c_e.tw
              + 4*n1Local*n2Local*n3Local;/* twiddle buffer pointer   */
  in_wLocal   = (float*)lib_smem_falloc (fft_mem_handle, 4*N*FFT_OMP_SP_3D_C2C_NUMOFLINEBUFS*sizeof(float), 8);
  out_wLocal  = (float*)lib_smem_falloc (fft_mem_handle, 4*N*FFT_OMP_SP_3D_C2C_NUMOFLINEBUFS*sizeof(float), 8);
  out_tLocal  = (float*)lib_smem_falloc (fft_mem_handle, 2*N*FFT_OMP_SP_3D_C2C_NUMOFLINEBUFS*sizeof(float), 8);
#ifdef FFT_MEM_MODEL_LG
    tw_wLocal   = (float*)lib_smem_falloc (fft_mem_handle, twsizeMax*sizeof(float), 8);
#endif

#ifdef FFT_MEM_MODEL_LG
    if(in_wLocal==NULL || out_wLocal==NULL || out_tLocal==NULL || tw_wLocal==NULL) {
#else
    tw_wLocal = NULL;
    if(in_wLocal==NULL || out_wLocal==NULL || out_tLocal==NULL) {
#endif
        printf("Memory allocation error!\n");
        return FFT_ERROR;
    }

  if ((actualCoreNum==1)||
      (actualCoreNum==2)||
      (actualCoreNum==4)||
      (actualCoreNum==8))
    omp_set_num_threads (actualCoreNum);
  else
    omp_set_num_threads (OMP_MAX_NUM_CORES);
  numThread = omp_get_max_threads();
  edmaLocal = (FFT_EDMA_Struct *)edmaState;

  // convert float to byte
  twsizeLocal = twsizeLocal*sizeof(float);
  BnsizeLocal = BnsizeLocal*sizeof(float);

  planesLocal = n3Local/numThread;
#pragma omp parallel private(tid, did, numLine, tw_aLocal, Bn_aLocal) shared(inLocal, work1Local, \
      twLocal, in_wLocal, out_wLocal, out_tLocal, tw_wLocal, n1Local, n2Local, n3Local, planesLocal, \
      edmaLocal, BnLocal, Bn_wLocal, workbuf_bnLocal, M1Local, M2Local, M3Local, radLocal, twsizeLocal, \
      N_p2Local, s_r3Local, s_r5Local, BnsizeLocal, tw_offsetLocal, Bn_offsetLocal)
  {
    tid = omp_get_thread_num();     // Obtain thread number
    did = DNUM;
    numLine = FFT_OMP_SP_3D_C2C_NUMOFLINES_SM;

    if (tw_wLocal != NULL) {
      /* prepare twiddle buffer */
      lib_emt_copy1D1D(
      (edmaLocal+did)->channel[FFT_XFER_CHANNEL3],
      (void *)twLocal,
      (void *)tw_wLocal,
      twsizeLocal);
      tw_aLocal = tw_wLocal;
    } else
    {
      tw_aLocal = twLocal;
    }

    if (Bn_wLocal != NULL) {
      /* prepare Bn buffer */
      lib_emt_copy1D1D(
        (edmaLocal+did)->channel[FFT_XFER_CHANNEL2],
        (void *)BnLocal,
        (void *)Bn_wLocal,
        BnsizeLocal);
      Bn_aLocal = Bn_wLocal;
    } else {
      Bn_aLocal = BnLocal;
    }

    if (tw_wLocal != NULL)
      lib_emt_wait((edmaLocal+did)->channel[FFT_XFER_CHANNEL3]);
    if (Bn_wLocal != NULL)
      lib_emt_wait((edmaLocal+did)->channel[FFT_XFER_CHANNEL2]);

    /* 1st dimension fft, 8 lines */
    error = fft_omp_sp_1d_c2c_batch_ecpy_transpose (
        n1Local,
        n2Local,
        planesLocal,
        M1Local,
        radLocal,
        N_p2Local,
        s_r3Local,
        s_r5Local,
        &inLocal[tid*(n1Local*2*n2Local*planesLocal)],
        &work1Local[tid*(n1Local*2*n2Local*planesLocal)],
        in_wLocal,
        out_wLocal,
        out_tLocal,
        tw_aLocal,
        Bn_aLocal,
        workbuf_bnLocal,
        edmaLocal+did,
        numLine);
  }

  /* prepare for the 2nd dimension */
  twsizeLocal     = u.sp_3d_c2c_e.para2.twsize;
  BnsizeLocal     = u.sp_3d_c2c_e.para2.Bnsize;
  N_p2Local       = u.sp_3d_c2c_e.para2.N_p2;     /* power of 2 size in mixed radix */
  s_r3Local       = u.sp_3d_c2c_e.para2.s_r3;     /* number stages of radix 3       */
  s_r5Local       = u.sp_3d_c2c_e.para2.s_r5;     /* number stages of radix 5       */
  radLocal        = u.sp_3d_c2c_e.para2.rad;
  tw_offsetLocal  = u.sp_3d_c2c_e.para1.twsize;
  Bn_offsetLocal  = u.sp_3d_c2c_e.para1.Bnsize;

  // convert float to byte
  twsizeLocal = twsizeLocal*sizeof(float);
  BnsizeLocal = BnsizeLocal*sizeof(float);

  planesLocal = n3Local/numThread;
#pragma omp parallel private(tid, did, numLine, tw_aLocal, Bn_aLocal) shared(work1Local, work2Local, \
      twLocal, in_wLocal, out_wLocal, out_tLocal, tw_wLocal, n1Local, n2Local, n3Local, planesLocal, \
      edmaLocal, BnLocal, Bn_wLocal, workbuf_bnLocal, M1Local, M2Local, M3Local, radLocal, twsizeLocal, \
      N_p2Local, s_r3Local, s_r5Local, BnsizeLocal, tw_offsetLocal, Bn_offsetLocal)
  {
    tid = omp_get_thread_num();     // Obtain thread number
    did = DNUM;
    numLine = FFT_OMP_SP_3D_C2C_NUMOFLINES_SM;

    if (tw_wLocal != NULL) {
      /* prepare twiddle buffer */
      lib_emt_copy1D1D(
        (edmaLocal+did)->channel[FFT_XFER_CHANNEL3],
        (void *)(twLocal+tw_offsetLocal),
        (void *)tw_wLocal,
        twsizeLocal);
      tw_aLocal = tw_wLocal;
    }
    else {
      tw_aLocal = twLocal + tw_offsetLocal;
    }

    if (Bn_wLocal != NULL) {
      /* prepare Bn buffer when doing bluestein */
      lib_emt_copy1D1D(
        (edmaLocal+did)->channel[FFT_XFER_CHANNEL2],
        (void *)(BnLocal+Bn_offsetLocal),
        (void *)Bn_wLocal,
        BnsizeLocal);
      Bn_aLocal = Bn_wLocal;
    }
    else {
      Bn_aLocal = BnLocal + Bn_offsetLocal;
    }
    if (tw_wLocal != NULL)
      lib_emt_wait((edmaLocal+did)->channel[FFT_XFER_CHANNEL3]);
    if (Bn_wLocal != NULL)
      lib_emt_wait((edmaLocal+did)->channel[FFT_XFER_CHANNEL2]);

    /* 2nd dimension fft, 8 lines */
    error = fft_omp_sp_1d_c2c_batch_ecpy_transpose (
        n2Local,
        n1Local,
        planesLocal,
        M2Local,
        radLocal,
        N_p2Local,
        s_r3Local,
        s_r5Local,
        &work1Local[tid*(n2Local*2*n1Local*planesLocal)],
        &work2Local[tid*(n1Local*2*n2Local*planesLocal)],
        out_wLocal,
        in_wLocal,
        out_tLocal,
        tw_aLocal,
        Bn_aLocal,
        workbuf_bnLocal,
        edmaLocal+did,
        numLine);
  }

  twsizeLocal = u.sp_3d_c2c_e.para3.twsize;
  BnsizeLocal = u.sp_3d_c2c_e.para3.Bnsize;
  N_p2Local   = u.sp_3d_c2c_e.para3.N_p2;    /* power of 2 size in mixed radix  */
  s_r3Local   = u.sp_3d_c2c_e.para3.s_r3;    /* number stages of radix 3        */
  s_r5Local   = u.sp_3d_c2c_e.para3.s_r5;    /* number stages of radix 5        */
  radLocal    = u.sp_3d_c2c_e.para3.rad;
  tw_offsetLocal = u.sp_3d_c2c_e.para1.twsize + u.sp_3d_c2c_e.para2.twsize;
  Bn_offsetLocal = u.sp_3d_c2c_e.para1.Bnsize + u.sp_3d_c2c_e.para2.Bnsize;

  // convert float to byte
  twsizeLocal = twsizeLocal*sizeof(float);
  BnsizeLocal = BnsizeLocal*sizeof(float);

  planesLocal = n1Local*n2Local/numThread;
#pragma omp parallel private(tid, did, numLine, tw_aLocal, Bn_aLocal) shared(work2Local, outLocal, \
      twLocal, in_wLocal, out_wLocal, out_tLocal, tw_wLocal, n1Local, n2Local, n3Local, planesLocal, \
      edmaLocal, BnLocal, Bn_wLocal, workbuf_bnLocal,  M1Local, M2Local, M3Local, radLocal, twsizeLocal, \
      N_p2Local, s_r3Local, s_r5Local, BnsizeLocal, tw_offsetLocal, Bn_offsetLocal)
  {
    tid = omp_get_thread_num();     // Obtain thread number
    did = DNUM;
    numLine = FFT_OMP_SP_3D_C2C_NUMOFLINEBUFS;

    if (tw_wLocal != NULL) {
      /* prepare twiddle buffer */
      lib_emt_copy1D1D(
        (edmaLocal+did)->channel[FFT_XFER_CHANNEL3],
        (void *)(twLocal+tw_offsetLocal),
        (void *)tw_wLocal,
        twsizeLocal);
      tw_aLocal = tw_wLocal;
    }
    else {
      tw_aLocal = twLocal + tw_offsetLocal;
    }

    if (Bn_wLocal != NULL) {
      /* prepare Bn buffer when doing bluestein */
      lib_emt_copy1D1D(
        (edmaLocal+did)->channel[FFT_XFER_CHANNEL2],
        (void *)(BnLocal+Bn_offsetLocal),
        (void *)Bn_wLocal,
        BnsizeLocal);
      Bn_aLocal = Bn_wLocal;
    }
    else {
      Bn_aLocal = BnLocal + Bn_offsetLocal;
    }
    if (tw_wLocal != NULL)
      lib_emt_wait((edmaLocal+did)->channel[FFT_XFER_CHANNEL3]);
    if (Bn_wLocal != NULL)
      lib_emt_wait((edmaLocal+did)->channel[FFT_XFER_CHANNEL2]);

    /* 3rd dimension fft */
    error = fft_omp_sp_1d_c2c_batch_ecpy_transpose_3d (
        n1Local*n2Local,
        n3Local,
        planesLocal,
        M3Local,
        radLocal,
        N_p2Local,
        s_r3Local,
        s_r5Local,
        &work2Local[tid*2*planesLocal],
        &outLocal[tid*2*planesLocal],
        in_wLocal,
        out_wLocal,
        out_tLocal,
        tw_aLocal,
        Bn_aLocal,
        workbuf_bnLocal,
        edmaLocal+did,
        numLine);
  }

  omp_set_num_threads (OMP_MAX_NUM_CORES);

  return error;
} /* fft_omp_sp_3d_c2c_ecpy */

/* ======================================================================== */
/*  End of file:  fft_omp_sp_3d_c2c.c                                       */
/* ------------------------------------------------------------------------ */
/*            Copyright (c) 2013 Texas Instruments, Incorporated.           */
/*                           All Rights Reserved.                           */
/* ======================================================================== */
