/* ======================================================================= */
/*  TEXAS INSTRUMENTS, INC.                                                */
/*                                                                         */
/*  FFTLIB  FFT Library                                                    */
/*                                                                         */
/* Copyright (C) 2013 Texas Instruments Incorporated - http://www.ti.com/  */
/*                                                                         */
/*                                                                         */
/*  Redistribution and use in source and binary forms, with or without     */
/*  modification, are permitted provided that the following conditions     */
/*  are met:                                                               */
/*                                                                         */
/*    Redistributions of source code must retain the above copyright       */
/*    notice, this list of conditions and the following disclaimer.        */
/*                                                                         */
/*    Redistributions in binary form must reproduce the above copyright    */
/*    notice, this list of conditions and the following disclaimer in the  */
/*    documentation and/or other materials provided with the               */
/*    distribution.                                                        */
/*                                                                         */
/*    Neither the name of Texas Instruments Incorporated nor the names of  */
/*    its contributors may be used to endorse or promote products derived  */
/*    from this software without specific prior written permission.        */
/*                                                                         */
/*  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS    */
/*  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT      */
/*  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR  */
/*  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT   */
/*  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,  */
/*  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT       */
/*  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,  */
/*  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY  */
/*  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT    */
/*  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE  */
/*  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.   */
/*                                                                         */
/* ======================================================================= */

#include <xdc/std.h>
#include "c6x.h"
#include "stdlib.h"
#include <stdio.h>
#include <ti/fftlib/src/common/omp/omp_config.h>
#include "fft_omp_dp_3d_r2c.h"
#include <ti/fftlib/src/common/fft/fft_dp_mixed_bs.h>
#include <ti/dsplib/src/DSPF_dp_fftDPxDP/DSPF_dp_fftDPxDP.h>
#include <ti/fftlib/src/common/fft_edma.h>
#include <ti/runtime/openmp/omp.h>
#include <ti/libarch/libarch.h>

static void DSPF_dp_mat_trans_cplx(const double *restrict x, const int rows,
  const int cols, double *restrict y)
{
  int i, j = 0;

  #pragma MUST_ITERATE(2,,)
  for(i = 0; i < rows; i++)
  #pragma MUST_ITERATE(2,,)
  for(j = 0; j < cols; j++)
  {
    y[j*rows*2 + i*2] = x[j*2 + cols*i*2];
    y[j*rows*2 + i*2 + 1] = x[1 + j*2 + cols*i*2];
  }
}

static int fft_omp_dp_1d_r2c_ecpy_transpose (
    int    dim,               /* dimension of FFT               */
    int    N1,                /* FFT size                       */
    int    N2,                /* number of lines                */
    int    N3,                /* number of planes               */
    int    M,                 /* FFT size                       */
    int    rad,               /* radix for N1                   */
    int    N_p2,              /* power of 2 size in mixed radix */
    int    s_r3,              /* number stages of radix 3       */
    int    s_r5,              /* number stages of radix 5       */
    double *in,               /* input pointer                  */
    double *out,              /* output pointer                 */
    double *in_w,             /* input work buffer pointer      */
    double *out_w,            /* output working buffer pointer  */
    double *out_t,            /* temp working buffer pointer    */
    double *tw,               /* twiddle pointer                */
    double *Bn,               /* Bn pointer                     */
    double *workbuf,          /* workbuf pointer for BS         */
    FFT_EDMA_Struct  *edma,   /* edma resource pointer          */
    int num_of_lines          /* number of lines for the 1D FFT */
)
{
    int   error = FFT_NOERR, linecnt = 0, line_total;
    int   i, j, k, ch_i,ch_o,ch_i1,ch_o1,dum0,dum1, buffer_shift_flag = 0;
    int   insize, instep,
          outsample,
          linesize, linewrap;
    FFT_EDMA_Struct  *edmall2;

    double *restrict in_ptr = in, *restrict out_ptr = out, *restrict tempout, *restrict in_ptr_b = in;
    double *restrict input, *restrict output, *restrict in1, *restrict out1, *dum2, *dum3;

    /* if linesize is bigger than the 16 bit limit, use cpu to do the global transpose for 3rd dimension for now */

  edmall2 = edma;

    /* parameters used for output EDMA */
    if (dim == 1)
    {
        linesize   = N2*2*sizeof(double);
        linewrap   = 2*((N1/2+1)-1)*N2;
        insize     = N1*sizeof(double)*num_of_lines;
        instep     = N1*num_of_lines;
        outsample  = N1/2+1;
    }
    else
    {
        linesize   = N2*2*sizeof(double);
        linewrap   = 2*(N1-1)*N2;
        insize     = 2*N1*sizeof(double)*num_of_lines;
        instep     = 2*N1*num_of_lines;
        outsample  = N1;
    }


    
    /* prepare first FFT input buffer */
    lib_emt_copy1D1D(
      edmall2->channel[FFT_XFER_CHANNEL0],
      (void *)in_ptr,
      (void *)in_w,
      insize);
    in_ptr += instep;

    /* set pingpong buffer pointers */
    in1    = in_w;
    out1   = out_w;
    input  = &in_w[2*N1*num_of_lines];
    output = &out_w[2*N1*num_of_lines];
    tempout= out_t;

    /* prepare second FFT input buffer */
    lib_emt_copy1D1D(
      edmall2->channel[FFT_XFER_CHANNEL2],
      (void *)in_ptr,
      (void *)input,
      insize);
    in_ptr += instep;

    /* wait for first FFT input */
    lib_emt_wait(edmall2->channel[FFT_XFER_CHANNEL0]);
    /* call c2c fft */
#if (FFT_OMP_DP_3D_R2C_NUMOFLINEBUFS>1)
    if (dim == 1){
      /* call r2c fft for the 1st dimension */
      if (N1 == M) {
        if (s_r3 == 0 && s_r5 == 0) {
          for (i=0; i<num_of_lines; i++)
            DSPF_dp_fftDPxDP_r2c (N1, &in1[i*N1], tw, &tempout[i*N1*2], rad, 0, N1);
        }
        else {
          for (i=0; i<num_of_lines; i++)
            fft_DPxDP_mixed_radix_r2c (N1,  &in1[i*N1], tw, &tempout[i*N1*2], N_p2, s_r3, s_r5, rad);
        }
      }
      else{
        for (i=0; i<num_of_lines; i++)
          fft_DPxDP_r2c_bs (N1, M, rad, &in1[i*N1], &tempout[i*N1*2], tw, Bn, workbuf);
      }
    }
    else{
      /* call c2c fft for other dimensions */
      if (N1 == M) {
        if (s_r3 == 0 && s_r5 == 0) {
          for (i=0; i<num_of_lines; i++)
          DSPF_dp_fftDPxDP (N1, &in1[i*N1*2], tw, &tempout[i*N1*2], rad, 0, N1);
        }
        else {
          for (i=0; i<num_of_lines; i++)
            fft_DPxDP_mixed_radix (N1, &in1[i*N1*2], tw, &tempout[i*N1*2], N_p2, s_r3, s_r5, rad);
        }
      } /* if (N1 == M) */
      else{
        for (i=0; i<num_of_lines; i++)
          fft_DPxDP_bs (N1, M, rad, &in1[i*N1*2], &tempout[i*N1*2], tw, Bn, workbuf);
      }
    } /* if (dim == 1) */

    /* local transpose from tempout to out1 */
    DSPF_dp_mat_trans_cplx(tempout, num_of_lines, N1, out1);
#else
    if (dim == 1){
      /* call r2c fft */
      if (N1 == M) {
        if (s_r3 == 0 && s_r5 == 0) {
          DSPF_dp_fftDPxDP_r2c (N1, in1, tw, out1, rad, 0, N1);
        }
        else {
          fft_DPxDP_mixed_radix_r2c (N1, in1, tw, out1, N_p2, s_r3, s_r5, rad);
        }
      }
      else{
        fft_DPxDP_r2c_bs (N1, M, rad, in1, out1, tw, Bn, workbuf);
      }
    }
    else{
      /* call c2c fft */
      if (N1 == M) {
        if (s_r3 == 0 && s_r5 == 0) {
          DSPF_dp_fftDPxDP (N1, in1, tw, out1, rad, 0, N1);
        }
        else {
          fft_DPxDP_mixed_radix (N1, in1, tw, out1, N_p2, s_r3, s_r5, rad);
        }
      }
      else{
        fft_DPxDP_bs (N1, M, rad, in1, out1, tw, Bn, workbuf);
      }
    }
#endif
    /* export output data of first FFT*/
      lib_emt_copy1D2D(
      edmall2->channel[FFT_XFER_CHANNEL1],
      (void *)out1,
      (void *)out_ptr,
      2*sizeof(double)*num_of_lines,
      outsample,
      linesize);
      
      out_ptr += 2*num_of_lines;
      linecnt += num_of_lines;
#ifdef USEFASTEDMA
  /* fill the PaRAM for CHANNEL 3 without starting it */
  lib_emt_copy1D2D_Fill(
    edmall2->channel[FFT_XFER_CHANNEL3],
    (void *)out1,
    (void *)out_ptr,
    2*sizeof(double)*num_of_lines,
    outsample,
    linesize);
#endif

    /* prepare third FFT input buffer */
#ifndef USEFASTEDMA
    lib_emt_copy1D1D(
      edmall2->channel[FFT_XFER_CHANNEL0],
      (void *)in_ptr,
      (void *)in_w,
      insize);
#else
  lib_emt_copy1D1D_Fast(
    edmall2->channel[FFT_XFER_CHANNEL0],
    (void *)in_ptr,
    (void *)in_w);
#endif
    in_ptr += instep;

    /* set channel numbers */
    ch_i  = FFT_XFER_CHANNEL2;
    ch_o  = FFT_XFER_CHANNEL3;
    ch_i1 = FFT_XFER_CHANNEL0;
    ch_o1 = FFT_XFER_CHANNEL1;

    /* wait for EDMA */
    lib_emt_wait(edmall2->channel[ch_i]);

    if(dim == 1)
      line_total = (N3*N2)/num_of_lines;
    else
      line_total = (N3*(N2-1))/num_of_lines;

    for (j = 1; j < line_total; j++)
    {
#if (FFT_OMP_DP_3D_R2C_NUMOFLINEBUFS>1)
      if (dim == 1){
      /* call r2c fft for the 1st dimension */
        if (N1 == M) {
          if (s_r3 == 0 && s_r5 == 0) {
            for (i=0; i<num_of_lines; i++)
              DSPF_dp_fftDPxDP_r2c (N1, &input[i*N1], tw, &tempout[i*N1*2], rad, 0, N1);
          }
          else {
            for (i=0; i<num_of_lines; i++)
              fft_DPxDP_mixed_radix_r2c (N1, &input[i*N1], tw, &tempout[i*N1*2], N_p2, s_r3, s_r5, rad);
          }
        } /* if (N1 == M)*/
        else{
          for (i=0; i<num_of_lines; i++)
            fft_DPxDP_r2c_bs (N1, M, rad, &input[i*N1], &tempout[i*N1*2], tw, Bn, workbuf);
        }
      }
      else{
      /* call c2c fft for other dimensions */
        if (N1 == M) {
          if (s_r3 == 0 && s_r5 == 0) {
            for (i=0; i<num_of_lines; i++)
              DSPF_dp_fftDPxDP (N1, &input[i*N1*2], tw, &tempout[i*N1*2], rad, 0, N1);
          }
          else {
            for (i=0; i<num_of_lines; i++)
              fft_DPxDP_mixed_radix (N1, &input[i*N1*2], tw, &tempout[i*N1*2], N_p2, s_r3, s_r5, rad);
          }
        } /* if (N1 == M) */
        else{
          for (i=0; i<num_of_lines; i++)
            fft_DPxDP_bs (N1, M, rad, &input[i*N1*2], &tempout[i*N1*2], tw, Bn, workbuf);
        }
      }/* if (dim == 1) */

    /* local transpose from tempout to output */
    DSPF_dp_mat_trans_cplx(tempout, num_of_lines, N1, output);
#else
      if (dim == 1){
      /* call r2c fft for the 1st dimension */
        if (N1 == M) {
          if (s_r3 == 0 && s_r5 == 0) {
            DSPF_dp_fftDPxDP_r2c (N1, input, tw, output, rad, 0, N1);
          }
          else
            fft_DPxDP_mixed_radix_r2c (N1, input, tw, output, N_p2, s_r3, s_r5, rad);
        } /* if (N1 == M)*/
        else
          fft_DPxDP_r2c_bs (N1, M, rad, input, output, tw, Bn, workbuf);
      } /* if (dim == 1) */
      else{
        if (N1 == M) {
          if (s_r3 == 0 && s_r5 == 0) {
            DSPF_dp_fftDPxDP (N1, input, tw, output, rad, 0, N1);
          }
          else
            fft_DPxDP_mixed_radix (N1, input, tw, output, N_p2, s_r3, s_r5, rad);
        } /* if (N1 == M)*/
        else
          fft_DPxDP_bs (N1, M, rad, input, output, tw, Bn, workbuf);
      } /* else (dim == 1) */
#endif

      /* export output data*/
#ifndef USEFASTEDMA
        lib_emt_copy1D2D(
        edmall2->channel[ch_o],
        (void *)output,
        (void *)out_ptr,
        2*sizeof(double)*num_of_lines,
        outsample,
        linesize);
#else
      lib_emt_copy1D2D_Fast
      (
        edmall2->channel[ch_o],
          (void *)output,
          (void *)out_ptr);
#endif
      out_ptr += 2*num_of_lines;
      linecnt += num_of_lines;
    
      /* re-adjust output pointer when it's time for the next batch */
      if (linecnt >= N2-1) {
#if 1
        if (dim != 1){
          /* prepare the FFT input buffer */
        lib_emt_wait(edmall2->channel[ch_o]);
          /* process data */
          if (N1 == M) {
            if (s_r3 == 0 && s_r5 == 0) {
              DSPF_dp_fftDPxDP (N1, in1, tw, output, rad, 0, N1);
            }
            else
              fft_DPxDP_mixed_radix (N1, in1, tw, output, N_p2, s_r3, s_r5, rad);
          } /* if (N1 == M)*/
          else
            fft_DPxDP_bs (N1, M, rad, in1, output, tw, Bn, workbuf);

          /* transport out */
          for (i = 0; i < outsample; i++) {
            out_ptr[2*i*N2]   = output[2*i];
            out_ptr[2*i*N2+1] = output[2*i+1];
          }

          in_ptr_b = in_ptr;
          in_ptr  += 2*N1;
          out_ptr += 2;
          buffer_shift_flag = 1;
        }
#endif
        linecnt = 0;
        out_ptr += linewrap;
      }

      /* prepare input data */
#ifndef USEFASTEDMA
      lib_emt_copy1D1D(
        edmall2->channel[ch_i],
        (void *)in_ptr,
        (void *)input,
        insize);

#else
      lib_emt_copy1D1D_Fast(
        edmall2->channel[ch_i],
        (void *)in_ptr,
        (void *)input);
#endif
      in_ptr += instep;

      /* swap pingpong buffer pointers and XFER channel */
      dum2   = input;
      input  = in1;
      in1    = dum2;
      dum3   = output;
      output = out1;
      out1   = dum3;

      dum0   = ch_i;
      ch_i   = ch_i1;
      ch_i1  = dum0;
      dum1   = ch_o;
      ch_o   = ch_o1;
      ch_o1  = dum1;

      /* wait for EDMA */
      lib_emt_wait(edmall2->channel[ch_i]);
      if(buffer_shift_flag == 1){
        /* shift input buffer since the first line has already been processed */
      for (i = 0; i < num_of_lines-1; i++) {
        for(k = 0; k < 2*N1; k++)
          input[2*N1*i+k] = input[2*N1*(i+1)+k];
      }
      /* then get the last line by copying it */
        for(i = 0; i < 2*N1; i++)
          input[2*N1*(num_of_lines-1)+i] = in_ptr_b[i];
        buffer_shift_flag = 0;
      }

      lib_emt_wait(edmall2->channel[ch_o]);
    }

    lib_emt_wait(edmall2->channel[ch_i1]);
    lib_emt_wait(edmall2->channel[ch_o1]);

    return error;
} /* fft_omp_dp_1d_r2c_ecpy_transpose */

static int fft_omp_dp_1d_c2c_batch_ecpy_transpose_3d (
  int    N1,                /* total number of lines          */
  int    N3,                /* number of planes               */
  int    lines,             /* lines to execute               */
  int    M,                 /* Padded size of Anysize FFT     */
  int    rad,               /* radix                          */
  int    N_p2,              /* power of 2 size in mixed radix */
  int    s_r3,              /* number stages of radix 3       */
  int    s_r5,              /* number stages of radix 5       */
  double *in,               /* input pointer                  */
  double *out,              /* output pointer                 */
  double *in_w,             /* input work buffer pointer      */
  double *out_w,            /* output working buffer pointer  */
  double *out_t,            /* temp working buffer pointer    */
  double *tw,               /* twiddle pointer                */
  double *Bn,
  double *workbuf,
  FFT_EDMA_Struct  *edma,  /* edma resource pointer          */
  int num_of_lines         /* number of lines for the 1D FFT */
)
{
    int   error = FFT_NOERR;
    int   i,j,ch_i,ch_o,ch_i1,ch_o1,dum0,dum1;
    int   outsample = N3, linesize = 2*N1*sizeof(double);
    FFT_EDMA_Struct  *edmall2 = edma;

    double *restrict in_ptr = in, *restrict out_ptr = out, *restrict tempout;
    double *restrict input, *restrict output, *restrict in1, *restrict out1, *dum2, *dum3;

    /* prepare first FFT input buffer */
    lib_emt_copy2D1DLarge(
      edmall2->channel[FFT_XFER_CHANNEL0],
      (void *)in_ptr,
      (void *)in_w,
      2*sizeof(double)*num_of_lines,
      outsample,
      linesize);
    in_ptr += 2*num_of_lines;

    /* set pingpong buffer pointers */
    in1    = in_w;
    out1   = out_w;
    input  = &in_w[2*N3*num_of_lines];
    output = &out_w[2*N3*num_of_lines];
    tempout= out_t;

    /* prepare second FFT input buffer */
    lib_emt_copy2D1DLarge(
      edmall2->channel[FFT_XFER_CHANNEL2],
      (void *)in_ptr,
      (void *)input,
      2*sizeof(double)*num_of_lines,
      outsample,
      linesize);
    in_ptr += 2*num_of_lines;

    lib_emt_wait(edmall2->channel[FFT_XFER_CHANNEL0]);

    /* Calculating the FFT */
#if (FFT_OMP_DP_3D_R2C_NUMOFLINEBUFS>1)
    /* local transpose */
    DSPF_dp_mat_trans_cplx(in1, N3, num_of_lines, out1);


    if (N3 == M) {
      if (s_r3 == 0 && s_r5 == 0) {
        for (i=0; i<num_of_lines; i++)
          DSPF_dp_fftDPxDP (N3, &out1[i*N3*2], tw, &tempout[i*N3*2], rad, 0, N3);
      }
      else {
        for (i=0; i<num_of_lines; i++)
          fft_DPxDP_mixed_radix (N3, &out1[i*N3*2], tw, &tempout[i*N3*2], N_p2, s_r3, s_r5, rad);
      }
    }
    else{
      for (i=0; i<num_of_lines; i++)
        fft_DPxDP_bs (N3, M, rad, &out1[i*N3*2], &tempout[i*N3*2], tw, Bn, workbuf);
    }

    DSPF_dp_mat_trans_cplx(tempout, num_of_lines, N3, out1);
#else
    if (N3 == M) {
      if (s_r3 == 0 && s_r5 == 0) {
        DSPF_dp_fftDPxDP (N3, in1, tw, out1, rad, 0, N3);
      } else
      {
        fft_DPxDP_mixed_radix (N3, in1, tw, out1, N_p2, s_r3, s_r5, rad);
      }
    } else
    {
      fft_DPxDP_bs (N3, M, rad, in1, out1, tw, Bn, workbuf);
    }
#endif

    /* export output data of first FFT, transposed */
    lib_emt_copy1D2DLarge(
      edmall2->channel[FFT_XFER_CHANNEL1],
      (void *)out1,
      (void *)out_ptr,
      2*sizeof(double)*num_of_lines,
      outsample,
      linesize);
    out_ptr += 2*num_of_lines;
    
    /* prepare third FFT input buffer */
    lib_emt_copy2D1DLarge(
      edmall2->channel[FFT_XFER_CHANNEL0],
      (void *)in_ptr,
      (void *)in_w,
      2*sizeof(double)*num_of_lines,
      outsample,
      linesize);
    in_ptr += 2*num_of_lines;

    /* set channel numbers */
    ch_i  = FFT_XFER_CHANNEL2;
    ch_o  = FFT_XFER_CHANNEL3;
    ch_i1 = FFT_XFER_CHANNEL0;
    ch_o1 = FFT_XFER_CHANNEL1;

    /* wait for EDMA */
    lib_emt_wait(edmall2->channel[ch_i]);

    for (j = num_of_lines; j < lines; j += num_of_lines)
    {
#if (FFT_OMP_DP_3D_R2C_NUMOFLINEBUFS>1)
      /* local transpose */
      DSPF_dp_mat_trans_cplx(input, N3, num_of_lines, output);

      if (N3 == M) {
        if (s_r3 == 0 && s_r5 == 0) {
          for (i=0; i<num_of_lines; i++)
          DSPF_dp_fftDPxDP (N3, &output[i*N3*2], tw, &tempout[i*N3*2], rad, 0, N3);
        } 
        else {
          for (i=0; i<num_of_lines; i++)
          fft_DPxDP_mixed_radix (N3, &output[i*N3*2], tw, &tempout[i*N3*2], N_p2, s_r3, s_r5, rad);
        }
      } 
      else {
        for (i=0; i<num_of_lines; i++)
          fft_DPxDP_bs (N3, M, rad, &output[i*N3*2], &tempout[i*N3*2], tw, Bn, workbuf);
      }

      /* local transpose from tempout to output */
      DSPF_dp_mat_trans_cplx(tempout, num_of_lines, N3, output);
#else
      if (N3 == M) {
        if (s_r3 == 0 && s_r5 == 0) {
          DSPF_dp_fftDPxDP (N3, input, tw, output, rad, 0, N3);
        } else
        {
          fft_DPxDP_mixed_radix (N3, input, tw, output, N_p2, s_r3, s_r5, rad);
        }
      } else
      {
      fft_DPxDP_bs (N3, M, rad, input, output, tw, Bn, workbuf);
      }
#endif

      /* export output data, transposed */
      lib_emt_copy1D2DLarge(
        edmall2->channel[ch_o],
        (void *)output,
        (void *)out_ptr,
        2*sizeof(double)*num_of_lines,
        outsample,
        linesize);
      out_ptr += 2*num_of_lines;

      /* prepare input data */
      lib_emt_copy2D1DLarge(
        edmall2->channel[ch_i],
        (void *)in_ptr,
        (void *)input,
        2*sizeof(double)*num_of_lines,
        outsample,
        linesize);
      in_ptr += 2*num_of_lines;

      /* swap pingpong buffer pointers and XFER channel */
      dum2   = input;
      input  = in1;
      in1    = dum2;
      dum3   = output;
      output = out1;
      out1   = dum3;

      dum0   = ch_i;
      ch_i   = ch_i1;
      ch_i1  = dum0;
      dum1   = ch_o;
      ch_o   = ch_o1;
      ch_o1  = dum1;

      /* wait for EDMA */
      lib_emt_wait(edmall2->channel[ch_i]);
      lib_emt_wait(edmall2->channel[ch_o]);
    }

    /* wait for all ch to complete */
    lib_emt_wait(edmall2->channel[ch_i1]);
    lib_emt_wait(edmall2->channel[ch_o1]);


    return error;
} /* fft_omp_dp_1d_c2c_batch_ecpy_transpose_3d */
static int radLocal, twsizeLocal, BnsizeLocal;
static double *inLocal, *stage1Local, *stage2Local, *outLocal, *twLocal, *in_wLocal, *out_wLocal, *out_tLocal, *tw_wLocal;
static double *workLocal, *BnLocal, *Bn_wLocal;
static int n1Local, n2Local, n3Local, planesLocal, mLocal;
static int N_p2Local, s_r3Local, s_r5Local;
static FFT_EDMA_Struct *edmaLocal;
#ifndef FFT_MEM_MODEL_SM
#pragma DATA_SECTION (radLocal, ".mem_msm")
#pragma DATA_SECTION (twsizeLocal, ".mem_msm")
#pragma DATA_SECTION (BnsizeLocal, ".mem_msm")
#pragma DATA_SECTION (inLocal, ".mem_msm")
#pragma DATA_SECTION (stage1Local, ".mem_msm")
#pragma DATA_SECTION (stage2Local, ".mem_msm")
#pragma DATA_SECTION (outLocal, ".mem_msm")
#pragma DATA_SECTION (twLocal, ".mem_msm")
#pragma DATA_SECTION (in_wLocal, ".mem_msm")
#pragma DATA_SECTION (out_wLocal, ".mem_msm")
#pragma DATA_SECTION (out_tLocal, ".mem_msm")
#pragma DATA_SECTION (tw_wLocal, ".mem_msm")
#pragma DATA_SECTION (workLocal, ".mem_msm")
#pragma DATA_SECTION (BnLocal, ".mem_msm")
#pragma DATA_SECTION (Bn_wLocal, ".mem_msm")
#pragma DATA_SECTION (n1Local, ".mem_msm")
#pragma DATA_SECTION (n2Local, ".mem_msm")
#pragma DATA_SECTION (n3Local, ".mem_msm")
#pragma DATA_SECTION (planesLocal, ".mem_msm")
#pragma DATA_SECTION (mLocal, ".mem_msm")
#pragma DATA_SECTION (N_p2Local, ".mem_msm")
#pragma DATA_SECTION (s_r3Local, ".mem_msm")
#pragma DATA_SECTION (s_r5Local, ".mem_msm")
#pragma DATA_SECTION (edmaLocal, ".mem_msm")
#endif

int fft_omp_dp_3d_r2c_ecpy (
    /* int   N1;       FFT size         */
    /* int   N2;       FFT size         */
    /* int   N3;       FFT size         */
    /* float *in;      input pointer    */
    /* float *out;     output pointer   */
    /* float *in_w;    input pointer    */
    /* float *out_w;   output pointer   */
    /* float *tw;      twiddle pointer  */
    fft_param_u u,
    void *edmaState,
    int   actualCoreNum
)
{
    double *tw_a, *Bn_a;
    int error = FFT_NOERR;
    int numThread = omp_get_max_threads();
    int tid, did, numLine;
    int twsizeMax, BnsizeMax, N, scratch_buf_size;
    int tw_offsetLocal, Bn_offsetLocal;
    lib_memdscr_t **fft_mem_handle = fftGetMemHandle();

    n1Local       = u.dp_3d_r2c_e.N1;          /* signal size of the 1st dimension      */
    n2Local       = u.dp_3d_r2c_e.N2;          /* signal size of the 2nd dimension      */
    n3Local       = u.dp_3d_r2c_e.N3;          /* signal size of the 3rd dimension      */
    mLocal        = u.dp_3d_r2c_e.para1.M;     /* fft size                              */
    radLocal      = u.dp_3d_r2c_e.para1.rad;   /* radix                                 */
    N_p2Local     = u.dp_3d_r2c_e.para1.N_p2;  /* power of 2 size in mixed radix        */
    s_r3Local     = u.dp_3d_r2c_e.para1.s_r3;  /* number stages of radix 3              */
    s_r5Local     = u.dp_3d_r2c_e.para1.s_r5;  /* number stages of radix 5              */

    scratch_buf_size = 2*(n1Local/2+1)*n2Local*n3Local;

    inLocal      = u.dp_3d_r2c_e.in;                                    /* input pointer                               */
    outLocal     = u.dp_3d_r2c_e.out;                                   /* output pointer                              */
    stage1Local  = u.dp_3d_r2c_e.tw;                                    /* buffer pointer to hold intermediate results */
    stage2Local  = u.dp_3d_r2c_e.tw+scratch_buf_size;                   /* buffer pointer to hold intermediate results */
    twLocal      = u.dp_3d_r2c_e.tw+2*scratch_buf_size;                 /* twiddle buffer pointer                      */
    BnLocal      = u.dp_3d_r2c_e.Bn;                                    /* Bn buffer pointer                           */
    edmaLocal    = (FFT_EDMA_Struct *)edmaState;


    /* max of the 3 dimension */
    twsizeMax = (u.dp_3d_r2c_e.para1.twsize > u.dp_3d_r2c_e.para2.twsize) ? u.dp_3d_r2c_e.para1.twsize : u.dp_3d_r2c_e.para2.twsize;
    twsizeMax = (u.dp_3d_r2c_e.para3.twsize > twsizeMax) ? u.dp_3d_r2c_e.para3.twsize : twsizeMax;

    BnsizeMax = (u.dp_3d_r2c_e.para1.Bnsize > u.dp_3d_r2c_e.para2.Bnsize) ? u.dp_3d_r2c_e.para1.Bnsize : u.dp_3d_r2c_e.para2.Bnsize;
    BnsizeMax = (u.dp_3d_r2c_e.para3.Bnsize > BnsizeMax) ? u.dp_3d_r2c_e.para3.Bnsize : BnsizeMax;

    N = (u.dp_3d_r2c_e.N1 > u.dp_3d_r2c_e.N2) ? u.dp_3d_r2c_e.N1 : u.dp_3d_r2c_e.N2;
    N = (u.dp_3d_r2c_e.N3 > N) ? u.dp_3d_r2c_e.N3 : N;

    in_wLocal   = (double*)lib_smem_falloc (fft_mem_handle, 4*N*FFT_OMP_DP_3D_R2C_NUMOFLINEBUFS*sizeof(double), 8);
    out_wLocal  = (double*)lib_smem_falloc (fft_mem_handle, 4*N*FFT_OMP_DP_3D_R2C_NUMOFLINEBUFS*sizeof(double), 8);
    out_tLocal  = (double*)lib_smem_falloc (fft_mem_handle, 2*N*FFT_OMP_DP_3D_R2C_NUMOFLINEBUFS*sizeof(double), 8);
#ifdef FFT_MEM_MODEL_LG
    tw_wLocal   = (double*)lib_smem_falloc (fft_mem_handle, twsizeMax*sizeof(double), 8);
#endif

#ifdef FFT_MEM_MODEL_LG
    if(in_wLocal==NULL || out_wLocal==NULL || out_tLocal==NULL || tw_wLocal==NULL) {
#else
    tw_wLocal = NULL;
    if(in_wLocal==NULL || out_wLocal==NULL || out_tLocal==NULL) {
#endif
        printf("Memory allocation error!\n");
        return FFT_ERROR;
    }

    if((u.dp_3d_r2c_e.para1.use_bs == 1) || (u.dp_3d_r2c_e.para2.use_bs == 1) || (u.dp_3d_r2c_e.para3.use_bs == 1))
    {
      Bn_wLocal = tw_wLocal + twsizeMax;
      workLocal = Bn_wLocal + BnsizeMax;
    }else
    {
      Bn_wLocal = NULL;
      workLocal = NULL;
    }

    if ((actualCoreNum==1)||
        (actualCoreNum==2)||
        (actualCoreNum==4)||
        (actualCoreNum==8))
      omp_set_num_threads (actualCoreNum);
    else
      omp_set_num_threads (OMP_MAX_NUM_CORES);

    numThread = omp_get_max_threads();


    planesLocal = n3Local/numThread;
    twsizeLocal = u.dp_3d_r2c_e.para1.twsize*sizeof(double);
    BnsizeLocal = u.dp_3d_r2c_e.para1.Bnsize*sizeof(double);

#pragma omp parallel private(tid, did, numLine, tw_a, Bn_a) shared(radLocal, inLocal, stage1Local, \
      twLocal, in_wLocal, out_wLocal, out_tLocal, tw_wLocal, BnLocal, Bn_wLocal, workLocal, \
      n1Local, n2Local, n3Local, planesLocal, N_p2Local, s_r3Local, s_r5Local, mLocal, \
      twsizeLocal, BnsizeLocal, edmaLocal)
    {
    tid = omp_get_thread_num();     // Obtain thread number
    did = DNUM;
    numLine = FFT_OMP_DP_3D_R2C_NUMOFLINEBUFS_SM;

    if (tw_wLocal != NULL) {
      /* prepare twiddle buffer */
      lib_emt_copy1D1D(
      (edmaLocal+did)->channel[FFT_XFER_CHANNEL3],
      (void *)twLocal,
      (void *)tw_wLocal,
      twsizeLocal);
      tw_a = tw_wLocal;
    }
    else {
      tw_a = twLocal;
    }

    if (Bn_wLocal != NULL) {
      /* prepare Bn buffer */
      lib_emt_copy1D1D(
      (edmaLocal+did)->channel[FFT_XFER_CHANNEL2],
      (void *)BnLocal,
      (void *)Bn_wLocal,
      BnsizeLocal);
      Bn_a = Bn_wLocal;
    }
    else {
      Bn_a = BnLocal;
    }

    if (tw_wLocal != NULL)
      lib_emt_wait((edmaLocal+did)->channel[FFT_XFER_CHANNEL3]);
    if (Bn_wLocal != NULL)
      lib_emt_wait((edmaLocal+did)->channel[FFT_XFER_CHANNEL2]);

    /* 1st dimension fft */
    error = fft_omp_dp_1d_r2c_ecpy_transpose (1,
        n1Local,
        n2Local,
        planesLocal,
        mLocal,
        radLocal,
        N_p2Local,
        s_r3Local,
        s_r5Local,
        &inLocal[tid*(n1Local*2*n2Local*planesLocal/2)],
        &stage1Local[tid*((n1Local/2+1)*2*n2Local*planesLocal)],
        in_wLocal,
        out_wLocal,
        out_tLocal,
        tw_a,
        Bn_a,
        workLocal,
        edmaLocal+did,
        numLine);
    }

    /* prepare for the 2nd dimension */
    twsizeLocal    = u.dp_3d_r2c_e.para2.twsize*sizeof(double);
    BnsizeLocal    = u.dp_3d_r2c_e.para2.Bnsize*sizeof(double);
    N_p2Local      = u.dp_3d_r2c_e.para2.N_p2;    /* power of 2 size in mixed radix        */
    s_r3Local      = u.dp_3d_r2c_e.para2.s_r3;    /* number stages of radix 3              */
    s_r5Local      = u.dp_3d_r2c_e.para2.s_r5;    /* number stages of radix 5              */
    radLocal       = u.dp_3d_r2c_e.para2.rad;
    mLocal         = u.dp_3d_r2c_e.para2.M;
    tw_offsetLocal = u.dp_3d_r2c_e.para1.twsize;
    Bn_offsetLocal = u.dp_3d_r2c_e.para1.Bnsize;
    twLocal        = u.dp_3d_r2c_e.tw + 2*scratch_buf_size + tw_offsetLocal;    /* twiddle working buffer pointer        */
    BnLocal        = u.dp_3d_r2c_e.Bn + Bn_offsetLocal;    /* Bn working buffer pointer             */
    planesLocal    = n3Local/numThread;

#pragma omp parallel private(tid, did, numLine, tw_a, Bn_a) shared(radLocal, stage1Local, stage2Local, \
      twLocal, in_wLocal, out_wLocal, out_tLocal, tw_wLocal, BnLocal, Bn_wLocal, workLocal, \
      n1Local, n2Local, n3Local, planesLocal, N_p2Local, s_r3Local, s_r5Local, mLocal, edmaLocal)
    {
    tid = omp_get_thread_num();     // Obtain thread number
    did = DNUM;
    numLine = FFT_OMP_DP_3D_R2C_NUMOFLINEBUFS_SM;
    if (tw_wLocal != NULL) {
      /* prepare twiddle buffer */
      lib_emt_copy1D1D(
      (edmaLocal+did)->channel[FFT_XFER_CHANNEL3],
      (void *)(twLocal),
      (void *)tw_wLocal,
      twsizeLocal);
      tw_a = tw_wLocal;
    }
    else {
      tw_a = twLocal;
    }

    if (Bn_wLocal != NULL) {
      /* prepare Bn buffer */
      lib_emt_copy1D1D(
      (edmaLocal+did)->channel[FFT_XFER_CHANNEL2],
      (void *)(BnLocal),
      (void *)Bn_wLocal,
      BnsizeLocal);
      Bn_a = Bn_wLocal;
    }
    else {
      Bn_a = BnLocal;
    }

    if (tw_wLocal != NULL)
      lib_emt_wait((edmaLocal+did)->channel[FFT_XFER_CHANNEL3]);
    if (Bn_wLocal != NULL)
      lib_emt_wait((edmaLocal+did)->channel[FFT_XFER_CHANNEL2]);

    /* 2nd dimension fft */
    error = fft_omp_dp_1d_r2c_ecpy_transpose (2,
        n2Local,
        n1Local/2+1,
        planesLocal,
        mLocal,
        radLocal,
        N_p2Local,
        s_r3Local,
        s_r5Local,
        &stage1Local[tid*(n2Local*(n1Local/2+1)*2*planesLocal)],
        &stage2Local[tid*((n1Local/2+1)*2*n2Local*planesLocal)],
        out_wLocal,
        in_wLocal,
        out_tLocal,
        tw_a,
        Bn_a,
        workLocal,
        edmaLocal+did,
        numLine);
    }

    /* prepare for the 3rd dimension */
    twsizeLocal    = u.dp_3d_r2c_e.para3.twsize*sizeof(double);
    BnsizeLocal    = u.dp_3d_r2c_e.para3.Bnsize*sizeof(double);
    N_p2Local      = u.dp_3d_r2c_e.para3.N_p2;    /* power of 2 size in mixed radix        */
    s_r3Local      = u.dp_3d_r2c_e.para3.s_r3;    /* number stages of radix 3              */
    s_r5Local      = u.dp_3d_r2c_e.para3.s_r5;    /* number stages of radix 5              */
    radLocal       = u.dp_3d_r2c_e.para3.rad;
    mLocal         = u.dp_3d_r2c_e.para3.M;
    tw_offsetLocal = u.dp_3d_r2c_e.para1.twsize + u.dp_3d_r2c_e.para2.twsize;
    Bn_offsetLocal = u.dp_3d_r2c_e.para1.Bnsize + u.dp_3d_r2c_e.para2.Bnsize;
    twLocal        = u.dp_3d_r2c_e.tw+2*scratch_buf_size + tw_offsetLocal;    /* twiddle working buffer pointer        */
    BnLocal        = u.dp_3d_r2c_e.Bn + Bn_offsetLocal;    /* Bn working buffer pointer             */


    planesLocal = (n1Local/2+1)*n2Local/numThread;

#pragma omp parallel private(tid, did, numLine, tw_a, Bn_a) shared(radLocal, stage2Local, outLocal, \
      twLocal, in_wLocal, out_wLocal, out_tLocal, tw_wLocal, BnLocal, Bn_wLocal, workLocal, \
      n1Local, n2Local, n3Local, planesLocal, N_p2Local, s_r3Local, s_r5Local, mLocal, edmaLocal)
    {
    tid = omp_get_thread_num();     // Obtain thread number
    did = DNUM;
    numLine = FFT_OMP_DP_3D_R2C_NUMOFLINEBUFS;
    if (tw_wLocal != NULL) {
      /* prepare twiddle buffer */
      lib_emt_copy1D1D(
      (edmaLocal+did)->channel[FFT_XFER_CHANNEL3],
      (void *)(twLocal),
      (void *)tw_wLocal,
      twsizeLocal);
      tw_a = tw_wLocal;
    }
    else {
      tw_a = twLocal;
    }

    if (Bn_wLocal != NULL) {
      /* prepare Bn buffer */
      lib_emt_copy1D1D(
      (edmaLocal+did)->channel[FFT_XFER_CHANNEL2],
      (void *)(BnLocal),
      (void *)Bn_wLocal,
      BnsizeLocal);
      Bn_a = Bn_wLocal;
    }
    else {
      Bn_a = BnLocal;
    }

    if (tw_wLocal != NULL)
      lib_emt_wait((edmaLocal+did)->channel[FFT_XFER_CHANNEL3]);
    if (Bn_wLocal != NULL)
      lib_emt_wait((edmaLocal+did)->channel[FFT_XFER_CHANNEL2]);

    /* 3rd dimension fft */
    error = fft_omp_dp_1d_c2c_batch_ecpy_transpose_3d (
        (n1Local/2+1)*n2Local,
        n3Local,
        planesLocal,
        mLocal,
        radLocal,
        N_p2Local,
        s_r3Local,
        s_r5Local,
        &stage2Local[tid*2*planesLocal],
        &outLocal[tid*2*planesLocal],
        in_wLocal,
        out_wLocal,
        out_tLocal,
        tw_a,
        Bn_a,
        workLocal,
        edmaLocal+did, 
        numLine);
    }
    omp_set_num_threads (OMP_MAX_NUM_CORES);
    return error;
} /* fft_omp_dp_3d_r2c_ecpy */

/* ======================================================================== */
/*  End of file:  fft_omp_dp_3d_r2c.c                                       */
/* ------------------------------------------------------------------------ */
/*            Copyright (c) 2013 Texas Instruments, Incorporated.           */
/*                           All Rights Reserved.                           */
/* ======================================================================== */
