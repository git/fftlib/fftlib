/* ======================================================================= */
/*  TEXAS INSTRUMENTS, INC.                                                */
/*                                                                         */
/*  FFTLIB  FFT Library                                                    */
/*                                                                         */
/* Copyright (C) 2013 Texas Instruments Incorporated - http://www.ti.com/  */
/*                                                                         */
/*                                                                         */
/*  Redistribution and use in source and binary forms, with or without     */
/*  modification, are permitted provided that the following conditions     */
/*  are met:                                                               */
/*                                                                         */
/*    Redistributions of source code must retain the above copyright       */
/*    notice, this list of conditions and the following disclaimer.        */
/*                                                                         */
/*    Redistributions in binary form must reproduce the above copyright    */
/*    notice, this list of conditions and the following disclaimer in the  */
/*    documentation and/or other materials provided with the               */
/*    distribution.                                                        */
/*                                                                         */
/*    Neither the name of Texas Instruments Incorporated nor the names of  */
/*    its contributors may be used to endorse or promote products derived  */
/*    from this software without specific prior written permission.        */
/*                                                                         */
/*  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS    */
/*  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT      */
/*  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR  */
/*  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT   */
/*  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,  */
/*  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT       */
/*  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,  */
/*  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY  */
/*  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT    */
/*  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE  */
/*  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.   */
/*                                                                         */
/* ======================================================================= */

#include <xdc/std.h>
#include "c6x.h"
#include "stdlib.h"
#include <stdio.h>
#include <ti/fftlib/src/common/omp/omp_config.h>
#include "fft_omp_sp_2d_r2c.h"
#include <ti/fftlib/src/common/fft/fft_sp_mixed_bs.h>
#include <ti/dsplib/src/DSPF_sp_fftSPxSP/DSPF_sp_fftSPxSP.h>
#include <ti/fftlib/src/common/fft/fft_sp_cmplx_notwid_br.h>
#include <ti/fftlib/src/common/fft_edma.h>
#include <ti/dsplib/src/DSPF_sp_mat_trans_cplx/DSPF_sp_mat_trans_cplx.h>
#include <ti/runtime/openmp/omp.h>
#include <ti/libarch/libarch.h>

static int fft_omp_sp_1d_r2c_ecpy_transpose (
  int   dim,               /* dimension                      */
  int   N1,                /* FFT size                       */
  int   N2,                /* number of lines                */
  int   N3,                /* total number of lines          */
  int   M,                 /* Padded size of Anysize FFT     */
  int   rad,               /* radix                          */
  int   N_p2,              /* power of 2 size in mixed radix */
  int   s_r3,              /* number stages of radix 3       */
  int   s_r5,              /* number stages of radix 5       */
  float *in,               /* input pointer                  */
  float *out,              /* output pointer                 */
  float *in_w,             /* input work buffer pointer      */
  float *out_w,            /* output working buffer pointer  */
  float *out_t,            /* temp working buffer pointer    */
  float *tw,               /* twiddle pointer                */
  float *Bn_w,
  float *workbuf_bn,
  FFT_EDMA_Struct  *edma,  /* edma resource pointer          */
  int num_of_lines
)
{
    int   error = FFT_NOERR;
    int   i,j,ch_i,ch_o,ch_i1,ch_o1,dum0,dum1;
    int   insize, instep,
          outsample,
          linesize  = N3*2*sizeof(float),
          tid = DNUM;
    FFT_EDMA_Struct  *edmall2;

    float *restrict in_ptr = in, *restrict out_ptr = out, *restrict tempout;
    float *restrict input, *restrict output, *restrict in1, *restrict out1, *dum2, *dum3;

    if (dim == 1)
    {
      insize = N1*sizeof(float)*num_of_lines;
      instep = N1*num_of_lines;
      outsample  = N1/2+1;
    }
    else
    {
      insize = 2*N1*sizeof(float)*num_of_lines;
      instep = 2*N1*num_of_lines;
      outsample  = N1;
    }

  edmall2 = edma;
    /* prepare first FFT input buffer */
    lib_emt_copy1D1D(
      edmall2->channel[FFT_XFER_CHANNEL0],
      (void *)in_ptr,
      (void *)in_w,
      insize);
    in_ptr += instep;

    /* set pingpong buffer pointers */
    in1    = in_w;
    out1   = out_w;
    input  = &in_w[N1*dim*num_of_lines];
    output = &out_w[2*N1*num_of_lines];
    tempout= out_t;

    /* prepare second FFT input buffer */
    lib_emt_copy1D1D(
      edmall2->channel[FFT_XFER_CHANNEL2],
      (void *)in_ptr,
      (void *)input,
      insize);
    in_ptr += instep;

    /* wait for first FFT input */
    lib_emt_wait(edmall2->channel[FFT_XFER_CHANNEL0]);

#if (FFT_OMP_SP_2D_R2C_NUMOFLINEBUFS>1)
    if (dim == 1) {
      if (N1 == M) {
        if (s_r3 == 0 && s_r5 == 0) {
          for (i=0; i<num_of_lines; i++)
            fft_SPxSP_r2c (N1, &in1[i*N1], tw, &tempout[i*N1*2], N1);
        } else {
          for (i=0; i<num_of_lines; i++)
            fft_SPxSP_mixed_radix_r2c (N1, &in1[i*N1], tw, &tempout[i*N1*2], N_p2, s_r3, s_r5);
        }
      } /* if (N1 == M) */
      else {
        for (i=0; i<num_of_lines; i++)
          fft_SPxSP_r2c_bs (N1, M, rad, &in1[i*N1], &tempout[i*N1*2], tw, Bn_w, workbuf_bn);
      }
    } else {
      if (N1 == M) {
        if (s_r3 == 0 && s_r5 == 0) {
          for (i=0; i<num_of_lines; i++)
            fft_sp_cmplx_notwid_br((double *)&in1[i*N1*2], N1, (double *)&tempout[i*N1*2], (double *)tw);
        } else {
          for (i=0; i<num_of_lines; i++)
            fft_SPxSP_mixed_radix_c2c (N1, &in1[i*N1*2], tw, &tempout[i*N1*2], N_p2, s_r3, s_r5);
        }
      } /* if (N1 == M)*/
      else {
        for (i=0; i<num_of_lines; i++)
          fft_SPxSP_c2c_bs (N1, M, rad, &in1[i*N1*2], &tempout[i*N1*2], tw, Bn_w, workbuf_bn);
      }
    } /* if (dim) */

  /* local transpose from tempout to out1 */
  DSPF_sp_mat_trans_cplx(tempout, num_of_lines, N1, out1);
#else
    if (dim == 1) {
      if (N1 == M) {
        if (s_r3 == 0 && s_r5 == 0) {
          fft_SPxSP_r2c (N1, in1, tw, out1, N1);
        } else {
          fft_SPxSP_mixed_radix_r2c (N1, in1, tw, out1, N_p2, s_r3, s_r5);
        }
      } else {
        fft_SPxSP_r2c_bs (N1, M, rad, in1, out1, tw, Bn_w, workbuf_bn);
      }
    } else {
      if (N1 == M) {
        if (s_r3 == 0 && s_r5 == 0) {
          fft_sp_cmplx_notwid_br((double *)in1, N1, (double *)out1, (double *)tw);
        } else {
          fft_SPxSP_mixed_radix_c2c (N1, in1, tw, out1, N_p2, s_r3, s_r5);
        }
      } else {
        fft_SPxSP_c2c_bs (N1, M, rad, in1, out1, tw, Bn_w, workbuf_bn);
      }
    }
#endif

    /* export output data of first FFT, transposed */
    lib_emt_copy1D2D(
      edmall2->channel[FFT_XFER_CHANNEL1],
      (void *)out1,
      (void *)out_ptr,
      2*sizeof(float)*num_of_lines,
      outsample,
      linesize);
    out_ptr += 2*num_of_lines;
    
#ifdef USEFASTEDMA
  /* fill the PaRAM for CHANNEL 3 without starting it */
  lib_emt_copy1D2D_Fill(
    edmall2->channel[FFT_XFER_CHANNEL3],
    (void *)out1,
    (void *)out_ptr,
    2*sizeof(float)*num_of_lines,
    outsample,
    linesize);
#endif

    /* prepare third FFT input buffer */
#ifndef USEFASTEDMA
    lib_emt_copy1D1D(
      edmall2->channel[FFT_XFER_CHANNEL0],
      (void *)in_ptr,
      (void *)in_w,
      insize);
#else
  lib_emt_copy1D1D_Fast(
    edmall2->channel[FFT_XFER_CHANNEL0],
    (void *)in_ptr,
    (void *)in_w);
#endif
    in_ptr += instep;

    /* set channel numbers */
    ch_i  = FFT_XFER_CHANNEL2;
    ch_o  = FFT_XFER_CHANNEL3;
    ch_i1 = FFT_XFER_CHANNEL0;
    ch_o1 = FFT_XFER_CHANNEL1;

    /* wait for EDMA */
    lib_emt_wait(edmall2->channel[ch_i]);
    _nassert (N2 >= 4);

    for (j = 1; j < N2/num_of_lines; j++){
#if (FFT_OMP_SP_2D_R2C_NUMOFLINEBUFS>1)
      if (dim == 1) {
        if (N1 == M) {
          if (s_r3 == 0 && s_r5 == 0) {
            for (i=0; i<num_of_lines; i++)
              fft_SPxSP_r2c (N1, &input[i*N1], tw, &tempout[i*N1*2], N1);
          } else {
            for (i=0; i<num_of_lines; i++)
              fft_SPxSP_mixed_radix_r2c (N1, &input[i*N1], tw, &tempout[i*N1*2], N_p2, s_r3, s_r5);
          }
        } /* if (N1 == M) */
        else {
          for (i=0; i<num_of_lines; i++)
            fft_SPxSP_r2c_bs (N1, M, rad, &input[i*N1], &tempout[i*N1*2], tw, Bn_w, workbuf_bn);
        }
      } /* if (dim) */
      else {
        if (N1 == M) {
          if (s_r3 == 0 && s_r5 == 0) {
            for (i=0; i<num_of_lines; i++)
              fft_sp_cmplx_notwid_br((double *)&input[i*N1*2], N1, (double *)&tempout[i*N1*2], (double *)tw);
           } else {
            for (i=0; i<num_of_lines; i++)
              fft_SPxSP_mixed_radix_c2c (N1, &input[i*N1*2], tw, &tempout[i*N1*2], N_p2, s_r3, s_r5);
          }
        } /* if (N1 == M)*/
        else {
          for (i=0; i<num_of_lines; i++)
            fft_SPxSP_c2c_bs (N1, M, rad, &input[i*N1*2], &tempout[i*N1*2], tw, Bn_w, workbuf_bn);
        }
      }

      /* local transpose from tempout to out1 */
      DSPF_sp_mat_trans_cplx(tempout, num_of_lines, N1, output);
#else
    if (dim == 1) {
      if (N1 == M) {
        if (s_r3 == 0 && s_r5 == 0) {
          fft_SPxSP_r2c (N1, input, tw, output, N1);
        } else {
          fft_SPxSP_mixed_radix_r2c (N1, input, tw, output, N_p2, s_r3, s_r5);
        }
      } else {
        fft_SPxSP_r2c_bs (N1, M, rad, input, output, tw, Bn_w, workbuf_bn);
      }
    } else {
      if (N1 == M) {
        if (s_r3 == 0 && s_r5 == 0) {
          fft_sp_cmplx_notwid_br((double *)input, N1, (double *)output, (double *)tw);
        } else {
          fft_SPxSP_mixed_radix_c2c (N1, input, tw, output, N_p2, s_r3, s_r5);
        }
      } else {
        fft_SPxSP_c2c_bs (N1, M, rad, input, output, tw, Bn_w, workbuf_bn);
      }
    }
#endif

#ifndef USEFASTEDMA
      /* export output data, transposed */
      lib_emt_copy1D2D(
        edmall2->channel[ch_o],
        (void *)output,
        (void *)out_ptr,
        2*sizeof(float)*num_of_lines,
        outsample,
        linesize);
#else
    lib_emt_copy1D2D_Fast(
      edmall2->channel[ch_o],
      (void *)output,
      (void *)out_ptr);
#endif        
      out_ptr += 2*num_of_lines;
      /* prepare input data */
#ifndef USEFASTEDMA
      lib_emt_copy1D1D(
        edmall2->channel[ch_i],
        (void *)in_ptr,
        (void *)input,
        insize);
#else
    lib_emt_copy1D1D_Fast(
      edmall2->channel[ch_i],
      (void *)in_ptr,
      (void *)input);
#endif        
      in_ptr += instep;

      /* swap pingpong buffer pointers and XFER channel */
      dum2   = input;
      input  = in1;
      in1    = dum2;
      dum3   = output;
      output = out1;
      out1   = dum3;

      dum0   = ch_i;
      ch_i   = ch_i1;
      ch_i1  = dum0;
      dum1   = ch_o;
      ch_o   = ch_o1;
      ch_o1  = dum1;

      /* wait for EDMA */
      lib_emt_wait(edmall2->channel[ch_i]);
      lib_emt_wait(edmall2->channel[ch_o]);
    } /* for()*/
    
  /* wait for EDMA */
  lib_emt_wait(edmall2->channel[ch_i1]);
  lib_emt_wait(edmall2->channel[ch_o1]);

   return error;
} /* fft_omp_sp_1d_r2c_ecpy_transpose */

static int radLocal, n_p2Local, s_r3Local, s_r5Local;
static int n1Local, n2Local, m1Local, m2Local, linesLocal;
static float *dataLocal, *workbufLocal, *BnLocal, *twLocal, *data_wLocal, 
              *workbuf_wLocal, *Bn_wLocal, *workbuf_tLocal, *workbuf_bnLocal, 
              *tw_wLocal, *Bn_aLocal, *tw_aLocal, *stageLocal;
static FFT_EDMA_Struct *edmaLocal;
#ifndef FFT_MEM_MODEL_SM
#pragma DATA_SECTION (radLocal, ".mem_msm")
#pragma DATA_SECTION (n_p2Local, ".mem_msm")
#pragma DATA_SECTION (s_r3Local, ".mem_msm")
#pragma DATA_SECTION (s_r5Local, ".mem_msm")
#pragma DATA_SECTION (n1Local, ".mem_msm")
#pragma DATA_SECTION (n2Local, ".mem_msm")
#pragma DATA_SECTION (m1Local, ".mem_msm")
#pragma DATA_SECTION (m2Local, ".mem_msm")
#pragma DATA_SECTION (linesLocal, ".mem_msm")
#pragma DATA_SECTION (dataLocal, ".mem_msm")
#pragma DATA_SECTION (stageLocal, ".mem_msm")
#pragma DATA_SECTION (workbufLocal, ".mem_msm")
#pragma DATA_SECTION (twLocal, ".mem_msm")
#pragma DATA_SECTION (data_wLocal, ".mem_msm")
#pragma DATA_SECTION (workbuf_wLocal, ".mem_msm")
#pragma DATA_SECTION (workbuf_tLocal, ".mem_msm")
#pragma DATA_SECTION (tw_wLocal, ".mem_msm")
#pragma DATA_SECTION (Bn_aLocal, ".mem_msm")
#pragma DATA_SECTION (tw_aLocal, ".mem_msm")
#pragma DATA_SECTION (edmaLocal, ".mem_msm")
#endif

int fft_omp_sp_2d_r2c_ecpy (
  /* int   N1;            FFT size                          */
  /* int   N2;            FFT size                          */
  /* float *data;         data pointer                      */
  /* float *workbuf;      work buffer pointer               */
  /* float *data_w;       data internal pointer             */
  /* float *workbuf_w;    working buffer internal pointer   */
  /* float *tw;           twiddle pointer                   */
  fft_param_u u,
  void *edmaState,
  int   actualCoreNum
)
{
  int numThread = omp_get_max_threads();
  int tid, did, numLine, N, scratch_buf_size;

  int error = FFT_NOERR;
  int twsize, Bnsize, tw_offset, Bn_offset;
     
  /* max of Bnsize and twsize for internal memory usage */
  int twsizeMax = (u.sp_2d_r2c_e.para1.twsize > u.sp_2d_r2c_e.para2.twsize) ? u.sp_2d_r2c_e.para1.twsize : u.sp_2d_r2c_e.para2.twsize;
  int BnsizeMax = (u.sp_2d_r2c_e.para1.Bnsize > u.sp_2d_r2c_e.para2.Bnsize) ? u.sp_2d_r2c_e.para1.Bnsize : u.sp_2d_r2c_e.para2.Bnsize;
  lib_memdscr_t **fft_mem_handle = fftGetMemHandle();

  N                = u.sp_2d_r2c_e.N;
  n1Local          = u.sp_2d_r2c_e.N1;          /* signal size                    */
  n2Local          = u.sp_2d_r2c_e.N2;          /* signal size                    */
  m1Local          = u.sp_2d_r2c_e.para1.M;     /* first dimension FFT size M1    */
  m2Local          = u.sp_2d_r2c_e.para2.M;     /* second dimension FFT size M2   */
  scratch_buf_size = 2*(n1Local/2+1)*n2Local;

  /* input parameters */
  dataLocal       = u.sp_2d_r2c_e.in;                  /* data pointer                   */
  workbufLocal    = u.sp_2d_r2c_e.out;                 /* output pointer                 */
  twLocal         = u.sp_2d_r2c_e.tw+scratch_buf_size; /* twiddle         buffer pointer */
  BnLocal         = u.sp_2d_r2c_e.Bn;                  /* Bn buffer pointer              */
  stageLocal      = u.sp_2d_r2c_e.tw;                  /* scratch buffer                 */

  /* internal pointers        */
  data_wLocal    = (float*)lib_smem_falloc (fft_mem_handle, 4*N*FFT_OMP_SP_2D_R2C_NUMOFLINEBUFS*sizeof(float), 8);
  workbuf_wLocal = (float*)lib_smem_falloc (fft_mem_handle, 4*N*FFT_OMP_SP_2D_R2C_NUMOFLINEBUFS*sizeof(float), 8);
  workbuf_tLocal = (float*)lib_smem_falloc (fft_mem_handle, 2*N*FFT_OMP_SP_2D_R2C_NUMOFLINEBUFS*sizeof(float), 8);
#ifdef FFT_MEM_MODEL_LG
  tw_wLocal      = (float*)lib_smem_falloc (fft_mem_handle, twsizeMax*sizeof(float), 8);
#endif

#ifdef FFT_MEM_MODEL_LG
    if(data_wLocal==NULL || workbuf_wLocal==NULL || workbuf_tLocal==NULL || tw_wLocal==NULL) {
#else
    tw_wLocal = NULL;
    if(data_wLocal==NULL || workbuf_wLocal==NULL || workbuf_tLocal==NULL) {
#endif
        printf("Memory allocation error!\n");
        return FFT_ERROR;
    }

  edmaLocal = (FFT_EDMA_Struct *)edmaState;

  if((u.sp_2d_r2c_e.para1.use_bs == 1) || (u.sp_2d_r2c_e.para2.use_bs == 1))
  {
    Bn_wLocal        = tw_wLocal + twsizeMax;
    workbuf_bnLocal  = Bn_wLocal + BnsizeMax;
  }else
  {
    Bn_wLocal       = NULL;
    workbuf_bnLocal = NULL;
  }

  if ((actualCoreNum==1)||
      (actualCoreNum==2)||
      (actualCoreNum==4)||
      (actualCoreNum==8))
    omp_set_num_threads (actualCoreNum);
  else
    omp_set_num_threads (OMP_MAX_NUM_CORES);

  numThread = omp_get_max_threads();

  linesLocal = n2Local/numThread;

  radLocal         = u.sp_2d_r2c_e.para1.rad;    /* radix for N1                          */
    
  twsize            = u.sp_2d_r2c_e.para1.twsize +
                      u.sp_2d_r2c_e.para2.twsize; /* twiddle size in double words          */
  n_p2Local         = u.sp_2d_r2c_e.para1.N_p2;   /* power of 2 size in mixed radix        */
  s_r3Local         = u.sp_2d_r2c_e.para1.s_r3;   /* number stages of radix 3              */
  s_r5Local         = u.sp_2d_r2c_e.para1.s_r5;   /* number stages of radix 5              */
  Bnsize            = u.sp_2d_r2c_e.para1.Bnsize +
                      u.sp_2d_r2c_e.para2.Bnsize;

#pragma omp parallel private(did, tid, numLine, tw_aLocal, Bn_aLocal) shared(radLocal, dataLocal, stageLocal, BnLocal, \
  twLocal, data_wLocal, workbuf_wLocal, workbuf_tLocal, Bn_wLocal, tw_wLocal, workbuf_bnLocal, n1Local, \
  n2Local, m1Local, linesLocal, n_p2Local, edmaLocal, twsize, Bnsize, tw_offset, Bn_offset)
   {
    tid = omp_get_thread_num();     // Obtain thread number
    did = DNUM;
    numLine = FFT_OMP_SP_2D_R2C_NUMOFLINEBUFS;

    if (tw_wLocal != NULL) {
      /* prepare twiddle buffer */
      lib_emt_copy1D1D(
        ((edmaLocal+did)->channel[FFT_XFER_CHANNEL3]),
        (void *)twLocal,
        (void *)tw_wLocal,
        twsize*sizeof(float));
      tw_aLocal = tw_wLocal;
    }
    else {
      tw_aLocal = twLocal;
    }

    if (Bn_wLocal != NULL) {
      /* prepare Bn buffer */
      lib_emt_copy1D1D(
        ((edmaLocal+did)->channel[FFT_XFER_CHANNEL2]),
        (void *)BnLocal,
        (void *)Bn_wLocal,
        Bnsize*sizeof(float));
      Bn_aLocal = Bn_wLocal;
    } else {
      Bn_aLocal = BnLocal;
    }
    
    if (tw_wLocal != NULL)
    	lib_emt_wait((edmaLocal+did)->channel[FFT_XFER_CHANNEL3]);
    if (Bn_wLocal != NULL)
    	lib_emt_wait((edmaLocal+did)->channel[FFT_XFER_CHANNEL2]);

    /* 1st dimension fft */
    error = fft_omp_sp_1d_r2c_ecpy_transpose (
      1,
      n1Local,
      linesLocal,
      n2Local,
      m1Local,
      radLocal,
      n_p2Local,
      s_r3Local,
      s_r5Local,
      &dataLocal[tid*(n1Local*linesLocal)],
      &stageLocal[tid*2*linesLocal],
      data_wLocal,
      workbuf_wLocal,
      workbuf_tLocal,
      tw_aLocal,
      Bn_aLocal,
      workbuf_bnLocal,
      edmaLocal+did,
      numLine);
    }

  /* prepare for the 2nd dimension */
  n_p2Local   = u.sp_2d_r2c_e.para2.N_p2;    /* power of 2 size in mixed radix        */
  s_r3Local   = u.sp_2d_r2c_e.para2.s_r3;    /* number stages of radix 3              */
  s_r5Local   = u.sp_2d_r2c_e.para2.s_r5;    /* number stages of radix 5              */
  radLocal    = u.sp_2d_r2c_e.para2.rad;
  tw_offset   = u.sp_2d_r2c_e.para1.twsize;
  Bn_offset   = u.sp_2d_r2c_e.para1.Bnsize;
  
  linesLocal = (n1Local/2)/numThread;

#pragma omp parallel private(did, tid, numLine, tw_aLocal, Bn_aLocal) shared(radLocal, stageLocal, workbufLocal, BnLocal, \
  twLocal, data_wLocal, workbuf_wLocal, workbuf_tLocal, Bn_wLocal, tw_wLocal, workbuf_bnLocal, n1Local, \
  n2Local, m2Local, linesLocal, n_p2Local, edmaLocal, twsize, Bnsize, tw_offset, Bn_offset)
  {
    tid = omp_get_thread_num();     // Obtain thread number
    did = DNUM;
    numLine = FFT_OMP_SP_2D_R2C_NUMOFLINEBUFS;
    
  if (tw_wLocal != NULL) {
    /* prepare twiddle buffer */
    lib_emt_copy1D1D(
      ((edmaLocal+did)->channel[FFT_XFER_CHANNEL3]),
      (void *)(twLocal + tw_offset),
      (void *)tw_wLocal,
      twsize*sizeof(float));
    tw_aLocal = tw_wLocal;
  }
  else {
    tw_aLocal = twLocal + tw_offset;
  }

  if (Bn_wLocal != NULL) {
    /* prepare Bn buffer */
    lib_emt_copy1D1D(
      ((edmaLocal+did)->channel[FFT_XFER_CHANNEL2]),
      (void *)(BnLocal + Bn_offset),
      (void *)Bn_wLocal,
      Bnsize*sizeof(float));
    Bn_aLocal = Bn_wLocal;
  } else {
    Bn_aLocal = BnLocal;
  }

  if (tw_wLocal != NULL)
  	lib_emt_wait((edmaLocal+did)->channel[FFT_XFER_CHANNEL3]);
  if (Bn_wLocal != NULL)
  	lib_emt_wait((edmaLocal+did)->channel[FFT_XFER_CHANNEL2]);
    
    /* 2nd dimension fft */
    error = fft_omp_sp_1d_r2c_ecpy_transpose (
      2,
      n2Local,
      linesLocal,
      n1Local/2+1,
      m2Local,
      radLocal,
      n_p2Local, 
      s_r3Local, 
      s_r5Local,
      &stageLocal[tid*(n2Local*2*linesLocal)],
      &workbufLocal[tid*2*linesLocal],
      workbuf_wLocal,
      data_wLocal,
      workbuf_tLocal,
      tw_aLocal,
      Bn_aLocal,
      workbuf_bnLocal,
      edmaLocal+did,
      numLine);
    }

  /* the last line of 2nd dimension */

  if (tw_wLocal != NULL) {
    tw_aLocal = tw_wLocal;
  }
  else {
    tw_aLocal = twLocal;
  }

  if (Bn_wLocal != NULL) {
    Bn_aLocal = Bn_wLocal;
  } else {
    Bn_aLocal = BnLocal;
  }

  /* prepare FFT input buffer */
  lib_emt_copy1D1D(
    edmaLocal->channel[FFT_XFER_CHANNEL0],
    (void *)(stageLocal+n1Local*n2Local),
    (void *)workbuf_wLocal,
    n2Local*2*sizeof(float));

  lib_emt_wait((edmaLocal)->channel[FFT_XFER_CHANNEL0]);

  /* process the last line */
  if (n2Local == m2Local) {
    if (s_r3Local == 0 && s_r5Local == 0) {
         fft_sp_cmplx_notwid_br((double *)workbuf_wLocal, n2Local, (double *)data_wLocal, (double *)tw_aLocal);
    } else {
        fft_SPxSP_mixed_radix_c2c (n2Local, workbuf_wLocal, tw_aLocal, data_wLocal, n_p2Local, s_r3Local, s_r5Local);
    }
  } else {
      fft_SPxSP_c2c_bs (n2Local, m2Local, radLocal, workbuf_wLocal, data_wLocal, tw_aLocal, Bn_aLocal, workbuf_bnLocal);
  }

  /* export output data of last FFT, transposed */
   lib_emt_copy1D2D(
     edmaLocal->channel[FFT_XFER_CHANNEL1],
     (void *)data_wLocal,
     (void *)(workbufLocal+n1Local),
     2*sizeof(float),
     n2Local,
     (n1Local/2+1)*2*sizeof(float));

   lib_emt_wait((edmaLocal)->channel[FFT_XFER_CHANNEL1]);

    omp_set_num_threads (OMP_MAX_NUM_CORES);
    return error;
} /* fft_omp_sp_2d_r2c_ecpy */

/* ======================================================================== */
/*  End of file:  fft_omp_sp_2d_r2c.c                                       */
/* ------------------------------------------------------------------------ */
/*            Copyright (c) 2013 Texas Instruments, Incorporated.           */
/*                           All Rights Reserved.                           */
/* ======================================================================== */
