/* ======================================================================= */
/*  TEXAS INSTRUMENTS, INC.                                                */
/*                                                                         */
/*  FFTLIB  FFT Library                                                    */
/*                                                                         */
/* Copyright (C) 2013 Texas Instruments Incorporated - http://www.ti.com/  */ 
/*                                                                         */
/*                                                                         */
/*  Redistribution and use in source and binary forms, with or without     */
/*  modification, are permitted provided that the following conditions     */
/*  are met:                                                               */
/*                                                                         */
/*    Redistributions of source code must retain the above copyright       */
/*    notice, this list of conditions and the following disclaimer.        */
/*                                                                         */
/*    Redistributions in binary form must reproduce the above copyright    */
/*    notice, this list of conditions and the following disclaimer in the  */
/*    documentation and/or other materials provided with the               */
/*    distribution.                                                        */
/*                                                                         */
/*    Neither the name of Texas Instruments Incorporated nor the names of  */
/*    its contributors may be used to endorse or promote products derived  */
/*    from this software without specific prior written permission.        */
/*                                                                         */
/*  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS    */
/*  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT      */
/*  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR  */
/*  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT   */
/*  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,  */
/*  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT       */
/*  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,  */
/*  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY  */
/*  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT    */
/*  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE  */
/*  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.   */
/*                                                                         */
/* ======================================================================= */

#include <xdc/std.h>
#include "c6x.h"
#include "stdlib.h"
#include <ti/fftlib/src/common/ifft/ifft_sp_mixed_bs.h>
#include "ifft_sp_1d_c2c_batch.h"
#include <ti/fftlib/src/common/fft_edma.h>
#include <ti/fftlib/src/common/fft/fft_sp_cmplx_notwid_br.h>
#include <ti/fftlib/src/common/omp/omp_config.h>
#include <ti/runtime/openmp/omp.h>
#include <ti/libarch/libarch.h>

static int sp_1d_c2c_batch_ecpy (
    int   N,                  /* IFFT size                      */
    int   batch,              /* total number of lines          */
    int   M,                  /* Padded size of Anysize IFFT    */
    int   rad,                /* radix                          */
    int   N_p2,               /* power of 2 size in mixed radix */
    int   s_r3,               /* number stages of radix 3       */
    int   s_r5,               /* number stages of radix 5       */
    float *in,                /* input pointer                  */
    float *out,               /* output pointer                 */
    float *in_w,              /* input work buffer pointer      */
    float *out_w,             /* output working buffer pointer  */
    float *tw,                /* twiddle pointer                */
    float *Bn,
    float *workbuf,
    FFT_EDMA_Struct  *edma    /* edma resource pointer          */
)
{
    int   error = FFT_NOERR;
    int   j,ch_i,ch_o,ch_i1,ch_o1,dum0,dum1;
    int   insize  = N*2*sizeof(float),
          outsize = N*2*sizeof(float);

    float *restrict in_ptr = in, *restrict out_ptr = out;
    float *restrict input, *restrict output, *restrict in1, *restrict out1, *dum2, *dum3;

    /* prepare first IFFT input buffer */
    lib_emt_copy1D1D( 
      edma->channel[FFT_XFER_CHANNEL0], 
      (void *)in_ptr,
      (void *)in_w,
      insize);
    in_ptr += 2*N;

    /* set pingpong buffer pointers */
    in1    = in_w;
    out1   = out_w;
    input  = &in_w[2*N];
    output = &out_w[2*N];

    /* prepare second IFFT input buffer */
    lib_emt_copy1D1D( 
      edma->channel[FFT_XFER_CHANNEL2],
      (void *)in_ptr,
      (void *)input,
      insize);
    in_ptr += 2*N;

    /* wait for first IFFT input */
    lib_emt_wait(edma->channel[FFT_XFER_CHANNEL0]);

    /* first IFFT */
    if (N == M) {
      if (s_r3 == 0 && s_r5 == 0) {
        ifft_sp_cmplx_notwid_br((double *)in1, N, (double *)out1, (double *)tw);
      }
      else {
        ifft_SPxSP_mixed_radix_c2c (N, in1, tw, out1, N_p2, s_r3, s_r5);
      }
    }
    else
    {
      ifft_SPxSP_c2c_bs (N, M, rad, in1, out1, tw, Bn, workbuf);
    }

    /* export output data of first IFFT */
    lib_emt_copy1D1D( 
      edma->channel[FFT_XFER_CHANNEL1], 
      (void *)out1,
      (void *)out_ptr,
      outsize);
    out_ptr += 2*N;

    /* prepare third IFFT input buffer */
    lib_emt_copy1D1D( 
      edma->channel[FFT_XFER_CHANNEL0], 
      (void *)in_ptr,
      (void *)in_w,
      insize);
    in_ptr += 2*N;

    /* set channel numbers */
    ch_i  = FFT_XFER_CHANNEL2;
    ch_o  = FFT_XFER_CHANNEL3;
    ch_i1 = FFT_XFER_CHANNEL0;
    ch_o1 = FFT_XFER_CHANNEL1;

    /* wait for second IFFT input */
    lib_emt_wait(edma->channel[FFT_XFER_CHANNEL2]);

    for (j = 1; j < batch; j++) {

      if (N == M) {
        if (s_r3 == 0 && s_r5 == 0) {
          ifft_sp_cmplx_notwid_br((double *)input, N, (double *)output, (double *)tw);
        }
        else {
          ifft_SPxSP_mixed_radix_c2c (N, input, tw, output, N_p2, s_r3, s_r5);
        }
      }
      else
      {
        ifft_SPxSP_c2c_bs (N, M, rad, input, output, tw, Bn, workbuf);
      }

      /* export output data */
      lib_emt_copy1D1D( 
        edma->channel[ch_o],
        (void *)output,
        (void *)out_ptr,
        outsize);
      out_ptr += 2*N;

      /* prepare input data */
      lib_emt_copy1D1D( 
        edma->channel[ch_i],
        (void *)in_ptr,
        (void *)input,
        insize);
      in_ptr += 2*N;

      /* swap pingpong buffer pointers and XFER channel */
      dum2   = input;
      input  = in1;
      in1    = dum2;
      dum3   = output;
      output = out1;
      out1   = dum3;

      dum0   = ch_i;
      ch_i   = ch_i1;
      ch_i1  = dum0;
      dum1   = ch_o;
      ch_o   = ch_o1;
      ch_o1  = dum1;

      /* wait for EDMA */
      lib_emt_wait(edma->channel[ch_i]);
      lib_emt_wait(edma->channel[ch_o]);
    }

    /* wait for EDMA */
    lib_emt_wait(edma->channel[ch_i1]);
    lib_emt_wait(edma->channel[ch_o1]);

    return error;

} /* sp_1d_c2c_batch_ecpy */

static int radLocal, n_p2Local, s_r3Local, s_r5Local;
static int nLocal, batchLocal, batchPerCore, remainder, mLocal, numThread;
static float *inLocal, *outLocal, *BnLocal, *twLocal, *in_wLocal, 
              *out_wLocal, *Bn_wLocal, *workbuf_bnLocal, 
              *tw_wLocal, *Bn_aLocal, *tw_aLocal;
static FFT_EDMA_Struct *edmaLocal;
#ifndef FFT_MEM_MODEL_SM
#pragma DATA_SECTION (radLocal, ".mem_msm")
#pragma DATA_SECTION (n_p2Local, ".mem_msm")
#pragma DATA_SECTION (s_r3Local, ".mem_msm")
#pragma DATA_SECTION (s_r5Local, ".mem_msm")
#pragma DATA_SECTION (nLocal, ".mem_msm")
#pragma DATA_SECTION (batchLocal, ".mem_msm")
#pragma DATA_SECTION (numThread, ".mem_msm")
#pragma DATA_SECTION (batchPerCore, ".mem_msm")
#pragma DATA_SECTION (remainder, ".mem_msm")
#pragma DATA_SECTION (inLocal, ".mem_msm")
#pragma DATA_SECTION (outLocal, ".mem_msm")
#pragma DATA_SECTION (twLocal, ".mem_msm")
#pragma DATA_SECTION (in_wLocal, ".mem_msm")
#pragma DATA_SECTION (out_wLocal, ".mem_msm")
#pragma DATA_SECTION (tw_wLocal, ".mem_msm")
#pragma DATA_SECTION (Bn_aLocal, ".mem_msm")
#pragma DATA_SECTION (tw_aLocal, ".mem_msm")
#pragma DATA_SECTION (edmaLocal, ".mem_msm")
#endif

int ifft_sp_1d_c2c_batch_ecpy (
    /* int    N;       IFFT size        */
    /* int    batch;   batch size       */
    /* int    rad;     radix            */
    /* float *in;      input pointer    */
    /* float *out;     output pointer   */
    /* float *in_w;    input pointer    */
    /* float *out_w;   output pointer   */
    /* float *tw;      twiddle pointer  */
    fft_param_u u,
    void *edmaState,
    int   actualCoreNum
)
{
  int tid, did, batch, startidx;
  int error = FFT_NOERR;

  /* max of the N, Bnsize and twsize for internal memory usage */
  int twsize = u.sp_1d_c2c_batch_e.para1.twsize;
  int Bnsize = u.sp_1d_c2c_batch_e.para1.Bnsize;
  int N      = u.sp_1d_c2c_batch_e.N;
  lib_memdscr_t **fft_mem_handle = fftGetMemHandle();

  /* input parameters */
  inLocal    = u.sp_1d_c2c_batch_e.in;            /* data pointer                   */
  outLocal   = u.sp_1d_c2c_batch_e.out;           /* output pointer                 */
  twLocal    = u.sp_1d_c2c_batch_e.tw;            /* twiddle         buffer pointer */
  BnLocal    = u.sp_1d_c2c_batch_e.Bn;            /* Bn buffer pointer              */

  /* internal pointers */
  in_wLocal  = (float*)lib_smem_falloc (fft_mem_handle, 4*N*sizeof(float), 8);
  out_wLocal = (float*)lib_smem_falloc (fft_mem_handle, 4*N*sizeof(float), 8);
#ifdef FFT_MEM_MODEL_LG
  tw_wLocal  = (float*)lib_smem_falloc (fft_mem_handle, twsize*sizeof(float), 8);
#endif

#ifdef FFT_MEM_MODEL_LG
    if(in_wLocal==NULL || out_wLocal==NULL || tw_wLocal==NULL) {
#else
    tw_wLocal = NULL;
    if(in_wLocal==NULL || out_wLocal==NULL) {
#endif
        printf("Memory allocation error!\n");
        return FFT_ERROR;
    }

  if (u.sp_1d_c2c_batch_e.para1.use_bs) {
    Bn_wLocal       = tw_wLocal + twsize;         /* Bn working buffer pointer      */
    workbuf_bnLocal = Bn_wLocal + Bnsize;         /* workbuffer pointer for BS      */
  }else {
    Bn_wLocal = NULL;
    workbuf_bnLocal = NULL;
  }

  nLocal     = u.sp_1d_c2c_batch_e.N;             /* signal size                    */
  batchLocal = u.sp_1d_c2c_batch_e.batch;         /* batch size                     */
  mLocal     = u.sp_1d_c2c_batch_e.para1.M;       /* first dimension IFFT size M    */
  radLocal   = u.sp_1d_c2c_batch_e.para1.rad;     /* radix for N1                   */
    
  twsize     = u.sp_1d_c2c_batch_e.para1.twsize;  /* twiddle size in float words    */
  n_p2Local  = u.sp_1d_c2c_batch_e.para1.N_p2;    /* power of 2 size in mixed radix */
  s_r3Local  = u.sp_1d_c2c_batch_e.para1.s_r3;    /* number stages of radix 3       */
  s_r5Local  = u.sp_1d_c2c_batch_e.para1.s_r5;    /* number stages of radix 5       */
  Bnsize     = u.sp_1d_c2c_batch_e.para1.Bnsize;

  if ((actualCoreNum==1)||
      (actualCoreNum==2)||
      (actualCoreNum==4)||
      (actualCoreNum==8))
    omp_set_num_threads (actualCoreNum);
  else
    omp_set_num_threads (OMP_MAX_NUM_CORES);
  numThread = omp_get_max_threads();
  batchPerCore = batchLocal/numThread;
  remainder = batchLocal - numThread*batchPerCore;
  edmaLocal = (FFT_EDMA_Struct *)edmaState;

#pragma omp parallel private(did, tid, tw_aLocal, Bn_aLocal, batch, startidx) shared(radLocal, inLocal, \
  outLocal, BnLocal, twLocal, in_wLocal, out_wLocal, Bn_wLocal, tw_wLocal, workbuf_bnLocal, nLocal, \
  mLocal, n_p2Local, edmaLocal, twsize, Bnsize, numThread, batchLocal, batchPerCore, remainder)
  {
    tid = omp_get_thread_num();     // Obtain thread number
    did = DNUM;
  
    /* calculate batch size */
    batch = batchPerCore;
    startidx = batch*tid + remainder;
    if (tid < remainder) {
      batch += 1;
      startidx = batch*tid;
    }

    if (tw_wLocal != NULL) {
      /* prepare twiddle buffer */
      lib_emt_copy1D1D(
        ((edmaLocal+did)->channel[FFT_XFER_CHANNEL3]),
        (void *)twLocal,
        (void *)tw_wLocal,
        twsize*sizeof(float));
      tw_aLocal = tw_wLocal;
    }
    else {
      tw_aLocal = twLocal;
    }

    if (Bn_wLocal != NULL) {
      /* prepare Bn buffer */
      lib_emt_copy1D1D(
        ((edmaLocal+did)->channel[FFT_XFER_CHANNEL2]),
        (void *)BnLocal,
        (void *)Bn_wLocal,
        Bnsize*sizeof(float));
      Bn_aLocal = Bn_wLocal;
    } else {
      Bn_aLocal = BnLocal;
    }
    
    if (tw_wLocal != NULL)
      lib_emt_wait((edmaLocal+did)->channel[FFT_XFER_CHANNEL3]);
    if (Bn_wLocal != NULL)
      lib_emt_wait((edmaLocal+did)->channel[FFT_XFER_CHANNEL2]);

    /* batched ifft */
    error = sp_1d_c2c_batch_ecpy (
      nLocal,
      batch,
      mLocal,
      radLocal,
      n_p2Local,
      s_r3Local,
      s_r5Local,
      &inLocal[startidx*nLocal*2],
      &outLocal[startidx*nLocal*2],
      in_wLocal,
      out_wLocal,
      tw_aLocal,
      Bn_aLocal,
      workbuf_bnLocal,
      edmaLocal+did);
  }
  
  omp_set_num_threads (OMP_MAX_NUM_CORES);

  return error;
} /* ifft_sp_1d_c2c_batch_ecpy */

/* ======================================================================== */
/*  End of file:  ifft_sp_1d_c2c_batch.c                                    */
/* ------------------------------------------------------------------------ */
/*            Copyright (c) 2013 Texas Instruments, Incorporated.           */
/*                           All Rights Reserved.                           */
/* ======================================================================== */
