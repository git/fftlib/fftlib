/* ======================================================================= */
/*  TEXAS INSTRUMENTS, INC.                                                */
/*                                                                         */
/*  FFTLIB  FFT Library                                                    */
/*                                                                         */
/* Copyright (C) 2013 Texas Instruments Incorporated - http://www.ti.com/  */ 
/*                                                                         */
/*                                                                         */
/*  Redistribution and use in source and binary forms, with or without     */
/*  modification, are permitted provided that the following conditions     */
/*  are met:                                                               */
/*                                                                         */
/*    Redistributions of source code must retain the above copyright       */
/*    notice, this list of conditions and the following disclaimer.        */
/*                                                                         */
/*    Redistributions in binary form must reproduce the above copyright    */
/*    notice, this list of conditions and the following disclaimer in the  */
/*    documentation and/or other materials provided with the               */
/*    distribution.                                                        */
/*                                                                         */
/*    Neither the name of Texas Instruments Incorporated nor the names of  */
/*    its contributors may be used to endorse or promote products derived  */
/*    from this software without specific prior written permission.        */
/*                                                                         */
/*  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS    */
/*  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT      */
/*  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR  */
/*  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT   */
/*  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,  */
/*  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT       */
/*  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,  */
/*  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY  */
/*  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT    */
/*  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE  */
/*  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.   */
/*                                                                         */
/* ======================================================================= */

#include <ti/fftlib/src/common/fft_common.h>

#ifndef IFFT_SP_1D_C2R_H_
#define IFFT_SP_1D_C2R_H_ 1

/** @ingroup 1D */
/* @{ */

/** @defgroup IFFT_SP_1D_C2R */
/** @ingroup IFFT_SP_1D_C2R */
/* @{ */

#define IFFT_SP_1D_C2R_4STEP_MIN_SIZE  (     4*1024)
#define IFFT_SP_1D_C2R_4STEP_MAX_SIZE  (4*1024*1024)

/**
 *     @param IFFT_SP_1D_C2R_NUMOFLINEBUFS = number of cache lines to process each iteration
 */

#define IFFT_SP_1D_C2R_NUMOFLINEBUFS 2

/**
 *
 *     @param N = IFFT size dimension 1        
 *     @param mode = FFT_DIRECT & FFT_ECPY are supported for 1d IFFT
 */

/* IFFT plan 1D SP API */
fft_plan_t ifft_sp_plan_1d_c2r (
    int   N,        /* IFFT size */
    int   mode,     /* FFT_DIRECT or FFT_ECPY */
    fft_callout_t fxns
);

 /**
 *     This program performs one 1-dimensional single-precision complex-to-real IFFT.
 *  
 *     @param N = IFFT size      
 *     @param M = Power of 2 IFFT size, if Bluestein algorithm is used      
 *     @param twsize = Twiddle buffer size in bytes
 *     @param N_p2 = Power of 2 IFFT size in Mixed-radix IFFT
 *     @param s_r3 = Number of stages of radix-3
 *     @param s_r5 = Number of states of radix-5
 *     @param in = input pointer    
 *     @param out = output pointer   
 *     @param tw = twiddle pointer  
 *     @param Bn = Bluestein pointer  
 *     @param workbuf = Bluestein scratch pointer
 * 
 * @par Algorithm:
 *      Algorithm can run in radix 2, 3, 4 & 5 based upon value of N and Bluestein for all other sizes.<br>
 *
 * @par Assumptions:
 *      N is a positive value.<br>
 *      All input, output and working buffers are double word aligned.<br>
 *      No input, working or buffers overlap.<br>
 *
 * @par Size Limitations:
 *      Current limitations are: None.
 *
 * @par Implementation notes:
 * @b Endian Support: The code supports little endian modes.  <BR> 
 * @b Interruptibility: The code is interrupt-tolerant but not interruptible. <BR>
 *  
 */

/* IFFT execute 1D SP direct mode API */
int ifft_sp_1d_c2r_direct (
    /* int   N;        IFFT size         */
    /* float *in;      input pointer    */
    /* float *out;     output pointer   */
    /* float *tw;      twiddle pointer  */
    fft_param_u u,
    void  *edmaState
);


 /**
 *     This program performs one 1-dimensional single-precision complex-to-real IFFT.
 *  
 *     @param N = IFFT size      
 *     @param M = Power of 2 IFFT size, if Bluestein algorithm is used      
 *     @param twsize = Twiddle buffer size in bytes
 *     @param N_p2 = Power of 2 IFFT size in Mixed-radix IFFT
 *     @param s_r3 = Number of stages of radix-3
 *     @param s_r5 = Number of states of radix-5
 *     @param data = input pointer    
 *     @param work = output pointer   
 *     @param data_w = input/output buffer pointer    
 *     @param work_w = working buffer pointer  
 *     @param tw = twiddle pointer  
 *     @param tw_w = twiddle buffer pointer
 *     @param Bn = Bluestein pointer  
 *     @param Bn_w = Bluestein buffer pointer
 *     @param workbuf = Bluestein scratch pointer
 * 
 * @par Algorithm:
 *      Algorithm can run in radix 2, 3, 4 & 5 based upon value of N and Bluestein for all other sizes.<br>
 *
 * @par Assumptions:
 *      N is a positive value.<br>
 *      All input, output and working buffers are double word aligned.<br>
 *      No input, working or buffers overlap.<br>
 *
 * @par Size Limitations:
 *      Current limitations are: 1024 IFFT size with bluestein, 2048 IFFT size for power of 2.
 *      <br><b>Note:</b> 4_step ecpy will be utilized for IFFT sizes 2048 and above. 
 *
 * @par Implementation notes:
 * @b Endian Support: The code supports little endian modes.  <BR> 
 * @b Interruptibility: The code is interrupt-tolerant but not interruptible. <BR>
 *  
 */
 
/* IFFT execute 1D SP ecpy mode API */
int ifft_sp_1d_c2r_ecpy (
    /* int   N;        IFFT size                        */
    /* int   n1;       stage1 IFFT size                 */
    /* int   n2;       stage2 IFFT size                 */
    /* float *data;    data pointer                     */
    /* float *work;    working buffer pointer           */
    /* float *data_w;  data internal pointer            */
    /* float *work_w;  working buffer internal pointer  */
    /* float *tw;      twiddle pointer                  */
    /* float *tw_w;    twiddle internal pointer         */
    fft_param_u u,
    void *edmaState
);

 /**
 *     This program performs one 1-dimensional single-precision complex-to-real IFFT.
 *  
 *     @param n1 = IFFT size stage 1        
 *     @param n2 = IFFT size stage 2
 *     @param twsize = Twiddle buffer size in bytes
 *     @param N_p2 = stage 1 Power of 2 IFFT size in Mixed-radix IFFT
 *     @param s_r3 = stage 1 Number of stages of radix-3
 *     @param s_r5 = stage 1 Number of states of radix-5
 *     @param twsize1 = Twiddle buffer size for 1st stage of mixed-radix
 *     @param N_p21 = stage 2 Power of 2 IFFT size in Mixed-radix IFFT
 *     @param s_r31 = stage 2 Number of stages of radix-3
 *     @param s_r51 = stage 2 Number of states of radix-5
 *     @param data = input pointer    
 *     @param work = output pointer   
 *     @param data_w = input/output buffer pointer    
 *     @param work_w = working buffer pointer  
 *     @param work_t = Cache line buffer pointer  
 *     @param tw = twiddle pointer  
 *     @param tw_w = twiddle buffer pointer
 * 
 * @par Algorithm:
 *      Algorithm can run in radix 2, 3, 4 & 5 based upon value of N. <br>
 *
 * @par Assumptions:
 *      N is a positive value and multiple of 128.<br>
 *      All input, output and working buffers are double word aligned.<br>
 *      No input, working or buffers overlap.<br>
 *
 * @par Size Limitations:
 *      Current limitations are: 2048*2048 IFFT size for power of 2. 
 *
 * @par Implementation notes:
 * @b Endian Support: The code supports little endian modes.  <BR> 
 * @b Interruptibility: The code is interrupt-tolerant but not interruptible. <BR>
 *  
 */
 
/* IFFT execute 1D SP 4-step ecpy mode API */
int ifft_sp_1d_c2r_4step_ecpy (
    /* int   N;        IFFT size                        */
    /* int   n1;       stage1 IFFT size                 */
    /* int   n2;       stage2 IFFT size                 */
    /* float *data;    data pointer                     */
    /* float *work;    working buffer pointer           */
    /* float *data_w;  data internal pointer            */
    /* float *work_w;  working buffer internal pointer  */
    /* float *tw;      twiddle pointer                  */
    /* float *tw_w;    twiddle internal pointer         */
    fft_param_u u,
    void *edmaState
);
 
 /* }@ */ /* ingroup */
 /* }@ */ /* ingroup */
 
#endif /* IFFT_SP_1D_C2R_H_ */

/* ======================================================================== */
/*  End of file:  ifft_sp_1d_c2r.h                                          */
/* ------------------------------------------------------------------------ */
/*            Copyright (c) 2013 Texas Instruments, Incorporated.           */
/*                           All Rights Reserved.                           */
/* ======================================================================== */


