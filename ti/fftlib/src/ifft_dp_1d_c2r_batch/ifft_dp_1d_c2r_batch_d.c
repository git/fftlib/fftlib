/* ======================================================================= */
/*  TEXAS INSTRUMENTS, INC.                                                */
/*                                                                         */
/*  FFTLIB  FFT Library                                                    */
/*                                                                         */
/* Copyright (C) 2013 Texas Instruments Incorporated - http://www.ti.com/  */ 
/*                                                                         */
/*                                                                         */
/*  Redistribution and use in source and binary forms, with or without     */
/*  modification, are permitted provided that the following conditions     */
/*  are met:                                                               */
/*                                                                         */
/*    Redistributions of source code must retain the above copyright       */
/*    notice, this list of conditions and the following disclaimer.        */
/*                                                                         */
/*    Redistributions in binary form must reproduce the above copyright    */
/*    notice, this list of conditions and the following disclaimer in the  */
/*    documentation and/or other materials provided with the               */
/*    distribution.                                                        */
/*                                                                         */
/*    Neither the name of Texas Instruments Incorporated nor the names of  */
/*    its contributors may be used to endorse or promote products derived  */
/*    from this software without specific prior written permission.        */
/*                                                                         */
/*  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS    */
/*  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT      */
/*  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR  */
/*  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT   */
/*  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,  */
/*  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT       */
/*  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,  */
/*  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY  */
/*  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT    */
/*  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE  */
/*  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.   */
/*                                                                         */
/* ======================================================================= */

#include <xdc/std.h>
#include <stdio.h>
#include <time.h>
#include <stdlib.h>
#include <limits.h>
#include <math.h>
#include <c6x.h>

#include <ti/fftlib/src/common/omp/omp_config.h>
#include <ti/fftlib/src/common/fft_common_d.h>
#include "ifft_dp_1d_c2r_batch.h"
#include <ti/fftlib/src/common/ifft/ifft_dp_mixed_bs.h>
#include <ti/runtime/openmp/omp.h>

void tw_gen_cn (double *w, int n);
void idft_dp (int N, double x[], double y[], int N1);

/* ======================================================================== */
/*  Kernel-specific alignments                                              */
/* ======================================================================== */
#pragma DATA_SECTION(x_i,  ".mem_ddr");
#pragma DATA_SECTION(y_i,  ".mem_ddr");
#pragma DATA_SECTION(x_cn, ".mem_ddr");
#pragma DATA_SECTION(y_cn, ".mem_ddr");
#pragma DATA_SECTION(w_cn, ".mem_ddr");
#pragma DATA_SECTION(w_i,  ".mem_ddr");
#if ANYSIZE_SUPPORT
#pragma DATA_SECTION(Bn_i, ".mem_ddr");
#pragma DATA_ALIGN(Bn_i, 8);
#endif

#pragma DATA_ALIGN(x_i,  8);
#pragma DATA_ALIGN(x_cn, 8);

#pragma DATA_ALIGN(w_cn, 8);
#pragma DATA_ALIGN(w_i,  8);
#pragma DATA_ALIGN(y_i,  8);
#pragma DATA_ALIGN(y_cn, 8);

#pragma DATA_SECTION(local_work, ".mem_l2");
#pragma DATA_ALIGN  (local_work,  64);

/* ======================================================================== */
/*  Parameters of fixed dataset.                                            */
/* ======================================================================== */

#if ANYSIZE_SUPPORT
#define MAXN  (2*1024)
#else
#ifdef FFT_MEM_MODEL_LG
#define MAXN  (8*1024)
#else
# ifdef FFT_MEM_MODEL_MED
# define MAXN  (4*1024)
# else
#   ifdef FFT_MEM_MODEL_SM
#   define MAXN  (2*1024)
#   else
#     error "Unsupported MEM MODEL!"
#   endif
# endif
#endif
#endif
#define BATCH (100)
#define MB    (2*BATCH*MAXN)
#define PAD   (0)

/* ======================================================================== */
/*  Initialized arrays with fixed test data.                                */
/* ======================================================================== */

double x_i [MB + 2 * PAD];
double x_cn[MB + 2 * PAD];


double w_cn[2*MAXN + 2 * PAD];

double y_i [MB + 2 * PAD];
double y_cn[MB + 2 * PAD];

#ifdef FFT_MEM_MODEL_LG
double local_work[16432 + 1024 + 6*MAXN + 2 * PAD];
#else
# ifdef FFT_MEM_MODEL_MED
  double local_work[1024 + 6*MAXN + 2 * PAD];
# else
#   ifdef FFT_MEM_MODEL_SM
    double local_work[1024 + 6*MAXN + 2 * PAD];
#   else
#     error "Unsupported MEM MODEL!"
#   endif
# endif
#endif

#if ANYSIZE_SUPPORT

double Bn_i [4*MAXN + 2 * PAD];
double w_i  [6*MAXN + 2 * PAD];
double Bn_i_work [4*MAXN + 2 * PAD];
double workbuf [8*MAXN + 2 * PAD];

#else
double w_i  [3*MAXN + 2 * PAD];
#endif

/* ======================================================================== */
/*  Generate pointers to skip beyond array padding                          */
/* ======================================================================== */
double *const ptr_x_i  = x_i  + PAD;
double *const ptr_x_cn = x_cn + PAD;

double *const ptr_w_i  = w_i  + PAD;
double *const ptr_w_cn = w_cn + PAD;

double *const ptr_y_i  = y_i  + PAD;
double *const ptr_y_cn = y_cn + PAD;

double *const ptr_local_work  = local_work + PAD;

#if ANYSIZE_SUPPORT
double *const ptr_Bn_i_work  = Bn_i_work + PAD;
double *const ptr_Bn_i       = Bn_i + PAD;
double *const ptr_workbuf    = workbuf + PAD;
#else
double *const ptr_Bn_i_work  = NULL;
double *const ptr_Bn_i       = NULL;
double *const ptr_workbuf    = NULL;
#endif

/* calculate radix for 1D array */
static void calculate_rad (int N, fft_para_mix_bs_t *fft_para)
{
  int n, j, M, rad;
  int s_r3, s_r5, N_p2,dum;
  int use_bs;

  use_bs = 1;

  n = (N<<1)-1;
  j = _norm(n);
  M = 1 << (31-j);

  N_p2 = N/2;
  s_r3 = 0;
  s_r5 = 0;

  /* if N is odd, bypass the rest of calculation, use Bluestein */
  if (((N>>1)<<1) == N)
  {
    /* if N is power of 2, no zero padding, regular radix 2 or 4 fft */
    if (N == (M>>1))
    { /* power of 2 */
      M = N;
      j = j+1;
      use_bs = 0;
    }
    else {
      dum = N/2;
      while (dum/3*3 == dum) {
        s_r3++;
        dum /= 3;
      }
      while (dum/5*5 == dum) {
        s_r5++;
        dum /= 5;
      }
      if ((dum >= 4) && (dum == (1 << (30-_norm(dum))))) {
      /* mixed radix 2 and 3 */
        N_p2 = dum;
        use_bs = 0;
        M = N;
        j  = _norm(dum);
      }
    }
  }

  /* calculate radix for radix 2 or 4 c2r fft */
  if (j%2 == use_bs)
    rad = 4;
  else
    rad = 2;

  fft_para->N_p2   = N_p2;
  fft_para->s_r3   = s_r3;
  fft_para->s_r5   = s_r5;
  fft_para->M      = M;
  fft_para->rad    = rad;
  fft_para->use_bs = use_bs;

} /* calculate_rad */

/* calculate twiddle size, Bn buffer size and workbuffer size */
static void calculate_mem_size(int N, fft_para_mix_bs_t *fft_para, int *twsize, int *Bnsize, int *worksize)
{
  int dum, n, i;
  int use_bs = fft_para->use_bs;
  int M = fft_para->M;
  int s_r3 = fft_para->s_r3;
  int s_r5 = fft_para->s_r5;
  int N_p2 = fft_para->N_p2;

  /* Please note that Bn buffer and Workbuf are only used when doing Bluestein
   * meaning when use_bs == 1 */

  if (use_bs == 1)
  { /* when size is not power of 2 and not multiple of 3&5 */
  *twsize   = (2*M+2*N);
  *Bnsize   = (2*M);
  *worksize = (4*M);
  }
  else
  {
    if ((s_r3 == 0) && (s_r5 == 0)) {
    /* When signal is power of 2 */
      *twsize = N*3/2;
    }
    else {
    /* When signal is power of 3 or 5 */
      dum = 0;
      n   = 2*N;
      for (i = 0; i < s_r3; i++) {
        dum += n/3;
        n   /= 3;
      }
      n *= 2;  /* radix-5 has 8 twiddle vs radix-3 4 twiddles */
      for (i = 0; i < s_r5; i++) {
        dum += n/5;
        n   /= 5;
      }
      *twsize = (N/2+2*N_p2+dum);
    }
    *Bnsize   = 0;
    *worksize = 0;
  }
} /* calculate_mem_size */

/* ======================================================================== */
/*  MAIN -- Top level driver for the test.                                  */
/* ======================================================================== */

int main ()
{
    int    i, j, N;
    int    rad_cn;
    int    int_tw_size, twsize, localsize;
    int    Bnsize, worksize, int_Bn_size, work_size;

    clock_t t_start, t_stop, t_overhead, t_opt;
    double   diff, max_diff = 0;
    double  *ptr_scratch = NULL;

    fft_plan_t p;
    fft_callout_t plan_fxns;
    size_t  l2_SRAM_size_orig;
    uint32_t *temp;
    lib_memdscr_t **fft_mem_handle = fftGetMemHandle();

    /* --------------------------------------------------------------------- */
    /* intialize hardware timers                                             */
    /* --------------------------------------------------------------------- */
    TSCL=0;TSCH=0;

    /* initalize callout functions */
    plan_fxns.memoryRequest   = NULL;
    plan_fxns.memoryRelease   = NULL;
    plan_fxns.ecpyRequest     = fft_omp_assign_edma_resources;
    plan_fxns.ecpyRelease     = fft_omp_free_edma_resources;
    
    omp_set_num_threads (OMP_MAX_NUM_CORES);
    fft_config_memory (&l2_SRAM_size_orig);
    temp = (uint32_t *)lib_smem_falloc(fft_mem_handle, 256*6*sizeof(uint32_t), 3);
#if 1
    if(temp==NULL) {
        printf("Memory allocation error!\n");
        return;
    }
#endif

    /* initialize ECPY */
    #pragma omp parallel
    {
      fft_assert( (lib_emt_init() == LIB_EMT_SUCCESS), DNUM, "lib_emt_init() return error!");
      fftEdmaState[DNUM] = FFT_EDMA_STATE_INIT;
    }

    for (N = 128; N <= MAXN; N *= 2)
    {
        /* ---------------------------------------------------------------- */
        /* Initialize input vector temporarily.                             */
        /* ---------------------------------------------------------------- */
        memset (x_i,  0x55, sizeof (x_i) );
        memset (x_cn, 0x55, sizeof (x_cn));

        for (j = 0; j < BATCH; j++) {
          for (i = 1; i < N/2; i++) {
              x_cn[2*j*N + PAD + 2*i    ]       = sin (2 * 3.1415 * ( 50+j) * i / (double) N);
              x_cn[2*j*N + PAD + 2*i + 1]       = sin (2 * 3.1415 * (100+j) * i / (double) N);
              x_cn[2*j*N + PAD + 2*N - 2*i    ] =  x_cn[2*j*N + PAD + 2*i    ];
              x_cn[2*j*N + PAD + 2*N - 2*i + 1] = -x_cn[2*j*N + PAD + 2*i + 1];
              x_i [j*(N+2) + PAD + 2*i    ]     = sin (2 * 3.1415 * ( 50+j) * i / (double) N);
              x_i [j*(N+2) + PAD + 2*i + 1]     = sin (2 * 3.1415 * (100+j) * i / (double) N);
          }
          x_cn [2*j*N + PAD + 0]   = x_i [j*(N+2) + PAD + 0]   = 2;
          x_cn [2*j*N + PAD + 1]   = x_i [j*(N+2) + PAD + 1]   = 0;
          x_cn [2*j*N + PAD + N]   = x_i [j*(N+2) + PAD + N]   = 3;
          x_cn [2*j*N + PAD + N+1] = x_i [j*(N+2) + PAD + N+1] = 0;
        }

        /* ---------------------------------------------------------------- */
        /* Force uninitialized arrays to fixed values.                      */
        /* ---------------------------------------------------------------- */
        memset (y_i,  0xA5, sizeof (y_i) );
        memset (y_cn, 0xA5, sizeof (y_cn));

        /* ---------------------------------------------------------------- */
        /* Compute the overhead of calling clock twice to get timing info   */
        /* ---------------------------------------------------------------- */
        t_start = _itoll(TSCH, TSCL);
        t_stop  = _itoll(TSCH, TSCL);
        t_overhead = t_stop - t_start;

        /* ---------------------------------------------------------------- */
        /* Generate twiddle factors.                                        */
        /* ---------------------------------------------------------------- */
        j = 0;
        for (i = 0; i <= 31; i++)
            if ((N & (1 << i)) == 0)
                j++;
            else
                break;

        if (j % 2 == 0) {
            rad_cn = 4;
        }
        else {
            rad_cn = 2;
        }

        tw_gen_cn (ptr_w_cn, N);

        printf("ifft_dp_1d_c2r_batch_ecpy\tsize=#: %d\t", N);
        t_start = _itoll(TSCH, TSCL);
        for (j = 0; j < BATCH; j++) {
          DSPF_dp_ifftDPxDP (N, ptr_x_cn+(2*j*N), ptr_w_cn, ptr_y_cn+(2*j*N), rad_cn, 0, N);
//          idft_dp (N, ptr_x_cn+(2*j*N), ptr_y_cn+(2*j*N), N);
        }
        t_stop = _itoll(TSCH, TSCL);

        /*********************************************
         *  ecpy FFT test
         *********************************************/
        plan_fxns.ecpyRequest     = fft_omp_assign_edma_resources;
        plan_fxns.ecpyRelease     = fft_omp_free_edma_resources;

        /* ARM part of plan */
        /* determine the rad */
        p.u.dp_1d_c2r_batch_e.batch = BATCH;
        calculate_rad(N, &p.u.dp_1d_c2r_batch_e.para1);

        /* Calculate mem size for ECPY */

        calculate_mem_size(N, &p.u.dp_1d_c2r_batch_e.para1, &twsize, &Bnsize, &worksize);
        p.u.dp_1d_c2r_batch_e.para1.twsize = twsize;
        p.u.dp_1d_c2r_batch_e.para1.Bnsize = Bnsize;
        int_tw_size = twsize;
        int_Bn_size = Bnsize;
        work_size   = worksize;

        /* calculate local memory requirements */
#ifdef FFT_MEM_MODEL_LG
        localsize = sizeof(double)*(1024 + 6*N + (int_tw_size + int_Bn_size + work_size));
#else
# ifdef FFT_MEM_MODEL_MED
        localsize = sizeof(double)*(1024 + 6*N);
# else
#   ifdef FFT_MEM_MODEL_SM
        localsize = sizeof(double)*(1024 + 6*N);
#   else
#     error "Unsupported MEM MODEL!"
#   endif
# endif
#endif

        ptr_scratch = malloc(sizeof(double)*work_size);
        ifft_dp_plan_1d_c2r_batch (N, FFT_ECPY, plan_fxns, &p, ptr_x_i ,ptr_y_i, ptr_w_i, ptr_scratch);
        free(ptr_scratch);
        /* ---------------------------------------------------------------- */
        /* Compute the overhead of calling clock twice to get timing info   */
        /* ---------------------------------------------------------------- */
        p.edmaState = (*p.fftcout.ecpyRequest)(temp, FFT_NUM_EDMA_CH*FFT_MAX_EDMA_LINKS_3D*sizeof(float));
        (*p.fftcout.ecpyRelease)(p.edmaState);
        p.edmaState = (*p.fftcout.ecpyRequest)(temp, FFT_NUM_EDMA_CH*FFT_MAX_EDMA_LINKS_3D*sizeof(float));
        (*p.fftcout.ecpyRelease)(p.edmaState);
        t_start = _itoll(TSCH, TSCL);
        p.edmaState = (*p.fftcout.ecpyRequest)(temp, FFT_NUM_EDMA_CH*FFT_MAX_EDMA_LINKS_3D*sizeof(float));
        (*p.fftcout.ecpyRelease)(p.edmaState);
        t_stop  = _itoll(TSCH, TSCL);
        t_overhead = t_stop - t_start;

        /* ---------------------------------------------------------------------- */
        /* Set the number of cores used                                           */
        /* ---------------------------------------------------------------------- */
        p.actualCoreNum = ACTIVE_THREAD_COUNT;
        p.local = local_work;

        t_start = _itoll(TSCH, TSCL);
        fft_execute (p);
        t_stop = _itoll(TSCH, TSCL);
        t_opt = t_stop - t_start - t_overhead;

        /* ---------------------------------------------------------------- */
        /* compute difference and track max difference                      */  
        /* ---------------------------------------------------------------- */
        diff = 0; max_diff = 0;
        for(i=0; i<N*BATCH; i++) {
          diff = ptr_y_cn[2*i] - ptr_y_i[i];
          if (diff < 0) diff *= -1;
          if (diff > max_diff)
           max_diff = diff;
        }
        printf("max_diff = %f", max_diff);

        printf("\tN = %d\tCycle: %d\n\n", N, t_opt);

    }

    /* radix-3,5 testing */
    for (N = 120; N <= MAXN; N *= 15)
    {
        memset (x_i,  0x55, sizeof (x_i) );
        memset (x_cn, 0x55, sizeof (x_cn));

        /* ---------------------------------------------------------------- */
        /* Initialize input vector temporarily.                             */
        /* ---------------------------------------------------------------- */

        for (j = 0; j < BATCH; j++) {
          for (i = 1; i < N/2; i++) {
              x_cn[2*j*N + PAD + 2*i    ]       = sin (2 * 3.1415 * ( 50+j) * i / (double) N);
              x_cn[2*j*N + PAD + 2*i + 1]       = sin (2 * 3.1415 * (100+j) * i / (double) N);
              x_cn[2*j*N + PAD + 2*N - 2*i    ] =  x_cn[2*j*N + PAD + 2*i    ];
              x_cn[2*j*N + PAD + 2*N - 2*i + 1] = -x_cn[2*j*N + PAD + 2*i + 1];
              x_i [j*(N+2) + PAD + 2*i    ]     = sin (2 * 3.1415 * ( 50+j) * i / (double) N);
              x_i [j*(N+2) + PAD + 2*i + 1]     = sin (2 * 3.1415 * (100+j) * i / (double) N);
          }
          x_cn [2*j*N + PAD + 0]   = x_i [j*(N+2) + PAD + 0]   = 2;
          x_cn [2*j*N + PAD + 1]   = x_i [j*(N+2) + PAD + 1]   = 0;
          x_cn [2*j*N + PAD + N]   = x_i [j*(N+2) + PAD + N]   = 3;
          x_cn [2*j*N + PAD + N+1] = x_i [j*(N+2) + PAD + N+1] = 0;
        }

        /* ---------------------------------------------------------------- */
        /* Force uninitialized arrays to fixed values.                      */
        /* ---------------------------------------------------------------- */
        memset (y_i,  0xA5, sizeof (y_i) );
        memset (y_cn, 0xA5, sizeof (y_cn));

        printf("ifft_dp_1d_c2r_batch_ecpy\tsize=#: %d\t", N);
        t_start = _itoll(TSCH, TSCL);
        for (j = 0; j < BATCH; j++) {
          idft_dp (N, ptr_x_cn+(2*j*N), ptr_y_cn+(2*j*N), N);
        }
        t_stop = _itoll(TSCH, TSCL);

        /*********************************************
         *  ecpy FFT test
         *********************************************/
        plan_fxns.ecpyRequest     = fft_omp_assign_edma_resources;
        plan_fxns.ecpyRelease     = fft_omp_free_edma_resources;

        /* ARM part of plan */
        /* determine the rad */
        p.u.dp_1d_c2r_batch_e.batch = BATCH;
        calculate_rad(N, &p.u.dp_1d_c2r_batch_e.para1);

        /* Calculate mem size for ECPY */

        calculate_mem_size(N, &p.u.dp_1d_c2r_batch_e.para1, &twsize, &Bnsize, &worksize);
        p.u.dp_1d_c2r_batch_e.para1.twsize = twsize;
        p.u.dp_1d_c2r_batch_e.para1.Bnsize = Bnsize;
        int_tw_size = twsize;
        int_Bn_size = Bnsize;
        work_size   = worksize;

        /* calculate local memory requirements */
#ifdef FFT_MEM_MODEL_LG
        localsize = sizeof(double)*(1024 + 6*N + (int_tw_size + int_Bn_size + work_size));
#else
# ifdef FFT_MEM_MODEL_MED
        localsize = sizeof(double)*(1024 + 6*N);
# else
#   ifdef FFT_MEM_MODEL_SM
        localsize = sizeof(double)*(1024 + 6*N);
#   else
#     error "Unsupported MEM MODEL!"
#   endif
# endif
#endif

        ptr_scratch = malloc(sizeof(double)*work_size);
        ifft_dp_plan_1d_c2r_batch (N, FFT_ECPY, plan_fxns, &p, ptr_x_i ,ptr_y_i, ptr_w_i, ptr_scratch);
        free(ptr_scratch);
        /* ---------------------------------------------------------------- */
        /* Compute the overhead of calling clock twice to get timing info   */
        /* ---------------------------------------------------------------- */
        p.edmaState = (*p.fftcout.ecpyRequest)(temp, FFT_NUM_EDMA_CH*FFT_MAX_EDMA_LINKS_3D*sizeof(float));
        (*p.fftcout.ecpyRelease)(p.edmaState);
        p.edmaState = (*p.fftcout.ecpyRequest)(temp, FFT_NUM_EDMA_CH*FFT_MAX_EDMA_LINKS_3D*sizeof(float));
        (*p.fftcout.ecpyRelease)(p.edmaState);
        t_start = _itoll(TSCH, TSCL);
        p.edmaState = (*p.fftcout.ecpyRequest)(temp, FFT_NUM_EDMA_CH*FFT_MAX_EDMA_LINKS_3D*sizeof(float));
        (*p.fftcout.ecpyRelease)(p.edmaState);
        t_stop  = _itoll(TSCH, TSCL);
        t_overhead = t_stop - t_start;

        /* ---------------------------------------------------------------------- */
        /* Set the number of cores used                                           */
        /* ---------------------------------------------------------------------- */
        p.actualCoreNum = ACTIVE_THREAD_COUNT;
        p.local = local_work;

        t_start = _itoll(TSCH, TSCL);
        fft_execute (p);
        t_stop = _itoll(TSCH, TSCL);
        t_opt = t_stop - t_start - t_overhead;

        /* ---------------------------------------------------------------- */
        /* compute difference and track max difference                      */
        /* ---------------------------------------------------------------- */
        diff = 0; max_diff = 0;
        for(i=0; i<N*BATCH; i++) {
          diff = ptr_y_cn[2*i] - ptr_y_i[i];
          if (diff < 0) diff *= -1;
          if (diff > max_diff)
           max_diff = diff;
        }
        printf("max_diff = %f", max_diff);

        printf("\tN = %d\tCycle: %d\n\n", N, t_opt);
    }

#if ANYSIZE_SUPPORT
    /* BS testing */
    for (N = 16; N <= MAXN; N = N+33)
    {
        memset (x_i,  0x55, sizeof (x_i) );
        memset (x_cn, 0x55, sizeof (x_cn));

        /* ---------------------------------------------------------------- */
        /* Initialize input vector temporarily.                             */
        /* ---------------------------------------------------------------- */

        for (j = 0; j < BATCH; j++) {
          for (i = 1; i < N/2; i++) {
              x_cn[2*j*N + PAD + 2*i    ]       = sin (2 * 3.1415 * ( 50+j) * i / (double) N);
              x_cn[2*j*N + PAD + 2*i + 1]       = sin (2 * 3.1415 * (100+j) * i / (double) N);
              x_cn[2*j*N + PAD + 2*N - 2*i    ] =  x_cn[2*j*N + PAD + 2*i    ];
              x_cn[2*j*N + PAD + 2*N - 2*i + 1] = -x_cn[2*j*N + PAD + 2*i + 1];
              x_i [j*(N+2) + PAD + 2*i    ]     = sin (2 * 3.1415 * ( 50+j) * i / (double) N);
              x_i [j*(N+2) + PAD + 2*i + 1]     = sin (2 * 3.1415 * (100+j) * i / (double) N);
          }
          x_cn [2*j*N + PAD + 0]   = x_i [j*(N+2) + PAD + 0]   = 2;
          x_cn [2*j*N + PAD + 1]   = x_i [j*(N+2) + PAD + 1]   = 0;
          x_cn [2*j*N + PAD + N]   = x_i [j*(N+2) + PAD + N]   = 3;
          x_cn [2*j*N + PAD + N+1] = x_i [j*(N+2) + PAD + N+1] = 0;
        }

        /* ---------------------------------------------------------------- */
        /* Force uninitialized arrays to fixed values.                      */
        /* ---------------------------------------------------------------- */
        memset (y_i,  0xA5, sizeof (y_i) );
        memset (y_cn, 0xA5, sizeof (y_cn));

        printf("ifft_dp_1d_c2r_batch_ecpy\tsize=#: %d\t", N);
        t_start = _itoll(TSCH, TSCL);
        for (j = 0; j < BATCH; j++) {
          idft_dp (N, ptr_x_cn+(2*j*N), ptr_y_cn+(2*j*N), N);
        }
        t_stop = _itoll(TSCH, TSCL);

        /*********************************************
         *  ecpy FFT test
         *********************************************/
        plan_fxns.ecpyRequest     = fft_omp_assign_edma_resources;
        plan_fxns.ecpyRelease     = fft_omp_free_edma_resources;

        /* ARM part of plan */
        /* determine the rad */
        p.u.dp_1d_c2r_batch_e.batch = BATCH;
        calculate_rad(N, &p.u.dp_1d_c2r_batch_e.para1);

        /* Calculate mem size for ECPY */

        calculate_mem_size(N, &p.u.dp_1d_c2r_batch_e.para1, &twsize, &Bnsize, &worksize);
        p.u.dp_1d_c2r_batch_e.para1.twsize = twsize;
        p.u.dp_1d_c2r_batch_e.para1.Bnsize = Bnsize;
        int_tw_size = twsize;
        int_Bn_size = Bnsize;
        work_size   = worksize;

        /* calculate local memory requirements */
#ifdef FFT_MEM_MODEL_LG
        localsize = sizeof(double)*(1024 + 6*N + (int_tw_size + int_Bn_size + work_size));
#else
# ifdef FFT_MEM_MODEL_MED
        localsize = sizeof(double)*(1024 + 6*N);
# else
#   ifdef FFT_MEM_MODEL_SM
        localsize = sizeof(double)*(1024 + 6*N);
#   else
#     error "Unsupported MEM MODEL!"
#   endif
# endif
#endif

        ptr_scratch = malloc(sizeof(double)*work_size);
        ifft_dp_plan_1d_c2r_batch (N, FFT_ECPY, plan_fxns, &p, ptr_x_i ,ptr_y_i, ptr_w_i, ptr_scratch);
        free(ptr_scratch);
        /* ---------------------------------------------------------------- */
        /* Compute the overhead of calling clock twice to get timing info   */
        /* ---------------------------------------------------------------- */
        p.edmaState = (*p.fftcout.ecpyRequest)(temp, FFT_NUM_EDMA_CH*FFT_MAX_EDMA_LINKS_3D*sizeof(float));
        (*p.fftcout.ecpyRelease)(p.edmaState);
        p.edmaState = (*p.fftcout.ecpyRequest)(temp, FFT_NUM_EDMA_CH*FFT_MAX_EDMA_LINKS_3D*sizeof(float));
        (*p.fftcout.ecpyRelease)(p.edmaState);
        t_start = _itoll(TSCH, TSCL);
        p.edmaState = (*p.fftcout.ecpyRequest)(temp, FFT_NUM_EDMA_CH*FFT_MAX_EDMA_LINKS_3D*sizeof(float));
        (*p.fftcout.ecpyRelease)(p.edmaState);
        t_stop  = _itoll(TSCH, TSCL);
        t_overhead = t_stop - t_start;

        /* ---------------------------------------------------------------------- */
        /* Set the number of cores used                                           */
        /* ---------------------------------------------------------------------- */
        p.actualCoreNum = ACTIVE_THREAD_COUNT;
        p.local = local_work;

        t_start = _itoll(TSCH, TSCL);
        fft_execute (p);
        t_stop = _itoll(TSCH, TSCL);
        t_opt = t_stop - t_start - t_overhead;

        /* ---------------------------------------------------------------- */
        /* compute difference and track max difference                      */
        /* ---------------------------------------------------------------- */
        diff = 0; max_diff = 0;
        for(i=0; i<N*BATCH; i++) {
          diff = ptr_y_cn[2*i] - ptr_y_i[i];
          if (diff < 0) diff *= -1;
          if (diff > max_diff)
           max_diff = diff;
        }
        printf("max_diff = %f", max_diff);

        printf("\tN = %d\tCycle: %d\n\n", N, t_opt);
    }
#endif
}

/* Function for generating Specialized sequence of twiddle factors */
void tw_gen_cn (double *w, int n)
{
    int i, j, k;
    const double PI = 3.14159265358979323846;

    for (j = 1, k = 0; j <= n >> 2; j = j << 2)
    {
        for (i = 0; i < n >> 2; i += j)
        {
            w[k]     = (double) cos (2 * PI * i / n);
            w[k + 1] = (double) sin (2 * PI * i / n);
            w[k + 2] = (double) cos (4 * PI * i / n);
            w[k + 3] = (double) sin (4 * PI * i / n);
            w[k + 4] = (double) cos (6 * PI * i / n);
            w[k + 5] = (double) sin (6 * PI * i / n);

            k += 6;
        }
    }
}

void idft_dp (int N, double x[], double y[], int N1)
{
    int k, i, index;
    const double PI = 3.141592654;
    double *p_x;
    double arg, fx_0, fx_1, fy_0, fy_1, co, si;

    for (k = 0; k < N1; k++)
    {
        p_x = x;

        fy_0 = 0;
        fy_1 = 0;
        for (i = 0; i < N; i++)
        {
            fx_0 = p_x[0];
            fx_1 = p_x[1];
            p_x += 2;
            index = (i * k) % N;
            arg = 2 * PI * index / N;
            co = cos (arg);
            si = sin (arg);
            fy_0 += ((fx_0 * co) - (fx_1 * si));
            fy_1 += ((fx_1 * co) + (fx_0 * si));
        }
        y[2 * k] = fy_0;
        y[2 * k + 1] = fy_1;
    }
}
/* ======================================================================== */
/*  End of file:  fft_dp_1d_c2r_batch_d.c                                   */
/* ------------------------------------------------------------------------ */
/*            Copyright (c) 2013 Texas Instruments, Incorporated.           */
/*                           All Rights Reserved.                           */
/* ======================================================================== */
