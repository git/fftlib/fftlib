; =================================================================================== ;
; ENSA OF MARRAKECH - THALES AIR SYSTEMS                                              ;
;                                                                                     ;
; miss_pipe_notwid_br.asm -- standard c66x asm implementation                         ;
;       * title : Miss Pipelining routine to the L1D memory level                     ;
;       * description : transfers a buffer to L1D in a miss pipelined fashion         ;
;                       accesses 2 new L1D cache lines during each cycle              ;
;       * author : Mounir BAHTAT                                                      ;
;       * version : 1.0.0                                                             ;
;       * prototype : void _miss_pipe_notwid_br(double *src, int lgth)                ;
;       * parameters :                                                                ;
;           @src : pointer on the buffer                                              ;
;           @lgth : the number of iterations to be made                               ;
;                   = array size [# of double words] / 16                             ;
;                                                                                     ;
; =================================================================================== ;

; Copyright (C) 2012 - ENSA OF MARRAKECH - THALES AIR SYSTEMS             ; 
;                                                                         ;
;                                                                         ;
;  Redistribution and use in source and binary forms, with or without     ;
;  modification, are permitted provided that the following conditions     ;
;  are met:                                                               ;
;                                                                         ;
;    Redistributions of source code must retain the above copyright       ;
;    notice, this list of conditions and the following disclaimer.        ;
;                                                                         ;
;    Redistributions in binary form must reproduce the above copyright    ;
;    notice, this list of conditions and the following disclaimer in the  ;
;    documentation and/or other materials provided with the               ;
;    distribution.                                                        ;
;                                                                         ;
;    Neither the name of THALES AIR SYSTEMS nor the names of              ;
;    its contributors may be used to endorse or promote products derived  ;
;    from this software without specific prior written permission.        ;
;                                                                         ;
;  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS    ;
;  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT      ;
;  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR  ;
;  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT   ;
;  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,  ;
;  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT       ;
;  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,  ;
;  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY  ;
;  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT    ;
;  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE  ;
;  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.   ;
;                                                                         ;
; ======================================================================= ;

        .global _miss_pipe_notwid_br
_miss_pipe_notwid_br:

; A4 = pointer
; B4 = nb iterations
; B6 = next pointer

        MVC .S2 B4,ILC
        MV .S2X A4,B6
        ADDAD .D2 B6,9,B6
        NOP 1
        
        SPLOOP 1
        LDDW .D1 *A4++[16],A9:A8
||      LDDW .D2 *B6++[16],B9:B8
        SPKERNEL 0,0

        B .S2 B3
        NOP 5
