; =================================================================================== ;
; ENSA OF MARRAKECH - THALES AIR SYSTEMS                                              ;
;                                                                                     ;
; fft_rad_2p2_next_ro_f_notwid_br.asm -- standard c66x asm implementation             ;
;       * title : a routine that's used while large ffts                              ;
;       * description : performs radix-2^2 fft stages without bit-reversal            ;
;                       adds an additional radix-2 stage for power-2 sizes            ;
;                       the size should be limited to 4k samples                      ;
;       * author : Mounir BAHTAT                                                      ;
;       * version : 1.0.0                                                             ;
;       * prototype : void _fft_rad_2p2_next_ro_f_notwid_br(double *inp ,             ;
;                     double *out_l1, unsigned int n_per_pas, int *svg, int pas,      ;
;                     double *coeff, double *outp)                                    ;
;       * parameters :                                                                ;
;           @inp : pointer on the input buffer                                        ;
;           @out_l1 : pointer on the intermediate buffer, usually the input           ;
;           @n_per_pas : must be 1, "n/step"                                          ;
;           @svg : pointer to an external memory where to save some bank registers    ;
;           @pas : fft size n, "step"                                                 ;
;           @coeff : pointer on the twiddle factor                                    ;
;           @outp : the final output while the last stage                             ;
;                                                                                     ;
; =================================================================================== ;

; Copyright (C) 2012 - ENSA OF MARRAKECH - THALES AIR SYSTEMS             ; 
;                                                                         ;
;                                                                         ;
;  Redistribution and use in source and binary forms, with or without     ;
;  modification, are permitted provided that the following conditions     ;
;  are met:                                                               ;
;                                                                         ;
;    Redistributions of source code must retain the above copyright       ;
;    notice, this list of conditions and the following disclaimer.        ;
;                                                                         ;
;    Redistributions in binary form must reproduce the above copyright    ;
;    notice, this list of conditions and the following disclaimer in the  ;
;    documentation and/or other materials provided with the               ;
;    distribution.                                                        ;
;                                                                         ;
;    Neither the name of THALES AIR SYSTEMS nor the names of              ;
;    its contributors may be used to endorse or promote products derived  ;
;    from this software without specific prior written permission.        ;
;                                                                         ;
;  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS    ;
;  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT      ;
;  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR  ;
;  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT   ;
;  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,  ;
;  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT       ;
;  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,  ;
;  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY  ;
;  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT    ;
;  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE  ;
;  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.   ;
;                                                                         ;
; ======================================================================= ;

        .global _fft_rad_2p2_next_ro_f_notwid_br
_fft_rad_2p2_next_ro_f_notwid_br:

; A4 = inp
; B4 = outp
; A6 = N/pas
; B6 = Svg
; A8 = pas
; B8 = coeff
; A10 = final outp

        MV .L2 B6,B9
||      MV .L1 A6,A0 ; N/pas -> counter index A0
||      SHRU .S1 A8,2,A5 ; qpas
||      MV .D1X B8,A9 ; coeff
        STDW .D2T1 A11:A10,*B9++
||      MV .L2X A5,B5 ; qpas
||      ADD .L1 A5,A5,A6 ; 2 x qpas
||      MV .D1X B4,A25 ; save outp
        STDW .D2T1 A13:A12,*B9++
||      ADD .L2 B5,B5,B6 ; 2 x qpas
||      ADD .L1 A6,A5,A3 ; 3 x qpas
||      MV .S2X A0,B8 ; N/pas save
        STDW .D2T1 A15:A14,*B9++
        STDW .D2 B11:B10,*B9++
||      MV .L1 A4,A1 ; inp
||      MVKL .S1 3F800000h,A15 ; ONE
||      MV .D1 A4,A8 ; save inp
        STDW .D2 B13:B12,*B9++
||      ADDAD .D1 A1,A5,A1 ; inp + qpas x 8
||      MVKH .S1 3F800000h,A15 ; ONE
||      ZERO .L1 A14 ; ONE
        STDW .D2 B15:B14,*B9++
||      SUB .D1 A1,16,A1 ; inp + qpas x 8 -= 16
||      DMV .L1 A15,A14,A17:A16 ; W2=ONE
        STW .D2 B3,*B9--[12]
||      ADD .L2X 8,A4,B1 ; inp+8
||      MV .D1X B4,A2 ; outp v1
||      ADD .S2 B6,B5,B3 ; 3 x qpas
||      DMV .L1 A15,A14,A19:A18 ; W3=ONE
        ADDAD .D2 B1,B5,B1 ; inp+8 + qpas x 8
||      SHRU .S2 B5,1,B31 ; qpas/2
||      ADD .L2X 8,A2,B2 ; outp v1 +8
||      LDDW .D1T2 *A9,B11:B10 ; W1S
        SUB .L2 B1,16,B1 ; inp+8 + qpas x 8 -= 16
||      ADDAD .D1 A2,A5,A7 ; outp v2
||      ADDAD .D2 B2,B5,B7 ; outp v2+8
        ADDAD .D1 A7,A5,A4 ; outp v34
||      ADDAD .D2 B7,B5,B4 ; outp v34+8
        SUB .L2 B4,16,B4 ; outp v34+8 -= 16
||      SUB .L1 A4,16,A4 ; outp v34 -= 16

        NOP 1
        DMV .L2 B11,B10,B15:B14 ; W1=W1S
||      CMPYSP .M2 B11:B10,B11:B10,B23:B22:B21:B20 ; W1S x W1S = W2S
        NOP 3
        DADDSP .S2 B23:B22,B21:B20,B13:B12 ; W2S
        NOP 2
        DMV .L2 B13,B12,B17:B16 ; W2=W2S
||      CMPYSP .M2 B11:B10,B13:B12,B23:B22:B21:B20 ; W1S x W2S = W3S
        NOP 3
        DADDSP .S2 B23:B22,B21:B20,B19:B18 ; W3S
        NOP 2
        CMPYSP .M2 B11:B10,B19:B18,B23:B22:B21:B20 ; W1S x W3S = W4S
        NOP 1
        CMPYSP .M2 B19:B18,B19:B18,B23:B22:B21:B20 ; W3S x W3S = W6S
        NOP 1
        DADDSP .S2 B23:B22,B21:B20,B27:B26 ; W4S
||      CMPEQ .L2 B5,2,B0 ; if qpas==2 (pas==8)
        [B0] B .S2 power2_case
        DADDSP .S2 B23:B22,B21:B20,B27:B26 ; W6S
        
        MV .L1X B27,A11 ; W4S
||      MVKL .S2 80000000h,B0 ; XOR
        MV .S1X B26,A10 ; W4S

        MV .L1X B27,A13 ; W6S
||      MVKH .S2 80000000h,B0 ; XOR
        MV .S1X B26,A12 ; W6S
        
        MV .D1 A25,A8 ; save outp

        
main_loop:

        MVC .S2 B31,ILC
        NOP 3
        
        [A0] SPLOOP 8
||      MVC .S2 B31,RILC
||      SUB .L1 A0,1,A0
        
        LDDW .D1 *++A1[2],A27:A26 ; m1
||      LDDW .D2 *++B1[2],B27:B26 ; m1'

        LDDW .D1 *+A1[A6],A21:A20 ; m3
||      LDDW .D2 *+B1[B6],B21:B20 ; m3'

        LDDW .D1 *-A1[A5],A23:A22 ; m0
||      LDDW .D2 *-B1[B5],B23:B22 ; m0'

        LDDW .D1 *+A1[A5],A25:A24 ; m2
||      LDDW .D2 *+B1[B5],B25:B24 ; m2' 
        
        
        NOP 2
        
        
        DSUBSP .L1 A21:A20,A27:A26,A27:A26 ; a3
||      DSUBSP .L2 B21:B20,B27:B26,B27:B26 ; a3'

        DADDSP .L1 A21:A20,A27:A26,A23:A22 ; a1
||      DADDSP .L2 B21:B20,B27:B26,B23:B22 ; a1'

        DADDSP .L1 A23:A22,A25:A24,A29:A28 ; a0
||      DADDSP .L2 B23:B22,B25:B24,B29:B28 ; a0'

        ROTL .M1 A26,0,A31 ; a3ii2
||      ROTL .M2 B26,0,B31 ; a3ii2'
||      DSUBSP .L1 A23:A22,A25:A24,A29:A28 ; a2
||      DSUBSP .L2 B23:B22,B25:B24,B29:B28 ; a2'
        
        CMPYSP .M1X A15:A14,B13:B12,A31:A30:A29:A28 ; W1 x W2S
||      CMPYSP .M2 B15:B14,B13:B12,B31:B30:B29:B28 ; W1' x W2S
        
        DADDSP .L1 A29:A28,A23:A22,A23:A22 ; v1
||      DADDSP .L2 B29:B28,B23:B22,B23:B22 ; v1'
||      DSUBSP .S1 A29:A28,A23:A22,A25:A24 ; v2
||      DSUBSP .S2 B29:B28,B23:B22,B25:B24 ; v2'

        
        CMPYSP .M1 A17:A16,A11:A10,A31:A30:A29:A28 ; W2 x W4S
||      CMPYSP .M2X B17:B16,A11:A10,B31:B30:B29:B28 ; W2' x W4S
||      XOR .L1X A27,B0,A30 ; a3ii1
||      XOR .L2 B27,B0,B30 ; a3ii1
        
        
        CMPYSP .M1 A19:A18,A13:A12,A31:A30:A29:A28 ; W3 x W6S
||      CMPYSP .M2X B19:B18,A13:A12,B31:B30:B29:B28 ; W3' x W6S
||      DSUBSP .S1 A29:A28,A31:A30,A21:A20 ; v4
||      DSUBSP .S2 B29:B28,B31:B30,B21:B20 ; v4'
||      DADDSP .L1 A29:A28,A31:A30,A27:A26 ; v3
||      DADDSP .L2 B29:B28,B31:B30,B27:B26 ; v3'
        
        
        CMPYSP .M1 A17:A16,A25:A24,A31:A30:A29:A28 ; v2 x W2
||      CMPYSP .M2 B17:B16,B25:B24,B31:B30:B29:B28 ; v2' x W2'
||      STDW .D1 A23:A22,*A2++[2] ; store v1
||      STDW .D2 B23:B22,*B2++[2] ; store v1'
||      DADDSP .S1 A31:A30,A29:A28,A15:A14 ; W1
||      DADDSP .S2 B31:B30,B29:B28,B15:B14 ; W1'

        NOP 1
        
        DADDSP .S1 A31:A30,A29:A28,A17:A16 ; W2
||      DADDSP .S2 B31:B30,B29:B28,B17:B16 ; W2'
||      CMPYSP .M1 A15:A14,A27:A26,A23:A22:A21:A20 ; v3 x W1
||      CMPYSP .M2 B15:B14,B27:B26,B23:B22:B21:B20 ; v3' x W1'


        DADDSP .S1 A31:A30,A29:A28,A19:A18 ; W3
||      DADDSP .S2 B31:B30,B29:B28,B19:B18 ; W3'


        DSUBSP .S1 A31:A30,A29:A28,A25:A24 ; f2
||      DSUBSP .S2 B31:B30,B29:B28,B25:B24 ; f2'


        CMPYSP .M1 A19:A18,A21:A20,A31:A30:A29:A28 ; v4 x W3
||      CMPYSP .M2 B19:B18,B21:B20,B31:B30:B29:B28 ; v4' x W3'


        DSUBSP .S1 A23:A22,A21:A20,A25:A24 ; f3
||      DSUBSP .S2 B23:B22,B21:B20,B25:B24 ; f3'


        STDW .D1 A25:A24,*A7++[2] ; store f2
||      STDW .D2 B25:B24,*B7++[2] ; store f2'

        NOP 1
        
        
        DSUBSP .S1 A31:A30,A29:A28,A25:A24 ; f4
||      DSUBSP .S2 B31:B30,B29:B28,B25:B24 ; f4'
||      STDW .D1 A25:A24,*++A4[2] ; store f3
||      STDW .D2 B25:B24,*++B4[2] ; store f3'

        
        NOP 4
        
        
        STDW .D1 A25:A24,*+A4[A5] ; store f4
||      STDW .D2 B25:B24,*+B4[B5] ; store f4'
        

        SPKERNEL 0,0
        
setup_iter:

        ADDAD .D1 A1,A3,A1 ; inp
||      ADDAD .D2 B1,B3,B1 ; inp

        NOP 6
        
        SPMASKR ; reload
        
        NOP 1
        
        ZERO .L1 A15:A14 ; W1=ZERO
||      DMV .L2 B11,B10,B15:B14 ; W1=W1S

        NOP 1
        
        DMV .L2 B13,B12,B17:B16 ; W2=W2S
        
        NOP 1
        
        MVKH .S1 3F800000h,A15 ; W1=ONE
||      CMPYSP .M2 B11:B10,B13:B12,B31:B30:B29:B28 ; W3 = W1S x W2S
        
        ADDAD .D1 A2,A3,A2 ; outp v1
||      ADDAD .D2 B2,B3,B2 ; outp v1    
        
        NOP 1
        
        DMV .S1 A15,A14,A17:A16 ; W2=ONE
        
        DADDSP .S2 B31:B30,B29:B28,B19:B18 ; W3=W3S
        
        [A0] B .S2 setup_iter
||      [A0] SUB .S1 A0,1,A0
        
        NOP 1
        
        DMV .S1 A15,A14,A19:A18 ; W3=ONE
        
        ADDAD .D1 A7,A3,A7 ; outp v2
||      ADDAD .D2 B7,B3,B7 ; outp v2

        NOP 1
        
        ADDAD .D1 A4,A3,A4 ; outp v34
||      ADDAD .D2 B4,B3,B4 ; outp v34               

        ; end.
        
        
        
        SHRU .S1 A5,2,A5 ; qpas
||      SHRU .S2 B5,2,B5 ; qpas
||      MV .L1 A8,A1 ; inp
||      ADD .L2X 8,A8,B1 ; inp+8
        
        SHRU .S1 A6,2,A6 ; 2 x qpas
||      SHL .S2 B8,2,B8 ; N/PAS *=4
||      ADDAD .D1 A1,A5,A1 ; inp + qpas x 8
||      ADDAD .D2 B1,B5,B1 ; inp+8 + qpas x 8   
||      MV .L2X A10,B10 ; W4S -> W1S    


        SHRU .S1 A3,2,A3 ; 3 x qpas
||      SUB .D1 A1,16,A1 ; inp + qpas x 8 -= 16
||      SUB .D2 B1,16,B1 ; inp+8 + qpas x 8 -= 16
||      MV .L1 A8,A2 ; outp v1
||      ADD .L2X 8,A8,B2 ; outp v1

        SHRU .S2 B6,2,B6 ; 2 x qpas
||      MV .D1X B8,A0 ; *= 4
||      MVKL .S1 3F800000h,A15 ; ONE
        
        SHRU .S2 B3,2,B3 ; 3 x qpas
||      ADDAD .D1 A2,A5,A7 ; outp v2
||      ADDAD .D2 B2,B5,B7 ; outp v2+8
||      MVKH .S1 3F800000h,A15 ; ONE
||      ZERO .L1 A14 ; ONE
||      MV .L2X A11,B11 ; W4S -> W1S

        ADDAD .D1 A7,A5,A4 ; outp v34
||      ADDAD .D2 B7,B5,B4 ; outp v34+8
||      DMV .L1 A15,A14,A17:A16 ; W2=ONE
||      DMV .L2 B11,B10,B15:B14 ; W1=W1S
||      CMPYSP .M2 B11:B10,B11:B10,B23:B22:B21:B20 ; W1S x W1S = W2S

        SUB .L2 B4,16,B4 ; outp v34+8 -= 16
||      SUB .L1 A4,16,A4 ; outp v34 -= 16
||      SHRU .S2 B5,1,B31 ; qpas/2
||      DMV .S1 A15,A14,A19:A18 ; W3=ONE
        
        MVK .S2 8,B19
        CMPLT .L2 B5,B19,B0 ; stop if qpas<8
        DADDSP .S2 B23:B22,B21:B20,B13:B12 ; W2S
        NOP 2
        DMV .L2 B13,B12,B17:B16 ; W2=W2S
||      CMPYSP .M2 B11:B10,B13:B12,B23:B22:B21:B20 ; W1S x W2S = W3S
        NOP 3
        DADDSP .S2 B23:B22,B21:B20,B19:B18 ; W3S
        NOP 2
        CMPYSP .M2 B11:B10,B19:B18,B23:B22:B21:B20 ; W1S x W3S = W4S
        NOP 1
        CMPYSP .M2 B19:B18,B19:B18,B23:B22:B21:B20 ; W3S x W3S = W6S
        NOP 1
        DADDSP .S2 B23:B22,B21:B20,B27:B26 ; W4S
        [!B0] B .S2 main_loop
        DADDSP .S2 B23:B22,B21:B20,B27:B26 ; W6S
        
        MV .L1X B27,A11 ; W4S
        MV .S1X B26,A10 ; W4S

        MV .L1X B27,A13 ; W6S
||      MVKL .S2 80000000h,B0 ; XOR

        MV .S1X B26,A12 ; W6S
||      MVKH .S2 80000000h,B0 ; XOR



        ; end stages

        
        CMPEQ .L2 B5,4,B1
        [B1] B .S2 power4_case
        NOP 5

power2_case:
        
        ; power 2 case
        
        MV .D1 A8,A3 ; outp orig
||      ADD .L2X 8,A8,B1 ; inp+8
||      MV .S1X B8,A5 ; save N/pas ratio
||      DMV .S2 B15,B14,B13:B12 ; W1S
||      DMV .L1 A15,A14,A9:A8 ; ONE

        MV .L1 A3,A1 ; inp
||      ADD .L2X 8,A3,B2 ; outp+8
||      MV .S1X B8,A0 ; loop count
||      DMV .S2 B19,B18,B7:B6 ; W3S
||      LDDW .D2T1 *B9,A7:A6 ; final output load

        MV .L1 A3,A2 ; outp
||      ADD .L2X 8,A3,B4 ; outp v4 +8
||      ADDAD .D1 A1,2,A1 ; inp + qpas x 8
||      ADDAD .D2 B1,2,B1 ; inp+8 + qpas x 8
||      MV .S1 A3,A4 ; outp v4

        ADDAD .D1 A4,6,A4 ; outp v4 + 3 x qpas x 8
||      ADDAD .D2 B4,6,B4 ; outp v4 +8 + 3 x qpas x 8
||      DMV .L2 B17,B16,B11:B10 ; W2S
||      MVC .S2 B8,ILC
        
        NOP 3

        SPLOOP 8
        
        LDDW .D1 *A1++[4],A29:A28 ; m1
||      LDDW .D2 *B1++[4],B29:B28 ; m1'

        LDDW .D1 *A1--[6],A15:A14 ; m3
||      LDDW .D2 *B1--[6],B15:B14 ; m3'

        LDDW .D1 *A1++[4],A15:A14 ; m0
||      LDDW .D2 *B1++[4],B15:B14 ; m0'

        LDDW .D1 *A1++[6],A17:A16 ; m2
||      LDDW .D2 *B1++[6],B17:B16 ; m2'

        NOP 2
        
        DSUBSP .S1 A15:A14,A29:A28,A17:A16 ; a3
||      DSUBSP .S2 B15:B14,B29:B28,B17:B16 ; a3'
||      DADDSP .L1 A15:A14,A29:A28,A15:A14 ; a1
||      DADDSP .L2 B15:B14,B29:B28,B15:B14 ; a1'

        NOP 1

        DADDSP .S1 A15:A14,A17:A16,A17:A16 ; a0
||      DADDSP .S2 B15:B14,B17:B16,B17:B16 ; a0'
||      DSUBSP .L1 A15:A14,A17:A16,A19:A18 ; a2
||      DSUBSP .L2 B15:B14,B17:B16,B19:B18 ; a2'

        XOR .S1X A17,B0,A20 ; a3ii1
||      XOR .S2 B17,B0,B20 ; a3ii1
||      ROTL .M1 A16,0,A21 ; a3ii2
||      ROTL .M2 B16,0,B21 ; a3ii2'

        NOP 1

        DSUBSP .L1 A19:A18,A21:A20,A23:A22 ; v4
||      DSUBSP .L2 B19:B18,B21:B20,B23:B22 ; v4'

        DADDSP .S1 A17:A16,A15:A14,A23:A22 ; v1
||      DADDSP .S2 B17:B16,B15:B14,B23:B22 ; v1'

        DSUBSP .S1 A17:A16,A15:A14,A19:A18 ; v2
||      DSUBSP .S2 B17:B16,B15:B14,B19:B18 ; v2'
||      DADDSP .L1 A19:A18,A21:A20,A23:A22 ; v3
||      DADDSP .L2 B19:B18,B21:B20,B23:B22 ; v3'

        CMPYSP .M1 A9:A8,A23:A22,A31:A30:A29:A28 ; v4 x ONE
||      CMPYSP .M2 B7:B6,B23:B22,B31:B30:B29:B28 ; v4' x W3S

        STDW .D1 A23:A22,*A2++[8] ; store v1
||      STDW .D2 B23:B22,*B2++[8] ; store v1'

        CMPYSP .M1 A9:A8,A19:A18,A31:A30:A29:A28 ; v2 x ONE
||      CMPYSP .M2 B11:B10,B19:B18,B31:B30:B29:B28 ; v2' x W2S

        NOP 1

        DSUBSP .L1 A31:A30,A29:A28,A25:A24 ; f4
||      DSUBSP .L2 B31:B30,B29:B28,B25:B24 ; f4'

        NOP 1

        DSUBSP .L1 A31:A30,A29:A28,A25:A24 ; f2
||      DSUBSP .L2 B31:B30,B29:B28,B25:B24 ; f2'

        CMPYSP .M1 A9:A8,A23:A22,A31:A30:A29:A28 ; v3 x ONE
||      CMPYSP .M2 B13:B12,B23:B22,B31:B30:B29:B28 ; v3' x W1S

        STDW .D1 A25:A24,*A4--[4] ; store f4
||      STDW .D2 B25:B24,*B4--[4] ; store f4'

        NOP 1

        NOP 1

        DSUBSP .L1 A31:A30,A29:A28,A27:A26 ; f3
||      DSUBSP .L2 B31:B30,B29:B28,B27:B26 ; f3'

        NOP 1

        NOP 1
            
        STDW .D1 A25:A24,*A4++[2] ; store f2
||      STDW .D2 B25:B24,*B4++[2] ; store f2'

        STDW .D1 A27:A26,*A4++[10] ; store f3
||      STDW .D2 B27:B26,*B4++[10] ; store f3'

        SPKERNEL 0,0

        NOP 8
        NOP 8
        NOP 6
        
        ; end stage 2
        
        SHL .S1 A5,2,A5 ; *=4
||      MV .D1 A3,A1 ; inp
||      ADD .S2X 8,A3,B1 ; inp+8
||      MV .L1 A6,A2 ; outp

        ADD .S2X 8,A6,B2 ; outp+8
        MV .D2X A5,B5
        
        MVC .S2 B5,ILC

        NOP 3
        
        SPLOOP 2
        
        LDDW .D1T2 *A1++[2],B29:B28 ; m0
||      LDDW .D2T1 *B1++[2],A29:A28 ; m1

        NOP 4
        
        DADDSP .L1X A29:A28,B29:B28,A31:A30 ; f0
||      DSUBSP .L2X B29:B28,A29:A28,B31:B30 ; f1

        NOP 3
        
        STDW .D1 A31:A30,*A2++[2] ; store f0
||      STDW .D2 B31:B30,*B2++[2] ; store f1
        
        SPKERNEL 0,0
        
        NOP 3
        B .S2 quit
        NOP 5
        
power4_case:

        ADDAD .D2 B9,8,B9 ; store N/Pas ratio in "svg"
        STDW .D2 B9:B8,*B9 ; store N/Pas ratio in "svg"
||      MV .L1X B0,A5 ; XOR value

        CMPYSP .M2X B19:B18,A13:A12,B23:B22:B21:B20 ; W3S x W6S
||      DMV .L1 A13,A12,A7:A6 ; W6S

        MV .D1 A8,A3 ; outp orig
||      ADD .L2X 8,A8,B1 ; inp+8
||      SUB .D2 B8,3,B8 ; loop count ; - 2 - 1
||      DMV .S2 B15,B14,B13:B12 ; W1S
||      DMV .L1 A15,A14,A9:A8 ; ONE 

        MV .D1 A3,A1 ; inp
||      ADD .L2X 8,A3,B2 ; outp+8
||      MV .D2 B9,B3 ; move svg ptr
||      MV .S1X B8,A0 ; loop count
||      DMV .L1 A11,A10,A13:A12 ; W4S
||      DMV .S2 B19,B18,B7:B6 ; W3S

        MV .L1 A3,A2 ; outp
||      ADD .L2X 8,A3,B0 ; outp v4 +8
||      ADDAD .D1 A1,4,A1 ; inp + qpas x 8
||      ADDAD .D2 B1,4,B1 ; inp+8 + qpas x 8
||      MV .S1 A3,A4 ; outp v4

        ADDAD .D1 A4,12,A4 ; outp v4 + 3 x qpas x 8
||      ADDAD .D2 B0,12,B0 ; outp v4 +8 + 3 x qpas x 8
||      MV .L1X B16,A10 ; W2S
||      MV .L2X A6,B8 ; W6S
||      DADDSP .S2 B23:B22,B21:B20,B11:B10 ; W9S

        MV .L1X B17,A11 ; W2S
||      MV .L2X A7,B9 ; W6S
||      DMV .S2 B17,B16,B5:B4 ; W2S


        ; radix-4's stage 2, start point

        ; sequence 0
        LDDW .D1 *A1++[8],A29:A28 ; m1
||      LDDW .D2 *B1++[8],B29:B28 ; m1'
        LDDW .D1 *A1--[12],A15:A14 ; m3
||      LDDW .D2 *B1--[12],B15:B14 ; m3'
        LDDW .D1 *A1++[8],A15:A14 ; m0
||      LDDW .D2 *B1++[8],B15:B14 ; m0'
        LDDW .D1 *A1--[2],A17:A16 ; m2
||      LDDW .D2 *B1--[2],B17:B16 ; m2'
        NOP 1
        NOP 1
        DSUBSP .S1 A15:A14,A29:A28,A17:A16 ; a3
||      DSUBSP .S2 B15:B14,B29:B28,B17:B16 ; a3'
||      DADDSP .L1 A15:A14,A29:A28,A15:A14 ; a1
||      DADDSP .L2 B15:B14,B29:B28,B15:B14 ; a1'
        NOP 1
        DADDSP .S1 A15:A14,A17:A16,A17:A16 ; a0
||      DADDSP .S2 B15:B14,B17:B16,B17:B16 ; a0'
||      DSUBSP .L1 A15:A14,A17:A16,A19:A18 ; a2
||      DSUBSP .L2 B15:B14,B17:B16,B19:B18 ; a2'
||      LDDW .D1 *A1++[8],A29:A28 ; m1&
||      LDDW .D2 *B1++[8],B29:B28 ; m1'&
        XOR .S1 A17,A5,A20 ; a3ii1
||      XOR .S2X B17,A5,B20 ; a3ii1
||      ROTL .M1 A16,0,A21 ; a3ii2
||      ROTL .M2 B16,0,B21 ; a3ii2'
||      LDDW .D1 *A1--[12],A15:A14 ; m3&
||      LDDW .D2 *B1--[12],B15:B14 ; m3'&
        LDDW .D1 *A1++[8],A15:A14 ; m0&
||      LDDW .D2 *B1++[8],B15:B14 ; m0'&
        DSUBSP .L1 A19:A18,A21:A20,A23:A22 ; v4
||      DSUBSP .L2 B19:B18,B21:B20,B23:B22 ; v4'
||      LDDW .D1 *A1++[10],A17:A16 ; m2&
||      LDDW .D2 *B1++[10],B17:B16 ; m2'&
        DADDSP .S1 A17:A16,A15:A14,A23:A22 ; v1
||      DADDSP .S2 B17:B16,B15:B14,B23:B22 ; v1'
        DSUBSP .S1 A17:A16,A15:A14,A19:A18 ; v2
||      DSUBSP .S2 B17:B16,B15:B14,B19:B18 ; v2'
||      DADDSP .L1 A19:A18,A21:A20,A23:A22 ; v3
||      DADDSP .L2 B19:B18,B21:B20,B23:B22 ; v3'
        CMPYSP .M1 A9:A8,A23:A22,A31:A30:A29:A28 ; v4 x ONE
||      CMPYSP .M2 B7:B6,B23:B22,B31:B30:B29:B28 ; v4' x W3S
||      DSUBSP .S1 A15:A14,A29:A28,A17:A16 ; a3&
||      DSUBSP .S2 B15:B14,B29:B28,B17:B16 ; a3'&
||      DADDSP .L1 A15:A14,A29:A28,A15:A14 ; a1&
||      DADDSP .L2 B15:B14,B29:B28,B15:B14 ; a1'&
        STDW .D1 A23:A22,*A2++[2] ; store v1
||      STDW .D2 B23:B22,*B2++[2] ; store v1'
        ; sequence 1
        CMPYSP .M1 A9:A8,A19:A18,A31:A30:A29:A28 ; v2 x ONE
||      CMPYSP .M2 B5:B4,B19:B18,B31:B30:B29:B28 ; v2' x W2S
||      DADDSP .S1 A15:A14,A17:A16,A17:A16 ; a0&
||      DADDSP .S2 B15:B14,B17:B16,B17:B16 ; a0'&
||      DSUBSP .L1 A15:A14,A17:A16,A19:A18 ; a2&
||      DSUBSP .L2 B15:B14,B17:B16,B19:B18 ; a2'&
||      LDDW .D1 *A1++[8],A29:A28 ; m1
||      LDDW .D2 *B1++[8],B29:B28 ; m1'
        XOR .S1 A17,A5,A20 ; a3ii1&
||      XOR .S2X B17,A5,B20 ; a3ii1&
||      ROTL .M1 A16,0,A21 ; a3ii2&
||      ROTL .M2 B16,0,B21 ; a3ii2'&
||      LDDW .D1 *A1--[12],A15:A14 ; m3
||      LDDW .D2 *B1--[12],B15:B14 ; m3'
        DSUBSP .L1 A31:A30,A29:A28,A25:A24 ; f4
||      DSUBSP .L2 B31:B30,B29:B28,B25:B24 ; f4'
||      LDDW .D1 *A1++[8],A15:A14 ; m0
||      LDDW .D2 *B1++[8],B15:B14 ; m0'
        DSUBSP .L1 A19:A18,A21:A20,A23:A22 ; v4&
||      DSUBSP .L2 B19:B18,B21:B20,B23:B22 ; v4'&
||      LDDW .D1 *A1--[2],A17:A16 ; m2
||      LDDW .D2 *B1--[2],B17:B16 ; m2'
        DSUBSP .L1 A31:A30,A29:A28,A25:A24 ; f2
||      DSUBSP .L2 B31:B30,B29:B28,B25:B24 ; f2'
||      DADDSP .S1 A17:A16,A15:A14,A23:A22 ; v1&
||      DADDSP .S2 B17:B16,B15:B14,B23:B22 ; v1'&
        CMPYSP .M1 A9:A8,A23:A22,A31:A30:A29:A28 ; v3 x ONE
||      CMPYSP .M2 B13:B12,B23:B22,B31:B30:B29:B28 ; v3' x W1S
||      DSUBSP .S1 A17:A16,A15:A14,A19:A18 ; v2&
||      DSUBSP .S2 B17:B16,B15:B14,B19:B18 ; v2'&
||      DADDSP .L1 A19:A18,A21:A20,A23:A22 ; v3&
||      DADDSP .L2 B19:B18,B21:B20,B23:B22 ; v3'&
        STDW .D1 A25:A24,*A4--[8] ; store f4
||      STDW .D2 B25:B24,*B0--[8] ; store f4'
||      CMPYSP .M1 A7:A6,A23:A22,A31:A30:A29:A28 ; v4 x W6S&
||      CMPYSP .M2 B11:B10,B23:B22,B31:B30:B29:B28 ; v4' x W9S&
||      DSUBSP .S1 A15:A14,A29:A28,A17:A16 ; a3
||      DSUBSP .S2 B15:B14,B29:B28,B17:B16 ; a3'
||      DADDSP .L1 A15:A14,A29:A28,A15:A14 ; a1
||      DADDSP .L2 B15:B14,B29:B28,B15:B14 ; a1'
        STDW .D1 A23:A22,*A2++[14] ; store v1&
||      STDW .D2 B23:B22,*B2++[14] ; store v1'&
        CMPYSP .M1 A13:A12,A19:A18,A31:A30:A29:A28 ; v2 x W4S&
||      CMPYSP .M2 B9:B8,B19:B18,B31:B30:B29:B28 ; v2' x W6S&
||      DADDSP .S1 A15:A14,A17:A16,A17:A16 ; a0
||      DADDSP .S2 B15:B14,B17:B16,B17:B16 ; a0'
||      DSUBSP .L1 A15:A14,A17:A16,A19:A18 ; a2
||      DSUBSP .L2 B15:B14,B17:B16,B19:B18 ; a2'
||      LDDW .D1 *A1++[8],A29:A28 ; m1&
||      LDDW .D2 *B1++[8],B29:B28 ; m1'&
        DSUBSP .L1 A31:A30,A29:A28,A27:A26 ; f3
||      DSUBSP .L2 B31:B30,B29:B28,B27:B26 ; f3'
||      XOR .S1 A17,A5,A20 ; a3ii1
||      XOR .S2X B17,A5,B20 ; a3ii1
||      ROTL .M1 A16,0,A21 ; a3ii2
||      ROTL .M2 B16,0,B21 ; a3ii2'
||      LDDW .D1 *A1--[12],A15:A14 ; m3&
||      LDDW .D2 *B1--[12],B15:B14 ; m3'&
        DSUBSP .L1 A31:A30,A29:A28,A25:A24 ; f4&
||      DSUBSP .L2 B31:B30,B29:B28,B25:B24 ; f4'&
||      LDDW .D1 *A1++[8],A15:A14 ; m0&
||      LDDW .D2 *B1++[8],B15:B14 ; m0'&
        DSUBSP .L1 A19:A18,A21:A20,A23:A22 ; v4
||      DSUBSP .L2 B19:B18,B21:B20,B23:B22 ; v4'
||      LDDW .D1 *A1++[10],A17:A16 ; m2&
||      LDDW .D2 *B1++[10],B17:B16 ; m2'&
        DSUBSP .L1 A31:A30,A29:A28,A25:A24 ; f2&
||      DSUBSP .L2 B31:B30,B29:B28,B25:B24 ; f2'&
||      STDW .D1 A25:A24,*A4++[4] ; store f2
||      STDW .D2 B25:B24,*B0++[4] ; store f2'
||      DADDSP .S1 A17:A16,A15:A14,A23:A22 ; v1
||      DADDSP .S2 B17:B16,B15:B14,B23:B22 ; v1'
        CMPYSP .M1 A11:A10,A23:A22,A31:A30:A29:A28 ; v3 x W2S&
||      CMPYSP .M2 B7:B6,B23:B22,B31:B30:B29:B28 ; v3' x W3S&
||      STDW .D1 A27:A26,*A4++[6] ; store f3
||      STDW .D2 B27:B26,*B0++[6] ; store f3'
||      DSUBSP .S1 A17:A16,A15:A14,A19:A18 ; v2
||      DSUBSP .S2 B17:B16,B15:B14,B19:B18 ; v2'
||      DADDSP .L1 A19:A18,A21:A20,A23:A22 ; v3
||      DADDSP .L2 B19:B18,B21:B20,B23:B22 ; v3'
        STDW .D1 A25:A24,*A4--[8] ; store f4&
||      STDW .D2 B25:B24,*B0--[8] ; store f4'&
||      CMPYSP .M1 A9:A8,A23:A22,A31:A30:A29:A28 ; v4 x ONE
||      CMPYSP .M2 B7:B6,B23:B22,B31:B30:B29:B28 ; v4' x W3S
||      DSUBSP .S1 A15:A14,A29:A28,A17:A16 ; a3&
||      DSUBSP .S2 B15:B14,B29:B28,B17:B16 ; a3'&
||      DADDSP .L1 A15:A14,A29:A28,A15:A14 ; a1&
||      DADDSP .L2 B15:B14,B29:B28,B15:B14 ; a1'&
        STDW .D1 A23:A22,*A2++[2] ; store v1
||      STDW .D2 B23:B22,*B2++[2] ; store v1'
        ; sequence 2
loop_stg2:
        CMPYSP .M1 A9:A8,A19:A18,A31:A30:A29:A28 ; v2 x ONE
||      CMPYSP .M2 B5:B4,B19:B18,B31:B30:B29:B28 ; v2' x W2S
||      DADDSP .S1 A15:A14,A17:A16,A17:A16 ; a0&
||      DADDSP .S2 B15:B14,B17:B16,B17:B16 ; a0'&
||      DSUBSP .L1 A15:A14,A17:A16,A19:A18 ; a2&
||      DSUBSP .L2 B15:B14,B17:B16,B19:B18 ; a2'&
||      LDDW .D1 *A1++[8],A29:A28 ; m1
||      LDDW .D2 *B1++[8],B29:B28 ; m1'
        DSUBSP .L1 A31:A30,A29:A28,A27:A26 ; f3&
||      DSUBSP .L2 B31:B30,B29:B28,B27:B26 ; f3'&
||      XOR .S1 A17,A5,A20 ; a3ii1&
||      XOR .S2X B17,A5,B20 ; a3ii1&
||      ROTL .M1 A16,0,A21 ; a3ii2&
||      ROTL .M2 B16,0,B21 ; a3ii2'&
||      LDDW .D1 *A1--[12],A15:A14 ; m3
||      LDDW .D2 *B1--[12],B15:B14 ; m3'
        DSUBSP .L1 A31:A30,A29:A28,A25:A24 ; f4
||      DSUBSP .L2 B31:B30,B29:B28,B25:B24 ; f4'
||      LDDW .D1 *A1++[8],A15:A14 ; m0
||      LDDW .D2 *B1++[8],B15:B14 ; m0'
        DSUBSP .L1 A19:A18,A21:A20,A23:A22 ; v4&
||      DSUBSP .L2 B19:B18,B21:B20,B23:B22 ; v4'&
||      LDDW .D1 *A1--[2],A17:A16 ; m2
||      LDDW .D2 *B1--[2],B17:B16 ; m2'
        STDW .D1 A25:A24,*A4++[4] ; store f2&
||      STDW .D2 B25:B24,*B0++[4] ; store f2'&
||      DSUBSP .L1 A31:A30,A29:A28,A25:A24 ; f2
||      DSUBSP .L2 B31:B30,B29:B28,B25:B24 ; f2'
||      DADDSP .S1 A17:A16,A15:A14,A23:A22 ; v1&
||      DADDSP .S2 B17:B16,B15:B14,B23:B22 ; v1'&
        STDW .D1 A27:A26,*A4++[18] ; store f3&
||      STDW .D2 B27:B26,*B0++[18] ; store f3'&
||      CMPYSP .M1 A9:A8,A23:A22,A31:A30:A29:A28 ; v3 x ONE
||      CMPYSP .M2 B13:B12,B23:B22,B31:B30:B29:B28 ; v3' x W1S
||      DSUBSP .S1 A17:A16,A15:A14,A19:A18 ; v2&
||      DSUBSP .S2 B17:B16,B15:B14,B19:B18 ; v2'&
||      DADDSP .L1 A19:A18,A21:A20,A23:A22 ; v3&
||      DADDSP .L2 B19:B18,B21:B20,B23:B22 ; v3'&
        STDW .D1 A25:A24,*A4--[8] ; store f4
||      STDW .D2 B25:B24,*B0--[8] ; store f4'
||      CMPYSP .M1 A7:A6,A23:A22,A31:A30:A29:A28 ; v4 x W6S&
||      CMPYSP .M2 B11:B10,B23:B22,B31:B30:B29:B28 ; v4' x W9S&
||      DSUBSP .S1 A15:A14,A29:A28,A17:A16 ; a3
||      DSUBSP .S2 B15:B14,B29:B28,B17:B16 ; a3'
||      DADDSP .L1 A15:A14,A29:A28,A15:A14 ; a1
||      DADDSP .L2 B15:B14,B29:B28,B15:B14 ; a1'
        STDW .D1 A23:A22,*A2++[14] ; store v1&
||      STDW .D2 B23:B22,*B2++[14] ; store v1'&
        CMPYSP .M1 A13:A12,A19:A18,A31:A30:A29:A28 ; v2 x W4S&
||      CMPYSP .M2 B9:B8,B19:B18,B31:B30:B29:B28 ; v2' x W6S&
||      DADDSP .S1 A15:A14,A17:A16,A17:A16 ; a0
||      DADDSP .S2 B15:B14,B17:B16,B17:B16 ; a0'
||      DSUBSP .L1 A15:A14,A17:A16,A19:A18 ; a2
||      DSUBSP .L2 B15:B14,B17:B16,B19:B18 ; a2'
||      LDDW .D1 *A1++[8],A29:A28 ; m1&
||      LDDW .D2 *B1++[8],B29:B28 ; m1'&
        DSUBSP .L1 A31:A30,A29:A28,A27:A26 ; f3
||      DSUBSP .L2 B31:B30,B29:B28,B27:B26 ; f3'
||      XOR .S1 A17,A5,A20 ; a3ii1
||      XOR .S2X B17,A5,B20 ; a3ii1
||      ROTL .M1 A16,0,A21 ; a3ii2
||      ROTL .M2 B16,0,B21 ; a3ii2'
||      LDDW .D1 *A1--[12],A15:A14 ; m3&
||      LDDW .D2 *B1--[12],B15:B14 ; m3'&
        DSUBSP .L1 A31:A30,A29:A28,A25:A24 ; f4&
||      DSUBSP .L2 B31:B30,B29:B28,B25:B24 ; f4'&
||      LDDW .D1 *A1++[8],A15:A14 ; m0&
||      LDDW .D2 *B1++[8],B15:B14 ; m0'&
||      [A0] B .S1 loop_stg2 ; LOOP STAGE 2
        DSUBSP .L1 A19:A18,A21:A20,A23:A22 ; v4
||      DSUBSP .L2 B19:B18,B21:B20,B23:B22 ; v4'
||      LDDW .D1 *A1++[10],A17:A16 ; m2&
||      LDDW .D2 *B1++[10],B17:B16 ; m2'&
||      [A0] SUB .S1 A0,1,A0 ; SUB A0 to A0
        DSUBSP .L1 A31:A30,A29:A28,A25:A24 ; f2&
||      DSUBSP .L2 B31:B30,B29:B28,B25:B24 ; f2'&
||      STDW .D1 A25:A24,*A4++[4] ; store f2
||      STDW .D2 B25:B24,*B0++[4] ; store f2'
||      DADDSP .S1 A17:A16,A15:A14,A23:A22 ; v1
||      DADDSP .S2 B17:B16,B15:B14,B23:B22 ; v1'
        CMPYSP .M1 A11:A10,A23:A22,A31:A30:A29:A28 ; v3 x W2S&
||      CMPYSP .M2 B7:B6,B23:B22,B31:B30:B29:B28 ; v3' x W3S&
||      STDW .D1 A27:A26,*A4++[6] ; store f3
||      STDW .D2 B27:B26,*B0++[6] ; store f3'
||      DSUBSP .S1 A17:A16,A15:A14,A19:A18 ; v2
||      DSUBSP .S2 B17:B16,B15:B14,B19:B18 ; v2'
||      DADDSP .L1 A19:A18,A21:A20,A23:A22 ; v3
||      DADDSP .L2 B19:B18,B21:B20,B23:B22 ; v3'
        STDW .D1 A25:A24,*A4--[8] ; store f4&
||      STDW .D2 B25:B24,*B0--[8] ; store f4'&
||      CMPYSP .M1 A9:A8,A23:A22,A31:A30:A29:A28 ; v4 x ONE
||      CMPYSP .M2 B7:B6,B23:B22,B31:B30:B29:B28 ; v4' x W3S
||      DSUBSP .S1 A15:A14,A29:A28,A17:A16 ; a3&
||      DSUBSP .S2 B15:B14,B29:B28,B17:B16 ; a3'&
||      DADDSP .L1 A15:A14,A29:A28,A15:A14 ; a1&
||      DADDSP .L2 B15:B14,B29:B28,B15:B14 ; a1'&
        STDW .D1 A23:A22,*A2++[2] ; store v1
||      STDW .D2 B23:B22,*B2++[2] ; store v1'
        ; sequence 3
        CMPYSP .M1 A9:A8,A19:A18,A31:A30:A29:A28 ; v2 x ONE
||      CMPYSP .M2 B5:B4,B19:B18,B31:B30:B29:B28 ; v2' x W2S
||      DADDSP .S1 A15:A14,A17:A16,A17:A16 ; a0&
||      DADDSP .S2 B15:B14,B17:B16,B17:B16 ; a0'&
||      DSUBSP .L1 A15:A14,A17:A16,A19:A18 ; a2&
||      DSUBSP .L2 B15:B14,B17:B16,B19:B18 ; a2'&
        DSUBSP .L1 A31:A30,A29:A28,A27:A26 ; f3&
||      DSUBSP .L2 B31:B30,B29:B28,B27:B26 ; f3'&
||      XOR .S1 A17,A5,A20 ; a3ii1&
||      XOR .S2X B17,A5,B20 ; a3ii1&
||      ROTL .M1 A16,0,A21 ; a3ii2&
||      ROTL .M2 B16,0,B21 ; a3ii2'&
        DSUBSP .L1 A31:A30,A29:A28,A25:A24 ; f4
||      DSUBSP .L2 B31:B30,B29:B28,B25:B24 ; f4'
        DSUBSP .L1 A19:A18,A21:A20,A23:A22 ; v4&
||      DSUBSP .L2 B19:B18,B21:B20,B23:B22 ; v4'&
        STDW .D1 A25:A24,*A4++[4] ; store f2&
||      STDW .D2 B25:B24,*B0++[4] ; store f2'&
||      DSUBSP .L1 A31:A30,A29:A28,A25:A24 ; f2
||      DSUBSP .L2 B31:B30,B29:B28,B25:B24 ; f2'
||      DADDSP .S1 A17:A16,A15:A14,A23:A22 ; v1&
||      DADDSP .S2 B17:B16,B15:B14,B23:B22 ; v1'&
        STDW .D1 A27:A26,*A4++[18] ; store f3&
||      STDW .D2 B27:B26,*B0++[18] ; store f3'&
||      CMPYSP .M1 A9:A8,A23:A22,A31:A30:A29:A28 ; v3 x ONE
||      CMPYSP .M2 B13:B12,B23:B22,B31:B30:B29:B28 ; v3' x W1S
||      DSUBSP .S1 A17:A16,A15:A14,A19:A18 ; v2&
||      DSUBSP .S2 B17:B16,B15:B14,B19:B18 ; v2'&
||      DADDSP .L1 A19:A18,A21:A20,A23:A22 ; v3&
||      DADDSP .L2 B19:B18,B21:B20,B23:B22 ; v3'&
        STDW .D1 A25:A24,*A4--[8] ; store f4
||      STDW .D2 B25:B24,*B0--[8] ; store f4'
||      CMPYSP .M1 A7:A6,A23:A22,A31:A30:A29:A28 ; v4 x W6S&
||      CMPYSP .M2 B11:B10,B23:B22,B31:B30:B29:B28 ; v4' x W9S&
        STDW .D1 A23:A22,*A2++[14] ; store v1&
||      STDW .D2 B23:B22,*B2++[14] ; store v1'&
        CMPYSP .M1 A13:A12,A19:A18,A31:A30:A29:A28 ; v2 x W4S&
||      CMPYSP .M2 B9:B8,B19:B18,B31:B30:B29:B28 ; v2' x W6S&
        DSUBSP .L1 A31:A30,A29:A28,A27:A26 ; f3
||      DSUBSP .L2 B31:B30,B29:B28,B27:B26 ; f3'
        DSUBSP .L1 A31:A30,A29:A28,A25:A24 ; f4&
||      DSUBSP .L2 B31:B30,B29:B28,B25:B24 ; f4'&
        NOP 1
        DSUBSP .L1 A31:A30,A29:A28,A25:A24 ; f2&
||      DSUBSP .L2 B31:B30,B29:B28,B25:B24 ; f2'&
||      STDW .D1 A25:A24,*A4++[4] ; store f2
||      STDW .D2 B25:B24,*B0++[4] ; store f2'
        CMPYSP .M1 A11:A10,A23:A22,A31:A30:A29:A28 ; v3 x W2S&
||      CMPYSP .M2 B7:B6,B23:B22,B31:B30:B29:B28 ; v3' x W3S&
||      STDW .D1 A27:A26,*A4++[6] ; store f3
||      STDW .D2 B27:B26,*B0++[6] ; store f3'
        STDW .D1 A25:A24,*A4--[8] ; store f4&
||      STDW .D2 B25:B24,*B0--[8] ; store f4'&
        NOP 1
        ; sequence 4
        NOP 1
        DSUBSP .L1 A31:A30,A29:A28,A27:A26 ; f3&
||      DSUBSP .L2 B31:B30,B29:B28,B27:B26 ; f3'&
        NOP 1
        NOP 1
        STDW .D1 A25:A24,*A4++[4] ; store f2&
||      STDW .D2 B25:B24,*B0++[4] ; store f2'&
        STDW .D1 A27:A26,*A4++[18] ; store f3&
||      STDW .D2 B27:B26,*B0++[18] ; store f3'&

        ; end stage 2

        LDDW .D2T1 *B3--[8],A5:A4 ; N/Pas load
||      MV .L2X A5,B0 ; re-move XOR
        LDDW .D2T1 *B3,A7:A6 ; final output
        NOP 3
        MV .D1 A4,A5 ; N/Pas
        
        SHL .S1 A5,2,A5
||      MV .L2 B3,B9 ; re-move svg ptr
||      MV .D1 A3,A1 ; inp m3
||      MV .S2X A3,B1 ; inp m1
        MV .L1 A6,A2 ; outp v3
||      ADDAD .D1 A1,3,A1 ; inp m3
||      ADDAD .D2 B1,1,B1 ; inp m1
||      MV .S2X A5,B5

        MV .L2X A6,B2 ; outp v2

        ADDAD .D1 A2,2,A2 ; outp v3
||      ADDAD .D2 B2,1,B2 ; outp v2
||      MVC .S2 B5,ILC

        NOP 3
        
        SPLOOP 4
        
        LDDW .D1 *A1--[3],A31:A30 ; m3
||      LDDW .D2 *B1++[1],B31:B30 ; m1

        LDDW .D1 *A1++[7],A29:A28 ; m0
||      LDDW .D2 *B1++[3],B29:B28 ; m2

        NOP 3
        
        DSUBSP .S1X A31:A30,B31:B30,A27:A26 ; a3
||      DADDSP .S2X B31:B30,A31:A30,B27:B26 ; a1

        DSUBSP .S1X A29:A28,B29:B28,A25:A24 ; a2
||      DADDSP .S2X B29:B28,A29:A28,B25:B24 ; a0

        NOP 1
        
        XOR .L1X A27,B0,A26 ; a3ii1
||      ROTL .M1 A26,0,A27 ; a3ii2

        NOP 2
        
        DSUBSP .L1 A25:A24,A27:A26,A21:A20 ; v4
||      DADDSP .L2 B25:B24,B27:B26,B23:B22 ; v1
||      DADDSP .S1 A25:A24,A27:A26,A23:A22 ; v3
||      DSUBSP .S2 B25:B24,B27:B26,B21:B20 ; v2

        NOP 2
        
        STDW .D1 A23:A22,*A2++[1] ; store v3
||      STDW .D2 B21:B20,*B2--[1] ; store v2

        STDW .D1 A21:A20,*A2++[3] ; store v4
||      STDW .D2 B23:B22,*B2++[5] ; store v1
        
        SPKERNEL 0,0
        
        NOP 8
        NOP 4
        
        ; end stage 1

quit:
        
; loading registers
        LDDW .D2T1 *B9++,A11:A10
        LDDW .D2T1 *B9++,A13:A12
        LDDW .D2T1 *B9++,A15:A14
        LDDW .D2 *B9++,B11:B10
        LDDW .D2 *B9++,B13:B12
        LDDW .D2 *B9++,B15:B14
        LDW .D2 *B9,B3
        NOP 4
; loaded registers
        B .S2 B3
        NOP 5
