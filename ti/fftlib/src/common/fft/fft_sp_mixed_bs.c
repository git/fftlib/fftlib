/* ======================================================================= */
/*  TEXAS INSTRUMENTS, INC.                                                */
/*                                                                         */
/*  FFTLIB  FFT Library                                                    */
/*                                                                         */
/* Copyright (C) 2013 Texas Instruments Incorporated - http://www.ti.com/  */ 
/*                                                                         */
/*                                                                         */
/*  Redistribution and use in source and binary forms, with or without     */
/*  modification, are permitted provided that the following conditions     */
/*  are met:                                                               */
/*                                                                         */
/*    Redistributions of source code must retain the above copyright       */
/*    notice, this list of conditions and the following disclaimer.        */
/*                                                                         */
/*    Redistributions in binary form must reproduce the above copyright    */
/*    notice, this list of conditions and the following disclaimer in the  */
/*    documentation and/or other materials provided with the               */
/*    distribution.                                                        */
/*                                                                         */
/*    Neither the name of Texas Instruments Incorporated nor the names of  */
/*    its contributors may be used to endorse or promote products derived  */
/*    from this software without specific prior written permission.        */
/*                                                                         */
/*  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS    */
/*  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT      */
/*  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR  */
/*  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT   */
/*  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,  */
/*  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT       */
/*  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,  */
/*  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY  */
/*  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT    */
/*  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE  */
/*  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.   */
/*                                                                         */
/* ======================================================================= */

#include <xdc/std.h>
#include "c6x.h"
#include "stdlib.h"
#include <ti/dsplib/src/DSPF_sp_fftSPxSP/DSPF_sp_fftSPxSP.h>
#include <ti/dsplib/src/DSPF_sp_ifftSPxSP/DSPF_sp_ifftSPxSP.h>
#include <ti/fftlib/src/common/fft/fft_sp_cmplx_notwid_br.h>
#include "../ifft/ifft_sp_cmplx_notwid_br.h"

void fft_SPxSP_r2c (int N, float *ptr_x, float *ptr_w, float *ptr_y, int n_min)
{
  int   i,k;
  float *restrict tw2 = ptr_w + 2;  /* alignment at 4 is more efficient in split factor loop */
  float *restrict y0, *restrict y1, *restrict y2;
  __float2_t y0_i_r, y1_i_r, co_si, twid, hf_hf, mh_hf, mo_o;
  __float2_t temp1, temp2, temp3, temp4, temp5, temp6, temp7;

  /* complex to complex FFT */
  fft_sp_cmplx_notwid_br((double*)ptr_x, N/2, (double*)ptr_y, (double*)ptr_w);

  /* split factors */
  y0_i_r = _amem8_f2(&ptr_y[0]);
  y1_i_r = _amem8_f2(&ptr_y[N/2]);

  _amem8_f2(&ptr_y[N]) = _ftof2(0.0, _lof2(y0_i_r) - _hif2(y0_i_r));
  _amem8_f2(&ptr_y[0]) = _ftof2(0.0, _lof2(y0_i_r) + _hif2(y0_i_r));
  _amem8_f2(&ptr_y[N+N/2]) = y1_i_r;
  ptr_y[N/2+1] = -_hif2(y1_i_r);

  hf_hf = _ftof2( 0.5, 0.5);
  mh_hf = _ftof2(-0.5, 0.5);
  mo_o  = _ftof2(-1.0, 1.0);

  co_si = _amem8_f2(&tw2[0]);

  y0 = ptr_y;
  y1 = ptr_y + N;
  y2 = ptr_y + 2*N;

  y0_i_r = _amem8_f2(&y0[2]);
  y1_i_r = _amem8_f2(&y1[-2]);

  temp1 = _dmpysp(_daddsp(y1_i_r, y0_i_r), hf_hf);
  temp2 = _dmpysp(_dsubsp(y1_i_r, y0_i_r), mh_hf);
  temp3 = _ftof2(_hif2(temp2), _lof2(temp1));
  temp4 = _ftof2(_lof2(temp2), _hif2(temp1));
  temp5 = _complex_mpysp(temp4, co_si);
  temp6 = _daddsp(temp3, temp5);
  temp7 = _dsubsp(temp3, temp5);

  _amem8_f2(&y0[2])  = temp6;
  _amem8_f2(&y1[-2]) = _dmpysp(temp7, mo_o);
  _amem8_f2(&y1[2])  = temp7;
  _amem8_f2(&y2[-2]) = _dmpysp(temp6, mo_o);

  #pragma UNROLL(2)
  for (i = 4; i < N/2; i += 2) {
      twid   = _amem8_f2(&tw2[i-2]);

      k      = -i;
      y0_i_r = _amem8_f2(&y0[i]);
      y1_i_r = _amem8_f2(&y1[k]);

      temp1  = _dmpysp(_daddsp(y1_i_r, y0_i_r), hf_hf);
      temp2  = _dmpysp(_dsubsp(y1_i_r, y0_i_r), mh_hf);
      temp3  = _ftof2(_hif2(temp2), _lof2(temp1));
      temp4  = _ftof2(_lof2(temp2), _hif2(temp1));
      temp5  = _complex_mpysp(temp4, twid);
      temp6  = _daddsp(temp3, temp5);
      temp7  = _dsubsp(temp3, temp5);

      _amem8_f2(&y0[i]) = temp6;
      _amem8_f2(&y1[k]) = _dmpysp(temp7, mo_o);
      _amem8_f2(&y1[i]) = temp7;
      _amem8_f2(&y2[k]) = _dmpysp(temp6, mo_o);
  }
} /* fft_SPxSP_r2c */

void fft_SPxSP_mixed_radix_r2c (int N, float *ptr_x, float *ptr_w, float *ptr_y,
                                int N_p2, int s_r3, int s_r5)
{
  int i, j, k, l, m, dum, k_rev, l1, l2, l3, l4, predj, tw_offset, stride, fft_jmp, n2, n3;
  float *restrict tw1 = ptr_w + N/2;
  float *restrict tw2 = ptr_w + 2;  /* alignment at 4 is more efficient in split factor loop */
  float *restrict y0, *restrict y1, *restrict y2;
  float *restrict x0, *restrict x1, *restrict w;
  __float2_t y0_i_r, y1_i_r, co_si, twid, hf_hf, mh_hf, mo_o;
  __float2_t temp1, temp2, temp3, temp4, temp5, temp6, temp7, temp8;
  __float2_t x_10, x_l1_10, x_l2_10, x_l3_10, x_l4_10, co10_si10, co20_si20, co30_si30, co40_si40;
  __float2_t mp5_mp5, mpi3_pi3, si2pi5, si4pi5, co2pi5, co4pi5;

  mp5_mp5   = _ftof2(-0.5f, -0.5f);
  mpi3_pi3  = _ftof2(-0.8660254f,  0.8660254f);   /* -sqrt(3)/2, sqrt(3)/2 */

  stride    = N;
  tw_offset = 0;
  fft_jmp   = 2*N;

  /* radix-3 loop */
  for (i = 0; i < s_r3; i ++) {

      j = 0;
      fft_jmp /= 3;

      l1 = stride/3;
      l2 = l1*2;

      x0 = ptr_x;
      w  = tw1 + tw_offset;
      tw_offset += fft_jmp;

      _nassert((int)(w) % 8 == 0);
      _nassert((int)(x0) % 8 == 0);
      _nassert((int)(N) % 12 == 0);
      _nassert(N >= 12);

      #pragma UNROLL(2)
      for (k = 0; k < N/6; k++) {

          /*-------------------------------------------------------------*/
          /*  Radix-3 loop                                               */
          /*-------------------------------------------------------------*/
          co10_si10 = _amem8_f2_const(&w[j]);
          co20_si20 = _amem8_f2_const(&w[j+2]);

          x_10    = _amem8_f2(&x0[0]);
          x_l1_10 = _amem8_f2(&x0[l1]);
          x_l2_10 = _amem8_f2(&x0[l2]);

          temp1 = _daddsp(x_l1_10, x_l2_10);
          temp2 = _dsubsp(x_l1_10, x_l2_10);
          temp3 = _dmpysp(mp5_mp5, temp1);
          temp4 = _dmpysp(mpi3_pi3, temp2);
          temp5 = _ftof2 (_lof2(temp4), _hif2(temp4));
          temp6 = _daddsp(temp3, temp5);
          temp7 = _dsubsp(temp3, temp5);

          x1 = (float*)_mvd((int)x0);

          _amem8_f2(&x1[0])  = _daddsp(x_10, temp1);
          _amem8_f2(&x1[l1]) = _complex_mpysp(co10_si10, _daddsp(x_10, temp7));
          _amem8_f2(&x1[l2]) = _complex_mpysp(co20_si20, _daddsp(x_10, temp6));

          j  += 4;
          x0 += 2;
          predj = (j - fft_jmp);
          if (!predj) x0 += fft_jmp;
          if (!predj) j = 0;
      }
      stride /= 3;
  }

  /* radix-5 loop */
  si2pi5  = _ftof2( 0.9510565163f,  -0.9510565163f);    /* sin(2Pi/5) */
  si4pi5  = _ftof2( 0.5877852523f,  -0.5877852523f);    /* sin(4Pi/5) */
  co2pi5  = _ftof2( 0.309016994375f, 0.309016994375f);  /* cos(2Pi/5) */
  co4pi5  = _ftof2(-0.809016994375f,-0.809016994375f);  /* cos(4Pi/5) */
  fft_jmp *= 2; /* radix-5 has 8 twiddle vs radix-3 4 twiddles */
  for (i = 0; i < s_r5; i ++) {

      j = 0;
      fft_jmp /= 5;

      l1 = stride/5;
      l2 = l1*2;
      l3 = l1*3;
      l4 = l1*4;

      x0 = ptr_x;
      w  = tw1 + tw_offset;
      tw_offset += fft_jmp;

      _nassert((int)(w) % 8 == 0);
      _nassert((int)(x0) % 8 == 0);
      _nassert((int)(N) % 20 == 0);
      _nassert(N >= 20);

      for (k = 0; k < N/10; k++) {

          /*-------------------------------------------------------------*/
          /*  Radix-5 loop                                               */
          /*-------------------------------------------------------------*/
          co10_si10 = _amem8_f2_const(&w[j]);
          co20_si20 = _amem8_f2_const(&w[j+2]);
          co30_si30 = _amem8_f2_const(&w[j+4]);
          co40_si40 = _amem8_f2_const(&w[j+6]);

          x_10    = _amem8_f2(&x0[0]);
          x_l1_10 = _amem8_f2(&x0[l1]);
          x_l2_10 = _amem8_f2(&x0[l2]);
          x_l3_10 = _amem8_f2(&x0[l3]);
          x_l4_10 = _amem8_f2(&x0[l4]);

          temp1 = _daddsp(x_l1_10, x_l4_10);
          temp2 = _dsubsp(x_l1_10, x_l4_10);
          temp3 = _daddsp(x_l2_10, x_l3_10);
          temp4 = _dsubsp(x_l2_10, x_l3_10);

          temp5 = _daddsp(_dmpysp(co2pi5, temp1), _dmpysp(co4pi5, temp3));
          temp6 = _daddsp(_dmpysp(co4pi5, temp1), _dmpysp(co2pi5, temp3));
          temp7 = _daddsp(_dmpysp(si2pi5, temp2), _dmpysp(si4pi5, temp4));
          temp8 = _dsubsp(_dmpysp(si4pi5, temp2), _dmpysp(si2pi5, temp4));
          temp7 = _ftof2 (_lof2(temp7), _hif2(temp7));
          temp8 = _ftof2 (_lof2(temp8), _hif2(temp8));

          x1 = (float*)_mvd((int)x0);

          _amem8_f2(&x1[0])  = _daddsp(x_10, _daddsp(temp1, temp3));
          _amem8_f2(&x1[l1]) = _complex_mpysp(co10_si10, _daddsp(x_10, _daddsp(temp5, temp7)));
          _amem8_f2(&x1[l2]) = _complex_mpysp(co20_si20, _daddsp(x_10, _daddsp(temp6, temp8)));
          _amem8_f2(&x1[l3]) = _complex_mpysp(co30_si30, _daddsp(x_10, _dsubsp(temp6, temp8)));
          _amem8_f2(&x1[l4]) = _complex_mpysp(co40_si40, _daddsp(x_10, _dsubsp(temp5, temp7)));

          j  += 8;
          x0 += 2;
          predj = (j - fft_jmp);
          if (!predj) x0 += fft_jmp;
          if (!predj) j = 0;
      }
      stride /= 5;
  }

  x1 = ptr_x;
  y1 = ptr_y; 
  for (i = 0; i < N; i += N_p2*2) {
    /* radix-2 loop */
    fft_sp_cmplx_notwid_br((double*)&x1[i], N_p2, (double*)&y1[i], (double*)ptr_w);
  }

  /* radix-3 bit reversal */
  x1 = ptr_x;
  n2 = 30 - _norm(N_p2);
  n3 = N>>(n2+1);
   
  for (i = 0, l = 0; i < N; i+= 2*N_p2, l++) {
    k = l;
    k_rev = 0;
    for (m = 0; m < s_r5; m++) {
      dum = k/5;
      k_rev = k_rev*5 + k - dum*5;
      k = dum;
    }
    for (m = 0; m < s_r3; m++) {
      dum = k/3;
      k_rev = k_rev*3 + k - dum*3;
      k = dum;
    }
    y1 = ptr_y + i;
    
    _nassert((int)(N_p2) % 4 == 0);
    _nassert(N_p2 >= 4);
    #pragma UNROLL(2)
    for (j = 0; j < N_p2; j++) {
      _amem8_f2(&x1[(j*n3 + k_rev)*2]) = _amem8_f2(&y1[j*2]);
    }
  }

  /* split factors */
  y0_i_r = _amem8_f2(&ptr_x[0]);
  y1_i_r = _amem8_f2(&ptr_x[N/2]);

  _amem8_f2(&ptr_y[N]) = _ftof2(0.0, _lof2(y0_i_r) - _hif2(y0_i_r));
  _amem8_f2(&ptr_y[0]) = _ftof2(0.0, _lof2(y0_i_r) + _hif2(y0_i_r));
  _amem8_f2(&ptr_y[N+N/2]) = y1_i_r;
  _amem8_f2(&ptr_y[N/2]) = y1_i_r;
  ptr_y[N/2+1] = -_hif2(y1_i_r);

  hf_hf = _ftof2( 0.5f, 0.5f);
  mh_hf = _ftof2(-0.5f, 0.5f);
  mo_o  = _ftof2(-1.0f, 1.0f);

  co_si = _amem8_f2(&tw2[0]);

  x0 = ptr_x;
  x1 = ptr_x + N;
  y0 = ptr_y;
  y1 = ptr_y + N;
  y2 = ptr_y + 2*N;

  y0_i_r = _amem8_f2(&x0[2]);
  y1_i_r = _amem8_f2(&x1[-2]);

  temp1 = _dmpysp(_daddsp(y1_i_r, y0_i_r), hf_hf);
  temp2 = _dmpysp(_dsubsp(y1_i_r, y0_i_r), mh_hf);
  temp3 = _ftof2(_hif2(temp2), _lof2(temp1));
  temp4 = _ftof2(_lof2(temp2), _hif2(temp1));
  temp5 = _complex_mpysp(temp4, co_si);
  temp6 = _daddsp(temp3, temp5);
  temp7 = _dsubsp(temp3, temp5);

  _amem8_f2(&y0[2])  = temp6;
  _amem8_f2(&y1[-2]) = _dmpysp(temp7, mo_o);
  _amem8_f2(&y1[2])  = temp7;
  _amem8_f2(&y2[-2]) = _dmpysp(temp6, mo_o);

  #pragma UNROLL(2)
  for (i = 4; i < N/2; i += 2) {
      twid   = _amem8_f2(&tw2[i-2]);

      k      = -i;
      y0_i_r = _amem8_f2(&x0[i]);
      y1_i_r = _amem8_f2(&x1[k]);

      temp1  = _dmpysp(_daddsp(y1_i_r, y0_i_r), hf_hf);
      temp2  = _dmpysp(_dsubsp(y1_i_r, y0_i_r), mh_hf);
      temp3  = _ftof2(_hif2(temp2), _lof2(temp1));
      temp4  = _ftof2(_lof2(temp2), _hif2(temp1));
      temp5  = _complex_mpysp(temp4, twid);
      temp6  = _daddsp(temp3, temp5);
      temp7  = _dsubsp(temp3, temp5);

      _amem8_f2(&y0[i]) = temp6;
      _amem8_f2(&y1[k]) = _dmpysp(temp7, mo_o);
      _amem8_f2(&y1[i]) = temp7;
      _amem8_f2(&y2[k]) = _dmpysp(temp6, mo_o);

  }
} /* fft_SPxSP_mixed_radix_r2c */

void fft_SPxSP_r2c_bs (
    int   N,           /* signal size           */
    int   M,           /* FFT size              */
    int   rad,         /* FFT radix             */
    float * in,        /* input pointer         */
    float * out,       /* output  pointer       */
    float * tw,        /* twiddle and phase factor pointer  */
    float * Bn,        /* pointer to FFT of bn  */
    float * workbuf    /* work buffer pointer   */
    )
{
    int   i, N1;
    float *restrict Bk  = tw + 2;             /* phase factor pointer */
    float *restrict twi = tw + 2 + 2*N;       /* twiddle for ifft */
    float *restrict An, *restrict output;
    __float2_t temp1, temp2, temp, temp_out1, temp_out2;
    __float2_t temp_in, temp_bk1, temp_bk2;

    /* Construct and zero pad the signal to the right size */
    _nassert(N >= 8);

    /* Bluestein's FFT for fft size not power of 2 */

    /* ------------------------------------------------------------------------
     * initialize the work buffer to zeros, the workbuffer is 4*M words long,
     * but only need to initialize the part without signals
     * ---------------------------------------------------------------------- */

    for (i = 0; i<2*(M-N); i+=1)
    {
      _amem8_f2(&workbuf[2*i+2*N]) = 0;
    }

    /*---------------------------------------------------------------
     *  construct the input signal with phase factor An = in*Bk*.
     *  Note, Bk* = Re(Bk) - i*im(Bk)
     * --------------------------------------------------------------*/
    An     = workbuf;
    output = workbuf+2*M;

    /* make sure N1 is even number */
    N1 = (N>>2)<<2;

#pragma MUST_ITERATE(4,,2)
#pragma UNROLL(2)
    for (i = 0; i<(N1>>1); i++)
    {
      temp_in = _amem8_f2(&in[2*i]);
      temp_bk1 = _amem8_f2(&Bk[4*i]);
      temp_bk2 = _amem8_f2(&Bk[4*i+2]);

#ifdef _LITTLE_ENDIAN
      temp_out1 = _dmpysp (_ftof2(-1*_lof2(temp_in),_lof2(temp_in)),temp_bk1);
      temp_out2 = _dmpysp (_ftof2(-1*_hif2(temp_in),_hif2(temp_in)),temp_bk2);

#else
      temp_out1 = _dmpysp (_ftof2(_hif2(temp_in),(-1)*_hif2(temp_in)),temp_bk1);
      temp_out2 = _dmpysp (_ftof2(_lof2(temp_in),(-1)*_lof2(temp_in)),temp_bk2);
#endif
      _amem8_f2(&An[4*i])   = temp_out1;
      _amem8_f2(&An[4*i+2]) = temp_out2;
    }

    /* finish off the last samples */
    for (i = N1; i < N; i++)
    {
        temp_bk1 = _amem8_f2(&Bk[2*i]);
#ifdef _LITTLE_ENDIAN
        temp_out1 = _dmpysp (_ftof2(-1*in[i],in[i]),temp_bk1);
#else
        temp_out1 = _dmpysp (_ftof2(in[i],(-1)*in[i]),temp_bk1);
#endif
        _amem8_f2(&An[2*i])   = temp_out1;
    }
    /* FFT of the constructed signal An */

    fft_sp_cmplx_notwid_br((double*)An, M, (double*)output, (double*)tw);

    /* IFFT of An*Bn */
    /* Bn is pre-calculated */
    output = workbuf;
    An = workbuf+2*M;

#pragma MUST_ITERATE(4,,4)

    for (i = 0; i<M; i++)
    {
      temp1 = _amem8_f2(&An[2*i]);
      temp2 = _amem8_f2(&Bn[2*i]);
      temp  = _complex_mpysp(temp1,temp2);
#ifdef _LITTLE_ENDIAN
      _amem8_f2(&output[2*i]) = _ftof2(_lof2(temp), -_hif2(temp));
#else
      _amem8_f2(&output[2*i]) = temp;
#endif
    }

    ifft_sp_cmplx_notwid_br((double*)output, M, (double*)An, (double*)twi);

    /* multiply with bk* which is the conjugate of bk */

    /* make sure N1 is even number */
    N1 = (N>>1)<<1;

#pragma MUST_ITERATE(4,,2)
#pragma UNROLL(2)
    for (i = 0; i<N1; i++)
    {
      temp1 = _amem8_f2(&An[2*i]);
      temp2 = _amem8_f2(&Bk[2*i]);
      temp    = _complex_conjugate_mpysp(temp1,temp2);
#ifdef _LITTLE_ENDIAN
      _amem8_f2(&out[2*i]) = _ftof2(_lof2(temp), _hif2(temp));
#else
      _amem8_f2(&out[2*i]) = _ftof2(_hif2(temp), -_lof2(temp));
#endif
    }

    /* finish off the last samples */
    for (i = N1; i<N; i++)
    {
      temp1 = _amem8_f2(&An[2*i]);
      temp2 = _amem8_f2(&Bk[2*i]);
      temp    = _complex_conjugate_mpysp(temp1,temp2);
#ifdef _LITTLE_ENDIAN
      _amem8_f2(&out[2*i]) = _ftof2(_lof2(temp), _hif2(temp));
#else
      _amem8_f2(&out[2*i]) = _ftof2(_hif2(temp), -_lof2(temp));
#endif
    }
} /* fft_SPxSP_r2c_bs */

void fft_SPxSP_mixed_radix_c2c (int N, float *ptr_x, float *ptr_w, float *ptr_y,
                                int N_p2, int s_r3, int s_r5)
{
  int i, j, k, l, m, dum, k_rev, l1, l2, l3, l4, predj, tw_offset, stride, fft_jmp, n2, n3;
  float *restrict tw1 = ptr_w + 2;
  float *restrict y1, *restrict x0, *restrict x1, *restrict w;
  __float2_t temp1, temp2, temp3, temp4, temp5, temp6, temp7, temp8;
  __float2_t x_10, x_l1_10, x_l2_10, x_l3_10, x_l4_10, co10_si10, co20_si20, co30_si30, co40_si40;
  __float2_t mp5_mp5, mpi3_pi3, si2pi5, si4pi5, co2pi5, co4pi5;

  mp5_mp5   = _ftof2(-0.5f, -0.5f);
  mpi3_pi3  = _ftof2(-0.8660254f,  0.8660254f);   /* -sqrt(3)/2, sqrt(3)/2 */

  stride    = 2*N;
  tw_offset = 0;
  fft_jmp   = 4*N;

  /* radix-3 loop */
  for (i = 0; i < s_r3; i ++) {

      j = 0;
      fft_jmp /= 3;

      l1 = stride/3;
      l2 = l1*2;

      x0 = ptr_x;
      w  = tw1 + tw_offset;
      tw_offset += fft_jmp;

      _nassert((int)(w) % 8 == 0);
      _nassert((int)(x0) % 8 == 0);
      _nassert((int)(N) % 6 == 0);
      _nassert(N >= 6);

      #pragma UNROLL(2)
      for (k = 0; k < N/3; k++) {

          /*-------------------------------------------------------------*/
          /*  Radix-3 loop                                               */
          /*-------------------------------------------------------------*/
          co10_si10 = _amem8_f2_const(&w[j]);
          co20_si20 = _amem8_f2_const(&w[j+2]);

          x_10    = _amem8_f2(&x0[0]);
          x_l1_10 = _amem8_f2(&x0[l1]);
          x_l2_10 = _amem8_f2(&x0[l2]);

          temp1 = _daddsp(x_l1_10, x_l2_10);
          temp2 = _dsubsp(x_l1_10, x_l2_10);
          temp3 = _dmpysp(mp5_mp5, temp1);
          temp4 = _dmpysp(mpi3_pi3, temp2);
          temp5 = _ftof2 (_lof2(temp4), _hif2(temp4));
          temp6 = _daddsp(temp3, temp5);
          temp7 = _dsubsp(temp3, temp5);

          x1 = (float*)_mvd((int)x0);

          _amem8_f2(&x1[0])  = _daddsp(x_10, temp1);
          _amem8_f2(&x1[l1]) = _complex_mpysp(co10_si10, _daddsp(x_10, temp7));
          _amem8_f2(&x1[l2]) = _complex_mpysp(co20_si20, _daddsp(x_10, temp6));

          j  += 4;
          x0 += 2;
          predj = (j - fft_jmp);
          if (!predj) x0 += fft_jmp;
          if (!predj) j = 0;
      }
      stride /= 3;
  }

  /* radix-5 loop */
  si2pi5  = _ftof2( 0.9510565163f,  -0.9510565163f);    /* sin(2Pi/5) */
  si4pi5  = _ftof2( 0.5877852523f,  -0.5877852523f);    /* sin(4Pi/5) */
  co2pi5  = _ftof2( 0.309016994375f, 0.309016994375f);  /* cos(2Pi/5) */
  co4pi5  = _ftof2(-0.809016994375f,-0.809016994375f);  /* cos(4Pi/5) */
  fft_jmp *= 2; /* radix-5 has 8 twiddle vs radix-3 4 twiddles */
  for (i = 0; i < s_r5; i ++) {

      j = 0;
      fft_jmp /= 5;

      l1 = stride/5;
      l2 = l1*2;
      l3 = l1*3;
      l4 = l1*4;

      x0 = ptr_x;
      w  = tw1 + tw_offset;
      tw_offset += fft_jmp;

      _nassert((int)(w) % 8 == 0);
      _nassert((int)(x0) % 8 == 0);
      _nassert((int)(N) % 10 == 0);
      _nassert(N >= 10);

      for (k = 0; k < N/5; k++) {

          /*-------------------------------------------------------------*/
          /*  Radix-5 loop                                               */
          /*-------------------------------------------------------------*/
          co10_si10 = _amem8_f2_const(&w[j]);
          co20_si20 = _amem8_f2_const(&w[j+2]);
          co30_si30 = _amem8_f2_const(&w[j+4]);
          co40_si40 = _amem8_f2_const(&w[j+6]);

          x_10    = _amem8_f2(&x0[0]);
          x_l1_10 = _amem8_f2(&x0[l1]);
          x_l2_10 = _amem8_f2(&x0[l2]);
          x_l3_10 = _amem8_f2(&x0[l3]);
          x_l4_10 = _amem8_f2(&x0[l4]);

          temp1 = _daddsp(x_l1_10, x_l4_10);
          temp2 = _dsubsp(x_l1_10, x_l4_10);
          temp3 = _daddsp(x_l2_10, x_l3_10);
          temp4 = _dsubsp(x_l2_10, x_l3_10);

          temp5 = _daddsp(_dmpysp(co2pi5, temp1), _dmpysp(co4pi5, temp3));
          temp6 = _daddsp(_dmpysp(co4pi5, temp1), _dmpysp(co2pi5, temp3));
          temp7 = _daddsp(_dmpysp(si2pi5, temp2), _dmpysp(si4pi5, temp4));
          temp8 = _dsubsp(_dmpysp(si4pi5, temp2), _dmpysp(si2pi5, temp4));
          temp7 = _ftof2 (_lof2(temp7), _hif2(temp7));
          temp8 = _ftof2 (_lof2(temp8), _hif2(temp8));

          x1 = (float*)_mvd((int)x0);

          _amem8_f2(&x1[0])  = _daddsp(x_10, _daddsp(temp1, temp3));
          _amem8_f2(&x1[l1]) = _complex_mpysp(co10_si10, _daddsp(x_10, _daddsp(temp5, temp7)));
          _amem8_f2(&x1[l2]) = _complex_mpysp(co20_si20, _daddsp(x_10, _daddsp(temp6, temp8)));
          _amem8_f2(&x1[l3]) = _complex_mpysp(co30_si30, _daddsp(x_10, _dsubsp(temp6, temp8)));
          _amem8_f2(&x1[l4]) = _complex_mpysp(co40_si40, _daddsp(x_10, _dsubsp(temp5, temp7)));

          j  += 8;
          x0 += 2;
          predj = (j - fft_jmp);
          if (!predj) x0 += fft_jmp;
          if (!predj) j = 0;
      }
      stride /= 5;
  }

  x1 = ptr_x;
  y1 = ptr_y; 
  for (i = 0; i < 2*N; i += N_p2*2) {
    /* radix-2 loop */
    fft_sp_cmplx_notwid_br((double*)&x1[i], N_p2, (double*)&y1[i], (double*)ptr_w);
  }

  /* radix-3,5 bit reversal */
  x1 = ptr_x;
  n2 = 30 - _norm(N_p2);
  n3 = N>>n2;
   
  for (i = 0, l = 0; i < 2*N; i+= 2*N_p2, l++) {
    k = l;
    k_rev = 0;
    for (m = 0; m < s_r5; m++) {
      dum = k/5;
      k_rev = k_rev*5 + k - dum*5;
      k = dum;
    }
    for (m = 0; m < s_r3; m++) {
      dum = k/3;
      k_rev = k_rev*3 + k - dum*3;
      k = dum;
    }
    y1 = ptr_y + i;
    
    _nassert((int)(N_p2) % 4 == 0);
    _nassert(N_p2 >= 4);
    #pragma UNROLL(2)
    for (j = 0; j < N_p2; j++) {
      _amem8_f2(&x1[(j*n3 + k_rev)*2]) = _amem8_f2(&y1[j*2]);
    }
  }

  x1 = ptr_x;
  y1 = ptr_y; 
  for (i = 0; i < 2*N; i += 2) {
    _amem8_f2(&y1[i]) = _amem8_f2(&x1[i]);
  }
} /* fft_SPxSP_mixed_radix_c2c */

void fft_SPxSP_c2c_bs (
    int   N,           /* signal size           */
    int   M,           /* FFT size              */
    int   rad,         /* FFT radix             */
    float *in,         /* input pointer         */
    float *out,        /* output  pointer       */
    float *tw,         /* twiddle and phase factor pointer  */
    float *Bn,         /* pointer to FFT of bn  */
    float *workbuf     /* work buffer pointer   */
    )
{
    int   i, N1;
    float *restrict Bk = tw+2;
    float *restrict twi = tw+2+2*N;
    float *restrict An, *restrict output;
    __float2_t temp, temp1, temp2;

    /* Construct and zero pad the signal to the right size */
    _nassert(N >= 8);

    /* Bluestein's FFT when signal size is not power of 2 */

    /* ------------------------------------------------------------------------
     * initialize the work buffer to zeros, the workbuffer is 4*M words long,
     * but only need to initialize the part without signals
     * ---------------------------------------------------------------------- */

    for (i = 0; i<2*(M-N); i+=1)
    {
      _amem8_f2(&workbuf[2*i+2*N]) = 0;
    }

    /*---------------------------------------------------------------
     *  construct the input signal with phase factor An = in*Bk*.
     *  Note, Bk* = Re(Bk) - i*im(Bk)
     * --------------------------------------------------------------*/
    An     = workbuf;
    output = workbuf+2*M;

    /* make sure N1 is even */
    N1 = (N>>1)<<1;

#pragma MUST_ITERATE(4,,2)
#pragma UNROLL(2)
    for (i = 0; i<N1; i++)
    {
      temp1 = _amem8_f2(&in[2*i]);
      temp2 = _amem8_f2(&Bk[2*i]);

      temp    = _complex_conjugate_mpysp(temp1,temp2);
#ifdef _LITTLE_ENDIAN
      _amem8_f2(&An[2*i]) = _ftof2(_lof2(temp), _hif2(temp));
#else
      _amem8_f2(&An[2*i]) = _ftof2(_hif2(temp), -_lof2(temp));
#endif
    }

    if (N1 < N) {
      temp1 = _amem8_f2(&in[2*N1]);
      temp2 = _amem8_f2(&Bk[2*N1]);

      temp    = _complex_conjugate_mpysp(temp1,temp2);
#ifdef _LITTLE_ENDIAN
      _amem8_f2(&An[2*N1]) = _ftof2(_lof2(temp), _hif2(temp));
#else
      _amem8_f2(&An[2*N1]) = _ftof2(_hif2(temp), -_lof2(temp));
#endif
    }

    /* FFT of the constructed signal An */
    fft_sp_cmplx_notwid_br((double*)An, M, (double*)output, (double*)tw);

    /* IFFT of An*Bn */
    /* Bn is pre-calculated */
    output = workbuf;
    An = workbuf+2*M;

#pragma MUST_ITERATE(4,,4)

    for (i = 0; i<M; i++)
    {
      temp1 = _amem8_f2(&An[2*i]);
      temp2 = _amem8_f2(&Bn[2*i]);
      temp  = _complex_mpysp(temp1,temp2);
#ifdef _LITTLE_ENDIAN
      _amem8_f2(&output[2*i]) = _ftof2(_lof2(temp), -_hif2(temp));
#else
      _amem8_f2(&output[2*i]) = temp;
#endif
    }

    ifft_sp_cmplx_notwid_br((double*)output, M, (double*)An, (double*)twi);


    /* multiply the result of ifft() with bk* which is the conjugate of bk */

    /* make sure N1 is even */
    N1 = (N>>2)<<2;

#pragma MUST_ITERATE(4,,2)
#pragma UNROLL(2)
    for (i = 0; i<N1; i++)
    {
      temp1 = _amem8_f2(&An[2*i]);
      temp2 = _amem8_f2(&Bk[2*i]);
      temp    = _complex_conjugate_mpysp(temp1,temp2);
#ifdef _LITTLE_ENDIAN
      _amem8_f2(&out[2*i]) = _ftof2(_lof2(temp), _hif2(temp));
#else
      _amem8_f2(&out[2*i]) = _ftof2(_hif2(temp), -_lof2(temp));
#endif
    }

    /* finish off the last samples */
    for (i = N1; i<N; i++)
    {
      temp1 = _amem8_f2(&An[2*i]);
      temp2 = _amem8_f2(&Bk[2*i]);
      temp    = _complex_conjugate_mpysp(temp1,temp2);
#ifdef _LITTLE_ENDIAN
      _amem8_f2(&out[2*i]) = _ftof2(_lof2(temp), _hif2(temp));
#else
      _amem8_f2(&out[2*i]) = _ftof2(_hif2(temp), -_lof2(temp));
#endif
    }
} /* fft_SPxSP_c2c_bs */

/* ======================================================================== */
/*  End of file:  fft_sp_mixed_bs.c                                         */
/* ------------------------------------------------------------------------ */
/*            Copyright (c) 2013 Texas Instruments, Incorporated.           */
/*                           All Rights Reserved.                           */
/* ======================================================================== */
