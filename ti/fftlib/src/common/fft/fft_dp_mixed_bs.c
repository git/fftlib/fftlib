/* ======================================================================= */
/*  TEXAS INSTRUMENTS, INC.                                                */
/*                                                                         */
/*  FFTLIB  FFT Library                                                    */
/*                                                                         */
/* Copyright (C) 2013 Texas Instruments Incorporated - http://www.ti.com/  */ 
/*                                                                         */
/*                                                                         */
/*  Redistribution and use in source and binary forms, with or without     */
/*  modification, are permitted provided that the following conditions     */
/*  are met:                                                               */
/*                                                                         */
/*    Redistributions of source code must retain the above copyright       */
/*    notice, this list of conditions and the following disclaimer.        */
/*                                                                         */
/*    Redistributions in binary form must reproduce the above copyright    */
/*    notice, this list of conditions and the following disclaimer in the  */
/*    documentation and/or other materials provided with the               */
/*    distribution.                                                        */
/*                                                                         */
/*    Neither the name of Texas Instruments Incorporated nor the names of  */
/*    its contributors may be used to endorse or promote products derived  */
/*    from this software without specific prior written permission.        */
/*                                                                         */
/*  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS    */
/*  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT      */
/*  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR  */
/*  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT   */
/*  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,  */
/*  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT       */
/*  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,  */
/*  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY  */
/*  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT    */
/*  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE  */
/*  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.   */
/*                                                                         */
/* ======================================================================= */

#include <xdc/std.h>
#include "c6x.h"
#include "stdlib.h"
#include <ti/dsplib/src/DSPF_dp_fftDPxDP/DSPF_dp_fftDPxDP.h>

void fft_DPxDP_mixed_radix (int N, double *ptr_x, double *ptr_w, double *ptr_y,
                                int N_p2, int s_r3, int s_r5, int rad)
{
  int i, j, k, l, m, dum, k_rev, l1, l2, l3, l4, predj, tw_offset, stride, fft_jmp, n2, n3;
  double *restrict tw1 = ptr_w + N_p2*2;
  double *restrict y1;
  double *restrict x0, *restrict x1, *restrict w;
  double temp0, temp1, temp2, temp3, temp4, temp5, temp6, temp7, temp8, temp9, tempa, tempb;
  double t0, t1, t2, t3, t4, t5, t6, t7;
  double x_r, x_i, x_l1_r, x_l1_i, x_l2_r, x_l2_i, x_l3_r, x_l3_i, x_l4_r, x_l4_i;
  double co10, si10, co20, si20, co30, si30, co40, si40;
  double co2pi3 =-0.50000000000000,   /* cos(2*pi/3) */
         si2pi3 = 0.86602540378444,   /* sin(2*pi/3) */
         co2pi5 = 0.30901699437495,   /* cos(2*pi/5) */
         si2pi5 = 0.95105651629515,   /* sin(2*pi/5) */
         co4pi5 =-0.80901699437495,   /* cos(4*pi/5) */
         si4pi5 = 0.58778525229247;   /* sin(4*pi/5) */

  stride    = 2*N;
  tw_offset = 0;
  fft_jmp   = 4*N;

  /* radix-3 loop */
  for (i = 0; i < s_r3; i ++) {

      j = 0;
      fft_jmp /= 3;

      l1 = stride/3;
      l2 = l1*2;

      x0 = ptr_x;
      w  = tw1 + tw_offset;
      tw_offset += fft_jmp;

      _nassert((int)(w) % 8 == 0);
      _nassert((int)(x0) % 8 == 0);
      _nassert((int)(N) % 6 == 0);
      _nassert(N >= 6);

      for (k = 0; k < N/3; k++) {

          /*-------------------------------------------------------------*/
          /*  Radix-3 loop                                               */
          /*-------------------------------------------------------------*/
          co10 = w[j];    /* get twiddle factors */
          si10 = w[j+1];
          co20 = w[j+2];
          si20 = w[j+3];

          x_r    = x0[0];
          x_i    = x0[1];
          x_l1_r = x0[l1];
          x_l1_i = x0[l1+1];
          x_l2_r = x0[l2];
          x_l2_i = x0[l2+1];

          x1 = (double*)_mvd((int)x0);

          /* internal DFT computation */
          temp0 = x_l1_r + x_l2_r;
          temp1 = x_l1_i + x_l2_i;
          temp2 = x_l1_r - x_l2_r;
          temp3 = x_l1_i - x_l2_i;

          temp4 = temp0 * co2pi3;
          temp5 = temp1 * co2pi3;
          temp6 = temp2 * si2pi3;
          temp7 = temp3 * si2pi3;

          temp8 = x_r + temp4 + temp7;
          temp9 = x_i + temp5 - temp6;
          tempa = x_r + temp4 - temp7;
          tempb = x_i + temp5 + temp6;

          x1[0]    = x_r + temp0;
          x1[1]    = x_i + temp1;
          x1[l1]   = co10*temp8 + si10*temp9;
          x1[l1+1] = co10*temp9 - si10*temp8;
          x1[l2]   = co20*tempa + si20*tempb;
          x1[l2+1] = co20*tempb - si20*tempa;

          j  += 4;
          x0 += 2;
          predj = (j - fft_jmp);
          if (!predj) x0 += fft_jmp;
          if (!predj) j = 0;
      }
      stride /= 3;
  }

  /* radix-5 loop */
  fft_jmp *= 2; /* radix-5 has 8 twiddle vs radix-3 4 twiddles */
  for (i = 0; i < s_r5; i ++) {

      j = 0;
      fft_jmp /= 5;

      l1 = stride/5;
      l2 = l1*2;
      l3 = l1*3;
      l4 = l1*4;

      x0 = ptr_x;

      _nassert((int)(N) % 10 == 0);
      _nassert(N >= 10);

      for (k = 0; k < N/5; k++) {

          /*-------------------------------------------------------------*/
          /*  Radix-5 butterfly loop part 1 real-part                    */
          /*-------------------------------------------------------------*/
          x_r    = x0[0];
          x_l1_r = x0[l1];
          x_l2_r = x0[l2];
          x_l3_r = x0[l3];
          x_l4_r = x0[l4];

          x1 = (double*)_mvd((int)x0);

          temp0 = x_l1_r + x_l4_r;
          temp2 = x_l1_r - x_l4_r;
          temp4 = x_l2_r + x_l3_r;
          temp6 = x_l2_r - x_l3_r;

          x1[0]    = x_r + temp0 + temp4;
          x1[l1]   = x_r + temp0 * co2pi5 + temp4 * co4pi5;
          x1[l2]   = temp2 * si2pi5 + temp6 * si4pi5;
          x1[l3]   = x_r + temp4 * co2pi5 + temp0 * co4pi5;
          x1[l4]   = temp6 * si2pi5 - temp2 * si4pi5;

          j  += 8;
          x0 += 2;
          predj = (j - fft_jmp);
          if (!predj) x0 += fft_jmp;
          if (!predj) j = 0;
      }

      x0 = ptr_x + 1;

      _nassert((int)(N) % 10 == 0);
      _nassert(N >= 10);

      for (k = 0; k < N/5; k++) {

          /*-------------------------------------------------------------*/
          /*  Radix-5 butterfly loop part 1 imag-part                    */
          /*-------------------------------------------------------------*/
          x_i    = x0[0];
          x_l1_i = x0[l1];
          x_l2_i = x0[l2];
          x_l3_i = x0[l3];
          x_l4_i = x0[l4];

          x1 = (double*)_mvd((int)x0);

          temp1 = x_l1_i + x_l4_i;
          temp3 = x_l1_i - x_l4_i;
          temp5 = x_l2_i + x_l3_i;
          temp7 = x_l2_i - x_l3_i;

          x1[0]  = x_i + temp1 + temp5;
          x1[l1] = x_i + temp1 * co2pi5 + temp5 * co4pi5;
          x1[l2] = temp3 * si2pi5 + temp7 * si4pi5;
          x1[l3] = x_i + temp5 * co2pi5 + temp1 * co4pi5;
          x1[l4] = temp7 * si2pi5 - temp3 * si4pi5;

          j  += 8;
          x0 += 2;
          predj = (j - fft_jmp);
          if (!predj) x0 += fft_jmp;
          if (!predj) j = 0;
      }

      x0 = ptr_x;
      w  = tw1 + tw_offset;
      tw_offset += fft_jmp;

      _nassert((int)(w) % 8 == 0);
      _nassert((int)(x0) % 8 == 0);
      _nassert((int)(N) % 10 == 0);
      _nassert(N >= 10);

      for (k = 0; k < N/5; k++) {

          /*-------------------------------------------------------------*/
          /*  Radix-5 butterfly loop part 2                              */
          /*-------------------------------------------------------------*/
          t0 = x0[l1];
          t1 = x0[l1+1];
          t2 = x0[l2];
          t3 = x0[l2+1];
          t4 = x0[l3];
          t5 = x0[l3+1];
          t6 = x0[l4];
          t7 = x0[l4+1];

          co10 = w[j];    /* get twiddle factors */
          si10 = w[j+1];
          co20 = w[j+2];
          si20 = w[j+3];
          co30 = w[j+4];
          si30 = w[j+5];
          co40 = w[j+6];
          si40 = w[j+7];

          x1 = (double*)_mvd((int)x0);

          temp0 = t0 + t3;
          temp1 = t1 - t2;
          temp2 = t4 - t7;
          temp3 = t5 + t6;
          temp4 = t4 + t7;
          temp5 = t5 - t6;
          temp6 = t0 - t3;
          temp7 = t1 + t2;

          x1[l1]   = co10*temp0 + si10*temp1;
          x1[l1+1] = co10*temp1 - si10*temp0;
          x1[l2]   = co20*temp2 + si20*temp3;
          x1[l2+1] = co20*temp3 - si20*temp2;
          x1[l3]   = co30*temp4 + si30*temp5;
          x1[l3+1] = co30*temp5 - si30*temp4;
          x1[l4]   = co40*temp6 + si40*temp7;
          x1[l4+1] = co40*temp7 - si40*temp6;

          j  += 8;
          x0 += 2;
          predj = (j - fft_jmp);
          if (!predj) x0 += fft_jmp;
          if (!predj) j = 0;
      }
      stride /= 5;
  }

  x1 = ptr_x;
  y1 = ptr_y;
  for (i = 0; i < 2*N; i += N_p2*2) {
    /* radix-2 loop */
    DSPF_dp_fftDPxDP(N_p2, (double*)&x1[i], ptr_w, (double*)&y1[i], rad, 0, N_p2);
  }

  /* radix-3,5 bit reversal */
  x1 = ptr_x;
  n2 = 30 - _norm(N_p2);
  n3 = N>>n2;

  for (i = 0, l = 0; i < 2*N; i+= 2*N_p2, l++) {
    k = l;
    k_rev = 0;
    for (m = 0; m < s_r5; m++) {
      dum = k/5;
      k_rev = k_rev*5 + k - dum*5;
      k = dum;
    }
    for (m = 0; m < s_r3; m++) {
      dum = k/3;
      k_rev = k_rev*3 + k - dum*3;
      k = dum;
    }
    y1 = ptr_y + i;

    _nassert((int)(N_p2) % 4 == 0);
    _nassert(N_p2 >= 4);
    for (j = 0; j < N_p2; j++)
    {
      x1[(j*n3 + k_rev)*2]   = y1[j*2];
      x1[(j*n3 + k_rev)*2+1] = y1[j*2+1];
    }
  }

  /* copy x to y */
  x1 = ptr_x;
  y1 = ptr_y;
  for (i = 0; i < 2*N; i++) {
    y1[i] = x1[i];
  }
} /* fft_DPxDP_mixed_radix_c2c */

void fft_DPxDP_bs (
    int     N,          /* signal size           */
    int     M,          /* FFT size              */
    int     rad,        /* FFT radix             */
    double *in,         /* input pointer         */
    double *out,        /* output  pointer       */
    double *tw,         /* twiddle and phase factor pointer  */
    double *Bn,         /* pointer to FFT of bn  */
    double *workbuf     /* work buffer pointer   */
    )
{
    int   i;
    double *restrict Bk  = tw + 2*M;           /* phase factor pointer */
    double *restrict twi = tw;                 /* twiddle for ifft */
    double *restrict An, *restrict output;

    /* Construct and zero pad the signal to the right size */
    _nassert(N >= 8);

    /* Bluestein's FFT for fft size not power of 2 */

    /* ------------------------------------------------------------------------
     * initialize the work buffer to zeros, the workbuffer is 4*M words long,
     * but only need to initialize the part without signals
     * ---------------------------------------------------------------------- */
    for (i = 0; i<2*(M-N); i+=1)
    {
      workbuf[i+2*N] = 0;
    }

    /*---------------------------------------------------------------
     *  construct the input signal with phase factor An = in*Bk*.
     *  Note, Bk* = Re(Bk) - i*im(Bk)
     * --------------------------------------------------------------*/
    An = workbuf;
    output = workbuf+2*M;

#pragma MUST_ITERATE(2,,)

    for(i = 0; i < 2*N; i+=2)
    {
	  An[i]   = in[i]*Bk[i]   + in[i+1]*Bk[i+1];
	  An[i+1] = in[i+1]*Bk[i] - in[i]*Bk[i+1];
    }

    /* FFT of the constructed signal An */

    DSPF_dp_fftDPxDP (M, An, tw, output, rad, 0, M);

    /* IFFT of An*Bn */
    /* Bn is pre-calculated */
    output = workbuf;
    An = workbuf+2*M;

    for(i = 0; i < M; i++)
    {
	  output[2*i]   = An[2*i]*Bn[2*i] - An[2*i+1]*Bn[2*i+1];
	  output[2*i+1] = An[2*i+1]*Bn[2*i] + An[2*i]*Bn[2*i+1];
    }

    DSPF_dp_ifftDPxDP (M, output, twi, An, rad, 0, M);

    /* multiply with bk* which is the conjugate of bk */

    for(i = 0; i < N; i++)
    {
	  out[2*i]   = (An[2*i]*Bk[2*i]   + An[2*i+1]*Bk[2*i+1])/M;
	  out[2*i+1] = (An[2*i+1]*Bk[2*i] - An[2*i]*Bk[2*i+1])/M;
    }
} /* fft_DPxDP_bs */

void DSPF_dp_fftDPxDP_r2c (int N, double *restrict ptr_x, double *restrict ptr_w,
                       double *restrict ptr_y, int n_min, int offset, int n_max)
{

    int i, j, k, l1, l2, h2, predj, l0, radix;
    int tw_offset, stride, fft_jmp;

    double x0, x1, x2, x3, x4, x5, x6, x7;
    double xt0, yt0, xt1, yt1, xt2, yt2, yt3;
    double yt4, yt5, yt6, yt7;
    double si1, si2, si3, co1, co2, co3;
    double xh0, xh1, xh20, xh21, xl0, xl1, xl20, xl21;
    double x_0, x_1, x_l1, x_l1p1, x_h2, x_h2p1, x_l2, x_l2p1;
    double xl0_0, xl1_0, xl0_1, xl1_1;
    double xh0_0, xh1_0, xh0_1, xh1_1;
    double yr0, yi0, yr1, yi1, co, si, temp1, temp2, temp3 ,temp4, temp5, temp6;
    double *restrict x, *restrict w, *restrict w_base;
    double *restrict y0, *restrict y1, *restrict y2, *restrict y3, *restrict ptr_x0, *restrict ptr_x2;

    radix     = n_min;
    stride    = N/2;    /* n is the number of real samples */
    tw_offset = 0;
    w_base    = ptr_w + N/2;

    _nassert(stride > 4);
    while (stride > radix) {
        fft_jmp = stride + (stride >> 1);
        j  = 0;
        h2 = stride >> 1;
        l1 = stride;
        l2 = stride + (stride >> 1);
        x  = ptr_x;
        w  = w_base + tw_offset;

        _nassert((int)(w) % 8 == 0);
        _nassert((int)(x) % 8 == 0);
        _nassert(N >= 16);
        for (i = 0; i < N/2; i += 4) {
            co1 = w[j];
            si1 = w[j + 1];
            co2 = w[j + 2];
            si2 = w[j + 3];
            co3 = w[j + 4];
            si3 = w[j + 5];

            x_0    = x[0];
            x_1    = x[1];
            x_h2   = x[h2];
            x_h2p1 = x[h2 + 1];
            x_l1   = x[l1];
            x_l1p1 = x[l1 + 1];
            x_l2   = x[l2];
            x_l2p1 = x[l2 + 1];

            xh0  = x_0 + x_l1;
            xh1  = x_1 + x_l1p1;
            xl0  = x_0 - x_l1;
            xl1  = x_1 - x_l1p1;

            xh20 = x_l2   + x_h2;
            xh21 = x_l2p1 + x_h2p1;
            xl20 = x_h2   - x_l2;
            xl21 = x_h2p1 - x_l2p1;

            ptr_x2 = (double*)_mvd((int)x);
            ptr_x2[0] = xh0 + xh20;
            ptr_x2[1] = xh1 + xh21;

            x += 2;
            j += 6;
            predj = (j - fft_jmp);
            if (!predj) {
                x += fft_jmp;
                j  = 0;
            }

            xt0 = xh0 - xh20;
            yt0 = xh1 - xh21;
            xt1 = xl0 + xl21;
            yt2 = xl1 + xl20;
            xt2 = xl0 - xl21;
            yt1 = xl1 - xl20;

            ptr_x2[l1    ] = xt1 * co1 + yt1 * si1;
            ptr_x2[l1 + 1] = yt1 * co1 - xt1 * si1;
            ptr_x2[h2    ] = xt0 * co2 + yt0 * si2;
            ptr_x2[h2 + 1] = yt0 * co2 - xt0 * si2;
            ptr_x2[l2    ] = xt2 * co3 + yt2 * si3;
            ptr_x2[l2 + 1] = yt2 * co3 - xt2 * si3;
        }
        tw_offset += fft_jmp;
        stride = stride >> 2;
    } /* end while */

    j = offset >> 2;
    ptr_x0 = ptr_x;
    l0 = _norm(n_max >> 1) + 3;
    y0 = ptr_y;
    y2 = ptr_y + (n_max >> 1);
    y1 = y0 + (n_max >> 2);
    y3 = y2 + (n_max >> 2);

    if (radix == 4) {
        for (i = 0; i < N/2; i += 4) {
            k = _bitr(j) >> l0;
            j++;

            x0 = ptr_x0[0];
            x1 = ptr_x0[1];
            x2 = ptr_x0[2];
            x3 = ptr_x0[3];
            x4 = ptr_x0[4];
            x5 = ptr_x0[5];
            x6 = ptr_x0[6];
            x7 = ptr_x0[7];
            ptr_x0 += 8;

            xh0_0 = x0 + x4;
            xh1_0 = x1 + x5;
            xh0_1 = x2 + x6;
            xh1_1 = x3 + x7;

            yt0 = xh0_0 + xh0_1;
            yt1 = xh1_0 + xh1_1;
            yt4 = xh0_0 - xh0_1;
            yt5 = xh1_0 - xh1_1;

            xl0_0 = x0 - x4;
            xl1_0 = x1 - x5;
            xl0_1 = x2 - x6;
            xl1_1 = x3 - x7;

            yt2 = xl0_0 + xl1_1;
            yt3 = xl1_0 - xl0_1;
            yt6 = xl0_0 - xl1_1;
            yt7 = xl1_0 + xl0_1;

            y0[k    ] = yt0;
            y0[k + 1] = yt1;
            y1[k    ] = yt2;
            y1[k + 1] = yt3;
            y2[k    ] = yt4;
            y2[k + 1] = yt5;
            y3[k    ] = yt6;
            y3[k + 1] = yt7;
        }
    }
    else if (radix == 2) {
        for (i = 0; i < N/2; i += 4) {
            k = _bitr(j) >> l0;
            j++;

            xh0_0 = ptr_x0[0];
            xh1_0 = ptr_x0[1];
            xh0_1 = ptr_x0[2];
            xh1_1 = ptr_x0[3];
            xl0_0 = ptr_x0[4];
            xl1_0 = ptr_x0[5];
            xl1_1 = ptr_x0[6];
            xl0_1 = ptr_x0[7];
            ptr_x0 += 8;

            yt0 = xh0_0 + xh0_1;
            yt1 = xh1_0 + xh1_1;
            yt4 = xh0_0 - xh0_1;
            yt5 = xh1_0 - xh1_1;

            yt2 = xl0_0 + xl1_1;
            yt6 = xl0_0 - xl1_1;
            yt7 = xl1_0 - xl0_1;
            yt3 = xl1_0 + xl0_1;

            y0[k    ] = yt0;
            y0[k + 1] = yt1;
            y1[k    ] = yt2;
            y1[k + 1] = yt3;
            y2[k    ] = yt4;
            y2[k + 1] = yt5;
            y3[k    ] = yt6;
            y3[k + 1] = yt7;
        }
    }

    yr0 = ptr_y[0];
    yi0 = ptr_y[1];
    yr1 = ptr_y[N/2  ];
    yi1 = ptr_y[N/2+1];

    ptr_y[N    ] = yr0 - yi0;
    ptr_y[N + 1] = 0;
    ptr_y[0    ] = yr0 + yi0;
    ptr_y[1    ] = 0;
    ptr_y[N+N/2  ] =  yr1;
    ptr_y[N+N/2+1] =  yi1;
    ptr_y[  N/2+1] = -yi1;

    y0 = ptr_y;
    y1 = ptr_y + N;
    y2 = ptr_y + N;
    y3 = ptr_y + 2*N;
    w  = ptr_w - 2;

    for (i = 2; i < N/2; i+=2) {

        k   = -i;
        yr0 = y0[i];
        yi0 = y0[i+1];
        yr1 = y1[k];
        yi1 = y1[k+1];

        si  = w[i];
        co  = w[i+1];

        temp5 = yr1 - yr0;
        temp6 = yi0 + yi1;
        temp2 = si * temp5 + co * temp6;
        temp3 = co * temp5 - si * temp6;
        temp1 = (yr0 + yr1) / 2;
        temp4 = (yi0 - yi1) / 2;

        y0[i]   = temp1 + temp2;
        y0[i+1] = temp3 + temp4;
        y1[k]   = temp1 - temp2;
        y1[k+1] = temp3 - temp4;
        y2[i]   = temp1 - temp2;
        y2[i+1] = temp4 - temp3;
        y3[k]   = temp1 + temp2;
        y3[k+1] =-temp3 - temp4;
    }
} /* DSPF_dp_fftDPxDP_r2c */


void fft_DPxDP_mixed_radix_r2c (int N, double *ptr_x, double *ptr_w, double *ptr_y,
                                int N_p2, int s_r3, int s_r5, int rad)
{
  int i, j, k, l, m, dum, k_rev, l1, l2, l3, l4, predj, tw_offset, stride, fft_jmp, n2, n3;
  double *restrict tw1 = ptr_w + N/2 + N_p2*2;
  double *restrict tw2 = ptr_w + N/2;
  double *restrict y0, *restrict y1, *restrict y2, *restrict y3;
  double *restrict x0, *restrict x1, *restrict w;
  double yr0, yi0, yr1, yi1, co, si;
  double temp0, temp1, temp2, temp3, temp4, temp5, temp6, temp7, temp8, temp9, tempa, tempb;
  double t0, t1, t2, t3, t4, t5, t6, t7;
  double x_r, x_i, x_l1_r, x_l1_i, x_l2_r, x_l2_i, x_l3_r, x_l3_i, x_l4_r, x_l4_i;
  double co10, si10, co20, si20, co30, si30, co40, si40;
  double co2pi3 =-0.50000000000000,   /* cos(2*pi/3) */
         si2pi3 = 0.86602540378444,   /* sin(2*pi/3) */
         co2pi5 = 0.30901699437495,   /* cos(2*pi/5) */
         si2pi5 = 0.95105651629515,   /* sin(2*pi/5) */
         co4pi5 =-0.80901699437495,   /* cos(4*pi/5) */
         si4pi5 = 0.58778525229247;   /* sin(4*pi/5) */

  stride    = N;
  tw_offset = 0;
  fft_jmp   = 2*N;

  /* radix-3 loop */
  for (i = 0; i < s_r3; i ++) {

      j = 0;
      fft_jmp /= 3;

      l1 = stride/3;
      l2 = l1*2;

      x0 = ptr_x;
      w  = tw1 + tw_offset;
      tw_offset += fft_jmp;

      _nassert((int)(w) % 8 == 0);
      _nassert((int)(x0) % 8 == 0);
      _nassert((int)(N) % 12 == 0);
      _nassert(N >= 12);

      for (k = 0; k < N/6; k++) {

          /*-------------------------------------------------------------*/
          /*  Radix-3 loop                                               */
          /*-------------------------------------------------------------*/
          co10 = w[j];    /* get twiddle factors */
          si10 = w[j+1];
          co20 = w[j+2];
          si20 = w[j+3];

          x_r    = x0[0];
          x_i    = x0[1];
          x_l1_r = x0[l1];
          x_l1_i = x0[l1+1];
          x_l2_r = x0[l2];
          x_l2_i = x0[l2+1];

          x1 = (double*)_mvd((int)x0);

          /* internal DFT computation */
          temp0 = x_l1_r + x_l2_r;
          temp1 = x_l1_i + x_l2_i;
          temp2 = x_l1_r - x_l2_r;
          temp3 = x_l1_i - x_l2_i;

          temp4 = temp0 * co2pi3;
          temp5 = temp1 * co2pi3;
          temp6 = temp2 * si2pi3;
          temp7 = temp3 * si2pi3;

          temp8 = x_r + temp4 + temp7;
          temp9 = x_i + temp5 - temp6;
          tempa = x_r + temp4 - temp7;
          tempb = x_i + temp5 + temp6;

          x1[0]    = x_r + temp0;
          x1[1]    = x_i + temp1;
          x1[l1]   = co10*temp8 + si10*temp9;
          x1[l1+1] = co10*temp9 - si10*temp8;
          x1[l2]   = co20*tempa + si20*tempb;
          x1[l2+1] = co20*tempb - si20*tempa;

          j  += 4;
          x0 += 2;
          predj = (j - fft_jmp);
          if (!predj) x0 += fft_jmp;
          if (!predj) j = 0;
      }
      stride /= 3;
  }

  /* radix-5 loop */
  fft_jmp *= 2; /* radix-5 has 8 twiddle vs radix-3 4 twiddles */
  for (i = 0; i < s_r5; i ++) {

      j = 0;
      fft_jmp /= 5;

      l1 = stride/5;
      l2 = l1*2;
      l3 = l1*3;
      l4 = l1*4;

      x0 = ptr_x;

      _nassert((int)(N) % 20 == 0);
      _nassert(N >= 20);

      for (k = 0; k < N/10; k++) {

          /*-------------------------------------------------------------*/
          /*  Radix-5 butterfly loop part 1 real-part                    */
          /*-------------------------------------------------------------*/
          x_r    = x0[0];
          x_l1_r = x0[l1];
          x_l2_r = x0[l2];
          x_l3_r = x0[l3];
          x_l4_r = x0[l4];

          x1 = (double*)_mvd((int)x0);

          temp0 = x_l1_r + x_l4_r;
          temp2 = x_l1_r - x_l4_r;
          temp4 = x_l2_r + x_l3_r;
          temp6 = x_l2_r - x_l3_r;

          x1[0]    = x_r + temp0 + temp4;
          x1[l1]   = x_r + temp0 * co2pi5 + temp4 * co4pi5;
          x1[l2]   = temp2 * si2pi5 + temp6 * si4pi5;
          x1[l3]   = x_r + temp4 * co2pi5 + temp0 * co4pi5;
          x1[l4]   = temp6 * si2pi5 - temp2 * si4pi5;

          j  += 8;
          x0 += 2;
          predj = (j - fft_jmp);
          if (!predj) x0 += fft_jmp;
          if (!predj) j = 0;
      }

      x0 = ptr_x + 1;

      _nassert((int)(N) % 20 == 0);
      _nassert(N >= 20);

      for (k = 0; k < N/10; k++) {

          /*-------------------------------------------------------------*/
          /*  Radix-5 butterfly loop part 1 imag-part                    */
          /*-------------------------------------------------------------*/
          x_i    = x0[0];
          x_l1_i = x0[l1];
          x_l2_i = x0[l2];
          x_l3_i = x0[l3];
          x_l4_i = x0[l4];

          x1 = (double*)_mvd((int)x0);

          temp1 = x_l1_i + x_l4_i;
          temp3 = x_l1_i - x_l4_i;
          temp5 = x_l2_i + x_l3_i;
          temp7 = x_l2_i - x_l3_i;

          x1[0]  = x_i + temp1 + temp5;
          x1[l1] = x_i + temp1 * co2pi5 + temp5 * co4pi5;
          x1[l2] = temp3 * si2pi5 + temp7 * si4pi5;
          x1[l3] = x_i + temp5 * co2pi5 + temp1 * co4pi5;
          x1[l4] = temp7 * si2pi5 - temp3 * si4pi5;

          j  += 8;
          x0 += 2;
          predj = (j - fft_jmp);
          if (!predj) x0 += fft_jmp;
          if (!predj) j = 0;
      }

      x0 = ptr_x;
      w  = tw1 + tw_offset;
      tw_offset += fft_jmp;

      _nassert((int)(w) % 8 == 0);
      _nassert((int)(x0) % 8 == 0);
      _nassert((int)(N) % 20 == 0);
      _nassert(N >= 20);

      for (k = 0; k < N/10; k++) {

          /*-------------------------------------------------------------*/
          /*  Radix-5 butterfly loop part 2                              */
          /*-------------------------------------------------------------*/
          t0 = x0[l1];
          t1 = x0[l1+1];
          t2 = x0[l2];
          t3 = x0[l2+1];
          t4 = x0[l3];
          t5 = x0[l3+1];
          t6 = x0[l4];
          t7 = x0[l4+1];

          co10 = w[j];    /* get twiddle factors */
          si10 = w[j+1];
          co20 = w[j+2];
          si20 = w[j+3];
          co30 = w[j+4];
          si30 = w[j+5];
          co40 = w[j+6];
          si40 = w[j+7];

          x1 = (double*)_mvd((int)x0);

          temp0 = t0 + t3;
          temp1 = t1 - t2;
          temp2 = t4 - t7;
          temp3 = t5 + t6;
          temp4 = t4 + t7;
          temp5 = t5 - t6;
          temp6 = t0 - t3;
          temp7 = t1 + t2;

          x1[l1]   = co10*temp0 + si10*temp1;
          x1[l1+1] = co10*temp1 - si10*temp0;
          x1[l2]   = co20*temp2 + si20*temp3;
          x1[l2+1] = co20*temp3 - si20*temp2;
          x1[l3]   = co30*temp4 + si30*temp5;
          x1[l3+1] = co30*temp5 - si30*temp4;
          x1[l4]   = co40*temp6 + si40*temp7;
          x1[l4+1] = co40*temp7 - si40*temp6;

          j  += 8;
          x0 += 2;
          predj = (j - fft_jmp);
          if (!predj) x0 += fft_jmp;
          if (!predj) j = 0;
      }
      stride /= 5;
  }

  x1 = ptr_x;
  y1 = ptr_y;
  for (i = 0; i < N; i += N_p2*2) {
    /* radix-2 loop */
    DSPF_dp_fftDPxDP(N_p2, (double*)&x1[i], tw2, (double*)&y1[i], rad, 0, N_p2);
  }

  /* radix-3 bit reversal */
  x1 = ptr_x;
  n2 = 30 - _norm(N_p2);
  n3 = N>>(n2+1);

  for (i = 0, l = 0; i < N; i+= 2*N_p2, l++) {
    k = l;
    k_rev = 0;
    for (m = 0; m < s_r5; m++) {
      dum = k/5;
      k_rev = k_rev*5 + k - dum*5;
      k = dum;
    }
    for (m = 0; m < s_r3; m++) {
      dum = k/3;
      k_rev = k_rev*3 + k - dum*3;
      k = dum;
    }
    y1 = ptr_y + i;

    _nassert((int)(N_p2) % 4 == 0);
    _nassert(N_p2 >= 4);
    for (j = 0; j < N_p2; j++)
    {
      x1[(j*n3 + k_rev)*2]   = y1[j*2];
      x1[(j*n3 + k_rev)*2+1] = y1[j*2+1];
    }
  }

  /* split factors */
  yr0 = ptr_x[0];
  yi0 = ptr_x[1];
  yr1 = ptr_x[N/2  ];
  yi1 = ptr_x[N/2+1];

  ptr_y[N    ]   = yr0 - yi0;
  ptr_y[N + 1]   = 0;
  ptr_y[0    ]   = yr0 + yi0;
  ptr_y[1    ]   = 0;
  ptr_y[N+N/2  ] = yr1;
  ptr_y[N+N/2+1] = yi1;
  ptr_y[  N/2  ] = yr1;
  ptr_y[  N/2+1] =-yi1;

  x0 = ptr_x;
  x1 = ptr_x + N;
  y0 = ptr_y;
  y1 = ptr_y + N;
  y2 = ptr_y + N;
  y3 = ptr_y + 2*N;
  w  = ptr_w - 2;

  for (i = 2; i < N/2; i+=2) {

      k   = -i;
      yr0 = x0[i];
      yi0 = x0[i+1];
      yr1 = x1[k];
      yi1 = x1[k+1];

      si  = w[i];
      co  = w[i+1];

      temp5 = yr1 - yr0;
      temp6 = yi0 + yi1;
      temp2 = si * temp5 + co * temp6;
      temp3 = co * temp5 - si * temp6;
      temp1 = (yr0 + yr1) / 2;
      temp4 = (yi0 - yi1) / 2;

      y0[i]   = temp1 + temp2;
      y0[i+1] = temp3 + temp4;
      y1[k]   = temp1 - temp2;
      y1[k+1] = temp3 - temp4;
      y2[i]   = temp1 - temp2;
      y2[i+1] = temp4 - temp3;
      y3[k]   = temp1 + temp2;
      y3[k+1] =-temp3 - temp4;
  }
} /* fft_DPxDP_mixed_radix_r2c */

void fft_DPxDP_r2c_bs (
    int     N,          /* signal size           */
    int     M,          /* FFT size              */
    int     rad,        /* FFT radix             */
    double *in,         /* input pointer         */
    double *out,        /* output  pointer       */
    double *tw,         /* twiddle and phase factor pointer  */
    double *Bn,         /* pointer to FFT of bn  */
    double *workbuf     /* work buffer pointer   */
    )
{
    int   i;
    double *restrict Bk  = tw + 2*M;           /* phase factor pointer */
    double *restrict twi = tw;                 /* twiddle for ifft */
    double *restrict An, *restrict output;

    /* Construct and zero pad the signal to the right size */
    _nassert(N >= 8);

    /* Bluestein's FFT for fft size not power of 2 */

    /* ------------------------------------------------------------------------
     * initialize the work buffer to zeros, the workbuffer is 4*M words long,
     * but only need to initialize the part without signals
     * ---------------------------------------------------------------------- */

    for (i = 0; i<2*(M-N); i+=1)
    {
      workbuf[i+2*N] = 0;
    }

    /*---------------------------------------------------------------
     *  construct the input signal with phase factor An = in*Bk*.
     *  Note, Bk* = Re(Bk) - i*im(Bk)
     * --------------------------------------------------------------*/
    An = workbuf;
    output = workbuf+2*M;

#pragma MUST_ITERATE(2,,)

    for(i = 0; i < N; i++)
    {
	  An[2*i]   =   in[i]*Bk[2*i];
	  An[2*i+1] = - in[i]*Bk[2*i+1];
    }

    /* FFT of the constructed signal An */

    DSPF_dp_fftDPxDP (M, An, tw, output, rad, 0, M);

    /* IFFT of An*Bn */
    /* Bn is pre-calculated */
    output = workbuf;
    An = workbuf+2*M;

    for(i = 0; i < M; i++)
    {
	  output[2*i]   = An[2*i]*Bn[2*i] - An[2*i+1]*Bn[2*i+1];
	  output[2*i+1] = An[2*i+1]*Bn[2*i] + An[2*i]*Bn[2*i+1];
    }

    DSPF_dp_ifftDPxDP (M, output, twi, An, rad, 0, M);

    /* multiply with bk* which is the conjugate of bk */

    for(i = 0; i < N; i++)
    {
	  out[2*i]   = An[2*i]*Bk[2*i]   + An[2*i+1]*Bk[2*i+1];
	  out[2*i+1] = An[2*i+1]*Bk[2*i] - An[2*i]*Bk[2*i+1];
    }
} /* fft_DPxDP_r2c_bs */

/* ======================================================================== */
/*  End of file:  fft_dp_mixed_bs.c                                         */
/* ------------------------------------------------------------------------ */
/*            Copyright (c) 2013 Texas Instruments, Incorporated.           */
/*                           All Rights Reserved.                           */
/* ======================================================================== */
