/* ======================================================================= */
/*  TEXAS INSTRUMENTS, INC.                                                */
/*                                                                         */
/*  FFTLIB  FFT Library                                                    */
/*                                                                         */
/* Copyright (C) 2013 Texas Instruments Incorporated - http://www.ti.com/  */ 
/*                                                                         */
/*                                                                         */
/*  Redistribution and use in source and binary forms, with or without     */
/*  modification, are permitted provided that the following conditions     */
/*  are met:                                                               */
/*                                                                         */
/*    Redistributions of source code must retain the above copyright       */
/*    notice, this list of conditions and the following disclaimer.        */
/*                                                                         */
/*    Redistributions in binary form must reproduce the above copyright    */
/*    notice, this list of conditions and the following disclaimer in the  */
/*    documentation and/or other materials provided with the               */
/*    distribution.                                                        */
/*                                                                         */
/*    Neither the name of Texas Instruments Incorporated nor the names of  */
/*    its contributors may be used to endorse or promote products derived  */
/*    from this software without specific prior written permission.        */
/*                                                                         */
/*  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS    */
/*  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT      */
/*  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR  */
/*  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT   */
/*  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,  */
/*  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT       */
/*  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,  */
/*  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY  */
/*  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT    */
/*  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE  */
/*  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.   */
/*                                                                         */
/* ======================================================================= */

#include <xdc/std.h>
#include "fft_common.h"
#include <ti/libarch/libarch.h>

/* Size requirement of each of the 4 memory types */
#define FFT_MEM_SIZE_VFAST (  0*1024UL) 

#ifdef FFT_MEM_MODEL_LG
#define FFT_MEM_SIZE_FAST  (768*1024UL)
#else
#ifdef FFT_MEM_MODEL_SM
#define FFT_MEM_SIZE_FAST  (128*1024UL)
#else
//#ifdef FFT_MEM_MODEL_MED
#define FFT_MEM_SIZE_FAST  (264*1024UL)
//#endif
#endif
#endif

#define FFT_MEM_SIZE_MED   (  0*1024UL)
#define FFT_MEM_SIZE_SLOW  (  0*1024UL)

/* Define memory descriptors for memory management */
lib_memdscr_t fft_vfast_buf;
lib_memdscr_t fft_fast_buf;
lib_memdscr_t fft_med_buf;
lib_memdscr_t fft_slow_buf;

/* Define a memory descriptor array */
lib_memdscr_t * fft_memdscr_tab[LIB_MEMTYPE_N] = {
    &fft_vfast_buf,
    &fft_fast_buf,
    &fft_med_buf,
    &fft_slow_buf
};

/*==============================================================================
 * This function returns the address of the memory descriptor array
 *============================================================================*/
void * fftGetMemHandle()
{
    return((void *)&fft_memdscr_tab[0]);
} /* testGetMemHandle */

/*==============================================================================
 * This function returns the size requirement of each of the 4 memory types
 *============================================================================*/
void fft_GetSizes(size_t *smem_size_vfast, size_t *smem_size_fast, 
                  size_t *smem_size_med,   size_t *smem_size_slow)
{
    *smem_size_vfast = FFT_MEM_SIZE_VFAST;  // very fast scratch memory
    *smem_size_fast  = FFT_MEM_SIZE_FAST;   // fast scratch memory
    *smem_size_med   = FFT_MEM_SIZE_MED;    // medium speed scratch memory
    *smem_size_slow  = FFT_MEM_SIZE_SLOW;   // slow scratch memory
} /* FFT_GetSizes */

/*==============================================================================
 *   It performs necessary initialization through library framework API in order
 *   to do memory allocations.
 *============================================================================*/
int fft_Init(void * sfast_buf_base,  size_t sfast_buf_size)
{
    lib_memdscr_t **fft_mem_handle = fftGetMemHandle();

    /* Verify supplied memories meet requirements */
    if((sfast_buf_base !=NULL) && (sfast_buf_size >=FFT_MEM_SIZE_FAST)) {
        lib_smem_finit(fft_mem_handle, sfast_buf_base,  sfast_buf_size);

        return(FFT_INIT_NOERR);
    }
    else {
        return(FFT_INIT_ERROR);
    }
} /* fft_Init */

int fft_execute (
    fft_plan_t p
)
{
    int      error = FFT_NOERR;
    uint32_t *temp;
    lib_memdscr_t **fft_mem_handle = fftGetMemHandle();

    lib_smem_freset (fft_mem_handle);
    if ( p.fftcout.ecpyRequest ) {
//      p.edmaState = (*p.fftcout.ecpyRequest)(p.local, FFT_MAX_EDMA_LINKS_3D*FFT_NUM_EDMA_CH*sizeof(uint32_t));
      temp = (uint32_t *)lib_smem_falloc(fft_mem_handle, 256*6*sizeof(uint32_t), 3);
#if 1
      if(temp==NULL) {
          printf("Memory allocation error!\n");
          return FFT_ERROR;
      }
#endif
      p.edmaState = (*p.fftcout.ecpyRequest)(temp, 256*6*sizeof(uint32_t));
    }
    error = (*p.fftfxn) (p.u, p.edmaState, p.actualCoreNum);

    if ( p.fftcout.ecpyRelease ) {
      (*p.fftcout.ecpyRelease)(p.edmaState);
    }
    return error;
} /* FFT_execute */

/* FFT destroy plan APT */
int fft_destroy_plan (fft_plan_t p)
{
  int error = FFT_NOERR;

  /* release memory */
  //(*p.fftcout.memoryRelease) (p.nbuf, p.bufs);

  return error;
}

/* ======================================================================== */
/*  End of file:  fft_common.c                                              */
/* ------------------------------------------------------------------------ */
/*            Copyright (c) 2013 Texas Instruments, Incorporated.           */
/*                           All Rights Reserved.                           */
/* ======================================================================== */
