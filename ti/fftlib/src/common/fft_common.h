/* ======================================================================= */
/*  TEXAS INSTRUMENTS, INC.                                                */
/*                                                                         */
/*  FFTLIB  FFT Library                                                    */
/*                                                                         */
/* Copyright (C) 2013 Texas Instruments Incorporated - http://www.ti.com/  */ 
/*                                                                         */
/*                                                                         */
/*  Redistribution and use in source and binary forms, with or without     */
/*  modification, are permitted provided that the following conditions     */
/*  are met:                                                               */
/*                                                                         */
/*    Redistributions of source code must retain the above copyright       */
/*    notice, this list of conditions and the following disclaimer.        */
/*                                                                         */
/*    Redistributions in binary form must reproduce the above copyright    */
/*    notice, this list of conditions and the following disclaimer in the  */
/*    documentation and/or other materials provided with the               */
/*    distribution.                                                        */
/*                                                                         */
/*    Neither the name of Texas Instruments Incorporated nor the names of  */
/*    its contributors may be used to endorse or promote products derived  */
/*    from this software without specific prior written permission.        */
/*                                                                         */
/*  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS    */
/*  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT      */
/*  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR  */
/*  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT   */
/*  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,  */
/*  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT       */
/*  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,  */
/*  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY  */
/*  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT    */
/*  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE  */
/*  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.   */
/*                                                                         */
/* ======================================================================= */

#ifndef FFT_COMMON_H_
#define FFT_COMMON_H_ 1

/* FFT error codes */
enum {
  FFT_NOERR = 0,
  FFT_ERROR = 1
};  

/* FFT error codes */
enum {
  FFT_INIT_NOERR = 0,
  FFT_INIT_ERROR = 1
};  

/* FFT modes */
enum {
  FFT_DIRECT  = 0,
  FFT_ECPY    = 1
};  

 /* Memory classes */
#define FFT_MAX_NUM_BUFS  10
enum {
  FFT_MEM_CLASS_EXTERNAL = 0,   /**< generic (bulk) external memory */
  FFT_MEM_CLASS_INTERNAL = 1,   /**< generic (bulk) internal memory */
  FFT_MEM_CLASS_SHARED   = 2    /**< generic (bulk) shared memory   */
};
  
typedef struct {

  int   mclass;    /**< Memory class. It is used to describe kind of 
                       memory that is requested or returned. For 
                       example: external, internal, or similar. One 
                       should use FFT_CLASS_xxx constants. On RETURN, 
                       class may be changed if original memory class 
                       is exhausted. */
  int   log2align; /**< Alignment information (>= 0). If zero, no 
                       alignment is required nor provided. Otherwise, 
                       memory buffer is aligned on appropriate power 
                       of 2 (e.g. if log2align=3, buffer is aligned 
                       modulo-8). */
  int   size;      /**< Number of bytes that are requested or supplied. */
  void  *base;     /**< Base address of the requested buffer. */
} FFTmemBuffer_t;

typedef struct fft_plan_callout_t {
    void (*memoryRequest) (int nbufs, FFTmemBuffer_t *bufs);
    void *(*ecpyRequest)  (void *buf, int buf_size);

    void (*memoryRelease) (int nbufs, FFTmemBuffer_t *bufs);
    void (*ecpyRelease)   (void *edmaState);
} fft_callout_t;

/* parameters for mix radix and anysize support */
typedef struct {
  int         M;         /* actually FFT size, could be different from signal size when doing BS */
  int         twsize;    /* twiddle buffer size */
  int         Bnsize;    /* Bn buffer size when doing BS */
  int         worksize;  /* size of the workbuffer */
  int         rad;       /* 2, 3, 4, or 5*/
  int         N_p2;
  int         s_r3;
  int         s_r5;
  int         use_bs;    /* 0: no BS, 1: use BS */
} fft_para_mix_bs_t;


/* FFT parameters */
typedef union {
  struct {
    int   N;
    fft_para_mix_bs_t para1;    
    float *in;
    float *out;
    float *tw;
    float *Bn;
    float *workbuf;
  }sp_1d_c2c_d;

  struct {
    int   N;
    int   n1;
    int   n2;
    fft_para_mix_bs_t para1;
    fft_para_mix_bs_t para2;    
    float *data;
    float *work;
    float *data_w;
    float *work_w;
    float *work_t;
    float *tw;
    float *tw_w;
    float *Bn;
    float *Bn_w;
    float *workbuf;
  }sp_1d_c2c_e;

  struct {
    int   N;
    fft_para_mix_bs_t para1;    
    float *in;
    float *out;
    float *tw;
    float *Bn;
    float *workbuf;
  }sp_1d_r2c_d;

  struct {
    int   N;
    int   n1;
    int   n2;
    fft_para_mix_bs_t para1;
    fft_para_mix_bs_t para2;        
    float *data;
    float *work;
    float *data_w;
    float *work_w;
    float *work_t;
    float *tw;
    float *tw_w;
    float *Bn;
    float *Bn_w;
    float *workbuf;
  }sp_1d_r2c_e;

  struct {
    int   N;
    fft_para_mix_bs_t para1;    
    float *in;
    float *out;
    float *tw;
    float *Bn;
    float *workbuf;
  }sp_1d_c2r_d;

  struct {
    int   N;
    int   n1;
    int   n2;
    fft_para_mix_bs_t para1;
    fft_para_mix_bs_t para2;        
    float *data;
    float *work;
    float *data_w;
    float *work_w;
    float *work_t;
    float *tw;
    float *tw_w;
    float *Bn;
    float *Bn_w;
    float *workbuf;
  }sp_1d_c2r_e;

  struct {
    int     N;
    fft_para_mix_bs_t para1;
    double *in;
    double *out;
    double *tw;
    double *Bn;
    double *workbuf;
  }dp_1d_c2c_d;

  struct {
    int     N;
    int     n1;
    int     n2;  
    fft_para_mix_bs_t para1;
    fft_para_mix_bs_t para2;
    double *data;
    double *work;
    double *data_w;
    double *work_w;
    double *work_t;
    double *tw;
    double *tw_w;
    double *Bn;
    double *Bn_w;
    double *workbuf;
  }dp_1d_c2c_e;

  struct {
    int     N;
    fft_para_mix_bs_t para1;
    double *in;
    double *out;
    double *tw;
    double *Bn;
    double *workbuf;
  }dp_1d_r2c_d;

  struct {
    int     N;
    int     n1;
    int     n2;
    fft_para_mix_bs_t para1;
    fft_para_mix_bs_t para2;
    double *data;
    double *work;
    double *data_w;
    double *work_w;
    double *work_t;
    double *tw;
    double *tw_w;
    double *Bn;
    double *Bn_w;
    double *workbuf;
  }dp_1d_r2c_e;

  struct {
    int     N;
    fft_para_mix_bs_t para1;
    double *in;
    double *out;
    double *tw;
    double *Bn;
    double *workbuf;
  }dp_1d_c2r_d;

  struct {
    int     N;
    int     n1;
    int     n2;
    fft_para_mix_bs_t para1;
    fft_para_mix_bs_t para2;
    double *data;
    double *work;
    double *data_w;
    double *work_w;
    double *work_t;
    double *tw;
    double *tw_w;
    double *Bn;
    double *Bn_w;
    double *workbuf;
  }dp_1d_c2r_e;

  struct {
    int     N;
    int     batch;
    fft_para_mix_bs_t para1;
    float *in;
    float *out;
    float *tw;
    float *Bn;
  }sp_1d_r2c_batch_e;

  struct {
    int     N;
    int     batch;
    fft_para_mix_bs_t para1;
    float *in;
    float *out;
    float *tw;
    float *Bn;
  }sp_1d_c2r_batch_e;

  struct {
    int     N;
    int     batch;
    fft_para_mix_bs_t para1;
    float *in;
    float *out;
    float *tw;
    float *Bn;
  }sp_1d_c2c_batch_e;

    struct {
    int   N1;
    int   N2;
    fft_para_mix_bs_t para1;
    fft_para_mix_bs_t para2; 
    float *data;
    float *workbuf;
    float *data_w;
    float *workbuf_w;
    float *workbuf_t;
    float *Bn;
    float *Bn_w;
    float *workbuf_bn;
    float *tw;
    float *tw_w;
  }sp_2d_c2c_e;

    struct {
	int   N;
    int   N1;
    int   N2;
    fft_para_mix_bs_t para1;
    fft_para_mix_bs_t para2; 
    float *in;
    float *out;
    float *Bn;
    float *Bn_w;
    float *workbuf_bn;
    float *tw;
    float *tw_w;
  }sp_2d_c2r_e;

    struct {
	int   N;
    int   N1;
    int   N2;
    fft_para_mix_bs_t para1;
    fft_para_mix_bs_t para2;
    float *in;
    float *out;
    float *Bn;
    float *Bn_w;
    float *workbuf_bn;
    float *tw;
    float *tw_w;
  }sp_2d_r2c_e;

  struct {
    int   N;
    int   N1;
    int   N2;
    int   N3;
    fft_para_mix_bs_t para1;
    fft_para_mix_bs_t para2;
    fft_para_mix_bs_t para3;
    float *in;
    float *out;
    float *in_w;
    float *out_w;
    float *out_t;
    float *tw;
    float *tw_w;
    float *Bn;
    float *Bn_w;
    float *workbuf_bn;
  }sp_3d_c2c_e;

  struct {
    int   N;
    int   N1;
    int   N2;
    int   N3;
    fft_para_mix_bs_t para1;
    fft_para_mix_bs_t para2;
    fft_para_mix_bs_t para3;
    float *in;
    float *out;
    float *in_w;
    float *out_w;
    float *out_t;
    float *tw;
    float *tw_w;
    float *Bn;
    float *Bn_w;
    float *workbuf_bn;
  }sp_3d_c2r_e;

  struct {
    int   N;
    int   N1;
    int   N2;
    int   N3;
    fft_para_mix_bs_t para1;
    fft_para_mix_bs_t para2;
    fft_para_mix_bs_t para3;
    float *in;
    float *out;
    float *in_w;
    float *out_w;
    float *out_t;
    float *tw;
    float *tw_w;
    float *Bn;
    float *Bn_w;
    float *workbuf_bn;
  }sp_3d_r2c_e;

  struct {
    int     N;
    int     batch;
    fft_para_mix_bs_t para1;
    double *in;
    double *out;
    double *tw;
    double *Bn;
  }dp_1d_c2c_batch_e;

   struct {
    int     N;
    int     batch;
    fft_para_mix_bs_t para1;
    double *in;
    double *out;
    double *tw;
    double *Bn;
  }dp_1d_r2c_batch_e;

   struct {
    int     N;
    int     batch;
    fft_para_mix_bs_t para1;
    double *in;
    double *out;
    double *tw;
    double *Bn;
  }dp_1d_c2r_batch_e;

  struct {
    int    N1;
    int    N2;
    fft_para_mix_bs_t para1;
    fft_para_mix_bs_t para2; 
    double *data;
    double *workbuf;
    double *data_w;
    double *workbuf_w;
    double *workbuf_t;
    double *Bn;
    double *Bn_w;
    double *workbuf_bn;
    double *tw;
    double *tw_w;
  }dp_2d_c2c_e;

  struct {
    int    N1;
    int    N2;
    fft_para_mix_bs_t para1;
    fft_para_mix_bs_t para2;
    double *data;
    double *workbuf;
    double *data_w;
    double *workbuf_w;
    double *workbuf_t;
    double *Bn;
    double *Bn_w;
    double *workbuf_bn;
    double *tw;
    double *tw_w;
  }dp_2d_c2r_e;

  struct {
    int    N1;
    int    N2;
    fft_para_mix_bs_t para1;
    fft_para_mix_bs_t para2; 
    double *data;
    double *workbuf;
    double *data_w;
    double *workbuf_w;
    double *workbuf_t;
    double *Bn;
    double *Bn_w;
    double *workbuf_bn;
    double *tw;
    double *tw_w;
  }dp_2d_r2c_e;

  struct {
    int    N1;
    int    N2;
    int    N3;
    fft_para_mix_bs_t para1;
    fft_para_mix_bs_t para2;
    fft_para_mix_bs_t para3;
    double *in;
    double *out;
    double *in_w;
    double *out_w;
    double *out_t;
    double *tw;
    double *tw_w;
    double *Bn;
    double *Bn_w;
    double *workbuf;
  }dp_3d_c2c_e;

  struct {
    int    N1;
    int    N2;
    int    N3;
    fft_para_mix_bs_t para1;
    fft_para_mix_bs_t para2;
    fft_para_mix_bs_t para3;
    double *in;
    double *out;
    double *in_w;
    double *out_w;
    double *out_t;
    double *tw;
    double *tw_w;
    double *Bn;
    double *Bn_w;
    double *workbuf;
  }dp_3d_c2r_e;

  struct {
    int    N1;
    int    N2;
    int    N3;
    fft_para_mix_bs_t para1;
    fft_para_mix_bs_t para2;
    fft_para_mix_bs_t para3;
    double *in;
    double *out;
    double *in_w;
    double *out_w;
    double *out_t;
    double *tw;
    double *tw_w;
    double *Bn;
    double *Bn_w;
    double *workbuf;
  }dp_3d_r2c_e;

} fft_param_u;

/* FFT plan */
typedef struct {
  int             nbuf;
  FFTmemBuffer_t  bufs[FFT_MAX_NUM_BUFS];    
  void           *local;
  int (*fftfxn)  (fft_param_u u, void *edmaState, int actualCoreNum);
  fft_callout_t   fftcout;
  void           *edmaState;
  int             actualCoreNum;
  fft_param_u u;
} fft_plan_t;

/* FFT execute API */
int fft_execute (fft_plan_t p);

/* FFT destroy plan APT */
int fft_destroy_plan (fft_plan_t p);

void *fftGetMemHandle();
int  fft_Init(void *sfast_buf_base,  size_t sfast_buf_size);

static void DSPF_dp_mat_trans_cplx(const double *restrict x, const int rows,
  const int cols, double *restrict y);
static void DSPF_dp_mat_trans_cplx_r2r(const double *restrict x, const int rows,
  const int cols, double *restrict y);

#endif /* FFT_COMMON_H_ */

/* ======================================================================== */
/*  End of file:  fft_common.h                                              */
/* ------------------------------------------------------------------------ */
/*            Copyright (c) 2013 Texas Instruments, Incorporated.           */
/*                           All Rights Reserved.                           */
/* ======================================================================== */


