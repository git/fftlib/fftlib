/******************************************************************************
 * FILE PURPOSE: Data structures for using DMA in Framework
 ******************************************************************************
 * FILE NAME:   fft_ires.h
 *
 * (C) Copyright 2013, Texas Instruments Incorporated
 *
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions
 *  are met:
 *
 *    Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *
 *    Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the
 *    distribution.
 *
 *    Neither the name of Texas Instruments Incorporated nor the names of
 *    its contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 *  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 *  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 *  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 *  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 *  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 *  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 *  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 *  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 *  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 *  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 *  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
*/

#ifndef _FFT_IRES_H
#define _FFT_IRES_H

#include <ti/sdo/fc/edmamgr/edmamgr.h>

#define FFT_NUM_EDMA_CH   6
#define FFT_XFER_CHANNEL0 0
#define FFT_XFER_CHANNEL1 1
#define FFT_XFER_CHANNEL2 2
#define FFT_XFER_CHANNEL3 3
#define FFT_XFER_CHANNEL4 4
#define FFT_XFER_CHANNEL5 5

#define FFT_MAX_EDMA_LINKS        1
#define FFT_MAX_EDMA_LINKS_3D     256

typedef struct fft_edma_struct
{
    unsigned int num_channels;
    /* IRES stuff */
    EdmaMgr_Handle      channel[FFT_NUM_EDMA_CH];
} FFT_EDMA_Struct;

#endif

/* nothing after this point */
