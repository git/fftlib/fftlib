/* ======================================================================= */
/*  TEXAS INSTRUMENTS, INC.                                                */
/*                                                                         */
/*  FFTLIB  FFT Library                                                    */
/*                                                                         */
/* Copyright (C) 2013 Texas Instruments Incorporated - http://www.ti.com/  */ 
/*                                                                         */
/*                                                                         */
/*  Redistribution and use in source and binary forms, with or without     */
/*  modification, are permitted provided that the following conditions     */
/*  are met:                                                               */
/*                                                                         */
/*    Redistributions of source code must retain the above copyright       */
/*    notice, this list of conditions and the following disclaimer.        */
/*                                                                         */
/*    Redistributions in binary form must reproduce the above copyright    */
/*    notice, this list of conditions and the following disclaimer in the  */
/*    documentation and/or other materials provided with the               */
/*    distribution.                                                        */
/*                                                                         */
/*    Neither the name of Texas Instruments Incorporated nor the names of  */
/*    its contributors may be used to endorse or promote products derived  */
/*    from this software without specific prior written permission.        */
/*                                                                         */
/*  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS    */
/*  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT      */
/*  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR  */
/*  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT   */
/*  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,  */
/*  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT       */
/*  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,  */
/*  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY  */
/*  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT    */
/*  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE  */
/*  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.   */
/*                                                                         */
/* ======================================================================= */

#ifndef FFT_COMMON_D_H_
#define FFT_COMMON_D_H_ 1
#include <ti/libarch/libarch.h>
#include <ti/fftlib/src/common/fft_edma.h>
#include <ti/fftlib/src/common/omp/omp_config.h>

#define ANYSIZE_SUPPORT 0

#define FFT_MEM_NOERR 0
#define FFT_MEM_ERROR 1

#define FFT_EDMA_STATE_INIT         0
#define FFT_EDMA_STATE_ALLOCATED    1

extern cregister volatile unsigned int DNUM;
#define ACTIVE_THREAD_COUNT (OMP_NUM_CORES)

extern int fftEdmaState[OMP_MAX_NUM_CORES];
extern FFT_EDMA_Struct gEdmaState[OMP_MAX_NUM_CORES];
extern lib_emt_ConfigLarge_t cfg;
extern IALG_MemRec memTab[FFT_NUM_EDMA_CH];

void fft_assert(int statement, int node_id, const char *error);
void *fft_omp_assign_edma_resources(void *buf, int buf_size);
void fft_omp_free_edma_resources(void *edma);
int fft_config_memory(size_t *l2_SRAM_size_orig);
int fft_reconfig_memory(size_t l2_SRAM_size_orig);

void fft_GetSizes(size_t *smem_size_vfast, size_t *smem_size_fast, 
                  size_t *smem_size_med,   size_t *smem_size_slow);
#endif /* FFT_COMMON_D_H_ */

/* ======================================================================== */
/*  End of file:  fft_common_d.h                                            */
/* ------------------------------------------------------------------------ */
/*            Copyright (c) 2013 Texas Instruments, Incorporated.           */
/*                           All Rights Reserved.                           */
/* ======================================================================== */


