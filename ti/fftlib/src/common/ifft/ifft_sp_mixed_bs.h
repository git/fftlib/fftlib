/* ======================================================================= */
/*  TEXAS INSTRUMENTS, INC.                                                */
/*                                                                         */
/*  FFTLIB  FFT Library                                                    */
/*                                                                         */
/* Copyright (C) 2013 Texas Instruments Incorporated - http://www.ti.com/  */ 
/*                                                                         */
/*                                                                         */
/*  Redistribution and use in source and binary forms, with or without     */
/*  modification, are permitted provided that the following conditions     */
/*  are met:                                                               */
/*                                                                         */
/*    Redistributions of source code must retain the above copyright       */
/*    notice, this list of conditions and the following disclaimer.        */
/*                                                                         */
/*    Redistributions in binary form must reproduce the above copyright    */
/*    notice, this list of conditions and the following disclaimer in the  */
/*    documentation and/or other materials provided with the               */
/*    distribution.                                                        */
/*                                                                         */
/*    Neither the name of Texas Instruments Incorporated nor the names of  */
/*    its contributors may be used to endorse or promote products derived  */
/*    from this software without specific prior written permission.        */
/*                                                                         */
/*  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS    */
/*  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT      */
/*  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR  */
/*  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT   */
/*  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,  */
/*  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT       */
/*  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,  */
/*  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY  */
/*  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT    */
/*  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE  */
/*  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.   */
/*                                                                         */
/* ======================================================================= */

#include <ti/fftlib/src/common/fft_common.h>

#ifndef IFFT_SP_MIXED_BS_H_
#define IFFT_SP_MIXED_BS_H_ 1

/** @ingroup 2D */
/* @{ */

/** @defgroup IFFT_SP_MIXED_BS */
/** @ingroup IFFT_SP_MIXED_BS */
/* @{ */

/**
 *     This program performs mixed radix FFT and bluestein FFT for anysize support.
 *  
 *      @param x = Input data array
 *      @param y = Output data array
 * 
 * @par Algorithm:
 *
 * @par Assumptions: 
 *
 * @par Implementation notes:
 * @b Endian Support: The code supports both big and little endian modes. <BR> 
 * @b Interruptibility: The code is interruptible. <BR>
 *  
 */
 
 /* }@ */ /* ingroup */
 /* }@ */ /* ingroup */
 
void ifft_SPxSP_mixed_radix_c2c (int N, float *ptr_x, float *ptr_w, float *ptr_y,
                                int N_p2, int s_r3, int s_r5);

void ifft_SPxSP_mixed_radix_c2r (int N, float *ptr_x, float *ptr_w, float *ptr_y,
                                int N_p2, int s_r3, int s_r5);

void ifft_SPxSP_c2c_bs (
    int   N,           /* signal size           */
    int   M,           /* FFT size              */
    int   rad,         /* FFT radix             */
    float *in,         /* input pointer         */
    float *out,        /* output  pointer       */
    float *tw,         /* twiddle and phase factor pointer  */
    float *Bn,         /* pointer to FFT of bn  */
    float *workbuf     /* work buffer pointer   */
    );

void ifft_SPxSP_c2r_bs (
    int   N,           /* signal size           */
    int   M,           /* FFT size              */
    int   rad,         /* FFT radix             */
    float *in,         /* input pointer         */
    float *out,        /* output  pointer       */
    float *tw,         /* twiddle and phase factor pointer  */
    float *Bn,         /* pointer to FFT of bn  */
    float *workbuf     /* work buffer pointer   */
    );

void ifft_SPxSP_c2r (int N, float *ptr_x, float *ptr_w, float *ptr_y, int n_min);

#endif /* IFFT_SP_MIXED_BS_H_ */

/* ======================================================================== */
/*  End of file: ifft_sp_mixed_bs.h                                         */
/* ------------------------------------------------------------------------ */
/*            Copyright (c) 2013 Texas Instruments, Incorporated.           */
/*                           All Rights Reserved.                           */
/* ======================================================================== */


