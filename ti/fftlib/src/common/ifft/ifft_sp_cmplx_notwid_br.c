/* ===================================================================================  */
/*  ENSA OF MARRAKECH - THALES AIR SYSTEMS                                              */
/*                                                                                      */
/*  NAME                                                                            */
/*      ifft_sp_cmplx_notwid_br -- Optimized standard c66x assembly Implementation      */
/*                                                                                      */
/*  USAGE                                                                               */
/*      single precision floating point radix-2^2 ifft [no twiddle factors' references] */
/*                                                                                      */
/* ===================================================================================  */

/* Copyright (C) 2012 - ENSA OF MARRAKECH - THALES AIR SYSTEMS             */ 
/*                                                                         */
/*                                                                         */
/*  Redistribution and use in source and binary forms, with or without     */
/*  modification, are permitted provided that the following conditions     */
/*  are met:                                                               */
/*                                                                         */
/*    Redistributions of source code must retain the above copyright       */
/*    notice, this list of conditions and the following disclaimer.        */
/*                                                                         */
/*    Redistributions in binary form must reproduce the above copyright    */
/*    notice, this list of conditions and the following disclaimer in the  */
/*    documentation and/or other materials provided with the               */
/*    distribution.                                                        */
/*                                                                         */
/*    Neither the name of THALES AIR SYSTEMS nor the names of              */
/*    its contributors may be used to endorse or promote products derived  */
/*    from this software without specific prior written permission.        */
/*                                                                         */
/*  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS    */
/*  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT      */
/*  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR  */
/*  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT   */
/*  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,  */
/*  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT       */
/*  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,  */
/*  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY  */
/*  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT    */
/*  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE  */
/*  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.   */
/*                                                                         */
/* ======================================================================= */

#include <stdio.h>

// small iffts optimization
void _ifft_rad_2p2_next_sm1_notwid_br(double *inp , double *out_l1, unsigned int n_per_pas, int *svg, int pas, double *coeff, double *outp);

// small iffts optimization
void _ifft_rad_2p2_next_sm2_notwid_br(double *inp , double *out_l1, unsigned int n_per_pas, int *svg, int pas, double *coeff, double *outp);

// main ifft routine
void _ifft_rad_2p2_next_notwid_br(double *inp , double *out_l1, unsigned int n_per_pas, int *svg, int pas, double *coeff, double *outp);

// used for very large iffts [main ifft pass]
void _ifft_large_notwid_br(double *inp , double *out_l1, unsigned int n_per_pas, int *svg, int pas, double *coeffd);

// used for very large iffts [reversed outputs ifft]
void _ifft_rad_2p2_next_ro_f_notwid_br(double *inp , double *out_l1, unsigned int n_per_pas, int *svg, int pas, double *coeff, unsigned int rn);

// used for very large iffts [optimized bit-reversal routine]
void _i_bit_rev_ii32_notwid_br(double *inp,double *outp,int n,int *svg);

// miss pipelining routine [touch]
void _miss_pipe_notwid_br(double *src, int lgth);

static  int svg[20];
#pragma DATA_SECTION (svg, ".mem_l2")

// the main ifft entry
void ifft_sp_cmplx_notwid_br(double *inp,unsigned int n, double *outp, double *coeff) {
  unsigned int interrupt_context;
  if (n<=16) {
    interrupt_context = _disable_interrupts();
    _ifft_rad_2p2_next_sm1_notwid_br(inp,inp,1,svg,n,coeff,outp);
    _restore_interrupts(interrupt_context);
    return;
  }
  if (n<=64) {
    _miss_pipe_notwid_br(inp,n/16);
    interrupt_context = _disable_interrupts();
    _ifft_rad_2p2_next_sm2_notwid_br(inp,inp,1,svg,n,coeff,outp);
    _restore_interrupts(interrupt_context);
    return;
  }
  if (n<=4096) {
    _miss_pipe_notwid_br(inp,n/16);
    interrupt_context = _disable_interrupts();
    _ifft_rad_2p2_next_notwid_br(inp,inp,1,svg,n,coeff,outp);
    _restore_interrupts(interrupt_context);
  } else { // large ifft case
    int k,pas=n/2;
    double coeff_wn=*coeff;

    _miss_pipe_notwid_br(inp,128);
    interrupt_context = _disable_interrupts();
    _ifft_large_notwid_br(inp,inp,1,svg,n,&coeff_wn);
    _restore_interrupts(interrupt_context);

    coeff_wn=_complex_mpysp(coeff_wn,coeff_wn);

    while (pas>4096) {

      for (k=0;k<n/pas;k++) {
        interrupt_context = _disable_interrupts();
        _ifft_large_notwid_br(inp+k*pas,inp+k*pas,1,svg,pas,&coeff_wn);
        _restore_interrupts(interrupt_context);
      }

      pas=pas/2;

      coeff_wn=_complex_mpysp(coeff_wn,coeff_wn);

    }

    for (k=0;k<n/pas;k++) {
      interrupt_context = _disable_interrupts();
      _ifft_rad_2p2_next_ro_f_notwid_br(inp+k*pas,inp+k*pas,1,svg,pas,&coeff_wn,n);
      _restore_interrupts(interrupt_context);
    }

		interrupt_context = _disable_interrupts();
    _i_bit_rev_ii32_notwid_br(inp,outp,n,svg);
		_restore_interrupts(interrupt_context);
  }
}
