; =================================================================================== ;
; ENSA OF MARRAKECH - THALES AIR SYSTEMS                                              ;
;                                                                                     ;
; ifft_rad_2p2_next_sm1_notwid_br.asm -- standard c66x asm implementation             ;
;       * title : a routine that's used while small iffts                             ;
;       * description : optimized iffts for sizes <= 16                               ;
;       * author : Mounir BAHTAT                                                      ;
;       * version : 1.0.0                                                             ;
;       * prototype : void _ifft_rad_2p2_next_sm1_notwid_br(double *inp ,             ;
;                     double *out_l1, unsigned int n_per_pas, int *svg, int pas,      ;
;                     double *coeff, double *outp)                                    ;
;       * parameters :                                                                ;
;           @inp : pointer on the input buffer                                        ;
;           @out_l1 : pointer on the intermediate buffer, usually the input           ;
;           @n_per_pas : must be 1, "n/step"                                          ;
;           @svg : pointer to an external memory where to save some bank registers    ;
;           @pas : ifft size n, "step"                                                ;
;           @coeff : pointer on the twiddle factor                                    ;
;           @outp : the final output where results are written during the last stage  ;
;                                                                                     ;
; =================================================================================== ;

; Copyright (C) 2012 - ENSA OF MARRAKECH - THALES AIR SYSTEMS             ; 
;                                                                         ;
;                                                                         ;
;  Redistribution and use in source and binary forms, with or without     ;
;  modification, are permitted provided that the following conditions     ;
;  are met:                                                               ;
;                                                                         ;
;    Redistributions of source code must retain the above copyright       ;
;    notice, this list of conditions and the following disclaimer.        ;
;                                                                         ;
;    Redistributions in binary form must reproduce the above copyright    ;
;    notice, this list of conditions and the following disclaimer in the  ;
;    documentation and/or other materials provided with the               ;
;    distribution.                                                        ;
;                                                                         ;
;    Neither the name of THALES AIR SYSTEMS nor the names of              ;
;    its contributors may be used to endorse or promote products derived  ;
;    from this software without specific prior written permission.        ;
;                                                                         ;
;  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS    ;
;  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT      ;
;  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR  ;
;  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT   ;
;  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,  ;
;  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT       ;
;  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,  ;
;  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY  ;
;  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT    ;
;  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE  ;
;  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.   ;
;                                                                         ;
; ======================================================================= ;

        .global _ifft_rad_2p2_next_sm1_notwid_br
_ifft_rad_2p2_next_sm1_notwid_br:

; A4 = inp
; B4 = outp
; A6 = N/pas
; B6 = Svg
; A8 = pas
; B8 = coeff
; A10 = final outp

        MVK .S1 16,A1
        CMPEQ .L1 A8,A1,A0
        
        [A0] B .S1 case_16
        NOP 5
        
        MV .S2 B8,B4 ; coeff
||      MV .D1 A10,A6 ; final output

        LDDW .D1 *A4++[4],A31:A30 ; m0
||      LDDW .D2 *B4,B19:B18 ; W1
||      ADD .L2X 8,A4,B5 ; inp+1
||      MVKL .S2 80000000h,B0 ; XOR

        LDDW .D1 *A4--[2],A29:A28 ; m4
||      LDDW .D2 *B5++[4],B31:B30 ; m1
||      MVKH .S2 80000000h,B0 ; XOR

        LDDW .D1 *A4++[4],A31:A30 ; m2
||      LDDW .D2 *B5--[2],B29:B28 ; m5
||      MVKL .S1 3F800000h,A8 ; ONE
||      MV .S2X A6,B6 ; outp

        LDDW .D1 *A4,A29:A28 ; m6
||      LDDW .D2 *B5++[4],B31:B30 ; m3
||      MVKH .S1 3F800000h,A8 ; ONE

        LDDW .D2 *B5,B29:B28 ; m7

        CMPYSP .M2 B19:B18,B19:B18,B23:B22:B21:B20 ; W1 x W1

        DADDSP .L1 A31:A30,A29:A28,A27:A26 ; k0
||      DSUBSP .S1 A31:A30,A29:A28,A25:A24 ; k4

        DADDSP .L2 B31:B30,B29:B28,B27:B26 ; k2
||      DSUBSP .S2 B31:B30,B29:B28,B25:B24 ; k6
||      MV .S1 A8,A9 ; ONE

        DADDSP .L1 A31:A30,A29:A28,A23:A22 ; k1
||      DSUBSP .S1 A31:A30,A29:A28,A21:A20 ; k5

        DADDSP .L2 B31:B30,B29:B28,B23:B22 ; k3
||      DSUBSP .S2 B31:B30,B29:B28,B21:B20 ; k7

        DADDSP .L2 B23:B22,B21:B20,B17:B16 ; W2

        XOR .S1X A21,B0,A20 ; k5 * i
||      ROTL .M1 A20,0,A21 ; k5 * i
||      DADDSP .L1 A27:A26,A23:A22,A31:A30 ; m0

        DSUBSP .S1 A27:A26,A23:A22,A29:A28 ; m2
||      XOR .S2 B21,B0,B20 ; k7 * i
||      ROTL .M2 B20,0,B21 ; k7 * i

        DADDSP .L1 A25:A24,A21:A20,A27:A26 ; m4
||      DSUBSP .S1 A25:A24,A21:A20,A25:A24 ; m6
||      DADDSP .L2 B27:B26,B23:B22,B31:B30 ; m1
||      DSUBSP .S2 B27:B26,B23:B22,B29:B28 ; m3'
||      CMPYSP .M2 B19:B18,B17:B16,B23:B22:B21:B20 ; W1 x W2

        DADDSP .L2 B25:B24,B21:B20,B27:B26 ; m5'
||      DSUBSP .S2 B25:B24,B21:B20,B25:B24 ; m7'

        NOP 1
        
        DSUBSP .S1X A31:A30,B31:B30,A31:A30 ; st4'
||      DADDSP .S2X B31:B30,A31:A30,B31:B30 ; st0'
||      CMPYSP .M2 B17:B16,B29:B28,B23:B22:B21:B20 ; W2 x m3'

        DADDSP .L2 B23:B22,B21:B20,B19:B18 ; W3
||      CMPYSP .M2 B19:B18,B27:B26,B23:B22:B21:B20 ; W1 x m5'

        NOP 1
        
        DMPYSP .M1 A31:A30,A9:A8,A31:A30 ; st4
||      DMPYSP .M2X B31:B30,A9:A8,B31:B30 ; st0
        
        CMPYSP .M2 B19:B18,B25:B24,B23:B22:B21:B20 ; W3 x m7'
||      DSUBSP .L2 B23:B22,B21:B20,B29:B28 ; m3

        DSUBSP .L2 B23:B22,B21:B20,B27:B26 ; m5
        
        NOP 1

        DSUBSP .S1X A29:A28,B29:B28,A29:A28 ; st6'
||      DADDSP .S2X B29:B28,A29:A28,B29:B28 ; st2'
||      STDW .D1 A31:A30,*+A6[4] ; st4
||      STDW .D2 B31:B30,*B6 ; st0

        DSUBSP .L2 B23:B22,B21:B20,B25:B24 ; m7
||      DSUBSP .S1X A27:A26,B27:B26,A27:A26 ; st5'
||      DADDSP .S2X B27:B26,A27:A26,B27:B26 ; st1'

        NOP 1
        
        DMPYSP .M1 A29:A28,A9:A8,A29:A28 ; st6
||      DMPYSP .M2X B29:B28,A9:A8,B29:B28 ; st2

        DSUBSP .S1X A25:A24,B25:B24,A25:A24 ; st7'
||      DADDSP .S2X B25:B24,A25:A24,B25:B24 ; st3'

        DMPYSP .M1 A27:A26,A9:A8,A27:A26 ; st5
||      DMPYSP .M2X B27:B26,A9:A8,B27:B26 ; st1

        B .S2 B3
        
        DMPYSP .M1 A25:A24,A9:A8,A25:A24 ; st7
||      DMPYSP .M2X B25:B24,A9:A8,B25:B24 ; st3
||      STDW .D1 A29:A28,*+A6[6] ; st6
||      STDW .D2 B29:B28,*+B6[2] ; st2

        NOP 1
        
        STDW .D1 A27:A26,*+A6[5] ; st5
||      STDW .D2 B27:B26,*+B6[1] ; st1

        NOP 1
        
        STDW .D1 A25:A24,*+A6[7] ; st7
||      STDW .D2 B25:B24,*+B6[3] ; st3
        
case_16:
        
        MV .L2 B6,B9
||      SHRU .S1 A8,2,A5 ; qpas
||      MV .S2 B4,B25 ; save outp
||      LDDW .D2 *B8,B11:B10 ; W1S
        STDW .D2T1 A11:A10,*B9++
||      MV .L2X A5,B5 ; qpas
||      ADD .L1 A5,A5,A6 ; 2 x qpas
||      MV .S1 A10,A0 ; last output
        STDW .D2T1 A13:A12,*B9++
||      ADD .L2 B5,B5,B6 ; 2 x qpas
||      ADD .L1 A6,A5,A3 ; 3 x qpas
        STDW .D2T1 A15:A14,*B9++
        STDW .D2 B11:B10,*B9++
||      MV .L1 A4,A1 ; inp
||      MVKL .S1 3F800000h,A15 ; ONE
||      MV .D1 A4,A8 ; save inp
        STDW .D2 B13:B12,*B9++
||      ADDAD .D1 A1,A5,A1 ; inp + qpas x 8
||      MVKH .S1 3F800000h,A15 ; ONE
||      ZERO .L1 A14 ; ONE
        STDW .D2 B15:B14,*B9++
||      SUB .D1 A1,16,A1 ; inp + qpas x 8 -= 16
||      DMV .L1 A15,A14,A17:A16 ; W2=ONE
||      DMV .L2 B11,B10,B15:B14 ; W1=W1S
||      CMPYSP .M2 B11:B10,B11:B10,B23:B22:B21:B20 ; W1S x W1S = W2S
        STW .D2 B3,*B9--[12]
||      ADD .L2X 8,A4,B1 ; inp+8
||      MV .D1X B4,A2 ; outp v1
||      ADD .S2 B6,B5,B3 ; 3 x qpas
||      DMV .L1 A15,A14,A19:A18 ; W3=ONE
        ADDAD .D2 B1,B5,B1 ; inp+8 + qpas x 8
||      SHRU .S2 B5,1,B31 ; qpas/2
||      ADD .L2X 8,A2,B2 ; outp v1 +8
        SUB .L2 B1,16,B1 ; inp+8 + qpas x 8 -= 16
||      ADDAD .D1 A2,A5,A7 ; outp v2
||      ADDAD .D2 B2,B5,B7 ; outp v2+8
        ADDAD .D1 A7,A5,A4 ; outp v34
||      ADDAD .D2 B7,B5,B4 ; outp v34+8
||      DADDSP .S2 B23:B22,B21:B20,B13:B12 ; W2S
        SUB .L2 B4,16,B4 ; outp v34+8 -= 16
||      SUB .L1 A4,16,A4 ; outp v34 -= 16
        NOP 1
        DMV .L2 B13,B12,B17:B16 ; W2=W2S
||      CMPYSP .M2 B11:B10,B13:B12,B23:B22:B21:B20 ; W1S x W2S = W3S
        NOP 3
        DADDSP .S2 B23:B22,B21:B20,B19:B18 ; W3S
        NOP 2
        CMPYSP .M2 B11:B10,B19:B18,B23:B22:B21:B20 ; W1S x W3S = W4S
        MVKL .S2 80000000h,B0 ; XOR
        CMPYSP .M2 B19:B18,B19:B18,B23:B22:B21:B20 ; W3S x W3S = W6S
        MVKH .S2 80000000h,B0 ; XOR
        DADDSP .S2 B23:B22,B21:B20,B27:B26 ; W4S
        MV .D1X B25,A8 ; save outp
||      MVC .S2 B31,ILC
        DADDSP .L2 B23:B22,B21:B20,B27:B26 ; W6S
        
        MV .L1X B27,A11 ; W4S
        MV .S1X B26,A10 ; W4S

        MV .L1X B27,A13 ; W6S
        MV .S1X B26,A12 ; W6S
        
        SPLOOP 8
        
        LDDW .D1 *++A1[2],A27:A26 ; m1
||      LDDW .D2 *++B1[2],B27:B26 ; m1'

        LDDW .D1 *+A1[A6],A21:A20 ; m3
||      LDDW .D2 *+B1[B6],B21:B20 ; m3'

        LDDW .D1 *-A1[A5],A23:A22 ; m0
||      LDDW .D2 *-B1[B5],B23:B22 ; m0'

        LDDW .D1 *+A1[A5],A25:A24 ; m2
||      LDDW .D2 *+B1[B5],B25:B24 ; m2' 
        
        
        NOP 2
        
        
        DSUBSP .L1 A27:A26,A21:A20,A27:A26 ; a3
||      DSUBSP .L2 B27:B26,B21:B20,B27:B26 ; a3'

        DADDSP .L1 A21:A20,A27:A26,A23:A22 ; a1
||      DADDSP .L2 B21:B20,B27:B26,B23:B22 ; a1'

        DADDSP .L1 A23:A22,A25:A24,A29:A28 ; a0
||      DADDSP .L2 B23:B22,B25:B24,B29:B28 ; a0'

        ROTL .M1 A26,0,A31 ; a3ii2
||      ROTL .M2 B26,0,B31 ; a3ii2'
||      DSUBSP .L1 A23:A22,A25:A24,A29:A28 ; a2
||      DSUBSP .L2 B23:B22,B25:B24,B29:B28 ; a2'
        
        CMPYSP .M1X A15:A14,B13:B12,A31:A30:A29:A28 ; W1 x W2S
||      CMPYSP .M2 B15:B14,B13:B12,B31:B30:B29:B28 ; W1' x W2S
        
        DADDSP .L1 A29:A28,A23:A22,A23:A22 ; v1
||      DADDSP .L2 B29:B28,B23:B22,B23:B22 ; v1'
||      DSUBSP .S1 A29:A28,A23:A22,A25:A24 ; v2
||      DSUBSP .S2 B29:B28,B23:B22,B25:B24 ; v2'

        
        CMPYSP .M1 A17:A16,A11:A10,A31:A30:A29:A28 ; W2 x W4S
||      CMPYSP .M2X B17:B16,A11:A10,B31:B30:B29:B28 ; W2' x W4S
||      XOR .L1X A27,B0,A30 ; a3ii1
||      XOR .L2 B27,B0,B30 ; a3ii1
        
        
        CMPYSP .M1 A19:A18,A13:A12,A31:A30:A29:A28 ; W3 x W6S
||      CMPYSP .M2X B19:B18,A13:A12,B31:B30:B29:B28 ; W3' x W6S
||      DSUBSP .S1 A29:A28,A31:A30,A21:A20 ; v4
||      DSUBSP .S2 B29:B28,B31:B30,B21:B20 ; v4'
||      DADDSP .L1 A29:A28,A31:A30,A27:A26 ; v3
||      DADDSP .L2 B29:B28,B31:B30,B27:B26 ; v3'
        
        
        CMPYSP .M1 A17:A16,A25:A24,A31:A30:A29:A28 ; v2 x W2
||      CMPYSP .M2 B17:B16,B25:B24,B31:B30:B29:B28 ; v2' x W2'
||      STDW .D1 A23:A22,*A2++[2] ; store v1
||      STDW .D2 B23:B22,*B2++[2] ; store v1'
||      DADDSP .S1 A31:A30,A29:A28,A15:A14 ; W1
||      DADDSP .S2 B31:B30,B29:B28,B15:B14 ; W1'

        NOP 1
        
        DADDSP .S1 A31:A30,A29:A28,A17:A16 ; W2
||      DADDSP .S2 B31:B30,B29:B28,B17:B16 ; W2'
||      CMPYSP .M1 A15:A14,A27:A26,A23:A22:A21:A20 ; v3 x W1
||      CMPYSP .M2 B15:B14,B27:B26,B23:B22:B21:B20 ; v3' x W1'


        DADDSP .S1 A31:A30,A29:A28,A19:A18 ; W3
||      DADDSP .S2 B31:B30,B29:B28,B19:B18 ; W3'


        DSUBSP .S1 A31:A30,A29:A28,A25:A24 ; f2
||      DSUBSP .S2 B31:B30,B29:B28,B25:B24 ; f2'


        CMPYSP .M1 A19:A18,A21:A20,A31:A30:A29:A28 ; v4 x W3
||      CMPYSP .M2 B19:B18,B21:B20,B31:B30:B29:B28 ; v4' x W3'


        DSUBSP .S1 A23:A22,A21:A20,A25:A24 ; f3
||      DSUBSP .S2 B23:B22,B21:B20,B25:B24 ; f3'


        STDW .D1 A25:A24,*A7++[2] ; store f2
||      STDW .D2 B25:B24,*B7++[2] ; store f2'

        NOP 1
        
        
        DSUBSP .S1 A31:A30,A29:A28,A25:A24 ; f4
||      DSUBSP .S2 B31:B30,B29:B28,B25:B24 ; f4'
||      STDW .D1 A25:A24,*++A4[2] ; store f3
||      STDW .D2 B25:B24,*++B4[2] ; store f3'

        
        NOP 4
        
        
        STDW .D1 A25:A24,*+A4[A5] ; store f4
||      STDW .D2 B25:B24,*+B4[B5] ; store f4'
        
                
        SPKERNEL 0,0
        
        NOP 8
        
        ; end stages

        
power4_case:

        MVK .S1 1,A0 ; N/16
||      MV .L1 A8,A5 ; LD 0
||      MV .L2X A8,B5 ; LD N/2+2
||      MV .D1 A0,A6 ; last output
        
        SHL .S1 A0,2,A20 ; N/16 in A0

        MV .S2X A20,B20 ; N/4
||      ZERO .D2 B6 ; Add-B
||      MV .S1 A6,A15 ; ST-0

        ADD .L2X 8,A6,B15 ; ST-1
||      ADDAD .D2 B5,2,B5 ; LD N/2+2
        
        ADD .L1 A20,A20,A21 ; N/2 
||      ADD .L2 B20,B20,B21 ; N/2
||      SUB .S1 A20,3,A4 ; N/4 LD OFFSET
||      ADD .S2 B20,1,B4 ; 3N/4 LD OFFSET
||      ADDAD .D1 A15,2,A14 ; ST-2
||      ADDAD .D2 B15,2,B14 ; ST-3

        ADD .L1 A20,A21,A22 ; 3N/4
||      ADD .L2 B20,B21,B22 ; 3N/4
||      ADD .S1 A21,A21,A23 ; N
||      ADD .S2 B21,B21,B23 ; N
||      ADDAD .D2 B5,B21,B5 ; LD N/2+2

        NORM .L2 B23,B3 ; 32-R
||      ADDAD .D1 A15,A21,A13 ; ST-N/2
||      ADDAD .D2 B15,B21,B13 ; ST-N/2+1
        
        ADD .L2 2,B3,B3 ; 32-R
||      ADDAD .D1 A13,2,A12 ; ST-N/2+2
||      ADDAD .D2 B13,2,B12 ; ST-N/2+3

        ADDAD .D1 A15,A20,A11 ; ST-N/4
||      ADDAD .D2 B15,B20,B11 ; ST-N/4+1
||      ADD .L1 A4,6,A6 ; N/4+3
||      SUB .L2 B4,2,B8 ; N/4-1
||      SHL .S1 A4,3,A4 ; *=8
||      SHL .S2 B4,3,B4 ; *=8

        ADDAD .D1 A15,A22,A2 ; ST-3N/4
||      ADDAD .D2 B15,B22,B2 ; ST-3N/4+1
||      MVKL .S1 3F800000h,A8 ; ONE

        ADDAD .D1 A2,2,A1 ; ST-3N/4+2
||      ADDAD .D2 B2,2,B1 ; ST-3N/4+3
||      MVKH .S1 3F800000h,A8 ; ONE

        ADDAD .D1 A11,2,A10 ; ST-N/4+2
||      ADDAD .D2 B11,2,B10 ; ST-N/4+3
||      MV .S1 A8,A9 ; 1
        
        ; START

        SHL .S2 B6,B3,B27 ; shft-B
        ADD .S2 4,B6,B6 ; ind+=4
||      BITR .M1X B27,A27 ; br-A
||      BITR .M2 B27,B27 ; br-B
        LDW .D2 *+B9[12],B3
        LDDW .D1 *++A5[A27],A29:A28 ; load 0
||      LDDW .D2 *++B5[B27],B29:B28 ; load N/2+2
||      ADD .S1 A6,A27,A7 ; UPD
||      ADD .S2 B8,B27,B7 ; UPD
        LDDW .D1 *++A5[1],A25:A24 ; load 1
||      LDDW .D2 *++B5[1],B25:B24 ; load N/2+3
        LDDW .D1 *++A5[1],A27:A26 ; load 2
||      LDDW .D2 *--B5[3],B27:B26 ; load N/2
        LDDW .D1 *++A5[1],A29:A28 ; load 3
||      LDDW .D2 *++B5[1],B29:B28 ; load N/2+1
        ADD .S1 A5,A4,A5 ; UPD
||      ADD .S2 B5,B4,B5 ; UPD
        NOP 1
        NOP 1
        DADDSP .L1 A29:A28,A27:A26,A23:A22 ; 0-a0
||      DADDSP .L2 B27:B26,B29:B28,B23:B22 ; N/2-a0
||      DSUBSP .S1 A29:A28,A27:A26,A25:A24 ; 0-a2
||      DSUBSP .S2 B27:B26,B29:B28,B25:B24 ; N/2-a2
        DADDSP .L1 A25:A24,A29:A28,A19:A18 ; 0-a1
||      DADDSP .L2 B25:B24,B29:B28,B19:B18 ; N/2-a1
||      DSUBSP .S1 A25:A24,A29:A28,A17:A16 ; 0-a3
||      DSUBSP .S2 B29:B28,B25:B24,B17:B16 ; N/2-a3
        NOP 1
        NOP 1
        XOR .S1X A17,B0,A16 ; 0-a3ii
||      XOR .S2 B17,B0,B16 ; N/2-a3ii
||      ROTL .M1 A16,0,A17 ; 0-a3ii
||      ROTL .M2 B16,0,B17 ; N/2-a3ii
||      DADDSP .L1 A23:A22,A19:A18,A21:A20 ; 0-v1
||      DADDSP .L2 B23:B22,B19:B18,B21:B20 ; N/2-v1
        DSUBSP .L1 A23:A22,A19:A18,A17:A16 ; 0-v2
||      DSUBSP .L2 B23:B22,B19:B18,B17:B16 ; N/2-v2
||      LDDW .D1 *A5++[1],A17:A16 ; load N/4
||      LDDW .D2 *B5++[1],B17:B16 ; load 3N/4+2
        DADDSP .L1 A25:A24,A17:A16,A19:A18 ; 0-v3
||      DADDSP .L2 B25:B24,B17:B16,B19:B18 ; N/2-v3
||      LDDW .D1 *A5++[1],A19:A18 ; load N/4+1
||      LDDW .D2 *B5--[3],B19:B18 ; load 3N/4+3
        DSUBSP .L1 A25:A24,A17:A16,A21:A20 ; 0-v4
||      DSUBSP .L2 B25:B24,B17:B16,B21:B20 ; N/2-v4
||      LDDW .D1 *A5++[1],A21:A20 ; load N/4+2
||      LDDW .D2 *B5++[1],B21:B20 ; load 3N/4
        DMPYSP .M1 A21:A20,A9:A8,A29:A28 ; M-0-v1
||      DMPYSP .M2X B21:B20,A9:A8,B29:B28 ; M-N/2-v1
||      LDDW .D1 *A5--[A7],A17:A16 ; load N/4+3
||      LDDW .D2 *B5--[B7],B17:B16 ; load 3N/4+1
        DMPYSP .M1 A17:A16,A9:A8,A21:A20 ; M-0-v2
||      DMPYSP .M2X B17:B16,A9:A8,B21:B20 ; M-N/2-v2
        DMPYSP .M1 A19:A18,A9:A8,A23:A22 ; M-0-v3
||      DMPYSP .M2X B19:B18,A9:A8,B23:B22 ; M-N/2-v3
        DMPYSP .M1 A21:A20,A9:A8,A19:A18 ; M-0-v4
||      DMPYSP .M2X B21:B20,A9:A8,B19:B18 ; M-N/2-v4
        DADDSP .L1 A17:A16,A21:A20,A17:A16 ; N/4-a0
||      DADDSP .L2 B21:B20,B17:B16,B17:B16 ; 3N/4-a0
||      DSUBSP .S1 A17:A16,A21:A20,A31:A30 ; N/4-a2
||      DSUBSP .S2 B21:B20,B17:B16,B31:B30 ; 3N/4-a2
        STDW .D1 A29:A28,*A15++[4] ; ST-0
||      STDW .D2 B29:B28,*B15++[4] ; ST-1
        DADDSP .L1 A19:A18,A17:A16,A27:A26 ; N/4-a1
||      DADDSP .L2 B17:B16,B19:B18,B27:B26 ; 3N/4-a1
||      DSUBSP .S1 A19:A18,A17:A16,A21:A20 ; N/4-a3
||      DSUBSP .S2 B17:B16,B19:B18,B21:B20 ; 3N/4-a3
        STDW .D1 A21:A20,*A13++[4] ; ST-N/2
||      STDW .D2 B21:B20,*B13++[4] ; ST-N/2+1
        NOP 1
        STDW .D1 A23:A22,*A11++[4] ; ST-N/4
||      STDW .D2 B23:B22,*B11++[4] ; ST-N/4+1
        DADDSP .L1 A17:A16,A27:A26,A27:A26 ; N/4-v1
||      DADDSP .L2 B17:B16,B27:B26,B27:B26 ; 3N/4-v1
        DSUBSP .L1 A17:A16,A27:A26,A23:A22 ; N/4-v2
||      DSUBSP .L2 B17:B16,B27:B26,B23:B22 ; 3N/4-v2
||      STDW .D1 A19:A18,*A2++[4] ; ST-3N/4
||      STDW .D2 B19:B18,*B2++[4] ; ST-3N/4+1
        NOP 1
        XOR .S1X A21,B0,A18 ; N/4-a3ii
||      XOR .S2 B21,B0,B18 ; 3N/4-a3ii
||      ROTL .M1 A20,0,A19 ; N/4-a3ii
||      ROTL .M2 B20,0,B19 ; 3N/4-a3ii
        DMPYSP .M1 A27:A26,A9:A8,A31:A30 ; M-N/4-v1
||      DMPYSP .M2X B27:B26,A9:A8,B31:B30 ; M-3N/4-v1
        NOP 1
        DADDSP .L1 A31:A30,A19:A18,A25:A24 ; N/4-v3
||      DADDSP .L2 B31:B30,B19:B18,B25:B24 ; 3N/4-v3
||      DSUBSP .S1 A31:A30,A19:A18,A27:A26 ; N/4-v4
||      DSUBSP .S2 B31:B30,B19:B18,B27:B26 ; 3N/4-v4
        NOP 1
        LDDW .D2T1 *+B9[2],A15:A14
        LDDW .D2 *+B9[5],B15:B14
        DMPYSP .M1 A23:A22,A9:A8,A21:A20 ; M-N/4-v2
||      DMPYSP .M2X B23:B22,A9:A8,B21:B20 ; M-3N/4-v2
||      LDDW .D2T1 *+B9[1],A13:A12
        LDDW .D2 *+B9[4],B13:B12
        DMPYSP .M1 A25:A24,A9:A8,A23:A22 ; M-N/4-v3
||      DMPYSP .M2X B25:B24,A9:A8,B23:B22 ; M-3N/4-v3
||      STDW .D1 A31:A30,*A14 ; ST-2
||      STDW .D2 B31:B30,*B14 ; ST-3
        DMPYSP .M1 A27:A26,A9:A8,A29:A28 ; M-N/4-v4
||      DMPYSP .M2X B27:B26,A9:A8,B29:B28 ; M-3N/4-v4
||      LDDW .D2T1 *B9,A11:A10
        STDW .D1 A21:A20,*A12 ; ST-N/2+2
||      STDW .D2 B21:B20,*B12 ; ST-N/2+3
||      B .S2 B3
        LDDW .D2 *+B9[3],B11:B10
        STDW .D1 A23:A22,*A10 ; ST-N/4+2
||      STDW .D2 B23:B22,*B10 ; ST-N/4+3
        NOP 1
        STDW .D1 A29:A28,*A1 ; ST-3N/4+2
||      STDW .D2 B29:B28,*B1 ; ST-3N/4+3
        NOP 1
        
        ; end stage 1

