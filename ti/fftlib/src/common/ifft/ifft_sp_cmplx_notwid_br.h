/* ===================================================================================  */
/*  ENSA OF MARRAKECH - THALES AIR SYSTEMS                                              */
/*                                                                                      */
/*  NAME                                                                                */
/*      ifft_sp_cmplx_notwid_br -- Optimized standard c66x assembly Implementation      */
/*                                                                                      */
/*  USAGE                                                                               */
/*      single precision floating point radix-2^2 ifft [no twiddle factors' references] */
/*                                                                                      */
/* ===================================================================================  */

/* Copyright (C) 2012 - ENSA OF MARRAKECH - THALES AIR SYSTEMS             */ 
/*                                                                         */
/*                                                                         */
/*  Redistribution and use in source and binary forms, with or without     */
/*  modification, are permitted provided that the following conditions     */
/*  are met:                                                               */
/*                                                                         */
/*    Redistributions of source code must retain the above copyright       */
/*    notice, this list of conditions and the following disclaimer.        */
/*                                                                         */
/*    Redistributions in binary form must reproduce the above copyright    */
/*    notice, this list of conditions and the following disclaimer in the  */
/*    documentation and/or other materials provided with the               */
/*    distribution.                                                        */
/*                                                                         */
/*    Neither the name of THALES AIR SYSTEMS nor the names of              */
/*    its contributors may be used to endorse or promote products derived  */
/*    from this software without specific prior written permission.        */
/*                                                                         */
/*  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS    */
/*  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT      */
/*  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR  */
/*  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT   */
/*  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,  */
/*  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT       */
/*  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,  */
/*  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY  */
/*  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT    */
/*  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE  */
/*  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.   */
/*                                                                         */
/* ======================================================================= */

/** 
 *   @file  ifft_sp_cmplx_notwid_br.h
 *
 *   @brief  
 *      ifft_sp_cmplx_notwid_br -- Optimized standard c66x assembly Implementation <BR />
 *    single precision floating point radix-2^2 ifft [no twiddle factors' references]
 *
 *   @version
 *    1.0.0
 *
 *   @author
 *    Mounir BAHTAT
 */

 /** @defgroup IFFT_SP_CMPLX_NOTWID_BR ifft_sp_cmplx_notwid_br
 *
 * @section Introduction
 *
 * @subsection Description
 *
 *    -> C-Callable standard C66x assembly routine for radix-2^2 iFFT Including Bit-Reversing
 *    -> iFFT on single-precision complex samples
 *    -> Only 1 twiddle factor is needed on arguments [no need to allocate a twiddle factor array]
 *    -> The input is also modified by the iFFT routine
 *
 * @subsection Maximum Peak Performance
 *
 *    -> 1 cycle per pseudo-radix-2 butterfly [Nlog4(N) cycles; approximated/simplified formula]
 *    -> Maximum performance reached at n=4K on L1D, benchmarking 1.04 cycles per pseudo-radix-2 butterfly
 *    [25780 cycles]
 *
 * @subsection Implementation Notes
 *
 *    -> Endianness : Little Endian
 *    -> Complex representation : (RE:IM) IMAG parts in odd positions and REAL parts in even positions
 *    -> Arrays should align on the double words boundary
 */
 
 /**
@defgroup IFFT_SP_CMPLX_NOTWID_BR_FUNCTIONS  ifft_sp_cmplx_notwid_br functions
@ingroup IFFT_SP_CMPLX_NOTWID_BR
*/

/** @addtogroup IFFT_SP_CMPLX_NOTWID_BR_FUNCTIONS
 @{ */

/**
 * \fn void ifft_sp_cmplx_notwid_br(double *inp,unsigned int n, double *outp, double *coeff)
 * \details ifft_sp_cmplx_notwid_br -- Optimized standard c66x assembly Implementation <br /> <br />
 * @b Author
 *   @n Mounir BAHTAT
 *
 * @b Description
 *   @n Single precision floating point radix-2^2 DIF ifft [no twiddle factors' references]
 *   @n This implementation includes bit-reversal
 *
 * @b Version
 *   @n 1.0.0
 *
 * @b Language
 *   @n Standard C66x ASM, C, C instrinsics
 *
 * \param inp pointer on 'n' input samples
 * \param n number of samples, must be a power of 2 ; n>=8
 * \param outp iFFT result buffer on 'n' samples ; must be different from the inp buffer
 * \param coeff a pointer on a single twiddle factor Wn=exp(j2PI/n) ; provided in the (IM:RE) form
 *
 *
 *   @b Example
 *   @verbatim
    double inp[N]; // input buffer of complex samples
    double outp[N]; // output buffer of complex samples
    double coeff_wn; // the single twiddle factor

    wr_im(&coeff_wn, cos( 2*M_PI/(double)N ) ); // twiddle in IM:RE
    wr_re(&coeff_wn, sin( 2*M_PI/(double)N ) ); // twiddle in IM:RE
    
    ifft_sp_cmplx_notwid_br(inp,N,outp,&coeff_wn); // ifft call
     @endverbatim
 */

/**
@}
*/

#ifndef ifft_sp_cmplx_notwid_br_H_
#define ifft_sp_cmplx_notwid_br_H_

void ifft_sp_cmplx_notwid_br(double *inp,unsigned int n, double *outp, double *coeff);

#endif /* ifft_sp_cmplx_notwid_br_H_ */
