; =================================================================================== ;
; ENSA OF MARRAKECH - THALES AIR SYSTEMS                                              ;
;                                                                                     ;
; ifft_large_notwid_br.asm -- standard c66x asm implementation                        ;
;       * title : main routine for large iffts, radix-2 passes                        ;
;       * description : performs radix-2 ifft stages for large sizes                  ;
;                       in order to avoid miss thrashing                              ;
;       * author : Mounir BAHTAT                                                      ;
;       * version : 1.0.0                                                             ;
;       * prototype : void _ifft_large_notwid_br(double *inp , double *out_l1,        ;
;                     unsigned int n_per_pas, int *svg, int pas, double *coeffd)      ;
;       * parameters :                                                                ;
;           @inp : pointer on the input buffer                                        ;
;           @out_l1 : pointer on the intermediate buffer, usually the input           ;
;           @n_per_pas : must be 1, "n/step"                                          ;
;           @svg : pointer to an external memory where to save some bank registers    ;
;           @pas : ifft size n, "step"                                                ;
;           @coeffd : pointer on the twiddle factor                                   ;
;       * used as an in-place ifft                                                    ;
;                                                                                     ;
; =================================================================================== ;

; Copyright (C) 2012 - ENSA OF MARRAKECH - THALES AIR SYSTEMS             ; 
;                                                                         ;
;                                                                         ;
;  Redistribution and use in source and binary forms, with or without     ;
;  modification, are permitted provided that the following conditions     ;
;  are met:                                                               ;
;                                                                         ;
;    Redistributions of source code must retain the above copyright       ;
;    notice, this list of conditions and the following disclaimer.        ;
;                                                                         ;
;    Redistributions in binary form must reproduce the above copyright    ;
;    notice, this list of conditions and the following disclaimer in the  ;
;    documentation and/or other materials provided with the               ;
;    distribution.                                                        ;
;                                                                         ;
;    Neither the name of THALES AIR SYSTEMS nor the names of              ;
;    its contributors may be used to endorse or promote products derived  ;
;    from this software without specific prior written permission.        ;
;                                                                         ;
;  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS    ;
;  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT      ;
;  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR  ;
;  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT   ;
;  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,  ;
;  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT       ;
;  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,  ;
;  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY  ;
;  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT    ;
;  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE  ;
;  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.   ;
;                                                                         ;
; ======================================================================= ;

        .global _ifft_large_notwid_br
_ifft_large_notwid_br:

; A4 = inp
; B4 = outp
; A6 = N/pas
; B6 = Svg
; A8 = pas
; B8 = coeff

; saving registers
        MV .S2 B6,B9
        STDW .D2T1 A11:A10,*B9++
        STDW .D2T1 A13:A12,*B9++
        STDW .D2T1 A15:A14,*B9++
        STDW .D2 B11:B10,*B9++
        STDW .D2 B13:B12,*B9++
        STDW .D2 B15:B14,*B9++
        STW .D2 B3,*B9--[12]
; saved registers

        MV .L1 A4,A2 ; inp
||      ADD .L2X 8,A4,B2 ; inp + 1
||      SHRU .S1 A8,1,A7 ; N/2
||      LDDW .D2T1 *B8,A11:A10 ; W1S
        MV .L1X B4,A4 ; outp
||      ADD .L2 B4,8,B4 ; outp + 1
||      MV .S2X A7,B7 ; N/2
||      SHRU .S1 A8,3,A0 ; N/8
        ADDAD .D1 A2,A7,A3 ; inp + N/2
||      ADDAD .D2 B2,B7,B3 ; inp + N/2 + 1
||      MVKL .S1 3F800000h,A12 ; ONE
        ADDAD .D1 A4,A7,A5 ; outp + N/2
||      ADDAD .D2 B4,B7,B5 ; outp + N/2 + 1
||      MVKH .S1 3F800000h,A13 ; ONE
        DMV .L1 A13,A12,A17:A16 ; W1=ONE
        CMPYSP .M1 A11:A10,A11:A10,A31:A30:A29:A28 ; W1S x W1S
||      MV .S2X A10,B16 ; W1S
        MV .S2X A11,B17 ; W1S
        NOP 2
        DADDSP .L1 A31:A30,A29:A28,A19:A18 ; W2S
        NOP 2
        CMPYSP .M1 A11:A10,A19:A18,A31:A30:A29:A28 ; W2S x W1S
        CMPYSP .M1 A19:A18,A19:A18,A31:A30:A29:A28 ; W2S x W2S
        NOP 2
        DADDSP .L1 A31:A30,A29:A28,A21:A20 ; W3S
        DADDSP .L1 A31:A30,A29:A28,A15:A14 ; W4S
        MV .S2X A0,B0 ; N/8
        MV .S2X A20,B18 ; W3S
        MV .S2X A21,B19 ; W3S
        MV .S2X A14,B14 ; W4S
        MV .S2X A15,B15 ; W4S
        MV .S2X A12,B12 ; ONE
        MV .S2X A13,B13 ; ONE
        
        MVC .S2 B0,ILC
        NOP 3

        SPLOOP 8

        LDDW .D1 *A2++[2],A27:A26 ; LD-0
||      LDDW .D2 *B2++[2],B27:B26 ; LD-1

        LDDW .D1 *A2++[2],A21:A20 ; LD-2
||      LDDW .D2 *B2++[2],B21:B20 ; LD-3

        LDDW .D1 *A3++[2],A29:A28 ; LD-N/2
||      LDDW .D2 *B3++[2],B29:B28 ; LD-N/2+1

        NOP 2

        LDDW .D1 *A3++[2],A31:A30 ; LD-N/2+2
||      LDDW .D2 *B3++[2],B31:B30 ; LD-N/2+3

        NOP 1

        DADDSP .L1 A27:A26,A29:A28,A25:A24 ; a-0 = "LD-0"+"LD-N/2"
||      DADDSP .L2 B27:B26,B29:B28,B25:B24 ; a-1 = "LD-1"+"LD-N/2+1"
||      DSUBSP .S1 A27:A26,A29:A28,A29:A28 ; s-N = "LD-0"-"LD-N/2"
||      DSUBSP .S2 B27:B26,B29:B28,B29:B28 ; s-N+1 = "LD-1"-"LD-N/2+1"

        CMPYSP .M1 A15:A14,A17:A16,A31:A30:A29:A28 ; W1
||      CMPYSP .M2 B15:B14,B17:B16,B31:B30:B29:B28 ; W2

        CMPYSP .M1 A15:A14,A19:A18,A31:A30:A29:A28 ; W3
||      CMPYSP .M2 B15:B14,B19:B18,B31:B30:B29:B28 ; W4

        CMPYSP .M1 A17:A16,A29:A28,A31:A30:A29:A28 ; MUL-N = "W1" x "s-N"
||      CMPYSP .M2 B17:B16,B29:B28,B31:B30:B29:B28 ; MUL-N+1 = "W2" x "s-N+1"
||      DADDSP .L1 A21:A20,A31:A30,A23:A22 ; a-2 = "LD-2"+"LD-N/2+2"
||      DADDSP .L2 B21:B20,B31:B30,B23:B22 ; a-3 = "LD-3"+"LD-N/2+3"
||      DSUBSP .S1 A21:A20,A31:A30,A21:A20 ; s-N+2 = "LD-2"-"LD-N/2+2"
||      DSUBSP .S2 B21:B20,B31:B30,B21:B20 ; s-N+3 = "LD-3"-"LD-N/2+3"

        NOP 1
        
        DADDSP .L1 A31:A30,A29:A28,A17:A16 ; W1
||      DADDSP .L2 B31:B30,B29:B28,B17:B16 ; W2

        CMPYSP .M1 A19:A18,A21:A20,A31:A30:A29:A28 ; MUL-N+2 = "W3" x "s-N+2"
||      CMPYSP .M2 B19:B18,B21:B20,B31:B30:B29:B28 ; MUL-N+3 = "W4" x "s-N+3"
||      DADDSP .L1 A31:A30,A29:A28,A19:A18 ; W3
||      DADDSP .L2 B31:B30,B29:B28,B19:B18 ; W4

        DSUBSP .L1 A31:A30,A29:A28,A27:A26 ; v-N
||      DSUBSP .L2 B31:B30,B29:B28,B27:B26 ; v-N+1
||      STDW .D1 A25:A24,*A4++[2] ; ST-0 ; a-0
||      STDW .D2 B25:B24,*B4++[2] ; ST-1 ; a-1

        STDW .D1 A23:A22,*A4++[2] ; ST-2 ; a-2
||      STDW .D2 B23:B22,*B4++[2] ; ST-3 ; a-3

        NOP 1
        
        DSUBSP .L1 A31:A30,A29:A28,A27:A26 ; v-N+2
||      DSUBSP .L2 B31:B30,B29:B28,B27:B26 ; v-N+3

        NOP 1
        
        STDW .D1 A27:A26,*A5++[2] ; ST-N ; v-N
||      STDW .D2 B27:B26,*B5++[2] ; ST-N+1 ; v-N+1
        
        STDW .D1 A27:A26,*A5++[2] ; ST-N+2 ; v-N+2
||      STDW .D2 B27:B26,*B5++[2] ; ST-N+3 ; v-N+3

        SPKERNEL 0,0
        
        NOP 8
        NOP 5


; loading registers
        LDDW .D2T1 *B9++,A11:A10
        LDDW .D2T1 *B9++,A13:A12
        LDDW .D2T1 *B9++,A15:A14
        LDDW .D2 *B9++,B11:B10
        LDDW .D2 *B9++,B13:B12
        LDDW .D2 *B9++,B15:B14
        LDW .D2 *B9,B3
        NOP 4
; loaded registers
        B .S2 B3
        NOP 5
