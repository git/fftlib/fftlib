; =================================================================================== ;
; ENSA OF MARRAKECH - THALES AIR SYSTEMS                                              ;
;                                                                                     ;
; i_bit_rev_ii32_notwid_br.asm -- standard c66x asm implementation                    ;
;       * title : a routine that's used while large iffts                             ;
;       * description : optimized bit-reversal routine with an extended ii=32         ;
;                       this optimization tries to exploit each cache line at best    ;
;       * author : Mounir BAHTAT                                                      ;
;       * version : 1.0.0                                                             ;
;       * prototype : void _i_bit_rev_ii32_notwid_br(double *inp,double *outp, int n, ;
;                     int *svg)                                                       ;
;       * parameters :                                                                ;
;           @inp : pointer on the input buffer                                        ;
;           @outp : pointer on the output buffer,[in-place operation isn't supported] ;
;           @n : buffers size, must be >= 32                                          ;
;           @svg : pointer to an external memory where to save some bank registers    ;
;                                                                                     ;
; =================================================================================== ;

; Copyright (C) 2012 - ENSA OF MARRAKECH - THALES AIR SYSTEMS             ; 
;                                                                         ;
;                                                                         ;
;  Redistribution and use in source and binary forms, with or without     ;
;  modification, are permitted provided that the following conditions     ;
;  are met:                                                               ;
;                                                                         ;
;    Redistributions of source code must retain the above copyright       ;
;    notice, this list of conditions and the following disclaimer.        ;
;                                                                         ;
;    Redistributions in binary form must reproduce the above copyright    ;
;    notice, this list of conditions and the following disclaimer in the  ;
;    documentation and/or other materials provided with the               ;
;    distribution.                                                        ;
;                                                                         ;
;    Neither the name of THALES AIR SYSTEMS nor the names of              ;
;    its contributors may be used to endorse or promote products derived  ;
;    from this software without specific prior written permission.        ;
;                                                                         ;
;  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS    ;
;  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT      ;
;  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR  ;
;  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT   ;
;  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,  ;
;  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT       ;
;  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,  ;
;  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY  ;
;  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT    ;
;  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE  ;
;  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.   ;
;                                                                         ;
; ======================================================================= ;

        .global _i_bit_rev_ii32_notwid_br
_i_bit_rev_ii32_notwid_br:

; A4 = inp
; B4 = outp
; A6 = N
; B6 = Svg

; saving registers
        MV .S2 B6,B0
        STDW .D2T1 A11:A10,*B0++
        STDW .D2T1 A13:A12,*B0++
        STDW .D2T1 A15:A14,*B0++
        STDW .D2 B11:B10,*B0++
        STDW .D2 B13:B12,*B0++
        STDW .D2 B15:B14,*B0++
        STW .D2 B3,*B0--[12]
; saved registers

        ADD .L2 8,B4,B11 ; outp
||      MV .L1X B4,A11 ; outp   
||      SHRU .S1 A6,5,A0 ; N/32
        
        SUB .D1 A0,1,A0 ; N/32-1
||      MVK .S1 4,A1 ; ADD
||      NORM .L1 A6,A2 ; 32-R

        ADD .L1 A2,2,A2 ; 32-R
||      MV .D1 A4,A3 ; inp  
||      MV .D2X A4,B3 ; inp
||      SHRU .S1 A6,1,A4 ; N/2
        
        MV .S2X A4,B4 ; N/2
||      MV .L2 B3,B1 ; LD-0

        ADDAD .D2 B3,B4,B3 ; LD-N/2
        ADD .S2 8,B3,B3 ; LD-N/2+1

        MV .L2 B3,B2 ; LD-N/2+1
||      SHRU .S1 A4,1,A5 ; N/4
        
        SUB .D1 A5,7,A5 ; N/4-7
||      ZERO .L1 A29 ; br
||      ZERO .L2 B29 ; br
        
        ADD .L2X 2,A5,B5 ; N/4-7
||      SUB .D1 A4,4,A6 ; N/2-4
||      SUB .D2 B4,4,B6 ; N/2-4
||      SHRU .S1 A4,1,A7 ; N/4
||      SHRU .S2 B4,1,B7 ; N/4

        ADDAD .D1 A11,A7,A10 ; ST-N/4
||      ADDAD .D2 B11,B7,B10 ; ST-N/4
||      SHRU .S1 A4,2,A20 ; N/8
||      SHRU .S2 B4,2,B20 ; N/8

        ADDAD .D1 A11,A20,A9 ; ST-N/8
||      ADDAD .D2 B11,B20,B9 ; ST-N/8
||      ADD .L1 A20,A7,A7 ; 3N/8
||      ADD .L2 B20,B7,B7 ; 3N/8

        ADDAD .D1 A11,A7,A8 ; ST-3N/8
||      ADDAD .D2 B11,B7,B8 ; ST-3N/8
        
        ADDAD .D1 A11,A6,A11 ; ST-0
||      ADDAD .D2 B11,B6,B11 ; ST-0

        ADDAD .D1 A10,A6,A10 ; ST-N/4
||      ADDAD .D2 B10,B6,B10 ; ST-N/4

        ADDAD .D1 A9,A6,A9 ; ST-N/8
||      ADDAD .D2 B9,B6,B9 ; ST-N/8

        ADDAD .D1 A8,A6,A8 ; ST-3N/8
||      ADDAD .D2 B8,B6,B8 ; ST-3N/8
        
loop_br_ii32:

        LDDW .D1 *++A3[A29],A31:A30 ; LD-0
||      LDDW .D2 *++B3[B29],B31:B30 ; LD-N/2+1

        LDDW .D1 *++A3[1],A31:A30 ; LD-1
||      LDDW .D2 *--B3[1],B31:B30 ; LD-N/2

        LDDW .D1 *++A3[1],A31:A30 ; LD-2
||      LDDW .D2 *++B3[3],B31:B30 ; LD-N/2+3

        LDDW .D1 *++A3[1],A31:A30 ; LD-3
||      LDDW .D2 *--B3[1],B31:B30 ; LD-N/2+2

        LDDW .D1 *++A3[1],A31:A30 ; LD-4
||      LDDW .D2 *++B3[3],B31:B30 ; LD-N/2+5

        LDDW .D1 *++A3[1],A31:A30 ; LD-5
||      LDDW .D2 *--B3[1],B31:B30 ; LD-N/2+4
||      DMVD .L1 A31,A30,A29:A28 ; ping-pong
||      DMVD .L2 B31,B30,B29:B28 ; ping-pong

        LDDW .D1 *++A3[1],A31:A30 ; LD-6
||      LDDW .D2 *++B3[3],B31:B30 ; LD-N/2+7
||      DMVD .L1 A31,A30,A29:A28 ; ping-pong
||      DMVD .L2 B31,B30,B29:B28 ; ping-pong

        LDDW .D1 *++A3[1],A31:A30 ; LD-7
||      LDDW .D2 *--B3[1],B31:B30 ; LD-N/2+6
||      DMVD .L1 A31,A30,A29:A28 ; ping-pong
||      DMVD .L2 B31,B30,B29:B28 ; ping-pong

        LDDW .D1 *++A3[A5],A23:A22 ; LD-N/4
||      LDDW .D2 *++B3[B5],B23:B22 ; LD-3N/4+1
||      DMVD .L1 A31,A30,A29:A28 ; ping-pong
||      DMVD .L2 B31,B30,B29:B28 ; ping-pong

        LDDW .D1 *++A3[1],A21:A20 ; LD-N/4+1
||      LDDW .D2 *--B3[1],B21:B20 ; LD-3N/4
||      DMVD .L1 A31,A30,A29:A28 ; ping-pong
||      DMVD .L2 B31,B30,B29:B28 ; ping-pong
||      DMVD .S1 A29,A28,A27:A26 ; ping-pong - 0
||      DMVD .S2 B29,B28,B27:B26 ; ping-pong - N/2+1

        LDDW .D1 *++A3[1],A15:A14 ; LD-N/4+2
||      LDDW .D2 *++B3[3],B15:B14 ; LD-3N/4+3
||      DMVD .L1 A31,A30,A29:A28 ; ping-pong
||      DMVD .L2 B31,B30,B29:B28 ; ping-pong
||      DMVD .S1 A29,A28,A25:A24 ; ping-pong - 1
||      DMVD .S2 B29,B28,B25:B24 ; ping-pong - N/2

        LDDW .D1 *++A3[1],A13:A12 ; LD-N/4+3
||      LDDW .D2 *--B3[1],B13:B12 ; LD-3N/4+2
||      DMVD .L1 A31,A30,A29:A28 ; ping-pong
||      DMVD .L2 B31,B30,B29:B28 ; ping-pong
||      DMVD .S1 A29,A28,A31:A30 ; ping-pong
||      DMVD .S2 B29,B28,B31:B30 ; ping-pong

        LDDW .D1 *++A3[1],A29:A28 ; LD-N/4+4
||      LDDW .D2 *++B3[3],B29:B28 ; LD-3N/4+5
||      DMVD .L1 A31,A30,A29:A28 ; ping-pong
||      DMVD .L2 B31,B30,B29:B28 ; ping-pong
||      DMVD .S1 A29,A28,A31:A30 ; ping-pong
||      DMVD .S2 B29,B28,B31:B30 ; ping-pong

        LDDW .D1 *++A3[1],A29:A28 ; LD-N/4+5
||      LDDW .D2 *--B3[1],B29:B28 ; LD-3N/4+4
||      DMVD .S1 A29,A28,A31:A30 ; ping-pong
||      DMVD .S2 B29,B28,B31:B30 ; ping-pong

        LDDW .D1 *++A3[1],A29:A28 ; LD-N/4+6
||      LDDW .D2 *++B3[3],B29:B28 ; LD-3N/4+7
||      DMVD .S1 A29,A28,A31:A30 ; ping-pong
||      DMVD .S2 B29,B28,B31:B30 ; ping-pong

        LDDW .D1 *++A3[1],A29:A28 ; LD-N/4+7
||      LDDW .D2 *--B3[1],B29:B28 ; LD-3N/4+6
||      DMVD .S1 A29,A28,A31:A30 ; ping-pong
||      DMVD .S2 B29,B28,B31:B30 ; ping-pong
||      DMVD .L1 A31,A30,A19:A18 ; ping-pong - 2
||      DMVD .L2 B31,B30,B19:B18 ; ping-pong - N/2+3

        DMVD .S1 A29,A28,A31:A30 ; ping-pong
||      DMVD .S2 B29,B28,B31:B30 ; ping-pong
||      STDW .D1 A27:A26,*--A11[A6] ; ST-0
||      STDW .D2 B25:B24,*--B11[B6] ; ST-1
||      DMVD .L1 A31,A30,A17:A16 ; ping-pong - 3
||      DMVD .L2 B31,B30,B17:B16 ; ping-pong - N/2+2

        DMVD .L1 A31,A30,A27:A26 ; ping-pong - 4
||      DMVD .L2 B31,B30,B27:B26 ; ping-pong - N/2+5
||      DMVD .S1 A29,A28,A23:A22 ; ping-pong - N/4+4
||      DMVD .S2 B29,B28,B23:B22 ; ping-pong - 3N/4+5
||      STDW .D1 A23:A22,*+A11[2] ; ST-2
||      STDW .D2 B21:B20,*+B11[2] ; ST-3


        DMVD .L1 A31,A30,A25:A24 ; ping-pong - 5
||      DMVD .L2 B31,B30,B25:B24 ; ping-pong - N/2+4
||      DMVD .S1 A29,A28,A21:A20 ; ping-pong - N/4+5
||      DMVD .S2 B29,B28,B21:B20 ; ping-pong - 3N/4+4
||      STDW .D1 A25:A24,*++A11[A4] ; ST-N/2
||      STDW .D2 B27:B26,*++B11[B4] ; ST-N/2+1

        DMVD .S1 A29,A28,A29:A28 ; ping-pong
||      DMVD .S2 B29,B28,B29:B28 ; ping-pong
||      DMVD .L1 A31,A30,A31:A30 ; ping-pong
||      DMVD .L2 B31,B30,B31:B30 ; ping-pong
||      STDW .D1 A21:A20,*+A11[2] ; ST-N/2+2
||      STDW .D2 B23:B22,*+B11[2] ; ST-N/2+3

        DMVD .S1 A29,A28,A29:A28 ; ping-pong
||      DMVD .S2 B29,B28,B29:B28 ; ping-pong
||      DMVD .L1 A31,A30,A31:A30 ; ping-pong
||      DMVD .L2 B31,B30,B31:B30 ; ping-pong
||      STDW .D1 A19:A18,*--A10[A6] ; ST-N/4
||      STDW .D2 B17:B16,*--B10[B6] ; ST-N/4+1

        STDW .D1 A15:A14,*+A10[2] ; ST-N/4+2
||      STDW .D2 B13:B12,*+B10[2] ; ST-N/4+3

        STDW .D1 A17:A16,*++A10[A4] ; ST-3N/4
||      STDW .D2 B19:B18,*++B10[B4] ; ST-3N/4+1

        DMVD .L1 A31,A30,A19:A18 ; ping-pong - 6
||      DMVD .L2 B31,B30,B19:B18 ; ping-pong - N/2+7
||      DMVD .S1 A29,A28,A15:A14 ; ping-pong - N/4+6
||      DMVD .S2 B29,B28,B15:B14 ; ping-pong - 3N/4+7
||      STDW .D1 A13:A12,*+A10[2] ; ST-3N/4+2
||      STDW .D2 B15:B14,*+B10[2] ; ST-3N/4+3


        DMVD .L1 A31,A30,A17:A16 ; ping-pong - 7
||      DMVD .L2 B31,B30,B17:B16 ; ping-pong - N/2+6
||      DMVD .S1 A29,A28,A13:A12 ; ping-pong - N/4+7
||      DMVD .S2 B29,B28,B13:B12 ; ping-pong - 3N/4+6
||      STDW .D1 A27:A26,*--A9[A6] ; ST-N/8
||      STDW .D2 B25:B24,*--B9[B6] ; ST-N/8+1

        STDW .D1 A23:A22,*+A9[2] ; ST-N/8+2
||      STDW .D2 B21:B20,*+B9[2] ; ST-N/8+3

        [A0] B .S1 loop_br_ii32
||      STDW .D1 A25:A24,*++A9[A4] ; ST-5N/8
||      STDW .D2 B27:B26,*++B9[B4] ; ST-5N/8+1

        STDW .D1 A21:A20,*+A9[2] ; ST-5N/8+2
||      STDW .D2 B23:B22,*+B9[2] ; ST-5N/8+3

        SHL .S1 A1,A2,A29 ; shft-A
||      STDW .D1 A19:A18,*--A8[A6] ; ST-3N/8
||      STDW .D2 B17:B16,*--B8[B6] ; ST-3N/8+1

        ADD .S1 A1,4,A1 ; add-A
||      BITR .M1 A29,A29 ; br-A
||      SUB .L1 A0,1,A0 ; loop index
||      STDW .D1 A15:A14,*+A8[2] ; ST-3N/8+2
||      STDW .D2 B13:B12,*+B8[2] ; ST-3N/8+3

        STDW .D1 A17:A16,*++A8[A4] ; ST-7N/8
||      STDW .D2 B19:B18,*++B8[B4] ; ST-7N/8+1

        MV .L1X B1,A3 ; ld init
||      MV .L2 B2,B3 ; ld init
||      MV .S2X A29,B29 ; move br-A
||      STDW .D1 A13:A12,*+A8[2] ; ST-7N/8+2
||      STDW .D2 B15:B14,*+B8[2] ; ST-7N/8+3

; loading registers
        LDDW .D2T1 *B0++,A11:A10
        LDDW .D2T1 *B0++,A13:A12
        LDDW .D2T1 *B0++,A15:A14
        LDDW .D2 *B0++,B11:B10
        LDDW .D2 *B0++,B13:B12
        LDDW .D2 *B0++,B15:B14
        LDW .D2 *B0,B3
        NOP 4
; loaded registers
        B .S2 B3
        NOP 5

