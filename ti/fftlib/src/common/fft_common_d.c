/* ======================================================================= */
/*  TEXAS INSTRUMENTS, INC.                                                */
/*                                                                         */
/*  FFTLIB  FFT Library                                                    */
/*                                                                         */
/* Copyright (C) 2013 Texas Instruments Incorporated - http://www.ti.com/  */ 
/*                                                                         */
/*                                                                         */
/*  Redistribution and use in source and binary forms, with or without     */
/*  modification, are permitted provided that the following conditions     */
/*  are met:                                                               */
/*                                                                         */
/*    Redistributions of source code must retain the above copyright       */
/*    notice, this list of conditions and the following disclaimer.        */
/*                                                                         */
/*    Redistributions in binary form must reproduce the above copyright    */
/*    notice, this list of conditions and the following disclaimer in the  */
/*    documentation and/or other materials provided with the               */
/*    distribution.                                                        */
/*                                                                         */
/*    Neither the name of Texas Instruments Incorporated nor the names of  */
/*    its contributors may be used to endorse or promote products derived  */
/*    from this software without specific prior written permission.        */
/*                                                                         */
/*  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS    */
/*  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT      */
/*  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR  */
/*  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT   */
/*  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,  */
/*  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT       */
/*  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,  */
/*  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY  */
/*  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT    */
/*  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE  */
/*  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.   */
/*                                                                         */
/* ======================================================================= */

#include <xdc/std.h>
#include <stdio.h>
#include <time.h>
#include <stdlib.h>
#include <limits.h>
#include <math.h>
#include <c6x.h>

#include <ti/fftlib/src/common/omp/omp_config.h>
#include <ti/runtime/openmp/omp.h>
#include "fft_common_d.h"

int fftEdmaState[OMP_MAX_NUM_CORES];
FFT_EDMA_Struct gEdmaState[OMP_MAX_NUM_CORES];
#pragma DATA_SECTION (gEdmaState, ".mem_msm")
lib_emt_ConfigLarge_t cfg;
IALG_MemRec memTab[FFT_NUM_EDMA_CH];

/* Function to trap exceptions */
void fft_assert(int statement, int node_id, const char *error)
{
  volatile int dbg_halt = 1;

  if(!statement) {
    printf("%s (%d)\n",error,node_id);
    while(dbg_halt);
  }
}

void *fft_omp_assign_edma_resources(void *buf, int buf_size)
{
  /*
   * The edmaInstances are indexes into the C6678_config[] array defined in
   * fft_c6678_config, which is used to specify how EDMA resources are
   * divided between cores.
   */
  int   i, ret_val;
  void *ret = (void *) (&gEdmaState[0]);
  uint32_t           *buf0 = buf;

  cfg.max_num_lines = FFT_MAX_EDMA_LINKS_3D;
  for (i = 0; i < FFT_NUM_EDMA_CH; i++) {
    ret_val = lib_emt_getSizesLarge(&cfg, &memTab[i]);
    fft_assert( (ret_val == LIB_EMT_SUCCESS), DNUM, "error with lib_emt_getsizeslarge()");
    memTab[i].base = buf0;
    buf0 += memTab[i].size/sizeof(uint32_t);
  }

  fft_assert( ((memTab[0].size*FFT_NUM_EDMA_CH) <= buf_size), DNUM, "Cannot satisfy large channel memory requirements!");

  #pragma omp parallel
  {
    if ( fftEdmaState[DNUM] != FFT_EDMA_STATE_ALLOCATED )
    {
      gEdmaState[DNUM].num_channels = 0;

      while ( gEdmaState[DNUM].num_channels < FFT_NUM_EDMA_CH )
      {
        fft_assert( ((gEdmaState[DNUM].channel[gEdmaState[DNUM].num_channels]) = lib_emt_allocLarge(&cfg, &memTab[gEdmaState[DNUM].num_channels])) != NULL , DNUM, "lib_emt_alloc() failed ");

        gEdmaState[DNUM].num_channels++;
      }
    }
    fftEdmaState[DNUM] = FFT_EDMA_STATE_ALLOCATED;
  }

  return ret;
}

void fft_omp_free_edma_resources(void *edma)
{
  /*
   * The edmaInstances are indexes into the C6678_config[] array defined in
   * fft_c6678_config, which is used to specify how EDMA resources are
   * divided between cores.
   */
  int ret_val;

  #pragma omp parallel
  {

    if ( fftEdmaState[DNUM] == FFT_EDMA_STATE_ALLOCATED )
    {
      while ( gEdmaState[DNUM].num_channels > 0 )
      {
        gEdmaState[DNUM].num_channels--;

        ret_val = lib_emt_free(gEdmaState[DNUM].channel[gEdmaState[DNUM].num_channels]);

        fft_assert( ret_val == LIB_EMT_SUCCESS, DNUM, "edma free failed!");

      }
    }
    fftEdmaState[DNUM] = FFT_EDMA_STATE_INIT;
  }
}

/*==============================================================================
 * This function prepares memory for the processing function 
 *============================================================================*/
int fft_config_memory(size_t *l2_SRAM_size_orig)
{
    size_t smem_size_vfast, smem_size_fast, smem_size_med, smem_size_slow;
    void  *l2_SRAM_ptr;
    int    l2_cfg_err;
    
    /* First, verify the provided/available memory meet requirements */
    fft_GetSizes(&smem_size_vfast, &smem_size_fast, &smem_size_med, &smem_size_slow);

    if((smem_size_fast > lib_get_L2_total_size()))    // available L2 SRAM  
    {                                                            
        return(FFT_MEM_ERROR);
    }
    
    /* Configure L2 if necessary */
    *l2_SRAM_size_orig  = lib_get_L2_SRAM_size();     // get current L2 SRAM size 
    printf("Original L2 SRAM size is %d.\n", *l2_SRAM_size_orig);
  
    l2_cfg_err = LIB_CACHE_SUCCESS;
    if(*l2_SRAM_size_orig <= smem_size_fast) {        // configure L2 if needs more SRAM 
      #pragma omp parallel
      { 
        l2_cfg_err  = lib_L2_config_SRAM(smem_size_fast);    
      }
    }
    
    if(l2_cfg_err) {
      printf("Error in configuring L1D and L2!\n.");
      return(FFT_MEM_ERROR);        
    }

    printf("Configured L2  SRAM size is %d.\n", lib_get_L2_SRAM_size());
    
    /* get L2 SRAM base address */
    l2_SRAM_ptr  = lib_get_L2_SRAM_base();   

    printf("L2  SRAM base address is 0x%x.\n", (unsigned int) l2_SRAM_ptr);
  
    /* pass allocated memories for heap initialization */
    if(fft_Init(l2_SRAM_ptr, smem_size_fast)) {
      return(FFT_MEM_ERROR);        
    }

    return(FFT_MEM_NOERR);            
} /* fft_config_memory */


/*==============================================================================
 * This function reconfigures L1D and L2 after processing is finished
 *============================================================================*/
int fft_reconfig_memory(size_t l2_SRAM_size_orig)
{
    int l2_cfg_err;
    
    l2_cfg_err = LIB_CACHE_SUCCESS;
    if(l2_SRAM_size_orig <= lib_get_L2_SRAM_size()) {
      #pragma omp parallel
      {
        l2_cfg_err  = lib_L2_config_SRAM(l2_SRAM_size_orig);      
      }
    }
    
    /* configure L1D and L2 back */    
    if(l2_cfg_err) {
      printf("Error in reconfiguring L1D and L2!\n.");
      return(FFT_MEM_ERROR);
    }   

    printf("Reconfigured L2  SRAM size is %d.\n", lib_get_L2_SRAM_size());
    
    return(FFT_MEM_NOERR);            
} /* fft_reconfig_memory */

/* ======================================================================== */
/*  End of file:  fft_common_d.c                                            */
/* ------------------------------------------------------------------------ */
/*            Copyright (c) 2013 Texas Instruments, Incorporated.           */
/*                           All Rights Reserved.                           */
/* ======================================================================== */
