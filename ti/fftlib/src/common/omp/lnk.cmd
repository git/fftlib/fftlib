/* ======================================================================= */
/* lnk.cmd -- Linker command file                                          */
/*                                                                         */
/* Rev 0.0.1                                                               */
/*                                                                         */
/* Copyright (C) 2011 Texas Instruments Incorporated - http://www.ti.com/  */ 
/*                                                                         */
/*                                                                         */
/*  Redistribution and use in source and binary forms, with or without     */
/*  modification, are permitted provided that the following conditions     */
/*  are met:                                                               */
/*                                                                         */
/*    Redistributions of source code must retain the above copyright       */
/*    notice, this list of conditions and the following disclaimer.        */
/*                                                                         */
/*    Redistributions in binary form must reproduce the above copyright    */
/*    notice, this list of conditions and the following disclaimer in the  */
/*    documentation and/or other materials provided with the               */
/*    distribution.                                                        */
/*                                                                         */
/*    Neither the name of Texas Instruments Incorporated nor the names of  */
/*    its contributors may be used to endorse or promote products derived  */
/*    from this software without specific prior written permission.        */
/*                                                                         */
/*  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS    */
/*  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT      */
/*  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR  */
/*  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT   */
/*  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,  */
/*  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT       */
/*  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,  */
/*  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY  */
/*  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT    */
/*  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE  */
/*  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.   */
/*                                                                         */
/* ======================================================================= */
SECTIONS
{
    /*
     *  The .far and .fardata sections of FC and EDMA3LLD need to be local to each core.
     *  These sections are therefore placed in memory local to each core.
     */
    .fclocalfar :
    {
        "edmamgr.ae66"      (.fardata)
        "ecpy.ae66"         (.fardata)
        "edma3Chan.ae66"    (.fardata)
        "edma3.ae66"	    (.fardata)
        "rman.ae66"	    (.fardata)
        "nullres.ae66"	    (.fardata)
        "fcsettings.ae66"   (.fardata)
        "edma3_lld_rm.ae66" (.fardata)

        "edmamgr.ae66"      (.far)
        "ecpy.ae66"         (.far)
        "edma3Chan.ae66"    (.far)
        "edma3.ae66"	    (.far)
        "rman.ae66"	    (.far)
        "nullres.ae66"	    (.far)
        "fcsettings.ae66"   (.far)
        "edma3_lld_rm.ae66" (.far)
    } > L2SRAM

    .fardata: load >> DDR3
    .far: load >> DDR3
}
-lC:\FFTLIB_Tools\dsplib_c66x_3_1_1_1\lib\dsplib.ae66
