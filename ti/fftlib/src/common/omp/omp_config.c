/**
 * \file tomp_config.c
 * 
 * \brief Implements default versions of configuration functions          
 *
 * Functions implemented:
 *   __TI_omp_reset 
 *   __TI_omp_configure
 */

#include <ti/runtime/openmp/omp.h>
#include <ti/csl/csl_cache.h>
#include <ti/csl/csl_cacheAux.h>
#include <ti/csl/csl_msmc.h>
#include <ti/csl/csl_msmcAux.h>

extern cregister volatile unsigned int DNUM;

/** MAR register bits to enable caching/prefetch/write-through */
#define CACHE_DISABLE   (0x0)
#define CACHE_PC        (0x1)
#define CACHE_WTE       (0x2)
#define CACHE_PFX       (0x8)


/* @{ */
#include <ti/fftlib/src/common/omp/omp_config.h>

/**
 * Default reset routine. Invoked by all cores during boot, before cinit.
 *
 * Invoked before C initialization is performed - C init run addresses
 * can be in regions mapped by MPAX.
 * Annotated weak, can be overridden by a definition in application source
 * @see c_int00
 *
 * Typically performs the following operations:
 * - Sets up caches
 * - Initializes the MPAX registers for mapping memory regions
 * - Initializes the MAR registers to set attributes for memory regions
 *
 * NOTE: The addresses and sizes used here must correspond to those specified 
 * in the Platform or linker command file!
 */

#define MSMCSRAM_START_ADDR     (0x0C000000)
#define MSMCSRAM_SIZE           (0x00400000)    /* 4 MB */
#define MSMCSRAM_NC_START_ADDR  (0xA0000000)
#define MSMCSRAM_NC_SIZE        (0x00100000)    /* 1 MB */
#define DDR_START_ADDR          (0x80000000)
#define DDR_SIZE                (0x20000000)    /* 512 MB */

void __TI_omp_reset(void)
{
    /* Configure caches */
    CACHE_setL1DSize(CACHE_L1_32KCACHE);
    CACHE_setL1PSize(CACHE_L1_32KCACHE);
//    CACHE_setL2Size(CACHE_32KCACHE);

    /* OMP runtime requires a portion of MSMCSRAM to be non-cached. Since it is 
     * not possible to disable caching in the MSMCSRAM address range, we need a 
     * 2 step process:
     * 1. Map a portion of MSMCSRAM into a range that can be marked as
     *    non-cached. This is done using the MPAX register
     * 2. Annotate the mapped section as non-cached using the appropriate
     *    MAR register for that memory range
     * All accesses to MSMCSRAM through the mapped address range will not
     * be cached.
     */
    __TI_setMPAX(3, MSMCSRAM_NC_START_ADDR, 
                    MSMCSRAM_START_ADDR, 0x13 /* 1 MB */);
    __TI_setMAR(MSMCSRAM_NC_START_ADDR, MSMCSRAM_NC_SIZE, CACHE_DISABLE);

    /* Annotate MSMCSRAM and DDR as cached + prefetch + write through */
    __TI_setMAR(MSMCSRAM_START_ADDR, MSMCSRAM_SIZE, 
                CACHE_PC|CACHE_PFX|CACHE_WTE);
    __TI_setMAR(DDR_START_ADDR, DDR_SIZE, CACHE_PC|CACHE_PFX|CACHE_WTE);

    /* QMSS memories are not cacheable 
     * - Keystone I  devices: MAR52 : 0x34xx_xxxx
     */
    CACHE_disableCaching(52);
    CACHE_setMemRegionInfo(52, 0, 0);

    /* TODO: Remove after OpenEM runtime supports a "no PDSP" mode         */
    /* Map 1 MB from MSMCSRAM_NC_START_ADDR to MSMCSRAM_START_ADDR for     */
    /* masters other than the CorePac DSPs (in this case, the PDSP)        */
    /* Use segment 2 for PRIVID 8 and 10 (QMSS)                            */
    if (DNUM == 0) {
        CSL_MSMC_SESMPAXH lvSesMpaxh;
        CSL_MSMC_SESMPAXL lvSesMpaxl;
        lvSesMpaxh.segSz = 0x13;
        lvSesMpaxh.baddr = MSMCSRAM_NC_START_ADDR >> 12; 
        CSL_MSMC_setSESMPAXH(8,2,&lvSesMpaxh);
        CSL_MSMC_setSESMPAXH(10,2,&lvSesMpaxh);
        lvSesMpaxl.ux = 1;
        lvSesMpaxl.uw = 1;
        lvSesMpaxl.ur = 1;
        lvSesMpaxl.sx = 1;
        lvSesMpaxl.sw = 1;
        lvSesMpaxl.sr = 1;
        lvSesMpaxl.raddr = MSMCSRAM_START_ADDR >> 12;
        CSL_MSMC_setSESMPAXL(8,2,&lvSesMpaxl);
        CSL_MSMC_setSESMPAXL(10,2,&lvSesMpaxl);
    }
}


/**
 * Default runtime configuration function. Called by the master core.
 *
 * It is annotated weak and can be overridden by a user provided function 
 * with the same name.
 * @see __TI_omp_config_thread_stack
 */
void __TI_omp_configure(void)
{
    __TI_omp_config_cores (OMP_MASTER_CORE_IDX, OMP_NUM_CORES);
    __TI_omp_wait_at_core_barrier();
    __TI_omp_config_hw_semaphores(/*hw_sem_base_idx=*/3);
    if (DNUM == OMP_MASTER_CORE_IDX)
    {
        __TI_omp_config_hw_queues (/* em_hw_queue_base_idx=*/       1024,
                                   /* em_free_queue_base_idx=*/     2048,
                                   /* em_dma_queue_base_idx=*/      0,
                                   /* em_private_free_queue_idx=*/  1023,
                                   /* em_first_desc_idx_in_linking_ram=*/0,
                                   /* em_first_memory_region_idx=*/ 0);
    /* Thread stacks in core local memory */
        __TI_omp_config_thread_stack(1, 0x10000);
     }
}
/* @} */

