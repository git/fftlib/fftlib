/* ======================================================================= */
/*  TEXAS INSTRUMENTS, INC.                                                */
/*                                                                         */
/*  FFTLIB  FFT Library                                                    */
/*                                                                         */
/* Copyright (C) 2013 Texas Instruments Incorporated - http://www.ti.com/  */ 
/*                                                                         */
/*                                                                         */
/*  Redistribution and use in source and binary forms, with or without     */
/*  modification, are permitted provided that the following conditions     */
/*  are met:                                                               */
/*                                                                         */
/*    Redistributions of source code must retain the above copyright       */
/*    notice, this list of conditions and the following disclaimer.        */
/*                                                                         */
/*    Redistributions in binary form must reproduce the above copyright    */
/*    notice, this list of conditions and the following disclaimer in the  */
/*    documentation and/or other materials provided with the               */
/*    distribution.                                                        */
/*                                                                         */
/*    Neither the name of Texas Instruments Incorporated nor the names of  */
/*    its contributors may be used to endorse or promote products derived  */
/*    from this software without specific prior written permission.        */
/*                                                                         */
/*  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS    */
/*  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT      */
/*  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR  */
/*  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT   */
/*  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,  */
/*  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT       */
/*  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,  */
/*  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY  */
/*  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT    */
/*  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE  */
/*  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.   */
/*                                                                         */
/* ======================================================================= */

#include <xdc/std.h>
#include "c6x.h"
#include "stdlib.h"
#include "ifft_sp_1d_c2c.h"
#include <ti/fftlib/src/common/ifft/ifft_sp_mixed_bs.h>
#include <ti/fftlib/src/common/ifft/ifft_sp_cmplx_notwid_br.h>
#include <ti/fftlib/src/common/fft_edma.h>
#include <ti/dsplib/src/DSPF_sp_mat_trans_cplx/DSPF_sp_mat_trans_cplx.h>

int ifft_sp_1d_c2c_direct (
    /* int   N;        IFFT size            */
    /* float *in;      input pointer        */
    /* float *out;     output pointer       */
    /* float *tw;      twiddle pointer      */
    fft_param_u u,
    void  *edmaState
)
{
    int   error = FFT_NOERR;
    /* input parameters */
    int   N    = u.sp_1d_c2c_d.N;          /* signal size      */
    int   M    = u.sp_1d_c2c_d.para1.M;    /* FFT size M       */
    int   N_p2 = u.sp_1d_c2c_d.para1.N_p2; /* power of 2 size in mixed radix */
    int   s_r3 = u.sp_1d_c2c_d.para1.s_r3; /* number stages of radix 3       */
    int   s_r5 = u.sp_1d_c2c_d.para1.s_r5; /* number stages of radix 5       */
       
    float *in  = u.sp_1d_c2c_d.in;         /* input pointer    */
    float *out = u.sp_1d_c2c_d.out;        /* output pointer   */
    float *tw  = u.sp_1d_c2c_d.tw;         /* twiddle pointer  */
    float *Bn  = u.sp_1d_c2c_d.Bn;
    float *workbuf = u.sp_1d_c2c_d.workbuf;

    if (N == M) {
      if (s_r3 == 0 && s_r5 == 0) {
        ifft_sp_cmplx_notwid_br((double *)in, N, (double *)out, (double *)tw);
      } else {
        ifft_SPxSP_mixed_radix_c2c (N, in, tw, out, N_p2, s_r3, s_r5);
      }
    } else {
      ifft_SPxSP_c2c_bs (N, M, 0, in, out, tw, Bn, workbuf);
    }


    return error;
}

int ifft_sp_1d_c2c_ecpy (
    /* int   N;           IFFT size               */
    /* float *data;       data pointer            */
    /* float *data_w;     data internal pointer   */
    /* float *work_w;     working buffer pointer  */
    /* float *tw;         twiddle pointer         */
    /* float *tw_w;       twiddle work            */
    fft_param_u u,
    void *edmaState
)
{
    /* input parameters */
    int   N        = u.sp_1d_c2c_e.N;            /* signal size                    */
    int   M        = u.sp_1d_c2c_e.para1.M;      /* FFT size M       */
    int   twsize   = u.sp_1d_c2c_e.para1.twsize; /* twiddle size in double words   */
    int   N_p2     = u.sp_1d_c2c_e.para1.N_p2;   /* power of 2 size in mixed radix */
    int   s_r3     = u.sp_1d_c2c_e.para1.s_r3;   /* number stages of radix 3       */
    int   s_r5     = u.sp_1d_c2c_e.para1.s_r5;   /* number stages of radix 5       */
    int   Bnsize   = u.sp_1d_c2c_e.para1.Bnsize; /* Bn size in double words        */
    
    float *data    = u.sp_1d_c2c_e.data;    /* data pointer                   */
    float *data_w  = u.sp_1d_c2c_e.data_w;  /* data internal pointer          */
    float *work_w  = u.sp_1d_c2c_e.work_w;  /* working buffer pointer         */
    float *tw      = u.sp_1d_c2c_e.tw;      /* twiddle pointer                */
    float *tw_w    = u.sp_1d_c2c_e.tw_w;    /* twiddle working buffer pointer */
    float *Bn      = u.sp_1d_c2c_e.Bn;
    float *Bn_w    = u.sp_1d_c2c_e.Bn_w;
    float *workbuf = u.sp_1d_c2c_e.workbuf;

    int   error = FFT_NOERR;
    int   insize  = 2*N*sizeof(float),
          outsize = 2*N*sizeof(float);

    FFT_EDMA_Struct  *edma = (FFT_EDMA_Struct *)edmaState;

    if (tw_w != NULL) {
      /* prepare twiddle buffer */
      EdmaMgr_copy1D1D(
        edma->channel[FFT_XFER_CHANNEL3], 
        (void *)tw,
        (void *)tw_w,
        twsize*sizeof(float));
      tw = tw_w;
    }
    
    if (Bn_w != NULL) {
      /* prepare twiddle buffer */
      EdmaMgr_copy1D1D(
        edma->channel[FFT_XFER_CHANNEL1], 
        (void *)Bn,
        (void *)Bn_w,
        Bnsize*sizeof(float));
      Bn = Bn_w;
    }

    /* prepare IFFT input buffer */
    EdmaMgr_copy1D1D( 
      edma->channel[FFT_XFER_CHANNEL0], 
      (void *)data,
      (void *)data_w,
      insize);

    /* wait for first IFFT input */
    EdmaMgr_wait(edma->channel[FFT_XFER_CHANNEL0]);

    if (tw_w != NULL) {
      EdmaMgr_wait(edma->channel[FFT_XFER_CHANNEL3]);
    }

    if (N == M) {
      if (s_r3 == 0 && s_r5 == 0) {
        ifft_sp_cmplx_notwid_br((double *)data_w, N, (double *)work_w, (double *)tw);
      } else {
        ifft_SPxSP_mixed_radix_c2c (N, data_w, tw, work_w, N_p2, s_r3, s_r5);
      }
    } else {
      if (Bn_w != NULL) {
        EdmaMgr_wait(edma->channel[FFT_XFER_CHANNEL1]);
      }
      ifft_SPxSP_c2c_bs (N, M, 0, data_w, work_w, tw, Bn, workbuf);
    }


    /* export output data of first IFFT */
    EdmaMgr_copy1D1D( 
      edma->channel[FFT_XFER_CHANNEL1], 
      (void *)work_w,
      (void *)data,
      outsize);

    EdmaMgr_wait(edma->channel[FFT_XFER_CHANNEL1]);

    return error;
} /* ifft_sp_1d_c2c_ecpy */

int ifft_sp_1d_c2c_4step_ecpy (
    /* int   N;        IFFT size                        */
    /* int   n1;       stage1 IFFT size                 */
    /* int   n2;       stage2 IFFT size                 */
    /* float *data;    data pointer                     */
    /* float *work;    working buffer pointer           */
    /* float *data_w;  data internal pointer            */
    /* float *work_w;  working buffer internal pointer  */
    /* float *tw;      twiddle pointer                  */
    /* float *tw_w;    twiddle internal pointer         */
    fft_param_u u,
    void *edmaState
)
{
    /* input parameters */
    int   N1        = u.sp_1d_c2c_e.n1;           /* stage 1 size                     */
    int   N2        = u.sp_1d_c2c_e.n2;           /* stage 2 size                     */
    int   N_p2      = u.sp_1d_c2c_e.para1.N_p2;   /* power of 2 size in mixed radix   */
    int   s_r3      = u.sp_1d_c2c_e.para1.s_r3;   /* number stages of radix 3         */
    int   s_r5      = u.sp_1d_c2c_e.para1.s_r5;   /* number stages of radix 5         */
    int   tw_offset = u.sp_1d_c2c_e.para1.twsize; /* Offset needed for twiddle ptr    */
   
    float *data    = u.sp_1d_c2c_e.data;    /* data pointer                     */
    float *work    = u.sp_1d_c2c_e.work;    /* working buffer pointer           */
    float *data_w  = u.sp_1d_c2c_e.data_w;  /* data internal pointer            */
    float *work_w  = u.sp_1d_c2c_e.work_w;  /* working buffer internal pointer  */
    float *work_t  = u.sp_1d_c2c_e.work_t;  /* temp buffer internal pointer     */
    float *tw      = u.sp_1d_c2c_e.tw;      /* twiddle pointer                  */
    float *tw_w    = u.sp_1d_c2c_e.tw_w;    /* twiddle working buffer pointer   */

    FFT_EDMA_Struct  *edma = (FFT_EDMA_Struct *)edmaState;
    int   error = FFT_NOERR, num_of_lines = IFFT_SP_1D_C2C_NUMOFLINEBUFS;
    int   twsize = (u.sp_1d_c2c_e.para1.twsize+u.sp_1d_c2c_e.para2.twsize)*sizeof(float);
    int   i,j,ch_i,ch_o,ch_i1,ch_o1,dum0,dum1;
    int   insize    = N1*2*sizeof(float),
          outsample = N1,
          linesize  = N2*2*sizeof(float);

    float *restrict in_ptr = data, *restrict out_ptr = work, *restrict tempout;
    float *restrict input, *restrict output, *restrict in1, *restrict out1, *dum2, *dum3;
    float *restrict tw1, *restrict tw2;
    __float2_t co_si;
    __float2_t twid, temp;

    if (tw_w != NULL) {
      /* prepare twiddle buffer */
      EdmaMgr_copy1D1D(
        edma->channel[FFT_XFER_CHANNEL3], 
        (void *)tw,
        (void *)tw_w,
        twsize);
      tw  = tw_w;
    }
    tw1 = tw + tw_offset + 2*N2;
    tw2 = tw + tw_offset - 2;

    _nassert(N1 >= 8);
    _nassert(N2 >= 8);

    /* stage 1: transpose, n2 IFFT's of size n1, apply global twiddle, transpose */
    /* prepare first IFFT input buffer */
    EdmaMgr_copy2D1D(
      edma->channel[FFT_XFER_CHANNEL0],
      (void *)in_ptr,
      (void *)data_w,
      2*sizeof(float),
      outsample,
      linesize);
    in_ptr += 2;

    /* set pingpong buffer pointers */
    in1    = data_w;
    out1   = work_w;
    input  = &data_w[2*N1];
    output = &work_w[2*N1];

    /* prepare second IFFT input buffer */
    EdmaMgr_copy2D1D(
      edma->channel[FFT_XFER_CHANNEL2],
      (void *)in_ptr,
      (void *)input,
      2*sizeof(float),
      outsample,
      linesize);
    in_ptr += 2;

    /* wait for first IFFT input */
    if (tw_w != NULL) {
      EdmaMgr_wait(edma->channel[FFT_XFER_CHANNEL3]);
    }
    EdmaMgr_wait(edma->channel[FFT_XFER_CHANNEL0]);

    if (s_r3 == 0 && s_r5 == 0) {
      ifft_sp_cmplx_notwid_br((double *)in1, N1, (double *)out1, (double *)tw);
    } else {
      ifft_SPxSP_mixed_radix_c2c (N1, in1, tw, out1, N_p2, s_r3, s_r5);
    }

    /* export output data of first IFFT, transposed */
    EdmaMgr_copy1D2D(
      edma->channel[FFT_XFER_CHANNEL1],
      (void *)out1,
      (void *)out_ptr,
      2*sizeof(float),
      outsample,
      linesize);
    out_ptr += 2;

    /* prepare third IFFT input buffer */
    EdmaMgr_copy2D1D(
      edma->channel[FFT_XFER_CHANNEL0],
      (void *)in_ptr,
      (void *)data_w,
      2*sizeof(float),
      outsample,
      linesize);
    in_ptr += 2;

    /* set channel numbers */
    ch_i  = FFT_XFER_CHANNEL2;
    ch_o  = FFT_XFER_CHANNEL3;
    ch_i1 = FFT_XFER_CHANNEL0;
    ch_o1 = FFT_XFER_CHANNEL1;

    /* wait for EDMA */
    EdmaMgr_wait(edma->channel[ch_i]);

    for (i = 1; i < N2; i++) {

      if (s_r3 == 0 && s_r5 == 0) {
        ifft_sp_cmplx_notwid_br((double *)input, N1, (double *)output, (double *)tw);
      } else {
        ifft_SPxSP_mixed_radix_c2c (N1, input, tw, output, N_p2, s_r3, s_r5);
      }
      co_si = _amem8_f2(&tw2[2*i]);
      twid  = co_si;
      for (j = 1; j < N1; j++) {
          temp = _amem8_f2(&output[2*j]);
          temp = _complex_mpysp(temp, twid);
          twid = _complex_mpysp(co_si, twid);
          _amem8_f2(&output[2*j]) = temp;
      }

      /* export output data, transposed */
      EdmaMgr_copy1D2D(
        edma->channel[ch_o],
        (void *)output,
        (void *)out_ptr,
        2*sizeof(float),
        outsample,
        linesize);
      out_ptr += 2;

      /* prepare input data */
      EdmaMgr_copy2D1D(
        edma->channel[ch_i],
        (void *)in_ptr,
        (void *)input,
        2*sizeof(float),
        outsample,
        linesize);
      in_ptr += 2;

      /* swap pingpong buffer pointers and XFER channel */
      dum2   = input;
      input  = in1;
      in1    = dum2;
      dum3   = output;
      output = out1;
      out1   = dum3;

      dum0   = ch_i;
      ch_i   = ch_i1;
      ch_i1  = dum0;
      dum1   = ch_o;
      ch_o   = ch_o1;
      ch_o1  = dum1;

      /* wait for EDMA */
      EdmaMgr_wait(edma->channel[ch_i]);
      EdmaMgr_wait(edma->channel[ch_o]);
    }

    /* wait for stage1 to complete */
    EdmaMgr_wait(edma->channel[ch_i1]);
    EdmaMgr_wait(edma->channel[ch_o1]);

    /* stage 2: n1 IFFT's of size n2, transpose */
    insize    = N2*2*sizeof(float)*num_of_lines;
    outsample = N2;
    linesize  = N1*2*sizeof(float);
    in_ptr    = work;
    out_ptr   = data;
    N_p2      = u.sp_1d_c2c_e.para2.N_p2;   /* power of 2 size in mixed radix   */
    s_r3      = u.sp_1d_c2c_e.para2.s_r3;   /* number stages of radix 3         */
    s_r5      = u.sp_1d_c2c_e.para2.s_r5;   /* number stages of radix 5         */     

    /* prepare first IFFT input buffer */
    EdmaMgr_copy1D1D(
      edma->channel[FFT_XFER_CHANNEL0],
      (void *)in_ptr,
      (void *)work_w,
      insize);
    in_ptr += 2*N2*num_of_lines;

    /* set pingpong buffer pointers */
    in1    = work_w;
    out1   = data_w;
    input  = &work_w[2*N2*num_of_lines];
    output = &data_w[2*N2*num_of_lines];
    tempout= work_t;

    /* prepare second IFFT input buffer */
    EdmaMgr_copy1D1D(
      edma->channel[FFT_XFER_CHANNEL2],
      (void *)in_ptr,
      (void *)input,
      insize);
    in_ptr += 2*N2*num_of_lines;

    /* wait for first IFFT input */
    EdmaMgr_wait(edma->channel[FFT_XFER_CHANNEL0]);

#if (IFFT_SP_1D_C2C_NUMOFLINEBUFS>1)
    if (s_r3 == 0 && s_r5 == 0) {
      for (i=0; i<num_of_lines; i++) {
        ifft_sp_cmplx_notwid_br((double *)&in1[i*N2*2], N2, (double *)&tempout[i*N2*2], (double *)tw1);
      }
    }
    else {
      for (i=0; i<num_of_lines; i++) {
        ifft_SPxSP_mixed_radix_c2c (N2, &in1[i*N2*2], tw1, &tempout[i*N2*2], N_p2, s_r3, s_r5);
      }
    }
  
    DSPF_sp_mat_trans_cplx(tempout, num_of_lines, N2, out1);
#else
    if (s_r3 == 0 && s_r5 == 0) {
      ifft_sp_cmplx_notwid_br((double *)in1, N2, (double *)out1, (double *)tw1);
    } else {
      ifft_SPxSP_mixed_radix_c2c (N2, in1, tw1, out1, N_p2, s_r3, s_r5);
    }
#endif 

    /* export output data of first IFFT, transposed */
    EdmaMgr_copy1D2D(
      edma->channel[FFT_XFER_CHANNEL1],
      (void *)out1,
      (void *)out_ptr,
      2*sizeof(float)*num_of_lines,
      outsample,
      linesize);
    out_ptr += 2*num_of_lines;

    /* prepare third IFFT input buffer */
    EdmaMgr_copy1D1D(
      edma->channel[FFT_XFER_CHANNEL0],
      (void *)in_ptr,
      (void *)work_w,
      insize);
    in_ptr += 2*N2*num_of_lines;

    /* set channel numbers */
    ch_i  = FFT_XFER_CHANNEL2;
    ch_o  = FFT_XFER_CHANNEL3;
    ch_i1 = FFT_XFER_CHANNEL0;
    ch_o1 = FFT_XFER_CHANNEL1;

    /* wait for EDMA */
    EdmaMgr_wait(edma->channel[ch_i]);

    for (j = 1; j < N1/num_of_lines; j++) {

#if (IFFT_SP_1D_C2C_NUMOFLINEBUFS>1)
      if (s_r3 == 0 && s_r5 == 0) {
        for (i=0; i<num_of_lines; i++) {
          ifft_sp_cmplx_notwid_br((double *)&input[i*N2*2], N2, (double *)&tempout[i*N2*2], (double *)tw1);
        }
      }
      else {
        for (i=0; i<num_of_lines; i++) {
          ifft_SPxSP_mixed_radix_c2c (N2, &input[i*N2*2], tw1, &tempout[i*N2*2], N_p2, s_r3, s_r5);
        }
      }
  
      DSPF_sp_mat_trans_cplx(tempout, num_of_lines, N2, output);
#else
      if (s_r3 == 0 && s_r5 == 0) {
        ifft_sp_cmplx_notwid_br((double *)input, N2, (double *)output, (double *)tw1);
      } else {
        ifft_SPxSP_mixed_radix_c2c (N2, input, tw1, output, N_p2, s_r3, s_r5);
      }
#endif 

      /* export output data, transposed */
      EdmaMgr_copy1D2D(
        edma->channel[ch_o],
        (void *)output,
        (void *)out_ptr,
        2*sizeof(float)*num_of_lines,
        outsample,
        linesize);
      out_ptr += 2*num_of_lines;

      /* prepare input data */
      EdmaMgr_copy1D1D(
        edma->channel[ch_i],
        (void *)in_ptr,
        (void *)input,
        insize);
      in_ptr += 2*N2*num_of_lines;

      /* swap pingpong buffer pointers and XFER channel */
      dum2   = input;
      input  = in1;
      in1    = dum2;
      dum3   = output;
      output = out1;
      out1   = dum3;

      dum0   = ch_i;
      ch_i   = ch_i1;
      ch_i1  = dum0;
      dum1   = ch_o;
      ch_o   = ch_o1;
      ch_o1  = dum1;

      /* wait for EDMA */
      EdmaMgr_wait(edma->channel[ch_i]);
      EdmaMgr_wait(edma->channel[ch_o]);
    }

    /* wait for stage1 to complete */
    EdmaMgr_wait(edma->channel[ch_i1]);
    EdmaMgr_wait(edma->channel[ch_o1]);

    return error;
} /* ifft_sp_2d_c2c_4step_ecpy */

/* ======================================================================== */
/*  End of file:  ifft_sp_1d_c2c.c                                          */
/* ------------------------------------------------------------------------ */
/*            Copyright (c) 2013 Texas Instruments, Incorporated.           */
/*                           All Rights Reserved.                           */
/* ======================================================================== */
