/* ======================================================================= */
/*  TEXAS INSTRUMENTS, INC.                                                */
/*                                                                         */
/*  FFTLIB  FFT Library                                                    */
/*                                                                         */
/* Copyright (C) 2013 Texas Instruments Incorporated - http://www.ti.com/  */ 
/*                                                                         */
/*                                                                         */
/*  Redistribution and use in source and binary forms, with or without     */
/*  modification, are permitted provided that the following conditions     */
/*  are met:                                                               */
/*                                                                         */
/*    Redistributions of source code must retain the above copyright       */
/*    notice, this list of conditions and the following disclaimer.        */
/*                                                                         */
/*    Redistributions in binary form must reproduce the above copyright    */
/*    notice, this list of conditions and the following disclaimer in the  */
/*    documentation and/or other materials provided with the               */
/*    distribution.                                                        */
/*                                                                         */
/*    Neither the name of Texas Instruments Incorporated nor the names of  */
/*    its contributors may be used to endorse or promote products derived  */
/*    from this software without specific prior written permission.        */
/*                                                                         */
/*  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS    */
/*  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT      */
/*  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR  */
/*  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT   */
/*  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,  */
/*  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT       */
/*  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,  */
/*  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY  */
/*  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT    */
/*  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE  */
/*  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.   */
/*                                                                         */
/* ======================================================================= */

#include <xdc/std.h>
#include "c6x.h"
#include "stdlib.h"
#include "ifft_dp_2d_c2c.h"
#include <ti/fftlib/src/common/ifft/ifft_dp_mixed_bs.h>
#include <ti/dsplib/src/DSPF_dp_ifftDPxDP/DSPF_dp_ifftDPxDP.h>
#include <ti/fftlib/src/common/fft_edma.h>

static void DSPF_dp_mat_trans_cplx(const double *restrict x, const int rows,
  const int cols, double *restrict y)
{
  int i, j = 0;

  #pragma MUST_ITERATE(2,,)
  for(i = 0; i < rows; i++)
    #pragma MUST_ITERATE(2,,)
    for(j = 0; j < cols; j++)
    {
      y[j*rows*2 + i*2] = x[j*2 + cols*i*2];
      y[j*rows*2 + i*2 + 1] = x[1 + j*2 + cols*i*2];
    }
}

int ifft_dp_1d_c2c_ecpy_transpose (
    int   N1,                /* FFT size                       */
    int   N2,                /* number of lines                */
    int   M,                 /* Padded size of Anysize FFT     */
    int   rad,               /* radix                          */
    int   N_p2,              /* power of 2 size in mixed radix */
    int   s_r3,              /* number stages of radix 3       */
    int   s_r5,              /* number stages of radix 5       */
    double *in,               /* input pointer                  */
    double *out,              /* output pointer                 */
    double *in_w,             /* input work buffer pointer      */
    double *out_w,            /* output working buffer pointer  */
    double *out_t,            /* temp working buffer pointer    */
    double *tw,               /* twiddle pointer                */
    double *Bn_w,
    double *workbuf_bn,
    FFT_EDMA_Struct  *edma,  /* edma resource pointer          */
    int num_of_lines
)
{
    int   error = FFT_NOERR;
    int   i, j,ch_i,ch_o,ch_i1,ch_o1,dum0,dum1;
    int   insize    = N1*2*sizeof(double)*num_of_lines,
          outsample = N1,
          linesize  = N2*2*sizeof(double);

    double *restrict in_ptr = in, *restrict out_ptr = out, *restrict tempout;
    double *restrict input, *restrict output, *restrict in1, *restrict out1, *dum2, *dum3;

    /* prepare first FFT input buffer */
    EdmaMgr_copy1D1D(
      edma->channel[FFT_XFER_CHANNEL0],
      (void *)in_ptr,
      (void *)in_w,
      insize);
    in_ptr += 2*N1*num_of_lines;

    /* set pingpong buffer pointers */
    in1    = in_w;
    out1   = out_w;
    input  = &in_w[2*N1*num_of_lines];
    output = &out_w[2*N1*num_of_lines];
    tempout= out_t;

    /* prepare second FFT input buffer */
    EdmaMgr_copy1D1D(
      edma->channel[FFT_XFER_CHANNEL2],
      (void *)in_ptr,
      (void *)input,
      insize);
    in_ptr += 2*N1*num_of_lines;

    /* wait for first FFT input */
    EdmaMgr_wait(edma->channel[FFT_XFER_CHANNEL0]);

    /* doing the 1D DP IFFT */
#if (IFFT_DP_2D_C2C_NUMOFLINEBUFS>1)
    if (N1 == M) {
      if (s_r3 == 0 && s_r5 == 0) {
        for (i=0; i<num_of_lines; i++)
          DSPF_dp_ifftDPxDP (N1, &in1[i*N1*2], tw, &tempout[i*N1*2], rad, 0, N1);
      }
      else {
        for (i=0; i<num_of_lines; i++)
          ifft_DPxDP_mixed_radix (N1, &in1[i*N1*2], tw, &tempout[i*N1*2], N_p2, s_r3, s_r5, rad);
      }
    }
    else{
      for (i=0; i<num_of_lines; i++)
        ifft_DPxDP_bs (N1, M, rad, &in1[i*N1*2], &tempout[i*N1*2], tw, Bn_w, workbuf_bn);
    }
    /* local transpose from tempout to out1 */
    DSPF_dp_mat_trans_cplx(tempout, num_of_lines, N1, out1);
#else
    if (N1 == M) {
      if (s_r3 == 0 && s_r5 == 0) {
    	  DSPF_dp_ifftDPxDP (N1, in1, tw, out1, rad, 0, N1);
      }
      else {
        ifft_DPxDP_mixed_radix (N1, in1, tw, out1, N_p2, s_r3, s_r5, rad);
      }
    }
    else{
      ifft_DPxDP_bs (N1, M, rad, in1, out1, tw, Bn_w, workbuf_bn);
    }
#endif

    /* export output data of first FFT*/

    EdmaMgr_copy1D2D(
      edma->channel[FFT_XFER_CHANNEL1],
      (void *)out1,
      (void *)out_ptr,
      2*sizeof(double)*num_of_lines,
      outsample,
      linesize);
    out_ptr += 2*num_of_lines;

    /* prepare third FFT input buffer */
    EdmaMgr_copy1D1D(
      edma->channel[FFT_XFER_CHANNEL0],
      (void *)in_ptr,
      (void *)in_w,
      insize);
    in_ptr += 2*N1*num_of_lines;

    /* set channel numbers */
    ch_i  = FFT_XFER_CHANNEL2;
    ch_o  = FFT_XFER_CHANNEL3;
    ch_i1 = FFT_XFER_CHANNEL0;
    ch_o1 = FFT_XFER_CHANNEL1;

	/* wait for EDMA */
    EdmaMgr_wait(edma->channel[ch_i]);

    _nassert (N2 >= 4);

    for (j = 1; j < N2/num_of_lines; j++)
    {
    	/* wait for EDMA */
//        EdmaMgr_wait(&edmaState.ecpyChan[ch_i]);

#if (IFFT_DP_2D_C2C_NUMOFLINEBUFS>1)
      if (N1 == M) {
        if (s_r3 == 0 && s_r5 == 0) {
          for (i=0; i<num_of_lines; i++)
            DSPF_dp_ifftDPxDP (N1, &input[i*N1*2], tw, &tempout[i*N1*2], rad, 0, N1);
        }
        else {
          for (i=0; i<num_of_lines; i++)
            ifft_DPxDP_mixed_radix (N1, &input[i*N1*2], tw, &tempout[i*N1*2], N_p2, s_r3, s_r5, rad);
        }
      }
      else{
        for (i=0; i<num_of_lines; i++)
          ifft_DPxDP_bs (N1, M, rad, &input[i*N1*2], &tempout[i*N1*2], tw, Bn_w, workbuf_bn);
      }
    
      /* local transpose for output */
      DSPF_dp_mat_trans_cplx(tempout, num_of_lines, N1, output);
#else
      if (N1 == M) {
        if (s_r3 == 0 && s_r5 == 0) {
          DSPF_dp_ifftDPxDP (N1, input, tw, output, rad, 0, N1);
        }
        else {
          ifft_DPxDP_mixed_radix (N1, input, tw, output, N_p2, s_r3, s_r5, rad);
        }
      }
      else{
        ifft_DPxDP_bs (N1, M, rad, input, output, tw, Bn_w, workbuf_bn);
      }
#endif

      /* export output data*/

      EdmaMgr_copy1D2D(
        edma->channel[ch_o],
        (void *)output,
        (void *)out_ptr,
        2*sizeof(double)*num_of_lines,
        outsample,
        linesize);
      out_ptr += 2*num_of_lines;

      /* prepare input data */
      EdmaMgr_copy1D1D(
        edma->channel[ch_i],
        (void *)in_ptr,
        (void *)input,
        insize);
      in_ptr += 2*N1*num_of_lines;

      /* swap pingpong buffer pointers and XFER channel */
      dum2   = input;
      input  = in1;
      in1    = dum2;
      dum3   = output;
      output = out1;
      out1   = dum3;

      dum0   = ch_i;
      ch_i   = ch_i1;
      ch_i1  = dum0;
      dum1   = ch_o;
      ch_o   = ch_o1;
      ch_o1  = dum1;

      /* wait for EDMA */
    EdmaMgr_wait(edma->channel[ch_i]);
    EdmaMgr_wait(edma->channel[ch_o]);
  }

  /* wait for EDMA */
  EdmaMgr_wait(edma->channel[ch_i1]);
  EdmaMgr_wait(edma->channel[ch_o1]);
  
    return error;
} /* ifft_dp_1d_c2c_batch_ecpy_transpose */

int ifft_dp_2d_c2c_ecpy (
    /* int   N1;            FFT size         */
    /* int   N2;            FFT size         */
    /* int   rad1           radix for N1     */
    /* int   rad2           radix for N2     */
    /* double *data;         data pointer     */
    /* double *workbuf;      work buffer pointer   */
    /* double *data_w;       data internal pointer    */
    /* double *workbuf_w;    working buffer internal pointer   */
    /* double *tw;           twiddle pointer  */
	fft_param_u u,
	void *edmaState
)
{
    /* input parameters */
    double *data       = u.dp_2d_c2c_e.data;        /* data pointer                   */
    double *workbuf    = u.dp_2d_c2c_e.workbuf;     /* output pointer                 */
    double *data_w     = u.dp_2d_c2c_e.data_w;      /* input working buffer pointer   */
    double *workbuf_w  = u.dp_2d_c2c_e.workbuf_w;   /* output working buffer pointer  */
    double *workbuf_t  = u.dp_2d_c2c_e.workbuf_t;   /* temp working buffer pointer    */
    double *tw_w       = u.dp_2d_c2c_e.tw_w;        /* twiddle working buffer pointer */
    double *tw         = u.dp_2d_c2c_e.tw;          /* twiddle buffer pointer         */
    double *Bn         = u.dp_2d_c2c_e.Bn;          /* Bn buffer pointer              */
    double *Bn_w       = u.dp_2d_c2c_e.Bn_w;        /* Bn working buffer pointer      */
    double *workbuf_bn = u.dp_2d_c2c_e.workbuf_bn;  /* workbuffer pointer for BS      */
    double *tw_a, *Bn_a;
    
    int   N1          = u.dp_2d_c2c_e.N1;           /* signal size                           */
    int   N2          = u.dp_2d_c2c_e.N2;           /* signal size                           */
    int   M1          = u.dp_2d_c2c_e.para1.M;      /* first dimension FFT size M1           */
    int   M2          = u.dp_2d_c2c_e.para2.M;      /* second dimension FFT size M2          */
    int   rad         = u.dp_2d_c2c_e.para1.rad;    /* radix for N1                          */
    
    int   twsize      = u.dp_2d_c2c_e.para1.twsize; /* twiddle size in double words          */
    int   N_p2        = u.dp_2d_c2c_e.para1.N_p2;   /* power of 2 size in mixed radix        */
    int   s_r3        = u.dp_2d_c2c_e.para1.s_r3;   /* number stages of radix 3              */
    int   s_r5        = u.dp_2d_c2c_e.para1.s_r5;   /* number stages of radix 5              */
    int   Bnsize      = u.dp_2d_c2c_e.para1.Bnsize;
    int   tw_offset, Bn_offset;
    
    FFT_EDMA_Struct  *edma = (FFT_EDMA_Struct *)edmaState;

    int   error = FFT_NOERR, numLine = IFFT_DP_2D_C2C_NUMOFLINEBUFS;

    if (tw != NULL) {
      /* prepare twiddle buffer */
      EdmaMgr_copy1D1D(
        edma->channel[FFT_XFER_CHANNEL3],
        (void *)tw,
        (void *)tw_w,
        twsize*sizeof(double));
      tw_a = tw_w;
    }
    else {
      tw_a = tw;
    }
   
    if (Bn_w != NULL) {
      /* prepare Bn buffer */
      EdmaMgr_copy1D1D(
        edma->channel[FFT_XFER_CHANNEL2],
        (void *)Bn,
        (void *)Bn_w,
        Bnsize*sizeof(double));
      Bn_a = Bn_w;
    } else {
      Bn_a = Bn;
    }
    
    if (tw_w != NULL)
    	EdmaMgr_wait(edma->channel[FFT_XFER_CHANNEL3]);
    if (Bn_w != NULL)
    	EdmaMgr_wait(edma->channel[FFT_XFER_CHANNEL2]);

    /* 1st dimension fft */
    error = ifft_dp_1d_c2c_ecpy_transpose (N1, N2, M1, rad, N_p2, s_r3, s_r5, data, workbuf,
                                          data_w, workbuf_w, workbuf_t, tw_a, Bn_a, 
                                          workbuf_bn, edma, numLine);
                                                                     
    /* prepare for the 2nd dimension */
    twsize = u.dp_2d_c2c_e.para2.twsize;
    Bnsize = u.dp_2d_c2c_e.para2.Bnsize;
    N_p2   = u.dp_2d_c2c_e.para2.N_p2;    /* power of 2 size in mixed radix        */
    s_r3   = u.dp_2d_c2c_e.para2.s_r3;    /* number stages of radix 3              */
    s_r5   = u.dp_2d_c2c_e.para2.s_r5;    /* number stages of radix 5              */
    rad    = u.dp_2d_c2c_e.para2.rad;
    tw_offset = u.dp_2d_c2c_e.para1.twsize;
    Bn_offset = u.dp_2d_c2c_e.para1.Bnsize;    
                                          
    if (tw != NULL) {
      /* prepare twiddle buffer */
      EdmaMgr_copy1D1D(
        edma->channel[FFT_XFER_CHANNEL3],
        (void *)(tw+tw_offset),
        (void *)tw_w,
        twsize*sizeof(double));
      tw_a = tw_w;
    }
    else {
      tw_a = tw + tw_offset;
    }

    if (Bn_w != NULL) {
      /* prepare Bn buffer when doing bluestein */
      EdmaMgr_copy1D1D(
        edma->channel[FFT_XFER_CHANNEL2],
        (void *)(Bn+Bn_offset),
        (void *)Bn_w,
        Bnsize*sizeof(double));
      Bn_a = Bn_w;
    }
    else {
      Bn_a = Bn + Bn_offset;
    }
    if (tw_w != NULL)
    	EdmaMgr_wait(edma->channel[FFT_XFER_CHANNEL3]);
    if (Bn_w != NULL)
    	EdmaMgr_wait(edma->channel[FFT_XFER_CHANNEL2]);  
    /* 2nd dimension fft */
    error = ifft_dp_1d_c2c_ecpy_transpose (N2, N1, M2, rad, N_p2, s_r3, s_r5, workbuf, data,
                                          workbuf_w, data_w, workbuf_t, tw_a, Bn_a, 
                                          workbuf_bn, edma, numLine);

    return error;
} /* ifft_dp_2d_c2c_ecpy */

/* ======================================================================== */
/*  End of file:  ifft_dp_2d_c2c.c                                           */
/* ------------------------------------------------------------------------ */
/*            Copyright (c) 2013 Texas Instruments, Incorporated.           */
/*                           All Rights Reserved.                           */
/* ======================================================================== */
