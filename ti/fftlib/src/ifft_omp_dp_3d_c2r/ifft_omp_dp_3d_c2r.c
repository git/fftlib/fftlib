/* ======================================================================= */
/*  TEXAS INSTRUMENTS, INC.                                                */
/*                                                                         */
/*  FFTLIB  FFT Library                                                    */
/*                                                                         */
/* Copyright (C) 2013 Texas Instruments Incorporated - http://www.ti.com/  */
/*                                                                         */
/*                                                                         */
/*  Redistribution and use in source and binary forms, with or without     */
/*  modification, are permitted provided that the following conditions     */
/*  are met:                                                               */
/*                                                                         */
/*    Redistributions of source code must retain the above copyright       */
/*    notice, this list of conditions and the following disclaimer.        */
/*                                                                         */
/*    Redistributions in binary form must reproduce the above copyright    */
/*    notice, this list of conditions and the following disclaimer in the  */
/*    documentation and/or other materials provided with the               */
/*    distribution.                                                        */
/*                                                                         */
/*    Neither the name of Texas Instruments Incorporated nor the names of  */
/*    its contributors may be used to endorse or promote products derived  */
/*    from this software without specific prior written permission.        */
/*                                                                         */
/*  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS    */
/*  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT      */
/*  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR  */
/*  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT   */
/*  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,  */
/*  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT       */
/*  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,  */
/*  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY  */
/*  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT    */
/*  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE  */
/*  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.   */
/*                                                                         */
/* ======================================================================= */

#include <xdc/std.h>
#include "c6x.h"
#include "stdlib.h"
#include <stdio.h>
#include <ti/fftlib/src/common/omp/omp_config.h>
#include "ifft_omp_dp_3d_c2r.h"
#include <ti/fftlib/src/common/ifft/ifft_dp_mixed_bs.h>
#include <ti/dsplib/src/DSPF_dp_ifftDPxDP/DSPF_dp_ifftDPxDP.h>
#include <ti/fftlib/src/common/fft_edma.h>
#include <ti/dsplib/src/DSPF_dp_mat_trans/DSPF_dp_mat_trans.h>
#include <ti/runtime/openmp/omp.h>
#include <ti/libarch/libarch.h>

static void DSPF_dp_mat_trans_cplx(const double *restrict x, const int rows,
  const int cols, double *restrict y)
{
  int i, j = 0;

  #pragma MUST_ITERATE(2,,)
  for(i = 0; i < rows; i++)
  #pragma MUST_ITERATE(2,,)
  for(j = 0; j < cols; j++)
  {
    y[j*rows*2 + i*2] = x[j*2 + cols*i*2];
    y[j*rows*2 + i*2 + 1] = x[1 + j*2 + cols*i*2];
  }
}

/* used for the 2nd dimension ifft */
static int ifft_omp_dp_1d_c2c_batch_ecpy_transpose (
    int    N1,                /* IFFT size                      */
    int    N2,                /* number of lines                */
    int    N3,                /* number of planes               */
    int    M,                 /* FFT size                       */
    int    rad,               /* radix for N1                   */
    int    N_p2,              /* power of 2 size in mixed radix */
    int    s_r3,              /* number stages of radix 3       */
    int    s_r5,              /* number stages of radix 5       */
    double *in,               /* input pointer                  */
    double *out,              /* output pointer                 */
    double *in_w,             /* input work buffer pointer      */
    double *out_w,            /* output working buffer pointer  */
    double *out_t,            /* temp working buffer pointer    */
    double *tw,               /* twiddle pointer                */
    double *Bn,               /* Bn pointer                     */
    double *workbuf,          /* workbuf pointer for BS         */
    FFT_EDMA_Struct  *edma,   /* edma resource pointer          */
    int num_of_lines          /* number of lines for the 1D FFT */
)
{
    int   error = FFT_NOERR, linecnt = 0;
    int   i,j,ch_i,ch_o,ch_i1,ch_o1,dum0,dum1;
    int   instep,
          outsample = N1,
          linesize, linewrap;
    FFT_EDMA_Struct  *edmall2;

    double *restrict in_ptr = in, *restrict out_ptr = out, *restrict tempout;
    double *restrict input, *restrict output, *restrict in1, *restrict out1, *dum2, *dum3;

    edmall2 = edma;

    /* parameters used for output EDMA */
    linesize   = N2*2*sizeof(double);
    linewrap   = 2*(N1-1)*N2;
    instep = 2*num_of_lines;

    /* set pingpong buffer pointers */
    in1    = in_w;
    out1   = out_w;
    input  = &in_w[2*num_of_lines*N1];
    output = &out_w[2*num_of_lines*N1];
    tempout= out_t;

    /* prepare first IFFT input buffer */

    lib_emt_copy2D1D(
      edmall2->channel[FFT_XFER_CHANNEL0],
      (void *)in_ptr,
      (void *)in_w,
      2*sizeof(double)*num_of_lines,
      outsample,
      linesize);
    in_ptr        += instep;

    /* prepare second IFFT input buffer */

    lib_emt_copy2D1D(
      edmall2->channel[FFT_XFER_CHANNEL2],
      (void *)in_ptr,
      (void *)input,
      2*sizeof(double)*num_of_lines,
      outsample,
      linesize);

    in_ptr += instep;

    /* wait for first IFFT input */
    lib_emt_wait(edmall2->channel[FFT_XFER_CHANNEL0]);

    /* call c2c ifft */
#if (IFFT_OMP_DP_3D_C2R_NUMOFLINEBUFS>1)

    /* local transpose from in to out */
    DSPF_dp_mat_trans_cplx(in1, N1, num_of_lines, out1);

    if (N1 == M) {
      if (s_r3 == 0 && s_r5 == 0) {
        for (i=0; i<num_of_lines; i++)
          DSPF_dp_ifftDPxDP (N1, &out1[i*N1*2], tw, &tempout[i*N1*2], rad, 0, N1);
      }
      else {
        for (i=0; i<num_of_lines; i++)
          ifft_DPxDP_mixed_radix (N1, &out1[i*N1*2], tw, &tempout[i*N1*2], N_p2, s_r3, s_r5, rad);
      }
    }
    else{
      for (i=0; i<num_of_lines; i++)
        ifft_DPxDP_bs (N1, M, rad, &out1[i*N1*2], &tempout[i*N1*2], tw, Bn, workbuf);
    }
    /* local transpose from tempout to out1 */
    DSPF_dp_mat_trans_cplx(tempout, num_of_lines, N1, out1);

#else
    if (N1 == M) {
      if (s_r3 == 0 && s_r5 == 0) {
        DSPF_dp_ifftDPxDP (N1, in1, tw, out1, rad, 0, N1);
      }
      else {
        ifft_DPxDP_mixed_radix (N1, in1, tw, out1, N_p2, s_r3, s_r5, rad);
      }
    }
    else{
      ifft_DPxDP_bs (N1, M, rad, in1, out1, tw, Bn, workbuf);
    }

#endif

    /* export output data of first IFFT*/
      lib_emt_copy1D2D(
      edmall2->channel[FFT_XFER_CHANNEL1],
      (void *)out1,
      (void *)out_ptr,
      2*sizeof(double)*num_of_lines,
      outsample,
      linesize);

      out_ptr += 2*num_of_lines;
      linecnt += 1*num_of_lines;

#ifdef USEFASTEDMA
  /* fill the PaRAM for CHANNEL 3 without starting it */
  lib_emt_copy1D2D_Fill(
    edmall2->channel[FFT_XFER_CHANNEL3],
    (void *)out1,
    (void *)out_ptr,
    2*sizeof(double)*num_of_lines,
    outsample,
    linesize);
#endif

    /* prepare third IFFT input buffer */
#ifndef USEFASTEDMA

  lib_emt_copy2D1D(
    edmall2->channel[FFT_XFER_CHANNEL0],
    (void *)in_ptr,
    (void *)in_w,
    2*sizeof(double)*num_of_lines,
    outsample,
    linesize);

#else
  lib_emt_copy1D1D_Fast(
    edmall2->channel[FFT_XFER_CHANNEL0],
    (void *)in_ptr,
    (void *)in_w);
#endif
    in_ptr  += instep;

    /* set channel numbers */
    ch_i  = FFT_XFER_CHANNEL2;
    ch_o  = FFT_XFER_CHANNEL3;
    ch_i1 = FFT_XFER_CHANNEL0;
    ch_o1 = FFT_XFER_CHANNEL1;

    /* wait for EDMA */
    lib_emt_wait(edmall2->channel[ch_i]);

    for (j = 1; j < (N3*(N2-1))/num_of_lines; j++) {

      /* call c2c ifft */
#if (IFFT_OMP_DP_3D_C2R_NUMOFLINEBUFS>1)
      /* local transpose from in to out */
      DSPF_dp_mat_trans_cplx(input, N1, num_of_lines, output);

      if (N1 == M) {
        if (s_r3 == 0 && s_r5 == 0) {
          for (i=0; i<num_of_lines; i++)
            DSPF_dp_ifftDPxDP (N1, &output[i*N1*2], tw, &tempout[i*N1*2], rad, 0, N1);
        }
        else {
          for (i=0; i<num_of_lines; i++)
            ifft_DPxDP_mixed_radix (N1, &output[i*N1*2], tw, &tempout[i*N1*2], N_p2, s_r3, s_r5, rad);
        }
      }
      else{
        for (i=0; i<num_of_lines; i++)
          ifft_DPxDP_bs (N1, M, rad, &output[i*N1*2], &tempout[i*N1*2], tw, Bn, workbuf);
      }
      /* local transpose for output */
      DSPF_dp_mat_trans_cplx(tempout, num_of_lines, N1, output);

#else
      if (N1 == M) {
        if (s_r3 == 0 && s_r5 == 0) {
          DSPF_dp_ifftDPxDP (N1, input, tw, output, rad, 0, N1);
        }
        else {
          ifft_DPxDP_mixed_radix (N1, input, tw, output, N_p2, s_r3, s_r5, rad);
        }
      }
      else{
        ifft_DPxDP_bs (N1, M, rad, input, output, tw, Bn, workbuf);
      }

#endif

      /* export output data*/
#ifndef USEFASTEDMA
        lib_emt_copy1D2D(
        edmall2->channel[ch_o],
        (void *)output,
        (void *)out_ptr,
        2*sizeof(double)*num_of_lines,
        outsample,
        linesize);
#else
      lib_emt_copy1D2D_Fast (
        edmall2->channel[ch_o],
          (void *)output,
          (void *)out_ptr);
#endif
      out_ptr += 2*num_of_lines;
      linecnt += 1*num_of_lines;

      if (linecnt >= (N2 - 1)){
        linecnt = 0;
        out_ptr += 2 + linewrap;
     }
      else{
        /* re-adjust output pointer when it's time for the next batch */
        if (linecnt >= (N2-1-num_of_lines)) {
            /* get input */
            for (i = 0; i < outsample; i++) {
              input[2*i]   = in_ptr[2*i*N2];
              input[2*i+1] = in_ptr[2*i*N2+1];
            }

          /* prepare the FFT input buffer */
          /* process data */
          if (N1 == M) {
            if (s_r3 == 0 && s_r5 == 0) {
              DSPF_dp_ifftDPxDP (N1, input, tw, tempout, rad, 0, N1);
            }
            else
              ifft_DPxDP_mixed_radix (N1, input, tw, tempout, N_p2, s_r3, s_r5, rad);
          } /* if (N1 == M)*/
          else
            ifft_DPxDP_bs (N1, M, rad, input, tempout, tw, Bn, workbuf);

          /* transport out */
          for (i = 0; i < outsample; i++) {
            out_ptr[2*i*N2+2*num_of_lines]   = tempout[2*i];
            out_ptr[2*i*N2+2*num_of_lines+1] = tempout[2*i+1];
          }
          in_ptr  += 2 + linewrap;
        }
      }

      /* prepare input data */
#ifndef USEFASTEDMA

      lib_emt_copy2D1D(
        edmall2->channel[ch_i],
        (void *)in_ptr,
        (void *)input,
        2*sizeof(double)*num_of_lines,
        outsample,
        linesize);
#else
      lib_emt_copy1D1D_Fast(
        edmall2->channel[ch_i],
        (void *)in_ptr,
        (void *)input);
#endif
      in_ptr        += instep;

      /* swap pingpong buffer pointers and XFER channel */
      dum2   = input;
      input  = in1;
      in1    = dum2;
      dum3   = output;
      output = out1;
      out1   = dum3;

      dum0   = ch_i;
      ch_i   = ch_i1;
      ch_i1  = dum0;
      dum1   = ch_o;
      ch_o   = ch_o1;
      ch_o1  = dum1;

      /* wait for EDMA */
      lib_emt_wait(edmall2->channel[ch_i]);
      lib_emt_wait(edmall2->channel[ch_o]);
    }

    lib_emt_wait(edmall2->channel[ch_i1]);
    lib_emt_wait(edmall2->channel[ch_o1]);

    return error;
} /* fft_omp_dp_1d_c2c_batch_ecpy_transpose */

/* used for the 1st dimension ifft */
static int ifft_omp_dp_1d_c2r_batch_ecpy_transpose (
    int    N1,                /* IFFT size                      */
    int    N2,                /* number of lines                */
    int    N3,                /* number of planes               */
    int    M,                 /* FFT size                       */
    int    rad,               /* radix for N1                   */
    int    N_p2,              /* power of 2 size in mixed radix */
    int    s_r3,              /* number stages of radix 3       */
    int    s_r5,              /* number stages of radix 5       */
    double *in,               /* input pointer                  */
    double *out,              /* output pointer                 */
    double *in_w,             /* input work buffer pointer      */
    double *out_w,            /* output working buffer pointer  */
    double *out_t,            /* temp working buffer pointer    */
    double *tw,               /* twiddle pointer                */
    double *Bn,               /* Bn pointer                     */
    double *workbuf,          /* workbuf pointer for BS         */
    FFT_EDMA_Struct  *edma,   /* edma resource pointer          */
    int num_of_lines          /* number of lines for the 1D FFT */
)
{
    int   error = FFT_NOERR;
    int   i,j,k,ch_i,ch_o,ch_i1,ch_o1,dum0,dum1;
    int   insize, instep,
          outsize, outstep;
    FFT_EDMA_Struct  *edmall2;

    double *restrict in_ptr = in, *restrict out_ptr = out, *restrict tempout;
    double *restrict input, *restrict output, *restrict in1, *restrict out1, *dum2, *dum3;

    edmall2 = edma;
  
    /* parameters used for output EDMA */
    insize  = 2*(N1/2+1)*sizeof(double)*num_of_lines;
    instep  = 2*(N1/2+1)*num_of_lines;
    outsize = N1*sizeof(double)*num_of_lines;
    outstep = N1*num_of_lines;

    /* prepare first IFFT input buffer */
    lib_emt_copy1D1D(
      edmall2->channel[FFT_XFER_CHANNEL0],
      (void *)in_ptr,
      (void *)in_w,
      insize);
    in_ptr += instep;

    /* set pingpong buffer pointers */
    in1    = in_w;
    out1   = out_w;
    input  = &in_w[2*N1*num_of_lines];
    output = &out_w[2*N1*num_of_lines];
    tempout= out_t;

    /* prepare second IFFT input buffer */
    lib_emt_copy1D1D(
      edmall2->channel[FFT_XFER_CHANNEL2],
      (void *)in_ptr,
      (void *)input,
      insize);
    in_ptr += instep;

    /* wait for first IFFT input */
    lib_emt_wait(edmall2->channel[FFT_XFER_CHANNEL0]);

    /* call c2r ifft */
#if (IFFT_OMP_DP_3D_C2R_NUMOFLINEBUFS>1)
    /* reconstruct the signal based on the half spectrum */
    for (i = 0; i < num_of_lines; i++){
      for(j = 0; j < N1/2+1; j++){
        tempout[2*N1*i+2*j]   = in1[2*(N1/2+1)*i+2*j];
        tempout[2*N1*i+2*j+1] = in1[2*(N1/2+1)*i+2*j+1];
      }
    }

    for (i = 0; i < num_of_lines; i++){
      for(j = 1; j < N1/2; j++){
        tempout[2*N1*(i+1)-2*j]   =  tempout[2*N1*i+2*j];
        tempout[2*N1*(i+1)-2*j+1] = -tempout[2*N1*i+2*j+1];
      }
    }
    if (N1 == M) {
      if (s_r3 == 0 && s_r5 == 0) {
        for (i=0; i<num_of_lines; i++)
          DSPF_dp_ifftDPxDP_c2r (N1, &tempout[i*N1*2], tw, &out1[i*N1], rad, 0, N1);
      }
      else {
        for (i=0; i<num_of_lines; i++)
          ifft_DPxDP_mixed_radix_c2r (N1, &tempout[i*N1*2], tw, &out1[i*N1], N_p2, s_r3, s_r5, rad);
      }
    }
    else{
      for (i=0; i<num_of_lines; i++)
        ifft_DPxDP_c2r_bs (N1, M, rad, &tempout[i*N1*2], &out1[i*N1], tw, Bn, workbuf);
    }

#else
    if (N1 == M) {
      if (s_r3 == 0 && s_r5 == 0) {
        DSPF_dp_ifftDPxDP_c2r (N1, in1, tw, out1, rad, 0, N1);
      }
      else {
        ifft_DPxDP_mixed_radix_c2r (N1, in1, tw, out1, N_p2, s_r3, s_r5, rad);
      }
    }
    else{
      ifft_DPxDP_c2r_bs (N1, M, rad, in1, out1, tw, Bn, workbuf);
    }
#endif

    /* export output data of first IFFT*/
    lib_emt_copy1D1D(
      edmall2->channel[FFT_XFER_CHANNEL1],
      (void *)out1,
      (void *)out_ptr,
      outsize);

      out_ptr += outstep;
    
#ifdef USEFASTEDMA
  /* fill the PaRAM for CHANNEL 3 without starting it */
  lib_emt_copy1D2D_Fill(
    edmall2->channel[FFT_XFER_CHANNEL3],
    (void *)out1,
    (void *)out_ptr,
    2*sizeof(double)*num_of_lines,
    outsample,
    linesize);
#endif

    /* prepare third IFFT input buffer */
#ifndef USEFASTEDMA
    lib_emt_copy1D1D(
      edmall2->channel[FFT_XFER_CHANNEL0],
      (void *)in_ptr,
      (void *)in_w,
      insize);
#else
  lib_emt_copy1D1D_Fast(
    edmall2->channel[FFT_XFER_CHANNEL0],
    (void *)in_ptr,
    (void *)in_w);
#endif
    in_ptr += instep;

    /* set channel numbers */
    ch_i  = FFT_XFER_CHANNEL2;
    ch_o  = FFT_XFER_CHANNEL3;
    ch_i1 = FFT_XFER_CHANNEL0;
    ch_o1 = FFT_XFER_CHANNEL1;

    /* wait for EDMA */
    lib_emt_wait(edmall2->channel[ch_i]);

    for (k = 1; k < (N3*N2)/num_of_lines; k++) {

      /* call c2r ifft */
#if (IFFT_OMP_DP_3D_C2R_NUMOFLINEBUFS>1)
      /* reconstruct the signal based on the half spectrum */
      for (i = 0; i < num_of_lines; i++){
        for(j = 0; j < N1/2+1; j++){
          tempout[2*N1*i+2*j]   = input[2*(N1/2+1)*i+2*j];
          tempout[2*N1*i+2*j+1] = input[2*(N1/2+1)*i+2*j+1];
        }
      }

      for (i = 0; i < num_of_lines; i++){
        for(j = 1; j < N1/2; j++){
          tempout[2*N1*(i+1)-2*j]   =  tempout[2*N1*i+2*j];
          tempout[2*N1*(i+1)-2*j+1] = -tempout[2*N1*i+2*j+1];
        }
      }

      if (N1 == M) {
        if (s_r3 == 0 && s_r5 == 0) {
          for (i=0; i<num_of_lines; i++)
            DSPF_dp_ifftDPxDP_c2r (N1, &tempout[i*N1*2], tw, &output[i*N1], rad, 0, N1);
        }
        else {
          for (i=0; i<num_of_lines; i++)
            ifft_DPxDP_mixed_radix_c2r (N1, &tempout[i*N1*2], tw, &output[i*N1], N_p2, s_r3, s_r5, rad);
        }
      }
      else{
        for (i=0; i<num_of_lines; i++)
          ifft_DPxDP_c2r_bs (N1, M, rad, &tempout[i*N1*2], &output[i*N1], tw, Bn, workbuf);
      }

#else
      if (N1 == M) {
        if (s_r3 == 0 && s_r5 == 0) {
          DSPF_dp_ifftDPxDP_c2r (N1, input, tw, output, rad, 0, N1);
        }
        else {
          ifft_DPxDP_mixed_radix_c2r (N1, input, tw, output, N_p2, s_r3, s_r5, rad);
        }
      }
      else{
        ifft_DPxDP_c2r_bs (N1, M, rad, input, output, tw, Bn, workbuf);
      }
#endif

      /* export output data*/
#ifndef USEFASTEDMA
      lib_emt_copy1D1D(
        edmall2->channel[ch_o],
        (void *)output,
        (void *)out_ptr,
        outsize);

#else
      lib_emt_copy1D2D_Fast (
        edmall2->channel[ch_o],
          (void *)output,
          (void *)out_ptr);
#endif
      out_ptr += outstep;

      /* prepare input data */
#ifndef USEFASTEDMA
      lib_emt_copy1D1D(
        edmall2->channel[ch_i],
        (void *)in_ptr,
        (void *)input,
        insize);

#else
      lib_emt_copy1D1D_Fast(
        edmall2->channel[ch_i],
        (void *)in_ptr,
        (void *)input);
#endif
      in_ptr += instep;

      /* swap pingpong buffer pointers and XFER channel */
      dum2   = input;
      input  = in1;
      in1    = dum2;
      dum3   = output;
      output = out1;
      out1   = dum3;

      dum0   = ch_i;
      ch_i   = ch_i1;
      ch_i1  = dum0;
      dum1   = ch_o;
      ch_o   = ch_o1;
      ch_o1  = dum1;

      /* wait for EDMA */
      lib_emt_wait(edmall2->channel[ch_i]);
      lib_emt_wait(edmall2->channel[ch_o]);
    }

    lib_emt_wait(edmall2->channel[ch_i1]);
    lib_emt_wait(edmall2->channel[ch_o1]);

    return error;
} /* fft_omp_dp_1d_c2r_batch_ecpy_transpose */


static int ifft_omp_dp_1d_c2c_batch_ecpy_transpose_3d (
    int    N1,                /* IFFT size                      */
    int    N3,                /* number of planes               */
    int    lines,             /* lines to execute               */
    int    M,                 /* FFT size                       */
    int    rad,               /* radix for N1                   */
    int    N_p2,              /* power of 2 size in mixed radix */
    int    s_r3,              /* number stages of radix 3       */
    int    s_r5,              /* number stages of radix 5       */
    double *in,               /* input pointer                  */
    double *out,              /* output pointer                 */
    double *in_w,             /* input work buffer pointer      */
    double *out_w,            /* output working buffer pointer  */
    double *out_t,            /* temp working buffer pointer    */
    double *tw,               /* twiddle pointer                */
    double *Bn,               /* Bn pointer                     */
    double *workbuf,          /* workbuf pointer for BS         */
    FFT_EDMA_Struct  *edma,   /* edma resource pointer          */
    int num_of_lines          /* number of lines for the 1D FFT */
)
{
    int   error = FFT_NOERR;
    int   i,j,ch_i,ch_o,ch_i1,ch_o1,dum0,dum1;
    int   outsample = N3, linesize = 2*N1*sizeof(double);
    FFT_EDMA_Struct  *edmall2 = edma;

    double *restrict in_ptr = in, *restrict out_ptr = out, *restrict tempout;
    double *restrict input, *restrict output, *restrict in1, *restrict out1, *dum2, *dum3;

    /* prepare first FFT input buffer */
    lib_emt_copy2D1DLarge(
      edmall2->channel[FFT_XFER_CHANNEL0],
      (void *)in_ptr,
      (void *)in_w,
      2*sizeof(double)*num_of_lines,
      outsample,
      linesize);
    in_ptr += 2*num_of_lines;

    /* set pingpong buffer pointers */
    in1    = in_w;
    out1   = out_w;
    input  = &in_w[2*N3*num_of_lines];
    output = &out_w[2*N3*num_of_lines];
    tempout= out_t;

    /* prepare second FFT input buffer */
    lib_emt_copy2D1DLarge(
      edmall2->channel[FFT_XFER_CHANNEL2],
      (void *)in_ptr,
      (void *)input,
      2*sizeof(double)*num_of_lines,
      outsample,
      linesize);
    in_ptr += 2*num_of_lines;

    /* wait for first IFFT input */
    lib_emt_wait(edmall2->channel[FFT_XFER_CHANNEL0]);

    /* call c2r ifft */
#if (IFFT_OMP_DP_3D_C2R_NUMOFLINEBUFS>1)
    DSPF_dp_mat_trans_cplx(in1, N3, num_of_lines, out1);
    if (N3 == M) {
      if (s_r3 == 0 && s_r5 == 0) {
        for (i=0; i<num_of_lines; i++)
          DSPF_dp_ifftDPxDP (N3, &out1[i*N3*2], tw, &tempout[2*i*N3], rad, 0, N3);
      }
      else {
        for (i=0; i<num_of_lines; i++)
          ifft_DPxDP_mixed_radix (N3, &out1[i*N3*2], tw, &tempout[2*i*N3], N_p2, s_r3, s_r5, rad);
      }
    }
    else{
      for (i=0; i<num_of_lines; i++)
        ifft_DPxDP_bs (N3, M, rad, &out1[i*N3*2], &tempout[2*i*N3], tw, Bn, workbuf);
    }
    /* local transpose from tempout to out1 */
    DSPF_dp_mat_trans_cplx(tempout, num_of_lines, N3, out1);
#else
    if (N3 == M) {
      if (s_r3 == 0 && s_r5 == 0) {
        DSPF_dp_ifftDPxDP (N3, in1, tw, out1, rad, 0, N3);
      }
      else {
        ifft_DPxDP_mixed_radix (N3, in1, tw, out1, N_p2, s_r3, s_r5, rad);
      }
    }
    else{
      ifft_DPxDP_bs (N3, M, rad, in1, out1, tw, Bn, workbuf);
    }
#endif

    /* export output data of first IFFT*/
    lib_emt_copy1D2DLarge(
      edmall2->channel[FFT_XFER_CHANNEL1],
      (void *)out1,
      (void *)out_ptr,
      2*sizeof(double)*num_of_lines,
      outsample,
      linesize);

    out_ptr += 2*num_of_lines;


#ifdef USEFASTEDMA
  /* fill the PaRAM for CHANNEL 3 without starting it */
  lib_emt_copy1D2D_Fill(
    edmall2->channel[FFT_XFER_CHANNEL3],
    (void *)out1,
    (void *)out_ptr,
    sizeof(double)*num_of_lines,
    outsample,
    linesize);
#endif

    /* prepare third IFFT input buffer */
#ifndef USEFASTEDMA
    lib_emt_copy2D1DLarge(
      edmall2->channel[FFT_XFER_CHANNEL0],
      (void *)in_ptr,
      (void *)in_w,
      2*sizeof(double)*num_of_lines,
      outsample,
      linesize);
#else
  lib_emt_copy1D1D_Fast(
    edmall2->channel[FFT_XFER_CHANNEL0],
    (void *)in_ptr,
    (void *)in_w);
#endif
    in_ptr += 2*num_of_lines;

    /* set channel numbers */
    ch_i  = FFT_XFER_CHANNEL2;
    ch_o  = FFT_XFER_CHANNEL3;
    ch_i1 = FFT_XFER_CHANNEL0;
    ch_o1 = FFT_XFER_CHANNEL1;

    /* wait for EDMA */
    lib_emt_wait(edmall2->channel[ch_i]);

    for (j = num_of_lines; j < lines; j += num_of_lines){

      /* call c2r ifft */
#if (IFFT_OMP_DP_3D_C2R_NUMOFLINEBUFS>1)
      /* local transpose */
      DSPF_dp_mat_trans_cplx(input, N3, num_of_lines, output);
      if (N3 == M) {
        if (s_r3 == 0 && s_r5 == 0) {
          for (i=0; i<num_of_lines; i++)
            DSPF_dp_ifftDPxDP (N3, &output[i*N3*2], tw, &tempout[2*i*N3], rad, 0, N3);
        }
        else {
          for (i=0; i<num_of_lines; i++)
            ifft_DPxDP_mixed_radix (N3, &output[i*N3*2], tw, &tempout[2*i*N3], N_p2, s_r3, s_r5, rad);
        }
      }
      else{
        for (i=0; i<num_of_lines; i++)
          ifft_DPxDP_bs (N3, M, rad, &output[i*N3*2], &tempout[2*i*N3], tw, Bn, workbuf);
      }

    /* local transpose for output */
      DSPF_dp_mat_trans_cplx(tempout, num_of_lines, N3, output);
#else
      if (N3 == M) {
        if (s_r3 == 0 && s_r5 == 0) {
          DSPF_dp_ifftDPxDP (N3, input, tw, output, rad, 0, N3);
        }
        else {
          ifft_DPxDP_mixed_radix (N3, input, tw, output, N_p2, s_r3, s_r5, rad);
        }
      }
      else{
        ifft_DPxDP_bs (N3, M, rad, input, output, tw, Bn, workbuf);
      }
#endif

      /* export output data*/
#ifndef USEFASTEDMA
      lib_emt_copy1D2DLarge(
        edmall2->channel[ch_o],
        (void *)output,
        (void *)out_ptr,
        2*sizeof(double)*num_of_lines,
        outsample,
        linesize);
#else
      lib_emt_copy1D2D_Fast (
        edmall2->channel[ch_o],
          (void *)output,
          (void *)out_ptr);
#endif
      out_ptr += 2*num_of_lines;

      /* prepare input data */
#ifndef USEFASTEDMA
      lib_emt_copy2D1DLarge(
        edmall2->channel[ch_i],
        (void *)in_ptr,
        (void *)input,
        2*sizeof(double)*num_of_lines,
        outsample,
        linesize);

#else
      lib_emt_copy1D1D_Fast(
        edmall2->channel[ch_i],
        (void *)in_ptr,
        (void *)input);
#endif
      in_ptr += 2*num_of_lines;

      /* swap pingpong buffer pointers and XFER channel */
      dum2   = input;
      input  = in1;
      in1    = dum2;
      dum3   = output;
      output = out1;
      out1   = dum3;

      dum0   = ch_i;
      ch_i   = ch_i1;
      ch_i1  = dum0;
      dum1   = ch_o;
      ch_o   = ch_o1;
      ch_o1  = dum1;

      /* wait for EDMA */
      lib_emt_wait(edmall2->channel[ch_i]);
      lib_emt_wait(edmall2->channel[ch_o]);
    }

    lib_emt_wait(edmall2->channel[ch_i1]);
    lib_emt_wait(edmall2->channel[ch_o1]);

    return error;
} /* ifft_dp_1d_c2r_ecpy_transpose */

static int radLocal, twsizeLocal, BnsizeLocal;
static double *inLocal, *outLocal, *stage1Local, *stage2Local, *twLocal, *in_wLocal, *out_wLocal, *out_tLocal, *tw_wLocal;
static double *workLocal, *BnLocal, *Bn_wLocal;
static int n1Local, n2Local, n3Local, planesLocal, mLocal;
static int N_p2Local, s_r3Local, s_r5Local;
static FFT_EDMA_Struct *edmaLocal;
#ifndef FFT_MEM_MODEL_SM
#pragma DATA_SECTION (radLocal, ".mem_msm")
#pragma DATA_SECTION (twsizeLocal, ".mem_msm")
#pragma DATA_SECTION (BnsizeLocal, ".mem_msm")
#pragma DATA_SECTION (inLocal, ".mem_msm")
#pragma DATA_SECTION (outLocal, ".mem_msm")
#pragma DATA_SECTION (stage1Local, ".mem_msm")
#pragma DATA_SECTION (stage2Local, ".mem_msm")
#pragma DATA_SECTION (twLocal, ".mem_msm")
#pragma DATA_SECTION (in_wLocal, ".mem_msm")
#pragma DATA_SECTION (out_wLocal, ".mem_msm")
#pragma DATA_SECTION (out_tLocal, ".mem_msm")
#pragma DATA_SECTION (tw_wLocal, ".mem_msm")
#pragma DATA_SECTION (workLocal, ".mem_msm")
#pragma DATA_SECTION (BnLocal, ".mem_msm")
#pragma DATA_SECTION (Bn_wLocal, ".mem_msm")
#pragma DATA_SECTION (n1Local, ".mem_msm")
#pragma DATA_SECTION (n2Local, ".mem_msm")
#pragma DATA_SECTION (n3Local, ".mem_msm")
#pragma DATA_SECTION (planesLocal, ".mem_msm")
#pragma DATA_SECTION (mLocal, ".mem_msm")
#pragma DATA_SECTION (N_p2Local, ".mem_msm")
#pragma DATA_SECTION (s_r3Local, ".mem_msm")
#pragma DATA_SECTION (s_r5Local, ".mem_msm")
#pragma DATA_SECTION (edmaLocal, ".mem_msm")
#endif

int ifft_omp_dp_3d_c2r_ecpy (
    /* int    N1;       IFFT size        */
    /* int    N2;       IFFT size        */
    /* int    N3;       IFFT size        */
    /* double *in;      input pointer    */
    /* double *out;     output pointer   */
    /* double *in_w;    input pointer    */
    /* double *out_w;   output pointer   */
    /* double *tw;      twiddle pointer  */
    fft_param_u u,
    void *edmaState,
    int   actualCoreNum
)
{
    double *tw_a, *Bn_a;
    int   error = FFT_NOERR;
    int numThread = omp_get_max_threads();
    int tid, did, numLine;
    int twsizeMax, BnsizeMax, N, scratch_buf_size;
    int tw_offsetLocal, Bn_offsetLocal;
    lib_memdscr_t **fft_mem_handle = fftGetMemHandle();

    n1Local       = u.dp_3d_c2r_e.N1;          /* signal size of the 1st dimension      */
    n2Local       = u.dp_3d_c2r_e.N2;          /* signal size of the 2nd dimension      */
    n3Local       = u.dp_3d_c2r_e.N3;          /* signal size of the 3rd dimension      */

    scratch_buf_size = 2*(n1Local/2+1)*n2Local*n3Local;

    inLocal      = u.dp_3d_c2r_e.in;                     /* input pointer                         */
    outLocal     = u.dp_3d_c2r_e.out;                    /* output pointer                        */
    stage1Local  = u.dp_3d_c2r_e.tw;                     /* work buffer pointer                   */
    stage2Local  = u.dp_3d_c2r_e.tw+scratch_buf_size;    /* work buffer pointer                   */
    twLocal      = u.dp_3d_c2r_e.tw+2*scratch_buf_size;  /* twiddle buffer pointer                */
    BnLocal      = u.dp_3d_c2r_e.Bn;                     /* Bn buffer pointer                     */
    edmaLocal    = (FFT_EDMA_Struct *)edmaState;

    /* max of the 3 dimension */
    twsizeMax = (u.dp_3d_c2r_e.para1.twsize > u.dp_3d_c2r_e.para2.twsize) ? u.dp_3d_c2r_e.para1.twsize : u.dp_3d_c2r_e.para2.twsize;
    twsizeMax = (u.dp_3d_c2r_e.para3.twsize > twsizeMax) ? u.dp_3d_c2r_e.para3.twsize : twsizeMax;

    BnsizeMax = (u.dp_3d_c2r_e.para1.Bnsize > u.dp_3d_c2r_e.para2.Bnsize) ? u.dp_3d_c2r_e.para1.Bnsize : u.dp_3d_c2r_e.para2.Bnsize;
    BnsizeMax = (u.dp_3d_c2r_e.para3.Bnsize > BnsizeMax) ? u.dp_3d_c2r_e.para3.Bnsize : BnsizeMax;

    N = (u.dp_3d_c2r_e.N1 > u.dp_3d_c2r_e.N2) ? u.dp_3d_c2r_e.N1 : u.dp_3d_c2r_e.N2;
    N = (u.dp_3d_c2r_e.N3 > N) ? u.dp_3d_c2r_e.N3 : N;

    in_wLocal   = (double*)lib_smem_falloc (fft_mem_handle, 4*N*IFFT_OMP_DP_3D_C2R_NUMOFLINEBUFS*sizeof(double), 8);
    out_wLocal  = (double*)lib_smem_falloc (fft_mem_handle, 4*N*IFFT_OMP_DP_3D_C2R_NUMOFLINEBUFS*sizeof(double), 8);
    out_tLocal  = (double*)lib_smem_falloc (fft_mem_handle, 2*N*IFFT_OMP_DP_3D_C2R_NUMOFLINEBUFS*sizeof(double), 8);
#ifdef FFT_MEM_MODEL_LG
    tw_wLocal   = (double*)lib_smem_falloc (fft_mem_handle, twsizeMax*sizeof(double), 8);
#endif

#ifdef FFT_MEM_MODEL_LG
    if(in_wLocal==NULL || out_wLocal==NULL || out_tLocal==NULL || tw_wLocal==NULL) {
#else
    tw_wLocal = NULL;
    if(in_wLocal==NULL || out_wLocal==NULL || out_tLocal==NULL) {
#endif
        printf("Memory allocation error!\n");
        return FFT_ERROR;
    }

    if((u.dp_3d_c2r_e.para1.use_bs == 1) || (u.dp_3d_c2r_e.para2.use_bs == 1) || (u.dp_3d_c2r_e.para3.use_bs == 1))
    {
     Bn_wLocal = tw_wLocal + twsizeMax;
     workLocal = Bn_wLocal + BnsizeMax;
    }else
    {
     Bn_wLocal = NULL;
     workLocal = NULL;
    }

    if ((actualCoreNum==1)||
        (actualCoreNum==2)||
        (actualCoreNum==4)||
        (actualCoreNum==8))
      omp_set_num_threads (actualCoreNum);
    else
      omp_set_num_threads (OMP_MAX_NUM_CORES);

    numThread = omp_get_max_threads();

    /* prepare for the 3rd dimension */
     twsizeLocal    = u.dp_3d_c2r_e.para3.twsize*sizeof(double);
     BnsizeLocal    = u.dp_3d_c2r_e.para3.Bnsize*sizeof(double);
     N_p2Local      = u.dp_3d_c2r_e.para3.N_p2;    /* power of 2 size in mixed radix        */
     s_r3Local      = u.dp_3d_c2r_e.para3.s_r3;    /* number stages of radix 3              */
     s_r5Local      = u.dp_3d_c2r_e.para3.s_r5;    /* number stages of radix 5              */
     radLocal       = u.dp_3d_c2r_e.para3.rad;
     mLocal         = u.dp_3d_c2r_e.para3.M;
     tw_offsetLocal = u.dp_3d_c2r_e.para1.twsize + u.dp_3d_c2r_e.para2.twsize;
     Bn_offsetLocal = u.dp_3d_c2r_e.para1.Bnsize + u.dp_3d_c2r_e.para2.Bnsize;
     twLocal        = u.dp_3d_c2r_e.tw + 2*scratch_buf_size + tw_offsetLocal; /* twiddle working buffer pointer        */
     BnLocal        = u.dp_3d_c2r_e.Bn + Bn_offsetLocal;   /* Bn working buffer pointer             */


     planesLocal = (n1Local/2+1)*n2Local/numThread;
#pragma omp parallel private(tid, did, numLine, tw_a, Bn_a) shared(radLocal, inLocal, stage1Local, \
       twLocal, in_wLocal, out_wLocal, out_tLocal, tw_wLocal, BnLocal, Bn_wLocal, workLocal, \
       n1Local, n2Local, n3Local, planesLocal, N_p2Local, s_r3Local, s_r5Local, mLocal, edmaLocal)
     {
     tid = omp_get_thread_num();     // Obtain thread number
     did = DNUM;
     numLine = IFFT_OMP_DP_3D_C2R_NUMOFLINEBUFS;
     if (tw_wLocal != NULL) {
       /* prepare twiddle buffer */
       lib_emt_copy1D1D(
       (edmaLocal+did)->channel[FFT_XFER_CHANNEL3],
       (void *)(twLocal),
       (void *)tw_wLocal,
       twsizeLocal);
       tw_a = tw_wLocal;
     }
     else {
       tw_a = twLocal;
     }

     if (Bn_wLocal != NULL) {
       /* prepare Bn buffer */
       lib_emt_copy1D1D(
       (edmaLocal+did)->channel[FFT_XFER_CHANNEL2],
       (void *)(BnLocal),
       (void *)Bn_wLocal,
       BnsizeLocal);
       Bn_a = Bn_wLocal;
     }
     else {
       Bn_a = BnLocal;
     }

     if (tw_wLocal != NULL)
       lib_emt_wait((edmaLocal+did)->channel[FFT_XFER_CHANNEL3]);
     if (Bn_wLocal != NULL)
       lib_emt_wait((edmaLocal+did)->channel[FFT_XFER_CHANNEL2]);
     /* 3rd dimension ifft */
     error = ifft_omp_dp_1d_c2c_batch_ecpy_transpose_3d (
         (n1Local/2+1)*n2Local,
         n3Local,
         planesLocal,
         mLocal,
         radLocal,
         N_p2Local,
         s_r3Local,
         s_r5Local,
         &inLocal[tid*2*planesLocal],
         &stage1Local[tid*2*planesLocal],
         in_wLocal,
         out_wLocal,
         out_tLocal,
         tw_a,
         Bn_a,
         workLocal,
         edmaLocal+did,
         numLine);
     }

    /* prepare for the 2nd dimension */

    twsizeLocal    = u.dp_3d_c2r_e.para2.twsize*sizeof(double);
    BnsizeLocal    = u.dp_3d_c2r_e.para2.Bnsize*sizeof(double);
    N_p2Local      = u.dp_3d_c2r_e.para2.N_p2;    /* power of 2 size in mixed radix        */
    s_r3Local      = u.dp_3d_c2r_e.para2.s_r3;    /* number stages of radix 3              */
    s_r5Local      = u.dp_3d_c2r_e.para2.s_r5;    /* number stages of radix 5              */
    radLocal       = u.dp_3d_c2r_e.para2.rad;
    mLocal         = u.dp_3d_c2r_e.para2.M;
    tw_offsetLocal = u.dp_3d_c2r_e.para1.twsize;
    Bn_offsetLocal = u.dp_3d_c2r_e.para1.Bnsize;
    twLocal        = u.dp_3d_c2r_e.tw + 2*scratch_buf_size + tw_offsetLocal;  /* twiddle working buffer pointer        */
    BnLocal        = u.dp_3d_c2r_e.Bn + Bn_offsetLocal;   /* Bn working buffer pointer             */

    planesLocal = n3Local/numThread;

#pragma omp parallel private(tid, did, numLine, tw_a, Bn_a) shared(radLocal, stage1Local, stage2Local, \
      twLocal, in_wLocal, out_wLocal, out_tLocal, tw_wLocal, BnLocal, Bn_wLocal, workLocal, \
      n1Local, n2Local, planesLocal, N_p2Local, s_r3Local, s_r5Local, mLocal, edmaLocal)
    {
    tid = omp_get_thread_num();     // Obtain thread number
    did = DNUM;
    numLine = IFFT_OMP_DP_3D_C2R_NUMOFLINEBUFS_SM;
    if (tw_wLocal != NULL) {
      /* prepare twiddle buffer */
      lib_emt_copy1D1D(
      (edmaLocal+did)->channel[FFT_XFER_CHANNEL3],
      (void *)(twLocal),
      (void *)tw_wLocal,
      twsizeLocal);
      tw_a = tw_wLocal;
    }
    else {
      tw_a = twLocal;
    }

    if (Bn_wLocal != NULL) {
      /* prepare Bn buffer */
      lib_emt_copy1D1D(
      (edmaLocal+did)->channel[FFT_XFER_CHANNEL2],
      (void *)(BnLocal),
      (void *)Bn_wLocal,
      BnsizeLocal);
      Bn_a = Bn_wLocal;
    }
    else {
      Bn_a = BnLocal;
    }

    if (tw_wLocal != NULL)
      lib_emt_wait((edmaLocal+did)->channel[FFT_XFER_CHANNEL3]);
    if (Bn_wLocal != NULL)
      lib_emt_wait((edmaLocal+did)->channel[FFT_XFER_CHANNEL2]);
    /* 2nd dimension ifft */
    error = ifft_omp_dp_1d_c2c_batch_ecpy_transpose (
        n2Local,
        (n1Local/2+1),
        planesLocal,
        mLocal,
        radLocal,
        N_p2Local,
        s_r3Local,
        s_r5Local,
        &stage1Local[tid*(n2Local*(n1Local/2+1)*2*planesLocal)],
        &stage2Local[tid*(n2Local*(n1Local/2+1)*2*planesLocal)],
        out_wLocal,
        in_wLocal,
        out_tLocal,
        tw_a,
        Bn_a,
        workLocal,
        edmaLocal+did,
        numLine);
    }

    /* prepare for the 1st dimension */

    planesLocal = n3Local/numThread;
    twsizeLocal = u.dp_3d_c2r_e.para1.twsize*sizeof(double);
    BnsizeLocal = u.dp_3d_c2r_e.para1.Bnsize*sizeof(double);
    mLocal        = u.dp_3d_c2r_e.para1.M;              /* fft size                              */
    radLocal      = u.dp_3d_c2r_e.para1.rad;            /* radix                                 */
    N_p2Local     = u.dp_3d_c2r_e.para1.N_p2;           /* power of 2 size in mixed radix        */
    s_r3Local     = u.dp_3d_c2r_e.para1.s_r3;           /* number stages of radix 3              */
    s_r5Local     = u.dp_3d_c2r_e.para1.s_r5;           /* number stages of radix 5              */
    twLocal        = u.dp_3d_c2r_e.tw+2*scratch_buf_size; /* twiddle working buffer pointer        */
    BnLocal        = u.dp_3d_c2r_e.Bn;                                 /* Bn working buffer pointer             */

#pragma omp parallel private(tid, did) shared(radLocal, stage2Local, outLocal, numLine,\
      twLocal, in_wLocal, out_wLocal, out_tLocal, tw_wLocal, BnLocal, Bn_wLocal, workLocal, \
      n1Local, n2Local, planesLocal, N_p2Local, s_r3Local, s_r5Local, mLocal, \
      twsizeLocal, BnsizeLocal, edmaLocal)
    {
    tid = omp_get_thread_num();     // Obtain thread number
    did = DNUM;
    numLine = IFFT_OMP_DP_3D_C2R_NUMOFLINEBUFS_SM;

    if (tw_wLocal != NULL) {
      /* prepare twiddle buffer */
      lib_emt_copy1D1D(
      (edmaLocal+did)->channel[FFT_XFER_CHANNEL3],
      (void *)twLocal,
      (void *)tw_wLocal,
      twsizeLocal);
      tw_a = tw_wLocal;
    }
    else {
      tw_a = twLocal;
    }

    if (Bn_wLocal != NULL) {
      /* prepare Bn buffer */
      lib_emt_copy1D1D(
      (edmaLocal+did)->channel[FFT_XFER_CHANNEL2],
      (void *)BnLocal,
      (void *)Bn_wLocal,
      BnsizeLocal);
      Bn_a = Bn_wLocal;
    }
    else {
      Bn_a = BnLocal;
    }
    if (tw_wLocal != NULL)
      lib_emt_wait((edmaLocal+did)->channel[FFT_XFER_CHANNEL3]);
    if (Bn_wLocal != NULL)
      lib_emt_wait((edmaLocal+did)->channel[FFT_XFER_CHANNEL2]);
    /* 1st dimension ifft */
    error = ifft_omp_dp_1d_c2r_batch_ecpy_transpose (
        n1Local,
        n2Local,
        planesLocal,
        mLocal,
        radLocal,
        N_p2Local,
        s_r3Local,
        s_r5Local,
        &stage2Local[tid*((n1Local/2+1)*2*n2Local*planesLocal)],
        &outLocal[tid*(n1Local*n2Local*planesLocal)],
        in_wLocal,
        out_wLocal,
        out_tLocal,
        tw_a,
        Bn_a,
        workLocal,
        edmaLocal+did,
        numLine);
    }

    omp_set_num_threads (OMP_MAX_NUM_CORES);
    return error;
} /* ifft_omp_dp_3d_c2r_ecpy */

/* ======================================================================== */
/*  End of file:  ifft_omp_dp_3d_c2r.c                                      */
/* ------------------------------------------------------------------------ */
/*            Copyright (c) 2013 Texas Instruments, Incorporated.           */
/*                           All Rights Reserved.                           */
/* ======================================================================== */
