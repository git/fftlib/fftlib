/* ======================================================================= */
/*  TEXAS INSTRUMENTS, INC.                                                */
/*                                                                         */
/*  FFTLIB  FFT Library                                                    */
/*                                                                         */
/* Copyright (C) 2013 Texas Instruments Incorporated - http://www.ti.com/  */
/*                                                                         */
/*                                                                         */
/*  Redistribution and use in source and binary forms, with or without     */
/*  modification, are permitted provided that the following conditions     */
/*  are met:                                                               */
/*                                                                         */
/*    Redistributions of source code must retain the above copyright       */
/*    notice, this list of conditions and the following disclaimer.        */
/*                                                                         */
/*    Redistributions in binary form must reproduce the above copyright    */
/*    notice, this list of conditions and the following disclaimer in the  */
/*    documentation and/or other materials provided with the               */
/*    distribution.                                                        */
/*                                                                         */
/*    Neither the name of Texas Instruments Incorporated nor the names of  */
/*    its contributors may be used to endorse or promote products derived  */
/*    from this software without specific prior written permission.        */
/*                                                                         */
/*  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS    */
/*  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT      */
/*  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR  */
/*  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT   */
/*  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,  */
/*  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT       */
/*  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,  */
/*  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY  */
/*  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT    */
/*  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE  */
/*  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.   */
/*                                                                         */
/* ======================================================================= */

#include <xdc/std.h>
#include <stdio.h>
#include <time.h>
#include <stdlib.h>
#include <limits.h>
#include <math.h>
#include <c6x.h>
#include <ti/csl/csl_cacheAux.h>

#include <ti/fftlib/src/common/omp/omp_config.h>
#include <ti/fftlib/src/common/fft_common_d.h>
#include "ifft_omp_dp_3d_c2r.h"
#include <ti/dsplib/src/DSPF_dp_fftDPxDP/DSPF_dp_fftDPxDP.h>
#include <ti/dsplib/src/DSPF_dp_ifftDPxDP/DSPF_dp_ifftDPxDP.h>
#include <ti/runtime/openmp/omp.h>

void dft_3d (int N1, int N2, int N3, double x[], double w[], double y[]);
void dft (int N, double x[], double y[], int N1);
void tw_gen_cn (double *w, int n1, int n2, int n3);
void ifft_3d_cmp (int N1, int rad1, int N2, int rad2, int N3, int rad3, double x[], double tw[], double y[]);

/* calculate radix for 1D array */
static void calculate_rad (int dim, int N, fft_para_mix_bs_t *fft_para)
{
  int n, j, M, rad;
  int s_r3, s_r5, N_p2,dum;
    int use_bs;

  use_bs = 1;

    n = (N<<1)-1;
    j = _norm(n);
    M = 1 << (31-j);
    if (dim!=3)
      j = _norm(M);

    if (dim == 3)
      N_p2 = N/2;
    else
      N_p2 = N;
    s_r3 = 0;
    s_r5 = 0;

    /* if N is odd, bypass the rest of calculation, use Bluestein */
    if (((N>>1)<<1) == N)
    {
      /* if N is power of 2, no zero padding, regular radix 2 or 4 fft */
      if (N == (M>>1))
      { /* power of 2 */
        M = N;
        j = j+1;
        use_bs = 0;
      }
      else {
          if (dim == 3)
          dum = N/2;
          else
            dum = N;
          while (dum/3*3 == dum) {
            s_r3++;
            dum /= 3;
          }
          while (dum/5*5 == dum) {
            s_r5++;
            dum /= 5;
          }
          if ((dum >= 4) && (dum == (1 << (30-_norm(dum))))) {
          /* mixed radix 2 and 3 */
            N_p2                       = dum;
            use_bs = 0;
            M = N;
            j  = _norm(dum);
          }
        }
      }

    /* calculate radix for radix 2 or 4 c2r fft */
    if (dim==3)
    {
      if (j%2 == use_bs)
        rad = 4;
      else
        rad = 2;
    } else
    {
        if (j%2 == 0)
          rad = 4;
        else
          rad = 2;
    }

    fft_para->N_p2 = N_p2;
    fft_para->s_r3 = s_r3;
    fft_para->s_r5 = s_r5;
    fft_para->M    = M;
    fft_para->rad  = rad;
    fft_para->use_bs = use_bs;

} /*calculate_rad ()*/

/* calculate twiddle size, Bn buffer size and workbuffer size */
static void calculate_mem_size(int dim, int N,  fft_para_mix_bs_t *fft_para, int *twsize, int *Bnsize, int *worksize)
{
  int dum, n, i;
  int M      = fft_para->M;
  int use_bs = fft_para->use_bs;
  int s_r3   = fft_para->s_r3;
  int s_r5   = fft_para->s_r5;
  int N_p2   = fft_para->N_p2;
  /* Please note that Bn buffer and Workbuf are only used when doing Bluestein
   * meaning when use_bs == 1 */

  if (use_bs == 1)
  { /* when size is not power of 2 and not multiple of 3&5 */
  *twsize   = (2*M+2*N);
  *Bnsize   = (2*M);
  *worksize = (4*M);
  }
  else
  {
    if ((s_r3 == 0) && (s_r5 == 0)) {
    /* When signal is power of 2 */
        if (dim == 3)
          *twsize   = N*3;
        else
          *twsize   = N*2;
    }
    else {
    /* When signal is power of 3 or 5 */
      dum = 0;
      if (dim == 3)
        n  = 2*N;
      else
        n  = 4*N;
      for (i = 0; i < s_r3; i++) {
        dum += n/3;
        n  /= 3;
      }
      n *= 2;  /* radix-5 has 8 twiddle vs radix-3 4 twiddles */
      for (i = 0; i < s_r5; i++) {
        dum += n/5;
        n  /= 5;
      }
      if (dim == 3)
        *twsize = (N/2+2*N_p2+dum);
      else
        *twsize = (2*N_p2+dum);
    }
    *Bnsize   = 0;
    *worksize = 0;
  }
}

/* ======================================================================== */
/*  Kernel-specific alignments                                              */
/* ======================================================================== */
#pragma DATA_SECTION(x_i, ".mem_ddr");
#pragma DATA_SECTION(y_i, ".mem_ddr");
#pragma DATA_SECTION(x_cn, ".mem_ddr");
#pragma DATA_SECTION(y_cn, ".mem_ddr");
//#pragma DATA_SECTION(w_cn, ".mem_ddr");

#pragma DATA_SECTION(w_i, ".mem_ddr");

#pragma DATA_ALIGN(x_i,  8);
#pragma DATA_ALIGN(x_cn, 8);


//#pragma DATA_ALIGN(w_cn, 8);

#pragma DATA_ALIGN(y_i,  8);
#pragma DATA_ALIGN(y_cn, 8);

#pragma DATA_SECTION(local_work, ".mem_l2");
#pragma DATA_ALIGN(local_work,  64);


/* ======================================================================== */
/*  Parameters of fixed dataset.                                            */
/* ======================================================================== */

#if ANYSIZE_SUPPORT
#define MAXN  (128)
#else
#define MAXN  (128)
#endif
#define M     (2*MAXN*MAXN*MAXN)
#define PAD   (0)

/* ======================================================================== */
/*  Initialized arrays with fixed test data.                                */
/* ======================================================================== */

double x_i [M + 2 * PAD];
double x_cn[M + 2 * PAD];

//double w_cn[6*MAXN + 2 * PAD];

double y_i [M + 2 * PAD];
double y_cn[M + 2 * PAD];


#if ANYSIZE_SUPPORT
double w_i [3*6*MAXN + 4*(MAXN/2+1)*MAXN*MAXN + 2 * PAD];
double local_work [16432 + FFT_NUM_EDMA_CH*FFT_MAX_EDMA_LINKS_3D + 12*MAXN*IFFT_OMP_DP_3D_C2R_NUMOFLINEBUFS + 6*MAXN + 4*MAXN + 8* MAXN+ 2 + 2*PAD];

#else
double w_i  [6*MAXN +  4*(MAXN/2+1)*MAXN*MAXN + 2 * PAD]; /* including staging buffer to protect input content */
#ifdef FFT_MEM_MODEL_LG
double local_work [16432 + FFT_NUM_EDMA_CH*FFT_MAX_EDMA_LINKS_3D + 12*MAXN*IFFT_OMP_DP_3D_C2R_NUMOFLINEBUFS + 2*MAXN + 2 + 2*PAD];
#else
double local_work [FFT_NUM_EDMA_CH*FFT_MAX_EDMA_LINKS_3D + 12*MAXN*IFFT_OMP_DP_3D_C2R_NUMOFLINEBUFS + 2*MAXN + 2 + 2*PAD];
#endif
#endif

/* ======================================================================== */
/*  Generate pointers to skip beyond array padding                          */
/* ======================================================================== */
double *const ptr_x_i  = x_i  + PAD;
double *const ptr_x_cn = x_cn + PAD;

//double *const ptr_w_cn = w_cn + PAD;
double *const ptr_w_cn = NULL;

double *const ptr_y_i  = y_i  + PAD;
double *const ptr_y_cn = y_cn + PAD;
double *const ptr_w_i  = w_i + PAD;

/* ======================================================================== */
/*  MAIN -- Top level driver for the test.                                  */
/* ======================================================================== */

int main ()
{
    int     i, j, k, N1, N2, N3;

    int     int_tw_size, int_Bn_size, work_size;
    int     twsize, Bnsize, worksize;
    int     ext_tw_size, ext_Bn_size;
    clock_t t_start, t_stop, t_overhead, t_opt;
    double   diff, max_diff = 0;
    fft_plan_t p;
    fft_callout_t plan_fxns;
    size_t  l2_SRAM_size_orig;
    uint32_t *temp;
    lib_memdscr_t **fft_mem_handle = fftGetMemHandle();

    /* --------------------------------------------------------------------- */
    /* intialize hardware timers                                             */
    /* --------------------------------------------------------------------- */
    TSCL=0;TSCH=0;

    /* initalize callout functions */
    plan_fxns.memoryRequest   = NULL;
    plan_fxns.memoryRelease   = NULL;
    plan_fxns.ecpyRequest     = fft_omp_assign_edma_resources;
    plan_fxns.ecpyRelease     = fft_omp_free_edma_resources;
    
    /* initialize ECPY */ 
    omp_set_num_threads (ACTIVE_THREAD_COUNT);
    fft_config_memory (&l2_SRAM_size_orig);
    temp = (uint32_t *)lib_smem_falloc(fft_mem_handle, 256*6*sizeof(uint32_t), 3);
#if 1
    if(temp==NULL) {
        printf("Memory allocation error!\n");
        return;
    }
#endif

    #pragma omp parallel
    {
      fft_assert( (lib_emt_init() == LIB_EMT_SUCCESS), DNUM, "lib_emt_init() return error!");
      fftEdmaState[DNUM] = FFT_EDMA_STATE_INIT;
    }
#if 1
    /* radix 2&4 testing */
    for (N1 = 64, N2 = 32, N3 = 64, k = 1; N2 <= MAXN; N1 = N1 << 1, N2 = N2 << 1, k++)
    {

        memset (x_i,  0x55, sizeof (x_i) );
        memset (x_cn, 0x55, sizeof (x_cn));
        memset (y_cn,  0x55, sizeof (y_cn) );

        /* ---------------------------------------------------------------- */
        /* Construct the input signal to meet the requirement for c2r.      */
        /* ---------------------------------------------------------------- */

        for (j = 0; j < N3; j++) {
           for (i = 0; i < N1*N2; i++) {
             x_cn[PAD + 2*i + j*2*N1*N2]     = sin (2 * 3.14159265358979323846 * (50+j) * i / (double) (N2));
             x_cn[PAD + 2*i + j*2*N1*N2 + 1] = 0;
           }
        }

        dft_3d (N1, N2, N3, x_cn, y_cn, x_i);


        /* ARM side of the work */

        /* determine the radix for the first dimension */
        calculate_rad(3, N1, &p.u.dp_3d_c2r_e.para1);
        /* determine the radix for the second dimension */
        calculate_rad(2, N2, &p.u.dp_3d_c2r_e.para2);
        /* determine the radix for the third dimension */
        calculate_rad(1, N3, &p.u.dp_3d_c2r_e.para3);

        /* evaluate the  1st dimension */
        calculate_mem_size(3, N1, &p.u.dp_3d_c2r_e.para1, &twsize, &Bnsize, &worksize);
        p.u.dp_3d_c2r_e.para1.twsize = twsize;
        p.u.dp_3d_c2r_e.para1.Bnsize = Bnsize;
        int_tw_size = ext_tw_size = twsize;
        int_Bn_size = ext_Bn_size = Bnsize;
        work_size   = worksize;

        /* evaluate the  2nd dimension */
        calculate_mem_size(2, N2, &p.u.dp_3d_c2r_e.para2, &twsize, &Bnsize, &worksize);
        p.u.dp_3d_c2r_e.para2.twsize = twsize;
        p.u.dp_3d_c2r_e.para2.Bnsize = Bnsize;
        int_tw_size  = (int_tw_size > twsize) ? int_tw_size : twsize;
        ext_tw_size += twsize;
        int_Bn_size  = (int_Bn_size > Bnsize) ? int_Bn_size : Bnsize;
        ext_Bn_size += Bnsize;
        work_size    = (work_size > worksize) ? work_size : worksize;

        /* evaluate the  3rd dimension */
        calculate_mem_size(1, N3, &p.u.dp_3d_c2r_e.para3, &twsize, &Bnsize, &worksize);
        p.u.dp_3d_c2r_e.para3.twsize = twsize;
        p.u.dp_3d_c2r_e.para3.Bnsize = Bnsize;
        int_tw_size  = (int_tw_size > twsize) ? int_tw_size : twsize;
        ext_tw_size += twsize;
        int_Bn_size  = (int_Bn_size > Bnsize) ? int_Bn_size : Bnsize;
        ext_Bn_size += Bnsize;
        work_size    = (work_size > worksize) ? work_size : worksize;


        /* ---------------------------------------------------------------- */
        /* Force uninitialized arrays to fixed values.                      */
        /* ---------------------------------------------------------------- */
        memset (y_i,  0xA5, sizeof (y_i) );

        ifft_omp_dp_plan_3d_c2r (N1, N2, N3, FFT_ECPY, plan_fxns, &p, ptr_x_i, ptr_y_i, ptr_w_i, NULL);
        CACHE_wbInvAllL2(CACHE_WAIT);
        
        /* ---------------------------------------------------------------- */
        /* Compute the overhead of calling clock twice to get timing info   */
        /* ---------------------------------------------------------------- */
        p.edmaState = (*p.fftcout.ecpyRequest)(temp, FFT_NUM_EDMA_CH*FFT_MAX_EDMA_LINKS_3D*sizeof(float));
        (*p.fftcout.ecpyRelease)(p.edmaState);
        p.edmaState = (*p.fftcout.ecpyRequest)(temp, FFT_NUM_EDMA_CH*FFT_MAX_EDMA_LINKS_3D*sizeof(float));
        (*p.fftcout.ecpyRelease)(p.edmaState);
        t_start = _itoll(TSCH, TSCL);
        p.edmaState = (*p.fftcout.ecpyRequest)(temp, FFT_NUM_EDMA_CH*FFT_MAX_EDMA_LINKS_3D*sizeof(float));
        (*p.fftcout.ecpyRelease)(p.edmaState);
        t_stop  = _itoll(TSCH, TSCL);
        t_overhead = t_stop - t_start;
        
        printf("fft_dp_plan_3d_c2r is done\n");
        /* ---------------------------------------------------------------------- */
        /* Set the number of cores used                                           */
        /* ---------------------------------------------------------------------- */
        p.actualCoreNum = ACTIVE_THREAD_COUNT;
        p.local = local_work;
        t_start = _itoll(TSCH, TSCL);
        fft_execute (p);
        t_stop = _itoll(TSCH, TSCL);
        t_opt  = (t_stop - t_start) - t_overhead;
        printf("fft_execute is done\n");

        /* ---------------------------------------------------------------- */
        /* compute difference and track max difference                      */  
        /* ---------------------------------------------------------------- */
        diff = 0; max_diff = 0;
        for(i=0; i<N1*N2*N3; i++) {
          diff = _fabs(ptr_x_cn[2*i]*N1*N2*N3 - ptr_y_i[i]);
          if (diff > max_diff)
            max_diff = diff;
        }
        printf("ifft_dp_3d_c2r_ecpy\tsize= %d X %d X %d\t", N1, N2, N3);
        printf("max_diff = %f", max_diff);
        printf("\t N3 = %d\tCycle: %d\n\n", N3, t_opt);

    }
#endif
#if 1
    /* radix 3&5 and bluestein testing */
    for (N1 = 64, N2 = 64, N3 = 32*3, k = 1; ((N1 <= MAXN) && (N2 <= MAXN) && (N3 <= MAXN)); N3 = N3*3, k++)
    {
        memset (x_i,  0x55, sizeof (x_i) );
        memset (x_cn, 0x55, sizeof (x_cn));
        memset (y_cn,  0x55, sizeof (y_cn) );

        /* ---------------------------------------------------------------- */
        /* Construct the input signal to meet the requirement for c2r.      */
        /* ---------------------------------------------------------------- */

        for (j = 0; j < N3; j++) {
           for (i = 0; i < N1*N2; i++) {
             x_cn[PAD + 2*i + j*2*N1*N2]     = sin (2 * 3.14159265358979323846 * (50+j) * i / (double) (N2));
             x_cn[PAD + 2*i + j*2*N1*N2 + 1] = 0;
           }
        }

        dft_3d (N1, N2, N3, x_cn, y_cn, x_i);

        /* ---------------------------------------------------------------- */
        /* Force uninitialized arrays to fixed values.                      */
        /* ---------------------------------------------------------------- */
        memset (y_i,  0xA5, sizeof (y_i) );

        /* determine the radix for the first dimension */
        calculate_rad(3, N1, &p.u.dp_3d_c2r_e.para1);
        /* determine the radix for the second dimension */
        calculate_rad(2, N2, &p.u.dp_3d_c2r_e.para2);
        /* determine the radix for the third dimension */
        calculate_rad(1, N3, &p.u.dp_3d_c2r_e.para3);

        /* evaluate the  1st dimension */
        calculate_mem_size(3, N1, &p.u.dp_3d_c2r_e.para1, &twsize, &Bnsize, &worksize);
        p.u.dp_3d_c2r_e.para1.twsize = twsize;
        p.u.dp_3d_c2r_e.para1.Bnsize = Bnsize;
        int_tw_size = ext_tw_size = twsize;
        int_Bn_size = ext_Bn_size = Bnsize;
        work_size   = worksize;

        /* evaluate the  2nd dimension */
        calculate_mem_size(2, N2, &p.u.dp_3d_c2r_e.para2, &twsize, &Bnsize, &worksize);
        p.u.dp_3d_c2r_e.para2.twsize = twsize;
        p.u.dp_3d_c2r_e.para2.Bnsize = Bnsize;
        int_tw_size  = (int_tw_size > twsize) ? int_tw_size : twsize;
        ext_tw_size += twsize;
        int_Bn_size  = (int_Bn_size > Bnsize) ? int_Bn_size : Bnsize;
        ext_Bn_size += Bnsize;
        work_size    = (work_size > worksize) ? work_size : worksize;

        /* evaluate the  3rd dimension */
        calculate_mem_size(1, N3, &p.u.dp_3d_c2r_e.para3, &twsize, &Bnsize, &worksize);
        p.u.dp_3d_c2r_e.para3.twsize = twsize;
        p.u.dp_3d_c2r_e.para3.Bnsize = Bnsize;
        int_tw_size  = (int_tw_size > twsize) ? int_tw_size : twsize;
        ext_tw_size += twsize;
        int_Bn_size  = (int_Bn_size > Bnsize) ? int_Bn_size : Bnsize;
        ext_Bn_size += Bnsize;
        work_size    = (work_size > worksize) ? work_size : worksize;


        /* ---------------------------------------------------------------- */
        /* Force uninitialized arrays to fixed values.                      */
        /* ---------------------------------------------------------------- */
        memset (y_i,  0xA5, sizeof (y_i) );

        ifft_omp_dp_plan_3d_c2r (N1, N2, N3, FFT_ECPY, plan_fxns, &p, ptr_x_i, ptr_y_i, ptr_w_i, NULL);

        /* ---------------------------------------------------------------- */
        /* Compute the overhead of calling clock twice to get timing info   */
        /* ---------------------------------------------------------------- */
        p.edmaState = (*p.fftcout.ecpyRequest)(temp, FFT_NUM_EDMA_CH*FFT_MAX_EDMA_LINKS_3D*sizeof(float));
        (*p.fftcout.ecpyRelease)(p.edmaState);
        p.edmaState = (*p.fftcout.ecpyRequest)(temp, FFT_NUM_EDMA_CH*FFT_MAX_EDMA_LINKS_3D*sizeof(float));
        (*p.fftcout.ecpyRelease)(p.edmaState);
        t_start = _itoll(TSCH, TSCL);
        p.edmaState = (*p.fftcout.ecpyRequest)(temp, FFT_NUM_EDMA_CH*FFT_MAX_EDMA_LINKS_3D*sizeof(float));
        (*p.fftcout.ecpyRelease)(p.edmaState);
        t_stop  = _itoll(TSCH, TSCL);
        t_overhead = t_stop - t_start;

        printf("fft_dp_plan_3d_c2r is done\n");
        /* ---------------------------------------------------------------------- */
        /* Set the number of cores used                                           */
        /* ---------------------------------------------------------------------- */
        p.actualCoreNum = ACTIVE_THREAD_COUNT;

        t_start = _itoll(TSCH, TSCL);
        p.local = local_work;
        fft_execute (p);
        t_stop = _itoll(TSCH, TSCL);
        t_opt  = (t_stop - t_start) - t_overhead;
        printf("fft_execute is done\n");

        /* ---------------------------------------------------------------- */
        /* compute difference and track max difference                      */
        /* ---------------------------------------------------------------- */
        diff = 0; max_diff = 0;
        for(i=0; i<N1*N2*N3; i++) {
          diff = _fabs(ptr_x_cn[2*i]*N1*N2*N3 - ptr_y_i[i]);
          if (diff > max_diff) max_diff = diff;
        }
        printf("ifft_dp_3d_c2r_ecpy\tsize= %d X %d X %d\t", N1, N2, N3);
        printf("max_diff = %f", max_diff);
        printf("\t N3 = %d\tCycle: %d\n\n", N3, t_opt);

    }
#endif
}

void dft (int N, double x[], double y[], int N1)
{
    int k, i, index;
    const double PI = 3.14159265358979323846;
    double *p_x;
    double arg, fx_0, fx_1, fy_0, fy_1, co, si;

    for (k = 0; k < N1; k++)
    {
        p_x = x;

        fy_0 = 0;
        fy_1 = 0;
        for (i = 0; i < N; i++)
        {
            fx_0 = p_x[0];
            fx_1 = p_x[1];
            p_x += 2;
            index = (i * k) % N;
            arg = 2 * PI * index / N;
            co = cos (arg);
            si = -sin (arg);
            fy_0 += ((fx_0 * co) - (fx_1 * si));
            fy_1 += ((fx_1 * co) + (fx_0 * si));
        }
        y[2 * k] = fy_0;
        y[2 * k + 1] = fy_1;
    }
}

/* Function for calculating 3d FFT */
void dft_3d (int N1, int N2, int N3, double x[], double w[], double y[])
{
    int k, i, j, m, n1, n2, n3;

    double *restrict p_x = x, *restrict p_y = y, *restrict p_w = w;

    /* calculate all the 2D fft along n1, n2 axis*/
    for (m = 0; m < N3; m++)
    {
      /* calculate N2 FFT size of N1 */
      for (k = 0; k < N2; k++)
      {
        p_x = x + k*2*N1 + m*2*N1*N2;
        p_y = y + k*2*N1 + m*2*N1*N2;
        dft (N1, p_x, p_y, N1);
      }
      /* matrix transpose, save only half of the spectrum */
      for(i = 0; i < 2*N2; i+=2)
        for(j = 0; j < (N1/2+1); j++)
        {
            w[2*j*N2 + i + m*2*(N1/2+1)*N2]     = y[2*j + N1*i + m*2*N1*N2];
            w[2*j*N2 + i + m*2*(N1/2+1)*N2 + 1] = y[2*j + N1*i + m*2*N1*N2 + 1];
        }

      /* calculate N1 FFT size of N2 */
      for (k = 0; k < N1/2+1; k++)
      {
        p_w = w + k*2*N2 + m*2*(N1/2+1)*N2;
        p_y = y + k*2*N2 + m*2*(N1/2+1)*N2;
        dft (N2, p_w, p_y, N2);

      }

      /* matrix transpose */
      for(i = 0; i < 2*(N1/2+1); i+=2)

        for(j = 0; j < N2; j++)
        {
            w[2*j*(N1/2+1) + m*2*(N1/2+1)*N2 + i]     = y[2*j + N2*i + m*2*(N1/2+1)*N2];
            w[2*j*(N1/2+1) + m*2*(N1/2+1)*N2 + i + 1] = y[2*j + N2*i + m*2*(N1/2+1)*N2 + 1];
        }
    }


    /* calculate 1D fft along n3 axis */

    /* first global transpose */
    for (n1 = 0; n1 < (N1/2+1); n1++)
    {
      for(n2 = 0; n2 < N2; n2++)
      {
        for(n3 = 0; n3 < N3; n3++)
        {
          y[2*n1*N3 + n2*2*(N1/2+1)*N3 + 2*n3 ]     = w[2*n1 + n2*2*(N1/2+1) + 2*(N1/2+1)*N2*n3];
          y[2*n1*N3 + n2*2*(N1/2+1)*N3 + 2*n3 + 1]  = w[2*n1 + n2*2*(N1/2+1) + 2*(N1/2+1)*N2*n3 + 1];
        }
      }
   }

   for (m = 0; m < N2; m++)
   {
   /* calculate N1 FFT size of N3 */
     for (k = 0; k < (N1/2+1); k++)
     {
       p_w = w + k*2*N3 + m*2*N3*(N1/2+1);
       p_y = y + k*2*N3 + m*2*N3*(N1/2+1);
       dft (N3, p_y, p_w, N3);
     }
   }

   /* global transpose back */
   for (n1 = 0; n1 < (N1/2+1); n1++)
   {
     for(n2 = 0; n2 < N2; n2++)
     {
       for(n3 = 0; n3 < N3; n3++)
       {
       y[2*n1 + n2*2*(N1/2+1) + 2*(N1/2+1)*N2*n3]     = w[2*n1*N3 + n2*2*(N1/2+1)*N3 + 2*n3 ];
       y[2*n1 + n2*2*(N1/2+1) + 2*(N1/2+1)*N2*n3 + 1] = w[2*n1*N3 + n2*2*(N1/2+1)*N3 + 2*n3 + 1];
       }
     }
  }

}

/* ======================================================================== */
/*  End of file:  ifft_omp_dp_3d_c2r_d.c                                    */
/* ------------------------------------------------------------------------ */
/*            Copyright (c) 2013 Texas Instruments, Incorporated.           */
/*                           All Rights Reserved.                           */
/* ======================================================================== */
