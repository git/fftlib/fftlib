/* ======================================================================= */
/*  TEXAS INSTRUMENTS, INC.                                                */
/*                                                                         */
/*  FFTLIB  FFT Library                                                    */
/*                                                                         */
/* Copyright (C) 2013 Texas Instruments Incorporated - http://www.ti.com/  */ 
/*                                                                         */
/*                                                                         */
/*  Redistribution and use in source and binary forms, with or without     */
/*  modification, are permitted provided that the following conditions     */
/*  are met:                                                               */
/*                                                                         */
/*    Redistributions of source code must retain the above copyright       */
/*    notice, this list of conditions and the following disclaimer.        */
/*                                                                         */
/*    Redistributions in binary form must reproduce the above copyright    */
/*    notice, this list of conditions and the following disclaimer in the  */
/*    documentation and/or other materials provided with the               */
/*    distribution.                                                        */
/*                                                                         */
/*    Neither the name of Texas Instruments Incorporated nor the names of  */
/*    its contributors may be used to endorse or promote products derived  */
/*    from this software without specific prior written permission.        */
/*                                                                         */
/*  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS    */
/*  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT      */
/*  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR  */
/*  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT   */
/*  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,  */
/*  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT       */
/*  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,  */
/*  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY  */
/*  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT    */
/*  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE  */
/*  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.   */
/*                                                                         */
/* ======================================================================= */

#include <ti/fftlib/src/common/fft_common.h>

#ifndef IFFT_DP_3D_C2R_H_
#define IFFT_DP_3D_C2R_H_ 1

/** @ingroup 3D */
/* @{ */

/** @defgroup IFFT_DP_3D_C2R */
/** @ingroup IFFT_DP_3D_C2R */
/* @{ */

#define IFFT_DP_3D_C2R_MIN_SIZE (      4*4*4)
#define IFFT_DP_3D_C2R_MAX_SIZE (1024*1024*1024)

/**
 *     @param IFFT_DP_3D_C2R_NUMOFLINEBUFS = number of cache lines to process each iteration
 */
 
#define IFFT_DP_3D_C2R_NUMOFLINEBUFS 4

/**
 *     This function sets up the parameters for each dimension and the algorithm to 
 *     utilize.
 *
 *     @param N1 = FFT size dimension 1        
 *     @param N2 = FFT size dimension 2
 *     @param N3 = FFT size dimension 3
 *     @param mode = Only FFT_ECPY is supported for 3d FFT
 */

/* IFFT plan 3D DP API */
fft_plan_t ifft_dp_plan_3d_c2r (
    int   N1,       /* IFFT size */
    int   N2,       /* IFFT size */
    int   N3,       /* IFFT size */
    int   mode,     /* IFFT_DIRECT or FFT_ECPY */
    fft_callout_t fxns
);

 /**
 *     This program performs one 3-dimensional double-precision real-to-complex IFFT.
 *  
 *     @param N1 = FFT size dimension 1        
 *     @param N2 = FFT size dimension 2
 *     @param N3 = FFT size dimension 3
 *     @param para1 = Twiddle, mixed radix & bluestein parameters for dimension 1
 *     @param para2 = Twiddle, mixed radix & bluestein parameters for dimension 2
 *     @param para2 = Twiddle, mixed radix & bluestein parameters for dimension 3
 *     @param data = input/output pointer    
 *     @param workbuf = working pointer   
 *     @param data_w = input/output buffer pointer    
 *     @param workbuf_w = working buffer pointer  
 *     @param workbuf_t = Cache line buffer pointer  
 *     @param tw = twiddle pointer  
 *     @param tw_w = twiddle buffer pointer
 *     @param Bn = Bluestein pointer  
 *     @param Bn_w = Bluestein buffer pointer
 *     @param workbuf_bn = Bluestein scratch pointer
 * 
 * @par Algorithm:
 *      Algorithm can run in anysize or power of two mode based upon values of N1, N2 & N3. Supported radix
 *      values are 2, 3, 4, & 5, and Bluestein for all other sizes.<br><b>Note:</b>Parameters 
 *      <tt>para1</tt> & <tt>para2</tt> are determined at runtime and will depend upon type of FFT
 *      desired, radix 3 & 5, radix 2 & 4, or bluestein.
 *
 * @par Assumptions:
 *      N1, N2 & N3 are a multiple of IFFT_DP_3D_C2R_NUMOFLINEBUFS and positive values.<br>
 *      All input, output and working buffers are double word aligned.<br>
 *      No input, working or buffers overlap.<br>
 *
 * @par Size Limitations:
 *      Current limitations are: 512 IFFT size with bluestein, 1024 IFFT size for power of 2.
 *
 * @par Implementation notes:
 * @b Endian Support: The code supports little endian modes.  <BR> 
 * @b Interruptibility: The code is interrupt-tolerant but not interruptible. <BR>
 *  
 */

/* IFFT execute 3D DP ecpy mode API */
int ifft_dp_3d_c2r_ecpy (
    /* int   N1;            FFT size        */
    /* int   N2;            FFT size        */
    /* int   N2;            FFT size        */
    /* fft_para_mix_bs_t para1;             */
    /* fft_para_mix_bs_t para2;             */
    /* fft_para_mix_bs_t para3;             */
    /* double *data;        input/output pointer    */
    /* double *workbuf;     working pointer   */
    /* double *data_w;      input/output buffer pointer    */
    /* double *workbuf_w;   working buffer pointer   */
    /* double *workbuf_t;   working buffer pointer   */
    /* double *tw;          twiddle pointer  */
    /* double *tw_w;        twiddle buffer pointer  */
    /* double *Bn;          Bluestein pointer  */
    /* double *Bn_w;        Bluestein buffer pointer  */
    /* double *workbuf_bn;  Bluestein scratch pointer  */
    fft_param_u u,
    void *edmaState
);
 
 /* }@ */ /* ingroup */
 /* }@ */ /* ingroup */
 
#endif /* IFFT_DP_2D_C2R_H_ */

/* ======================================================================== */
/*  End of file: ifft_dp_3d_c2r.h                                           */
/* ------------------------------------------------------------------------ */
/*            Copyright (c) 2013 Texas Instruments, Incorporated.           */
/*                           All Rights Reserved.                           */
/* ======================================================================== */


