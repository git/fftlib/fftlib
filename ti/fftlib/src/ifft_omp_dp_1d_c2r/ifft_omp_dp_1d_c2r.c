/* ======================================================================= */
/*  TEXAS INSTRUMENTS, INC.                                                */
/*                                                                         */
/*  FFTLIB  FFT Library                                                    */
/*                                                                         */
/* Copyright (C) 2013 Texas Instruments Incorporated - http://www.ti.com/  */ 
/*                                                                         */
/*                                                                         */
/*  Redistribution and use in source and binary forms, with or without     */
/*  modification, are permitted provided that the following conditions     */
/*  are met:                                                               */
/*                                                                         */
/*    Redistributions of source code must retain the above copyright       */
/*    notice, this list of conditions and the following disclaimer.        */
/*                                                                         */
/*    Redistributions in binary form must reproduce the above copyright    */
/*    notice, this list of conditions and the following disclaimer in the  */
/*    documentation and/or other materials provided with the               */
/*    distribution.                                                        */
/*                                                                         */
/*    Neither the name of Texas Instruments Incorporated nor the names of  */
/*    its contributors may be used to endorse or promote products derived  */
/*    from this software without specific prior written permission.        */
/*                                                                         */
/*  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS    */
/*  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT      */
/*  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR  */
/*  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT   */
/*  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,  */
/*  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT       */
/*  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,  */
/*  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY  */
/*  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT    */
/*  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE  */
/*  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.   */
/*                                                                         */
/* ======================================================================= */
#include <stdio.h>
#include <xdc/std.h>
#include "c6x.h"
#include "stdlib.h"
#include <ti/fftlib/src/common/omp/omp_config.h>
#include "ifft_omp_dp_1d_c2r.h"
#include <ti/fftlib/src/common/fft_edma.h>
#include <ti/fftlib/src/common/ifft/ifft_dp_mixed_bs.h>
#include <ti/dsplib/src/DSPF_dp_ifftDPxDP/DSPF_dp_ifftDPxDP.h>
#include <ti/dsplib/src/DSPF_dp_mat_trans/DSPF_dp_mat_trans.h>
#include <ti/runtime/openmp/omp.h>
#include <ti/libarch/libarch.h>

static void DSPF_dp_mat_trans_cplx(const double *restrict x, const int rows,
  const int cols, double *restrict y)
{
  int i, j = 0;

  #pragma MUST_ITERATE(2,,)
  for(i = 0; i < rows; i++)
  #pragma MUST_ITERATE(2,,)
  for(j = 0; j < cols; j++)
  {
    y[j*rows*2 + i*2] = x[j*2 + cols*i*2];
    y[j*rows*2 + i*2 + 1] = x[1 + j*2 + cols*i*2];
  }
}
static int    N1;       /* stage 1 size                     */
static int    N2;       /* stage 2 size                     */
static int    rad1;     /* stage 1 radix                    */
static int    rad2;     /* stage 2 radix                    */
static int   N_p2;      /* power of 2 size in mixed radix   */
static int   s_r3;      /* number stages of radix 3         */
static int   s_r5;      /* number stages of radix 5         */
static double *data;    /* data pointer                     */
static double *out;     /* output pointer                   */
static double *work;    /* working buffer pointer           */
static double *data_w;  /* data internal pointer            */
static double *work_w;  /* working buffer internal pointer  */
static double *work_t;  /* temp buffer internal pointer     */
static double *tw_e;    /* twiddle buffer pointer           */
static double *tw_w;    /* twiddle working buffer pointer   */
static int    lines;
static FFT_EDMA_Struct *edma;
#ifndef FFT_MEM_MODEL_SM
#pragma DATA_SECTION (edma,   ".mem_msm")
#pragma DATA_SECTION (lines,  ".mem_msm")
#pragma DATA_SECTION (N1,     ".mem_msm")
#pragma DATA_SECTION (N2,     ".mem_msm")
#pragma DATA_SECTION (rad1,   ".mem_msm")
#pragma DATA_SECTION (rad2,   ".mem_msm")
#pragma DATA_SECTION (s_r3,   ".mem_msm")
#pragma DATA_SECTION (s_r5,   ".mem_msm")
#pragma DATA_SECTION (N_p2,   ".mem_msm")
#pragma DATA_SECTION (data,   ".mem_msm")
#pragma DATA_SECTION (out,    ".mem_msm")
#pragma DATA_SECTION (work,   ".mem_msm")
#pragma DATA_SECTION (data_w, ".mem_msm")
#pragma DATA_SECTION (work_w, ".mem_msm")
#pragma DATA_SECTION (tw_e,   ".mem_msm")
#pragma DATA_SECTION (tw_w,   ".mem_msm")
#endif

int ifft_omp_dp_1d_c2r_ecpy (
    /* int    N;        IFFT size                        */
    /* int    n1;       stage1 IFFT size                 */
    /* int    n2;       stage2 IFFT size                 */
    /* double *data;    data pointer                     */
    /* double *work;    working buffer pointer           */
    /* double *data_w;  data internal pointer            */
    /* double *work_w;  working buffer internal pointer  */
    /* double *tw;      twiddle pointer                  */
    /* double *tw_w;    twiddle internal pointer         */
    fft_param_u u,
    void *edmaState,
    int   actualCoreNum
)
{
    int numThread;
    int tid, did, num_of_lines, lines_to_process;
    int error = FFT_NOERR;
    int twsize, tw_offset;
    int i,j,k,ch_i,ch_o,ch_i1,ch_o1,ch_o2,ch_o3,dum0,dum1;
    int insize, outsample, linesize;

    double *restrict in_ptr_f, *restrict out_ptr_f, *restrict tempout;
    double *restrict in_ptr_b, *restrict out_ptr_b;
    double *restrict in_ptr, *restrict out_ptr;
    double *restrict input, *restrict output, *restrict in1, *restrict out1, *dum2, *dum3;
    double *restrict tw, *restrict tw1, *restrict tw2;
    double temp_r, temp_i;
    double twid_r, twid_i, tw_cos, tw_sin, temp;
    lib_memdscr_t **fft_mem_handle = fftGetMemHandle();

    /* input parameters */
    N1      = u.dp_1d_c2r_e.n1;         /* stage 1 size                     */
    N2      = u.dp_1d_c2r_e.n2;         /* stage 2 size                     */
    rad1    = u.dp_1d_c2r_e.para1.rad;  /* stage 1 radix                    */
    rad2    = u.dp_1d_c2r_e.para2.rad;  /* stage 2 radix                    */
    N_p2    = u.dp_1d_c2r_e.para1.N_p2; /* power of 2 size in mixed radix   */
    s_r3    = u.dp_1d_c2r_e.para1.s_r3; /* number stages of radix 3         */
    s_r5    = u.dp_1d_c2r_e.para1.s_r5; /* number stages of radix 5         */
    data    = u.dp_1d_c2r_e.data;       /* data pointer                     */
    out     = u.dp_1d_c2r_e.work;       /* output pointer                   */
    work    = u.dp_1d_c2r_e.tw;         /* working buffer pointer           */
    tw_e    = u.dp_1d_c2r_e.tw+2*N1*N2; /* twiddle pointer                  */
    tw_offset = u.dp_1d_c2r_e.para1.twsize; /* Offset needed for twiddle ptr    */
    edma    = (FFT_EDMA_Struct *)edmaState;
    data_w  = (double*)lib_smem_falloc (fft_mem_handle, 4*N1*IFFT_OMP_DP_1D_C2R_NUMOFLINEBUFS*sizeof(double), 8);
    work_w  = (double*)lib_smem_falloc (fft_mem_handle, 4*N1*IFFT_OMP_DP_1D_C2R_NUMOFLINEBUFS*sizeof(double), 8);
    work_t  = (double*)lib_smem_falloc (fft_mem_handle, 2*N1*IFFT_OMP_DP_1D_C2R_NUMOFLINEBUFS*sizeof(double), 8);
    twsize  = (u.dp_1d_c2r_e.para1.twsize+2*N2 > u.dp_1d_c2r_e.para2.twsize) ? u.dp_1d_c2r_e.para1.twsize+2*N2 : u.dp_1d_c2r_e.para2.twsize;
#ifdef FFT_MEM_MODEL_LG
    tw_w    = (double*)lib_smem_falloc (fft_mem_handle, twsize*sizeof(double), 8);
#endif

#ifdef FFT_MEM_MODEL_LG
    if(data_w==NULL || work_w==NULL || work_t==NULL || tw_w==NULL) {
#else
    tw_w = NULL;
    if(data_w==NULL || work_w==NULL || work_t==NULL) {
#endif
        printf("Memory allocation error!\n");
        return FFT_ERROR;
    }

    if ((actualCoreNum==1)||
        (actualCoreNum==2)||
        (actualCoreNum==4)||
        (actualCoreNum==8))
      omp_set_num_threads (actualCoreNum);
    else
      omp_set_num_threads (OMP_MAX_NUM_CORES);
    numThread = omp_get_max_threads();


    num_of_lines = IFFT_OMP_DP_1D_C2R_NUMOFLINEBUFS;
    lines   = (N2/numThread/num_of_lines)*num_of_lines;

    if ((lines*numThread < N2) && (lines > 0))
      lines += num_of_lines;

    if (lines*numThread > N2)
      lines -= num_of_lines;
    twsize    = (u.dp_1d_c2r_e.para1.twsize + 2*N2)*sizeof(double);
    insize    = N1*2*sizeof(double);
    outsample = N1;
    linesize  = N2*2*sizeof(double);

    /* Bring in the 1st and N2/2+1 column since they're special */
    in_ptr_f  = &data[0];         /* pointer starts at the 1st column the N1xN2 matrix */
    out_ptr_f = &work[0];
    in_ptr_b  = &data[N2];        /* pointer starts at the N2/2 column of the N1xN2 matrix */
    out_ptr_b = &work[N2];

    in1    = data_w;
    out1   = work_w;
    input  = &data_w[N1*num_of_lines/2];
    output = &work_w[N1*num_of_lines/2];


    tw = tw_e;
    if (tw_w != NULL) {
      /* prepare twiddle buffer */
      lib_emt_copy1D1D(
        ((edma)->channel[FFT_XFER_CHANNEL3]),
        (void *)tw_e,
        (void *)tw_w,
        twsize);
      tw = tw_w;
    }
    tid = omp_get_thread_num();     // Obtain thread number
    tw2 = tw + tw_offset + 2*tid*lines;

    /* in1 has half of the first column */
    lib_emt_copy2D1D(
      ((edma)->channel[FFT_XFER_CHANNEL0]),
      (void *)in_ptr_f,
      (void *)in1,
      2*sizeof(double),
      outsample,
      linesize);

    /* input has half of the N2/2+1 column */
    lib_emt_copy2D1D(
      ((edma)->channel[FFT_XFER_CHANNEL1]),
      (void *)in_ptr_b,
      (void *)input,
      2*sizeof(double),
      outsample,
      linesize);

    /* wait for first IFFT input */
    if (tw_w != NULL) {
      lib_emt_wait(((edma)->channel[FFT_XFER_CHANNEL3]));
    }
    lib_emt_wait(((edma)->channel[FFT_XFER_CHANNEL0]));

    /* reconstruct the first column */
    for(i = 1; i < N1/2; i++){
      in1[(N1-i)*2]   =  in1[2*i];
      in1[(N1-i)*2+1] = -in1[2*i+1];
    }

    if (s_r3 == 0 && s_r5 == 0) {
        DSPF_dp_ifftDPxDP (N1, &in1[0], tw, &out1[0], rad1, 0, N1);
    } else {
        ifft_DPxDP_mixed_radix (N1, &in1[0], tw, &out1[0], N_p2, s_r3, s_r5, rad1);
    }

    tw_cos = tw2[0];
    tw_sin = tw2[1];
    twid_r = tw_cos;
    twid_i = tw_sin;
    for (j = 2; j < 2*N1; j+=2) {
      temp_r = out1[j];
      temp_i = out1[j+1];
      out1[j]   = temp_r*twid_r - temp_i*twid_i;
      out1[j+1] = temp_r*twid_i + temp_i*twid_r;
      temp   = tw_cos*twid_r - tw_sin*twid_i;
      twid_i = tw_sin*twid_r + tw_cos*twid_i;
      twid_r = temp;
    }

    /* export output data of first IFFT, transposed */
    lib_emt_copy1D2D(
      ((edma)->channel[FFT_XFER_CHANNEL0]),
      (void *)out1,
      (void *)out_ptr_f,
      2*sizeof(double),
      outsample,
      linesize);

    /* wait for the N2/2+1 column */
    lib_emt_wait(((edma)->channel[FFT_XFER_CHANNEL1]));

    /* reconstruct the N2/2+1 column */
    for(i = 0; i < N1/2; i++){
      input[(N1-1-i)*2]   =  input[2*i];
      input[(N1-1-i)*2+1] = -input[2*i+1];
    }

    if (s_r3 == 0 && s_r5 == 0) {
        DSPF_dp_ifftDPxDP (N1, &input[0], tw, &output[0], rad1, 0, N1);
    } else {
        ifft_DPxDP_mixed_radix (N1, &input[0], tw, &output[0], N_p2, s_r3, s_r5, rad1);
    }

    tw_cos = tw2[N2];
    tw_sin = tw2[N2+1];
    twid_r = tw_cos;
    twid_i = tw_sin;
    for (j = 2; j < 2*N1; j+=2) {
      temp_r = output[j];
      temp_i = output[j+1];
      output[j]   = temp_r*twid_r - temp_i*twid_i;
      output[j+1] = temp_r*twid_i + temp_i*twid_r;
      temp   = tw_cos*twid_r - tw_sin*twid_i;
      twid_i = tw_sin*twid_r + tw_cos*twid_i;
      twid_r = temp;
    }

    /* export output data of first IFFT, transposed */
    lib_emt_copy1D2D(
      ((edma)->channel[FFT_XFER_CHANNEL1]),
      (void *)output,
      (void *)out_ptr_b,
      2*sizeof(double),
      outsample,
      linesize);

    /* wait for the 1st and N2/2+1 column */
    lib_emt_wait(((edma)->channel[FFT_XFER_CHANNEL0]));
    lib_emt_wait(((edma)->channel[FFT_XFER_CHANNEL1]));


    /* from 2nd colume,
     * at the 1st core,  combine the 2nd and the N2th column together to reconstruct 2 columns
     *                   combine the 3rd and the (N2-1)th column,
     * at the 2nd core, combine the 4rd and the (N2-2)th column,
     *                   combine the 5rd and the (N2-3)th column,
     * at the 3rd core, combine the 6th and the (N2-4)th column
     *                   combine the 7rd and the (N2-5)th column,and so on
     * still reading in 4 lines per core and compute 4 lines per core at the same time, but the 2 front lines
     * and 2 back lines need to be done separately
     * */
#pragma omp parallel private(tw, tw1, tw2, tempout, in_ptr_f, in_ptr_b, out_ptr_f, out_ptr_b, input, output, in1, out1, dum2, dum3, did, i, \
		i,j,k,ch_i,ch_o,ch_i1,ch_o1,ch_o2,ch_o3,dum0,dum1, temp_r, temp_i, twid_r, twid_i, tw_cos, tw_sin, temp, tid, lines_to_process) shared(num_of_lines, \
        rad1, rad2, data, work, data_w, work_w, work_t, tw_e, tw_w, N1, N2, lines, edma, twsize, insize, outsample, \
        tw_offset, linesize, N_p2, s_r3, s_r5)
  {
    tid = omp_get_thread_num();     // Obtain thread number
    did = DNUM;
    lines_to_process = lines;

    if(tid == numThread - 1)
      lines_to_process = N2 - lines*tid;

    if(lines < 1){
      if (tid < (N2/num_of_lines))
        lines_to_process = num_of_lines;
      else
    	lines_to_process = 0;
    }

    if (lines_to_process > 0){

    tw = tw_e;
    if (tw_w != NULL) {
      /* prepare twiddle buffer */
      lib_emt_copy1D1D(
        ((edma+did)->channel[FFT_XFER_CHANNEL3]), 
        (void *)tw_e,
        (void *)tw_w,
        twsize);
      tw = tw_w;
    }
    /* tw2 for front columns */
    tw2 = tw + tw_offset + num_of_lines*tid + 2;

    _nassert(N1 >= 8);
    _nassert(N2 >= 8);

    /* each core brings in 2 columns at front and 2 columns at the back for the matrix at each iteration */

    in_ptr_f  = &data[(1+(num_of_lines/2)*tid)*2];         /* pointer starts at the left side of the N1xN2 matrix */
    in_ptr_b  = &data[(N2-(1+tid)*num_of_lines/2)*2];    /* pointer starts at the right side of the N1xN2 matrix */
    out_ptr_f = &work[(1+(num_of_lines/2)*tid)*2];
    out_ptr_b = &work[(N2-(1+tid)*num_of_lines/2)*2];
    tempout   = work_t;

    /* stage 1: transpose, n2 IFFT's of size n1, apply global twiddle, transpose */
    /* prepare first IFFT input buffer */

    /* Brings in the front 2 columns */
    lib_emt_copy2D1D(
      ((edma+did)->channel[FFT_XFER_CHANNEL0]), 
      (void *)in_ptr_f,
      (void *)data_w,
      2*sizeof(double)*num_of_lines/2,
      outsample,
      linesize);

    lib_emt_wait(((edma+did)->channel[FFT_XFER_CHANNEL0]));

    lib_emt_copy2D1D(
      ((edma+did)->channel[FFT_XFER_CHANNEL0]),
      (void *)in_ptr_b,
      (void *)(data_w+N1*num_of_lines),
      2*sizeof(double)*num_of_lines/2,
      outsample,
      linesize);

    if((lines > num_of_lines)){
      in_ptr_f += num_of_lines*numThread;
      in_ptr_b -= num_of_lines*numThread;
    }
    else {
      in_ptr_f += num_of_lines;
      in_ptr_b -= num_of_lines;
    }

    lib_emt_wait(((edma+did)->channel[FFT_XFER_CHANNEL0]));

    /* construct the full signal based on the 2 parts */

    /* first do the transpose, front, back separately */
    for (i = 0; i<num_of_lines/2; i++){
      for (j = 0; j<N1/2; j++){
    	tempout[j*2+i*N1*2]   = data_w[j*4+i*2];
    	tempout[j*2+i*N1*2+1] = data_w[j*4+i*2+1];
      }
    }

    for (i = 0; i<num_of_lines/2; i++){
      for (j = 0; j<N1/2; j++){
    	tempout[j*2+i*N1*2+num_of_lines*N1]   = data_w[j*4+i*2+num_of_lines*N1];
    	tempout[j*2+i*N1*2+num_of_lines*N1+1] = data_w[j*4+i*2+num_of_lines*N1+1];
      }
    }

    for (i = 0; i<num_of_lines/2; i++){
      for (j = 0; j<N1/2; j++){
   	    tempout[(N1-1-j)*2+i*N1*2]     =  tempout[(3*N1+j)*2-i*N1*2];
    	tempout[(N1-1-j)*2+i*N1*2+1]   = -tempout[(3*N1+j)*2-i*N1*2+1];
    	}
    }

    /* set pingpong buffer pointers */
    in1    = data_w;
    out1   = work_w;
    input  = &data_w[2*N1*num_of_lines];
    output = &work_w[2*N1*num_of_lines];

    /* prepare second IFFT input buffer */
    /* bring in the front columns */
    lib_emt_copy2D1D(
      ((edma+did)->channel[FFT_XFER_CHANNEL2]), 
      (void *)in_ptr_f,
      (void *)input,
      2*sizeof(double)*num_of_lines/2,
      outsample,
      linesize);

    /* wait for front columns */
    lib_emt_wait(((edma+did)->channel[FFT_XFER_CHANNEL2]));

    /* bring in the back columns */
    lib_emt_copy2D1D(
      ((edma+did)->channel[FFT_XFER_CHANNEL2]),
      (void *)in_ptr_b,
      (void *)(input+num_of_lines*N1),
      2*sizeof(double)*num_of_lines/2,
      outsample,
      linesize);

    if((lines > 2*num_of_lines)){
      in_ptr_f += num_of_lines*numThread;
      in_ptr_b -= num_of_lines*numThread;
    }
    else {
      in_ptr_f += num_of_lines;
      in_ptr_b -= num_of_lines;
    }


    /* wait for first IFFT input */
//    lib_emt_wait(((edma+did)->channel[FFT_XFER_CHANNEL2]));


#if (IFFT_OMP_DP_1D_C2R_NUMOFLINEBUFS>1)
    /* first ifft */
    if (s_r3 == 0 && s_r5 == 0) {
      for (i=0; i<num_of_lines/2; i++)
        DSPF_dp_ifftDPxDP (N1, &tempout[i*N1*2], tw, &in1[i*N1*2], rad1, 0, N1);
    } else {
      for (i=0; i<num_of_lines/2; i++)
        ifft_DPxDP_mixed_radix (N1, &tempout[i*N1*2], tw, &in1[i*N1*2], N_p2, s_r3, s_r5, rad1);
    }

    for (i=0; i<num_of_lines/2; i++) {
      tw_cos = tw2[2*i];
      tw_sin = tw2[2*i+1];
      twid_r = tw_cos;
      twid_i = tw_sin;
      for (j = 2; j < 2*N1; j+=2) {
          temp_r = in1[i*N1*2+j];
          temp_i = in1[i*N1*2+j+1];
          in1[i*N1*2+j]   = temp_r*twid_r - temp_i*twid_i;
          in1[i*N1*2+j+1] = temp_r*twid_i + temp_i*twid_r;
          temp   = tw_cos*twid_r - tw_sin*twid_i;
          twid_i = tw_sin*twid_r + tw_cos*twid_i;
          twid_r = temp;
      }
    }

    /* conjugate the number to form the back columns */

    for (i=0; i<num_of_lines/2; i++){
      for (j=0; j<N1; j++){
        in1[(num_of_lines-i-1)*N1*2+2*j]   =  in1[i*N1*2+2*j];
        in1[(num_of_lines-i-1)*N1*2+2*j+1] = -in1[i*N1*2+2*j+1];
      }
    }

    if ((lines > num_of_lines) || tid < (N2/num_of_lines-1)){
      /* local transpose */
      DSPF_dp_mat_trans_cplx(in1, num_of_lines/2, N1, out1);
      DSPF_dp_mat_trans_cplx(&in1[num_of_lines*N1], num_of_lines/2, N1, &out1[num_of_lines*N1]);
    }
    else {
      /* copy to output*/

      for (i = 0; i<N1; i++){
        out1[2*i]   = in1[2*i];
        out1[2*i+1] = in1[2*i+1];
        out1[num_of_lines*N1+2*i]   =  in1[2*i];
        out1[num_of_lines*N1+2*i+1] = -in1[2*i+1];
      }
   }

#else
/* if won't work for this case */
#endif

    if(lines_to_process > num_of_lines)
    {
      /* export output data of first front IFFT, transposed */
      lib_emt_copy1D2D(
        ((edma+did)->channel[FFT_XFER_CHANNEL1]),
        (void *)out1,
        (void *)out_ptr_f,
        2*sizeof(double)*num_of_lines/2,
        outsample,
        linesize);
      out_ptr_f += num_of_lines * numThread;
      /* wait for EDMA */
      lib_emt_wait(((edma+did)->channel[FFT_XFER_CHANNEL1]));

      /* export output data of first back IFFT, transposed */
      lib_emt_copy1D2D(
      ((edma+did)->channel[FFT_XFER_CHANNEL4]),
      (void *)(out1+num_of_lines*N1),
      (void *)out_ptr_b,
      2*sizeof(double)*num_of_lines/2,
      outsample,
      linesize);

      out_ptr_b -= num_of_lines * numThread;
      /* wait for EDMA */
      lib_emt_wait(((edma+did)->channel[FFT_XFER_CHANNEL4]));
    }
    else {
      if(tid < (N2/num_of_lines - 1)){

        lib_emt_copy1D2D(
          ((edma+did)->channel[FFT_XFER_CHANNEL1]),
           (void *)out1,
           (void *)out_ptr_f,
           2*sizeof(double)*num_of_lines/2,
           outsample,
           linesize);
//        out_ptr_f += num_of_lines * (N2/num_of_lines);
        lib_emt_wait(((edma+did)->channel[FFT_XFER_CHANNEL1]));

        lib_emt_copy1D2D(
          ((edma+did)->channel[FFT_XFER_CHANNEL4]),
           (void *)(out1+N1*num_of_lines),
           (void *)out_ptr_b,
           2*sizeof(double)*num_of_lines/2,
           outsample,
           linesize);

//        out_ptr_b -= num_of_lines * (N2/num_of_lines);
        lib_emt_wait(((edma+did)->channel[FFT_XFER_CHANNEL4]));
      }
      else{

        lib_emt_copy1D2D(
          ((edma+did)->channel[FFT_XFER_CHANNEL1]),
           (void *)out1,
           (void *)out_ptr_f,
           2*sizeof(double),
           outsample,
           linesize);
        lib_emt_wait(((edma+did)->channel[FFT_XFER_CHANNEL1]));

        lib_emt_copy1D2D(
          ((edma+did)->channel[FFT_XFER_CHANNEL4]),
           (void *)(out1+N1*num_of_lines),
           (void *)(out_ptr_b+2),
           2*sizeof(double),
           outsample,
           linesize);
        lib_emt_wait(((edma+did)->channel[FFT_XFER_CHANNEL4]));

      }
  }

#ifdef USEFASTEDMA
  /* fill the PaRAM for CHANNEL 3 without starting it */
  lib_emt_copy1D2D_Fill(
    ((edma+did)->channel[FFT_XFER_CHANNEL3]),
    (void *)out1,
    (void *)out_ptr,
    2*sizeof(double)*num_of_lines,
    outsample,
    linesize);
#endif

    /* prepare third IFFT input buffer */
    lib_emt_copy2D1D(
      ((edma+did)->channel[FFT_XFER_CHANNEL0]), 
      (void *)in_ptr_f,
      (void *)data_w,
      2*sizeof(double)*num_of_lines/2,
      outsample,
      linesize);
    /* wait for EDMA */
    lib_emt_wait(((edma+did)->channel[FFT_XFER_CHANNEL0]));

    lib_emt_copy2D1D(
      ((edma+did)->channel[FFT_XFER_CHANNEL0]),
      (void *)in_ptr_b,
      (void *)(data_w+num_of_lines*N1),
      2*sizeof(double)*num_of_lines/2,
      outsample,
      linesize);

    if((lines > 3*num_of_lines)){
      in_ptr_f += num_of_lines*numThread;
      in_ptr_b -= num_of_lines*numThread;
    }
    else {
      in_ptr_f += num_of_lines;
      in_ptr_b -= num_of_lines;
    }

    /* set channel numbers */
    ch_i  = FFT_XFER_CHANNEL2;
    ch_o  = FFT_XFER_CHANNEL3;
    ch_i1 = FFT_XFER_CHANNEL0;
    ch_o1 = FFT_XFER_CHANNEL1;
    ch_o2 = FFT_XFER_CHANNEL4;
    ch_o3 = FFT_XFER_CHANNEL5;

    /* wait for EDMA */
    lib_emt_wait(((edma+did)->channel[ch_i]));

#if (IFFT_OMP_DP_1D_C2R_NUMOFLINEBUFS>1)
    for (k = num_of_lines; k < lines_to_process; k += num_of_lines) {
      /* local transpose */
      /* construct the full signal based on the 2 parts */
      if (k < lines)
        tw2   = tw2 + num_of_lines * numThread;
      else
        tw2   = tw2 + num_of_lines;

      /* first do the transpose, front, back separately */
      for (i = 0; i<num_of_lines/2; i++){
        for (j = 0; j<N1/2; j++){
      	tempout[j*2+i*N1*2]   = input[j*4+i*2];
      	tempout[j*2+i*N1*2+1] = input[j*4+i*2+1];
        }
      }

      for (i = 0; i<num_of_lines/2; i++){
        for (j = 0; j<N1/2; j++){
      	tempout[j*2+i*N1*2+num_of_lines*N1]   = input[j*4+i*2+num_of_lines*N1];
      	tempout[j*2+i*N1*2+num_of_lines*N1+1] = input[j*4+i*2+num_of_lines*N1+1];
        }
      }

      for (i = 0; i<num_of_lines/2; i++){
        for (j = 0; j<N1/2; j++){
     	  tempout[(N1-1-j)*2+i*N1*2]     =  tempout[(3*N1+j)*2-i*N1*2];
      	  tempout[(N1-1-j)*2+i*N1*2+1]   = -tempout[(3*N1+j)*2-i*N1*2+1];
      	}
      }

      if (s_r3 == 0 && s_r5 == 0) {
        for (i=0; i<num_of_lines/2; i++)
          DSPF_dp_ifftDPxDP (N1, &tempout[i*N1*2], tw, &input[i*N1*2], rad1, 0, N1);
      } else {
        for (i=0; i<num_of_lines/2; i++)
          ifft_DPxDP_mixed_radix (N1, &tempout[i*N1*2], tw, &input[i*N1*2], N_p2, s_r3, s_r5, rad1);
      }

      for (i=0; i<num_of_lines/2; i++) {
        tw_cos = tw2[2*i];
        tw_sin = tw2[2*i+1];
        twid_r = tw_cos;
        twid_i = tw_sin;
        for (j = 2; j < 2*N1; j+=2) {
            temp_r = input[i*N1*2+j];
            temp_i = input[i*N1*2+j+1];
            input[i*N1*2+j]   = temp_r*twid_r - temp_i*twid_i;
            input[i*N1*2+j+1] = temp_r*twid_i + temp_i*twid_r;
            temp   = tw_cos*twid_r - tw_sin*twid_i;
            twid_i = tw_sin*twid_r + tw_cos*twid_i;
            twid_r = temp;
        }
      }

      /* conjugate the number to form the back columns */

      for (i=0; i<num_of_lines/2; i++){
        for (j=0; j<N1; j++){
          input[(num_of_lines-i-1)*N1*2+2*j]   =  input[i*N1*2+2*j];
          input[(num_of_lines-i-1)*N1*2+2*j+1] = -input[i*N1*2+2*j+1];
        }
      }

      if ((k < (lines_to_process - num_of_lines)) || tid < (numThread-1)){
        /* local transpose */
        DSPF_dp_mat_trans_cplx(input, num_of_lines/2, N1, output);
        DSPF_dp_mat_trans_cplx(&input[num_of_lines*N1], num_of_lines/2, N1, &output[num_of_lines*N1]);
      }
      else{
        /* copy to the output */
        for (i = 0; i<N1; i++){
          output[2*i]   = input[2*i];
          output[2*i+1] = input[2*i+1];
          output[num_of_lines*N1+2*i]   =  input[2*i];
          output[num_of_lines*N1+2*i+1] = -input[2*i+1];
        }

      }
#else
/* it won't work in this case */
#endif
      /* export output data, transposed */
    if(( k < (lines - num_of_lines) ) || (tid < (numThread-1))){

      lib_emt_copy1D2D(
        ((edma+did)->channel[ch_o]), 
        (void *)output,
        (void *)out_ptr_f,
        2*sizeof(double)*num_of_lines/2,
        outsample,
        linesize);
      out_ptr_f += num_of_lines * numThread;
      lib_emt_wait(((edma+did)->channel[ch_o]));

      lib_emt_copy1D2D(
        ((edma+did)->channel[ch_o2]),
        (void *)(output+N1*num_of_lines),
        (void *)out_ptr_b,
        2*sizeof(double)*num_of_lines/2,
        outsample,
        linesize);

      out_ptr_b -= num_of_lines * numThread;

      lib_emt_wait(((edma+did)->channel[ch_o2]));
    }
    else{
      if ((lines < lines_to_process) && (k < (lines_to_process - num_of_lines))) {

    	if ((k == lines) && (lines == num_of_lines)) {
      	  out_ptr_f -= num_of_lines * (numThread - 1);
    	  out_ptr_b += num_of_lines * (numThread - 1);
    	}

        lib_emt_copy1D2D(
          ((edma+did)->channel[ch_o]),
          (void *)output,
          (void *)out_ptr_f,
          2*sizeof(double)*num_of_lines/2,
          outsample,
          linesize);
        out_ptr_f += num_of_lines;
        lib_emt_wait(((edma+did)->channel[ch_o]));

        lib_emt_copy1D2D(
          ((edma+did)->channel[ch_o2]),
          (void *)(output+N1*num_of_lines),
          (void *)(out_ptr_b),
          2*sizeof(double)*num_of_lines/2,
          outsample,
          linesize);
        out_ptr_b -= num_of_lines;
        lib_emt_wait(((edma+did)->channel[ch_o2]));
      }else {

        lib_emt_copy1D2D(
          ((edma+did)->channel[ch_o]),
          (void *)output,
          (void *)out_ptr_f,
          2*sizeof(double),
          outsample,
          linesize);
          lib_emt_wait(((edma+did)->channel[ch_o]));

         lib_emt_copy1D2D(
          ((edma+did)->channel[ch_o2]),
          (void *)(output+N1*num_of_lines),
          (void *)(out_ptr_b+2),
          2*sizeof(double),
          outsample,
          linesize);
          lib_emt_wait(((edma+did)->channel[ch_o2]));
        }
    }

      /* prepare input data */
      lib_emt_copy2D1D(
        ((edma+did)->channel[ch_i]), 
        (void *)in_ptr_f,
        (void *)input,
        2*sizeof(double)*num_of_lines/2,
        outsample,
        linesize);
      lib_emt_wait(((edma+did)->channel[ch_i]));

      /* prepare input data */
      lib_emt_copy2D1D(
        ((edma+did)->channel[ch_i]),
        (void *)in_ptr_b,
        (void *)(input+N1*num_of_lines),
        2*sizeof(double)*num_of_lines/2,
        outsample,
        linesize);

      if (((lines > 4*num_of_lines) && (k < (lines - 3*num_of_lines))) || (lines_to_process == lines)) {
        in_ptr_f += num_of_lines * numThread;
        in_ptr_b -= num_of_lines * numThread;
      }
      else {
        in_ptr_f += num_of_lines;
        in_ptr_b -= num_of_lines;
      }

      /* swap pingpong buffer pointers and XFER channel */
      dum2   = input;
      input  = in1;
      in1    = dum2;
      dum3   = output;
      output = out1;
      out1   = dum3;

      dum0   = ch_i;
      ch_i   = ch_i1;
      ch_i1  = dum0;
      dum1   = ch_o;
      ch_o   = ch_o1;
      ch_o1  = dum1;
      dum1   = ch_o2;
      ch_o2  = ch_o3;
      ch_o3  = dum1;

      /* wait for EDMA */
      lib_emt_wait(((edma+did)->channel[ch_i]));
      lib_emt_wait(((edma+did)->channel[ch_o]));
      lib_emt_wait(((edma+did)->channel[ch_o2]));
    }

    /********************************************************* end of the first stage ***********************************************************************************/
    /* wait for stage1 to complete */
    lib_emt_wait(((edma+did)->channel[ch_i1]));
    lib_emt_wait(((edma+did)->channel[ch_o1]));
    lib_emt_wait(((edma+did)->channel[ch_o3]));
    }
  }

  /* stage 2: n1 IFFT's of size n2, transpose */
  num_of_lines = IFFT_OMP_DP_1D_C2R_NUMOFLINEBUFS;
  lines     = (N1/numThread/num_of_lines)*num_of_lines;
  if ((lines*numThread < N1) && (lines > 0))
    lines += num_of_lines;

  if (lines*numThread > N1)
    lines -= num_of_lines;
  twsize    = (u.dp_1d_c2r_e.para2.twsize)*sizeof(double);
  insize    = N2*2*sizeof(double)*num_of_lines;
  outsample = N2;
  linesize  = N1*sizeof(double);
  N_p2      = u.dp_1d_c2r_e.para2.N_p2;
  s_r3      = u.dp_1d_c2r_e.para2.s_r3;
  s_r5      = u.dp_1d_c2r_e.para2.s_r5;

#pragma omp parallel private(tid, tw, tw1, tw2, in_ptr, out_ptr, tempout, input, output, in1, out1, dum2, dum3, did, i, \
		i,j,ch_i,ch_o,ch_i1,ch_o1,dum0,dum1, temp_r, temp_i, twid_r, twid_i, tw_cos, tw_sin, temp, lines_to_process) shared(num_of_lines, \
        rad1, rad2, out, work, data_w, work_w, work_t, tw_e, tw_w, N1, N2, lines, edma, twsize, insize, outsample, \
        tw_offset, linesize, N_p2, s_r3, s_r5)
  {
    tid = omp_get_thread_num();     // Obtain thread number
    did = DNUM;
    lines_to_process = lines;
    if(tid == numThread - 1)
      lines_to_process = N1 - lines*tid;

    tw1 = tw_e + tw_offset + 2*N2;
    if (tw_w != NULL) {
      /* prepare twiddle buffer */
      lib_emt_copy1D1D(
        ((edma+did)->channel[FFT_XFER_CHANNEL3]),
        (void *)(tw_e + tw_offset + 2*N2),
        (void *)tw_w,
        twsize);
      tw1 = tw_w;
    }

    in_ptr  = &work[tid*(N2*2*lines)];
    out_ptr = &out[tid*lines];

    /* prepare first IFFT input buffer */
#ifndef USEFASTEDMA
    lib_emt_copy1D1D(
      ((edma+did)->channel[FFT_XFER_CHANNEL0]),
      (void *)in_ptr,
      (void *)work_w,
      insize);
#else
  lib_emt_copy1D1D_Fast(
    ((edma+did)->channel[FFT_XFER_CHANNEL0]),
    (void *)in_ptr,
    (void *)work_w);
#endif
    in_ptr += 2*N2*num_of_lines;

    /* set pingpong buffer pointers */
    in1    = work_w;
    out1   = data_w;
    input  = &work_w[2*N2*num_of_lines];
    output = &data_w[N2*num_of_lines];
    tempout = work_t;

    /* prepare second IFFT input buffer */
#ifndef USEFASTEDMA
      lib_emt_copy1D1D(
        ((edma+did)->channel[FFT_XFER_CHANNEL2]),
        (void *)in_ptr,
        (void *)input,
        insize);
#else
      lib_emt_copy1D1D_Fast(
        ((edma+did)->channel[FFT_XFER_CHANNEL2]),
        (void *)in_ptr,
        (void *)input);
#endif
    in_ptr += 2*N2*num_of_lines;

    /* wait for first IFFT input */
    lib_emt_wait(((edma+did)->channel[FFT_XFER_CHANNEL0]));
#if (IFFT_OMP_DP_1D_C2R_NUMOFLINEBUFS>1)
      if (s_r3 == 0 && s_r5 == 0) {
        for (i=0; i<num_of_lines; i++){      
          DSPF_dp_ifftDPxDP_c2r (N2, &in1[i*N2*2], tw1, &tempout[i*N2], rad2, 0, N2);
        }
      }
      else {
        for (i=0; i<num_of_lines; i++){      
          ifft_DPxDP_mixed_radix_c2r (N2, &in1[i*N2*2], tw1, &tempout[i*N2], N_p2, s_r3, s_r5, rad2);
        }
      }      

    DSPF_dp_mat_trans(tempout, num_of_lines, N2, out1);
#else
    if (s_r3 == 0 && s_r5 == 0) {  
      DSPF_dp_ifftDPxDP_c2r (N2, in1, tw1, out1, rad2, 0, N2);
    }
    else {
      ifft_DPxDP_mixed_radix_c2r (N2, in1, tw1, out1, N_p2, s_r3, s_r5, rad2);
    }      
#endif 

    /* export output data of first IFFT, transposed */
#ifndef USEFASTEDMA
    lib_emt_copy1D2D(
      ((edma+did)->channel[FFT_XFER_CHANNEL1]),
      (void *)out1,
      (void *)out_ptr,
      sizeof(double)*num_of_lines,
      outsample,
      linesize);
#else
    lib_emt_copy1D2D_Fast
    (
      ((edma+did)->channel[FFT_XFER_CHANNEL1]),
        (void *)out1,
        (void *)out_ptr);
#endif
    out_ptr += 1*num_of_lines;

    /* prepare third IFFT input buffer */
#ifndef USEFASTEDMA
    lib_emt_copy1D1D(
      ((edma+did)->channel[FFT_XFER_CHANNEL0]),
      (void *)in_ptr,
      (void *)work_w,
      insize);
#else
  lib_emt_copy1D1D_Fast(
    ((edma+did)->channel[FFT_XFER_CHANNEL0]),
    (void *)in_ptr,
    (void *)work_w);
#endif
    in_ptr += 2*N2*num_of_lines;

    /* set channel numbers */
    ch_i  = FFT_XFER_CHANNEL2;
    ch_o  = FFT_XFER_CHANNEL3;
    ch_i1 = FFT_XFER_CHANNEL0;
    ch_o1 = FFT_XFER_CHANNEL1;

    /* wait for EDMA */
    lib_emt_wait(((edma+did)->channel[ch_i]));

    for (j = 1; j < lines_to_process/num_of_lines; j++) {

#if (IFFT_OMP_DP_1D_C2R_NUMOFLINEBUFS>1)
    for (i=0; i<num_of_lines; i++)
    {
      if (s_r3 == 0 && s_r5 == 0) {  
        DSPF_dp_ifftDPxDP_c2r (N2, &input[i*N2*2], tw1, &tempout[i*N2], rad2, 0, N2);
      }
      else {
        ifft_DPxDP_mixed_radix_c2r (N2, &input[i*N2*2], tw1, &tempout[i*N2], N_p2, s_r3, s_r5, rad2);
      }        
    }
  
    DSPF_dp_mat_trans(tempout, num_of_lines, N2, output);
#else
    if (s_r3 == 0 && s_r5 == 0) {  
      DSPF_dp_ifftDPxDP_c2r (N2, input, tw1, output, rad2, 0, N2);
    }
    else {
      ifft_DPxDP_mixed_radix_c2r (N2, input, tw1, output, N_p2, s_r3, s_r5, rad2);
    }      
#endif   

      /* export output data, transposed */
#ifndef USEFASTEDMA
      lib_emt_copy1D2D(
        ((edma+did)->channel[ch_o]),
        (void *)output,
        (void *)out_ptr,
        sizeof(double)*num_of_lines,
        outsample,
        linesize);
#else
    lib_emt_copy1D2D_Fast
    (
      ((edma+did)->channel[ch_o]),
        (void *)output,
        (void *)out_ptr);
#endif
      out_ptr += 1*num_of_lines;

      /* prepare input data */
#ifndef USEFASTEDMA
      lib_emt_copy1D1D(
        ((edma+did)->channel[ch_i]),
        (void *)in_ptr,
        (void *)input,
        insize);

#else
      lib_emt_copy1D1D_Fast(
        ((edma+did)->channel[ch_i]),
        (void *)in_ptr,
        (void *)input);
#endif
      in_ptr += 2*N2*num_of_lines;

      /* swap pingpong buffer pointers and XFER channel */
      dum2   = input;
      input  = in1;
      in1    = dum2;
      dum3   = output;
      output = out1;
      out1   = dum3;

      dum0   = ch_i;
      ch_i   = ch_i1;
      ch_i1  = dum0;
      dum1   = ch_o;
      ch_o   = ch_o1;
      ch_o1  = dum1;

      /* wait for EDMA */
      lib_emt_wait(((edma+did)->channel[ch_i]));
      lib_emt_wait(((edma+did)->channel[ch_o]));
    }

    /* wait for stage1 to complete */
    lib_emt_wait(((edma+did)->channel[ch_i1]));
    lib_emt_wait(((edma+did)->channel[ch_o1]));
  }
  omp_set_num_threads (OMP_MAX_NUM_CORES);

  return error;
} /* ifft_omp_dp_1d_c2r_ecpy */

/* ======================================================================== */
/*  End of file:  ifft_omp_dp_1d_c2r.c                                      */
/* ------------------------------------------------------------------------ */
/*            Copyright (c) 2013 Texas Instruments, Incorporated.           */
/*                           All Rights Reserved.                           */
/* ======================================================================== */
