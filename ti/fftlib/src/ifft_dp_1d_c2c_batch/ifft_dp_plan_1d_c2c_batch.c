/* ======================================================================= */
/*  TEXAS INSTRUMENTS, INC.                                                */
/*                                                                         */
/*  FFTLIB  FFT Library                                                    */
/*                                                                         */
/* Copyright (C) 2013 Texas Instruments Incorporated - http://www.ti.com/  */ 
/*                                                                         */
/*                                                                         */
/*  Redistribution and use in source and binary forms, with or without     */
/*  modification, are permitted provided that the following conditions     */
/*  are met:                                                               */
/*                                                                         */
/*    Redistributions of source code must retain the above copyright       */
/*    notice, this list of conditions and the following disclaimer.        */
/*                                                                         */
/*    Redistributions in binary form must reproduce the above copyright    */
/*    notice, this list of conditions and the following disclaimer in the  */
/*    documentation and/or other materials provided with the               */
/*    distribution.                                                        */
/*                                                                         */
/*    Neither the name of Texas Instruments Incorporated nor the names of  */
/*    its contributors may be used to endorse or promote products derived  */
/*    from this software without specific prior written permission.        */
/*                                                                         */
/*  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS    */
/*  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT      */
/*  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR  */
/*  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT   */
/*  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,  */
/*  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT       */
/*  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,  */
/*  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY  */
/*  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT    */
/*  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE  */
/*  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.   */
/*                                                                         */
/* ======================================================================= */

#include <math.h>
#include <stdio.h>
#include <c6x.h>
#include <ti/dsplib/src/DSPF_dp_fftDPxDP/DSPF_dp_fftDPxDP.h>
#include "ifft_dp_1d_c2c_batch.h"
#include <ti/csl/csl_cacheAux.h>

/* twiddle generation */
static void twiddle_gen (int N, int M, int rad, int use_bs, int s_r3, int s_r5, int N_p2, double *tw, double *Bn, double *work)
{
	int     k, n, j, i;
	double *bn, *Bk;
    const double PI = 3.14159265358979323846;

    k = 0;
    if (use_bs == 0)
    {   /* first generate radix 2&4 twiddles */
        for (j = 1; j <= N_p2 >> 2; j = j << 2)
        {
          for (i = 0; i < N_p2 >> 2; i += j)
          {
             tw[k]     = cos (2 * PI * i / N_p2);
             tw[k + 1] = sin (2 * PI * i / N_p2);
             tw[k + 2] = cos (4 * PI * i / N_p2);
             tw[k + 3] = sin (4 * PI * i / N_p2);
             tw[k + 4] = cos (6 * PI * i / N_p2);
             tw[k + 5] = sin (6 * PI * i / N_p2);
             k += 6;
          }
        }

        if ((s_r3 != 0) || (s_r5 != 0)) {
          /* generate twiddles for radix-3 loop */
          k = N_p2*2;
          n = N;
          for (i = 0; i < s_r3; i++) {
            for (j = 0; j < n/3; j++) {
              tw[k]     =  cos (2 * PI * j / n);
              tw[k + 1] = -sin (2 * PI * j / n);
              tw[k + 2] =  cos (4 * PI * j / n);
              tw[k + 3] = -sin (4 * PI * j / n);
              k += 4;
            }
            n /= 3;
          }
          /* generate twiddles for radix-5 loop */
          for (i = 0; i < s_r5; i++) {
            for (j = 0; j < n/5; j++) {
              tw[k]     =  cos (2 * PI * j / n);
              tw[k + 1] = -sin (2 * PI * j / n);
              tw[k + 2] =  cos (4 * PI * j / n);
              tw[k + 3] = -sin (4 * PI * j / n);
              tw[k + 4] =  cos (6 * PI * j / n);
              tw[k + 5] = -sin (6 * PI * j / n);
              tw[k + 6] =  cos (8 * PI * j / n);
              tw[k + 7] = -sin (8 * PI * j / n);
              k += 8;
            }
            n /= 5;
          }
        }
     }
    else
    { /* generate twiddle for c2c fft */
      /*--------------------------------------
       * TW is organized as follows:
       *   twiddle for fft and ifft, size: 2*M
       *   Bk,                       size: 2*N
       *-------------------------------------*/

      k = 0;

      for (j = 1; j <= M >> 2; j = j << 2) {
        for (i = 0; i < M >> 2; i += j) {
          tw[k]     =  cos (2 * PI * i / M);
          tw[k + 1] =  sin (2 * PI * i / M);
          tw[k + 2] =  cos (4 * PI * i / M);
          tw[k + 3] =  sin (4 * PI * i / M);
          tw[k + 4] =  cos (6 * PI * i / M);
          tw[k + 5] =  sin (6 * PI * i / M);
          k += 6;
        }
      }
      k = 2*M;

      /* phase factor generation */
      for (i = 0; i < N ; i++) {
        tw[k]     =  cos (PI * i * i / N);
        tw[k + 1] = -sin (PI * i * i / N);

        k += 2;
      }

      /* precalculate FFT of bn */

#pragma MUST_ITERATE(4,,4)
      /* first reset the work buffer to zeros */
      for (i = 0; i < 2*M; i+=1)
      {
        work[i] = 0;
      }

      /* construct bn in work buffer */
      bn = work;
      Bk = tw + 2*M;

      /*---------------------------------------------*
       *         bn(0)  = Bk(0)
       *         bn(i)  = Bk(i)            0 < i < N
       *         bn(i)  = 0                N <= i < M-N-1
       *         bn(i)  = Bk(M-i)      M-N-1 <= i < M
       *  Note: bn(i) = Re(bn(i)) + i* Im(bn(i));
       ------------------------------------------- */
      bn[0] = Bk[0];
      bn[1] = Bk[1];

      for(i = 1; i < N; i++)
      {
        bn[2*i]   = bn[2*(M-i)]   = Bk[2*i];
        bn[2*i+1] = bn[2*(M-i)+1] = Bk[2*i+1];
      }

      /* FFT of the constructed signal Bn = fft(bn) */
      DSPF_dp_fftDPxDP (M, bn, tw, Bn, rad, 0, M);
    }
}

void ifft_dp_plan_1d_c2c_batch (
    int     N,      /* IFFT size */
    int     mode,   /* FFT_DIRECT or FFT_ECPY */
    fft_callout_t fxns,
    fft_plan_t *p,
    double *in,
    double *out,
    double *tw,
    double *work    /* scratch buffer to calculate Bn, not to be saved in the instance structure */
)
{
    int     use_bs, rad, M;
    int     s_r3, s_r5, N_p2;

    double *Bn;

    /* only support FFT_ECPY */
    p->fftfxn = ifft_dp_1d_c2c_batch_ecpy;

    memcpy(&p->fftcout, &fxns, sizeof(fft_callout_t));

    p->u.dp_1d_c2c_batch_e.N    = N;

    p->u.dp_1d_c2c_batch_e.tw   = tw;
    if (p->u.dp_1d_c2c_batch_e.para1.use_bs == 1)
      p->u.dp_1d_c2c_batch_e.Bn = tw + p->u.dp_1d_c2c_batch_e.para1.twsize;
    else
      p->u.dp_1d_c2c_batch_e.Bn = NULL;

    p->u.dp_1d_c2c_batch_e.in   = in;
    p->u.dp_1d_c2c_batch_e.out  = out;

    /* twiddle generation */
    Bn = p->u.dp_1d_c2c_batch_e.Bn;

    /* generate twiddle for the c2c ifft*/
    s_r3   = p->u.dp_1d_c2c_batch_e.para1.s_r3;
    s_r5   = p->u.dp_1d_c2c_batch_e.para1.s_r5;
    N_p2   = p->u.dp_1d_c2c_batch_e.para1.N_p2;
    rad    = p->u.dp_1d_c2c_batch_e.para1.rad;
    use_bs = p->u.dp_1d_c2c_batch_e.para1.use_bs;
    M      = p->u.dp_1d_c2c_batch_e.para1.M;
    twiddle_gen (N, M, rad, use_bs, s_r3, s_r5, N_p2, tw, Bn, work);

    CACHE_wbInvAllL2(CACHE_WAIT);
}

/* ======================================================================== */
/*  End of file:  ifft_plan_dp_1d_c2c_batch.c                               */
/* ------------------------------------------------------------------------ */
/*            Copyright (c) 2013 Texas Instruments, Incorporated.           */
/*                           All Rights Reserved.                           */
/* ======================================================================== */
