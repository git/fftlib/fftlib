/* ======================================================================= */
/*  TEXAS INSTRUMENTS, INC.                                                */
/*                                                                         */
/*  FFTLIB  FFT Library                                                    */
/*                                                                         */
/* Copyright (C) 2013 Texas Instruments Incorporated - http://www.ti.com/  */
/*                                                                         */
/*                                                                         */
/*  Redistribution and use in source and binary forms, with or without     */
/*  modification, are permitted provided that the following conditions     */
/*  are met:                                                               */
/*                                                                         */
/*    Redistributions of source code must retain the above copyright       */
/*    notice, this list of conditions and the following disclaimer.        */
/*                                                                         */
/*    Redistributions in binary form must reproduce the above copyright    */
/*    notice, this list of conditions and the following disclaimer in the  */
/*    documentation and/or other materials provided with the               */
/*    distribution.                                                        */
/*                                                                         */
/*    Neither the name of Texas Instruments Incorporated nor the names of  */
/*    its contributors may be used to endorse or promote products derived  */
/*    from this software without specific prior written permission.        */
/*                                                                         */
/*  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS    */
/*  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT      */
/*  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR  */
/*  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT   */
/*  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,  */
/*  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT       */
/*  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,  */
/*  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY  */
/*  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT    */
/*  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE  */
/*  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.   */
/*                                                                         */
/* ======================================================================= */

#include <math.h>
#include <stdio.h>
#include <c6x.h>
#include <ti/csl/csl_cacheAux.h>
#include "ifft_omp_sp_1d_c2c.h"

/* twiddle generation */
static void twiddle_gen (int dim, int N, int n, int s_r3, int s_r5, int N_p2, float *tw)
{
  int k, j, i;
  const double PI = 3.14159265358979323846;

  k = 0;
  
  /* twiddle_gen for ecpy - bluestein not supported */
  if (dim == 2) {
    /* global twiddle generation */
    for (i = 0; i < n; i++) {
      tw[k    ] = -sin (2 * PI * i / N);
      tw[k + 1] =  cos (2 * PI * i / N);
      k += 2;
    }
    k = 2*n;
  }
  
    /* twiddle generation */
    tw[k    ] = sin (2 * PI / N_p2);
    tw[k + 1] = cos (2 * PI / N_p2);
  
    if ((s_r3 != 0) || (s_r5 != 0)) {
      /* generate twiddles for radix-3 loop */
      k += 2;
      for (i = 0; i < s_r3; i++) {
        for (j = 0; j < n/3; j++) {
#ifdef _LITTLE_ENDIAN
          tw[k]     = (float) -sin (2 * PI * j / n);
          tw[k + 1] = (float)  cos (2 * PI * j / n);
          tw[k + 2] = (float) -sin (4 * PI * j / n);
          tw[k + 3] = (float)  cos (4 * PI * j / n);
#else
          tw[k]     = (float) cos (2 * PI * j / n);
          tw[k + 1] = (float) sin (2 * PI * j / n);
          tw[k + 2] = (float) cos (4 * PI * j / n);
          tw[k + 3] = (float) sin (4 * PI * j / n);
#endif
          k += 4;
        }
        n /= 3;
      }

      /* generate twiddles for radix-5 loop */
      for (i = 0; i < s_r5; i++) {
        for (j = 0; j < n/5; j++) {
#ifdef _LITTLE_ENDIAN
          tw[k]     = (float) -sin (2 * PI * j / n);
          tw[k + 1] = (float)  cos (2 * PI * j / n);
          tw[k + 2] = (float) -sin (4 * PI * j / n);
          tw[k + 3] = (float)  cos (4 * PI * j / n);
          tw[k + 4] = (float) -sin (6 * PI * j / n);
          tw[k + 5] = (float)  cos (6 * PI * j / n);
          tw[k + 6] = (float) -sin (8 * PI * j / n);
          tw[k + 7] = (float)  cos (8 * PI * j / n);
#else
          tw[k]     = (float) cos (2 * PI * j / n);
          tw[k + 1] = (float) sin (2 * PI * j / n);
          tw[k + 2] = (float) cos (4 * PI * j / n);
          tw[k + 3] = (float) sin (4 * PI * j / n);
          tw[k + 4] = (float) cos (6 * PI * j / n);
          tw[k + 5] = (float) sin (6 * PI * j / n);
          tw[k + 6] = (float) cos (8 * PI * j / n);
          tw[k + 7] = (float) sin (8 * PI * j / n);
#endif
          k += 8;
        }
        n /= 5;
      }
    }
} /* twiddle_gen()*/

void ifft_omp_sp_plan_1d_c2c (
    int   N,        /* FFT size */
    int   mode,     /* FFT_DIRECT or FFT_ECPY */
    fft_callout_t fxns,
    fft_plan_t *p,
    float *in,
    float *out,
    float *tw
)
{
    int n1, n2, twsize;
    int s_r3, s_r5, N_p2;

    /* Setup the ifft function */
    p->fftfxn = ifft_omp_sp_1d_c2c_ecpy;

    memcpy(&p->fftcout, &fxns, sizeof(fft_callout_t));
    
    p->u.sp_1d_c2c_e.N      = N;
    p->u.sp_1d_c2c_e.data   = in;
    p->u.sp_1d_c2c_e.work   = out;
    p->u.sp_1d_c2c_e.tw     = tw;

    /* generate twiddle for the c2c ifft*/
    s_r3 = p->u.sp_1d_c2c_e.para1.s_r3;
    s_r5 = p->u.sp_1d_c2c_e.para1.s_r5;
    N_p2 = p->u.sp_1d_c2c_e.para1.N_p2;
    n1   = p->u.sp_1d_c2c_e.n1;
    twiddle_gen (1, N, n1, s_r3, s_r5, N_p2, tw);
    
    twsize = p->u.sp_1d_c2c_e.para1.twsize;
    s_r3 = p->u.sp_1d_c2c_e.para2.s_r3;
    s_r5 = p->u.sp_1d_c2c_e.para2.s_r5;
    N_p2 = p->u.sp_1d_c2c_e.para2.N_p2;
    n2   = p->u.sp_1d_c2c_e.n2;
    twiddle_gen (2, N, n2, s_r3, s_r5, N_p2, tw+twsize);

    CACHE_wbInvAllL2(CACHE_WAIT);
}

/* ======================================================================== */
/*  End of file:  ifft_omp_plan_sp_1d_c2c.c                                 */
/* ------------------------------------------------------------------------ */
/*            Copyright (c) 2013 Texas Instruments, Incorporated.           */
/*                           All Rights Reserved.                           */
/* ======================================================================== */
