/* ======================================================================= */
/*  TEXAS INSTRUMENTS, INC.                                                */
/*                                                                         */
/*  FFTLIB  FFT Library                                                    */
/*                                                                         */
/* Copyright (C) 2013 Texas Instruments Incorporated - http://www.ti.com/  */ 
/*                                                                         */
/*                                                                         */
/*  Redistribution and use in source and binary forms, with or without     */
/*  modification, are permitted provided that the following conditions     */
/*  are met:                                                               */
/*                                                                         */
/*    Redistributions of source code must retain the above copyright       */
/*    notice, this list of conditions and the following disclaimer.        */
/*                                                                         */
/*    Redistributions in binary form must reproduce the above copyright    */
/*    notice, this list of conditions and the following disclaimer in the  */
/*    documentation and/or other materials provided with the               */
/*    distribution.                                                        */
/*                                                                         */
/*    Neither the name of Texas Instruments Incorporated nor the names of  */
/*    its contributors may be used to endorse or promote products derived  */
/*    from this software without specific prior written permission.        */
/*                                                                         */
/*  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS    */
/*  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT      */
/*  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR  */
/*  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT   */
/*  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,  */
/*  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT       */
/*  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,  */
/*  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY  */
/*  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT    */
/*  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE  */
/*  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.   */
/*                                                                         */
/* ======================================================================= */

#include <xdc/std.h>
#include "c6x.h"
#include "stdlib.h"
#include "fft_dp_3d_r2c.h"
#include <ti/fftlib/src/common/fft/fft_dp_mixed_bs.h>
#include <ti/dsplib/src/DSPF_dp_fftDPxDP/DSPF_dp_fftDPxDP.h>
#include <ti/fftlib/src/common/fft_edma.h>

static void DSPF_dp_mat_trans_cplx(const double *restrict x, const int rows,
  const int cols, double *restrict y)
{
  int i, j = 0;

  #pragma MUST_ITERATE(2,,)
  for(i = 0; i < rows; i++)
  #pragma MUST_ITERATE(2,,)
  for(j = 0; j < cols; j++)
  {
    y[j*rows*2 + i*2] = x[j*2 + cols*i*2];
    y[j*rows*2 + i*2 + 1] = x[1 + j*2 + cols*i*2];
  }
}

int fft_dp_1d_r2c_batch_ecpy_transpose (
    int    dim,               /* dimension of FFT               */
    int    N1,                /* FFT size                       */
    int    N2,                /* number of lines                */
    int    N3,                /* number of planes               */
    int    M,                 /* FFT size                       */
    int    rad,               /* radix for N1                   */
    int    N_p2,              /* power of 2 size in mixed radix */
    int    s_r3,              /* number stages of radix 3       */
    int    s_r5,              /* number stages of radix 5       */
    int    flag,              /* edma transpose flag            */
    double *in,               /* input pointer                  */
    double *out,              /* output pointer                 */
    double *in_w,             /* input work buffer pointer      */
    double *out_w,            /* output working buffer pointer  */
    double *out_t,            /* temp working buffer pointer    */
    double *tw,               /* twiddle pointer                */
    double *Bn,               /* Bn pointer                     */
    double *workbuf,          /* workbuf pointer for BS         */
    FFT_EDMA_Struct  *edma,   /* edma resource pointer          */
    int num_of_lines          /* number of lines for the 1D FFT */
)
{
    int   error = FFT_NOERR, linecnt = 0, n1;
    int   i, j,ch_i,ch_o,ch_i1,ch_o1,dum0,dum1;
    int   insize, instep,
          outsample = N1,
          linesize, outputstep, linewrap;

    double *restrict in_ptr = in, *restrict out_ptr = out, *restrict tempout;
    double *restrict input, *restrict output, *restrict in1, *restrict out1, *dum2, *dum3;

    /* if linesize is bigger than the 16 bit limit, use cpu to do the global transpose for 3rd dimension for now */
    /* parameters used for output EDMA */
    if (dim == 1)
    {
      linesize   = N2*2*sizeof(double);
      outputstep = 2;
      linewrap   = 2*(N1-1)*N2;
    }
    else if (dim == 2)
    {
      linesize  = N2*N3*2*sizeof(double);
      outputstep = 2*N3;
      linewrap   = -2*(N2*N3-1);
    }
    else if (dim == 3)
    {
      linesize  = N2*N3*2*sizeof(double);
      outputstep = 2;
      linewrap   = 0;
      if (flag != 1)
        num_of_lines = 1;
    }

    /* parameters used for input EDMA */
    if (dim == 1)
    {
      insize = N1*sizeof(double)*num_of_lines;
      instep = N1*num_of_lines;
    }
    else
    {
      insize = 2*N1*sizeof(double)*num_of_lines;
      instep = 2*N1*num_of_lines;
    }
    
    /* prepare first FFT input buffer */
    EdmaMgr_copy1D1D(
      edma->channel[FFT_XFER_CHANNEL0],
      (void *)in_ptr,
      (void *)in_w,
      insize);
    in_ptr += instep;

    /* set pingpong buffer pointers */
    in1    = in_w;
    out1   = out_w;
    input  = &in_w[instep];
    output = &out_w[2*N1*num_of_lines];
    tempout= out_t;

    /* prepare second FFT input buffer */
    EdmaMgr_copy1D1D(
      edma->channel[FFT_XFER_CHANNEL2],
      (void *)in_ptr,
      (void *)input,
      insize);
    in_ptr += instep;

    /* wait for first FFT input */
    EdmaMgr_wait(edma->channel[FFT_XFER_CHANNEL0]);

    /* 1st fft */
#if (FFT_DP_3D_R2C_NUMOFLINEBUFS>1)
    if (dim == 1){
      /* call r2c fft for the 1st dimension */
      if (N1 == M) {
        if (s_r3 == 0 && s_r5 == 0) {
          for (i=0; i<num_of_lines; i++)
            DSPF_dp_fftDPxDP_r2c (N1, &in1[i*N1], tw, &tempout[i*N1*2], rad, 0, N1);
        }
        else {
          for (i=0; i<num_of_lines; i++)
            fft_DPxDP_mixed_radix_r2c (N1,  &in1[i*N1], tw, &tempout[i*N1*2], N_p2, s_r3, s_r5, rad);
        }
      }
      else{
        for (i=0; i<num_of_lines; i++)
          fft_DPxDP_r2c_bs (N1, M, rad, &in1[i*N1], &tempout[i*N1*2], tw, Bn, workbuf);
      }
    }
    else{
      /* call c2c fft for other dimensions */
      if (N1 == M) {
        if (s_r3 == 0 && s_r5 == 0) {
          for (i=0; i<num_of_lines; i++)
        	DSPF_dp_fftDPxDP (N1, &in1[i*N1*2], tw, &tempout[i*N1*2], rad, 0, N1);
        }
        else {
          for (i=0; i<num_of_lines; i++)
            fft_DPxDP_mixed_radix (N1, &in1[i*N1*2], tw, &tempout[i*N1*2], N_p2, s_r3, s_r5, rad);
        }
      } /* if (N1 == M) */
      else{
        for (i=0; i<num_of_lines; i++)
          fft_DPxDP_bs (N1, M, rad, &in1[i*N1*2], &tempout[i*N1*2], tw, Bn, workbuf);
      }
    } /* if (dim == 1) */

    /* local transpose from tempout to out1 */
    DSPF_dp_mat_trans_cplx(tempout, num_of_lines, N1, out1);
#else
    if (dim == 1){
      /* call r2c fft */
      if (N1 == M) {
        if (s_r3 == 0 && s_r5 == 0) {
          DSPF_dp_fftDPxDP_r2c (N1, in1, tw, out1, rad, 0, N1);
        }
        else {
          fft_DPxDP_mixed_radix_r2c (N1, in1, tw, out1, N_p2, s_r3, s_r5, rad);
        }
      }
      else{
        fft_DPxDP_r2c_bs (N1, M, rad, in1, out1, tw, Bn, workbuf);
      }
    }
    else{
      /* call c2c fft */
      if (N1 == M) {
        if (s_r3 == 0 && s_r5 == 0) {
          DSPF_dp_fftDPxDP (N1, in1, tw, out1, rad, 0, N1);
        }
        else {
          fft_DPxDP_mixed_radix (N1, in1, tw, out1, N_p2, s_r3, s_r5, rad);
        }
      }
      else{
        fft_DPxDP_bs (N1, M, rad, in1, out1, tw, Bn, workbuf);
      }
    }
#endif

    /* export output data of first FFT*/
    if (dim == 1 || flag == 1)
    {
      EdmaMgr_copy1D2D(
      edma->channel[FFT_XFER_CHANNEL1],
      (void *)out1,
      (void *)out_ptr,
      2*sizeof(double)*num_of_lines,
      outsample,
      linesize);
      
      out_ptr += outputstep*num_of_lines;
      linecnt += 1*num_of_lines;
    }
    else
    {
      for (i = 0; i < num_of_lines; i++)
      {
        for (n1 = 0; n1 < outsample; n1++)
        {
          out_ptr[n1*linesize/sizeof(double)]     = out1[2*n1 + i*2*outsample];
          out_ptr[n1*linesize/sizeof(double) + 1] = out1[2*n1 + i*2*outsample + 1];
        }

        out_ptr += outputstep;
        linecnt += 1;
      }
    }

    /* prepare third FFT input buffer */
    EdmaMgr_copy1D1D(
      edma->channel[FFT_XFER_CHANNEL0],
      (void *)in_ptr,
      (void *)in_w,
      insize);
    in_ptr += instep;

    /* set channel numbers */
    ch_i  = FFT_XFER_CHANNEL2;
    ch_o  = FFT_XFER_CHANNEL3;
    ch_i1 = FFT_XFER_CHANNEL0;
    ch_o1 = FFT_XFER_CHANNEL1;

    /* wait for EDMA */
    EdmaMgr_wait(edma->channel[ch_i]);

    for (j = 1; j < (N3*N2)/num_of_lines; j++) 
    {
#if (FFT_DP_3D_R2C_NUMOFLINEBUFS>1)
      if (dim == 1){
      /* call r2c fft for the 1st dimension */
        if (N1 == M) {
          if (s_r3 == 0 && s_r5 == 0) {
            for (i=0; i<num_of_lines; i++)
              DSPF_dp_fftDPxDP_r2c (N1, &input[i*N1], tw, &tempout[i*N1*2], rad, 0, N1);
          }
          else {
            for (i=0; i<num_of_lines; i++)
              fft_DPxDP_mixed_radix_r2c (N1, &input[i*N1], tw, &tempout[i*N1*2], N_p2, s_r3, s_r5, rad);
          }
        } /* if (N1 == M)*/
        else{
          for (i=0; i<num_of_lines; i++)
            fft_DPxDP_r2c_bs (N1, M, rad, &input[i*N1], &tempout[i*N1*2], tw, Bn, workbuf);
        }
      }
      else{
      /* call c2c fft for other dimensions */
        if (N1 == M) {
          if (s_r3 == 0 && s_r5 == 0) {
            for (i=0; i<num_of_lines; i++)
              DSPF_dp_fftDPxDP (N1, &input[i*N1*2], tw, &tempout[i*N1*2], rad, 0, N1);
          }
          else {
            for (i=0; i<num_of_lines; i++)
              fft_DPxDP_mixed_radix (N1, &input[i*N1*2], tw, &tempout[i*N1*2], N_p2, s_r3, s_r5, rad);
          }
        } /* if (N1 == M) */
        else{
          for (i=0; i<num_of_lines; i++)
            fft_DPxDP_bs (N1, M, rad, &input[i*N1*2], &tempout[i*N1*2], tw, Bn, workbuf);
        }
      }/* if (dim == 1) */

      /* local transpose from tempout to output */
      DSPF_dp_mat_trans_cplx(tempout, num_of_lines, N1, output);
#else
      if (dim == 1){
      /* call r2c fft for the 1st dimension */
        if (N1 == M) {
          if (s_r3 == 0 && s_r5 == 0) {
            DSPF_dp_fftDPxDP_r2c (N1, input, tw, output, rad, 0, N1);
          }
          else
            fft_DPxDP_mixed_radix_r2c (N1, input, tw, output, N_p2, s_r3, s_r5, rad);
        } /* if (N1 == M)*/
        else
          fft_DPxDP_r2c_bs (N1, M, rad, input, output, tw, Bn, workbuf);
      } /* if (dim == 1) */
      else{
        if (N1 == M) {
          if (s_r3 == 0 && s_r5 == 0) {
            DSPF_dp_fftDPxDP (N1, input, tw, output, rad, 0, N1);
          }
          else
            fft_DPxDP_mixed_radix (N1, input, tw, output, N_p2, s_r3, s_r5, rad);
        } /* if (N1 == M)*/
        else
          fft_DPxDP_bs (N1, M, rad, input, output, tw, Bn, workbuf);
      } /* else (dim == 1) */
#endif

      /* export output data*/
      if (dim == 1 || flag == 1)
      {
        EdmaMgr_copy1D2D(
        edma->channel[ch_o],
        (void *)output,
        (void *)out_ptr,
        2*sizeof(double)*num_of_lines,
        outsample,
        linesize);
        out_ptr += outputstep*num_of_lines;
        linecnt += 1*num_of_lines;
      }
      else
      {
        for (i = 0; i < num_of_lines; i++)
        {
          for (n1 = 0; n1 < outsample; n1++)
          {
            out_ptr[n1*linesize/sizeof(double)]     = output[2*n1 + i*2*outsample];
            out_ptr[n1*linesize/sizeof(double) + 1] = output[2*n1 + i*2*outsample + 1];
          }
		  out_ptr += outputstep;
		  linecnt += 1;
        }
      }
    
      /* re-adjust output pointer when it's time for the next batch */
      if (linecnt >= N2)
      {
        linecnt = 0;
        out_ptr += linewrap;
      }

      /* prepare input data */
      EdmaMgr_copy1D1D(
        edma->channel[ch_i],
        (void *)in_ptr,
        (void *)input,
        insize);
      in_ptr += instep;

      /* swap pingpong buffer pointers and XFER channel */
      dum2   = input;
      input  = in1;
      in1    = dum2;
      dum3   = output;
      output = out1;
      out1   = dum3;

      dum0   = ch_i;
      ch_i   = ch_i1;
      ch_i1  = dum0;
      dum1   = ch_o;
      ch_o   = ch_o1;
      ch_o1  = dum1;

      /* wait for EDMA */
      EdmaMgr_wait(edma->channel[ch_i]);
      if (dim == 1 || flag == 1)
        EdmaMgr_wait(edma->channel[ch_o]);
    } /* for (j) */

    EdmaMgr_wait(edma->channel[ch_i1]);
    if (dim == 1 || flag == 1)
      EdmaMgr_wait(edma->channel[ch_o1]);

    return error;
} /* fft_dp_1d_r2c_ecpy_transpose */

int fft_dp_3d_r2c_ecpy (
    /* int   N1;       FFT size         */
    /* int   N2;       FFT size         */
    /* int   N3;       FFT size         */
    /* float *in;      input pointer    */
    /* float *out;     output pointer   */
    /* float *in_w;    input pointer    */
    /* float *out_w;   output pointer   */
    /* float *tw;      twiddle pointer  */
    fft_param_u u,
    void *edmaState
)
{
    /* input parameters */
    double *in      = u.dp_3d_r2c_e.in;         /* input pointer                         */
    double *out     = u.dp_3d_r2c_e.out;        /* output pointer                        */
    double *tw      = u.dp_3d_r2c_e.tw;         /* twiddle buffer pointer                */
    double *in_w    = u.dp_3d_r2c_e.in_w;       /* input working buffer pointer          */
    double *out_w   = u.dp_3d_r2c_e.out_w;      /* output working buffer pointer         */
    double *out_t   = u.dp_3d_r2c_e.out_t;      /* temp working buffer pointer           */
    double *tw_w    = u.dp_3d_r2c_e.tw_w;       /* twiddle working buffer pointer        */
    double *Bn      = u.dp_3d_r2c_e.Bn;         /* precalculated Bn buffer pointer       */
    double *Bn_w    = u.dp_3d_r2c_e.Bn_w;       /* Bn working buffer pointer             */
    double *workbuf = u.dp_3d_r2c_e.workbuf;    /* workbuffer pointer for BS             */
    double *tw_a, *Bn_a;

    int   N1       = u.dp_3d_r2c_e.N1;             /* first dimension signal size N1        */
    int   N2       = u.dp_3d_r2c_e.N2;             /* first dimension signal size N2        */
    int   N3       = u.dp_3d_r2c_e.N3;             /* first dimension signal size N3        */
    int   M1       = u.dp_3d_r2c_e.para1.M;        /* first dimension FFT size M1           */
    int   M2       = u.dp_3d_r2c_e.para2.M;        /* second dimension FFT size M2          */
    int   M3       = u.dp_3d_r2c_e.para3.M;        /* third dimension FFT size M3           */
    int   rad      = u.dp_3d_r2c_e.para1.rad;      /* radix for N1                          */

    int   flag     = u.dp_3d_r2c_e.ecpy_flag;   /* flag to indicate whether the signal
                                                   size is too big to do edma transpose  */

    int     twsize  = u.dp_3d_r2c_e.para1.twsize;      /* twiddle size for 1st fft       */
    int     N_p2    = u.dp_3d_r2c_e.para1.N_p2;        /* power of 2 size in mixed radix  */
    int     s_r3    = u.dp_3d_r2c_e.para1.s_r3;        /* number stages of radix 3        */
    int     s_r5    = u.dp_3d_r2c_e.para1.s_r5;        /* number stages of radix 5        */
    int     tw_offset, Bn_offset;
    FFT_EDMA_Struct  *edma = (FFT_EDMA_Struct *)edmaState;

    int   error = FFT_NOERR, numLine = FFT_DP_3D_R2C_NUMOFLINEBUFS;;
    int   Bnsize  = u.dp_3d_r2c_e.para1.Bnsize;

    if (tw_w != NULL) {
      /* prepare twiddle buffer */
      EdmaMgr_copy1D1D(
        edma->channel[FFT_XFER_CHANNEL3],
        (void *)tw,
        (void *)tw_w,
        twsize * sizeof(double));
      tw_a = tw_w;
    }
    else {
      tw_a = tw;
    }

    if (Bn_w != NULL) {
      /* prepare Bn buffer when doing bluestein */
      EdmaMgr_copy1D1D(
        edma->channel[FFT_XFER_CHANNEL2],
        (void *)Bn,
        (void *)Bn_w,
        Bnsize * sizeof(double));
      Bn_a = Bn_w;
    }
    else {
      Bn_a = Bn;
    }
    if (tw_w != NULL)
    	EdmaMgr_wait(edma->channel[FFT_XFER_CHANNEL3]);
    if (Bn_w != NULL)
    	EdmaMgr_wait(edma->channel[FFT_XFER_CHANNEL2]);
    /* 1st dimension fft */
    error = fft_dp_1d_r2c_batch_ecpy_transpose (1, N1, N2, N3, M1, rad, N_p2, s_r3, s_r5, flag, in, out, in_w, out_w, out_t, tw_a, Bn_a, workbuf, edma, numLine);

    /* prepare for the 2nd dimension */
    twsize = u.dp_3d_r2c_e.para2.twsize;
    Bnsize = u.dp_3d_r2c_e.para2.Bnsize;
    N_p2   = u.dp_3d_r2c_e.para2.N_p2;    /* power of 2 size in mixed radix        */
    s_r3   = u.dp_3d_r2c_e.para2.s_r3;    /* number stages of radix 3              */
    s_r5   = u.dp_3d_r2c_e.para2.s_r5;    /* number stages of radix 5              */
    rad    = u.dp_3d_r2c_e.para2.rad;
    tw_offset = u.dp_3d_r2c_e.para1.twsize;
    Bn_offset = u.dp_3d_r2c_e.para1.Bnsize;

    if (tw_w != NULL) {
      /* prepare twiddle buffer */
      EdmaMgr_copy1D1D(
        edma->channel[FFT_XFER_CHANNEL3],
        (void *)(tw+tw_offset),
        (void *)tw_w,
        twsize*sizeof(double));
      tw_a = tw_w;
    }
    else {
      tw_a = tw + tw_offset;
    }

    if (Bn_w != NULL) {
      /* prepare Bn buffer when doing bluestein */
      EdmaMgr_copy1D1D(
        edma->channel[FFT_XFER_CHANNEL2],
        (void *)(Bn+Bn_offset),
        (void *)Bn_w,
        Bnsize*sizeof(double));
      Bn_a = Bn_w;
    }
    else {
      Bn_a = Bn + Bn_offset;
    }
    if (tw_w != NULL)
    	EdmaMgr_wait(edma->channel[FFT_XFER_CHANNEL3]);
    if (Bn_w != NULL)
    	EdmaMgr_wait(edma->channel[FFT_XFER_CHANNEL2]);

    /* 2nd dimension fft */
    error = fft_dp_1d_r2c_batch_ecpy_transpose (2, N2, N1, N3, M2, rad, N_p2, s_r3, s_r5, flag, out, in, out_w, in_w, out_t, tw_a, Bn_a, workbuf, edma, 1);

    /* prepare for the 3rd dimension */
    twsize    = u.dp_3d_r2c_e.para3.twsize;
    Bnsize    = u.dp_3d_r2c_e.para3.Bnsize;
    N_p2      = u.dp_3d_r2c_e.para3.N_p2;    /* power of 2 size in mixed radix        */
    s_r3      = u.dp_3d_r2c_e.para3.s_r3;    /* number stages of radix 3              */
    s_r5      = u.dp_3d_r2c_e.para3.s_r5;    /* number stages of radix 5              */
    rad       = u.dp_3d_r2c_e.para3.rad;
    tw_offset = u.dp_3d_r2c_e.para1.twsize + u.dp_3d_r2c_e.para2.twsize;
    Bn_offset = u.dp_3d_r2c_e.para1.Bnsize + u.dp_3d_r2c_e.para2.Bnsize;

    if (tw_w != NULL) {
      /* prepare twiddle buffer */
      EdmaMgr_copy1D1D(
        edma->channel[FFT_XFER_CHANNEL3],
        (void *)(tw+tw_offset),
        (void *)tw_w,
        twsize * sizeof(double));
      tw_a = tw_w;
    }
    else {
      tw_a = tw + tw_offset;
    }

    if (Bn_w != NULL) {
      /* prepare Bn buffer when doing bluestein */
      EdmaMgr_copy1D1D(
        edma->channel[FFT_XFER_CHANNEL2],
        (void *)(Bn+Bn_offset),
        (void *)Bn_w,
        Bnsize * sizeof(double));
      Bn_a = Bn_w;
    }
    else {
      Bn_a = Bn + Bn_offset;
    }
    if (tw_w != NULL)
    	EdmaMgr_wait(edma->channel[FFT_XFER_CHANNEL3]);
    if (Bn_w != NULL)
    	EdmaMgr_wait(edma->channel[FFT_XFER_CHANNEL2]);
    /* 3rd dimension fft */
    error = fft_dp_1d_r2c_batch_ecpy_transpose (3, N3, N1, N2, M3, rad, N_p2, s_r3, s_r5, flag, in, out, in_w, out_w, out_t, tw_a, Bn_a, workbuf, edma, numLine);

    return error;
} /* fft_dp_3d_r2c_ecpy */

/* ======================================================================== */
/*  End of file:  fft_dp_3d_r2c.c                                           */
/* ------------------------------------------------------------------------ */
/*            Copyright (c) 2013 Texas Instruments, Incorporated.           */
/*                           All Rights Reserved.                           */
/* ======================================================================== */
