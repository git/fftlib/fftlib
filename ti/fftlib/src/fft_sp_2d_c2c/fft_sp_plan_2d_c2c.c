/* ======================================================================= */
/*  TEXAS INSTRUMENTS, INC.                                                */
/*                                                                         */
/*  FFTLIB  FFT Library                                                    */
/*                                                                         */
/* Copyright (C) 2013 Texas Instruments Incorporated - http://www.ti.com/  */ 
/*                                                                         */
/*                                                                         */
/*  Redistribution and use in source and binary forms, with or without     */
/*  modification, are permitted provided that the following conditions     */
/*  are met:                                                               */
/*                                                                         */
/*    Redistributions of source code must retain the above copyright       */
/*    notice, this list of conditions and the following disclaimer.        */
/*                                                                         */
/*    Redistributions in binary form must reproduce the above copyright    */
/*    notice, this list of conditions and the following disclaimer in the  */
/*    documentation and/or other materials provided with the               */
/*    distribution.                                                        */
/*                                                                         */
/*    Neither the name of Texas Instruments Incorporated nor the names of  */
/*    its contributors may be used to endorse or promote products derived  */
/*    from this software without specific prior written permission.        */
/*                                                                         */
/*  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS    */
/*  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT      */
/*  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR  */
/*  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT   */
/*  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,  */
/*  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT       */
/*  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,  */
/*  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY  */
/*  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT    */
/*  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE  */
/*  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.   */
/*                                                                         */
/* ======================================================================= */

#include <math.h>
#include <stdio.h>
#include <c6x.h>
#include "fft_sp_2d_c2c.h"
#include <ti/fftlib/src/common/fft/fft_sp_cmplx_notwid_br.h>
#include <ti/csl/csl_cacheAux.h>

/* FFT 2d sp c2c memory buffers */
#define FFT_SP_2D_C2C_EXTERN_X_BUFN      0
#define FFT_SP_2D_C2C_EXTERN_Y_BUFN      (FFT_SP_2D_C2C_EXTERN_X_BUFN    +1)
#define FFT_SP_2D_C2C_EXTERN_W_BUFN      (FFT_SP_2D_C2C_EXTERN_Y_BUFN    +1)
#define FFT_SP_2D_C2C_EXTERN_BN_BUFN     (FFT_SP_2D_C2C_EXTERN_W_BUFN    +1)
#define FFT_SP_2D_C2C_INTERN_X_BUFN      (FFT_SP_2D_C2C_EXTERN_BN_BUFN   +1)
#define FFT_SP_2D_C2C_INTERN_Y_BUFN      (FFT_SP_2D_C2C_INTERN_X_BUFN    +1)
#define FFT_SP_2D_C2C_INTERN_W_BUFN      (FFT_SP_2D_C2C_INTERN_Y_BUFN    +1)
#define FFT_SP_2D_C2C_INTERN_BN_BUFN     (FFT_SP_2D_C2C_INTERN_W_BUFN    +1)
#define FFT_SP_2D_C2C_INTERN_WORK_BUFN   (FFT_SP_2D_C2C_INTERN_BN_BUFN   +1)
#define FFT_SP_2D_C2C_INTERN_T_BUFN      (FFT_SP_2D_C2C_INTERN_WORK_BUFN +1)
#define FFT_SP_2D_C2C_NBUFS              (FFT_SP_2D_C2C_INTERN_T_BUFN    +1)
/* class, log2align, size, base */

static FFTmemBuffer_t FFT_sp_2d_c2c_Bufs[FFT_SP_2D_C2C_NBUFS] = {
  {FFT_MEM_CLASS_EXTERNAL, 3, 0, 0},   /* EXTERN_X */
  {FFT_MEM_CLASS_EXTERNAL, 3, 0, 0},   /* EXTERN_Y */
  {FFT_MEM_CLASS_EXTERNAL, 3, 0, 0},   /* EXTERN_W */
  {FFT_MEM_CLASS_EXTERNAL, 3, 0, 0},   /* EXTERN_BN */
  {FFT_MEM_CLASS_INTERNAL, 3, 0, 0},   /* INTERN_X */
  {FFT_MEM_CLASS_INTERNAL, 3, 0, 0},   /* INTERN_Y */
  {FFT_MEM_CLASS_INTERNAL, 3, 0, 0},   /* INTERN_W */
  {FFT_MEM_CLASS_INTERNAL, 3, 0, 0},   /* INTERN_T */
  {FFT_MEM_CLASS_INTERNAL, 3, 0, 0},   /* INTERN_BN */
  {FFT_MEM_CLASS_INTERNAL, 3, 0, 0}    /* INTERN_WORK */
};

/* calculate radix for 1D array */
static void calculate_rad (int N, fft_para_mix_bs_t *fft_para, int *use_bs)
{
  int n, j, M, rad;
  int s_r3, s_r5, N_p2, dum;

  *use_bs = 1;

  /* calculate the size for zero padding when the signal size is not power of 2 */
  n = (N<<1)-1;
  j = _norm(n);
  M = 1 << (31-j);

  N_p2 = N;
  s_r3 = 0;
  s_r5 = 0;

  /* if N is odd, bypass the rest of calculation, use Bluestein */
  if (((N>>1)<<1) == N)
  {
    /* if N is power of 2, no zero padding, regular radix 2 or 4 fft */
    if (N == (M>>1))
    { /* power of 2 */
      M = N;
      j = j+1;
      *use_bs = 0;
    }
    else {
      dum = N;
      while (dum/3*3 == dum) {
        s_r3++;
        dum /= 3;
      }
      while (dum/5*5 == dum) {
        s_r5++;
        dum /= 5;
      }
      if ((dum >= 8) && (dum == (1 << (30-_norm(dum))))) {
        /* mixed radix 2 and 3 */
        N_p2                 = dum;
        *use_bs = 0;
        M = N;
        j  = _norm(dum);
      }
    }
  }

  /* calculate radix for radix 2 or 4 r2c or c2c fft */
  if (j%2 == 0)
    rad = 4;
  else
    rad = 2;

  fft_para->N_p2 = N_p2;
  fft_para->s_r3 = s_r3;
  fft_para->s_r5 = s_r5;
  fft_para->M    = M;
  fft_para->rad  = rad;

} /*calculate_rad ()*/

/* calculate twiddle size, Bn buffer size and workbuffer size */
static void calculate_mem_size(int N, int M, int use_bs, int s_r3, int s_r5, int N_p2, int *twsize, int *Bnsize, int *worksize)
{
  int dum, n, i;
  /* Please note that Bn buffer and Workbuf are only used when doing Bluestein
   * meaning when use_bs == 1 */

  if (use_bs == 1)
  { /* when size is not power of 2 and not multiple of 3&5 */
    *twsize   = (2+2*N+2);
    *Bnsize   = (2*M);
    *worksize = (4*M);
  }
  else
  {
    if ((s_r3 == 0) && (s_r5 == 0)) {
    /* When signal is power of 2 */
    *twsize   = 2;
    }
    else {
    /* When signal is power of 3 or 5 */
      dum = 0;
      n  = 4*N;
      for (i = 0; i < s_r3; i++) {
        dum += n/3;
        n  /= 3;
      }
      n *= 2;  /* radix-5 has 8 twiddle vs radix-3 4 twiddles */
      for (i = 0; i < s_r5; i++) {
        dum += n/5;
        n  /= 5;
      }
      *twsize = (2+dum);
    }
    *Bnsize   = 0;
    *worksize = 0;
  }
} /* calculate_mem_size() */

/* twiddle generation */
static void twiddle_gen (int N, int M, int rad, int use_bs, int s_r3, int s_r5, int N_p2, float *tw, float *Bn, float *work)
{
  int     k, n, j, i;
  float *bn, *Bk;
  const double PI = 3.14159265358979323846;

  k = 0;
  if (use_bs == 0){
    tw[k    ] = -sin (2 * PI / N_p2);
    tw[k + 1] =  cos (2 * PI / N_p2);

    if ((s_r3 != 0) || (s_r5 != 0)) {
      /* generate twiddles for radix-3 loop */
        k  = 2;
        n = N;
      for (i = 0; i < s_r3; i++) {
          for (j = 0; j < n/3; j++) {
#ifdef _LITTLE_ENDIAN
            tw[k]     = (float) sin (2 * PI * j / n);
            tw[k + 1] = (float) cos (2 * PI * j / n);
            tw[k + 2] = (float) sin (4 * PI * j / n);
            tw[k + 3] = (float) cos (4 * PI * j / n);
#else
            tw[k]     = (float)  cos (2 * PI * j / n);
            tw[k + 1] = (float) -sin (2 * PI * j / n);
            tw[k + 2] = (float)  cos (4 * PI * j / n);
            tw[k + 3] = (float) -sin (4 * PI * j / n);
#endif
            k += 4;
          }
          n /= 3;
        }

        /* generate twiddles for radix-5 loop */
        for (i = 0; i < s_r5; i++) {
          for (j = 0; j < n/5; j++) {
#ifdef _LITTLE_ENDIAN
            tw[k]     = (float) sin (2 * PI * j / n);
            tw[k + 1] = (float) cos (2 * PI * j / n);
            tw[k + 2] = (float) sin (4 * PI * j / n);
            tw[k + 3] = (float) cos (4 * PI * j / n);
            tw[k + 4] = (float) sin (6 * PI * j / n);
            tw[k + 5] = (float) cos (6 * PI * j / n);
            tw[k + 6] = (float) sin (8 * PI * j / n);
            tw[k + 7] = (float) cos (8 * PI * j / n);
#else
            tw[k]     = (float)  cos (2 * PI * j / n);
            tw[k + 1] = (float) -sin (2 * PI * j / n);
            tw[k + 2] = (float)  cos (4 * PI * j / n);
            tw[k + 3] = (float) -sin (4 * PI * j / n);
            tw[k + 4] = (float)  cos (6 * PI * j / n);
            tw[k + 5] = (float) -sin (6 * PI * j / n);
            tw[k + 6] = (float)  cos (8 * PI * j / n);
            tw[k + 7] = (float) -sin (8 * PI * j / n);
#endif
            k += 8;
          }
          n /= 5;
        }
      }
    }
    else
    { /* generate twiddle for c2c fft */

      /*--------------------------------------
       *  TW is organized as follows:
       *     twiddle for fft,  size: 2
       *     Bk,               size: 2*N
       *     twiddle for ifft, size: 2
       *-------------------------------------*/

      tw[k    ] = -sin (2 * PI / M);
      tw[k + 1] =  cos (2 * PI / M);

      k = 2;

      /* phase factor generation */
      for (i = 0; i < N ; i++) {
        tw[k]     = (float)  cos (PI * i * i / N);
        tw[k + 1] = (float)  sin (PI * i * i / N);

        k += 2;
      }

      k = 2*N + 2;

      /* twiddle for ifft */
      tw[k    ] =  sin (2 * PI / M);
      tw[k + 1] =  cos (2 * PI / M);

      /* precalculate FFT of bn */

#pragma MUST_ITERATE(4,,4)
      /* first reset the work buffer to zeros */
      for (i = 0; i<M; i+=1)
      {
         _amem8_f2(&work[2*i]) = 0;
      }

      /* construct bn in work buffer */
      bn = work;
      Bk = tw + 2;

      /*---------------------------------------------*
       *         bn(0)    = Bk(0)
       *         bn(i)    = Bk(i)    0 < i <N
       *         bn(N1-i) = Bk(i)    N <= i <N1
       *  Note: bn(i) = Re(bn(i)) + i* Im(bn(i));
       ------------------------------------------- */
      bn[0] = Bk[0];
      bn[1] = Bk[1];

      for(i = 1; i < N; i++)
      {
        bn[2*i]   = bn[2*(M-i)]   = Bk[2*i];
        bn[2*i+1] = bn[2*(M-i)+1] = Bk[2*i+1];
      }

      /* FFT of the constructed signal Bn = fft(bn) */
      fft_sp_cmplx_notwid_br((double*)bn, M, (double*)Bn, (double*)tw);
    }
} /* twiddle_gen() */

fft_plan_t fft_sp_plan_2d_c2c (
    int   N1,       /* FFT size */
    int   N2,       /* FFT size */
    int   mode,     /* FFT_DIRECT or FFT_ECPY */
    fft_callout_t fxns
)
{
    int          N, rad;
    int          M1, M2;
    int          int_tw_size, ext_tw_size, int_Bn_size, ext_Bn_size, work_size;
    int          twsize, Bnsize, worksize;
    int          use_bs_1 = 0, use_bs_2 = 0;
    int          s_r3, s_r5, N_p2;
    float        *tw, *tw_w;
    float        *restrict Bn, *restrict work;
    fft_plan_t   p;

    /* only support FFT_ECPY */
    p.fftfxn = fft_sp_2d_c2c_ecpy;

    /* choose the largest size of both dimensions */
    N = (N1 > N2) ? N1 : N2;
    p.nbuf = FFT_SP_2D_C2C_NBUFS;

    memcpy(&p.fftcout, &fxns, sizeof(fft_callout_t));
    memcpy(p.bufs, FFT_sp_2d_c2c_Bufs, FFT_SP_2D_C2C_NBUFS*sizeof(FFTmemBuffer_t));

    p.u.sp_2d_c2c_e.N1     = N1;
    p.u.sp_2d_c2c_e.N2     = N2;

    /* determine the radix for the first dimension */
    calculate_rad(N1, &p.u.sp_2d_c2c_e.para1, &use_bs_1);
    M1 = p.u.sp_2d_c2c_e.para1.M;

    /* determine the radix for the second dimension */
    calculate_rad(N2, &p.u.sp_2d_c2c_e.para2, &use_bs_2);
    M2 = p.u.sp_2d_c2c_e.para2.M;

    /* calculate memory requirements */
    p.bufs[FFT_SP_2D_C2C_EXTERN_X_BUFN].size = sizeof(float)*N1*N2*2;
    p.bufs[FFT_SP_2D_C2C_EXTERN_Y_BUFN].size = sizeof(float)*N1*N2*2;
    p.bufs[FFT_SP_2D_C2C_INTERN_X_BUFN].size = sizeof(float)*N*4*FFT_SP_2D_C2C_NUMOFLINEBUFS;
    p.bufs[FFT_SP_2D_C2C_INTERN_Y_BUFN].size = sizeof(float)*N*4*FFT_SP_2D_C2C_NUMOFLINEBUFS;
    p.bufs[FFT_SP_2D_C2C_INTERN_T_BUFN].size = sizeof(float)*N*2*FFT_SP_2D_C2C_NUMOFLINEBUFS;

    /* evaluate the  1st dimension */
    s_r3 = p.u.sp_2d_c2c_e.para1.s_r3;
    s_r5 = p.u.sp_2d_c2c_e.para1.s_r5;
    N_p2 = p.u.sp_2d_c2c_e.para1.N_p2;
    calculate_mem_size(N1, M1, use_bs_1, s_r3, s_r5, N_p2, &twsize, &Bnsize, &worksize);
    p.u.sp_2d_c2c_e.para1.twsize = twsize;
    p.u.sp_2d_c2c_e.para1.Bnsize = Bnsize;
    int_tw_size = ext_tw_size = twsize;
    int_Bn_size = ext_Bn_size = Bnsize;
    work_size   = worksize;

    /* evaluate the  2nd dimension */
    s_r3 = p.u.sp_2d_c2c_e.para2.s_r3;
    s_r5 = p.u.sp_2d_c2c_e.para2.s_r5;
    N_p2 = p.u.sp_2d_c2c_e.para2.N_p2;
    calculate_mem_size(N2, M2, use_bs_2, s_r3, s_r5, N_p2, &twsize, &Bnsize, &worksize);
    p.u.sp_2d_c2c_e.para2.twsize = twsize;
    p.u.sp_2d_c2c_e.para2.Bnsize = Bnsize;
    int_tw_size  = (int_tw_size > twsize) ? int_tw_size : twsize;
    ext_tw_size += twsize;
    int_Bn_size  = (int_Bn_size > Bnsize) ? int_Bn_size : Bnsize;
    ext_Bn_size += Bnsize;
    work_size    = (work_size > worksize) ? work_size : worksize;

    p.bufs[FFT_SP_2D_C2C_EXTERN_W_BUFN].size    = sizeof(float)*ext_tw_size;
    p.bufs[FFT_SP_2D_C2C_INTERN_W_BUFN].size    = sizeof(float)*int_tw_size;
    p.bufs[FFT_SP_2D_C2C_EXTERN_BN_BUFN].size   = sizeof(float)*ext_Bn_size;
    p.bufs[FFT_SP_2D_C2C_INTERN_BN_BUFN].size   = sizeof(float)*int_Bn_size;
    p.bufs[FFT_SP_2D_C2C_INTERN_WORK_BUFN].size = sizeof(float)*work_size;

    (*fxns.memoryRequest)(FFT_SP_2D_C2C_NBUFS, p.bufs);

    p.u.sp_2d_c2c_e.data      = p.bufs[FFT_SP_2D_C2C_EXTERN_X_BUFN].base;
    p.u.sp_2d_c2c_e.workbuf   = p.bufs[FFT_SP_2D_C2C_EXTERN_Y_BUFN].base;
    p.u.sp_2d_c2c_e.data_w    = p.bufs[FFT_SP_2D_C2C_INTERN_X_BUFN].base;
    p.u.sp_2d_c2c_e.workbuf_w = p.bufs[FFT_SP_2D_C2C_INTERN_Y_BUFN].base;
    p.u.sp_2d_c2c_e.workbuf_t = p.bufs[FFT_SP_2D_C2C_INTERN_T_BUFN].base;
    tw                        = p.bufs[FFT_SP_2D_C2C_EXTERN_W_BUFN].base;
    tw_w                      = p.bufs[FFT_SP_2D_C2C_INTERN_W_BUFN].base;
    p.u.sp_2d_c2c_e.tw        = tw;
    p.u.sp_2d_c2c_e.tw_w      = tw_w;
    
    if ((use_bs_1 == 1) || (use_bs_2 == 1))
    {
      /* if use Bluestein FFT, set the buffers to do Bluestein */
      p.u.sp_2d_c2c_e.Bn         = p.bufs[FFT_SP_2D_C2C_EXTERN_BN_BUFN].base;
      p.u.sp_2d_c2c_e.Bn_w       = p.bufs[FFT_SP_2D_C2C_INTERN_BN_BUFN].base;
      p.u.sp_2d_c2c_e.workbuf_bn = p.bufs[FFT_SP_2D_C2C_INTERN_WORK_BUFN].base;
    }
    else
    {
      /* otherwise, set the buffers to NULL */
      p.u.sp_2d_c2c_e.Bn         = NULL;
      p.u.sp_2d_c2c_e.Bn_w       = NULL;
      p.u.sp_2d_c2c_e.workbuf_bn = NULL;
    }
    
    /* twiddle generation */
    Bn   = p.u.sp_2d_c2c_e.Bn;
    work = p.u.sp_2d_c2c_e.workbuf_bn;

    /* generate twiddle for the first dimension c2c fft */
    /* split factors */
    s_r3 = p.u.sp_2d_c2c_e.para1.s_r3;
    s_r5 = p.u.sp_2d_c2c_e.para1.s_r5;
    N_p2 = p.u.sp_2d_c2c_e.para1.N_p2;
    rad  = p.u.sp_2d_c2c_e.para1.rad;
    twiddle_gen (N1, M1, rad, use_bs_1, s_r3, s_r5, N_p2, tw, Bn, work);

    /* generate twiddle for the second dimension c2c fft */
    twsize = p.u.sp_2d_c2c_e.para1.twsize;
    Bnsize = p.u.sp_2d_c2c_e.para1.Bnsize;

    s_r3 = p.u.sp_2d_c2c_e.para2.s_r3;
    s_r5 = p.u.sp_2d_c2c_e.para2.s_r5;
    N_p2 = p.u.sp_2d_c2c_e.para2.N_p2;
    rad  = p.u.sp_2d_c2c_e.para2.rad;
    twiddle_gen (N2, M2, rad, use_bs_2, s_r3, s_r5, N_p2, tw+twsize, Bn+Bnsize, work);

    CACHE_wbInvAllL2(CACHE_WAIT);
    return (p);
}

/* ======================================================================== */
/*  End of file:  fft_plan_sp_2d_c2c.c                                      */
/* ------------------------------------------------------------------------ */
/*            Copyright (c) 2013 Texas Instruments, Incorporated.           */
/*                           All Rights Reserved.                           */
/* ======================================================================== */
