/* ======================================================================= */
/*  TEXAS INSTRUMENTS, INC.                                                */
/*                                                                         */
/*  FFTLIB  FFT Library                                                    */
/*                                                                         */
/* Copyright (C) 2013 Texas Instruments Incorporated - http://www.ti.com/  */
/*                                                                         */
/*                                                                         */
/*  Redistribution and use in source and binary forms, with or without     */
/*  modification, are permitted provided that the following conditions     */
/*  are met:                                                               */
/*                                                                         */
/*    Redistributions of source code must retain the above copyright       */
/*    notice, this list of conditions and the following disclaimer.        */
/*                                                                         */
/*    Redistributions in binary form must reproduce the above copyright    */
/*    notice, this list of conditions and the following disclaimer in the  */
/*    documentation and/or other materials provided with the               */
/*    distribution.                                                        */
/*                                                                         */
/*    Neither the name of Texas Instruments Incorporated nor the names of  */
/*    its contributors may be used to endorse or promote products derived  */
/*    from this software without specific prior written permission.        */
/*                                                                         */
/*  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS    */
/*  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT      */
/*  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR  */
/*  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT   */
/*  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,  */
/*  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT       */
/*  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,  */
/*  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY  */
/*  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT    */
/*  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE  */
/*  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.   */
/*                                                                         */
/* ======================================================================= */

#include <xdc/std.h>
#include <stdio.h>
#include <time.h>
#include <stdlib.h>
#include <limits.h>
#include <math.h>
#include <c6x.h>
#include <ti/csl/csl_cacheAux.h>

#include <ti/fftlib/src/common/omp/omp_config.h>
#include <ti/fftlib/src/common/fft_common_d.h>
#include "ifft_omp_dp_2d_c2r.h"
#include <ti/dsplib/src/DSPF_dp_fftDPxDP/DSPF_dp_fftDPxDP.h>
#include <ti/dsplib/src/DSPF_dp_ifftDPxDP/DSPF_dp_ifftDPxDP.h>
#include <ti/runtime/openmp/omp.h>

void dft_2d (int N1, int N2, double x[], double w[], double y[]);
void idft_2d(int N1, int N2, double x[], double w[], double y[]);
void dft (int N, double x[], double y[], int N1);
void idft (int N, double x[], double y[], int N1);

void tw_gen_cn (double *w, int n1, int n2);
void ifft_2d_cmp (int N1, int rad1, int N2, int rad2, double x[], double tw[], double y[]);

/* calculate radix for 1D array */
static void calculate_rad (int dim, int N, fft_para_mix_bs_t *fft_para)
{
	int n, j, M, rad;
	int s_r3, s_r5, N_p2,dum;
	int use_bs;

	use_bs = 1;

    n = (N<<1)-1;
    j = _norm(n);
    M = 1 << (31-j);
    if (dim==1)
      j = _norm(M);

    if (dim == 2)
    	N_p2 = N/2;
    else
    	N_p2 = N;
    s_r3 = 0;
    s_r5 = 0;

    /* if N is odd, bypass the rest of calculation, use Bluestein */
    if (((N>>1)<<1) == N)
    {
      /* if N is power of 2, no zero padding, regular radix 2 or 4 fft */
      if (N == (M>>1))
      { /* power of 2 */
        M = N;
        j = j+1;
        use_bs = 0;
      }
      else {
          if (dim == 2)
    	    dum = N/2;
          else
      	    dum = N;
          while (dum/3*3 == dum) {
            s_r3++;
            dum /= 3;
          }
          while (dum/5*5 == dum) {
            s_r5++;
            dum /= 5;
          }
          if ((dum >= 4) && (dum == (1 << (30-_norm(dum))))) {
          /* mixed radix 2 and 3 */
            N_p2                       = dum;
            use_bs = 0;
            M = N;
            j  = _norm(dum);
          }
        }
      }

    /* calculate radix for radix 2 or 4 c2r fft */
    if (dim==2)
    {
      if (j%2 == use_bs)
        rad = 4;
      else
        rad = 2;
    } else
    {
        if (j%2 == 0)
          rad = 4;
        else
          rad = 2;
    }

    fft_para->N_p2 = N_p2;
    fft_para->s_r3 = s_r3;
    fft_para->s_r5 = s_r5;
    fft_para->M    = M;
    fft_para->rad  = rad;
    fft_para->use_bs = use_bs;

} /*calculate_rad ()*/

/* calculate twiddle size, Bn buffer size and workbuffer size */
static void calculate_mem_size(int dim, int N, fft_para_mix_bs_t *fft_para, int *twsize, int *Bnsize, int *worksize)
{
  int dum, n, i;
  int use_bs = fft_para->use_bs;
  int M = fft_para->M;
  int s_r3 = fft_para->s_r3;
  int s_r5 = fft_para->s_r5;
  int N_p2 = fft_para->N_p2;
  /* Please note that Bn buffer and Workbuf are only used when doing Bluestein
   * meaning when use_bs == 1 */

  if (use_bs == 1)
  { /* when size is not power of 2 and not multiple of 3&5 */
	*twsize   = (2*M+2*N);
	*Bnsize   = (2*M);
	*worksize = (4*M);
  }
  else
  {
    if ((s_r3 == 0) && (s_r5 == 0)) {
    /* When signal is power of 2 */
        if (dim == 2)
          *twsize   = N*3;
        else
          *twsize   = N*2;
    }
    else {
    /* When signal is power of 3 or 5 */
      dum = 0;
      if (dim == 2)
        n  = 2*N;
      else
        n  = 4*N;
      for (i = 0; i < s_r3; i++) {
        dum += n/3;
        n  /= 3;
      }
      n *= 2;  /* radix-5 has 8 twiddle vs radix-3 4 twiddles */
      for (i = 0; i < s_r5; i++) {
        dum += n/5;
        n  /= 5;
      }
      if (dim == 2)
        *twsize = (N/2+2*N_p2+dum);
      else
        *twsize = (2*N_p2+dum);
    }
    *Bnsize   = 0;
    *worksize = 0;
  }
}

/* ======================================================================== */
/*  Kernel-specific alignments                                              */
/* ======================================================================== */
#pragma DATA_SECTION(sig_i, ".mem_ddr");
#pragma DATA_SECTION(workbuf_i, ".mem_ddr");
#pragma DATA_SECTION(sig_cn, ".mem_ddr");
#pragma DATA_SECTION(workbuf_cn, ".mem_ddr");
#pragma DATA_SECTION(w_cn, ".mem_ddr");
#pragma DATA_SECTION(w_i, ".mem_ddr");


#pragma DATA_ALIGN(sig_i,  8);
#pragma DATA_ALIGN(sig_cn, 8);

#pragma DATA_ALIGN(w_i, 8);
#pragma DATA_ALIGN(w_cn, 8);

#pragma DATA_ALIGN(workbuf_i,  8);
#pragma DATA_ALIGN(workbuf_cn, 8);

#pragma DATA_SECTION(local_work, ".mem_l2");

#pragma DATA_ALIGN(local_work,  64);


/* ======================================================================== */
/*  Parameters of fixed dataset.                                            */
/* ======================================================================== */

#if ANYSIZE_SUPPORT
#define MAXN  (512)
#else
#ifdef FFT_MEM_MODEL_LG
#define MAXN  (1024)
#else
# ifdef FFT_MEM_MODEL_MED
#  define MAXN  (512)
# else
#   ifdef FFT_MEM_MODEL_SM
#    define MAXN  (512)
#   else
#     error "Unsupported MEM MODEL!"
#   endif
# endif
#endif
#endif

#define M     (2*MAXN*MAXN)
#define PAD   (0)

/* ======================================================================== */
/*  Initialized arrays with fixed test data.                                */
/* ======================================================================== */

double sig_i [M + 2 * PAD];
double sig_cn[M + 2 * PAD];

double w_cn[6*MAXN + 2 * PAD];

double workbuf_i [M + 2 * PAD];
double workbuf_cn[M + 2 * PAD];


#if ANYSIZE_SUPPORT
double w_i[3*6*MAXN + M + 2 + 2*8*MAXN + 2 * PAD];
double local_work [16432 + 1024+MAXN*10*IFFT_OMP_DP_2D_C2R_NUMOFLINEBUFS + 8*MAXN + 8*MAXN + 6*MAXN + 6 + 2 * PAD]; /* x,y + work + Bn + tw */
#else
double w_i[6*MAXN + M + 2 + 2 * PAD];
#ifdef FFT_MEM_MODEL_LG
double local_work [16432 + MAXN*10*IFFT_OMP_DP_2D_C2R_NUMOFLINEBUFS + 1024 + 2 * PAD];
#else
# ifdef FFT_MEM_MODEL_MED
  double local_work [MAXN*10*IFFT_OMP_DP_2D_C2R_NUMOFLINEBUFS + 1024 + 2 * PAD];
# else
#   ifdef FFT_MEM_MODEL_SM
    double local_work [MAXN*10*IFFT_OMP_DP_2D_C2R_NUMOFLINEBUFS + 1024 + 2 * PAD];
#   else
#     error "Unsupported MEM MODEL!"
#   endif
# endif
#endif
#endif

/* ======================================================================== */
/*  Generate pointers to skip beyond array padding                          */
/* ======================================================================== */
double *const ptr_sig_i  = sig_i  + PAD;
double *const ptr_sig_cn = sig_cn + PAD;

double *const ptr_w_cn = w_cn + PAD;
double *const ptr_w_i  = w_i + PAD;


double *const ptr_workbuf_i  = workbuf_i  + PAD;
double *const ptr_workbuf_cn = workbuf_cn + PAD;

double *const ptr_local_work  = local_work + PAD;

/* ======================================================================== */
/*  MAIN -- Top level driver for the test.                                  */
/* ======================================================================== */

int main ()
{
  int    i, j, k, N;
  int    N1, N2;
  int    int_tw_size, ext_tw_size, twsize, localsize;
  int    Bnsize, worksize, int_Bn_size, ext_Bn_size, work_size;
  clock_t t_start, t_stop, t_overhead, t_opt;
  double   diff, max_diff = 0;
  double  *ptr_scratch = NULL;
  fft_plan_t p;
  fft_callout_t plan_fxns;
  size_t  l2_SRAM_size_orig;
  uint32_t *temp;
  lib_memdscr_t **fft_mem_handle = fftGetMemHandle();

  /* --------------------------------------------------------------------- */
  /* intialize hardware timers                                             */
  /* --------------------------------------------------------------------- */
  TSCL=0;TSCH=0;

  /* initalize callout functions */
  plan_fxns.memoryRequest   = NULL;
  plan_fxns.memoryRelease   = NULL;
  plan_fxns.ecpyRequest     = fft_omp_assign_edma_resources;
  plan_fxns.ecpyRelease     = fft_omp_free_edma_resources;
    
  /* initialize ECPY */ 
  omp_set_num_threads (ACTIVE_THREAD_COUNT);
  fft_config_memory (&l2_SRAM_size_orig);
  temp = (uint32_t *)lib_smem_falloc(fft_mem_handle, 256*6*sizeof(uint32_t), 3);
#if 1
  if(temp==NULL) {
      printf("Memory allocation error!\n");
      return;
  }
#endif

  #pragma omp parallel
  {
    fft_assert( (lib_emt_init() == LIB_EMT_SUCCESS), DNUM, "lib_emt_init() return error!");
    fftEdmaState[DNUM] = FFT_EDMA_STATE_INIT;
  }

    /* radix 2&4 testing */
    for (N1 = 128, N2 = 32, k = 1; N2 <= MAXN; N2 = N2 << 1, k++)
    {

        memset (sig_i,  0x55, sizeof (sig_i) );
        memset (sig_cn, 0x55, sizeof (sig_cn));
        memset (workbuf_cn,  0x55, sizeof (workbuf_cn) );

        /* ---------------------------------------------------------------- */
        /* Construct the input signal to meet the requirement for c2r.      */
        /* ---------------------------------------------------------------- */
        for (j = 0; j < N2; j++) {
           for (i = 0; i < N1; i++) {
               sig_cn[PAD + 2*i + j*2*N1]     = sin (2 * 3.14159265358979323846 * (50+j) * i / (double) (N1));
               sig_cn[PAD + 2*i + j*2*N1 + 1] = 0;
           }
        }

        dft_2d (N2, N1, sig_cn, workbuf_cn, sig_i);

        /* Now to construct the signal under test to provide only half of the spectrum  */

        for (k = 0, i = 0; i < N2; i++)
          for (j = 0; j < N1/2+1; j++){
            sig_i[2*k]     = sig_i[i*2*N1 + 2*j];
            sig_i[2*k + 1] = sig_i[i*2*N1 + 2*j + 1];
            k ++;
          }

        for (i = 2*k; i < 2*N1*N2; i++){
        	sig_i[i] = 0;
        }
        /* ---------------------------------------------------------------- */
        /* Force uninitialized arrays to fixed values.                      */
        /* ---------------------------------------------------------------- */
        memset (workbuf_i,  0xA5, sizeof (workbuf_i) );

        /* ARM part of plan */
        /* determine the rad for the first  & second dimensions */
        calculate_rad(1, N1, &p.u.dp_2d_c2r_e.para1);

        calculate_rad(2, N2, &p.u.dp_2d_c2r_e.para2);

        /* Calculate mem size for 2 dimensions for ECPY */
        /* evaluate the  1st dimension */

        calculate_mem_size(1, N1, &p.u.dp_2d_c2r_e.para1, &twsize, &Bnsize, &worksize);
        p.u.dp_2d_c2r_e.para1.twsize = twsize;
        p.u.dp_2d_c2r_e.para1.Bnsize = Bnsize;
        int_tw_size = ext_tw_size = twsize;
        int_Bn_size = ext_Bn_size = Bnsize;
        work_size   = worksize;

        /* evaluate the 2nd dimension */

        calculate_mem_size(2, N2, &p.u.dp_2d_c2r_e.para2, &twsize, &Bnsize, &worksize);
        p.u.dp_2d_c2r_e.para2.twsize = twsize;
        p.u.dp_2d_c2r_e.para2.Bnsize = Bnsize;
        int_tw_size  = (int_tw_size > twsize) ? int_tw_size : twsize;
        ext_tw_size += twsize;
        int_Bn_size  = (int_Bn_size > Bnsize) ? int_Bn_size : Bnsize;
        ext_Bn_size += Bnsize;
        work_size    = (work_size > worksize) ? work_size : worksize;

        /* calculate local memory requirements */
        N = (N1 > N2) ? N1 : N2;
#ifdef FFT_MEM_MODEL_LG
        localsize = sizeof(double)*(1024 + 10*N*IFFT_OMP_DP_2D_C2R_NUMOFLINEBUFS + int_tw_size);
#else
# ifdef FFT_MEM_MODEL_MED
        localsize = sizeof(double)*(1024 + 10*N*IFFT_OMP_DP_2D_C2R_NUMOFLINEBUFS);
# else
#   ifdef FFT_MEM_MODEL_SM
        localsize = sizeof(double)*(1024 + 10*N*IFFT_OMP_DP_2D_C2R_NUMOFLINEBUFS);
#   else
#     error "Unsupported MEM MODEL!"
#   endif
# endif
#endif

        ptr_scratch = malloc(sizeof(double)*work_size);
        ifft_omp_dp_plan_2d_c2r (N1, N2, FFT_ECPY, plan_fxns, &p, ptr_sig_i ,ptr_workbuf_i, ptr_w_i, ptr_scratch);
        free(ptr_scratch);

//        CACHE_wbInvAllL2(CACHE_WAIT);

        /* ---------------------------------------------------------------- */
        /* Compute the overhead of calling clock twice to get timing info   */
        /* ---------------------------------------------------------------- */
        p.edmaState = (*p.fftcout.ecpyRequest)(temp, FFT_NUM_EDMA_CH*FFT_MAX_EDMA_LINKS_3D*sizeof(float));
        (*p.fftcout.ecpyRelease)(p.edmaState);
        p.edmaState = (*p.fftcout.ecpyRequest)(temp, FFT_NUM_EDMA_CH*FFT_MAX_EDMA_LINKS_3D*sizeof(float));
        (*p.fftcout.ecpyRelease)(p.edmaState);
        t_start = _itoll(TSCH, TSCL);
        p.edmaState = (*p.fftcout.ecpyRequest)(temp, FFT_NUM_EDMA_CH*FFT_MAX_EDMA_LINKS_3D*sizeof(float));
        (*p.fftcout.ecpyRelease)(p.edmaState);
        t_stop  = _itoll(TSCH, TSCL);
        t_overhead = t_stop - t_start;

        /* ---------------------------------------------------------------------- */
        /* Set the number of cores used                                           */
        /* ---------------------------------------------------------------------- */
        p.actualCoreNum = ACTIVE_THREAD_COUNT;
        p.local = local_work;

        t_start = _itoll(TSCH, TSCL);
        fft_execute (p);
        t_stop = _itoll(TSCH, TSCL);

        t_opt  = (t_stop - t_start) - t_overhead;

        /* ---------------------------------------------------------------- */
        /* compute difference and track max difference                      */  
        /* ---------------------------------------------------------------- */
        diff = 0; max_diff = 0;
        for(i=0; i<N1*N2; i++) {
          diff = _fabs(ptr_sig_cn[2*i]*N1*N2 - ptr_workbuf_i[i]);
          if (diff > max_diff)
        	  max_diff = diff;
        }
        
        printf("ifft_omp_dp_2d_c2r_ecpy\tsize=#: %dx%d\t", N1, N2);
        printf("max_diff = %f", max_diff);
        printf("\tCycle: %d\n\n",t_opt);

    }

#if 1
    /* radix 3,5 & bluestein testing, note: only 3&5 tests are supported */
    for (N1 = 32, N2 = 32, k = 1; N2 <= MAXN; N1 = N1*3, N2 = N2*5)
    {

        memset (sig_i,  0x55, sizeof (sig_i) );
        memset (sig_cn, 0x55, sizeof (sig_cn));
        memset (workbuf_cn,  0x55, sizeof (workbuf_cn) );

        /* ---------------------------------------------------------------- */
        /* Construct the input signal to meet the requirement for c2r.      */
        /* ---------------------------------------------------------------- */
        for (j = 0; j < N2; j++) {
           for (i = 0; i < N1; i++) {
               sig_cn[PAD + 2*i + j*2*N1]     = sin (2 * 3.14159265358979323846 * (50+j) * i / (double) (N1));
               sig_cn[PAD + 2*i + j*2*N1 + 1] = 0;
           }
        }

        dft_2d (N2, N1, sig_cn, workbuf_cn, sig_i);

        /* Now to construct the signal under test to provide only half of the spectrum  */

        for (k = 0, i = 0; i < N2; i++)
          for (j = 0; j < N1/2+1; j++){
            sig_i[2*k]     = sig_i[i*2*N1 + 2*j];
            sig_i[2*k + 1] = sig_i[i*2*N1 + 2*j + 1];
            k ++;
          }

        for (i = 2*k; i < 2*N1*N2; i++){
        	sig_i[i] = 0;
        }

        /* ---------------------------------------------------------------- */
        /* Force uninitialized arrays to fixed values.                      */
        /* ---------------------------------------------------------------- */
        memset (workbuf_i,  0xA5, sizeof (workbuf_i) );

        /* ARM part of plan */
        /* determine the rad for the first  & second dimensions */
        calculate_rad(1, N1, &p.u.dp_2d_c2r_e.para1);

        calculate_rad(2, N2, &p.u.dp_2d_c2r_e.para2);

        /* Calculate mem size for 2 dimensions for ECPY */
        /* evaluate the  1st dimension */

        calculate_mem_size(1, N1, &p.u.dp_2d_c2r_e.para1, &twsize, &Bnsize, &worksize);
        p.u.dp_2d_c2r_e.para1.twsize = twsize;
        p.u.dp_2d_c2r_e.para1.Bnsize = Bnsize;
        int_tw_size = ext_tw_size = twsize;
        int_Bn_size = ext_Bn_size = Bnsize;
        work_size   = worksize;

        /* evaluate the 2nd dimension */

        calculate_mem_size(2, N2, &p.u.dp_2d_c2r_e.para2, &twsize, &Bnsize, &worksize);
        p.u.dp_2d_c2r_e.para2.twsize = twsize;
        p.u.dp_2d_c2r_e.para2.Bnsize = Bnsize;
        int_tw_size  = (int_tw_size > twsize) ? int_tw_size : twsize;
        ext_tw_size += twsize;
        int_Bn_size  = (int_Bn_size > Bnsize) ? int_Bn_size : Bnsize;
        ext_Bn_size += Bnsize;
        work_size    = (work_size > worksize) ? work_size : worksize;

        /* calculate local memory requirements */
        N = (N1 > N2) ? N1 : N2;
#ifdef FFT_MEM_MODEL_LG
        localsize = sizeof(double)*(1024 + 10*N*IFFT_OMP_DP_2D_C2R_NUMOFLINEBUFS + int_tw_size);
#else
# ifdef FFT_MEM_MODEL_MED
        localsize = sizeof(double)*(1024 + 10*N*IFFT_OMP_DP_2D_C2R_NUMOFLINEBUFS);
# else
#   ifdef FFT_MEM_MODEL_SM
        localsize = sizeof(double)*(1024 + 10*N*IFFT_OMP_DP_2D_C2R_NUMOFLINEBUFS);
#   else
#     error "Unsupported MEM MODEL!"
#   endif
# endif
#endif

        ptr_scratch = malloc(sizeof(double)*work_size);
        ifft_omp_dp_plan_2d_c2r (N1, N2, FFT_ECPY, plan_fxns, &p, ptr_sig_i ,ptr_workbuf_i, ptr_w_i, ptr_scratch);
        free(ptr_scratch);


        /* ---------------------------------------------------------------- */
        /* Compute the overhead of calling clock twice to get timing info   */
        /* ---------------------------------------------------------------- */
        p.edmaState = (*p.fftcout.ecpyRequest)(temp, FFT_NUM_EDMA_CH*FFT_MAX_EDMA_LINKS_3D*sizeof(float));
        (*p.fftcout.ecpyRelease)(p.edmaState);
        p.edmaState = (*p.fftcout.ecpyRequest)(temp, FFT_NUM_EDMA_CH*FFT_MAX_EDMA_LINKS_3D*sizeof(float));
        (*p.fftcout.ecpyRelease)(p.edmaState);
        t_start = _itoll(TSCH, TSCL);
        p.edmaState = (*p.fftcout.ecpyRequest)(temp, FFT_NUM_EDMA_CH*FFT_MAX_EDMA_LINKS_3D*sizeof(float));
        (*p.fftcout.ecpyRelease)(p.edmaState);
        t_stop  = _itoll(TSCH, TSCL);
        t_overhead = t_stop - t_start;

        /* ---------------------------------------------------------------------- */
        /* Set the number of cores used                                           */
        /* ---------------------------------------------------------------------- */
        p.actualCoreNum = ACTIVE_THREAD_COUNT;
        p.local = local_work;

        t_start = _itoll(TSCH, TSCL);
        fft_execute (p);
        t_stop = _itoll(TSCH, TSCL);

        t_opt  = (t_stop - t_start) - t_overhead;

        /* ---------------------------------------------------------------- */
        /* compute difference and track max difference                      */  
        /* ---------------------------------------------------------------- */
        diff = 0; max_diff = 0;
        for(i=0; i<N1*N2; i++) {
          diff = _fabs(ptr_sig_cn[2*i]*N1*N2 - ptr_workbuf_i[i]);
          if (diff > max_diff)
        	  max_diff = diff;
        }
        
        printf("ifft_omp_dp_2d_c2r_ecpy\tsize=#: %dx%d\t", N1, N2);
        printf("max_diff = %f", max_diff);
        printf("\tCycle: %d\n\n", t_opt);
    }
#endif  
}

/* Function for generating Specialized sequence of twiddle factors */
void tw_gen_cn (double *w, int n1, int n2)
{
    int i, j, k;
    const double PI = 3.14159265358979323846;

    for (j = 1, k = 0; j <= n1 >> 2; j = j << 2)
    {
        for (i = 0; i < n1 >> 2; i += j)
        {
#ifdef _LITTLE_ENDIAN
            w[k]     = sin (2 * PI * i / n1);
            w[k + 1] = cos (2 * PI * i / n1);
            w[k + 2] = sin (4 * PI * i / n1);
            w[k + 3] = cos (4 * PI * i / n1);
            w[k + 4] = sin (6 * PI * i / n1);
            w[k + 5] = cos (6 * PI * i / n1);
#else
            w[k]     = cos (2 * PI * i / n1);
            w[k + 1] = -sin (2 * PI * i / n1);
            w[k + 2] = cos (4 * PI * i / n1);
            w[k + 3] = -sin (4 * PI * i / n1);
            w[k + 4] = cos (6 * PI * i / n1);
            w[k + 5] = -sin (6 * PI * i / n1);
#endif
            k += 6;
        }
    }

    k = 2*n1;

    for (j = 1; j <= n2 >> 2; j = j << 2)
    {
        for (i = 0; i < n2 >> 2; i += j)
        {
#ifdef _LITTLE_ENDIAN
            w[k]     = sin (2 * PI * i / n2);
            w[k + 1] = cos (2 * PI * i / n2);
            w[k + 2] = sin (4 * PI * i / n2);
            w[k + 3] = cos (4 * PI * i / n2);
            w[k + 4] = sin (6 * PI * i / n2);
            w[k + 5] = cos (6 * PI * i / n2);
#else
            w[k]     = cos (2 * PI * i / n2);
            w[k + 1] = -sin (2 * PI * i / n2);
            w[k + 2] = cos (4 * PI * i / n2);
            w[k + 3] = -sin (4 * PI * i / n2);
            w[k + 4] = cos (6 * PI * i / n2);
            w[k + 5] = -sin (6 * PI * i / n2);
#endif
            k += 6;
        }
    }
}

/* Function for calculating 2d IFFT */
void idft_2d(int N1, int N2, double x[], double w[], double y[])
{
    int k, i, j;

    double *restrict p_x = x, *restrict p_w = w, *restrict p_y = y;

    k = 0;
    /* calculate N1 iFFT size of N2 */
    for (k = 0; k < N1; k++)
    {
      p_x = x + k*2*N2;
      p_w = w + k*2*N2;
      idft (N2, p_x, p_w, N2);
    }
    /* matrix transpose */
    for(i = 0; i < 2*N1; i+=2)
        #pragma MUST_ITERATE(2,,)
        for(j = 0; j < N2; j++)
        {
            y[2*j*N1 + i]     = w[2*j + N2*i];
            y[2*j*N1 + i + 1] = w[2*j + N2*i + 1];
        }

    /* calculate N2 iFFT size of N1 */
    for (k = 0; k < N2; k++)
    {
        p_w = w + k*2*N1;
        p_y = y + k*2*N1;
        idft (N1, p_y, p_w, N1);

    }

    /* matrix transpose */
    for(i = 0; i < 2*N2; i+=2)
        #pragma MUST_ITERATE(2,,)
        for(j = 0; j < N1; j++)
        {
            y[2*j*N2 + i]     = w[2*j + N1*i];
            y[2*j*N2 + i + 1] = w[2*j + N1*i + 1];
        }
}

/* Function for calculating 2d FFT */
void dft_2d (int N1, int N2, double x[], double w[], double y[])
{
    int k, i, j;

    double *restrict p_x = x, *restrict p_w = w, *restrict p_y = y;

    k = 0;
    /* calculate N1 FFT size of N2 */
    for (k = 0; k < N1; k++)
    {
      p_x = x + k*2*N2;
      p_w = w + k*2*N2;
      dft (N2, p_x, p_w, N2);
    }
    /* matrix transpose */
    for(i = 0; i < 2*N1; i+=2)
        #pragma MUST_ITERATE(2,,)
        for(j = 0; j < N2; j++)
        {
            y[2*j*N1 + i]     = w[2*j + N2*i];
            y[2*j*N1 + i + 1] = w[2*j + N2*i + 1];
        }

    /* calculate N2 FFT size of N1 */
    for (k = 0; k < N2; k++)
    {
        p_w = w + k*2*N1;
        p_y = y + k*2*N1;
        dft (N1, p_y, p_w, N1);

    }

    /* matrix transpose */
    for(i = 0; i < 2*N2; i+=2)
        #pragma MUST_ITERATE(2,,)
        for(j = 0; j < N1; j++)
        {
            y[2*j*N2 + i]     = w[2*j + N1*i];
            y[2*j*N2 + i + 1] = w[2*j + N1*i + 1];
        }
}

/* Function for calculating 1D any size DFT */
void dft (int N, double x[], double y[], int N1)
{
    int k, i, index;
    const double PI = 3.14159265358979323846;
    double *p_x;
    double arg, fx_0, fx_1, fy_0, fy_1, co, si;

    for (k = 0; k < N1; k++)
    {
        p_x = x;

        fy_0 = 0;
        fy_1 = 0;
        for (i = 0; i < N; i++)
        {
            fx_0 = p_x[0];
            fx_1 = p_x[1];
            p_x += 2;
            index = (i * k) % N;
            arg = 2 * PI * index / N;
            co = cos (arg);
            si = -sin (arg);
            fy_0 += ((fx_0 * co) - (fx_1 * si));
            fy_1 += ((fx_1 * co) + (fx_0 * si));
        }
        y[2 * k] = fy_0;
        y[2 * k + 1] = fy_1;
    }
}

void idft (int N, double x[], double y[], int N1)
{
    int k, i, index;
    const double PI = 3.14159265358979323846;
    double *p_x;
    double arg, fx_0, fx_1, fy_0, fy_1, co, si;

    for (k = 0; k < N1; k++)
    {
        p_x = x;

        fy_0 = 0;
        fy_1 = 0;
        for (i = 0; i < N; i++)
        {
            fx_0 = p_x[0];
            fx_1 = p_x[1];
            p_x += 2;
            index = (i * k) % N;
            arg = 2 * PI * index / N;
            co = cos (arg);
            si = sin (arg);
            fy_0 += ((fx_0 * co) - (fx_1 * si));
            fy_1 += ((fx_1 * co) + (fx_0 * si));
        }
        y[2 * k] = fy_0;
        y[2 * k + 1] = fy_1;
    }
}

/* ======================================================================== */
/*  End of file:  ifft_omp_dp_2d_c2r_d.c                                    */
/* ------------------------------------------------------------------------ */
/*            Copyright (c) 2013 Texas Instruments, Incorporated.           */
/*                           All Rights Reserved.                           */
/* ======================================================================== */
