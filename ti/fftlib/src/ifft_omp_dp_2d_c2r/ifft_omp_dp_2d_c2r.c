/* ======================================================================= */
/*  TEXAS INSTRUMENTS, INC.                                                */
/*                                                                         */
/*  FFTLIB  FFT Library                                                    */
/*                                                                         */
/* Copyright (C) 2013 Texas Instruments Incorporated - http://www.ti.com/  */
/*                                                                         */
/*                                                                         */
/*  Redistribution and use in source and binary forms, with or without     */
/*  modification, are permitted provided that the following conditions     */
/*  are met:                                                               */
/*                                                                         */
/*    Redistributions of source code must retain the above copyright       */
/*    notice, this list of conditions and the following disclaimer.        */
/*                                                                         */
/*    Redistributions in binary form must reproduce the above copyright    */
/*    notice, this list of conditions and the following disclaimer in the  */
/*    documentation and/or other materials provided with the               */
/*    distribution.                                                        */
/*                                                                         */
/*    Neither the name of Texas Instruments Incorporated nor the names of  */
/*    its contributors may be used to endorse or promote products derived  */
/*    from this software without specific prior written permission.        */
/*                                                                         */
/*  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS    */
/*  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT      */
/*  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR  */
/*  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT   */
/*  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,  */
/*  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT       */
/*  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,  */
/*  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY  */
/*  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT    */
/*  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE  */
/*  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.   */
/*                                                                         */
/* ======================================================================= */

#include <xdc/std.h>
#include "c6x.h"
#include "stdlib.h"
#include "stdio.h"
#include <ti/fftlib/src/common/omp/omp_config.h>
#include "ifft_omp_dp_2d_c2r.h"
#include <ti/fftlib/src/common/ifft/ifft_dp_mixed_bs.h>
#include <ti/dsplib/src/DSPF_dp_ifftDPxDP/DSPF_dp_ifftDPxDP.h>
#include <ti/fftlib/src/common/fft_edma.h>
#include <ti/dsplib/src/DSPF_dp_mat_trans/DSPF_dp_mat_trans.h>
#include <ti/runtime/openmp/omp.h>
#include <ti/libarch/libarch.h>

static void DSPF_dp_mat_trans_cplx(const double *restrict x, const int rows,
  const int cols, double *restrict y)
{
  int i, j = 0;

  #pragma MUST_ITERATE(2,,)
  for(i = 0; i < rows; i++)
  #pragma MUST_ITERATE(2,,)
  for(j = 0; j < cols; j++)
  {
    y[j*rows*2 + i*2] = x[j*2 + cols*i*2];
    y[j*rows*2 + i*2 + 1] = x[1 + j*2 + cols*i*2];
  }
}

static int ifft_omp_dp_1d_c2c_ecpy_transpose (
    int   N1,                 /* FFT size                       */
    int   N2,                 /* number of lines                */
    int   M,                  /* Padded size of Anysize FFT     */
    int   rad,                /* radix                          */
    int   N_p2,               /* power of 2 size in mixed radix */
    int   s_r3,               /* number stages of radix 3       */
    int   s_r5,               /* number stages of radix 5       */
    double *in,               /* input pointer                  */
    double *out,              /* output pointer                 */
    double *in_w,             /* input work buffer pointer      */
    double *out_w,            /* output working buffer pointer  */
    double *out_t,            /* temp working buffer pointer    */
    double *tw,               /* twiddle pointer                */
    double *Bn_w,
    double *workbuf_bn,
    FFT_EDMA_Struct  *edma,  /* edma resource pointer          */
    int num_of_lines
)
{
  int   error = FFT_NOERR;
  int   i, j, k, ch_i,ch_o,ch_i1,ch_o1,dum0,dum1,numThread, tid, lines;
  int   insize    = (N1/2+1)*sizeof(double)*num_of_lines,
        outsample = N1,
        linesize  = N2*2*sizeof(double);
  FFT_EDMA_Struct  *edmall2;

  double *restrict in_ptr_f = in, *restrict out_ptr_f = out, *restrict tempout;
  double *restrict in_ptr_b, *restrict out_ptr_b;
  double *restrict input, *restrict output, *restrict in1, *restrict out1, *dum2, *dum3;

  edmall2 = edma;
  numThread = omp_get_max_threads();
  lines = N2/numThread;
  tid = omp_get_thread_num();


  in_ptr_f  = &in[(1+(num_of_lines/2)*tid)*2*(N1/2+1)];            /* pointer starts at the left side of the N1xN2 matrix */
  in_ptr_b  = &in[(N2*N1/2+N2)*2-((1+tid)*num_of_lines/2)*2*(N1/2+1)];    /* pointer starts at the right side of the N1xN2 matrix */
  out_ptr_f = &out[(1+(num_of_lines/2)*tid)*2];
  out_ptr_b = &out[(N2-(1+tid)*num_of_lines/2)*2];

  /* prepare first FFT input buffer, get half from front and back each */
  lib_emt_copy1D1D(
    edmall2->channel[FFT_XFER_CHANNEL0],
    (void *)in_ptr_f,
    (void *)in_w,
    insize);
  in_ptr_f += (N1/2+1)*num_of_lines*numThread;

  /* wait for front FFT input */
  lib_emt_wait(edmall2->channel[FFT_XFER_CHANNEL0]);

  lib_emt_copy1D1D(
    edmall2->channel[FFT_XFER_CHANNEL0],
    (void *)in_ptr_b,
    (void *)(in_w+num_of_lines*(N1/2+1)),
    insize);
  in_ptr_b -= (N1/2+1)*num_of_lines*numThread;

  /* set pingpong buffer pointers */
  in1    = in_w;
  out1   = out_w;
  input  = &in_w[2*N1*num_of_lines];
  output = &out_w[2*N1*num_of_lines];
  tempout= out_t;

  /* prepare second FFT input buffer */
  lib_emt_copy1D1D(
    edmall2->channel[FFT_XFER_CHANNEL2],
    (void *)in_ptr_f,
    (void *)input,
    insize);
  in_ptr_f += (N1/2+1)*num_of_lines*numThread;

  /* wait for front FFT input */
  lib_emt_wait(edmall2->channel[FFT_XFER_CHANNEL2]);

  lib_emt_copy1D1D(
    edmall2->channel[FFT_XFER_CHANNEL2],
    (void *)in_ptr_b,
    (void *)(input+num_of_lines*(N1/2+1)),
    insize);
  in_ptr_b -= (N1/2+1)*num_of_lines*numThread;

  /* wait for first FFT input */
  lib_emt_wait(edmall2->channel[FFT_XFER_CHANNEL0]);


  /* first copy valid data to proper position, front, back together */
  for (i = 0; i<num_of_lines; i++){
    for (j = 0; j<N1/2+1; j++){
    	out1[j*2+i*N1*2]   = in1[j*2+i*(N1/2+1)*2];
    	out1[j*2+i*N1*2+1] = in1[j*2+i*(N1/2+1)*2+1];
    }
  }

  /* reconstruct the signal to full spectrum, since the spectrum is symmetrical, only the front half needs to be reconstructed */
  for (i = 0; i<num_of_lines/2; i++){
    for (j = 1; j<N1/2; j++){
      out1[(N1-j)*2+i*N1*2]     =  out1[(3*N1+j)*2-i*N1*2];
 	  out1[(N1-j)*2+i*N1*2+1]   = -out1[(3*N1+j)*2-i*N1*2+1];
  	}
  }

  for (i = 0; i<num_of_lines/2; i++){
    for (j = 1; j<N1/2; j++){
      out1[(4*N1-j)*2-i*N1*2]     =  out1[j*2+i*N1*2];
 	  out1[(4*N1-j)*2-i*N1*2+1]   = -out1[j*2+i*N1*2+1];
  	}
  }

  /* doing the 1D DP FFT */
#if (IFFT_OMP_DP_2D_C2R_NUMOFLINEBUFS>1)
    if (N1 == M) {
      if (s_r3 == 0 && s_r5 == 0) {
        for (i=0; i<num_of_lines; i++)
          DSPF_dp_ifftDPxDP (N1, &out1[i*N1*2], tw, &tempout[i*N1*2], rad, 0, N1);
      }
      else {
        for (i=0; i<num_of_lines; i++)
          ifft_DPxDP_mixed_radix (N1, &out1[i*N1*2], tw, &tempout[i*N1*2], N_p2, s_r3, s_r5, rad);
      }
    }
    else{
      for (i=0; i<num_of_lines; i++)
        ifft_DPxDP_bs (N1, M, rad, &out1[i*N1*2], &tempout[i*N1*2], tw, Bn_w, workbuf_bn);
    }

    /* local transpose from tempout to out1 */

    if ((lines > num_of_lines) || tid < (numThread-1)){
       /* local transpose */
       DSPF_dp_mat_trans_cplx(tempout, num_of_lines/2, N1, out1);
       DSPF_dp_mat_trans_cplx(&tempout[num_of_lines*N1], num_of_lines/2, N1, &out1[num_of_lines*N1]);
     }
     else {
       /* copy to output*/

       for (i = 0; i<N1; i++){
         out1[2*i]   = tempout[2*i];
         out1[2*i+1] = tempout[2*i+1];
         out1[num_of_lines*N1+2*i]   =  tempout[2*i+N1*(num_of_lines-1)*2];
         out1[num_of_lines*N1+2*i+1] =  tempout[2*i+N1*(num_of_lines-1)*2+1];
       }
    }

 #else
    if (N1 == M) {
      if (s_r3 == 0 && s_r5 == 0) {
    	  DSPF_dp_ifftDPxDP (N1, in1, tw, out1, rad, 0, N1);
      }
      else {
        ifft_DPxDP_mixed_radix (N1, in1, tw, out1, N_p2, s_r3, s_r5, rad);
      }
    }
    else{
      ifft_DPxDP_bs (N1, M, rad, in1, out1, tw, Bn_w, workbuf_bn);
    }
#endif

  if((lines > num_of_lines) || (tid < (numThread - 1))){
    /* export output data of first FFT*/
    lib_emt_copy1D2D(
    edmall2->channel[FFT_XFER_CHANNEL1],
    (void *)out1,
    (void *)out_ptr_f,
    sizeof(double)*num_of_lines,
    outsample,
    linesize);
    out_ptr_f += num_of_lines*numThread;

    /* wait for EDMA */
    lib_emt_wait(edmall2->channel[FFT_XFER_CHANNEL1]);

    lib_emt_copy1D2D(
    edmall2->channel[FFT_XFER_CHANNEL1],
    (void *)(out1+num_of_lines*N1),
    (void *)out_ptr_b,
    sizeof(double)*num_of_lines,
    outsample,
    linesize);
    out_ptr_b -= num_of_lines*numThread;

    /* wait for EDMA */
    lib_emt_wait(edmall2->channel[FFT_XFER_CHANNEL1]);
  }
  else {
        /* export the last 2 column's output data*/
        lib_emt_copy1D2D(
        edmall2->channel[FFT_XFER_CHANNEL1],
        (void *)out1,
        (void *)out_ptr_f,
        2*sizeof(double),
        outsample,
        linesize);

        /* wait for EDMA */
        lib_emt_wait(edmall2->channel[FFT_XFER_CHANNEL1]);

        lib_emt_copy1D2D(
        edmall2->channel[FFT_XFER_CHANNEL1],
        (void *)(out1+num_of_lines*N1),
        (void *)(out_ptr_b+2),
        2*sizeof(double),
        outsample,
        linesize);

        /* wait for EDMA */
        lib_emt_wait(edmall2->channel[FFT_XFER_CHANNEL1]);
  }

#ifdef USEFASTEDMA
  /* fill the PaRAM for CHANNEL 3 without starting it */
  lib_emt_copy1D2D_Fill(
    edmall2->channel[FFT_XFER_CHANNEL3],
    (void *)out1,
    (void *)out_ptr,
    2*sizeof(double)*num_of_lines,
    outsample,
    linesize);
#endif

  /* prepare third FFT input buffer */
#ifndef USEFASTEDMA
  lib_emt_copy1D1D(
    edmall2->channel[FFT_XFER_CHANNEL0],
    (void *)in_ptr_f,
    (void *)in_w,
    insize);
  lib_emt_wait(edmall2->channel[FFT_XFER_CHANNEL0]);

  lib_emt_copy1D1D(
  edmall2->channel[FFT_XFER_CHANNEL0],
  (void *)in_ptr_b,
  (void *)(in_w+num_of_lines*(N1/2+1)),
  insize);
  lib_emt_wait(edmall2->channel[FFT_XFER_CHANNEL0]);

#else
  lib_emt_copy1D1D_Fast(
    edmall2->channel[FFT_XFER_CHANNEL0],
    (void *)in_ptr,
    (void *)in_w);
#endif
  in_ptr_f += (N1/2+1)*num_of_lines*numThread;
  in_ptr_b -= (N1/2+1)*num_of_lines*numThread;

  /* set channel numbers */
  ch_i  = FFT_XFER_CHANNEL2;
  ch_o  = FFT_XFER_CHANNEL3;
  ch_i1 = FFT_XFER_CHANNEL0;
  ch_o1 = FFT_XFER_CHANNEL1;

  /* wait for EDMA */
  lib_emt_wait(edmall2->channel[ch_i]);

  _nassert (N2 >= 4);
  for (k = num_of_lines; k < lines; k += num_of_lines)
  {

	  /* first copy valid data to proper position, front, back together */
	  for (i = 0; i<num_of_lines; i++){
	    for (j = 0; j<N1/2+1; j++){
	    	output[j*2+i*N1*2]   = input[j*2+i*(N1/2+1)*2];
	    	output[j*2+i*N1*2+1] = input[j*2+i*(N1/2+1)*2+1];
	    }
	  }

	  /* reconstruct the signal to full spectrum, since the spectrum is symmetrical, only the front half needs to be reconstructed */
	  for (i = 0; i<num_of_lines/2; i++){
	    for (j = 1; j<N1/2; j++){
	      output[(N1-j)*2+i*N1*2]     =  output[(3*N1+j)*2-i*N1*2];
	      output[(N1-j)*2+i*N1*2+1]   = -output[(3*N1+j)*2-i*N1*2+1];
	  	}
	  }
	  for (i = 0; i<num_of_lines/2; i++){
	    for (j = 1; j<N1/2; j++){
	      output[(4*N1-j)*2-i*N1*2]     =  output[j*2+i*N1*2];
	      output[(4*N1-j)*2-i*N1*2+1]   = -output[j*2+i*N1*2+1];
	  	}
	  }

#if (IFFT_OMP_DP_2D_C2R_NUMOFLINEBUFS>1)
      if (N1 == M) {
        if (s_r3 == 0 && s_r5 == 0) {
          for (i=0; i<num_of_lines; i++)
            DSPF_dp_ifftDPxDP (N1, &output[i*N1*2], tw, &tempout[i*N1*2], rad, 0, N1);
        }
        else {
          for (i=0; i<num_of_lines; i++)
            ifft_DPxDP_mixed_radix (N1, &output[i*N1*2], tw, &tempout[i*N1*2], N_p2, s_r3, s_r5, rad);
        }
      }
      else{
        for (i=0; i<num_of_lines; i++)
          ifft_DPxDP_bs (N1, M, rad, &output[i*N1*2], &tempout[i*N1*2], tw, Bn_w, workbuf_bn);
      }

      /* local transpose from tempout to output */

      if ((k < (lines - num_of_lines)) || tid < (numThread-1)){
         /* local transpose */
         DSPF_dp_mat_trans_cplx(tempout, num_of_lines/2, N1, output);
         DSPF_dp_mat_trans_cplx(&tempout[num_of_lines*N1], num_of_lines/2, N1, &output[num_of_lines*N1]);
       }
#else
      if (N1 == M) {
        if (s_r3 == 0 && s_r5 == 0) {
          DSPF_dp_ifftDPxDP (N1, input, tw, output, rad, 0, N1);
        }
        else {
          ifft_DPxDP_mixed_radix (N1, input, tw, output, N_p2, s_r3, s_r5, rad);
        }
      }
      else{
        ifft_DPxDP_bs (N1, M, rad, input, output, tw, Bn_w, workbuf_bn);
      }
    }
#endif

    /* export output data*/

#ifndef USEFASTEDMA

  if((k < (lines - num_of_lines)) || (tid < (numThread - 1)) ) {
    lib_emt_copy1D2D
    (
      edmall2->channel[ch_o],
        (void *)output,
        (void *)out_ptr_f,
        sizeof(double)*num_of_lines,
        outsample,
        linesize);
    lib_emt_wait(edmall2->channel[ch_o]);
    out_ptr_f += num_of_lines*numThread;

    lib_emt_copy1D2D
    (
      edmall2->channel[ch_o],
        (void *)(output+N1*num_of_lines),
        (void *)out_ptr_b,
        sizeof(double)*num_of_lines,
        outsample,
        linesize);
    lib_emt_wait(edmall2->channel[ch_o]);
    out_ptr_b -= num_of_lines*numThread;
  }
  else {

	  lib_emt_copy1D2D
        (
          edmall2->channel[ch_o],
            (void *)tempout,
            (void *)out_ptr_f,
            2*sizeof(double),
            outsample,
            linesize);
        lib_emt_wait(edmall2->channel[ch_o]);

        lib_emt_copy1D2D
        (
          edmall2->channel[ch_o],
            (void *)(tempout+N1*(num_of_lines-1)*2),
            (void *)(out_ptr_b+2),
            2*sizeof(double),
            outsample,
            linesize);
        lib_emt_wait(edmall2->channel[ch_o]);
    }

#else
    lib_emt_copy1D2D_Fast
    (
      edmall2->channel[ch_o],
        (void *)output,
        (void *)out_ptr);
#endif

    /* prepare input data */
#ifndef USEFASTEDMA
      lib_emt_copy1D1D(
        edmall2->channel[ch_i],
        (void *)in_ptr_f,
        (void *)input,
        insize);
      lib_emt_wait(edmall2->channel[ch_i]);

      lib_emt_copy1D1D(
        edmall2->channel[ch_i],
        (void *)in_ptr_b,
        (void *)(input+num_of_lines*(N1/2+1)),
        insize);
      lib_emt_wait(edmall2->channel[ch_i]);

#else
      lib_emt_copy1D1D_Fast(
        edmall2->channel[ch_i],
        (void *)in_ptr,
        (void *)input);
#endif
      in_ptr_f += (N1/2+1)*num_of_lines*numThread;
      in_ptr_b -= (N1/2+1)*num_of_lines*numThread;

      /* swap pingpong buffer pointers and XFER channel */
      dum2   = input;
      input  = in1;
      in1    = dum2;
      dum3   = output;
      output = out1;
      out1   = dum3;

      dum0   = ch_i;
      ch_i   = ch_i1;
      ch_i1  = dum0;
      dum1   = ch_o;
      ch_o   = ch_o1;
      ch_o1  = dum1;

      /* wait for EDMA */
      lib_emt_wait(edmall2->channel[ch_i]);
      lib_emt_wait(edmall2->channel[ch_o]);
    }

    /* wait for EDMA */
    lib_emt_wait(edmall2->channel[ch_i1]);
    lib_emt_wait(edmall2->channel[ch_o1]);

    return error;
} /* ifft_omp_dp_1d_c2c_batch_ecpy_transpose */

static int ifft_omp_dp_1d_c2r_ecpy_transpose (
    int   N1,                 /* FFT size                       */
    int   N2,                 /* number of lines                */
    int   N3,                 /* total number of lines          */
    int   M,                  /* Padded size of Anysize FFT     */
    int   rad,                /* radix                          */
    int   N_p2,               /* power of 2 size in mixed radix */
    int   s_r3,               /* number stages of radix 3       */
    int   s_r5,               /* number stages of radix 5       */
    double *in,               /* input pointer                  */
    double *out,              /* output pointer                 */
    double *in_w,             /* input work buffer pointer      */
    double *out_w,            /* output working buffer pointer  */
    double *out_t,            /* temp working buffer pointer    */
    double *tw,               /* twiddle pointer                */
    double *Bn_w,
    double *workbuf_bn,
    FFT_EDMA_Struct  *edma,  /* edma resource pointer          */
    int num_of_lines
)
{
  int   error = FFT_NOERR;
  int   i, j,ch_i,ch_o,ch_i1,ch_o1,dum0,dum1;
  int   insize    = N1*2*sizeof(double)*num_of_lines,
        outsample = N1,
        linesize  = N3*sizeof(double);
  FFT_EDMA_Struct  *edmall2;

  double *restrict in_ptr = in, *restrict out_ptr = out, *restrict tempout;
  double *restrict input, *restrict output, *restrict in1, *restrict out1, *dum2, *dum3;

  edmall2 = edma;
  /* prepare first FFT input buffer */
  lib_emt_copy1D1D(
    edmall2->channel[FFT_XFER_CHANNEL0],
    (void *)in_ptr,
    (void *)in_w,
    insize);
  in_ptr += 2*N1*num_of_lines;

  /* set pingpong buffer pointers */
  in1    = in_w;
  out1   = out_w;
  input  = &in_w[2*N1*num_of_lines];
  output = &out_w[N1*num_of_lines];
  tempout= out_t;

  /* prepare second FFT input buffer */
  lib_emt_copy1D1D(
    edmall2->channel[FFT_XFER_CHANNEL2],
    (void *)in_ptr,
    (void *)input,
    insize);
  in_ptr += 2*N1*num_of_lines;

  /* wait for first FFT input */
  lib_emt_wait(edmall2->channel[FFT_XFER_CHANNEL0]);

  /* doing the 1D DP FFT */
#if (IFFT_OMP_DP_2D_C2R_NUMOFLINEBUFS>1)
    if (N1 == M) {
      if (s_r3 == 0 && s_r5 == 0) {
        for (i=0; i<num_of_lines; i++)
          DSPF_dp_ifftDPxDP_c2r (N1, &in1[i*N1*2], tw, &tempout[i*N1], rad, 0, N1);
      }
      else {
        for (i=0; i<num_of_lines; i++)
          ifft_DPxDP_mixed_radix_c2r (N1, &in1[i*N1*2], tw, &tempout[i*N1], N_p2, s_r3, s_r5, rad);
      }
    }
    else{
      for (i=0; i<num_of_lines; i++)
        ifft_DPxDP_c2r_bs (N1, M, rad, &in1[i*N1*2], &tempout[i*N1], tw, Bn_w, workbuf_bn);
    }
    /* local transpose from tempout to out1 */
    DSPF_dp_mat_trans(tempout, num_of_lines, N1, out1);
#else
    if (N1 == M) {
      if (s_r3 == 0 && s_r5 == 0) {
    	  DSPF_dp_ifftDPxDP_c2r (N1, in1, tw, out1, rad, 0, N1);
      }
      else {
        ifft_DPxDP_mixed_radix_c2r (N1, in1, tw, out1, N_p2, s_r3, s_r5, rad);
      }
    }
    else{
      ifft_DPxDP_c2r_bs (N1, M, rad, in1, out1, tw, Bn_w, workbuf_bn);
    }
#endif

  /* export output data of first FFT*/
  lib_emt_copy1D2D(
    edmall2->channel[FFT_XFER_CHANNEL1],
    (void *)out1,
    (void *)out_ptr,
    sizeof(double)*num_of_lines,
    outsample,
    linesize);
  out_ptr += num_of_lines;

#ifdef USEFASTEDMA
  /* fill the PaRAM for CHANNEL 3 without starting it */
  lib_emt_copy1D2D_Fill(
    edmall2->channel[FFT_XFER_CHANNEL3],
    (void *)out1,
    (void *)out_ptr,
    sizeof(double)*num_of_lines,
    outsample,
    linesize);
#endif

  /* prepare third FFT input buffer */
#ifndef USEFASTEDMA
  lib_emt_copy1D1D(
    edmall2->channel[FFT_XFER_CHANNEL0],
    (void *)in_ptr,
    (void *)in_w,
    insize);
#else
  lib_emt_copy1D1D_Fast(
    edmall2->channel[FFT_XFER_CHANNEL0],
    (void *)in_ptr,
    (void *)in_w);
#endif
  in_ptr += 2*N1*num_of_lines;

  /* set channel numbers */
  ch_i  = FFT_XFER_CHANNEL2;
  ch_o  = FFT_XFER_CHANNEL3;
  ch_i1 = FFT_XFER_CHANNEL0;
  ch_o1 = FFT_XFER_CHANNEL1;

  /* wait for EDMA */
  lib_emt_wait(edmall2->channel[ch_i]);

  _nassert (N2 >= 4);
  for (j = 1; j < N2/num_of_lines; j++)
  {
#if (IFFT_OMP_DP_2D_C2R_NUMOFLINEBUFS>1)
      if (N1 == M) {
        if (s_r3 == 0 && s_r5 == 0) {
          for (i=0; i<num_of_lines; i++)
            DSPF_dp_ifftDPxDP_c2r (N1, &input[i*N1*2], tw, &tempout[i*N1], rad, 0, N1);
        }
        else {
          for (i=0; i<num_of_lines; i++)
            ifft_DPxDP_mixed_radix_c2r (N1, &input[i*N1*2], tw, &tempout[i*N1], N_p2, s_r3, s_r5, rad);
        }
      }
      else{
        for (i=0; i<num_of_lines; i++)
          ifft_DPxDP_c2r_bs (N1, M, rad, &input[i*N1*2], &tempout[i*N1], tw, Bn_w, workbuf_bn);
      }

      /* local transpose for output */
      DSPF_dp_mat_trans(tempout, num_of_lines, N1, output);
#else
      if (N1 == M) {
        if (s_r3 == 0 && s_r5 == 0) {
          DSPF_dp_ifftDPxDP (N1, input, tw, output, rad, 0, N1);
        }
        else {
          ifft_DPxDP_mixed_radix (N1, input, tw, output, N_p2, s_r3, s_r5, rad);
        }
      }
      else{
        ifft_DPxDP_bs (N1, M, rad, input, output, tw, Bn_w, workbuf_bn);
      }
    }
#endif

    /* export output data*/
#ifndef USEFASTEDMA
    lib_emt_copy1D2D
    (
      edmall2->channel[ch_o],
        (void *)output,
        (void *)out_ptr,
        sizeof(double)*num_of_lines,
        outsample,
        linesize);
#else
    lib_emt_copy1D2D_Fast
    (
      edmall2->channel[ch_o],
        (void *)output,
        (void *)out_ptr);
#endif
    out_ptr += num_of_lines;

    /* prepare input data */
#ifndef USEFASTEDMA
      lib_emt_copy1D1D(
        edmall2->channel[ch_i],
        (void *)in_ptr,
        (void *)input,
        insize);
#else
      lib_emt_copy1D1D_Fast(
        edmall2->channel[ch_i],
        (void *)in_ptr,
        (void *)input);
#endif
      in_ptr += 2*N1*num_of_lines;

      /* swap pingpong buffer pointers and XFER channel */
      dum2   = input;
      input  = in1;
      in1    = dum2;
      dum3   = output;
      output = out1;
      out1   = dum3;

      dum0   = ch_i;
      ch_i   = ch_i1;
      ch_i1  = dum0;
      dum1   = ch_o;
      ch_o   = ch_o1;
      ch_o1  = dum1;

      /* wait for EDMA */
      lib_emt_wait(edmall2->channel[ch_i]);
      lib_emt_wait(edmall2->channel[ch_o]);
    }

    /* wait for EDMA */
    lib_emt_wait(edmall2->channel[ch_i1]);
    lib_emt_wait(edmall2->channel[ch_o1]);

    return error;
} /* ifft_omp_dp_1d_c2r_batch_ecpy_transpose */

static int radLocal, n_p2Local, s_r3Local, s_r5Local;
static int n1Local, n2Local, m1Local, m2Local, linesLocal;
static double *dataLocal, *outLocal, *workbufLocal, *BnLocal, *twLocal, *data_wLocal, 
              *workbuf_wLocal, *Bn_wLocal, *workbuf_tLocal, *workbuf_bnLocal, 
              *tw_wLocal, *Bn_aLocal, *tw_aLocal;
static FFT_EDMA_Struct *edmaLocal;
#ifndef FFT_MEM_MODEL_SM
#pragma DATA_SECTION (radLocal, ".mem_msm")
#pragma DATA_SECTION (n_p2Local, ".mem_msm")
#pragma DATA_SECTION (s_r3Local, ".mem_msm")
#pragma DATA_SECTION (s_r5Local, ".mem_msm")
#pragma DATA_SECTION (n1Local, ".mem_msm")
#pragma DATA_SECTION (n2Local, ".mem_msm")
#pragma DATA_SECTION (m1Local, ".mem_msm")
#pragma DATA_SECTION (m2Local, ".mem_msm")
#pragma DATA_SECTION (linesLocal, ".mem_msm")
#pragma DATA_SECTION (dataLocal, ".mem_msm")
#pragma DATA_SECTION (outLocal, ".mem_msm")
#pragma DATA_SECTION (workbufLocal, ".mem_msm")
#pragma DATA_SECTION (twLocal, ".mem_msm")
#pragma DATA_SECTION (data_wLocal, ".mem_msm")
#pragma DATA_SECTION (workbuf_wLocal, ".mem_msm")
#pragma DATA_SECTION (workbuf_tLocal, ".mem_msm")
#pragma DATA_SECTION (tw_wLocal, ".mem_msm")
#pragma DATA_SECTION (Bn_aLocal, ".mem_msm")
#pragma DATA_SECTION (tw_aLocal, ".mem_msm")
#pragma DATA_SECTION (edmaLocal, ".mem_msm")
#endif

int ifft_omp_dp_2d_c2r_ecpy (
  /* int     N1;           FFT size         */
  /* int     N2;           FFT size         */
  /* int     rad1          radix for N1     */
  /* int     rad2          radix for N2     */
  /* double *data;         data pointer     */
  /* double *workbuf;      work buffer pointer   */
  /* double *data_w;       data internal pointer    */
  /* double *workbuf_w;    working buffer internal pointer   */
  /* double *tw;           twiddle pointer  */
  fft_param_u u,
  void *edmaState,
  int   actualCoreNum
)
{

  double *in1, *input, *out1, *output;  /* pointers to local memory*/
  double *in_ptr, *out_ptr;          /* pointers to external memory*/

  int numThread = omp_get_max_threads();
  int tid, did, numLine, i;

  int   error = FFT_NOERR;
  int   twsize, Bnsize, tw_offset, Bn_offset;

  /* max of the N, Bnsize and twsize for internal memory usage */
  int   twsizeMax = (u.dp_2d_c2r_e.para1.twsize > u.dp_2d_c2r_e.para2.twsize) ? u.dp_2d_c2r_e.para1.twsize : u.dp_2d_c2r_e.para2.twsize;
  int   BnsizeMax = (u.dp_2d_c2r_e.para1.Bnsize > u.dp_2d_c2r_e.para2.Bnsize) ? u.dp_2d_c2r_e.para1.Bnsize : u.dp_2d_c2r_e.para2.Bnsize;
  int   N = (u.dp_2d_c2r_e.N1 > u.dp_2d_c2r_e.N2) ? u.dp_2d_c2r_e.N1 : u.dp_2d_c2r_e.N2;  /* max of the 2 dimension */
  lib_memdscr_t **fft_mem_handle = fftGetMemHandle();

  n1Local          = u.dp_2d_c2r_e.N1;          /* signal size                    */
  n2Local          = u.dp_2d_c2r_e.N2;          /* signal size                    */
  m1Local          = u.dp_2d_c2r_e.para1.M;      /* first dimension FFT size M1           */
  m2Local          = u.dp_2d_c2r_e.para2.M;      /* second dimension FFT size M2          */
  radLocal         = u.dp_2d_c2r_e.para1.rad;    /* radix for N1                          */

  twsize            = u.dp_2d_c2r_e.para1.twsize; /* twiddle size in double words          */
  n_p2Local         = u.dp_2d_c2r_e.para1.N_p2;   /* power of 2 size in mixed radix        */
  s_r3Local         = u.dp_2d_c2r_e.para1.s_r3;   /* number stages of radix 3              */
  s_r5Local         = u.dp_2d_c2r_e.para1.s_r5;   /* number stages of radix 5              */
  Bnsize            = u.dp_2d_c2r_e.para1.Bnsize;

  /* input parameters */
  dataLocal       = u.dp_2d_c2r_e.data;        /* data pointer                   */
  outLocal        = u.dp_2d_c2r_e.workbuf;     /* output buffer                  */
  workbufLocal    = u.dp_2d_c2r_e.tw;          /* work buffer pointer            */
  twLocal         = u.dp_2d_c2r_e.tw+2*n1Local*n2Local;          /* twiddle         buffer pointer */
  BnLocal         = u.dp_2d_c2r_e.Bn;          /* Bn buffer pointer              */

    data_wLocal    = (double*)lib_smem_falloc (fft_mem_handle, 4*N*IFFT_OMP_DP_2D_C2R_NUMOFLINEBUFS*sizeof(double), 8);
    workbuf_wLocal = (double*)lib_smem_falloc (fft_mem_handle, 4*N*IFFT_OMP_DP_2D_C2R_NUMOFLINEBUFS*sizeof(double), 8);
    workbuf_tLocal = (double*)lib_smem_falloc (fft_mem_handle, 2*N*IFFT_OMP_DP_2D_C2R_NUMOFLINEBUFS*sizeof(double), 8);
#ifdef FFT_MEM_MODEL_LG
    tw_wLocal      = (double*)lib_smem_falloc (fft_mem_handle, twsizeMax*sizeof(double), 8);
#endif

#ifdef FFT_MEM_MODEL_LG
    if(data_wLocal==NULL || workbuf_wLocal==NULL || workbuf_tLocal==NULL || tw_wLocal==NULL) {
#else
    tw_wLocal = NULL;
    if(data_wLocal==NULL || workbuf_wLocal==NULL || workbuf_tLocal==NULL) {
#endif
        printf("Memory allocation error!\n");
        return FFT_ERROR;
    }

  if ((u.dp_2d_c2r_e.para1.use_bs) || (u.dp_2d_c2r_e.para2.use_bs)){
    Bn_wLocal       = tw_wLocal      + twsizeMax; /* Bn working buffer pointer      */
    workbuf_bnLocal = Bn_wLocal      + BnsizeMax; /* workbuffer pointer for BS      */
  }else {
	Bn_wLocal = NULL;
	workbuf_bnLocal = NULL;
  }
  
    
  if ((actualCoreNum==1)||
      (actualCoreNum==2)||
      (actualCoreNum==4)||
      (actualCoreNum==8))
    omp_set_num_threads (actualCoreNum);
  else
    omp_set_num_threads (OMP_MAX_NUM_CORES);
  numThread = omp_get_max_threads();
  linesLocal = n2Local/numThread;
  edmaLocal = (FFT_EDMA_Struct *)edmaState;

  /* first, we need to do the 1st and the N2/2 the FFT, since it's different from the reset */

  if (tw_wLocal != NULL) {
    /* prepare twiddle buffer */
    lib_emt_copy1D1D(
      ((edmaLocal)->channel[FFT_XFER_CHANNEL3]),
      (void *)twLocal,
      (void *)tw_wLocal,
      twsize*sizeof(double));
    tw_aLocal = tw_wLocal;
  }
  else {
    tw_aLocal = twLocal;
  }

  if (Bn_wLocal != NULL) {
     /* prepare Bn buffer */
     lib_emt_copy1D1D(
       ((edmaLocal)->channel[FFT_XFER_CHANNEL2]),
       (void *)BnLocal,
       (void *)Bn_wLocal,
       Bnsize*sizeof(double));
     Bn_aLocal = Bn_wLocal;
   } else {
     Bn_aLocal = BnLocal;
   }

  in_ptr  = dataLocal;
  out_ptr = workbufLocal;
  in1     = &data_wLocal[0];
  input   = &data_wLocal[n1Local*2];
  out1    = &workbuf_wLocal[0];
  output  = &workbuf_wLocal[n1Local*2];

/* prepare 1st FFT input buffer */
  lib_emt_copy1D1D(
	edmaLocal->channel[FFT_XFER_CHANNEL0],
    (void *)in_ptr,
    (void *)in1,
    2*(n1Local/2+1)*sizeof(double));

  /* prepare N2/2 FFT input buffer */
  lib_emt_copy1D1D(
    edmaLocal->channel[FFT_XFER_CHANNEL1],
    (void *)(in_ptr+(n1Local/2+1)*2*(n2Local/2)),
    (void *)input,
    2*(n1Local/2+1)*sizeof(double));

   if (tw_wLocal != NULL)
   	lib_emt_wait((edmaLocal)->channel[FFT_XFER_CHANNEL3]);
   if (Bn_wLocal != NULL)
   	lib_emt_wait((edmaLocal)->channel[FFT_XFER_CHANNEL2]);

  /* wait for 1st FFT inputs */
  lib_emt_wait(edmaLocal->channel[FFT_XFER_CHANNEL0]);

  /* construct the full spectrum */
  for(i = 1; i < n1Local/2; i++){
    in1[(n1Local-i)*2]   =  in1[2*i];
    in1[(n1Local-i)*2+1] = -in1[2*i+1];
  }

  if (n1Local == m1Local) {
    if (s_r3Local == 0 && s_r5Local == 0)
        DSPF_dp_ifftDPxDP (n1Local, &in1[0], tw_aLocal, &out1[0], radLocal, 0, n1Local);
    else
        ifft_DPxDP_mixed_radix (n1Local, &in1[0], tw_aLocal, &out1[0], n_p2Local, s_r3Local, s_r5Local, radLocal);
  }
  else{
      ifft_DPxDP_bs (n1Local, m1Local, radLocal, &in1[0], &out1[0], tw_aLocal, Bn_aLocal, workbuf_bnLocal);
  }

  /* transfter the result out */

  lib_emt_copy1D2D(
  edmaLocal->channel[FFT_XFER_CHANNEL2],
  (void *)out1,
  (void *)out_ptr,
  2*sizeof(double),
  n1Local,
  n2Local*2*sizeof(double));

  /* wait for N1/2 FFT inputs */
  lib_emt_wait(edmaLocal->channel[FFT_XFER_CHANNEL1]);

  /* construct the full spectrum */
  for(i = 1; i < n1Local/2; i++){
    input[(n1Local-i)*2]   =  input[2*i];
    input[(n1Local-i)*2+1] = -input[2*i+1];
  }

  if (n1Local == m1Local) {
    if (s_r3Local == 0 && s_r5Local == 0)
        DSPF_dp_ifftDPxDP (n1Local, &input[0], tw_aLocal, &output[0], radLocal, 0, n1Local);
    else
        ifft_DPxDP_mixed_radix (n1Local, &input[0], tw_aLocal, &output[0], n_p2Local, s_r3Local, s_r5Local, radLocal);
  }
  else{
      ifft_DPxDP_bs (n1Local, m1Local, radLocal, &input[0], &output[0], tw_aLocal, Bn_aLocal, workbuf_bnLocal);
  }
  /* transfter the result out */

  lib_emt_copy1D2D(
  edmaLocal->channel[FFT_XFER_CHANNEL3],
  (void *)output,
  (void *)(out_ptr+n2Local),
  2*sizeof(double),
  n1Local,
  n2Local*2*sizeof(double));

  /* wait for outputs */
  lib_emt_wait(edmaLocal->channel[FFT_XFER_CHANNEL2]);
  lib_emt_wait(edmaLocal->channel[FFT_XFER_CHANNEL3]);

#pragma omp parallel private(did, tid, numLine, tw_aLocal, Bn_aLocal) shared(radLocal, dataLocal, workbufLocal, BnLocal, \
  twLocal, data_wLocal, workbuf_wLocal, workbuf_tLocal, Bn_wLocal, tw_wLocal, workbuf_bnLocal, n1Local, \
  n2Local, m1Local, linesLocal, n_p2Local, s_r3Local, s_r5Local, edmaLocal, twsize, Bnsize, tw_offset, Bn_offset)
  {
    tid = omp_get_thread_num();     // Obtain thread number
    did = DNUM;
  
    if (tw_wLocal != NULL) {
      /* prepare twiddle buffer */
      lib_emt_copy1D1D(
        ((edmaLocal+did)->channel[FFT_XFER_CHANNEL3]),
        (void *)twLocal,
        (void *)tw_wLocal,
        twsize*sizeof(double));
      tw_aLocal = tw_wLocal;
    }
    else {
      tw_aLocal = twLocal;
    }

    if (Bn_wLocal != NULL) {
      /* prepare Bn buffer */
      lib_emt_copy1D1D(
        ((edmaLocal+did)->channel[FFT_XFER_CHANNEL2]),
        (void *)BnLocal,
        (void *)Bn_wLocal,
        Bnsize*sizeof(double));
      Bn_aLocal = Bn_wLocal;
    } else {
      Bn_aLocal = BnLocal;
    }
    
    if (tw_wLocal != NULL)
    	lib_emt_wait((edmaLocal+did)->channel[FFT_XFER_CHANNEL3]);
    if (Bn_wLocal != NULL)
    	lib_emt_wait((edmaLocal+did)->channel[FFT_XFER_CHANNEL2]);

    numLine = IFFT_OMP_DP_2D_C2R_NUMOFLINEBUFS;
    /* 1st dimension fft */
    error = ifft_omp_dp_1d_c2c_ecpy_transpose (
      n1Local,
      n2Local,
      m1Local,
      radLocal,
      n_p2Local,
      s_r3Local,
      s_r5Local,
      &dataLocal[0],
      &workbufLocal[0],
      data_wLocal,
      workbuf_wLocal,
      workbuf_tLocal,
      tw_aLocal,
      Bn_aLocal,
      workbuf_bnLocal,
      edmaLocal+did,
      numLine);
  }

    /* prepare for the 2nd dimension */
    n_p2Local   = u.dp_2d_c2r_e.para2.N_p2;    /* power of 2 size in mixed radix        */
    s_r3Local   = u.dp_2d_c2r_e.para2.s_r3;    /* number stages of radix 3              */
    s_r5Local   = u.dp_2d_c2r_e.para2.s_r5;    /* number stages of radix 5              */
    radLocal    = u.dp_2d_c2r_e.para2.rad;
    twsize      = u.dp_2d_c2r_e.para2.twsize; /* twiddle size in double words          */
    Bnsize      = u.dp_2d_c2r_e.para2.Bnsize;
    tw_offset   = u.dp_2d_c2r_e.para1.twsize;
    Bn_offset   = u.dp_2d_c2r_e.para1.Bnsize;
  
    linesLocal = n1Local/numThread;
  
#pragma omp parallel private(did, tid, numLine, tw_aLocal, Bn_aLocal) shared(radLocal, dataLocal, outLocal, workbufLocal, BnLocal, \
  twLocal, data_wLocal, workbuf_wLocal, workbuf_tLocal, Bn_wLocal, tw_wLocal, workbuf_bnLocal, n1Local, \
  n2Local, m2Local, linesLocal, n_p2Local, s_r3Local, s_r5Local, edmaLocal, twsize, Bnsize, tw_offset, Bn_offset)
  {
    tid = omp_get_thread_num();     // Obtain thread number
    did = DNUM;
    
    if (tw_wLocal != NULL) {
      /* prepare twiddle buffer */
      lib_emt_copy1D1D(
        ((edmaLocal+did)->channel[FFT_XFER_CHANNEL3]),
        (void *)(twLocal + tw_offset),
        (void *)tw_wLocal,
        twsize*sizeof(double));
      tw_aLocal = tw_wLocal;
    }
    else {
      tw_aLocal = twLocal + tw_offset;
    }

    if (Bn_wLocal != NULL) {
      /* prepare Bn buffer */
      lib_emt_copy1D1D(
        ((edmaLocal+did)->channel[FFT_XFER_CHANNEL2]),
        (void *)(BnLocal + Bn_offset),
        (void *)Bn_wLocal,
        Bnsize*sizeof(double));
      Bn_aLocal = Bn_wLocal;
    } else {
      Bn_aLocal = BnLocal;
    }

    if (tw_wLocal != NULL)
    	lib_emt_wait((edmaLocal+did)->channel[FFT_XFER_CHANNEL3]);
    if (Bn_wLocal != NULL)
    	lib_emt_wait((edmaLocal+did)->channel[FFT_XFER_CHANNEL2]);

    numLine = IFFT_OMP_DP_2D_C2R_NUMOFLINEBUFS;

    /* 2nd dimension fft */
    error = ifft_omp_dp_1d_c2r_ecpy_transpose (
      n2Local,
      linesLocal,
      n1Local,
      m2Local,
      radLocal,
      n_p2Local, 
      s_r3Local, 
      s_r5Local,
      &workbufLocal[tid*(n2Local*2*linesLocal)],
      &outLocal[tid*linesLocal],
      workbuf_wLocal,
      data_wLocal,
      workbuf_tLocal,
      tw_aLocal,
      Bn_aLocal,
      workbuf_bnLocal,
      edmaLocal+did,
      numLine);
    }
  omp_set_num_threads (OMP_MAX_NUM_CORES);

    return error;
} /* ifft_omp_dp_2d_c2r_ecpy */

/* ======================================================================== */
/*  End of file:  ifft_omp_dp_2d_c2r.c                                      */
/* ------------------------------------------------------------------------ */
/*            Copyright (c) 2013 Texas Instruments, Incorporated.           */
/*                           All Rights Reserved.                           */
/* ======================================================================== */
