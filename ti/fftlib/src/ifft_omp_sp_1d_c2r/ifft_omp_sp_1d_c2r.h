/* ======================================================================= */
/*  TEXAS INSTRUMENTS, INC.                                                */
/*                                                                         */
/*  FFTLIB  FFT Library                                                    */
/*                                                                         */
/* Copyright (C) 2013 Texas Instruments Incorporated - http://www.ti.com/  */ 
/*                                                                         */
/*                                                                         */
/*  Redistribution and use in source and binary forms, with or without     */
/*  modification, are permitted provided that the following conditions     */
/*  are met:                                                               */
/*                                                                         */
/*    Redistributions of source code must retain the above copyright       */
/*    notice, this list of conditions and the following disclaimer.        */
/*                                                                         */
/*    Redistributions in binary form must reproduce the above copyright    */
/*    notice, this list of conditions and the following disclaimer in the  */
/*    documentation and/or other materials provided with the               */
/*    distribution.                                                        */
/*                                                                         */
/*    Neither the name of Texas Instruments Incorporated nor the names of  */
/*    its contributors may be used to endorse or promote products derived  */
/*    from this software without specific prior written permission.        */
/*                                                                         */
/*  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS    */
/*  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT      */
/*  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR  */
/*  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT   */
/*  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,  */
/*  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT       */
/*  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,  */
/*  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY  */
/*  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT    */
/*  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE  */
/*  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.   */
/*                                                                         */
/* ======================================================================= */

#include <ti/fftlib/src/common/fft_common.h>

#ifndef IFFT_OMP_SP_1D_C2R_H_
#define IFFT_OMP_SP_1D_C2R_H_ 1

/** @ingroup 1D */
/* @{ */

/** @defgroup IFFT_OMP_SP_1D_C2R */
/** @ingroup IFFT_OMP_SP_1D_C2R */
/* @{ */
 
 /* }@ */ /* ingroup */
 /* }@ */ /* ingroup */

/**
 *     @param IFFT_OMP_SP_1D_C2R_4STEP_MIN_SIZE = minimum IFFT size
 */

#define IFFT_OMP_SP_1D_C2R_4STEP_MIN_SIZE  (   4*1024)

/**
 *     @param IFFT_OMP_SP_1D_C2R_4STEP_MAX_SIZE = maximum IFFT size
 */

#ifdef FFT_MEM_MODEL_LG
#define IFFT_OMP_SP_1D_C2R_4STEP_MAX_SIZE  (1024*1024)
#else
#define IFFT_OMP_SP_1D_C2R_4STEP_MAX_SIZE  (512*512)
#endif

/**
 *     @param IFFT_OMP_SP_1D_C2R_NUMOFLINEBUFS = number of IFFT lines to process each iteration
 */
 
#ifdef FFT_MEM_MODEL_SM
#define IFFT_OMP_SP_1D_C2R_NUMOFLINEBUFS 4
#else
#define IFFT_OMP_SP_1D_C2R_NUMOFLINEBUFS 8
#endif

 /**
 *     This program initializes one 1-dimensional single-precision complex-to-real IFFT.
 *  
 *     @param N = IFFT size      
 *     @param mode = only FFT_ECPY is supported
 *     @param fxns = call out functions used to allocated/release memory and EDMA resource
 *     @param p = return plan structure
 *     @param in = input pointer    
 *     @param out = output pointer   
 *     @param tw = twiddle pointer  
 * 
 * @par Algorithm:
 *      Algorithm can run in radix 2, 3, 4 & 5 based upon value of N. <br><b>Note:</b>Parameters 
 *      <tt>para1</tt> & <tt>para2</tt> are determined at runtime and will depend upon type of FFT
 *      desired, radix 3 & 5, or radix 2 & 4.
 *
 * @par Assumptions:
 *      N needs to be a multiple of 128 and positive.<br>
 *      All input, output and working buffers are double word aligned.<br>
 *      No input, working or buffers overlap.<br>
 *
 * @par Size Limitations:
 *      Current limitations are:
 *      1024*1024 for large memory model<br>
 *      512*512 for medium memory model <br>
 *      512*512 for small memory model  <br>
 *
 * @par Implementation notes:
 * @b Endian Support: The code supports little endian modes.  <BR> 
 * @b Interruptibility: The code is interrupt-tolerant but not interruptible. <BR>
 *  
 */

/* IFFT plan 1D SP API */
void ifft_omp_sp_plan_1d_c2r (
    int   N,        /* IFFT size */
    int   mode,     /* FFT_DIRECT or FFT_ECPY */
    fft_callout_t fxns,
    fft_plan_t *p,
    float *in,
    float *out,
    float *tw
);
 
 /**
 *
 *     This program uses info saved from plan function to execute 1-dimensional single-precision complex-to-real IFFT.
 *  
 *     @param u = FFT parameter from plan structure      
 *     @param edmastate = EDMA channel allocation
 *     @param actualCoreNum = run-time configured number of cores
 *     @param return value = error status, FFT_NOERR if success
 *
 */
 
/* IFFT execute 1D SP 4-step ecpy mode API */
int ifft_omp_sp_1d_c2r_ecpy (
    fft_param_u u,
    void *edmaState,
    int   actualCoreNum
);
 
#endif /* IFFT_OMP_SP_1D_C2R_H_ */

/* ======================================================================== */
/*  End of file:  ifft_omp_sp_1d_c2r.h                                      */
/* ------------------------------------------------------------------------ */
/*            Copyright (c) 2013 Texas Instruments, Incorporated.           */
/*                           All Rights Reserved.                           */
/* ======================================================================== */


