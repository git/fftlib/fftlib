/* ======================================================================= */
/*  TEXAS INSTRUMENTS, INC.                                                */
/*                                                                         */
/*  FFTLIB  FFT Library                                                    */
/*                                                                         */
/* Copyright (C) 2013 Texas Instruments Incorporated - http://www.ti.com/  */ 
/*                                                                         */
/*                                                                         */
/*  Redistribution and use in source and binary forms, with or without     */
/*  modification, are permitted provided that the following conditions     */
/*  are met:                                                               */
/*                                                                         */
/*    Redistributions of source code must retain the above copyright       */
/*    notice, this list of conditions and the following disclaimer.        */
/*                                                                         */
/*    Redistributions in binary form must reproduce the above copyright    */
/*    notice, this list of conditions and the following disclaimer in the  */
/*    documentation and/or other materials provided with the               */
/*    distribution.                                                        */
/*                                                                         */
/*    Neither the name of Texas Instruments Incorporated nor the names of  */
/*    its contributors may be used to endorse or promote products derived  */
/*    from this software without specific prior written permission.        */
/*                                                                         */
/*  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS    */
/*  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT      */
/*  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR  */
/*  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT   */
/*  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,  */
/*  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT       */
/*  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,  */
/*  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY  */
/*  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT    */
/*  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE  */
/*  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.   */
/*                                                                         */
/* ======================================================================= */

#include <xdc/std.h>
#include "c6x.h"
#include "stdlib.h"
#include <ti/fftlib/src/common/omp/omp_config.h>
#include "ifft_omp_sp_1d_c2r.h"
#include <ti/fftlib/src/common/ifft/ifft_sp_mixed_bs.h>
#include <ti/fftlib/src/common/ifft/ifft_sp_cmplx_notwid_br.h>
#include <ti/fftlib/src/common/fft_edma.h>
#include <ti/runtime/openmp/omp.h>
#include <ti/dsplib/src/DSPF_sp_mat_trans/DSPF_sp_mat_trans.h>
#include <ti/dsplib/src/DSPF_sp_mat_trans_cplx/DSPF_sp_mat_trans_cplx.h>
#include <ti/libarch/libarch.h>

static int    N1;       /* stage 1 size                     */
static int    N2;       /* stage 2 size                     */
static int   N_p2;      /* power of 2 size in mixed radix   */
static int   s_r3;      /* number stages of radix 3         */
static int   s_r5;      /* number stages of radix 5         */
static float *data;     /* data pointer                     */
static float *work;     /* working buffer pointer           */
static float *out;      /* output buffer pointer            */
static float *data_w;   /* data internal pointer            */
static float *work_w;   /* working buffer internal pointer  */
static float *work_t;   /* temp buffer internal pointer     */
static float *tw_e;     /* twiddle buffer pointer           */
static float *tw_w;     /* twiddle working buffer pointer   */
static int    lines;
static FFT_EDMA_Struct *edma;
#ifndef FFT_MEM_MODEL_SM
#pragma DATA_SECTION (edma,   ".mem_msm")
#pragma DATA_SECTION (lines,  ".mem_msm")
#pragma DATA_SECTION (N1,     ".mem_msm")
#pragma DATA_SECTION (N2,     ".mem_msm")
#pragma DATA_SECTION (s_r3,   ".mem_msm")
#pragma DATA_SECTION (s_r5,   ".mem_msm")
#pragma DATA_SECTION (N_p2,   ".mem_msm")
#pragma DATA_SECTION (data,   ".mem_msm")
#pragma DATA_SECTION (out,    ".mem_msm")
#pragma DATA_SECTION (work,   ".mem_msm")
#pragma DATA_SECTION (data_w, ".mem_msm")
#pragma DATA_SECTION (work_w, ".mem_msm")
#pragma DATA_SECTION (tw_e,   ".mem_msm")
#pragma DATA_SECTION (tw_w,   ".mem_msm")
#endif

int ifft_omp_sp_1d_c2r_ecpy (
    /* int   N;        IFFT size                         */
    /* int   n1;       stage1 IFFT size                  */
    /* int   n2;       stage2 IFFT size                  */
    /* float *data;    data pointer                     */
    /* float *work;    working buffer pointer           */
    /* float *data_w;  data internal pointer            */
    /* float *work_w;  working buffer internal pointer  */
    /* float *tw;      twiddle pointer                  */
    /* float *tw_w;    twiddle internal pointer         */
    fft_param_u u,
    void *edmaState,
    int   actualCoreNum
)
{
    int numThread = omp_get_max_threads();
    int tid, did, num_of_lines, lines_to_process;

    int   error = FFT_NOERR;
    int   twsize, tw_offset, N;
    int   i,j,k,ch_i,ch_o,ch_i1,ch_o1,ch_o2,ch_o3,dum0,dum1;
    int   insize, outsample, linesize;

    float *restrict in_ptr_f, *restrict out_ptr_f, *restrict tempout;
    float *restrict in_ptr_b, *restrict out_ptr_b;
    float *restrict in_ptr, *restrict out_ptr;
    float *restrict input, *restrict output, *restrict in1, *restrict out1, *dum2, *dum3;
    float *restrict tw, *restrict tw1;
    __float2_t co_si;
    __float2_t twid, temp;
    lib_memdscr_t **fft_mem_handle = fftGetMemHandle();

    /* input parameters */
    N1      = u.sp_1d_c2r_e.n1;         /* stage 1 size                     */
    N2      = u.sp_1d_c2r_e.n2;         /* stage 2 size                     */
    N       = u.sp_1d_c2r_e.N;
    N_p2    = u.sp_1d_c2r_e.para1.N_p2; /* power of 2 size in mixed radix   */
    s_r3    = u.sp_1d_c2r_e.para1.s_r3; /* number stages of radix 3         */
    s_r5    = u.sp_1d_c2r_e.para1.s_r5; /* number stages of radix 5         */    
    data    = u.sp_1d_c2r_e.data;       /* data pointer                     */
    out     = u.sp_1d_c2r_e.work;       /* working buffer pointer           */
    work    = u.sp_1d_c2r_e.tw;
    tw_e    = u.sp_1d_c2r_e.tw+2*N;         /* twiddle pointer                  */
    tw_offset = u.sp_1d_c2r_e.para1.twsize; /* Offset needed for twiddle ptr    */    

    edma    = (FFT_EDMA_Struct *)edmaState;

    data_w  = (float*)lib_smem_falloc (fft_mem_handle, 4*N1*IFFT_OMP_SP_1D_C2R_NUMOFLINEBUFS*sizeof(float), 8);
    work_w  = (float*)lib_smem_falloc (fft_mem_handle, 4*N1*IFFT_OMP_SP_1D_C2R_NUMOFLINEBUFS*sizeof(float), 8);
    work_t  = (float*)lib_smem_falloc (fft_mem_handle, 2*N1*IFFT_OMP_SP_1D_C2R_NUMOFLINEBUFS*sizeof(float), 8);
    twsize  = (u.sp_1d_c2r_e.para1.twsize+2*N2 > u.sp_1d_c2r_e.para2.twsize) ? u.sp_1d_c2r_e.para1.twsize+2*N2 : u.sp_1d_c2r_e.para2.twsize;
#ifdef FFT_MEM_MODEL_LG
    tw_w    = (float*)lib_smem_falloc (fft_mem_handle, twsize*sizeof(float), 8);
#endif

#ifdef FFT_MEM_MODEL_LG
    if(data_w==NULL || work_w==NULL || work_t==NULL || tw_w==NULL) {
#else
    tw_w = NULL;
    if(data_w==NULL || work_w==NULL || work_t==NULL) {
#endif
        printf("Memory allocation error!\n");
        return FFT_ERROR;
    }

    if ((actualCoreNum==1)||
        (actualCoreNum==2)||
        (actualCoreNum==4)||
        (actualCoreNum==8))
      omp_set_num_threads (actualCoreNum);
    else
      omp_set_num_threads (OMP_MAX_NUM_CORES);

    numThread = omp_get_max_threads();

    num_of_lines = IFFT_OMP_SP_1D_C2R_NUMOFLINEBUFS;
    lines   = (N2/numThread/num_of_lines)*num_of_lines;

    if ((lines*numThread < N2) && (lines > 0))
      lines += num_of_lines;

    if (lines*numThread > N2)
      lines -= num_of_lines;
    twsize    = (u.sp_1d_c2r_e.para1.twsize+2*N2)*sizeof(float);
    insize    = N1*2*sizeof(float);
    outsample = N1;
    linesize  = N2*2*sizeof(float);

    /* Bring in the 1st and N2/2+1 column since they're special */
    in_ptr_f  = &data[0];         /* pointer starts at the 1st column the N1xN2 matrix */
    out_ptr_f = &work[0];
    in_ptr_b  = &data[N2];        /* pointer starts at the N2/2 column of the N1xN2 matrix */
    out_ptr_b = &work[N2];

    in1    = data_w;
    out1   = work_w;
    input  = &data_w[N1*num_of_lines/2];
    output = &work_w[N1*num_of_lines/2];


    tw = tw_e;
    if (tw_w != NULL) {
      /* prepare twiddle buffer */
      lib_emt_copy1D1D(
        ((edma)->channel[FFT_XFER_CHANNEL3]),
        (void *)tw_e,
        (void *)tw_w,
        twsize);
      tw = tw_w;
    }
    tid = omp_get_thread_num();     // Obtain thread number
    tw1 = tw + tw_offset + 2*tid*lines;

    /* in1 has half of the first column */
    lib_emt_copy2D1D(
      ((edma)->channel[FFT_XFER_CHANNEL0]),
      (void *)in_ptr_f,
      (void *)in1,
      2*sizeof(float),
      outsample,
      linesize);

    /* input has half of the N2/2+1 column */
    lib_emt_copy2D1D(
      ((edma)->channel[FFT_XFER_CHANNEL1]),
      (void *)in_ptr_b,
      (void *)input,
      2*sizeof(float),
      outsample,
      linesize);

    /* wait for first IFFT input */
    if (tw_w != NULL) {
      lib_emt_wait(((edma)->channel[FFT_XFER_CHANNEL3]));
    }
    lib_emt_wait(((edma)->channel[FFT_XFER_CHANNEL0]));

    /* reconstruct the first column */
    for(i = 1; i < N1/2; i++){
      in1[(N1-i)*2]   =  in1[2*i];
      in1[(N1-i)*2+1] = -in1[2*i+1];
    }

    if (s_r3 == 0 && s_r5 == 0) {
      ifft_sp_cmplx_notwid_br((double *)in1, N1, (double *)out1, (double *)tw);
    } else {
      ifft_SPxSP_mixed_radix_c2c (N1, in1, tw, out1, N_p2, s_r3, s_r5);
    }

    co_si = _amem8_f2(tw1);
    twid  = co_si;
    for (j = 1; j < N1; j++) {
        temp = _amem8_f2(&out1[2*j]);
        temp = _complex_mpysp(temp, twid);
        twid = _complex_mpysp(co_si, twid);
        _amem8_f2(&out1[2*j]) = temp;
    }

    /* export output data of first IFFT, transposed */
    lib_emt_copy1D2D(
      ((edma)->channel[FFT_XFER_CHANNEL0]),
      (void *)out1,
      (void *)out_ptr_f,
      2*sizeof(float),
      outsample,
      linesize);

    /* wait for the N2/2+1 column */
    lib_emt_wait(((edma)->channel[FFT_XFER_CHANNEL1]));

    /* reconstruct the N2/2+1 column */
    for(i = 0; i < N1/2; i++){
      input[(N1-1-i)*2]   =  input[2*i];
      input[(N1-1-i)*2+1] = -input[2*i+1];
    }

    if (s_r3 == 0 && s_r5 == 0) {
      ifft_sp_cmplx_notwid_br((double *)input, N1, (double *)output, (double *)tw);
    } else {
      ifft_SPxSP_mixed_radix_c2c (N1, input, tw, output, N_p2, s_r3, s_r5);
    }

    co_si = _amem8_f2(tw1+ N2);
    twid  = co_si;
    for (j = 1; j < N1; j++) {
        temp = _amem8_f2(&output[2*j]);
        temp = _complex_mpysp(temp, twid);
        twid = _complex_mpysp(co_si, twid);
        _amem8_f2(&output[2*j]) = temp;
    }

    /* export output data of first IFFT, transposed */
    lib_emt_copy1D2D(
      ((edma)->channel[FFT_XFER_CHANNEL1]),
      (void *)output,
      (void *)out_ptr_b,
      2*sizeof(float),
      outsample,
      linesize);

    /* wait for the 1st and N2/2+1 column */
    lib_emt_wait(((edma)->channel[FFT_XFER_CHANNEL0]));
    lib_emt_wait(((edma)->channel[FFT_XFER_CHANNEL1]));
    /* from 2nd colume,
     * at the 1st core,  combine the 2nd and the N2th column together to reconstruct 2 columns
     *                   combine the 3rd and the (N2-1)th column,
     * at the 2nd core, combine the 4rd and the (N2-2)th column,
     *                   combine the 5rd and the (N2-3)th column,
     * at the 3rd core, combine the 6th and the (N2-4)th column
     *                   combine the 7rd and the (N2-5)th column,and so on
     * still reading in 4 lines per core and compute 4 lines per core at the same time, but the 2 front lines
     * and 2 back lines need to be done separately
     * */
#pragma omp parallel private(tid, tw, tw1, in_ptr_f, in_ptr_b, out_ptr_f, out_ptr_b, tempout, input, output, in1, out1, dum2, dum3, did, i, \
		i, j, k, ch_i, ch_o, ch_i1, ch_o1, dum0, dum1, twid, co_si, temp, lines_to_process) shared(num_of_lines, \
    data, work, data_w, work_w, work_t, tw_e, tw_w, N1, N2, lines, edma, twsize, insize, outsample, \
    linesize, tw_offset, N_p2, s_r3, s_r5)
  {
    tid = omp_get_thread_num();     // Obtain thread number
    did = DNUM;
    lines_to_process = lines;

    if(tid == numThread - 1)
      lines_to_process = N2 - lines*tid;

    if(lines < 1){
      if (tid < (N2/num_of_lines))
        lines_to_process = num_of_lines;
      else
    	lines_to_process = 0;
    }

    if (lines_to_process > 0){

    tw = tw_e;
    if (tw_w != NULL) {
      /* prepare twiddle buffer */
      lib_emt_copy1D1D(
        ((edma+did)->channel[FFT_XFER_CHANNEL3]), 
        (void *)tw_e,
        (void *)tw_w,
        twsize);
      tw  = tw_w;
    }
    tw1 = tw + tw_offset + num_of_lines*tid + 2;

    _nassert(N1 >= 8);
    _nassert(N2 >= 8);

    /* each core brings in 2 columns at front and 2 columns at the back for the matrix at each iteration */
    in_ptr_f  = &data[(1+(num_of_lines/2)*tid)*2];         /* pointer starts at the left side of the N1xN2 matrix */
    in_ptr_b  = &data[(N2-(1+tid)*num_of_lines/2)*2];    /* pointer starts at the right side of the N1xN2 matrix */
    out_ptr_f = &work[(1+(num_of_lines/2)*tid)*2];
    out_ptr_b = &work[(N2-(1+tid)*num_of_lines/2)*2];
    tempout   = work_t;

    /* stage 1: transpose, n2 IFFT's of size n1, apply global twiddle, transpose */
    /* prepare first IFFT input buffer */
    /* Brings in the front 2 columns */
    lib_emt_copy2D1D(
      ((edma+did)->channel[FFT_XFER_CHANNEL0]), 
      (void *)in_ptr_f,
      (void *)data_w,
      2*sizeof(float)*num_of_lines/2,
      outsample,
      linesize);

    lib_emt_wait(((edma+did)->channel[FFT_XFER_CHANNEL0]));

    lib_emt_copy2D1D(
      ((edma+did)->channel[FFT_XFER_CHANNEL0]),
      (void *)in_ptr_b,
      (void *)(data_w+N1*num_of_lines),
      2*sizeof(float)*num_of_lines/2,
      outsample,
      linesize);

    if((lines > num_of_lines)){
      in_ptr_f += num_of_lines*numThread;
      in_ptr_b -= num_of_lines*numThread;
    }
    else {
      in_ptr_f += num_of_lines;
      in_ptr_b -= num_of_lines;
    }

    lib_emt_wait(((edma+did)->channel[FFT_XFER_CHANNEL0]));

    /* construct the full signal based on the 2 parts */

    /* first do the transpose, front, back separately */
    for (i = 0; i<num_of_lines/2; i++){
      for (j = 0; j<N1/2; j++){
        _amem8_f2(&tempout[j*2+i*N1*2]) =  _amem8_f2(&data_w[j*4+i*2]);
      }
    }

    for (i = 0; i<num_of_lines/2; i++){
      for (j = 0; j<N1/2; j++){
        _amem8_f2(&tempout[j*2+i*N1*2+num_of_lines*N1]) =  _amem8_f2(&data_w[j*4+i*2+num_of_lines*N1]);
      }
    }

    for (i = 0; i<num_of_lines/2; i++){
      for (j = 0; j<N1/2; j++){
   	    tempout[(N1-1-j)*2+i*N1*2]     =  tempout[(3*N1+j)*2-i*N1*2];
    	tempout[(N1-1-j)*2+i*N1*2+1]   = -tempout[(3*N1+j)*2-i*N1*2+1];
    	}
    }
    /* set pingpong buffer pointers */
    in1    = data_w;
    out1   = work_w;
    input  = &data_w[2*N1*num_of_lines];
    output = &work_w[2*N1*num_of_lines];

    /* prepare second IFFT input buffer */
    /* bring in the front columns */
    lib_emt_copy2D1D(
      ((edma+did)->channel[FFT_XFER_CHANNEL2]), 
      (void *)in_ptr_f,
      (void *)input,
      2*sizeof(float)*num_of_lines/2,
      outsample,
      linesize);
    /* wait for front columns */
    lib_emt_wait(((edma+did)->channel[FFT_XFER_CHANNEL2]));

    /* bring in the back columns */
    lib_emt_copy2D1D(
      ((edma+did)->channel[FFT_XFER_CHANNEL2]),
      (void *)in_ptr_b,
      (void *)(input+num_of_lines*N1),
      2*sizeof(float)*num_of_lines/2,
      outsample,
      linesize);

    if((lines > 2*num_of_lines)){
      in_ptr_f += num_of_lines*numThread;
      in_ptr_b -= num_of_lines*numThread;
    }
    else {
      in_ptr_f += num_of_lines;
      in_ptr_b -= num_of_lines;
    }


    /* wait for first IFFT input */

#if (IFFT_OMP_SP_1D_C2R_NUMOFLINEBUFS>1)
    /* first ifft */
    if (s_r3 == 0 && s_r5 == 0) {
      for (i=0; i<num_of_lines/2; i++)
        ifft_sp_cmplx_notwid_br((double *)&tempout[i*N1*2], N1, (double *)&in1[i*N1*2], (double *)tw);
    } else {
      for (i=0; i<num_of_lines/2; i++)
        ifft_SPxSP_mixed_radix_c2c (N1, &tempout[i*N1*2], tw, &in1[i*N1*2], N_p2, s_r3, s_r5);
    }

    for (i=0; i<num_of_lines/2; i++) {
      co_si = _amem8_f2(tw1+2*i);
      twid  = co_si;
      for (j = 1; j < N1; j++) {
        temp = _amem8_f2(&in1[i*2*N1+2*j]);
        temp = _complex_mpysp(temp, twid);
        twid = _complex_mpysp(co_si, twid);
        _amem8_f2(&in1[i*N1*2+2*j]) = temp;
      }
    }
    /* conjugate the number to form the back columns */

    for (i=0; i<num_of_lines/2; i++){
      for (j=0; j<N1; j++){
        in1[(num_of_lines-i-1)*N1*2+2*j]   =  in1[i*N1*2+2*j];
        in1[(num_of_lines-i-1)*N1*2+2*j+1] = -in1[i*N1*2+2*j+1];
      }
    }

    if ((lines > num_of_lines) || tid < (N2/num_of_lines-1)){
      /* local transpose */
      DSPF_sp_mat_trans_cplx(in1, num_of_lines/2, N1, out1);
      DSPF_sp_mat_trans_cplx(&in1[num_of_lines*N1], num_of_lines/2, N1, &out1[num_of_lines*N1]);
    }
    else {
      /* copy to output*/

      for (i = 0; i<N1; i++){
        out1[2*i]   = in1[2*i];
        out1[2*i+1] = in1[2*i+1];
        out1[num_of_lines*N1+2*i]   =  in1[2*i];
        out1[num_of_lines*N1+2*i+1] = -in1[2*i+1];
      }
   }

#else
/* if won't work for this case */
#endif

    if(lines_to_process > num_of_lines)
    {
      /* export output data of first front IFFT, transposed */
      lib_emt_copy1D2D(
        ((edma+did)->channel[FFT_XFER_CHANNEL1]),
        (void *)out1,
        (void *)out_ptr_f,
        2*sizeof(float)*num_of_lines/2,
        outsample,
        linesize);
      out_ptr_f += num_of_lines * numThread;
      /* wait for EDMA */
      lib_emt_wait(((edma+did)->channel[FFT_XFER_CHANNEL1]));

      /* export output data of first back IFFT, transposed */
      lib_emt_copy1D2D(
      ((edma+did)->channel[FFT_XFER_CHANNEL4]),
      (void *)(out1+num_of_lines*N1),
      (void *)out_ptr_b,
      2*sizeof(float)*num_of_lines/2,
      outsample,
      linesize);

      out_ptr_b -= num_of_lines * numThread;
      /* wait for EDMA */
      lib_emt_wait(((edma+did)->channel[FFT_XFER_CHANNEL4]));
    }
    else {
      if(tid < (N2/num_of_lines - 1)){

        lib_emt_copy1D2D(
          ((edma+did)->channel[FFT_XFER_CHANNEL1]),
           (void *)out1,
           (void *)out_ptr_f,
           2*sizeof(float)*num_of_lines/2,
           outsample,
           linesize);
//        out_ptr_f += num_of_lines * (N2/num_of_lines);
        lib_emt_wait(((edma+did)->channel[FFT_XFER_CHANNEL1]));

        lib_emt_copy1D2D(
          ((edma+did)->channel[FFT_XFER_CHANNEL4]),
           (void *)(out1+N1*num_of_lines),
           (void *)out_ptr_b,
           2*sizeof(float)*num_of_lines/2,
           outsample,
           linesize);

//        out_ptr_b -= num_of_lines * (N2/num_of_lines);
        lib_emt_wait(((edma+did)->channel[FFT_XFER_CHANNEL4]));
      }
      else{

        lib_emt_copy1D2D(
          ((edma+did)->channel[FFT_XFER_CHANNEL1]),
           (void *)out1,
           (void *)out_ptr_f,
           2*sizeof(float),
           outsample,
           linesize);
        lib_emt_wait(((edma+did)->channel[FFT_XFER_CHANNEL1]));

        lib_emt_copy1D2D(
          ((edma+did)->channel[FFT_XFER_CHANNEL4]),
           (void *)(out1+N1*num_of_lines),
           (void *)(out_ptr_b+2),
           2*sizeof(float),
           outsample,
           linesize);
        lib_emt_wait(((edma+did)->channel[FFT_XFER_CHANNEL4]));

      }
    }

    /* prepare third IFFT input buffer */
    lib_emt_copy2D1D(
      ((edma+did)->channel[FFT_XFER_CHANNEL0]), 
      (void *)in_ptr_f,
      (void *)data_w,
      2*sizeof(float)*num_of_lines/2,
      outsample,
      linesize);
    /* wait for EDMA */
    lib_emt_wait(((edma+did)->channel[FFT_XFER_CHANNEL0]));

    lib_emt_copy2D1D(
      ((edma+did)->channel[FFT_XFER_CHANNEL0]),
      (void *)in_ptr_b,
      (void *)(data_w+num_of_lines*N1),
      2*sizeof(float)*num_of_lines/2,
      outsample,
      linesize);

    if((lines > 3*num_of_lines)){
      in_ptr_f += num_of_lines*numThread;
      in_ptr_b -= num_of_lines*numThread;
    }
    else {
      in_ptr_f += num_of_lines;
      in_ptr_b -= num_of_lines;
    }

    /* set channel numbers */
    ch_i  = FFT_XFER_CHANNEL2;
    ch_o  = FFT_XFER_CHANNEL3;
    ch_i1 = FFT_XFER_CHANNEL0;
    ch_o1 = FFT_XFER_CHANNEL1;
    ch_o2 = FFT_XFER_CHANNEL4;
    ch_o3 = FFT_XFER_CHANNEL5;

    /* wait for EDMA */
    lib_emt_wait(((edma+did)->channel[ch_i]));

#if (IFFT_OMP_SP_1D_C2R_NUMOFLINEBUFS>1)
    for (k = num_of_lines; k < lines_to_process; k += num_of_lines) {
      /* local transpose */
      /* construct the full signal based on the 2 parts */
      if (k < lines)
        tw1   = tw1 + num_of_lines * numThread;
      else
        tw1   = tw1 + num_of_lines;
      /* first do the transpose, front, back separately */
      for (i = 0; i<num_of_lines/2; i++){
        for (j = 0; j<N1/2; j++){
          _amem8_f2(&tempout[j*2+i*N1*2]) =  _amem8_f2(&input[j*4+i*2]);
        }
      }

      for (i = 0; i<num_of_lines/2; i++){
        for (j = 0; j<N1/2; j++){
          _amem8_f2(&tempout[j*2+i*N1*2+num_of_lines*N1]) =  _amem8_f2(&input[j*4+i*2+num_of_lines*N1]);
        }
      }

      for (i = 0; i<num_of_lines/2; i++){
        for (j = 0; j<N1/2; j++){
     	  tempout[(N1-1-j)*2+i*N1*2]     =  tempout[(3*N1+j)*2-i*N1*2];
      	  tempout[(N1-1-j)*2+i*N1*2+1]   = -tempout[(3*N1+j)*2-i*N1*2+1];
      	}
      }

      if (s_r3 == 0 && s_r5 == 0) {
        for (i=0; i<num_of_lines/2; i++)
          ifft_sp_cmplx_notwid_br((double *)&tempout[i*N1*2], N1, (double *)&input[i*N1*2], (double *)tw);
      } else {
        for (i=0; i<num_of_lines/2; i++)
          ifft_SPxSP_mixed_radix_c2c (N1, &tempout[i*N1*2], tw, &input[i*N1*2], N_p2, s_r3, s_r5);
      }

      for (i=0; i<num_of_lines/2; i++) {
        co_si = _amem8_f2(&tw1[2*i]);
        twid  = co_si;
        for (j = 1; j < N1; j++) {
          temp = _amem8_f2(&input[i*N1*2+2*j]);
          temp = _complex_mpysp(temp, twid);
          twid = _complex_mpysp(co_si, twid);
          _amem8_f2(&input[i*N1*2+2*j]) = temp;
        }
      }
      /* conjugate the number to form the back columns */

      for (i=0; i<num_of_lines/2; i++){
        for (j=0; j<N1; j++){
          input[(num_of_lines-i-1)*N1*2+2*j]   =  input[i*N1*2+2*j];
          input[(num_of_lines-i-1)*N1*2+2*j+1] = -input[i*N1*2+2*j+1];
        }
      }

      if ((k < (lines_to_process - num_of_lines)) || tid < (numThread-1)){
        /* local transpose */
        DSPF_sp_mat_trans_cplx(&input[0], num_of_lines/2, N1, &output[0]);
        DSPF_sp_mat_trans_cplx(&input[num_of_lines*N1], num_of_lines/2, N1, &output[num_of_lines*N1]);
      }
      else{
        /* copy to the output */
        for (i = 0; i<N1; i++){
          output[2*i]   = input[2*i];
          output[2*i+1] = input[2*i+1];
          output[num_of_lines*N1+2*i]   =  input[2*i];
          output[num_of_lines*N1+2*i+1] = -input[2*i+1];
        }

      }
#else
/* it won't work in this case */
#endif
      /* export output data, transposed */
    if(( k < (lines - num_of_lines) ) || (tid < (numThread-1))){

      lib_emt_copy1D2D(
        ((edma+did)->channel[ch_o]), 
        (void *)output,
        (void *)out_ptr_f,
        2*sizeof(float)*num_of_lines/2,
        outsample,
        linesize);
      out_ptr_f += num_of_lines * numThread;
      lib_emt_wait(((edma+did)->channel[ch_o]));

      lib_emt_copy1D2D(
        ((edma+did)->channel[ch_o2]),
        (void *)(output+N1*num_of_lines),
        (void *)out_ptr_b,
        2*sizeof(float)*num_of_lines/2,
        outsample,
        linesize);

      out_ptr_b -= num_of_lines * numThread;

      lib_emt_wait(((edma+did)->channel[ch_o2]));
    }
    else{
      if ((lines < lines_to_process) && (k < (lines_to_process - num_of_lines))) {

    	if ((k == lines) && (lines == num_of_lines)) {
      	  out_ptr_f -= num_of_lines * (numThread - 1);
    	  out_ptr_b += num_of_lines * (numThread - 1);
    	}

        lib_emt_copy1D2D(
          ((edma+did)->channel[ch_o]),
          (void *)output,
          (void *)out_ptr_f,
          2*sizeof(float)*num_of_lines/2,
          outsample,
          linesize);
        out_ptr_f += num_of_lines;
        lib_emt_wait(((edma+did)->channel[ch_o]));

        lib_emt_copy1D2D(
          ((edma+did)->channel[ch_o2]),
          (void *)(output+N1*num_of_lines),
          (void *)(out_ptr_b),
          2*sizeof(float)*num_of_lines/2,
          outsample,
          linesize);
        out_ptr_b -= num_of_lines;
        lib_emt_wait(((edma+did)->channel[ch_o2]));
      }else {

        lib_emt_copy1D2D(
          ((edma+did)->channel[ch_o]),
          (void *)output,
          (void *)out_ptr_f,
          2*sizeof(float),
          outsample,
          linesize);
          lib_emt_wait(((edma+did)->channel[ch_o]));

         lib_emt_copy1D2D(
          ((edma+did)->channel[ch_o2]),
          (void *)(output+N1*num_of_lines),
          (void *)(out_ptr_b+2),
          2*sizeof(float),
          outsample,
          linesize);
          lib_emt_wait(((edma+did)->channel[ch_o2]));
        }
    }

      /* prepare input data */
      lib_emt_copy2D1D(
        ((edma+did)->channel[ch_i]), 
        (void *)in_ptr_f,
        (void *)input,
        2*sizeof(float)*num_of_lines/2,
        outsample,
        linesize);
      lib_emt_wait(((edma+did)->channel[ch_i]));

      /* prepare input data */
      lib_emt_copy2D1D(
        ((edma+did)->channel[ch_i]),
        (void *)in_ptr_b,
        (void *)(input+N1*num_of_lines),
        2*sizeof(float)*num_of_lines/2,
        outsample,
        linesize);

      if (((lines > 4*num_of_lines) && (k < (lines - 3*num_of_lines))) || (lines_to_process == lines)) {
        in_ptr_f += num_of_lines * numThread;
        in_ptr_b -= num_of_lines * numThread;
      }
      else {
        in_ptr_f += num_of_lines;
        in_ptr_b -= num_of_lines;
      }

      /* swap pingpong buffer pointers and XFER channel */
      dum2   = input;
      input  = in1;
      in1    = dum2;
      dum3   = output;
      output = out1;
      out1   = dum3;

      dum0   = ch_i;
      ch_i   = ch_i1;
      ch_i1  = dum0;
      dum1   = ch_o;
      ch_o   = ch_o1;
      ch_o1  = dum1;
      dum1   = ch_o2;
      ch_o2  = ch_o3;
      ch_o3  = dum1;

      /* wait for EDMA */
      lib_emt_wait(((edma+did)->channel[ch_i]));
      lib_emt_wait(((edma+did)->channel[ch_o]));
      lib_emt_wait(((edma+did)->channel[ch_o2]));
    }
    /********************************************************* end of the first stage ***********************************************************************************/
    /* wait for stage1 to complete */
    lib_emt_wait(((edma+did)->channel[ch_i1]));
    lib_emt_wait(((edma+did)->channel[ch_o1]));
    lib_emt_wait(((edma+did)->channel[ch_o3]));
    }
  }

  /* stage 2: n1 IFFT's of size n2, transpose */
  num_of_lines = IFFT_OMP_SP_1D_C2R_NUMOFLINEBUFS;
  lines     = (N1/numThread/num_of_lines)*num_of_lines;
  if ((lines*numThread < N1) && (lines > 0))
    lines += num_of_lines;

  if (lines*numThread > N1)
    lines -= num_of_lines;
  twsize    = (u.sp_1d_c2r_e.para2.twsize)*sizeof(float);
  insize    = N2*2*sizeof(float)*num_of_lines;
  outsample = N2;
  linesize  = N1*sizeof(float);
  N_p2      = u.sp_1d_c2r_e.para2.N_p2;
  s_r3      = u.sp_1d_c2r_e.para2.s_r3;
  s_r5      = u.sp_1d_c2r_e.para2.s_r5;  

#pragma omp parallel private(tid, tw, tw1, in_ptr, out_ptr, tempout, input, output, in1, out1, dum2, dum3, did, i, \
		i, j, ch_i, ch_o, ch_i1, ch_o1, dum0, dum1, twid, co_si, temp, lines_to_process) shared(num_of_lines, \
    out, work, data_w, work_w, work_t, tw_e, tw_w, N1, N2, lines, edma, twsize, insize, outsample, \
    linesize, tw_offset, N_p2, s_r3, s_r5)
  {
    tid = omp_get_thread_num();     // Obtain thread number
    did = DNUM;
    lines_to_process = lines;
    if(tid == numThread - 1)
      lines_to_process = N1 - lines*tid;

    tw1 = tw_e + tw_offset + 2*N2;
    if (tw_w != NULL) {
      /* prepare twiddle buffer */
      lib_emt_copy1D1D(
        ((edma+did)->channel[FFT_XFER_CHANNEL3]),
        (void *)(tw_e + tw_offset + 2*N2),
        (void *)tw_w,
        twsize);
      tw1 = tw_w;
    }

    in_ptr  = &work[tid*(N2*2*lines)];
    out_ptr = &out[tid*lines];

    /* prepare first IFFT input buffer */
#ifndef USEFASTEDMA
    lib_emt_copy1D1D(
      ((edma+did)->channel[FFT_XFER_CHANNEL0]),
      (void *)in_ptr,
      (void *)work_w,
      insize);
#else
  lib_emt_copy1D1D_Fast(
    ((edma+did)->channel[FFT_XFER_CHANNEL0]),
    (void *)in_ptr,
    (void *)work_w);
#endif
    in_ptr += 2*N2*num_of_lines;

    /* set pingpong buffer pointers */
    in1    = work_w;
    out1   = data_w;
    input  = &work_w[2*N2*num_of_lines];
    output = &data_w[N2*num_of_lines];
    tempout= work_t;

    /* prepare second IFFT input buffer */
#ifndef USEFASTEDMA
      lib_emt_copy1D1D(
        ((edma+did)->channel[FFT_XFER_CHANNEL2]),
        (void *)in_ptr,
        (void *)input,
        insize);
#else
      lib_emt_copy1D1D_Fast(
        ((edma+did)->channel[FFT_XFER_CHANNEL2]),
        (void *)in_ptr,
        (void *)input);
#endif
    in_ptr += 2*N2*num_of_lines;

    /* wait for first IFFT input */
    lib_emt_wait(((edma+did)->channel[FFT_XFER_CHANNEL0]));
#if (IFFT_OMP_SP_1D_C2R_NUMOFLINEBUFS>1)
    if (s_r3 == 0 && s_r5 == 0) {
      for (i=0; i<num_of_lines; i++) {
        ifft_SPxSP_c2r (N2, &in1[i*N2*2], tw1, &tempout[i*N2], N2);
      }
    }
    else {
      for (i=0; i<num_of_lines; i++) {
        ifft_SPxSP_mixed_radix_c2r (N2, &in1[i*N2*2], tw1, &tempout[i*N2], N_p2, s_r3, s_r5);
      }
    }
  
    DSPF_sp_mat_trans(tempout, num_of_lines, N2, out1);
#else
    if (s_r3 == 0 && s_r5 == 0) {
      ifft_SPxSP_c2r (N2, in1, tw1, out1, N2);
    } else {
      ifft_SPxSP_mixed_radix_c2r (N2, in1, tw1, out1, N_p2, s_r3, s_r5);
    }
#endif  

    /* export output data of first IFFT, transposed */
#ifndef USEFASTEDMA
    lib_emt_copy1D2D(
      ((edma+did)->channel[FFT_XFER_CHANNEL1]), 
      (void *)out1,
      (void *)out_ptr,
      sizeof(float)*num_of_lines,
      outsample,
      linesize);
#else
    lib_emt_copy1D2D_Fast (
      ((edma+did)->channel[FFT_XFER_CHANNEL1]),
        (void *)out1,
        (void *)out_ptr);
#endif
    out_ptr += 1*num_of_lines;

    /* prepare third IFFT input buffer */
#ifndef USEFASTEDMA
    lib_emt_copy1D1D(
      ((edma+did)->channel[FFT_XFER_CHANNEL0]),
      (void *)in_ptr,
      (void *)work_w,
      insize);
#else
  lib_emt_copy1D1D_Fast(
    ((edma+did)->channel[FFT_XFER_CHANNEL0]),
    (void *)in_ptr,
    (void *)work_w);
#endif
    in_ptr += 2*N2*num_of_lines;

    /* set channel numbers */
    ch_i  = FFT_XFER_CHANNEL2;
    ch_o  = FFT_XFER_CHANNEL3;
    ch_i1 = FFT_XFER_CHANNEL0;
    ch_o1 = FFT_XFER_CHANNEL1;

    /* wait for EDMA */
    lib_emt_wait(((edma+did)->channel[ch_i]));

    for (j = 1; j < lines_to_process/num_of_lines; j++) {
#if (IFFT_OMP_SP_1D_C2R_NUMOFLINEBUFS>1)
      if (s_r3 == 0 && s_r5 == 0) {
        for (i=0; i<num_of_lines; i++) {
          ifft_SPxSP_c2r (N2, &input[i*N2*2], tw1, &tempout[i*N2], N2);
        }
      }
      else {
        for (i=0; i<num_of_lines; i++) {
          ifft_SPxSP_mixed_radix_c2r (N2, &input[i*N2*2], tw1, &tempout[i*N2], N_p2, s_r3, s_r5);
        }
      }
  
    DSPF_sp_mat_trans(tempout,  num_of_lines, N2, output);
#else
      if (s_r3 == 0 && s_r5 == 0) {
        ifft_SPxSP_c2r (N2, input, tw1, output, N2);
      } else {
        ifft_SPxSP_mixed_radix_c2r (N2, input, tw1, output, N_p2, s_r3, s_r5);
      }
#endif  

      /* export output data, transposed */
#ifndef USEFASTEDMA
      lib_emt_copy1D2D(
        ((edma+did)->channel[ch_o]),
        (void *)output,
        (void *)out_ptr,
        sizeof(float)*num_of_lines,
        outsample,
        linesize);
#else
    lib_emt_copy1D2D_Fast
    (
      ((edma+did)->channel[ch_o]),
        (void *)output,
        (void *)out_ptr);
#endif
      out_ptr += 1*num_of_lines;

      /* prepare input data */
#ifndef USEFASTEDMA
      lib_emt_copy1D1D(
        ((edma+did)->channel[ch_i]),
        (void *)in_ptr,
        (void *)input,
        insize);
#else
      lib_emt_copy1D1D_Fast(
        ((edma+did)->channel[ch_i]),
        (void *)in_ptr,
        (void *)input);
#endif
      in_ptr += 2*N2*num_of_lines;

      /* swap pingpong buffer pointers and XFER channel */
      dum2   = input;
      input  = in1;
      in1    = dum2;
      dum3   = output;
      output = out1;
      out1   = dum3;

      dum0   = ch_i;
      ch_i   = ch_i1;
      ch_i1  = dum0;
      dum1   = ch_o;
      ch_o   = ch_o1;
      ch_o1  = dum1;

      /* wait for EDMA */
      lib_emt_wait(((edma+did)->channel[ch_i]));
      lib_emt_wait(((edma+did)->channel[ch_o]));
    }

    /* wait for stage1 to complete */
    lib_emt_wait(((edma+did)->channel[ch_i1]));
    lib_emt_wait(((edma+did)->channel[ch_o1]));
  }
  omp_set_num_threads (OMP_MAX_NUM_CORES);

    return error;
} /* ifft_omp_sp_1d_c2r_ecpy */

/* ======================================================================== */
/*  End of file:  ifft_omp_sp_1d_c2r.c                                      */
/* ------------------------------------------------------------------------ */
/*            Copyright (c) 2013 Texas Instruments, Incorporated.           */
/*                           All Rights Reserved.                           */
/* ======================================================================== */
