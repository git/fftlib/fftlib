/* ======================================================================= */
/*  TEXAS INSTRUMENTS, INC.                                                */
/*                                                                         */
/*  FFTLIB  FFT Library                                                    */
/*                                                                         */
/* Copyright (C) 2013 Texas Instruments Incorporated - http://www.ti.com/  */ 
/*                                                                         */
/*                                                                         */
/*  Redistribution and use in source and binary forms, with or without     */
/*  modification, are permitted provided that the following conditions     */
/*  are met:                                                               */
/*                                                                         */
/*    Redistributions of source code must retain the above copyright       */
/*    notice, this list of conditions and the following disclaimer.        */
/*                                                                         */
/*    Redistributions in binary form must reproduce the above copyright    */
/*    notice, this list of conditions and the following disclaimer in the  */
/*    documentation and/or other materials provided with the               */
/*    distribution.                                                        */
/*                                                                         */
/*    Neither the name of Texas Instruments Incorporated nor the names of  */
/*    its contributors may be used to endorse or promote products derived  */
/*    from this software without specific prior written permission.        */
/*                                                                         */
/*  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS    */
/*  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT      */
/*  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR  */
/*  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT   */
/*  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,  */
/*  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT       */
/*  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,  */
/*  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY  */
/*  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT    */
/*  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE  */
/*  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.   */
/*                                                                         */
/* ======================================================================= */

#include <xdc/std.h>
#include "c6x.h"
#include "stdlib.h"
#include <stdio.h>
#include <ti/fftlib/src/common/omp/omp_config.h>
#include "fft_omp_dp_1d_r2c.h"
#include <ti/fftlib/src/common/fft/fft_dp_mixed_bs.h>
#include <ti/dsplib/src/DSPF_dp_fftDPxDP/DSPF_dp_fftDPxDP.h>
#include <ti/dsplib/src/DSPF_dp_mat_trans/DSPF_dp_mat_trans.h>
#include <ti/fftlib/src/common/fft_edma.h>
#include <ti/runtime/openmp/omp.h>
#include <ti/libarch/libarch.h>

static void DSPF_dp_mat_trans_cplx(const double *restrict x, const int rows,
  const int cols, double *restrict y)
{
  int i, j = 0;

  #pragma MUST_ITERATE(2,,)
  for(i = 0; i < rows; i++)
  #pragma MUST_ITERATE(2,,)
  for(j = 0; j < cols; j++)
  {
    y[j*rows*2 + i*2] = x[j*2 + cols*i*2];
    y[j*rows*2 + i*2 + 1] = x[1 + j*2 + cols*i*2];
  }
}

static int    N1;       /* stage 1 size                     */
static int    N2;       /* stage 2 size                     */
static int    rad1;     /* stage 1 radix                    */
static int    rad2;     /* stage 2 radix                    */
static int   N_p2;      /* power of 2 size in mixed radix   */
static int   s_r3;      /* number stages of radix 3         */
static int   s_r5;      /* number stages of radix 5         */
static double *data;    /* data pointer                     */
static double *out;     /* output pointer                   */
static double *work;    /* working buffer pointer           */
static double *data_w;  /* data internal pointer            */
static double *work_w;  /* working buffer internal pointer  */
static double *work_t;  /* temp buffer internal pointer     */
static double *tw_e;    /* twiddle buffer pointer           */
static double *tw_w;    /* twiddle working buffer pointer   */
static int    lines;
static FFT_EDMA_Struct *edma;
#ifndef FFT_MEM_MODEL_SM
#pragma DATA_SECTION (edma,   ".mem_msm")
#pragma DATA_SECTION (lines,  ".mem_msm")
#pragma DATA_SECTION (N1,     ".mem_msm")
#pragma DATA_SECTION (N2,     ".mem_msm")
#pragma DATA_SECTION (rad1,   ".mem_msm")
#pragma DATA_SECTION (rad2,   ".mem_msm")
#pragma DATA_SECTION (s_r3,   ".mem_msm")
#pragma DATA_SECTION (s_r5,   ".mem_msm")
#pragma DATA_SECTION (N_p2,   ".mem_msm")
#pragma DATA_SECTION (data,   ".mem_msm")
#pragma DATA_SECTION (out,    ".mem_msm")
#pragma DATA_SECTION (work,   ".mem_msm")
#pragma DATA_SECTION (data_w, ".mem_msm")
#pragma DATA_SECTION (work_w, ".mem_msm")
#pragma DATA_SECTION (tw_e,   ".mem_msm")
#pragma DATA_SECTION (tw_w,   ".mem_msm")
#endif

int fft_omp_dp_1d_r2c_ecpy (
    /* int    N;        FFT size                         */
    /* int    n1;       stage1 FFT size                  */
    /* int    n2;       stage2 FFT size                  */
    /* double *data;    data pointer                     */
    /* double *work;    working buffer pointer           */
    /* double *data_w;  data internal pointer            */
    /* double *work_w;  working buffer internal pointer  */
    /* double *work_t;  temp buffer internal pointer     */
    /* double *tw;      twiddle pointer                  */
    /* double *tw_w;    twiddle internal pointer         */
    fft_param_u u,
    void *edmaState,
    int   actualCoreNum
)
{
    int numThread = omp_get_max_threads();
    int tid, did, num_of_lines, lines_to_process, N;
    int error = FFT_NOERR;
    int twsize, tw_offset;
    int i,j,k,ch_i,ch_o,ch_i1,ch_o1,dum0,dum1;
    int insize, outsample, linesize;

    double *restrict in_ptr, *restrict out_ptr, *restrict tempout;
    double *restrict input, *restrict output, *restrict in1, *restrict out1, *dum2, *dum3;
    double *restrict tw, *restrict tw1, *restrict tw2;
    double temp_r, temp_i;
    double twid_r, twid_i, tw_cos, tw_sin, temp;
    lib_memdscr_t **fft_mem_handle = fftGetMemHandle();

    /* input parameters */
    N       = u.dp_1d_r2c_e.N;
    N1      = u.dp_1d_r2c_e.n1;         /* stage 1 size                     */
    N2      = u.dp_1d_r2c_e.n2;         /* stage 2 size                     */
    rad1    = u.dp_1d_r2c_e.para1.rad;  /* stage 1 radix                    */
    rad2    = u.dp_1d_r2c_e.para2.rad;  /* stage 2 radix                    */
    N_p2    = u.dp_1d_r2c_e.para1.N_p2; /* power of 2 size in mixed radix   */
    s_r3    = u.dp_1d_r2c_e.para1.s_r3; /* number stages of radix 3         */
    s_r5    = u.dp_1d_r2c_e.para1.s_r5; /* number stages of radix 5         */
    data    = u.dp_1d_r2c_e.data;       /* data pointer                     */
    out     = u.dp_1d_r2c_e.work;       /* output pointer                   */
    work    = u.dp_1d_r2c_e.tw;         /* working buffer pointer           */
    tw_e    = u.dp_1d_r2c_e.tw + 2*N;     /* twiddle pointer                  */
    tw_offset = u.dp_1d_r2c_e.para1.twsize; /* Offset needed for twiddle ptr    */

    data_w  = (double*)lib_smem_falloc (fft_mem_handle, 4*N1*FFT_OMP_DP_1D_R2C_NUMOFLINEBUFS*sizeof(double), 8);
    work_w  = (double*)lib_smem_falloc (fft_mem_handle, 4*N1*FFT_OMP_DP_1D_R2C_NUMOFLINEBUFS*sizeof(double), 8);
    work_t  = (double*)lib_smem_falloc (fft_mem_handle, 2*N1*FFT_OMP_DP_1D_R2C_NUMOFLINEBUFS*sizeof(double), 8);
    twsize  = (u.dp_1d_r2c_e.para1.twsize+2*N2 > u.dp_1d_r2c_e.para2.twsize) ? u.dp_1d_r2c_e.para1.twsize+2*N2 : u.dp_1d_r2c_e.para2.twsize;
#ifdef FFT_MEM_MODEL_LG
    tw_w    = (double*)lib_smem_falloc (fft_mem_handle, twsize*sizeof(double), 8);
#endif

#ifdef FFT_MEM_MODEL_LG
    if(data_w==NULL || work_w==NULL || work_t==NULL || tw_w==NULL) {
#else
    tw_w = NULL;
    if(data_w==NULL || work_w==NULL || work_t==NULL) {
#endif
        printf("Memory allocation error!\n");
        return FFT_ERROR;
    }

    if ((actualCoreNum==1)||
        (actualCoreNum==2)||
        (actualCoreNum==4)||
        (actualCoreNum==8))
      omp_set_num_threads (actualCoreNum);
    else
      omp_set_num_threads (OMP_MAX_NUM_CORES);
    numThread = omp_get_max_threads();
    edma    = (FFT_EDMA_Struct *)edmaState;

    num_of_lines = FFT_OMP_DP_1D_R2C_NUMOFLINEBUFS;    
    lines   = (N2/numThread/num_of_lines)*num_of_lines;
    if ((lines*numThread < N2) && (lines > 0))
      lines += num_of_lines;

    if (lines*numThread > N2)
      lines -= num_of_lines;

    twsize    = (u.dp_1d_r2c_e.para1.twsize + 2*N2)*sizeof(double);
    insize    = N1*sizeof(double);
    outsample = N1;
    linesize  = N2*sizeof(double);

#pragma omp parallel private(tw, tw1, tw2, in_ptr, out_ptr, tempout, input, output, in1, out1, dum2, dum3, did, i, \
    i,j,k,ch_i,ch_o,ch_i1,ch_o1,dum0,dum1, temp_r, temp_i, twid_r, twid_i, tw_cos, tw_sin, temp, tid, lines_to_process) shared(num_of_lines, \
        rad1, rad2, data, work, data_w, work_w, work_t, tw_e, tw_w, N1, N2, lines, edma, twsize, insize, outsample, \
        tw_offset, linesize, N_p2, s_r3, s_r5)
  {
    tid = omp_get_thread_num();     // Obtain thread number
    did = DNUM;

    lines_to_process = lines;

    if(tid == numThread - 1)
      lines_to_process = N2 - lines*tid;

    if(lines < 1){
      if (tid < (N2/num_of_lines))
        lines_to_process = num_of_lines;
      else
    	lines_to_process = 0;
    }

    if (lines_to_process > 0){
    tw = tw_e;
    if (tw_w != NULL) {
      /* prepare twiddle buffer */
      lib_emt_copy1D1D(
        ((edma+did)->channel[FFT_XFER_CHANNEL3]),
        (void *)tw_e,
        (void *)tw_w,
        twsize);
      tw = tw_w;
    }
    /* set global twiddle pointer */
    if(lines > 0)
      tw2 = tw + tw_offset + 2*tid*lines;
    else
      tw2 = tw + tw_offset + 2*tid*lines_to_process;

    _nassert(N1 >= 8);
    _nassert(N2 >= 8);

    if (lines > 0){
      in_ptr  = &data[tid*lines];
      out_ptr = &work[tid*2*lines];
    }
    else{
      in_ptr  = &data[tid*lines_to_process];
      out_ptr = &work[tid*2*lines_to_process];
    }

    /* stage 1: transpose, n2 FFT's of size n1, apply global twiddle, transpose */
    /* prepare first FFT input buffer */
    lib_emt_copy2D1D(
      ((edma+did)->channel[FFT_XFER_CHANNEL0]),
      (void *)in_ptr,
      (void *)data_w,
      sizeof(double)*num_of_lines,
      outsample,
      linesize);
    in_ptr += num_of_lines;

    /* set pingpong buffer pointers */
    in1    = data_w;
    out1   = work_w;
    input  = &data_w[2*N1*num_of_lines];
    output = &work_w[2*N1*num_of_lines];
    tempout= work_t;

    /* prepare second FFT input buffer */
    lib_emt_copy2D1D(
      ((edma+did)->channel[FFT_XFER_CHANNEL2]),
      (void *)in_ptr,
      (void *)input,
      sizeof(double)*num_of_lines,
      outsample,
      linesize);
    in_ptr += num_of_lines;

    /* wait for first FFT input */
    if (tw_w != NULL) {
      lib_emt_wait((edma+did)->channel[FFT_XFER_CHANNEL3]);
    }
    lib_emt_wait((edma+did)->channel[FFT_XFER_CHANNEL0]);

    /* Calculating the FFT */
#if (FFT_OMP_DP_1D_R2C_NUMOFLINEBUFS>1)
    /* local transpose */
    DSPF_dp_mat_trans(in1, N1, num_of_lines, tempout);

    if (s_r3 == 0 && s_r5 == 0) {
      for (i=0; i<num_of_lines; i++)
        DSPF_dp_fftDPxDP_r2c (N1, &tempout[i*N1], tw, &in1[i*N1*2], rad1, 0, N1);
    } else {
      for (i=0; i<num_of_lines; i++)
        fft_DPxDP_mixed_radix_r2c (N1, &tempout[i*N1], tw, &in1[i*N1*2], N_p2, s_r3, s_r5, rad1);
    }

    for (i=0; i<num_of_lines; i++) {
      tw_cos = tw2[2*i];
      tw_sin = tw2[2*i+1];
      twid_r = tw_cos;
      twid_i = tw_sin;
      for (j = 2; j < 2*N1; j+=2) {
          temp_r = in1[i*N1*2+j];
          temp_i = in1[i*N1*2+j+1];
          in1[i*N1*2+j]   = temp_r*twid_r - temp_i*twid_i;
          in1[i*N1*2+j+1] = temp_r*twid_i + temp_i*twid_r;
          temp   = tw_cos*twid_r - tw_sin*twid_i;
          twid_i = tw_sin*twid_r + tw_cos*twid_i;
          twid_r = temp;
      }
    }

    /* local transpose */
    DSPF_dp_mat_trans_cplx(in1, num_of_lines, N1, out1);
#else
    if (s_r3 == 0 && s_r5 == 0) {
      DSPF_dp_fftDPxDP_r2c (N1, in1, tw, out1, rad1, 0, N1);
    } else {
      fft_DPxDP_mixed_radix_r2c (N1, in1, tw, out1, N_p2, s_r3, s_r5, rad1);
    }
    tw_cos = tw2[0];
    tw_sin = tw2[1];
    twid_r = tw_cos;
    twid_i = tw_sin;
    for (j = 2; j < 2*N1; j+=2) {
        temp_r = out1[j];
        temp_i = out1[j+1];
        out1[j]   = temp_r*twid_r - temp_i*twid_i;
        out1[j+1] = temp_r*twid_i + temp_i*twid_r;
        temp   = tw_cos*twid_r - tw_sin*twid_i;
        twid_i = tw_sin*twid_r + tw_cos*twid_i;
        twid_r = temp;
    }
#endif
    /* export output data of first FFT, transposed */
    lib_emt_copy1D2D(
      ((edma+did)->channel[FFT_XFER_CHANNEL1]),
      (void *)out1,
      (void *)out_ptr,
      2*sizeof(double)*num_of_lines,
      outsample,
      2*linesize);
    out_ptr += 2*num_of_lines;
    
#ifdef USEFASTEDMA
  /* fill the PaRAM for CHANNEL 3 without starting it */
  lib_emt_copy1D2D_Fill(
    ((edma+did)->channel[FFT_XFER_CHANNEL3]),
    (void *)out1,
    (void *)out_ptr,
    2*sizeof(double)*num_of_lines,
    outsample,
    linesize);
#endif

    /* prepare third FFT input buffer */
    lib_emt_copy2D1D(
      ((edma+did)->channel[FFT_XFER_CHANNEL0]),
      (void *)in_ptr,
      (void *)data_w,
      sizeof(double)*num_of_lines,
      outsample,
      linesize);
    in_ptr += num_of_lines;

    /* set channel numbers */
    ch_i  = FFT_XFER_CHANNEL2;
    ch_o  = FFT_XFER_CHANNEL3;
    ch_i1 = FFT_XFER_CHANNEL0;
    ch_o1 = FFT_XFER_CHANNEL1;

    /* wait for EDMA */
    lib_emt_wait((edma+did)->channel[ch_i]);

#if (FFT_OMP_DP_1D_R2C_NUMOFLINEBUFS>1)
    for (k = num_of_lines; k < lines_to_process; k += num_of_lines) {
      /* local transpose */
      DSPF_dp_mat_trans(input, N1, num_of_lines, tempout);

      if (s_r3 == 0 && s_r5 == 0) {
        for (i=0; i<num_of_lines; i++)
          DSPF_dp_fftDPxDP_r2c (N1, &tempout[i*N1], tw, &input[i*N1*2], rad1, 0, N1);
      } else {
        for (i=0; i<num_of_lines; i++)
          fft_DPxDP_mixed_radix_r2c (N1, &tempout[i*N1], tw, &input[i*N1*2], N_p2, s_r3, s_r5, rad1);
      }

      for (i=0; i<num_of_lines; i++) {
        tw_cos = tw2[2*k+2*i];
        tw_sin = tw2[2*k+2*i+1];
        twid_r = tw_cos;
        twid_i = tw_sin;
        for (j = 2; j < 2*N1; j+=2) {
            temp_r = input[i*N1*2+j];
            temp_i = input[i*N1*2+j+1];
            input[i*N1*2+j]   = temp_r*twid_r - temp_i*twid_i;
            input[i*N1*2+j+1] = temp_r*twid_i + temp_i*twid_r;
            temp   = tw_cos*twid_r - tw_sin*twid_i;
            twid_i = tw_sin*twid_r + tw_cos*twid_i;
            twid_r = temp;
        }
      }

      /* local transpose */
      DSPF_dp_mat_trans_cplx(input, num_of_lines, N1, output);
#else
    for (i = 1; i < lines; i++) {

      if (s_r3 == 0 && s_r5 == 0) {
        DSPF_dp_fftDPxDP_r2c (N1, input, tw, output, rad1, 0, N1);
      } else {
        fft_DPxDP_mixed_radix_r2c (N1, input, tw, output, N_p2, s_r3, s_r5, rad1);
      } 
      tw_cos = tw2[2*i];
      tw_sin = tw2[2*i+1];
      twid_r = tw_cos;
      twid_i = tw_sin;
      for (j = 2; j < 2*N1; j+=2) {
          temp_r = output[j];
          temp_i = output[j+1];
          output[j]   = temp_r*twid_r - temp_i*twid_i;
          output[j+1] = temp_r*twid_i + temp_i*twid_r;
          temp   = tw_cos*twid_r - tw_sin*twid_i;
          twid_i = tw_sin*twid_r + tw_cos*twid_i;
          twid_r = temp;
      }
#endif
      /* export output data, transposed */
      lib_emt_copy1D2D(
        ((edma+did)->channel[ch_o]),
        (void *)output,
        (void *)out_ptr,
        2*sizeof(double)*num_of_lines,
        outsample,
        2*linesize);
      out_ptr += 2*num_of_lines;

      /* prepare input data */
      lib_emt_copy2D1D(
        ((edma+did)->channel[ch_i]),
        (void *)in_ptr,
        (void *)input,
        sizeof(double)*num_of_lines,
        outsample,
        linesize);
      in_ptr += num_of_lines;

      /* swap pingpong buffer pointers and XFER channel */
      dum2   = input;
      input  = in1;
      in1    = dum2;
      dum3   = output;
      output = out1;
      out1   = dum3;

      dum0   = ch_i;
      ch_i   = ch_i1;
      ch_i1  = dum0;
      dum1   = ch_o;
      ch_o   = ch_o1;
      ch_o1  = dum1;

      /* wait for EDMA */
      lib_emt_wait((edma+did)->channel[ch_i]);
      lib_emt_wait((edma+did)->channel[ch_o]);
    }
    /* wait for stage1 to complete */
    lib_emt_wait((edma+did)->channel[ch_i1]);
    lib_emt_wait((edma+did)->channel[ch_o1]);
    }
  }

  /* stage 2: n1 FFT's of size n2, transpose */
  num_of_lines = FFT_OMP_DP_1D_R2C_NUMOFLINEBUFS;
  lines     = (N1/numThread/num_of_lines)*num_of_lines;
  if ((lines*numThread < N1) && (lines > 0))
    lines += num_of_lines;

  if (lines*numThread > N1)
    lines -= num_of_lines;
  twsize    = (u.dp_1d_r2c_e.para2.twsize)*sizeof(double);
  insize    = N2*2*sizeof(double)*num_of_lines;
  outsample = N2;
  linesize  = N1*2*sizeof(double);
  N_p2      = u.dp_1d_r2c_e.para2.N_p2;
  s_r3      = u.dp_1d_r2c_e.para2.s_r3;
  s_r5      = u.dp_1d_r2c_e.para2.s_r5;


#pragma omp parallel private(tid, tw, tw1, tw2, in_ptr, out_ptr, tempout, input, output, in1, out1, dum2, dum3, did, i, \
    i,j,ch_i,ch_o,ch_i1,ch_o1,dum0,dum1, temp_r, temp_i, twid_r, twid_i, tw_cos, tw_sin, temp, lines_to_process) shared(num_of_lines, \
        rad1, rad2, out, work, data_w, work_w, work_t, tw_e, tw_w, N1, N2, lines, edma, twsize, insize, outsample, \
        tw_offset, linesize, N_p2, s_r3, s_r5)
  {
    tid = omp_get_thread_num();     // Obtain thread number
    did = DNUM;
    lines_to_process = lines;
    if(tid == numThread - 1)
      lines_to_process = N1 - lines*tid;

    tw1 = tw_e + tw_offset + 2*N2;
    if (tw_w != NULL) {
      /* prepare twiddle buffer */
      lib_emt_copy1D1D(
        ((edma+did)->channel[FFT_XFER_CHANNEL3]),
        (void *)(tw_e + tw_offset + 2*N2),
        (void *)tw_w,
        twsize);
      tw1 = tw_w;
    }

    in_ptr  = &work[tid*(N2*2*lines)];
    out_ptr = &out[tid*2*lines];

    /* prepare first FFT input buffer */
#ifndef USEFASTEDMA
    lib_emt_copy1D1D(
      ((edma+did)->channel[FFT_XFER_CHANNEL0]),
      (void *)in_ptr,
      (void *)work_w,
      insize);
#else
  lib_emt_copy1D1D_Fast(
    ((edma+did)->channel[FFT_XFER_CHANNEL0]),
    (void *)in_ptr,
    (void *)work_w);
#endif
    in_ptr += 2*N2*num_of_lines;

    /* set pingpong buffer pointers */
    in1    = work_w;
    out1   = data_w;
    input  = &work_w[2*N2*num_of_lines];
    output = &data_w[2*N2*num_of_lines];
    tempout= work_t;

    /* prepare second FFT input buffer */
#ifndef USEFASTEDMA
      lib_emt_copy1D1D(
        ((edma+did)->channel[FFT_XFER_CHANNEL2]),
        (void *)in_ptr,
        (void *)input,
        insize);
#else
      lib_emt_copy1D1D_Fast(
        ((edma+did)->channel[FFT_XFER_CHANNEL2]),
        (void *)in_ptr,
        (void *)input);
#endif
    in_ptr += 2*N2*num_of_lines;

    /* wait for first FFT input */
    lib_emt_wait((edma+did)->channel[FFT_XFER_CHANNEL0]);
#if (FFT_OMP_DP_1D_R2C_NUMOFLINEBUFS>1)
      if (s_r3 == 0 && s_r5 == 0) {
        for (i=0; i<num_of_lines; i++){      
          DSPF_dp_fftDPxDP (N2, &in1[i*N2*2], tw1, &tempout[i*N2*2], rad2, 0, N2);
        }
      }
      else {
        for (i=0; i<num_of_lines; i++){        
          fft_DPxDP_mixed_radix (N2, &in1[i*N2*2], tw1, &tempout[i*N2*2], N_p2, s_r3, s_r5, rad2);
        }
      }      
  
    DSPF_dp_mat_trans_cplx(tempout, num_of_lines, N2, out1);
#else
    if (s_r3 == 0 && s_r5 == 0) {
      DSPF_dp_fftDPxDP (N2, in1, tw1, out1, rad2, 0, N2);
    }
    else {
      fft_DPxDP_mixed_radix (N2, in1, tw1, out1, N_p2, s_r3, s_r5, rad2);
    }
#endif  

    /* export output data of first FFT, transposed */
#ifndef USEFASTEDMA
      lib_emt_copy1D2D(
        ((edma+did)->channel[FFT_XFER_CHANNEL1]),
        (void *)out1,
        (void *)out_ptr,
        2*sizeof(double)*num_of_lines,
        outsample/2,
        linesize);

     if (tid == 0){
      out_ptr[N1*outsample] = out1[outsample*num_of_lines];
      out_ptr[N1*outsample+1] = out1[outsample*num_of_lines+1];
     }

#else
    lib_emt_copy1D2D_Fast
    (
      ((edma+did)->channel[FFT_XFER_CHANNEL1]),
        (void *)out1,
        (void *)out_ptr);
#endif
    out_ptr += 2*num_of_lines;

    /* prepare third FFT input buffer */    
#ifndef USEFASTEDMA
    lib_emt_copy1D1D(
      ((edma+did)->channel[FFT_XFER_CHANNEL0]),
      (void *)in_ptr,
      (void *)work_w,
      insize);
#else
  lib_emt_copy1D1D_Fast(
    ((edma+did)->channel[FFT_XFER_CHANNEL0]),
    (void *)in_ptr,
    (void *)in_w);
#endif
    in_ptr += 2*N2*num_of_lines;

    /* set channel numbers */
    ch_i  = FFT_XFER_CHANNEL2;
    ch_o  = FFT_XFER_CHANNEL3;
    ch_i1 = FFT_XFER_CHANNEL0;
    ch_o1 = FFT_XFER_CHANNEL1;

    /* wait for EDMA */
    lib_emt_wait((edma+did)->channel[ch_i]);

    for (j = 1; j < lines_to_process/num_of_lines; j++) {
#if (FFT_OMP_DP_1D_R2C_NUMOFLINEBUFS>1)
      if (s_r3 == 0 && s_r5 == 0) {
        for (i=0; i<num_of_lines; i++){      
          DSPF_dp_fftDPxDP (N2, &input[i*N2*2], tw1, &tempout[i*N2*2], rad2, 0, N2);
        }
      }
      else {
        for (i=0; i<num_of_lines; i++){
          fft_DPxDP_mixed_radix (N2, &input[i*N2*2], tw1, &tempout[i*N2*2], N_p2, s_r3, s_r5, rad2);
        }
      }
  
    DSPF_dp_mat_trans_cplx(tempout, num_of_lines, N2, output);
#else
    if (s_r3 == 0 && s_r5 == 0) {
      DSPF_dp_fftDPxDP (N2, input, tw1, output, rad2, 0, N2);
    }
    else {
      fft_DPxDP_mixed_radix (N2, input, tw1, output, N_p2, s_r3, s_r5, rad2);
    }
#endif  

      /* export output data, transposed */
#ifndef USEFASTEDMA
      lib_emt_copy1D2D(
        ((edma+did)->channel[ch_o]),
        (void *)output,
        (void *)out_ptr,
        2*sizeof(double)*num_of_lines,
        outsample/2,
        linesize);
#else
    lib_emt_copy1D2D_Fast
    (
      ((edma+did)->channel[ch_o]),
        (void *)output,
        (void *)out_ptr);
#endif
      out_ptr += 2*num_of_lines;

      /* prepare input data */
#ifndef USEFASTEDMA
      lib_emt_copy1D1D(
        ((edma+did)->channel[ch_i]),
        (void *)in_ptr,
        (void *)input,
        insize);
#else
      lib_emt_copy1D1D_Fast(
        ((edma+did)->channel[ch_i]),
        (void *)in_ptr,
        (void *)input);
#endif
      in_ptr += 2*N2*num_of_lines;

      /* swap pingpong buffer pointers and XFER channel */
      dum2   = input;
      input  = in1;
      in1    = dum2;
      dum3   = output;
      output = out1;
      out1   = dum3;

      dum0   = ch_i;
      ch_i   = ch_i1;
      ch_i1  = dum0;
      dum1   = ch_o;
      ch_o   = ch_o1;
      ch_o1  = dum1;

      /* wait for EDMA */
      lib_emt_wait((edma+did)->channel[ch_i]);
      lib_emt_wait((edma+did)->channel[ch_o]);
    }

    /* wait for stage1 to complete */
    lib_emt_wait((edma+did)->channel[ch_i1]);
    lib_emt_wait((edma+did)->channel[ch_o1]);
  }

  omp_set_num_threads (OMP_MAX_NUM_CORES);

  return error;
} /* fft_omp_dp_1d_r2c_ecpy */

/* ======================================================================== */
/*  End of file:  fft_omp_dp_1d_r2c.c                                       */
/* ------------------------------------------------------------------------ */
/*            Copyright (c) 2013 Texas Instruments, Incorporated.           */
/*                           All Rights Reserved.                           */
/* ======================================================================== */
