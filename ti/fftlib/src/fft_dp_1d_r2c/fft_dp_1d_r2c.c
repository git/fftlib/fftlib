/* ======================================================================= */
/*  TEXAS INSTRUMENTS, INC.                                                */
/*                                                                         */
/*  FFTLIB  FFT Library                                                    */
/*                                                                         */
/* Copyright (C) 2013 Texas Instruments Incorporated - http://www.ti.com/  */ 
/*                                                                         */
/*                                                                         */
/*  Redistribution and use in source and binary forms, with or without     */
/*  modification, are permitted provided that the following conditions     */
/*  are met:                                                               */
/*                                                                         */
/*    Redistributions of source code must retain the above copyright       */
/*    notice, this list of conditions and the following disclaimer.        */
/*                                                                         */
/*    Redistributions in binary form must reproduce the above copyright    */
/*    notice, this list of conditions and the following disclaimer in the  */
/*    documentation and/or other materials provided with the               */
/*    distribution.                                                        */
/*                                                                         */
/*    Neither the name of Texas Instruments Incorporated nor the names of  */
/*    its contributors may be used to endorse or promote products derived  */
/*    from this software without specific prior written permission.        */
/*                                                                         */
/*  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS    */
/*  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT      */
/*  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR  */
/*  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT   */
/*  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,  */
/*  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT       */
/*  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,  */
/*  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY  */
/*  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT    */
/*  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE  */
/*  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.   */
/*                                                                         */
/* ======================================================================= */

#include <xdc/std.h>
#include "c6x.h"
#include "stdlib.h"
#include "fft_dp_1d_r2c.h"
#include <ti/fftlib/src/common/fft_edma.h>
#include <ti/fftlib/src/common/fft/fft_dp_mixed_bs.h>
#include <ti/dsplib/src/DSPF_dp_fftDPxDP/DSPF_dp_fftDPxDP.h>

static void DSPF_dp_mat_trans_cplx(const double *restrict x, const int rows,
  const int cols, double *restrict y)
{
  int i, j = 0;

  #pragma MUST_ITERATE(2,,)
  for(i = 0; i < rows; i++)
  #pragma MUST_ITERATE(2,,)
  for(j = 0; j < cols; j++)
  {
    y[j*rows*2 + i*2] = x[j*2 + cols*i*2];
    y[j*rows*2 + i*2 + 1] = x[1 + j*2 + cols*i*2];
  }
}

int fft_dp_1d_r2c_direct (
    /* int     N;       FFT size        */
    /* double *in;      input pointer   */
    /* double *out;     output pointer  */
    /* double *tw;      twiddle pointer */
    fft_param_u u,
    void  *edmaState
)
{
    int   error = FFT_NOERR;
    /* input parameters */
    int   N            = u.dp_1d_r2c_d.N;            /* signal size      */
    int   M            = u.dp_1d_r2c_d.para1.M;      /* FFT size M       */
    int   rad          = u.dp_1d_r2c_d.para1.rad;    /* radix            */
    int   N_p2         = u.dp_1d_r2c_d.para1.N_p2;   /* power of 2 size in mixed radix */
    int   s_r3         = u.dp_1d_r2c_d.para1.s_r3;   /* number stages of radix 3       */
    int   s_r5         = u.dp_1d_r2c_d.para1.s_r5;   /* number stages of radix 5       */
    
    double *in         = u.dp_1d_r2c_d.in;           /* input pointer    */
    double *out        = u.dp_1d_r2c_d.out;          /* output pointer   */
    double *tw         = u.dp_1d_r2c_d.tw;           /* twiddle pointer  */
    double *Bn         = u.dp_1d_r2c_d.Bn;           /* Bn buffer pointer              */
    double *workbuf    = u.dp_1d_r2c_d.workbuf;      /* workbuffer pointer for BS      */

    if (N == M) {
      if (s_r3 == 0 && s_r5 == 0) {
    	  DSPF_dp_fftDPxDP_r2c (N, in, tw, out, rad, 0, N);
      } else {
        fft_DPxDP_mixed_radix_r2c (N, in, tw, out, N_p2, s_r3, s_r5, rad);
      }
    }
    else {
      fft_DPxDP_r2c_bs (N, M, rad, in, out, tw,  Bn, workbuf);
    }

    return error;
}

int fft_dp_1d_r2c_ecpy (
    /* int    N;           FFT size                */
    /* double *data;       data pointer            */
    /* double *data_w;     data internal pointer   */
    /* double *work_w;     working buffer pointer  */
    /* double *tw;         twiddle pointer         */
    /* double *tw_w;       twiddle work            */
    fft_param_u u,
    void *edmaState
)
{
    /* input parameters */
    int   N            = u.dp_1d_r2c_e.N;            /* signal size                    */
    int   M            = u.dp_1d_r2c_e.para1.M;      /* FFT size M       */
    int   rad          = u.dp_1d_r2c_e.para1.rad;    /* radix                          */
    int   twsize       = u.dp_1d_r2c_e.para1.twsize; /* twiddle size in double words   */
    int   N_p2         = u.dp_1d_r2c_e.para1.N_p2;   /* power of 2 size in mixed radix */
    int   s_r3         = u.dp_1d_r2c_e.para1.s_r3;   /* number stages of radix 3       */
    int   s_r5         = u.dp_1d_r2c_e.para1.s_r5;   /* number stages of radix 5       */
    int   Bnsize       = u.dp_1d_r2c_e.para1.Bnsize; /* Bn size in double words        */
    
    double *data       = u.dp_1d_r2c_e.data;         /* data pointer                   */
    double *data_w     = u.dp_1d_r2c_e.data_w;       /* data internal pointer          */
    double *work_w     = u.dp_1d_r2c_e.work_w;       /* working buffer pointer         */
    double *tw         = u.dp_1d_r2c_e.tw;           /* twiddle pointer                */
    double *tw_w       = u.dp_1d_r2c_e.tw_w;         /* twiddle working buffer pointer */
    double *Bn         = u.dp_1d_r2c_e.Bn;           /* Bn buffer pointer              */
    double *Bn_w       = u.dp_1d_r2c_e.Bn_w;         /* Bn working buffer pointer      */
    double *workbuf    = u.dp_1d_r2c_e.workbuf;      /* workbuffer pointer for BS      */

    int   error = FFT_NOERR;
    int   insize  = N*sizeof(double),
          outsize = N*2*sizeof(double);

    FFT_EDMA_Struct  *edma = (FFT_EDMA_Struct *)edmaState;

    if (tw_w != NULL) {
      /* prepare twiddle buffer */
      EdmaMgr_copy1D1D(
        edma->channel[FFT_XFER_CHANNEL3], 
        (void *)tw,
        (void *)tw_w,
        twsize*sizeof(double));
      tw = tw_w;
    }
    
    if (Bn_w != NULL) {
      /* prepare twiddle buffer */
      EdmaMgr_copy1D1D(
        edma->channel[FFT_XFER_CHANNEL2], 
        (void *)Bn,
        (void *)Bn_w,
        Bnsize*sizeof(double));
      Bn = Bn_w;
    }

    /* prepare FFT input buffer */
    EdmaMgr_copy1D1D( 
      edma->channel[FFT_XFER_CHANNEL0], 
      (void *)data,
      (void *)data_w,
      insize);

    /* wait for first FFT input */
    EdmaMgr_wait(edma->channel[FFT_XFER_CHANNEL0]);

    if (tw != NULL) {
      EdmaMgr_wait(edma->channel[FFT_XFER_CHANNEL3]);
    }
    if (Bn != NULL) {
      EdmaMgr_wait(edma->channel[FFT_XFER_CHANNEL2]);
    }
    
    if (N == M) {
      if (s_r3 == 0 && s_r5 == 0) {
        DSPF_dp_fftDPxDP_r2c (N, data_w, tw, work_w, rad, 0, N);
      } else {
        fft_DPxDP_mixed_radix_r2c (N, data_w, tw, work_w, N_p2, s_r3, s_r5, rad);
      }
    }
    else {
      fft_DPxDP_r2c_bs (N, M, rad, data_w, work_w, tw,  Bn, workbuf);
    }
    
    /* export output data of first FFT */
    EdmaMgr_copy1D1D( 
      edma->channel[FFT_XFER_CHANNEL1], 
      (void *)work_w,
      (void *)data,
      outsize);

    EdmaMgr_wait(edma->channel[FFT_XFER_CHANNEL1]);

    return error;
} /* fft_dp_1d_r2c_ecpy */

int fft_dp_1d_r2c_4step_ecpy (
    /* int    N;        FFT size                         */
    /* int    n1;       stage1 FFT size                  */
    /* int    n2;       stage2 FFT size                  */
    /* double *data;    data pointer                     */
    /* double *work;    working buffer pointer           */
    /* double *data_w;  data internal pointer            */
    /* double *work_w;  working buffer internal pointer  */
    /* double *tw;      twiddle pointer                  */
    /* double *tw_w;    twiddle internal pointer         */
    fft_param_u u,
    void *edmaState
)
{
    /* input parameters */
    int    N1       = u.dp_1d_r2c_e.n1;           /* stage 1 size                     */
    int    N2       = u.dp_1d_r2c_e.n2;           /* stage 2 size                     */
    int    rad1     = u.dp_1d_r2c_e.para1.rad;    /* stage 1 radix                    */
    int    rad2     = u.dp_1d_r2c_e.para2.rad;    /* stage 2 radix                    */
    int   N_p2      = u.dp_1d_r2c_e.para1.N_p2;   /* power of 2 size in mixed radix   */
    int   s_r3      = u.dp_1d_r2c_e.para1.s_r3;   /* number stages of radix 3         */
    int   s_r5      = u.dp_1d_r2c_e.para1.s_r5;   /* number stages of radix 5         */
    int   tw_offset = u.dp_1d_r2c_e.para1.twsize; /* Offset needed for twiddle ptr    */
    
    double *data    = u.dp_1d_r2c_e.data;         /* data pointer                     */
    double *work    = u.dp_1d_r2c_e.work;         /* working buffer pointer           */
    double *data_w  = u.dp_1d_r2c_e.data_w;       /* data internal pointer            */
    double *work_w  = u.dp_1d_r2c_e.work_w;       /* working buffer internal pointer  */
    double *work_t  = u.dp_1d_r2c_e.work_t;       /* temp buffer internal pointer     */
    double *tw      = u.dp_1d_r2c_e.tw;           /* twiddle pointer                  */
    double *tw_w    = u.dp_1d_r2c_e.tw_w;         /* twiddle working buffer pointer   */

    FFT_EDMA_Struct  *edma = (FFT_EDMA_Struct *)edmaState;
    int   error = FFT_NOERR, num_of_lines = FFT_DP_1D_R2C_NUMOFLINEBUFS;
    int   twsize = (u.dp_1d_r2c_e.para1.twsize+u.dp_1d_r2c_e.para2.twsize)*sizeof(double);
    int   i,j,ch_i,ch_o,ch_i1,ch_o1,dum0,dum1;
    int   insize    = N1*sizeof(double),
          outsample = N1,
          linesize  = N2*sizeof(double);

    double *restrict in_ptr = data, *restrict out_ptr = work, *restrict tempout;
    double *restrict input, *restrict output, *restrict in1, *restrict out1, *dum2, *dum3;
    double *restrict tw1, *restrict tw2;
    double temp_r, temp_i;
    double twid_r, twid_i, tw_cos, tw_sin, temp;

    if (tw_w != NULL) {
      /* prepare twiddle buffer */
      EdmaMgr_copy1D1D(
        edma->channel[FFT_XFER_CHANNEL3], 
        (void *)tw,
        (void *)tw_w,
        twsize);
      tw  = tw_w;
    }
    tw1 = tw + tw_offset + 2*N2;
    tw2 = tw + tw_offset;

    _nassert(N1 >= 8);
    _nassert(N2 >= 8);

    /* stage 1: transpose, n2 FFT's of size n1, apply global twiddle, transpose */
    /* prepare first FFT input buffer */
    EdmaMgr_copy2D1D(
      edma->channel[FFT_XFER_CHANNEL0],
      (void *)in_ptr,
      (void *)data_w,
      sizeof(double),
      outsample,
      linesize);
    in_ptr += 1;

    /* set pingpong buffer pointers */
    in1    = data_w;
    out1   = work_w;
    input  = &data_w[N1];
    output = &work_w[2*N1];

    /* prepare second FFT input buffer */
    EdmaMgr_copy2D1D(
      edma->channel[FFT_XFER_CHANNEL2],
      (void *)in_ptr,
      (void *)input,
      sizeof(double),
      outsample,
      linesize);
    in_ptr += 1;

    /* wait for first FFT input */
    if (tw_w != NULL) {
      EdmaMgr_wait(edma->channel[FFT_XFER_CHANNEL3]);
    }
    EdmaMgr_wait(edma->channel[FFT_XFER_CHANNEL0]);

    if (s_r3 == 0 && s_r5 == 0) {
      DSPF_dp_fftDPxDP_r2c (N1, in1, tw, out1, rad1, 0, N1);
    } else {
      fft_DPxDP_mixed_radix_r2c (N1, in1, tw, out1, N_p2, s_r3, s_r5, rad1);
    }

    /* export output data of first FFT, transposed */
    EdmaMgr_copy1D2D(
      edma->channel[FFT_XFER_CHANNEL1],
      (void *)out1,
      (void *)out_ptr,
      2*sizeof(double),
      outsample,
      2*linesize);
    out_ptr += 2;

    /* prepare third FFT input buffer */
    EdmaMgr_copy2D1D(
      edma->channel[FFT_XFER_CHANNEL0],
      (void *)in_ptr,
      (void *)data_w,
      sizeof(double),
      outsample,
      linesize);
    in_ptr += 1;

    /* set channel numbers */
    ch_i  = FFT_XFER_CHANNEL2;
    ch_o  = FFT_XFER_CHANNEL3;
    ch_i1 = FFT_XFER_CHANNEL0;
    ch_o1 = FFT_XFER_CHANNEL1;

    /* wait for EDMA */
    EdmaMgr_wait(edma->channel[ch_i]);

    for (i = 1; i < N2; i++) {

      if (s_r3 == 0 && s_r5 == 0) {
        DSPF_dp_fftDPxDP_r2c (N1, input, tw, output, rad1, 0, N1);
      } else {
        fft_DPxDP_mixed_radix_r2c (N1, input, tw, output, N_p2, s_r3, s_r5, rad1);
      }      
      tw_cos = tw2[2*i-2];
      tw_sin = tw2[2*i-1];
      twid_r = tw_cos;
      twid_i = tw_sin;
      for (j = 2; j < 2*N1; j+=2) {
          temp_r = output[j];
          temp_i = output[j+1];
          output[j]   = temp_r*twid_r - temp_i*twid_i;
          output[j+1] = temp_r*twid_i + temp_i*twid_r;
          temp   = tw_cos*twid_r - tw_sin*twid_i;
          twid_i = tw_sin*twid_r + tw_cos*twid_i;
          twid_r = temp;
      }

      /* export output data, transposed */
      EdmaMgr_copy1D2D(
        edma->channel[ch_o],
        (void *)output,
        (void *)out_ptr,
        2*sizeof(double),
        outsample,
        2*linesize);
      out_ptr += 2;

      /* prepare input data */
      EdmaMgr_copy2D1D(
        edma->channel[ch_i],
        (void *)in_ptr,
        (void *)input,
        sizeof(double),
        outsample,
        linesize);
      in_ptr += 1;

      /* swap pingpong buffer pointers and XFER channel */
      dum2   = input;
      input  = in1;
      in1    = dum2;
      dum3   = output;
      output = out1;
      out1   = dum3;

      dum0   = ch_i;
      ch_i   = ch_i1;
      ch_i1  = dum0;
      dum1   = ch_o;
      ch_o   = ch_o1;
      ch_o1  = dum1;

      /* wait for EDMA */
      EdmaMgr_wait(edma->channel[ch_i]);
      EdmaMgr_wait(edma->channel[ch_o]);
    }

    /* wait for stage1 to complete */
    EdmaMgr_wait(edma->channel[ch_i1]);
    EdmaMgr_wait(edma->channel[ch_o1]);

    /* stage 2: n1 FFT's of size n2, transpose */
    insize    = N2*2*sizeof(double)*num_of_lines;
    outsample = N2;
    linesize  = N1*2*sizeof(double);
    in_ptr    = work;
    out_ptr   = data;
    N_p2      = u.dp_1d_r2c_e.para2.N_p2;   /* power of 2 size in mixed radix   */
    s_r3      = u.dp_1d_r2c_e.para2.s_r3;   /* number stages of radix 3         */
    s_r5      = u.dp_1d_r2c_e.para2.s_r5;   /* number stages of radix 5         */    

    /* prepare first FFT input buffer */
    EdmaMgr_copy1D1D(
      edma->channel[FFT_XFER_CHANNEL0],
      (void *)in_ptr,
      (void *)work_w,
      insize);
    in_ptr += 2*N2*num_of_lines;

    /* set pingpong buffer pointers */
    in1    = work_w;
    out1   = data_w;
    input  = &work_w[2*N2*num_of_lines];
    output = &data_w[2*N2*num_of_lines];
    tempout= work_t;

    /* prepare second FFT input buffer */
    EdmaMgr_copy1D1D(
      edma->channel[FFT_XFER_CHANNEL2],
      (void *)in_ptr,
      (void *)input,
      insize);
    in_ptr += 2*N2*num_of_lines;

    /* wait for first FFT input */
    EdmaMgr_wait(edma->channel[FFT_XFER_CHANNEL0]);

#if (FFT_DP_1D_R2C_NUMOFLINEBUFS>1)
      if (s_r3 == 0 && s_r5 == 0) {
        for (i=0; i<num_of_lines; i++){
          DSPF_dp_fftDPxDP (N2, &in1[i*N2*2], tw1, &tempout[i*N2*2], rad2, 0, N2);
        }
      }
      else {
        for (i=0; i<num_of_lines; i++){
          fft_DPxDP_mixed_radix (N2, &in1[i*N2*2], tw1, &tempout[i*N2*2], N_p2, s_r3, s_r5, rad2);
        }
      }      
  
    DSPF_dp_mat_trans_cplx(tempout, num_of_lines, N2, out1);
#else
    if (s_r3 == 0 && s_r5 == 0) {
      DSPF_dp_fftDPxDP (N2, in1, tw1, out1, rad2, 0, N2);
    }
    else {
      fft_DPxDP_mixed_radix (N, in1, tw1, out1, N_p2, s_r3, s_r5, rad2);
    }
#endif  

    /* export output data of first FFT, transposed */
    EdmaMgr_copy1D2D(
      edma->channel[FFT_XFER_CHANNEL1],
      (void *)out1,
      (void *)out_ptr,
      2*sizeof(double)*num_of_lines,
      outsample,
      linesize);
    out_ptr += 2*num_of_lines;

    /* prepare third FFT input buffer */
    EdmaMgr_copy1D1D(
      edma->channel[FFT_XFER_CHANNEL0],
      (void *)in_ptr,
      (void *)work_w,
      insize);
    in_ptr += 2*N2*num_of_lines;

    /* set channel numbers */
    ch_i  = FFT_XFER_CHANNEL2;
    ch_o  = FFT_XFER_CHANNEL3;
    ch_i1 = FFT_XFER_CHANNEL0;
    ch_o1 = FFT_XFER_CHANNEL1;

    /* wait for EDMA */
    EdmaMgr_wait(edma->channel[ch_i]);

    for (j = 1; j < N1/num_of_lines; j++) {

#if (FFT_DP_1D_R2C_NUMOFLINEBUFS>1)
      if (s_r3 == 0 && s_r5 == 0) {
        for (i=0; i<num_of_lines; i++){      
          DSPF_dp_fftDPxDP (N2, &input[i*N2*2], tw1, &tempout[i*N2*2], rad2, 0, N2);
        }
      }
      else {
        for (i=0; i<num_of_lines; i++){      
          fft_DPxDP_mixed_radix (N2, &input[i*N2*2], tw1, &tempout[i*N2*2], N_p2, s_r3, s_r5, rad2);
        }
      }
  
    DSPF_dp_mat_trans_cplx(tempout, num_of_lines, N2, output);
#else
    if (s_r3 == 0 && s_r5 == 0) {
      DSPF_dp_fftDPxDP (N2, input, tw1, output, rad2, 0, N2);
    }
    else {
      fft_DPxDP_mixed_radix (N, input, tw1, output, N_p2, s_r3, s_r5, rad2);
    }
#endif  

      /* export output data, transposed */
      EdmaMgr_copy1D2D(
        edma->channel[ch_o],
        (void *)output,
        (void *)out_ptr,
        2*sizeof(double)*num_of_lines,
        outsample,
        linesize);
      out_ptr += 2*num_of_lines;

      /* prepare input data */
      EdmaMgr_copy1D1D(
        edma->channel[ch_i],
        (void *)in_ptr,
        (void *)input,
        insize);
      in_ptr += 2*N2*num_of_lines;

      /* swap pingpong buffer pointers and XFER channel */
      dum2   = input;
      input  = in1;
      in1    = dum2;
      dum3   = output;
      output = out1;
      out1   = dum3;

      dum0   = ch_i;
      ch_i   = ch_i1;
      ch_i1  = dum0;
      dum1   = ch_o;
      ch_o   = ch_o1;
      ch_o1  = dum1;

      /* wait for EDMA */
      EdmaMgr_wait(edma->channel[ch_i]);
      EdmaMgr_wait(edma->channel[ch_o]);
    }

    /* wait for stage1 to complete */
    EdmaMgr_wait(edma->channel[ch_i1]);
    EdmaMgr_wait(edma->channel[ch_o1]);

    return error;
} /* fft_dp_2d_r2c_4step_ecpy */

/* ======================================================================== */
/*  End of file:  fft_dp_1d_r2c.c                                           */
/* ------------------------------------------------------------------------ */
/*            Copyright (c) 2013 Texas Instruments, Incorporated.           */
/*                           All Rights Reserved.                           */
/* ======================================================================== */
