/* ======================================================================= */
/*  TEXAS INSTRUMENTS, INC.                                                */
/*                                                                         */
/*  FFTLIB  FFT Library                                                    */
/*                                                                         */
/* Copyright (C) 2013 Texas Instruments Incorporated - http://www.ti.com/  */ 
/*                                                                         */
/*                                                                         */
/*  Redistribution and use in source and binary forms, with or without     */
/*  modification, are permitted provided that the following conditions     */
/*  are met:                                                               */
/*                                                                         */
/*    Redistributions of source code must retain the above copyright       */
/*    notice, this list of conditions and the following disclaimer.        */
/*                                                                         */
/*    Redistributions in binary form must reproduce the above copyright    */
/*    notice, this list of conditions and the following disclaimer in the  */
/*    documentation and/or other materials provided with the               */
/*    distribution.                                                        */
/*                                                                         */
/*    Neither the name of Texas Instruments Incorporated nor the names of  */
/*    its contributors may be used to endorse or promote products derived  */
/*    from this software without specific prior written permission.        */
/*                                                                         */
/*  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS    */
/*  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT      */
/*  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR  */
/*  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT   */
/*  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,  */
/*  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT       */
/*  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,  */
/*  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY  */
/*  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT    */
/*  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE  */
/*  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.   */
/*                                                                         */
/* ======================================================================= */

#include <ti/fftlib/src/common/fft_common.h>

#ifndef IFFT_SP_1D_C2R_BATCH_H_
#define IFFT_SP_1D_C2R_BATCH_H_ 1

/** @ingroup 1D */
/* @{ */

/** @defgroup IFFT_SP_1D_C2R_BATCH */
/** @ingroup IFFT_SP_1D_C2R_BATCH */
/* @{ */

 /**
 *     This program initializes one 1-dimensional single-precision complex-to-real batched IFFT.
 *  
 *     @param N = IFFT size      
 *     @param mode = only FFT_ECPY is supported
 *     @param fxns = call out functions used to allocated/release memory and EDMA resource
 *     @param p = return plan structure
 *     @param in = input pointer    
 *     @param out = output pointer   
 *     @param tw = twiddle pointer  
 *     @param workbuf = Bluestein scratch pointer
 * 
 * @par Algorithm:
 *      Algorithm supports radix values are 2, 3, 4 and 5.<br>
 *
 * @par Assumptions:
 *      Batch size is at least 8.<br>
 *      N is a positive value.<br>
 *      N is multiple of 8.<br>
 *      All input, output and working buffers are float word aligned.<br>
 *      No input, working or buffers overlap.<br>
 *
 * @par Size Limitations:
 *      16384 for large memory model
 *      8129 for medium memory model
 *      4096 for small memory model
 *
 * @par Implementation notes:
 * @b Endian Support: The code supports little endian modes.  <BR> 
 * @b Interruptibility: The code is interrupt-tolerant but not interruptible. <BR>
 *  
 */

/* IFFT plan 1D SP batch API */
void ifft_sp_plan_1d_c2r_batch (
    int   N,        /* IFFT size */
    int   mode,     /* FFT_ECPY */
    fft_callout_t fxns,
    fft_plan_t *p,
    float *in,
    float *out,
    float *tw,
    float *work
);
 
 /**
 *
 *     This program uses info saved from plan function to execute 1-dimensional single-precision complex-to-real batched IFFT.
 *  
 *     @param u = FFT parameter from plan structure      
 *     @param edmastate = EDMA channel allocation
 *     @param actualCoreNum = run-time configured number of cores
 *     @param return value = error status, FFT_NOERR if success
 *
 */

/* IFFT execute 1D SP ecpy mode batch API */
int ifft_sp_1d_c2r_batch_ecpy (
    fft_param_u u,
    void *edmaState,
    int   actualCoreNum
);
 
 /* }@ */ /* ingroup */
 /* }@ */ /* ingroup */

#endif /* IFFT_SP_1D_C2R_BATCH_H_ */

/* ======================================================================== */
/*  End of file:  ifft_sp_1d_c2r_batch.h                                    */
/* ------------------------------------------------------------------------ */
/*            Copyright (c) 2013 Texas Instruments, Incorporated.           */
/*                           All Rights Reserved.                           */
/* ======================================================================== */


