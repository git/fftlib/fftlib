/* ======================================================================= */
/*  TEXAS INSTRUMENTS, INC.                                                */
/*                                                                         */
/*  FFTLIB  FFT Library                                                    */
/*                                                                         */
/* Copyright (C) 2013 Texas Instruments Incorporated - http://www.ti.com/  */ 
/*                                                                         */
/*                                                                         */
/*  Redistribution and use in source and binary forms, with or without     */
/*  modification, are permitted provided that the following conditions     */
/*  are met:                                                               */
/*                                                                         */
/*    Redistributions of source code must retain the above copyright       */
/*    notice, this list of conditions and the following disclaimer.        */
/*                                                                         */
/*    Redistributions in binary form must reproduce the above copyright    */
/*    notice, this list of conditions and the following disclaimer in the  */
/*    documentation and/or other materials provided with the               */
/*    distribution.                                                        */
/*                                                                         */
/*    Neither the name of Texas Instruments Incorporated nor the names of  */
/*    its contributors may be used to endorse or promote products derived  */
/*    from this software without specific prior written permission.        */
/*                                                                         */
/*  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS    */
/*  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT      */
/*  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR  */
/*  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT   */
/*  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,  */
/*  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT       */
/*  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,  */
/*  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY  */
/*  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT    */
/*  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE  */
/*  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.   */
/*                                                                         */
/* ======================================================================= */

#include <ti/fftlib/src/common/fft_common.h>

#ifndef IFFT_DP_1D_C2C_H_
#define IFFT_DP_1D_C2C_H_ 1

/** @ingroup 1D */
/* @{ */

/** @defgroup IFFT_DP_1D_C2C */
/** @ingroup IFFT_DP_1D_C2C */
/* @{ */

#define IFFT_DP_1D_C2C_4STEP_MIN_SIZE  (   4*1024)
#define IFFT_DP_1D_C2C_4STEP_MAX_SIZE  (1024*1024)

/**
 *     @param IFFT_DP_1D_C2C_NUMOFLINEBUFS = number of cache lines to process each iteration
 */
 
#define IFFT_DP_1D_C2C_NUMOFLINEBUFS 2

/**
 *
 *     @param N = FFT size dimension 1        
 *     @param mode = FFT_DIRECT & FFT_ECPY are supported for 1d IFFT
 */
 
/* IFFT plan 1D DP API */
fft_plan_t ifft_dp_plan_1d_c2c (
    int   N,        /* IFFT size */
    int   mode,     /* FFT_DIRECT or FFT_ECPY */
    fft_callout_t fxns
);

 /**
 *     This program performs one 1-dimensional double-precision complex-to-complex IFFT.
 *  
 *     @param N = FFT size      
 *     @param para1 = Twiddle, mixed radix & bluestein parameters
 *     @param in = input pointer    
 *     @param out = output pointer   
 *     @param tw = twiddle pointer  
 *     @param Bn = Bluestein pointer  
 *     @param workbuf = Bluestein scratch pointer
 * 
 * @par Algorithm:
 *      Algorithm can run in anysize or power of two mode based upon values of N1 & N2. Supported radix
 *      values are 2, 3, 4, & 5, and Bluestein for all other sizes.<br><b>Note:</b>Parameters 
 *      <tt>para1</tt> & <tt>para2</tt> are determined at runtime and will depend upon type of FFT
 *      desired, radix 3 & 5, radix 2 & 4, or bluestein.
 *
 * @par Assumptions:
 *      N is a positive value.<br>
 *      All input, output and working buffers are double word aligned.<br>
 *      No input, working or buffers overlap.<br>
 *
 * @par Size Limitations:
 *      Current limitations are: 1024 IFFT size with bluestein, 2048 IFFT size for power of 2.
 *
 * @par Implementation notes:
 * @b Endian Support: The code supports little endian modes.  <BR> 
 * @b Interruptibility: The code is interrupt-tolerant but not interruptible. <BR>
 *  
 */
 
/* IFFT execute 1D DP direct mode API */
int ifft_dp_1d_c2c_direct (
    /* int     N;       IFFT size        */
    /* double *in;      input pointer    */
    /* double *out;     output pointer   */
    /* double *tw;      twiddle pointer  */
    fft_param_u u,
    void  *edmaState
);

 /**
 *     This program performs one 1-dimensional double-precision complex-to-complex IFFT.
 *  
 *     @param N = FFT size      
 *     @param para1 = Twiddle, mixed radix & bluestein parameters
 *     @param data = input pointer    
 *     @param work = output pointer   
 *     @param data_w = input/output buffer pointer    
 *     @param work_w = working buffer pointer  
 *     @param tw = twiddle pointer  
 *     @param tw_w = twiddle buffer pointer
 *     @param Bn = Bluestein pointer  
 *     @param Bn_w = Bluestein buffer pointer
 *     @param workbuf = Bluestein scratch pointer
 * 
 * @par Algorithm:
 *      Algorithm can run in anysize or power of two mode based upon values of N1 & N2. Supported radix
 *      values are 2, 3, 4, & 5, and Bluestein for all other sizes.<br><b>Note:</b>Parameters 
 *      <tt>para1</tt> & <tt>para2</tt> are determined at runtime and will depend upon type of FFT
 *      desired, radix 3 & 5, radix 2 & 4, or bluestein.
 *
 * @par Assumptions:
 *      N is a positive value.<br>
 *      All input, output and working buffers are double word aligned.<br>
 *      No input, working or buffers overlap.<br>
 *
 * @par Size Limitations:
 *      Current limitations are: 1024 IFFT size with bluestein, 2048 IFFT size for power of 2.
 *      <br><b>Note:</b> 4_step ecpy will be utilized for IFFT sizes 2048 and above. 
 *
 * @par Implementation notes:
 * @b Endian Support: The code supports little endian modes.  <BR> 
 * @b Interruptibility: The code is interrupt-tolerant but not interruptible. <BR>
 *  
 */
 
/* IFFT execute 1D DP ecpy mode API */
int ifft_dp_1d_c2c_ecpy (
    /* int     N;       IFFT size                        */
    /* int     n1;      stage1 IFFT size                 */
    /* int     n2;      stage2 IFFT size                 */
    /* double *data;    data pointer                     */
    /* double *work;    working buffer pointer           */
    /* double *data_w;  data internal pointer            */
    /* double *work_w;  working buffer internal pointer  */
    /* double *tw;      twiddle pointer                  */
    /* double *tw_w;    twiddle internal pointer         */
    fft_param_u u,
    void *edmaState
);
 
 /**
 *     This program performs one 1-dimensional double-precision complex-to-complex FFT.
 *  
 *     @param N = FFT size      
 *     @param n1 = FFT size stage 1        
 *     @param n2 = FFT size stage 2
 *     @param para1 = Twiddle, mixed radix & bluestein parameters for stage 1
 *     @param para2 = Twiddle, mixed radix & bluestein parameters for stage 2
 *     @param data = input pointer    
 *     @param work = output pointer   
 *     @param data_w = input/output buffer pointer    
 *     @param work_w = working buffer pointer  
 *     @param work_t = Cache line buffer pointer  
 *     @param tw = twiddle pointer  
 *     @param tw_w = twiddle buffer pointer
 * 
 * @par Algorithm:
 *      Algorithm can run in radix 2, 3, 4 & 5 based upon value of N. <br><b>Note:</b>Parameters 
 *      <tt>para1</tt> & <tt>para2</tt> are determined at runtime and will depend upon type of FFT
 *      desired, radix 3 & 5, or radix 2 & 4.
 *
 * @par Assumptions:
 *      N1 & N2 are a multiple of IFFT_DP_1D_C2C_NUMOFLINEBUFS and positive values.<br>
 *      All input, output and working buffers are double word aligned.<br>
 *      No input, working or buffers overlap.<br>
 *
 * @par Size Limitations:
 *      Current limitations are: 1024*1024 FFT size for power of 2. 
 *
 * @par Implementation notes:
 * @b Endian Support: The code supports little endian modes.  <BR> 
 * @b Interruptibility: The code is interrupt-tolerant but not interruptible. <BR>
 *  
 */
 
/* IFFT execute 1D DP 4-step ecpy mode API */
int ifft_dp_1d_c2c_4step_ecpy (
    /* int     N;       IFFT size                        */
    /* int     n1;      stage1 IFFT size                 */
    /* int     n2;      stage2 IFFT size                 */
    /* double *data;    data pointer                     */
    /* double *work;    working buffer pointer           */
    /* double *data_w;  data internal pointer            */
    /* double *work_w;  working buffer internal pointer  */
    /* double *tw;      twiddle pointer                  */
    /* double *tw_w;    twiddle internal pointer         */
    fft_param_u u,
    void *edmaState
);
 
 /* }@ */ /* ingroup */
 /* }@ */ /* ingroup */
 
#endif /* IFFT_DP_1D_C2C_H_ */

/* ======================================================================== */
/*  End of file:  ifft_dp_1d_c2c.h                                          */
/* ------------------------------------------------------------------------ */
/*            Copyright (c) 2013 Texas Instruments, Incorporated.           */
/*                           All Rights Reserved.                           */
/* ======================================================================== */


