/* ======================================================================= */
/*  TEXAS INSTRUMENTS, INC.                                                */
/*                                                                         */
/*  FFTLIB  FFT Library                                                    */
/*                                                                         */
/* Copyright (C) 2013 Texas Instruments Incorporated - http://www.ti.com/  */ 
/*                                                                         */
/*                                                                         */
/*  Redistribution and use in source and binary forms, with or without     */
/*  modification, are permitted provided that the following conditions     */
/*  are met:                                                               */
/*                                                                         */
/*    Redistributions of source code must retain the above copyright       */
/*    notice, this list of conditions and the following disclaimer.        */
/*                                                                         */
/*    Redistributions in binary form must reproduce the above copyright    */
/*    notice, this list of conditions and the following disclaimer in the  */
/*    documentation and/or other materials provided with the               */
/*    distribution.                                                        */
/*                                                                         */
/*    Neither the name of Texas Instruments Incorporated nor the names of  */
/*    its contributors may be used to endorse or promote products derived  */
/*    from this software without specific prior written permission.        */
/*                                                                         */
/*  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS    */
/*  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT      */
/*  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR  */
/*  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT   */
/*  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,  */
/*  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT       */
/*  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,  */
/*  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY  */
/*  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT    */
/*  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE  */
/*  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.   */
/*                                                                         */
/* ======================================================================= */

#include <math.h>
#include <stdio.h>
#include <c6x.h>
#include <ti/csl/csl_cacheAux.h>
#include <ti/dsplib/src/DSPF_dp_fftDPxDP/DSPF_dp_fftDPxDP.h>
#include "ifft_dp_1d_c2c.h"

/* FFT 1d dp c2c batch memory buffers */
#define IFFT_DP_1D_C2C_EXTERN_X_BUFN      0
#define IFFT_DP_1D_C2C_EXTERN_Y_BUFN      (IFFT_DP_1D_C2C_EXTERN_X_BUFN     +1)
#define IFFT_DP_1D_C2C_EXTERN_W_BUFN      (IFFT_DP_1D_C2C_EXTERN_Y_BUFN     +1)
#define IFFT_DP_1D_C2C_EXTERN_BN_BUFN     (IFFT_DP_1D_C2C_EXTERN_W_BUFN     +1)
#define IFFT_DP_1D_C2C_INTERN_X_BUFN      (IFFT_DP_1D_C2C_EXTERN_BN_BUFN    +1)
#define IFFT_DP_1D_C2C_INTERN_Y_BUFN      (IFFT_DP_1D_C2C_INTERN_X_BUFN     +1)
#define IFFT_DP_1D_C2C_INTERN_W_BUFN      (IFFT_DP_1D_C2C_INTERN_Y_BUFN     +1)
#define IFFT_DP_1D_C2C_INTERN_BN_BUFN     (IFFT_DP_1D_C2C_INTERN_W_BUFN     +1)
#define IFFT_DP_1D_C2C_INTERN_WORK_BUFN   (IFFT_DP_1D_C2C_INTERN_BN_BUFN    +1)
#define IFFT_DP_1D_C2C_INTERN_T_BUFN      (IFFT_DP_1D_C2C_INTERN_WORK_BUFN  +1)
#define IFFT_DP_1D_C2C_NBUFS              (IFFT_DP_1D_C2C_INTERN_T_BUFN  +1)

/* class, log2align, size, base */
static FFTmemBuffer_t IFFT_dp_1d_c2c_Bufs[IFFT_DP_1D_C2C_NBUFS] = {
  {FFT_MEM_CLASS_EXTERNAL, 3, 0, 0},   /* EXTERN_X */
  {FFT_MEM_CLASS_EXTERNAL, 3, 0, 0},   /* EXTERN_Y */
  {FFT_MEM_CLASS_EXTERNAL, 3, 0, 0},   /* EXTERN_W */
  {FFT_MEM_CLASS_EXTERNAL, 3, 0, 0},   /* EXTERN_BN */
  {FFT_MEM_CLASS_INTERNAL, 3, 0, 0},   /* INTERN_X */
  {FFT_MEM_CLASS_INTERNAL, 3, 0, 0},   /* INTERN_Y */
  {FFT_MEM_CLASS_INTERNAL, 3, 0, 0},   /* INTERN_W */
  {FFT_MEM_CLASS_INTERNAL, 3, 0, 0},   /* INTERN_BN   */
  {FFT_MEM_CLASS_INTERNAL, 3, 0, 0},   /* INTERN_WORK */
  {FFT_MEM_CLASS_INTERNAL, 3, 0, 0}    /* INTERN_T */
};

/* calculate radix for 1D array */
static void calculate_rad (int mode, int N, int *n1, int *n2, fft_para_mix_bs_t *fft_para1, 
                    fft_para_mix_bs_t *fft_para2, int *use_bs)
{
  int i, n, j, M, rad;
  int s_r3, s_r5, s_r3_2, s_r5_2, N_p2, dum;

  *use_bs = 1;
  n = (N<<1)-1;
  j = _norm(n);
  M = 1 << (31-j);
  j = _norm(M);

  N_p2 = N;
  s_r3 = 0;
  s_r5 = 0;

  if (mode == FFT_ECPY && N >= IFFT_DP_1D_C2C_4STEP_MIN_SIZE) {
    /* use 4 step ecpy FFT */
    dum = N;
    
    while (dum/3*3 == dum) {
      s_r3++;
      dum /= 3;
    }
    s_r3_2 = s_r3/2;
    s_r3   = s_r3 - s_r3_2;

    while (dum/5*5 == dum) {
      s_r5++;
      dum /= 5;
    }
    s_r5_2 = s_r5/2;
    s_r5   = s_r5 - s_r5_2;
    
    j = 30 - _norm(dum);
    if ((dum >= 32) && (dum == (1 << j))) {
      *use_bs = 0;
      
      *n1 = 1 << (j-j/2);
      fft_para1->N_p2 = *n1;
      for (i = 0; i < s_r3; i++)
        *n1 *= 3;
      for (i = 0; i < s_r5; i++)
        *n1 *= 5;
      
      *n2 = 1 << (j/2);
      fft_para2->N_p2 = *n2;
      for (i = 0; i < s_r3_2; i++)
        *n2 *= 3;
      for (i = 0; i < s_r5_2; i++)
        *n2 *= 5;

      j = _norm(fft_para1->N_p2);
      if (j%2 == 0)
        rad = 4;
      else
        rad = 2;

      fft_para1->s_r3 = s_r3;
      fft_para1->s_r5 = s_r5;
      fft_para1->rad  = rad;

      j = _norm(fft_para2->N_p2);
      if (j%2 == 0)
    	  rad = 4;
      else
    	  rad = 2;
      
      fft_para2->s_r3 = s_r3_2;
      fft_para2->s_r5 = s_r5_2;
      fft_para2->rad  = rad;
    } else {
      //not supported, will trigger a plan failure at this point
      return;
    }

  } else {
    /* use direct or 1-step ecpy mode */
    /* if N is odd, bypass the rest of calculation, use Bluestein */
    if (((N>>1)<<1) == N)
    {
      /* if N is power of 2, no zero padding, regular radix 2 or 4 fft */
      if (N == (M>>1))
      { /* power of 2 */
        M = N;
        j = j+1;
        *use_bs = 0;
      } else {
        dum = N;
        while (dum/3*3 == dum) {
          s_r3++;
          dum /= 3;
        }
        while (dum/5*5 == dum) {
          s_r5++;
          dum /= 5;
        }
        if ((dum >= 4) && (dum == (1 << (30-_norm(dum))))) {
        /* mixed radix 2 and 3 */
          N_p2 = dum;
          *use_bs = 0;
          M = N;
          j  = _norm(dum);
        }
      }
    }
  }

  if (fft_para2 == NULL) {
    /* calculate radix for radix 2 or 4 c2c fft */
    if (j%2 == 0)
      rad = 4;
    else
      rad = 2;
      
    /* ecpy & direct */
    fft_para1->N_p2 = N_p2;
    fft_para1->s_r3 = s_r3;
    fft_para1->s_r5 = s_r5;
    fft_para1->M    = M;
    fft_para1->rad  = rad;
  }
  
} /*calculate_rad ()*/

/* calculate twiddle size, Bn buffer size and workbuffer size */
static void calculate_mem_size(int N, int n_4step, int M, int use_bs, int s_r3, int s_r5, int N_p2, int *twsize, int *Bnsize, int *worksize)
{
  int dum, n, i;

  /* Please note that Bn buffer and Workbuf are only used when doing Bluestein
   * meaning when use_bs == 1 */
  if (use_bs == 1)
  { /* when size is not power of 2 and not multiple of 3&5 */
    *twsize   = (2*M+2*N);
    *Bnsize   = (2*M);
    *worksize = (4*M);
  }
  else
  {
    if (n_4step > 0)
      N = n_4step;
    if ((s_r3 == 0) && (s_r5 == 0)) {
    /* When signal is power of 2 */
      *twsize   = N*2;
    }
    else {
    /* When signal is power of 3 or 5 */
      dum = 0;
      n  = 4*N;
      for (i = 0; i < s_r3; i++) {
        dum += n/3;
        n  /= 3;
      }
      n *= 2;  /* radix-5 has 8 twiddle vs radix-3 4 twiddles */
      for (i = 0; i < s_r5; i++) {
        dum += n/5;
        n  /= 5;
      }
      *twsize = (2*N_p2+dum);
    }
    *Bnsize   = 0;
    *worksize = 0;
  }
} /* calculate_mem_size()*/

/* twiddle generation */
static void twiddle_gen (int dim, int mode, int N, int n1, int n2, int M, int rad, int use_bs, int s_r3, 
                  int s_r5, int N_p2, double *tw, double *Bn, double *work)
{
	int     k, n, j, i;
	double *bn, *Bk;
    const double PI = 3.14159265358979323846;

    k = 0;
    if ( mode == FFT_ECPY && n1 != 0) {     
      /* twiddle_gen for 4-step ecpy - bluestein not supported */
      if (dim == 1)
        n = n1;
      else {
        n = n2;
        
        /* global twiddle generation */
        for (i = 1; i < n; i++) {
          tw[k    ] =  cos (2 * PI * i / N);
          tw[k + 1] =  sin (2 * PI * i / N);
          k += 2;
        }
        k = 2*n;
      }
      
      for (j = 1; j <= N_p2 >> 2; j = j << 2) {
          for (i = 0; i < N_p2 >> 2; i += j) {
              tw[k]     = cos (2 * PI * i / N_p2);
              tw[k + 1] = sin (2 * PI * i / N_p2);
              tw[k + 2] = cos (4 * PI * i / N_p2);
              tw[k + 3] = sin (4 * PI * i / N_p2);
              tw[k + 4] = cos (6 * PI * i / N_p2);
              tw[k + 5] = sin (6 * PI * i / N_p2);
              k += 6;
          }
      }

      if ((s_r3 != 0) || (s_r5 != 0)) {
        /* generate twiddles for radix-3 loop */
        k  = N_p2*2;
        if (dim == 2) k += 2*n2;
        for (i = 0; i < s_r3; i++) {
          for (j = 0; j < n/3; j++) {
            tw[k]     = ( cos (2 * PI * j / n) / 3);
            tw[k + 1] = (-sin (2 * PI * j / n) / 3);
            tw[k + 2] = ( cos (4 * PI * j / n) / 3);
            tw[k + 3] = (-sin (4 * PI * j / n) / 3);
            k += 4;
          }
          n /= 3;
        }
        /* generate twiddles for radix-5 loop */
        for (i = 0; i < s_r5; i++) {
          for (j = 0; j < n/5; j++) {
            tw[k]     = ( cos (2 * PI * j / n) / 5);
            tw[k + 1] = (-sin (2 * PI * j / n) / 5);
            tw[k + 2] = ( cos (4 * PI * j / n) / 5);
            tw[k + 3] = (-sin (4 * PI * j / n) / 5);
            tw[k + 4] = ( cos (6 * PI * j / n) / 5);
            tw[k + 5] = (-sin (6 * PI * j / n) / 5);
            tw[k + 6] = ( cos (8 * PI * j / n) / 5);
            tw[k + 7] = (-sin (8 * PI * j / n) / 5);
            k += 8;
          }
          n /= 5;
        }
      }      
    } else {
      if (use_bs == 0)
      { /* first generate radix 2&4 twiddles */
          for (j = 1; j <= N_p2 >> 2; j = j << 2)
          {
            for (i = 0; i < N_p2 >> 2; i += j)
            {
               tw[k]     = cos (2 * PI * i / N_p2);
               tw[k + 1] = sin (2 * PI * i / N_p2);
               tw[k + 2] = cos (4 * PI * i / N_p2);
               tw[k + 3] = sin (4 * PI * i / N_p2);
               tw[k + 4] = cos (6 * PI * i / N_p2);
               tw[k + 5] = sin (6 * PI * i / N_p2);
               k += 6;
            }
          }

          if ((s_r3 != 0) || (s_r5 != 0)) {
            /* generate twiddles for radix-3 loop */
            k  = N_p2*2;
            n = N;
            for (i = 0; i < s_r3; i++) {
              for (j = 0; j < n/3; j++) {
                tw[k]     = ( cos (2 * PI * j / n) / 3);
                tw[k + 1] = (-sin (2 * PI * j / n) / 3);
                tw[k + 2] = ( cos (4 * PI * j / n) / 3);
                tw[k + 3] = (-sin (4 * PI * j / n) / 3);
                k += 4;
              }
              n /= 3;
            }
            /* generate twiddles for radix-5 loop */
            for (i = 0; i < s_r5; i++) {
              for (j = 0; j < n/5; j++) {
                tw[k]     = ( cos (2 * PI * j / n) / 5);
                tw[k + 1] = (-sin (2 * PI * j / n) / 5);
                tw[k + 2] = ( cos (4 * PI * j / n) / 5);
                tw[k + 3] = (-sin (4 * PI * j / n) / 5);
                tw[k + 4] = ( cos (6 * PI * j / n) / 5);
                tw[k + 5] = (-sin (6 * PI * j / n) / 5);
                tw[k + 6] = ( cos (8 * PI * j / n) / 5);
                tw[k + 7] = (-sin (8 * PI * j / n) / 5);
                k += 8;
              }
              n /= 5;
            }
          }
       }
      else
      { /* generate twiddle for c2c fft */
        /*--------------------------------------
         * TW is organized as follows:
         *   twiddle for fft and ifft, size: 2*M
         *   Bk,                       size: 2*N
         *-------------------------------------*/

        k = 0;

        for (j = 1; j <= M >> 2; j = j << 2) {
          for (i = 0; i < M >> 2; i += j) {
            tw[k]     =  cos (2 * PI * i / M);
            tw[k + 1] =  sin (2 * PI * i / M);
            tw[k + 2] =  cos (4 * PI * i / M);
            tw[k + 3] =  sin (4 * PI * i / M);
            tw[k + 4] =  cos (6 * PI * i / M);
            tw[k + 5] =  sin (6 * PI * i / M);
            k += 6;
          }
        }
        k = 2*M;

        /* phase factor generation */
        for (i = 0; i < N ; i++) {
          tw[k]     =  cos (PI * i * i / N);
          tw[k + 1] = -sin (PI * i * i / N);

          k += 2;
        }

        /* precalculate FFT of bn */

  #pragma MUST_ITERATE(4,,4)
        /* first reset the work buffer to zeros */
        for (i = 0; i < 2*M; i+=1)
        {
          work[i] = 0;
        }

        /* construct bn in work buffer */
        bn = work;
        Bk = tw + 2*M;

        /*---------------------------------------------*
         *         bn(0)  = Bk(0)/N
         *         bn(i)  = Bk(i)/N            0 < i < N
         *         bn(i)  = 0                  N <= i < M-N-1
         *         bn(i)  = Bk(M-i)/N      M-N-1 <= i < M
         *  Note: bn(i) = Re(bn(i)) + i* Im(bn(i));
         ------------------------------------------- */
        bn[0] = Bk[0]/N;
        bn[1] = Bk[1]/N;

        for(i = 1; i < N; i++)
        {
          bn[2*i]   = bn[2*(M-i)]   = Bk[2*i]/N;
          bn[2*i+1] = bn[2*(M-i)+1] = Bk[2*i+1]/N;
        }

        /* FFT of the constructed signal Bn = fft(bn) */
        DSPF_dp_fftDPxDP (M, bn, tw, Bn, rad, 0, M);
      }
    }
} /* twiddle_gen()*/

fft_plan_t ifft_dp_plan_1d_c2c (
    int   N,        /* FFT size */
    int   mode,     /* FFT_DIRECT or FFT_ECPY */
    fft_callout_t fxns
)
{
    int          rad, n1, n2;
    int          M1, M2;
    int          int_tw_size, ext_tw_size, int_Bn_size, ext_Bn_size, work_size;
    int          twsize, Bnsize, worksize;
    int          use_bs = 0;
    int          s_r3, s_r5, N_p2;

    double        *tw;
    double        *restrict Bn, *restrict work;

    fft_plan_t   p;

    /* Determine FFT method to use */
    if (mode == FFT_DIRECT) {
      p.fftfxn = ifft_dp_1d_c2c_direct;
    }
    else {  /* FFT_ECPY */
      if (N < IFFT_DP_1D_C2C_4STEP_MIN_SIZE) {
        p.fftfxn = ifft_dp_1d_c2c_ecpy;
      } else {
        p.fftfxn = ifft_dp_1d_c2c_4step_ecpy;
      }
    }

    p.nbuf = IFFT_DP_1D_C2C_NBUFS;

    memcpy(&p.fftcout, &fxns, sizeof(fft_callout_t));
    memcpy(p.bufs, IFFT_dp_1d_c2c_Bufs, IFFT_DP_1D_C2C_NBUFS*sizeof(FFTmemBuffer_t));

    /* Direct & smaller ECPY only calculate in one dimension */
    if (mode == FFT_DIRECT) {
      calculate_rad(mode, N, 0, 0, &p.u.dp_1d_c2c_d.para1, NULL, &use_bs);
      M1 = p.u.dp_1d_c2c_d.para1.M;
    } else if ((mode == FFT_ECPY) && (N < IFFT_DP_1D_C2C_4STEP_MIN_SIZE)){
        calculate_rad(mode, N, 0, 0, &p.u.dp_1d_c2c_e.para1, NULL, &use_bs);
        M1 = p.u.dp_1d_c2c_e.para1.M;
    } else {
      /* 4step ecpy */
      /* determine the radix for the first & second dimensions */
      calculate_rad(mode, N, &n1, &n2, &p.u.dp_1d_c2c_e.para1, &p.u.dp_1d_c2c_e.para2, &use_bs);
      M1 = p.u.dp_1d_c2c_e.para1.M;
      M2 = p.u.dp_1d_c2c_e.para2.M;
      p.u.dp_1d_c2c_e.n1 = n1;
      p.u.dp_1d_c2c_e.n2 = n2;
      /* not supported, plan failed */
      if (use_bs == 1) {
        return (p);
      }
    }
    
    /* calculate memory requirements */
    p.bufs[IFFT_DP_1D_C2C_EXTERN_X_BUFN].size = sizeof(double)*2*N;
    p.bufs[IFFT_DP_1D_C2C_EXTERN_Y_BUFN].size = sizeof(double)*2*N;
    if (mode != FFT_DIRECT) { /* FFT_ECPY */
      if (N < IFFT_DP_1D_C2C_4STEP_MIN_SIZE) {
        p.bufs[IFFT_DP_1D_C2C_INTERN_X_BUFN].size = sizeof(double)*2*N;
        p.bufs[IFFT_DP_1D_C2C_INTERN_Y_BUFN].size = sizeof(double)*2*N;
        p.bufs[IFFT_DP_1D_C2C_INTERN_T_BUFN].size = sizeof(double)*2*N;
      }
      else {
        p.bufs[IFFT_DP_1D_C2C_INTERN_X_BUFN].size = sizeof(double)*4*n1*IFFT_DP_1D_C2C_NUMOFLINEBUFS;
        p.bufs[IFFT_DP_1D_C2C_INTERN_Y_BUFN].size = sizeof(double)*4*n1*IFFT_DP_1D_C2C_NUMOFLINEBUFS;
        p.bufs[IFFT_DP_1D_C2C_INTERN_T_BUFN].size = sizeof(double)*2*n1*IFFT_DP_1D_C2C_NUMOFLINEBUFS;
      }
    }
    
    if (mode == FFT_DIRECT) {
      /* Calculate mem size for 1 dimension for Direct */
      s_r3 = p.u.dp_1d_c2c_d.para1.s_r3;
      s_r5 = p.u.dp_1d_c2c_d.para1.s_r5;
      N_p2 = p.u.dp_1d_c2c_d.para1.N_p2;
      calculate_mem_size(N, 0, M1, use_bs, s_r3, s_r5, N_p2, &twsize, 
                        &Bnsize, &worksize);
      p.u.dp_1d_c2c_d.para1.twsize = twsize;
      p.u.dp_1d_c2c_d.para1.Bnsize = Bnsize;
      int_tw_size = ext_tw_size = twsize;
      int_Bn_size = ext_Bn_size = Bnsize;
      work_size   = worksize;
    } else if ((mode == FFT_ECPY) && (N < IFFT_DP_1D_C2C_4STEP_MIN_SIZE)){
      /* Calculate mem size for 1 dimension for small ECPY */
      s_r3 = p.u.dp_1d_c2c_e.para1.s_r3;
      s_r5 = p.u.dp_1d_c2c_e.para1.s_r5;
      N_p2 = p.u.dp_1d_c2c_e.para1.N_p2;
      calculate_mem_size(N, 0, M1, use_bs, s_r3, s_r5, N_p2, &twsize, 
                        &Bnsize, &worksize);
      p.u.dp_1d_c2c_e.para1.twsize = twsize;
      p.u.dp_1d_c2c_e.para1.Bnsize = Bnsize;
      int_tw_size = ext_tw_size = twsize;
      int_Bn_size = ext_Bn_size = Bnsize;
      work_size   = worksize;
    } else {
      /* Calculate mem size for 2 dimensions for 4step ECPY */
      /* evaluate the  1st dimension */
      s_r3 = p.u.dp_1d_c2c_e.para1.s_r3;
      s_r5 = p.u.dp_1d_c2c_e.para1.s_r5;
      N_p2 = p.u.dp_1d_c2c_e.para1.N_p2;
      calculate_mem_size(N, n1, M1, 0, s_r3, s_r5, N_p2, &twsize, 
                        &Bnsize, &worksize);
      p.u.dp_1d_c2c_e.para1.twsize = twsize;
      p.u.dp_1d_c2c_e.para1.Bnsize = Bnsize;
      int_tw_size = ext_tw_size = twsize;
      int_Bn_size = ext_Bn_size = Bnsize;
      work_size   = worksize;
    
      /* evaluate the 2nd dimension */
      s_r3 = p.u.dp_1d_c2c_e.para2.s_r3;
      s_r5 = p.u.dp_1d_c2c_e.para2.s_r5;
      N_p2 = p.u.dp_1d_c2c_e.para2.N_p2;
      calculate_mem_size(N, n2, M2, 0, s_r3, s_r5, N_p2, &twsize, 
                        &Bnsize, &worksize);
      p.u.dp_1d_c2c_e.para2.twsize = 2*n2 + twsize;
      p.u.dp_1d_c2c_e.para2.Bnsize = Bnsize;
      int_tw_size  = (int_tw_size > twsize) ? int_tw_size : twsize;
      ext_tw_size += twsize;
      int_Bn_size  = (int_Bn_size > Bnsize) ? int_Bn_size : Bnsize;
      ext_Bn_size += Bnsize;
      work_size    = (work_size > worksize) ? work_size : worksize;
    }
    
    p.bufs[IFFT_DP_1D_C2C_EXTERN_W_BUFN].size    = sizeof(double)*ext_tw_size;
    p.bufs[IFFT_DP_1D_C2C_EXTERN_BN_BUFN].size   = sizeof(double)*ext_Bn_size;
    p.bufs[IFFT_DP_1D_C2C_INTERN_WORK_BUFN].size = sizeof(double)*work_size;
    
    if (mode == FFT_ECPY) {
      p.bufs[IFFT_DP_1D_C2C_INTERN_W_BUFN].size    = sizeof(double)*int_tw_size;
      p.bufs[IFFT_DP_1D_C2C_INTERN_BN_BUFN].size   = sizeof(double)*int_Bn_size;
    }
    
    (*fxns.memoryRequest)(IFFT_DP_1D_C2C_NBUFS, p.bufs);
    
    if (mode == FFT_DIRECT) {
      p.u.dp_1d_c2c_d.N     = N;
      p.u.dp_1d_c2c_d.in    = p.bufs[IFFT_DP_1D_C2C_EXTERN_X_BUFN].base;
      p.u.dp_1d_c2c_d.out   = p.bufs[IFFT_DP_1D_C2C_EXTERN_Y_BUFN].base;
      tw                    = p.bufs[IFFT_DP_1D_C2C_EXTERN_W_BUFN].base;
      p.u.dp_1d_c2c_d.tw    = tw;

      if (use_bs == 1)
      {
        /* if use Bluestein FFT for any dimension, set the buffers to do Bluestein */
        p.u.dp_1d_c2c_d.Bn         = p.bufs[IFFT_DP_1D_C2C_EXTERN_BN_BUFN].base;
        p.u.dp_1d_c2c_d.workbuf    = p.bufs[IFFT_DP_1D_C2C_INTERN_WORK_BUFN].base;
      }
      else
      {
        /* otherwise, set the buffers to NULL */
        p.u.dp_1d_c2c_d.Bn         = NULL;
        p.u.dp_1d_c2c_d.workbuf    = NULL;
      }
    } else {
      p.u.dp_1d_c2c_e.N      = N;
      p.u.dp_1d_c2c_e.data   = p.bufs[IFFT_DP_1D_C2C_EXTERN_X_BUFN].base;
      p.u.dp_1d_c2c_e.work   = p.bufs[IFFT_DP_1D_C2C_EXTERN_Y_BUFN].base;
      p.u.dp_1d_c2c_e.data_w = p.bufs[IFFT_DP_1D_C2C_INTERN_X_BUFN].base;
      p.u.dp_1d_c2c_e.work_w = p.bufs[IFFT_DP_1D_C2C_INTERN_Y_BUFN].base;
      p.u.dp_1d_c2c_e.work_t = p.bufs[IFFT_DP_1D_C2C_INTERN_T_BUFN].base;
      p.u.dp_1d_c2c_e.tw_w   = p.bufs[IFFT_DP_1D_C2C_INTERN_W_BUFN].base;
      tw                     = p.bufs[IFFT_DP_1D_C2C_EXTERN_W_BUFN].base;
      p.u.dp_1d_c2c_e.tw     = tw;
    
      if (use_bs == 1)
      {
        /* if use Bluestein FFT for any dimension, set the buffers to do Bluestein */
        p.u.dp_1d_c2c_e.Bn         = p.bufs[IFFT_DP_1D_C2C_EXTERN_BN_BUFN].base;
        p.u.dp_1d_c2c_e.Bn_w       = p.bufs[IFFT_DP_1D_C2C_INTERN_BN_BUFN].base;
        p.u.dp_1d_c2c_e.workbuf    = p.bufs[IFFT_DP_1D_C2C_INTERN_WORK_BUFN].base;
      }
      else
      {
        /* otherwise, set the buffers to NULL */
        p.u.dp_1d_c2c_e.Bn         = NULL;
        p.u.dp_1d_c2c_e.Bn_w       = NULL;
        p.u.dp_1d_c2c_e.workbuf    = NULL;
      }
    }

    /* generate twiddle for the c2c fft*/
    if (mode == FFT_DIRECT) {
      Bn   = p.u.dp_1d_c2c_d.Bn;
      work = p.u.dp_1d_c2c_d.workbuf;
      s_r3 = p.u.dp_1d_c2c_d.para1.s_r3;
      s_r5 = p.u.dp_1d_c2c_d.para1.s_r5;
      N_p2 = p.u.dp_1d_c2c_d.para1.N_p2;
      rad  = p.u.dp_1d_c2c_d.para1.rad;
      twiddle_gen (1, mode, N, 0, 0, M1, rad, use_bs, s_r3, s_r5, N_p2, tw, Bn, work);
    } else if (mode == FFT_ECPY && N < IFFT_DP_1D_C2C_4STEP_MIN_SIZE) {
      Bn   = p.u.dp_1d_c2c_e.Bn;
      work = p.u.dp_1d_c2c_e.workbuf;
      s_r3 = p.u.dp_1d_c2c_e.para1.s_r3;
      s_r5 = p.u.dp_1d_c2c_e.para1.s_r5;
      N_p2 = p.u.dp_1d_c2c_e.para1.N_p2;
      rad  = p.u.dp_1d_c2c_e.para1.rad;
      twiddle_gen (1, mode, N, 0, 0, M1, rad, use_bs, s_r3, s_r5, N_p2, tw, Bn, work);
    } else {
      Bn   = p.u.dp_1d_c2c_e.Bn;
      work = p.u.dp_1d_c2c_e.workbuf;
      s_r3 = p.u.dp_1d_c2c_e.para1.s_r3;
      s_r5 = p.u.dp_1d_c2c_e.para1.s_r5;
      N_p2 = p.u.dp_1d_c2c_e.para1.N_p2;
      rad  = p.u.dp_1d_c2c_e.para1.rad;
      twiddle_gen (1, mode, N, n1, n2, M1, rad, use_bs, s_r3, s_r5, N_p2, tw, Bn, work);
      
      twsize = p.u.dp_1d_c2c_e.para1.twsize;
      Bn   = p.u.dp_1d_c2c_e.Bn;
      work = p.u.dp_1d_c2c_e.workbuf;
      s_r3 = p.u.dp_1d_c2c_e.para2.s_r3;
      s_r5 = p.u.dp_1d_c2c_e.para2.s_r5;
      N_p2 = p.u.dp_1d_c2c_e.para2.N_p2;
      rad  = p.u.dp_1d_c2c_e.para2.rad;
      twiddle_gen (2, mode, N, n1, n2, M1, rad, use_bs, s_r3, s_r5, N_p2, tw+twsize, Bn, work);
    }

    CACHE_wbInvAllL2(CACHE_WAIT);        
    return (p);
}

/* ======================================================================== */
/*  End of file:  ifft_plan_dp_1d_c2c.c                                      */
/* ------------------------------------------------------------------------ */
/*            Copyright (c) 2013 Texas Instruments, Incorporated.           */
/*                           All Rights Reserved.                           */
/* ======================================================================== */
