/* ======================================================================= */
/*  TEXAS INSTRUMENTS, INC.                                                */
/*                                                                         */
/*  FFTLIB  FFT Library                                                    */
/*                                                                         */
/* Copyright (C) 2013 Texas Instruments Incorporated - http://www.ti.com/  */ 
/*                                                                         */
/*                                                                         */
/*  Redistribution and use in source and binary forms, with or without     */
/*  modification, are permitted provided that the following conditions     */
/*  are met:                                                               */
/*                                                                         */
/*    Redistributions of source code must retain the above copyright       */
/*    notice, this list of conditions and the following disclaimer.        */
/*                                                                         */
/*    Redistributions in binary form must reproduce the above copyright    */
/*    notice, this list of conditions and the following disclaimer in the  */
/*    documentation and/or other materials provided with the               */
/*    distribution.                                                        */
/*                                                                         */
/*    Neither the name of Texas Instruments Incorporated nor the names of  */
/*    its contributors may be used to endorse or promote products derived  */
/*    from this software without specific prior written permission.        */
/*                                                                         */
/*  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS    */
/*  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT      */
/*  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR  */
/*  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT   */
/*  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,  */
/*  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT       */
/*  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,  */
/*  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY  */
/*  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT    */
/*  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE  */
/*  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.   */
/*                                                                         */
/* ======================================================================= */

#include <xdc/std.h>
#include <stdio.h>
#include <time.h>
#include <stdlib.h>
#include <limits.h>
#include <math.h>
#include <c6x.h>

#include "fft_sp_1d_r2c.h"
#include <ti/fftlib/src/common/fft_edma.h>
#include <ti/dsplib/src/DSPF_sp_fftSPxSP/DSPF_sp_fftSPxSP.h>

extern cregister volatile unsigned int DNUM;

#define FFT_EDMA_STATE_INIT         0
#define FFT_EDMA_STATE_ALLOCATED    1

int fftEdmaState;
FFT_EDMA_Struct FFT_EDMA;

/* Function to trap exceptions */
void fft_assert(int statement, int node_id, const char *error)
{
  volatile int dbg_halt = 1;

  if(!statement) {
    printf("%s (%d)\n",error,node_id);
    while(dbg_halt);
  }
}

void *fft_assign_edma_resources(void)
{
  /*
   * The edmaInstances are indexes into the C6678_config[] array defined in
   * fft_c6678_config, which is used to specify how EDMA resources are
   * divided between cores.
   */
  int      ret_val, i;

  if ( fftEdmaState != FFT_EDMA_STATE_ALLOCATED )
  {
    FFT_EDMA.num_channels = 0;

    while ( FFT_EDMA.num_channels < FFT_NUM_EDMA_CH )
    {
      FFT_EDMA.channel[FFT_EDMA.num_channels] = EdmaMgr_alloc(FFT_MAX_EDMA_LINKS);
    
      fft_assert( (FFT_EDMA.channel[FFT_EDMA.num_channels] != NULL), 0, "EdmaMgr_alloc() failed ");

      FFT_EDMA.num_channels++;
    }
  }
  fftEdmaState = FFT_EDMA_STATE_ALLOCATED;

  return ((void *)&FFT_EDMA);
}

void fft_free_edma_resources(void *edma)
{
  FFT_EDMA_Struct *edmaState = (FFT_EDMA_Struct *)edma;
  /*
   * The edmaInstances are indexes into the C6678_config[] array defined in
   * fft_c6678_config, which is used to specify how EDMA resources are
   * divided between cores.
   */
  int      ret_val;

  fft_assert( (edma == &FFT_EDMA), 0, "Did not get correct edma state struct.");
}

void tw_gen_cn (float *w, int n);
void dft (int N, float x[], float y[], int N1);

/* ======================================================================== */
/*  Kernel-specific alignments                                              */
/* ======================================================================== */
#pragma DATA_SECTION(x_i,  ".mem_ddr");
#pragma DATA_SECTION(y_i,  ".mem_ddr");
#pragma DATA_SECTION(w_i,  ".mem_ddr");
#pragma DATA_SECTION(x_cn, ".mem_ddr");
#pragma DATA_SECTION(y_cn, ".mem_ddr");
#pragma DATA_SECTION(w_cn, ".mem_ddr");
#pragma DATA_SECTION(Bn_i, ".mem_ddr");

#pragma DATA_ALIGN(x_i,  8);
#pragma DATA_ALIGN(x_cn, 8);

#pragma DATA_ALIGN(w_i,  8);
#pragma DATA_ALIGN(Bn_i, 8);
#pragma DATA_ALIGN(w_cn, 8);

#pragma DATA_ALIGN(y_i,  8);
#pragma DATA_ALIGN(y_cn, 8);

#pragma DATA_SECTION(x_i_work, ".mem_l2");
#pragma DATA_SECTION(y_i_work, ".mem_l2");
#pragma DATA_SECTION(y_i_temp, ".mem_l2");
#pragma DATA_SECTION(Bn_i_work,".mem_l2");
#pragma DATA_SECTION(workbuf,  ".mem_l2");

#pragma DATA_ALIGN(x_i_work,  64);
#pragma DATA_ALIGN(y_i_work,  64);
#pragma DATA_ALIGN(y_i_temp,  64);
#pragma DATA_ALIGN(Bn_i_work, 8);
#pragma DATA_ALIGN(workbuf,   8);

/* ======================================================================== */
/*  Parameters of fixed dataset.                                            */
/* ======================================================================== */

#define MAXN  (2048*2048)
#define M     (2*MAXN)
#define M_i   (4*2048)
#define PAD   (0)

/* ======================================================================== */
/*  Initialized arrays with fixed test data.                                */
/* ======================================================================== */

float x_i [M + 2 * PAD];
float x_cn[M + 2 * PAD];

float Bn_i[4*MAXN + 2 * PAD];
float Bn_i_work[4*1024 + 2 * PAD];
float workbuf  [8*1024 + 2 * PAD];

float y_i [M + 2 * PAD];
float y_cn[M + 2 * PAD];

float x_i_work [M_i*2*FFT_SP_1D_R2C_NUMOFLINEBUFS + 2 * PAD];
float y_i_work [M_i*2*FFT_SP_1D_R2C_NUMOFLINEBUFS + 2 * PAD];
float y_i_temp [M_i*FFT_SP_1D_R2C_NUMOFLINEBUFS + 2 * PAD];

float w_i      [4*2048 + 2 * PAD];
float w_i_work [4*2048 + 2 * PAD];
float w_cn[M + 2 * PAD];

/* ======================================================================== */
/*  Generate pointers to skip beyond array padding                          */
/* ======================================================================== */
float *const ptr_x_i  = x_i  + PAD;
float *const ptr_x_cn = x_cn + PAD;

float *const ptr_w_i  = w_i  + PAD;
float *const ptr_w_cn = w_cn + PAD;
float *const ptr_Bn_i = Bn_i  + PAD;

float *const ptr_y_i  = y_i  + PAD;
float *const ptr_y_cn = y_cn + PAD;

float *const ptr_y_i_temp  = y_i_temp + PAD;
float *const ptr_y_i_work  = y_i_work + PAD;
float *const ptr_x_i_work  = x_i_work + PAD;
float *const ptr_w_i_work  = w_i_work + PAD;
float *const ptr_Bn_i_work = Bn_i_work + PAD;
float *const ptr_work      = workbuf + PAD;

void fft_memory_request (int nbufs, FFTmemBuffer_t *bufs)
{
  int i;

  printf ("FFT memory buffers:\n");
  printf ("    Buffer    Size(bytes)    Alignment\n");
  for (i = 0; i < nbufs; i++) {
    printf ("     %3d       %7d         %4d       \n", i, (int)bufs[i].size, (int)bufs[i].log2align);
  }
  bufs[0].base = ptr_x_i;
  bufs[1].base = ptr_y_i;
  bufs[2].base = ptr_w_i;

  bufs[4].base = ptr_x_i_work;
  bufs[5].base = ptr_y_i_work;
  bufs[6].base = ptr_w_i_work;
  bufs[9].base = ptr_y_i_temp;

  if (bufs[3].size != 0)
  {
    bufs[3].base = ptr_Bn_i;
    bufs[7].base = ptr_Bn_i_work;
  }
  else
  {
    bufs[3].base = NULL;
    bufs[7].base = NULL;
  }

  if (bufs[8].size != 0)
    bufs[8].base = ptr_work;
  else
    bufs[8].base = NULL;

} /* fft_memory_request */

void fft_memory_release (int nbufs, FFTmemBuffer_t *bufs)
{
  /* do nothing for now */
} /* fft_memory_request */

/* ======================================================================== */
/*  MAIN -- Top level driver for the test.                                  */
/* ======================================================================== */

int main ()
{
    int     i, j, N, rad_cn;
    clock_t t_start, t_stop, t_overhead, t_opt;
    float   diff, max_diff = 0;
    fft_plan_t p;
    fft_callout_t plan_fxns;

    /* --------------------------------------------------------------------- */
    /* intialize hardware timers                                             */
    /* --------------------------------------------------------------------- */
    TSCL=0;TSCH=0;

    /* initalize callout functions */
    plan_fxns.memoryRequest   = fft_memory_request;
    plan_fxns.memoryRelease   = fft_memory_release;
    
    /* initialize ECPY */
    fft_assert( (EdmaMgr_init(DNUM, NULL) == EdmaMgr_SUCCESS), DNUM, "EdmaMgr_init() return error!");
    fftEdmaState = FFT_EDMA_STATE_INIT;

    /* radix 2&4 testing */
    for (N = 256; N <= 65536; N = N*2)
    {

        memset (x_i,  0x55, sizeof (x_i) );
        memset (x_cn, 0x55, sizeof (x_cn));

        /* ---------------------------------------------------------------- */
        /* Initialize input vector temporarily.                             */
        /* ---------------------------------------------------------------- */

        for (i = 0; i < N; i++) {
          x_cn[PAD + 2*i  ] = sin (2 * 3.1415 * 50 * i / (double) N);
          x_cn[PAD + 2*i+1] = 0;
        }
        for (j = 0; j < N; j++) {
          x_i[PAD + j] = x_cn[PAD + 2*j];
        }

        /* ---------------------------------------------------------------- */
        /* Force uninitialized arrays to fixed values.                      */
        /* ---------------------------------------------------------------- */
        memset (y_i,  0xA5, sizeof (y_i) );
        memset (y_cn, 0xA5, sizeof (y_cn));

        /* ---------------------------------------------------------------- */
        /* Generate twiddle factors.                                        */
        /* ---------------------------------------------------------------- */
        j = 0;
        for (i = 0; i <= 31; i++)
            if ((N & (1 << i)) == 0)
                j++;
            else
                break;

        if (j % 2 == 0) {
            rad_cn = 4;
        }
        else {
            rad_cn = 2;
        }

        tw_gen_cn (ptr_w_cn, N);

        DSPF_sp_fftSPxSP (N, ptr_x_cn, ptr_w_cn, ptr_y_cn, NULL, rad_cn, 0, N);
//       	dft (N, ptr_x_cn, ptr_y_cn, N);

        /* ---------------------------------------------------------------- */
        /* Compute the overhead of allocating and freeing EDMA              */
        /* ---------------------------------------------------------------- */
        p.edmaState = fft_assign_edma_resources();
        fft_free_edma_resources(p.edmaState);
        t_start = _itoll(TSCH, TSCL);
        p.edmaState = fft_assign_edma_resources();
        fft_free_edma_resources(p.edmaState);
        t_stop  = _itoll(TSCH, TSCL);
        t_overhead = t_stop - t_start;

        /***************************************
         * ecpy fft test
         ***************************************/
        plan_fxns.ecpyRequest = fft_assign_edma_resources;
        plan_fxns.ecpyRelease = fft_free_edma_resources;

        p = fft_sp_plan_1d_r2c (N, FFT_ECPY, plan_fxns);
        t_start = _itoll(TSCH, TSCL);
        fft_execute (p);
        t_stop = _itoll(TSCH, TSCL);
        fft_destroy_plan (p);
        t_opt  = (t_stop - t_start) - t_overhead;

        /* ---------------------------------------------------------------- */
        /* compute difference and track max difference                      */  
        /* ---------------------------------------------------------------- */
        diff = 0; max_diff = 0;
        for(i=0; i<2*N; i++) {
          diff = _fabs(ptr_y_cn[i] - ptr_x_i[i]);
          if (diff > max_diff)
            max_diff = diff;
        }
        
        printf("fft_sp_1d_r2c_ecpy\tsize= %d\n", N);
        printf("max_diff = %f", max_diff);
        printf("\tN = %d\tCycle: %d\n\n", N, t_opt);

        /***************************************
         * direct form fft test
         ***************************************/
        
        /* ---------------------------------------------------------------- */
        /* Initialize input vector temporarily.                             */
        /* ---------------------------------------------------------------- */

        for (j = 0; j < N; j++) {
          x_i[PAD + j] = sin (2 * 3.1415 * 50 * j / (double) N);
        }

        /* ---------------------------------------------------------------- */
        /* Compute the overhead of EDMA assigment                           */
        /* ---------------------------------------------------------------- */
        t_start = _itoll(TSCH, TSCL);
        t_stop  = _itoll(TSCH, TSCL);
        t_overhead = t_stop - t_start;

        plan_fxns.ecpyRequest = NULL;
        plan_fxns.ecpyRelease = NULL;

        p = fft_sp_plan_1d_r2c (N, FFT_DIRECT, plan_fxns);
        t_start = _itoll(TSCH, TSCL);
        fft_execute (p);
        t_stop = _itoll(TSCH, TSCL);
        fft_destroy_plan (p);
        t_opt  = (t_stop - t_start) - t_overhead;

        /* ---------------------------------------------------------------- */
        /* compute difference and track max difference                      */
        /* ---------------------------------------------------------------- */
        diff = 0; max_diff = 0;
        for(i=0; i<2*N; i++) {
          diff = _fabs(ptr_y_cn[i] - ptr_y_i[i]);
          if (diff > max_diff) max_diff = diff;
        }
        
        printf("fft_sp_1d_r2c_direct\tsize= %d\n", N);
        printf("max_diff = %f", max_diff);
        printf("\tN = %d\tCycle: %d\n\n", N, t_opt);
    }

    /* radix-3,5 testing */
    for (N = 384; N <= 6000; N *= 15)
    {

        memset (x_i,  0x55, sizeof (x_i) );
        memset (x_cn, 0x55, sizeof (x_cn));

        /* ---------------------------------------------------------------- */
        /* Initialize input vector temporarily.                             */
        /* ---------------------------------------------------------------- */

        for (i = 0; i < N; i++) {
          x_cn[PAD + 2*i  ] = sin (2 * 3.1415 * 50 * i / (double) N);
          x_cn[PAD + 2*i+1] = 0;
        }
        for (j = 0; j < N; j++) {
          x_i[PAD + j] = x_cn[PAD + 2*j];
        }

        /* ---------------------------------------------------------------- */
        /* Force uninitialized arrays to fixed values.                      */
        /* ---------------------------------------------------------------- */
        memset (y_i,  0xA5, sizeof (y_i) );
        memset (y_cn, 0xA5, sizeof (y_cn));

       	dft (N, ptr_x_cn, ptr_y_cn, N);

        /* ---------------------------------------------------------------- */
        /* Compute the overhead of allocating and freeing EDMA              */
        /* ---------------------------------------------------------------- */
        p.edmaState = fft_assign_edma_resources();
        fft_free_edma_resources(p.edmaState);
        t_start = _itoll(TSCH, TSCL);
        p.edmaState = fft_assign_edma_resources();
        fft_free_edma_resources(p.edmaState);
        t_stop  = _itoll(TSCH, TSCL);
        t_overhead = t_stop - t_start;

        /***************************************
         * ecpy fft test
         ***************************************/
        plan_fxns.ecpyRequest = fft_assign_edma_resources;
        plan_fxns.ecpyRelease = fft_free_edma_resources;

        p = fft_sp_plan_1d_r2c (N, FFT_ECPY, plan_fxns);
        t_start = _itoll(TSCH, TSCL);
        fft_execute (p);
        t_stop = _itoll(TSCH, TSCL);
        fft_destroy_plan (p);
        t_opt  = (t_stop - t_start) - t_overhead;

        /* ---------------------------------------------------------------- */
        /* compute difference and track max difference                      */  
        /* ---------------------------------------------------------------- */
        diff = 0; max_diff = 0;
        for(i=0; i<2*N; i++) {
          diff = _fabs(ptr_y_cn[i] - ptr_x_i[i]);
          if (diff > max_diff)
            max_diff = diff;
        }
        
        printf("fft_sp_1d_r2c_ecpy\tsize= %d\n", N);
        printf("max_diff = %f", max_diff);
        printf("\tN = %d\tCycle: %d\n\n", N, t_opt);

        /***************************************
         * direct form fft test
         ***************************************/
        
        /* ---------------------------------------------------------------- */
        /* Initialize input vector temporarily.                             */
        /* ---------------------------------------------------------------- */

        for (j = 0; j < N; j++) {
          x_i[PAD + j] = sin (2 * 3.1415 * 50 * j / (double) N);
        }

        /* ---------------------------------------------------------------- */
        /* Compute the overhead of EDMA assigment                           */
        /* ---------------------------------------------------------------- */
        t_start = _itoll(TSCH, TSCL);
        t_stop  = _itoll(TSCH, TSCL);
        t_overhead = t_stop - t_start;

        plan_fxns.ecpyRequest = NULL;
        plan_fxns.ecpyRelease = NULL;

        p = fft_sp_plan_1d_r2c (N, FFT_DIRECT, plan_fxns);
        t_start = _itoll(TSCH, TSCL);
        fft_execute (p);
        t_stop = _itoll(TSCH, TSCL);
        fft_destroy_plan (p);
        t_opt  = (t_stop - t_start) - t_overhead;

        /* ---------------------------------------------------------------- */
        /* compute difference and track max difference                      */
        /* ---------------------------------------------------------------- */
        diff = 0; max_diff = 0;
        for(i=0; i<2*N; i++) {
          diff = _fabs(ptr_y_cn[i] - ptr_y_i[i]);
          if (diff > max_diff) max_diff = diff;
        }
        
        printf("fft_sp_1d_r2c_direct\tsize= %d\n", N);
        printf("max_diff = %f", max_diff);
        printf("\tN = %d\tCycle: %d\n\n", N, t_opt);
    }

    /* any size testing */
    for (N = 350; N <= 1024; N = N + 333)
    {

        memset (x_i,  0x55, sizeof (x_i) );
        memset (x_cn, 0x55, sizeof (x_cn));

        /* ---------------------------------------------------------------- */
        /* Initialize input vector temporarily.                             */
        /* ---------------------------------------------------------------- */

        for (i = 0; i < N; i++) {
          x_cn[PAD + 2*i  ] = sin (2 * 3.1415 * 50 * i / (double) N);
          x_cn[PAD + 2*i+1] = 0;
        }
        for (j = 0; j < N; j++) {
          x_i[PAD + j] = x_cn[PAD + 2*j];
        }

        /* ---------------------------------------------------------------- */
        /* Force uninitialized arrays to fixed values.                      */
        /* ---------------------------------------------------------------- */
        memset (y_i,  0xA5, sizeof (y_i) );
        memset (y_cn, 0xA5, sizeof (y_cn));

       	dft (N, ptr_x_cn, ptr_y_cn, N);

        /* ---------------------------------------------------------------- */
        /* Compute the overhead of allocating and freeing EDMA              */
        /* ---------------------------------------------------------------- */
        p.edmaState = fft_assign_edma_resources();
        fft_free_edma_resources(p.edmaState);
        t_start = _itoll(TSCH, TSCL);
        p.edmaState = fft_assign_edma_resources();
        fft_free_edma_resources(p.edmaState);
        t_stop  = _itoll(TSCH, TSCL);
        t_overhead = t_stop - t_start;

        /***************************************
         * ecpy fft test
         ***************************************/
        plan_fxns.ecpyRequest = fft_assign_edma_resources;
        plan_fxns.ecpyRelease = fft_free_edma_resources;

        p = fft_sp_plan_1d_r2c (N, FFT_ECPY, plan_fxns);
        t_start = _itoll(TSCH, TSCL);
        fft_execute (p);
        t_stop = _itoll(TSCH, TSCL);
        fft_destroy_plan (p);
        t_opt  = (t_stop - t_start) - t_overhead;

        /* ---------------------------------------------------------------- */
        /* compute difference and track max difference                      */  
        /* ---------------------------------------------------------------- */
        diff = 0; max_diff = 0;
        for(i=0; i<2*N; i++) {
          diff = _fabs(ptr_y_cn[i] - ptr_x_i[i]);
          if (diff > max_diff)
            max_diff = diff;
        }
        
        printf("fft_sp_1d_r2c_ecpy\tsize= %d\n", N);
        printf("max_diff = %f", max_diff);
        printf("\tN = %d\tCycle: %d\n\n", N, t_opt);

        /***************************************
         * direct form fft test
         ***************************************/
        
        /* ---------------------------------------------------------------- */
        /* Initialize input vector temporarily.                             */
        /* ---------------------------------------------------------------- */

        for (j = 0; j < N; j++) {
          x_i[PAD + j] = sin (2 * 3.1415 * 50 * j / (double) N);
        }

        /* ---------------------------------------------------------------- */
        /* Compute the overhead of EDMA assigment                           */
        /* ---------------------------------------------------------------- */
        t_start = _itoll(TSCH, TSCL);
        t_stop  = _itoll(TSCH, TSCL);
        t_overhead = t_stop - t_start;

        plan_fxns.ecpyRequest = NULL;
        plan_fxns.ecpyRelease = NULL;

        p = fft_sp_plan_1d_r2c (N, FFT_DIRECT, plan_fxns);
        t_start = _itoll(TSCH, TSCL);
        fft_execute (p);
        t_stop = _itoll(TSCH, TSCL);
        fft_destroy_plan (p);
        t_opt  = (t_stop - t_start) - t_overhead;

        /* ---------------------------------------------------------------- */
        /* compute difference and track max difference                      */
        /* ---------------------------------------------------------------- */
        diff = 0; max_diff = 0;
        for(i=0; i<2*N; i++) {
          diff = _fabs(ptr_y_cn[i] - ptr_y_i[i]);
          if (diff > max_diff) max_diff = diff;
        }
        
        printf("fft_sp_1d_r2c_direct\tsize= %d\n", N);
        printf("max_diff = %f", max_diff);
        printf("\tN = %d\tCycle: %d\n\n", N, t_opt);
    }
}

/* Function for generating Specialized sequence of twiddle factors */
void tw_gen_cn (float *w, int n)
{
    int i, j, k;
    const double PI = 3.141592654;

    for (j = 1, k = 0; j <= n >> 2; j = j << 2)
    {
        for (i = 0; i < n >> 2; i += j)
        {
#ifdef _LITTLE_ENDIAN
            w[k]     = (float) sin (2 * PI * i / n);
            w[k + 1] = (float) cos (2 * PI * i / n);
            w[k + 2] = (float) sin (4 * PI * i / n);
            w[k + 3] = (float) cos (4 * PI * i / n);
            w[k + 4] = (float) sin (6 * PI * i / n);
            w[k + 5] = (float) cos (6 * PI * i / n);
#else
            w[k]     = (float)  cos (2 * PI * i / n);
            w[k + 1] = (float) -sin (2 * PI * i / n);
            w[k + 2] = (float)  cos (4 * PI * i / n);
            w[k + 3] = (float) -sin (4 * PI * i / n);
            w[k + 4] = (float)  cos (6 * PI * i / n);
            w[k + 5] = (float) -sin (6 * PI * i / n);
#endif
            k += 6;
        }
    }
}

/* Function for calculating any size DFT */
void dft (int N, float x[], float y[], int N1)
{
    int k, i, index;
    const double PI = 3.141592654;
    float *p_x;
    float arg, fx_0, fx_1, fy_0, fy_1, co, si;

    for (k = 0; k < N1; k++)
    {
        p_x = x;

        fy_0 = 0;
        fy_1 = 0;
        for (i = 0; i < N; i++)
        {
            fx_0 = p_x[0];
            fx_1 = p_x[1];
            p_x += 2;
            index = (i * k) % N;
            arg = 2 * PI * index / N;
            co = cos (arg);
            si = -sin (arg);
            fy_0 += ((fx_0 * co) - (fx_1 * si));
            fy_1 += ((fx_1 * co) + (fx_0 * si));
        }
        y[2 * k] = fy_0;
        y[2 * k + 1] = fy_1;
    }
}

/* ======================================================================== */
/*  End of file:  fft_sp_1d_r2c_d.c                                         */
/* ------------------------------------------------------------------------ */
/*            Copyright (c) 2013 Texas Instruments, Incorporated.           */
/*                           All Rights Reserved.                           */
/* ======================================================================== */
