/* ======================================================================= */
/*  TEXAS INSTRUMENTS, INC.                                                */
/*                                                                         */
/*  FFTLIB  FFT Library                                                    */
/*                                                                         */
/* Copyright (C) 2013 Texas Instruments Incorporated - http://www.ti.com/  */ 
/*                                                                         */
/*                                                                         */
/*  Redistribution and use in source and binary forms, with or without     */
/*  modification, are permitted provided that the following conditions     */
/*  are met:                                                               */
/*                                                                         */
/*    Redistributions of source code must retain the above copyright       */
/*    notice, this list of conditions and the following disclaimer.        */
/*                                                                         */
/*    Redistributions in binary form must reproduce the above copyright    */
/*    notice, this list of conditions and the following disclaimer in the  */
/*    documentation and/or other materials provided with the               */
/*    distribution.                                                        */
/*                                                                         */
/*    Neither the name of Texas Instruments Incorporated nor the names of  */
/*    its contributors may be used to endorse or promote products derived  */
/*    from this software without specific prior written permission.        */
/*                                                                         */
/*  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS    */
/*  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT      */
/*  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR  */
/*  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT   */
/*  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,  */
/*  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT       */
/*  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,  */
/*  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY  */
/*  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT    */
/*  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE  */
/*  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.   */
/*                                                                         */
/* ======================================================================= */

#include <xdc/std.h>
#include <stdio.h>
#include <time.h>
#include <stdlib.h>
#include <limits.h>
#include <math.h>
#include <c6x.h>

#include <ti/sdo/fc/ecpy/ecpy.h>

#include "fft_sp_3d_c2c.h"
#include <ti/fftlib/src/common/fft_edma.h>
#include <ti/dsplib/src/DSPF_sp_fftSPxSP/DSPF_sp_fftSPxSP.h>

extern cregister volatile unsigned int DNUM;

#define FFT_EDMA_STATE_INIT         0
#define FFT_EDMA_STATE_ALLOCATED    1

int fftEdmaState;
FFT_EDMA_Struct FFT_EDMA;

/* Function to trap exceptions */
void fft_assert(int statement, int node_id, const char *error)
{
  volatile int dbg_halt = 1;

  if(!statement) {
    printf("%s (%d)\n",error,node_id);
    while(dbg_halt);
  }
}

void *fft_assign_edma_resources(void)
{
  /*
   * The edmaInstances are indexes into the C6678_config[] array defined in
   * fft_c6678_config, which is used to specify how EDMA resources are
   * divided between cores.
   */

  if ( fftEdmaState != FFT_EDMA_STATE_ALLOCATED )
  {
    FFT_EDMA.num_channels = 0;

    while ( FFT_EDMA.num_channels < FFT_NUM_EDMA_CH )
    {
      FFT_EDMA.channel[FFT_EDMA.num_channels] = EdmaMgr_alloc(FFT_MAX_EDMA_LINKS);
    
      fft_assert( (FFT_EDMA.channel[FFT_EDMA.num_channels] != NULL), 0, "EdmaMgr_alloc() failed ");

      FFT_EDMA.num_channels++;
    }
  }
  fftEdmaState = FFT_EDMA_STATE_ALLOCATED;

  return ((void *)&FFT_EDMA);
}

void fft_free_edma_resources(void *edma)
{
  FFT_EDMA_Struct *edmaState = (FFT_EDMA_Struct *)edma;
  /*
   * The edmaInstances are indexes into the C6678_config[] array defined in
   * fft_c6678_config, which is used to specify how EDMA resources are
   * divided between cores.
   */

  fft_assert( (edma == &FFT_EDMA), 0, "Did not get correct edma state struct.");
}

void tw_gen_cn (float *w, int n1, int n2, int n3);
void fft_3d_cmp (int N1, int rad1, int N2, int rad2, int N3, int rad3, float x[], float tw[], float y[]);

/* ======================================================================== */
/*  Kernel-specific alignments                                              */
/* ======================================================================== */
#pragma DATA_SECTION(x_i, ".mem_ddr");
#pragma DATA_SECTION(y_i, ".mem_ddr");
#pragma DATA_SECTION(x_cn, ".mem_ddr");
#pragma DATA_SECTION(y_cn, ".mem_ddr");
#pragma DATA_SECTION(w_i, ".mem_ddr");

#pragma DATA_ALIGN(x_i,  8);
#pragma DATA_ALIGN(x_cn, 8);
#pragma DATA_ALIGN(Bn_i,  8);
#pragma DATA_ALIGN(w_i, 8);

#pragma DATA_ALIGN(y_i,  8);
#pragma DATA_ALIGN(y_cn, 8);

#pragma DATA_SECTION(w_i, ".mem_ddr");
#pragma DATA_SECTION(Bn_i, ".mem_ddr");

#pragma DATA_SECTION(x_i_work, ".mem_l2");
#pragma DATA_SECTION(y_i_work, ".mem_l2");
#pragma DATA_SECTION(w_i_work, ".mem_l2");
#pragma DATA_SECTION(y_i_temp, ".mem_l2");
#pragma DATA_SECTION(Bn_i_work, ".mem_l2");
#pragma DATA_SECTION(workbuf, ".mem_l2");

#pragma DATA_ALIGN(x_i_work,  64);
#pragma DATA_ALIGN(y_i_work,  64);
#pragma DATA_ALIGN(w_i_work,  64);
#pragma DATA_ALIGN(Bn_i_work, 64);
#pragma DATA_ALIGN(workbuf, 8);


/* ======================================================================== */
/*  Parameters of fixed dataset.                                            */
/* ======================================================================== */
#define ANYSIZE_SUPPORT 1

#if ANYSIZE_SUPPORT
#define MAXN  (128)
#else
#define MAXN  (128)
#endif
#define M     (2*MAXN*MAXN*MAXN)
#define PAD   (0)

/* ======================================================================== */
/*  Initialized arrays with fixed test data.                                */
/* ======================================================================== */

float x_i [M + 2 * PAD];
float x_cn[M + 2 * PAD];

//float w_cn[6*MAXN + 2 * PAD];
float w_i[3*6*MAXN + 2 + 2 * PAD];
float Bn_i[3*4*MAXN + 2 * PAD];

float y_i [M + 2 * PAD];
float y_cn[M + 2 * PAD];

float x_i_work [4*MAXN*FFT_SP_3D_C2C_NUMOFLINEBUFS + 2 * PAD];
float y_i_work [4*MAXN*FFT_SP_3D_C2C_NUMOFLINEBUFS + 2 * PAD];
float y_i_temp [2*MAXN*FFT_SP_3D_C2C_NUMOFLINEBUFS + 2 * PAD];

#if ANYSIZE_SUPPORT
float w_i_work [6*MAXN + 2 + 2 * PAD];
float Bn_i_work[4*MAXN + 2 * PAD];
float workbuf  [8*MAXN + 2 * PAD];
#else
float w_i_work [MAXN + 2 + 2 * PAD];
#endif

/* ======================================================================== */
/*  Generate pointers to skip beyond array padding                          */
/* ======================================================================== */
float *const ptr_x_i  = x_i  + PAD;
float *const ptr_x_cn = x_cn + PAD;

//float *const ptr_w_cn = w_cn + PAD;
float *const ptr_w_cn = NULL;
float *const ptr_w_i  = w_i + PAD;
float *const ptr_Bn_i = Bn_i  + PAD;

float *const ptr_y_i  = y_i  + PAD;
float *const ptr_y_cn = y_cn + PAD;

float *const ptr_y_i_work  = y_i_work + PAD;
float *const ptr_x_i_work  = x_i_work + PAD;
float *const ptr_w_i_work  = w_i_work + PAD;
float *const ptr_y_i_temp  = y_i_temp + PAD;
float *const ptr_Bn_i_work = Bn_i_work + PAD;
float *const ptr_work      = workbuf + PAD;

void fft_memory_request (int nbufs, FFTmemBuffer_t *bufs)
{
  int i;

  printf ("FFT memory buffers:\n");
  printf ("    Buffer    Size(twords)    Alignment\n");
  for (i = 0; i < nbufs; i++) {
    printf ("     %3d       %7d         %4d       \n", i, (int)bufs[i].size, (int)bufs[i].log2align);
  }
  bufs[0].base = ptr_x_i;
  bufs[1].base = ptr_y_i;
  bufs[2].base = ptr_w_i;

  bufs[4].base = ptr_x_i_work;
  bufs[5].base = ptr_y_i_work;
  bufs[6].base = ptr_w_i_work;

  bufs[9].base = ptr_y_i_temp;

  if (bufs[3].size != 0){

    bufs[3].base = ptr_Bn_i;
    bufs[7].base = ptr_Bn_i_work;
    bufs[8].base = ptr_work;
  }
  else{

    bufs[3].base = NULL;
    bufs[7].base = NULL;
    bufs[8].base = NULL;
  }

} /* fft_memory_request */

void fft_memory_release (int nbufs, FFTmemBuffer_t *bufs)
{
  /* do nothing for now */
} /* fft_memory_request */

/* ======================================================================== */
/*  MAIN -- Top level driver for the test.                                  */
/* ======================================================================== */

int main ()
{
    int     i, j, k, rad1, rad2, rad3, N1, N2, N3;
    clock_t t_start, t_stop, t_overhead, t_opt;
    float   diff, max_diff = 0;
    fft_plan_t p;
    fft_callout_t          plan_fxns;

    /* --------------------------------------------------------------------- */
    /* intialize hardware timers                                             */
    /* --------------------------------------------------------------------- */
    TSCL=0;TSCH=0;

    /* initalize callout functions */
    plan_fxns.memoryRequest   = fft_memory_request;
    plan_fxns.ecpyRequest     = fft_assign_edma_resources;
    plan_fxns.memoryRelease   = fft_memory_release;
    plan_fxns.ecpyRelease     = fft_free_edma_resources;
    
    /* initialize ECPY */ 
    fft_assert( (EdmaMgr_init(DNUM, NULL) == EdmaMgr_SUCCESS), DNUM, "EdmaMgr_init() return error!");
    fftEdmaState = FFT_EDMA_STATE_INIT;

    /* radix 2&4 testing */
    for (N1 = 32, N2 = 32, N3 = 32, k = 1; N2 <= MAXN; N1 = N1 << 1, N2 = N2 << 1,  N3 = N3 << 1, k++)
    {

        memset (x_i,  0x55, sizeof (x_i) );
        memset (x_cn, 0x55, sizeof (x_cn));

        /* ---------------------------------------------------------------- */
        /* Initialize input vector temporarily.                             */
        /* ---------------------------------------------------------------- */

        for (j = 0; j < N3; j++) {
           for (i = 0; i < N1*N2; i++) {
             x_cn[PAD + 2*i + j*2*N1*N2]     = sin (2 * 3.1415 * (50+j) * i / (double) (N2));

             x_cn[PAD + 2*i + j*2*N1*N2 + 1] = sin (2 * 3.1415 * (60+j) * i / (double) (N2));
           }
         }
        for (j = 0; j < 2*N1*N2*N3; j++) {
             x_i [PAD + j]     = x_cn [ PAD + j];
         }

        /* ---------------------------------------------------------------- */
        /* Force uninitialized arrays to fixed values.                      */
        /* ---------------------------------------------------------------- */
        memset (y_i,  0xA5, sizeof (y_i) );
        memset (y_cn, 0xA5, sizeof (y_cn));

        /* ---------------------------------------------------------------- */
        /* Generate twiddle factors.                                        */
        /* ---------------------------------------------------------------- */

        j = _norm(N1);

        if (j%2 == 0)
            	rad1 = 4;
            else
            	rad1 = 2;

        j = _norm(N2);

        if (j%2 == 0)
            	rad2 = 4;
            else
            	rad2 = 2;

        j = _norm(N3);

        if (j%2 == 0)
            	rad3 = 4;
            else
            	rad3 = 2;

//        tw_gen_cn (ptr_w_cn, N1, N2, N3);

        t_start = _itoll(TSCH, TSCL);
        fft_3d_cmp (N1, rad1, N2, rad2, N3, rad3, ptr_x_cn, ptr_w_cn, ptr_y_cn);
        t_stop = _itoll(TSCH, TSCL);

        p = fft_sp_plan_3d_c2c (N1, N2, N3, FFT_ECPY, plan_fxns);
 
        /* ---------------------------------------------------------------- */
        /* Compute the overhead of calling clock twice to get timing info   */
        /* ---------------------------------------------------------------- */
        p.edmaState = (*p.fftcout.ecpyRequest)();
        (*p.fftcout.ecpyRelease)(p.edmaState);
        p.edmaState = (*p.fftcout.ecpyRequest)();
        (*p.fftcout.ecpyRelease)(p.edmaState);
        t_start = _itoll(TSCH, TSCL);
        p.edmaState = (*p.fftcout.ecpyRequest)();
        (*p.fftcout.ecpyRelease)(p.edmaState);
        t_stop  = _itoll(TSCH, TSCL);
        t_overhead = t_stop - t_start;
        
        t_start = _itoll(TSCH, TSCL);
        fft_execute (p);
        t_stop = _itoll(TSCH, TSCL);
        fft_destroy_plan (p);
        t_opt  = (t_stop - t_start) - t_overhead;

        /* ---------------------------------------------------------------- */
        /* compute difference and track max difference                      */  
        /* ---------------------------------------------------------------- */
        diff = 0; max_diff = 0;
        for(i=0; i<2*N1*N2*N3; i++) {
          diff = _fabs(ptr_y_cn[i] - ptr_y_i[i]);
          if (diff > max_diff) max_diff = diff;
        }

        printf("fftlib_sp_3d_c2c_ecpy\tsize=#: %d X %d X %d\t", N1, N2, N3);
        printf("max_diff = %f", max_diff);
        printf("\t N3 = %d\tCycle: %d\n\n", N3, t_opt);

    }

#if ANYSIZE_SUPPORT
    /* any size */
    /* radix 2&4 testing */
    for (N1 = 64, N2 = 16, N3 = 32, k = 1; ((N1 <= MAXN)&&(N2 <= MAXN)&&(N3 <= MAXN)); N1 = N1 + 8, N2 = N2 + 8, N3 = N3 + 16, k++)
    {

        memset (x_i,  0x55, sizeof (x_i) );
        memset (x_cn, 0x55, sizeof (x_cn));

        /* ---------------------------------------------------------------- */
        /* Initialize input vector temporarily.                             */
        /* ---------------------------------------------------------------- */

        for (j = 0; j < N3; j++) {
           for (i = 0; i < N1*N2; i++) {
             x_cn[PAD + 2*i + j*2*N1*N2]     = sin (2 * 3.1415 * (50+j) * i / (double) (N2));
             x_cn[PAD + 2*i + j*2*N1*N2 + 1] = sin (2 * 3.1415 * (60+j) * i / (double) (N2));
           }
        }
        for (j = 0; j < 2*N1*N2*N3; j++) {
             x_i[PAD + j] = x_cn [PAD + j];
        }

        /* ---------------------------------------------------------------- */
        /* Force uninitialized arrays to fixed values.                      */
        /* ---------------------------------------------------------------- */
        memset (y_i,  0xA5, sizeof (y_i) );
        memset (y_cn, 0xA5, sizeof (y_cn));

        /* ---------------------------------------------------------------- */
        /* Generate twiddle factors.                                        */
        /* ---------------------------------------------------------------- */

        j = _norm(N1);

        if (j%2 == 0)
          rad1 = 4;
        else
          rad1 = 2;

        j = _norm(N2);

        if (j%2 == 0)
          rad2 = 4;
        else
          rad2 = 2;

        j = _norm(N3);

        if (j%2 == 0)
          rad3 = 4;
        else
          rad3 = 2;

//        tw_gen_cn (ptr_w_cn, N1, N2, N3);

        t_start = _itoll(TSCH, TSCL);
        fft_3d_cmp (N1, rad1, N2, rad2, N3, rad3, ptr_x_cn, ptr_w_cn, ptr_y_cn);
        t_stop = _itoll(TSCH, TSCL);

        p = fft_sp_plan_3d_c2c (N1, N2, N3, FFT_ECPY, plan_fxns);

        /* ---------------------------------------------------------------- */
        /* Compute the overhead of calling clock twice to get timing info   */
        /* ---------------------------------------------------------------- */
        p.edmaState = (*p.fftcout.ecpyRequest)();
        (*p.fftcout.ecpyRelease)(p.edmaState);
        p.edmaState = (*p.fftcout.ecpyRequest)();
        (*p.fftcout.ecpyRelease)(p.edmaState);
        t_start = _itoll(TSCH, TSCL);
        p.edmaState = (*p.fftcout.ecpyRequest)();
        (*p.fftcout.ecpyRelease)(p.edmaState);
        t_stop  = _itoll(TSCH, TSCL);
        t_overhead = t_stop - t_start;

        t_start = _itoll(TSCH, TSCL);
        fft_execute (p);
        t_stop = _itoll(TSCH, TSCL);
        fft_destroy_plan (p);
        t_opt  = (t_stop - t_start) - t_overhead;

        /* ---------------------------------------------------------------- */
        /* compute difference and track max difference                      */
        /* ---------------------------------------------------------------- */
        diff = 0; max_diff = 0;
        for(i=0; i<2*N1*N2*N3; i++) {
          diff = _fabs(ptr_y_cn[i] - ptr_y_i[i]);
          if (diff > max_diff) max_diff = diff;
        }
        printf("fftlib_sp_3d_c2c_ecpy\tsize= %d X %d X %d\t", N1, N2, N3);
        printf("max_diff = %f", max_diff);
        printf("\t N3 = %d\tCycle: %d\n\n", N3, t_opt);

    }
#endif
}

/* Function for generating Specialized sequence of twiddle factors */
void tw_gen_cn (float *w, int n1, int n2, int n3)
{
    int i, j, k;
    const double PI = 3.141592654;

    for (j = 1, k = 0; j <= n1 >> 2; j = j << 2)
    {
        for (i = 0; i < n1 >> 2; i += j)
        {
#ifdef _LITTLE_ENDIAN
            w[k]     = (float) sin (2 * PI * i / n1);
            w[k + 1] = (float) cos (2 * PI * i / n1);
            w[k + 2] = (float) sin (4 * PI * i / n1);
            w[k + 3] = (float) cos (4 * PI * i / n1);
            w[k + 4] = (float) sin (6 * PI * i / n1);
            w[k + 5] = (float) cos (6 * PI * i / n1);
#else
            w[k]     = (float)  cos (2 * PI * i / n1);
            w[k + 1] = (float) -sin (2 * PI * i / n1);
            w[k + 2] = (float)  cos (4 * PI * i / n1);
            w[k + 3] = (float) -sin (4 * PI * i / n1);
            w[k + 4] = (float)  cos (6 * PI * i / n1);
            w[k + 5] = (float) -sin (6 * PI * i / n1);
#endif
            k += 6;
        }
    }

    k = 2*n1;

    for (j = 1; j <= n2 >> 2; j = j << 2)
    {
        for (i = 0; i < n2 >> 2; i += j)
        {
#ifdef _LITTLE_ENDIAN
            w[k]     = (float) sin (2 * PI * i / n2);
            w[k + 1] = (float) cos (2 * PI * i / n2);
            w[k + 2] = (float) sin (4 * PI * i / n2);
            w[k + 3] = (float) cos (4 * PI * i / n2);
            w[k + 4] = (float) sin (6 * PI * i / n2);
            w[k + 5] = (float) cos (6 * PI * i / n2);
#else
            w[k]     = (float)  cos (2 * PI * i / n2);
            w[k + 1] = (float) -sin (2 * PI * i / n2);
            w[k + 2] = (float)  cos (4 * PI * i / n2);
            w[k + 3] = (float) -sin (4 * PI * i / n2);
            w[k + 4] = (float)  cos (6 * PI * i / n2);
            w[k + 5] = (float) -sin (6 * PI * i / n2);
#endif
            k += 6;
        }
    }

    k = 2*n1 + 2*n2;

    for (j = 1; j <= n3 >> 2; j = j << 2)
    {
        for (i = 0; i < n3 >> 2; i += j)
        {
#ifdef _LITTLE_ENDIAN
            w[k]     = (float) sin (2 * PI * i / n3);
            w[k + 1] = (float) cos (2 * PI * i / n3);
            w[k + 2] = (float) sin (4 * PI * i / n3);
            w[k + 3] = (float) cos (4 * PI * i / n3);
            w[k + 4] = (float) sin (6 * PI * i / n3);
            w[k + 5] = (float) cos (6 * PI * i / n3);
#else
            w[k]     = (float)  cos (2 * PI * i / n3);
            w[k + 1] = (float) -sin (2 * PI * i / n3);
            w[k + 2] = (float)  cos (4 * PI * i / n3);
            w[k + 3] = (float) -sin (4 * PI * i / n3);
            w[k + 4] = (float)  cos (6 * PI * i / n3);
            w[k + 5] = (float) -sin (6 * PI * i / n3);
#endif
            k += 6;
        }
    }
}
void dft (int N, float x[], float y[], int N1)
{
    int k, i, index;
    const double PI = 3.141592654;
    float *p_x;
    float arg, fx_0, fx_1, fy_0, fy_1, co, si;

    for (k = 0; k < N1; k++)
    {
        p_x = x;

        fy_0 = 0;
        fy_1 = 0;
        for (i = 0; i < N; i++)
        {
            fx_0 = p_x[0];
            fx_1 = p_x[1];
            p_x += 2;
            index = (i * k) % N;
            arg = 2 * PI * index / N;
            co = cos (arg);
            si = -sin (arg);
            fy_0 += ((fx_0 * co) - (fx_1 * si));
            fy_1 += ((fx_1 * co) + (fx_0 * si));
        }
        y[2 * k] = fy_0;
        y[2 * k + 1] = fy_1;
    }
}

/* Function for calculating 2d FFT */

void fft_3d_cmp (int N1, int rad1, int N2, int rad2, int N3, int rad3, float x[], float tw[], float y[])
{
    int k, i, j, m, n1, n2, n3;

    float *restrict p_x = x, *restrict p_y = y;

    /* calculate all the 2D fft along n1, n2 axis*/
    for (m = 0; m < N3; m++)
    {
      /* calculate N2 FFT size of N1 */
      for (k = 0; k < N2; k++)
      {
        p_x = x + k*2*N1 + m*2*N1*N2;
        p_y = y + k*2*N1 + m*2*N1*N2;
 //     DSPF_sp_fftSPxSP (N1, p_x, tw, p_y, NULL, rad1, 0, N1);
        dft (N1, p_x, p_y, N1);
      }
      /* matrix transpose */
      for(i = 0; i < 2*N2; i+=2)
        for(j = 0; j < N1; j++)
        {
            x[2*j*N2 + i + m*2*N1*N2]     = y[2*j + N1*i + m*2*N1*N2];
            x[2*j*N2 + i + m*2*N1*N2 + 1] = y[2*j + N1*i + m*2*N1*N2 + 1];
        }
#if 1
      /* calculate N1 FFT size of N2 */
      for (k = 0; k < N1; k++)
      {
        p_x = x + k*2*N2 + m*2*N1*N2;
        p_y = y + k*2*N2 + m*2*N1*N2;
//        DSPF_sp_fftSPxSP(N2, p_x, tw2, p_y, NULL, rad2, 0, N2);
        dft (N2, p_x, p_y, N2);

      }

      /* matrix transpose */
      for(i = 0; i < 2*N1; i+=2)

        for(j = 0; j < N2; j++)
        {
            x[2*j*N1 + m*2*N1*N2 + i]     = y[2*j + N2*i + m*2*N1*N2];
            x[2*j*N1 + m*2*N1*N2 + i + 1] = y[2*j + N2*i + m*2*N1*N2 + 1];
        }
#endif
    }


    /* calculate 1D fft along n3 axis */

    /* first global transpose */
    for (n1 = 0; n1 < N1; n1++)
    {
      for(n2 = 0; n2 < N2; n2++)
      {
        for(n3 = 0; n3 < N3; n3++)
        {
          y[2*n1*N3 + n2*2*N1*N3 + 2*n3 ]     = x[2*n1 + n2*2*N1 + 2*N1*N2*n3];
          y[2*n1*N3 + n2*2*N1*N3 + 2*n3 + 1]  = x[2*n1 + n2*2*N1 + 2*N1*N2*n3 + 1];
        }
      }
   }

#if 1
   for (m = 0; m < N2; m++)
   {
   /* calculate N1 FFT size of N3 */
     for (k = 0; k < N1; k++)
     {
       p_x = x + k*2*N3 + m*2*N3*N1;
       p_y = y + k*2*N3 + m*2*N3*N1;
//        DSPF_sp_fftSPxSP(N3, p_x, tw+2*N1+2*N2, p_y, NULL, rad3, 0, N3);
       dft (N3, p_y, p_x, N3);
     }
   }

   /* global transpose back */
   for (n1 = 0; n1 < N1; n1++)
   {
     for(n2 = 0; n2 < N2; n2++)
     {
       for(n3 = 0; n3 < N3; n3++)
       {
    	 y[2*n1 + n2*2*N1 + 2*N1*N2*n3]     = x[2*n1*N3 + n2*2*N1*N3 + 2*n3 ];
    	 y[2*n1 + n2*2*N1 + 2*N1*N2*n3 + 1] = x[2*n1*N3 + n2*2*N1*N3 + 2*n3 + 1];
       }
     }
  }

#endif
}
/* ======================================================================== */
/*  End of file:  fft_sp_3d_c2c_d.c                                         */
/* ------------------------------------------------------------------------ */
/*            Copyright (c) 2013 Texas Instruments, Incorporated.           */
/*                           All Rights Reserved.                           */
/* ======================================================================== */
