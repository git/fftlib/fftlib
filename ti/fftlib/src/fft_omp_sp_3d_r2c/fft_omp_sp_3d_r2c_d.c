/* ======================================================================= */
/*  TEXAS INSTRUMENTS, INC.                                                */
/*                                                                         */
/*  FFTLIB  FFT Library                                                    */
/*                                                                         */
/* Copyright (C) 2013 Texas Instruments Incorporated - http://www.ti.com/  */
/*                                                                         */
/*                                                                         */
/*  Redistribution and use in source and binary forms, with or without     */
/*  modification, are permitted provided that the following conditions     */
/*  are met:                                                               */
/*                                                                         */
/*    Redistributions of source code must retain the above copyright       */
/*    notice, this list of conditions and the following disclaimer.        */
/*                                                                         */
/*    Redistributions in binary form must reproduce the above copyright    */
/*    notice, this list of conditions and the following disclaimer in the  */
/*    documentation and/or other materials provided with the               */
/*    distribution.                                                        */
/*                                                                         */
/*    Neither the name of Texas Instruments Incorporated nor the names of  */
/*    its contributors may be used to endorse or promote products derived  */
/*    from this software without specific prior written permission.        */
/*                                                                         */
/*  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS    */
/*  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT      */
/*  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR  */
/*  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT   */
/*  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,  */
/*  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT       */
/*  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,  */
/*  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY  */
/*  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT    */
/*  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE  */
/*  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.   */
/*                                                                         */
/* ======================================================================= */

#include <xdc/std.h>
#include <stdio.h>
#include <time.h>
#include <stdlib.h>
#include <limits.h>
#include <math.h>
#include <c6x.h>


#include <ti/fftlib/src/common/omp/omp_config.h>
#include <ti/fftlib/src/common/fft_common_d.h>
#include "fft_omp_sp_3d_r2c.h"
#include <ti/dsplib/src/DSPF_sp_fftSPxSP/DSPF_sp_fftSPxSP.h>
#include <ti/runtime/openmp/omp.h>

void tw_gen_cn (float *w, int n1, int n2, int n3);
void fft_3d_cmp (int N1, int rad1, int N2, int rad2, int N3, int rad3, float x[], float tw[], float y[]);

/* calculate radix for 1D array */
static void calculate_rad (int dim, int N, fft_para_mix_bs_t *fft_para, int *use_bs)
{
  int n, j, M, rad;
  int s_r3, s_r5, N_p2, dum;

  *use_bs = 1;

  /* calculate the size for zero padding when the signal size is not power of 2 */
  n = (N<<1)-1;
  j = _norm(n);
  M = 1 << (31-j);

  if (dim == 1)
    N_p2 = N/2;
  else
    N_p2 = N;
  s_r3 = 0;
  s_r5 = 0;

  /* if N is odd, bypass the rest of calculation, use Bluestein */
  if (((N>>1)<<1) == N)
  {
    /* if N is power of 2, no zero padding, regular radix 2 or 4 fft */
    if (N == (M>>1))
    { /* power of 2 */
      M = N;
      j = j+1;
      *use_bs = 0;
    }
    else {
      if (dim == 1)
        dum = N/2;
      else
        dum = N;
      while (dum/3*3 == dum) {
        s_r3++;
        dum /= 3;
      }
      while (dum/5*5 == dum) {
        s_r5++;
        dum /= 5;
      }
      if ((dum >= 8) && (dum == (1 << (30-_norm(dum))))) {
        /* mixed radix 2 and 3 */
        N_p2                 = dum;
        *use_bs = 0;
        M = N;
        j  = _norm(dum);
      }
    }
  }

  /* calculate radix for radix 2 or 4 r2c or c2c fft */
  if (j%2 == 0)
    rad = 4;
  else
    rad = 2;

  fft_para->N_p2 = N_p2;
  fft_para->s_r3 = s_r3;
  fft_para->s_r5 = s_r5;
  fft_para->M    = M;
  fft_para->rad  = rad;

} /*calculate_rad ()*/

/* calculate twiddle size, Bn buffer size and workbuffer size */
static void calculate_mem_size(int dim, int N, int M, int use_bs, int s_r3, int s_r5, int N_p2, int *twsize, int *Bnsize, int *worksize)
{
  int dum, n, i;
  /* Please note that Bn buffer and Workbuf are only used when doing Bluestein
   * meaning when use_bs == 1 */

  if (use_bs == 1)
  { /* when size is not power of 2 and not multiple of 3&5 */
    *twsize   = (2+2*N+2);
    *Bnsize   = (2*M);
    *worksize = (4*M);
  }
  else
  {
    if ((s_r3 == 0) && (s_r5 == 0)) {
    /* When signal is power of 2 */

      if (dim == 1)
      /* 1st dimension is r2c fft */
        *twsize   = (2+N/2);
      else
      /* other dimension is c2c fft */
        *twsize   = 2;
    }
    else {
    /* When signal is power of 3 or 5 */
      dum = 0;
      if (dim == 1)
    	  n = 2*N;
      else
        n  = 4*N;
      for (i = 0; i < s_r3; i++) {
        dum += n/3;
        n  /= 3;
      }
      n *= 2;  /* radix-5 has 8 twiddle vs radix-3 4 twiddles */
      for (i = 0; i < s_r5; i++) {
        dum += n/5;
        n  /= 5;
      }
      if(dim == 1)
        *twsize = (N/2+dum);
      else
        *twsize = (2+dum);
    }
    *Bnsize   = 0;
    *worksize = 0;
  }
} /* calculate_mem_size() */

/* ======================================================================== */
/*  Kernel-specific alignments                                              */
/* ======================================================================== */
#pragma DATA_SECTION(x_i, ".mem_ddr");
#pragma DATA_SECTION(y_i, ".mem_ddr");
#pragma DATA_SECTION(x_cn, ".mem_ddr");
#pragma DATA_SECTION(y_cn, ".mem_ddr");
//#pragma DATA_SECTION(w_cn, ".mem_ddr");
#pragma DATA_SECTION(w_i, ".mem_ddr");

#pragma DATA_ALIGN(x_i,  8);
#pragma DATA_ALIGN(x_cn, 8);
#pragma DATA_ALIGN(Bn_i,  8);
#pragma DATA_ALIGN(w_i, 8);

//#pragma DATA_ALIGN(w_cn, 8);

#pragma DATA_ALIGN(y_i,  8);
#pragma DATA_ALIGN(y_cn, 8);
#pragma DATA_ALIGN(w_i, 8);

#pragma DATA_SECTION(w_i, ".mem_ddr");
#pragma DATA_SECTION(Bn_i, ".mem_ddr");

#pragma DATA_SECTION(local_work, ".mem_l2");
#pragma DATA_ALIGN(local_work,  64);

/* ======================================================================== */
/*  Parameters of fixed dataset.                                            */
/* ======================================================================== */
#if ANYSIZE_SUPPORT
#define MAXN  (128)
#else
#define MAXN  (128)
#endif
#define M_i   (2*MAXN*MAXN*MAXN)
#define PAD   (0)

/* ======================================================================== */
/*  Initialized arrays with fixed test data.                                */
/* ======================================================================== */

float x_i [M_i + 2 * PAD];
float x_cn[M_i + 2 * PAD];

//float w_cn[6*MAXN + 2 * PAD];
float Bn_i[3*4*MAXN + 2 * PAD];

float y_i [M_i + 2 * PAD];
float y_cn[M_i + 2 * PAD];


#if ANYSIZE_SUPPORT
float w_i[3*6*MAXN + 4*(MAXN/2+1)*MAXN*MAXN + 12*MAXN + 8*MAXN + 2 + 2 * PAD];
float local_work [16432*2 + FFT_NUM_EDMA_CH*FFT_MAX_EDMA_LINKS_3D+10*MAXN*FFT_OMP_SP_3D_R2C_NUMOFLINEBUFS + 18*MAXN + 2 + 2*PAD];
#else
float w_i[MAXN + 4*(MAXN/2+1)*MAXN*MAXN + 4 + 2 * PAD];
#ifdef FFT_MEM_MODEL_LG
float local_work [16432*2 + FFT_NUM_EDMA_CH*FFT_MAX_EDMA_LINKS_3D+10*MAXN*FFT_OMP_SP_3D_R2C_NUMOFLINEBUFS + MAXN + 2 + 2*PAD];
#else
float local_work [FFT_NUM_EDMA_CH*FFT_MAX_EDMA_LINKS_3D+10*MAXN*FFT_OMP_SP_3D_R2C_NUMOFLINEBUFS + MAXN + 2 + 2*PAD];
#endif
#endif

/* ======================================================================== */
/*  Generate pointers to skip beyond array padding                          */
/* ======================================================================== */
float *const ptr_x_i  = x_i  + PAD;
float *const ptr_x_cn = x_cn + PAD;

//float *const ptr_w_cn = w_cn + PAD;
float *const ptr_w_cn = NULL;
float *const ptr_w_i  = w_i + PAD;

float *const ptr_y_i  = y_i  + PAD;
float *const ptr_y_cn = y_cn + PAD;

fft_plan_t p;


/* ======================================================================== */
/*  MAIN -- Top level driver for the test.                                  */
/* ======================================================================== */

int main ()
{
    int     i, j, k, rad1, rad2, rad3, N1, N2, N3;
    int     N, M1, M2, M3;
    int     int_tw_size, int_Bn_size, work_size, ext_tw_size, ext_Bn_size;
    int     twsize, Bnsize, worksize;
    int     use_bs_1 = 0, use_bs_2 = 0, use_bs_3 = 0;
    int     s_r3, s_r5, N_p2;
    clock_t t_start, t_stop, t_overhead, t_opt;
    float   diff, max_diff = 0;
    fft_callout_t          plan_fxns;
    size_t  l2_SRAM_size_orig;
    uint32_t *temp;
    lib_memdscr_t **fft_mem_handle = fftGetMemHandle();

    /* --------------------------------------------------------------------- */
    /* intialize hardware timers                                             */
    /* --------------------------------------------------------------------- */
    TSCL=0;TSCH=0;

    /* initalize callout functions */
    plan_fxns.memoryRequest   = NULL;
    plan_fxns.memoryRelease   = NULL;
    plan_fxns.ecpyRequest     = fft_omp_assign_edma_resources;
    plan_fxns.ecpyRelease     = fft_omp_free_edma_resources;
    
    /* initialize ECPY */ 
    omp_set_num_threads (OMP_MAX_NUM_CORES);
    fft_config_memory (&l2_SRAM_size_orig);
    temp = (uint32_t *)lib_smem_falloc(fft_mem_handle, 256*6*sizeof(uint32_t), 3);
#if 1
    if(temp==NULL) {
        printf("Memory allocation error!\n");
        return;
    }
#endif

    #pragma omp parallel
    {
      fft_assert( (lib_emt_init() == LIB_EMT_SUCCESS), DNUM, "lib_emt_init() return error!");
      fftEdmaState[DNUM] = FFT_EDMA_STATE_INIT;
    }

    /* radix 2&4 testing */
    for (N1 = 64, N2 = 64, N3 = 64, k = 1; N2 <= MAXN; N1 = N1 << 1, N2 = N2 << 1,  N3 = N3 << 1, k++)
    {

        memset (x_i,  0x55, sizeof (x_i) );
        memset (x_cn, 0x55, sizeof (x_cn));

        /* ---------------------------------------------------------------- */
        /* Initialize input vector temporarily.                             */
        /* ---------------------------------------------------------------- */

        for (j = 0; j < N3; j++) {
           for (i = 0; i < N1*N2; i++) {
             x_cn[PAD + 2*i + j*2*N1*N2]     = sin (2 * 3.1415 * (50+j) * i / (double) (N2));

             x_cn[PAD + 2*i + j*2*N1*N2 + 1] = 0;
           }
         }
        for (j = 0; j < N1*N2*N3; j++) {
             x_i [PAD + j]     = x_cn [ PAD + 2*j];
         }

        /* ---------------------------------------------------------------- */
        /* Force uninitialized arrays to fixed values.                      */
        /* ---------------------------------------------------------------- */
        memset (y_i,  0xA5, sizeof (y_i) );
        memset (y_cn, 0xA5, sizeof (y_cn));
        /* ---------------------------------------------------------------- */
        /* Generate twiddle factors.                                        */
        /* ---------------------------------------------------------------- */

        j = _norm(N1);

        if (j%2 == 0)
            	rad1 = 4;
            else
            	rad1 = 2;

        j = _norm(N2);

        if (j%2 == 0)
            	rad2 = 4;
            else
            	rad2 = 2;

        j = _norm(N3);

        if (j%2 == 0)
            	rad3 = 4;
            else
            	rad3 = 2;

//        tw_gen_cn (ptr_w_cn, N1, N2, N3);

        printf("fftlib_omp_sp_3d_r2c_ecpy\tsize=#: %d X %d X %d\t", N1, N2, N3);
        t_start = _itoll(TSCH, TSCL);

        fft_3d_cmp (N1, rad1, N2, rad2, N3, rad3, ptr_x_cn, ptr_w_cn, ptr_y_cn);

        t_stop = _itoll(TSCH, TSCL);
        
        /* ARM side code */
        
        /* determine the radix for the first dimension */
        calculate_rad(1, N1, &p.u.sp_3d_r2c_e.para1, &use_bs_1);
        M1 = p.u.sp_3d_r2c_e.para1.M;

        /* determine the radix for the second dimension */
        calculate_rad(2, N2, &p.u.sp_3d_r2c_e.para2, &use_bs_2);
        M2 = p.u.sp_3d_r2c_e.para2.M;

        /* determine the radix for the third dimension */
        calculate_rad(3, N3, &p.u.sp_3d_r2c_e.para3, &use_bs_3);
        M3 = p.u.sp_3d_r2c_e.para3.M;

        /* evaluate the 1st dimension */
        s_r3 = p.u.sp_3d_r2c_e.para1.s_r3;
        s_r5 = p.u.sp_3d_r2c_e.para1.s_r5;
        N_p2 = p.u.sp_3d_r2c_e.para1.N_p2;
        calculate_mem_size(1, N1, M1, use_bs_1, s_r3, s_r5, N_p2, &twsize, &Bnsize, &worksize);
        p.u.sp_3d_r2c_e.para1.twsize = twsize;
        p.u.sp_3d_r2c_e.para1.Bnsize = Bnsize;
        int_tw_size = ext_tw_size = twsize;
        int_Bn_size = ext_Bn_size = Bnsize;
        work_size   = worksize;

        /* evaluate the  2nd dimension */
        s_r3 = p.u.sp_3d_r2c_e.para2.s_r3;
        s_r5 = p.u.sp_3d_r2c_e.para2.s_r5;
        N_p2 = p.u.sp_3d_r2c_e.para2.N_p2;
        calculate_mem_size(2, N2, M2, use_bs_2, s_r3, s_r5, N_p2, &twsize, &Bnsize, &worksize);
        p.u.sp_3d_r2c_e.para2.twsize = twsize;
        p.u.sp_3d_r2c_e.para2.Bnsize = Bnsize;
        int_tw_size  = (int_tw_size > twsize) ? int_tw_size : twsize;
        ext_tw_size += twsize;
        int_Bn_size  = (int_Bn_size > Bnsize) ? int_Bn_size : Bnsize;
        ext_Bn_size += Bnsize;
        work_size    = (work_size > worksize) ? work_size : worksize;

        /* evaluate the  3rd dimension */
        s_r3 = p.u.sp_3d_r2c_e.para3.s_r3;
        s_r5 = p.u.sp_3d_r2c_e.para3.s_r5;
        N_p2 = p.u.sp_3d_r2c_e.para3.N_p2;
        calculate_mem_size(3, N3, M3, use_bs_3, s_r3, s_r5, N_p2, &twsize, &Bnsize, &worksize);
        p.u.sp_3d_r2c_e.para3.twsize = twsize;
        p.u.sp_3d_r2c_e.para3.Bnsize = Bnsize;
        int_tw_size  = (int_tw_size > twsize) ? int_tw_size : twsize;
        ext_tw_size += twsize;
        int_Bn_size  = (int_Bn_size > Bnsize) ? int_Bn_size : Bnsize;
        ext_Bn_size += Bnsize;
        work_size    = (work_size > worksize) ? work_size : worksize;

        fft_omp_sp_plan_3d_r2c (N1, N2, N3, FFT_ECPY, plan_fxns, &p, ptr_x_i ,ptr_y_i, ptr_w_i, NULL);

        /* ---------------------------------------------------------------- */
        /* Compute the overhead of allocating and freeing EDMA              */
        /* ---------------------------------------------------------------- */

        p.edmaState = (*p.fftcout.ecpyRequest)(temp, FFT_NUM_EDMA_CH*FFT_MAX_EDMA_LINKS_3D*sizeof(float));
        (*p.fftcout.ecpyRelease)(p.edmaState);
        t_start = _itoll(TSCH, TSCL);
        p.edmaState = (*p.fftcout.ecpyRequest)(temp, FFT_NUM_EDMA_CH*FFT_MAX_EDMA_LINKS_3D*sizeof(float));
        (*p.fftcout.ecpyRelease)(p.edmaState);
        t_stop  = _itoll(TSCH, TSCL);
        t_overhead = t_stop - t_start;
        printf("fft_dp_plan_3d_r2c is done\n");
        /* ---------------------------------------------------------------------- */
        /* Set the number of cores used                                           */
        /* ---------------------------------------------------------------------- */
        p.actualCoreNum = ACTIVE_THREAD_COUNT;
        p.local = local_work;
        t_start = _itoll(TSCH, TSCL);
        fft_execute (p);
        t_stop = _itoll(TSCH, TSCL);
        t_opt  = (t_stop - t_start) - t_overhead;

        /* ---------------------------------------------------------------- */
        /* compute difference and track max difference                      */  
        /* ---------------------------------------------------------------- */
        diff = 0; max_diff = 0;
        for(i=0; i<2*N3*N2*(N1/2+1); i++) {
          diff = _fabs(ptr_y_cn[i] - ptr_y_i[i]);
          if (diff > max_diff) max_diff = diff;
        }
 
        printf("fftlib_omp_sp_3d_r2c_ecpy\tsize=#: %d X %d X %d\n", N1, N2, N3);
        printf("max_diff = %f", max_diff);
        printf("\t N3 = %d\tCycle: %d\n\n", N3, t_opt);

    }

#if 1
    /* any size */
    for (N1 = 64, N2 = 32, N3 = 32, k = 1; ((N1 <= MAXN) && (N2 <= MAXN) && (N3 <= MAXN)); N3 = N3*3, k++)
    {

        memset (x_i,  0x55, sizeof (x_i) );
        memset (x_cn, 0x55, sizeof (x_cn));

        /* ---------------------------------------------------------------- */
        /* Initialize input vector temporarily.                             */
        /* ---------------------------------------------------------------- */

        for (j = 0; j < N3; j++) {
           for (i = 0; i < N1*N2; i++) {
             x_cn[PAD + 2*i + j*2*N1*N2]     = sin (2 * 3.1415 * (50+j) * i / (double) (N2));

             x_cn[PAD + 2*i + j*2*N1*N2 + 1] = 0;
           }
         }
        for (j = 0; j < N1*N2*N3; j++) {
             x_i [PAD + j]     = x_cn [ PAD + 2*j];
         }

        /* ---------------------------------------------------------------- */
        /* Force uninitialized arrays to fixed values.                      */
        /* ---------------------------------------------------------------- */
        memset (y_i,  0xA5, sizeof (y_i) );
        memset (y_cn, 0xA5, sizeof (y_cn));
        /* ---------------------------------------------------------------- */
        /* Generate twiddle factors.                                        */
        /* ---------------------------------------------------------------- */

        j = _norm(N1);

        if (j%2 == 0)
            	rad1 = 4;
            else
            	rad1 = 2;

        j = _norm(N2);

        if (j%2 == 0)
            	rad2 = 4;
            else
            	rad2 = 2;

        j = _norm(N3);

        if (j%2 == 0)
            	rad3 = 4;
            else
            	rad3 = 2;

//        tw_gen_cn (ptr_w_cn, N1, N2, N3);

        printf("fftlib_omp_sp_3d_r2c_ecpy\tsize=#: %d X %d X %d\t", N1, N2, N3);
        t_start = _itoll(TSCH, TSCL);

        fft_3d_cmp (N1, rad1, N2, rad2, N3, rad3, ptr_x_cn, ptr_w_cn, ptr_y_cn);

        t_stop = _itoll(TSCH, TSCL);
        
        /* ARM side code */
                
        /* determine the radix for the first dimension */
        calculate_rad(1, N1, &p.u.sp_3d_r2c_e.para1, &use_bs_1);
        M1 = p.u.sp_3d_r2c_e.para1.M;

        /* determine the radix for the second dimension */
        calculate_rad(2, N2, &p.u.sp_3d_r2c_e.para2, &use_bs_2);
        M2 = p.u.sp_3d_r2c_e.para2.M;

        /* determine the radix for the third dimension */
        calculate_rad(3, N3, &p.u.sp_3d_r2c_e.para3, &use_bs_3);
        M3 = p.u.sp_3d_r2c_e.para3.M;

        /* evaluate the 1st dimension */
        s_r3 = p.u.sp_3d_r2c_e.para1.s_r3;
        s_r5 = p.u.sp_3d_r2c_e.para1.s_r5;
        N_p2 = p.u.sp_3d_r2c_e.para1.N_p2;
        calculate_mem_size(1, N1, M1, use_bs_1, s_r3, s_r5, N_p2, &twsize, &Bnsize, &worksize);
        p.u.sp_3d_r2c_e.para1.twsize = twsize;
        p.u.sp_3d_r2c_e.para1.Bnsize = Bnsize;
        int_tw_size = ext_tw_size = twsize;
        int_Bn_size = ext_Bn_size = Bnsize;
        work_size   = worksize;

        /* evaluate the  2nd dimension */
        s_r3 = p.u.sp_3d_r2c_e.para2.s_r3;
        s_r5 = p.u.sp_3d_r2c_e.para2.s_r5;
        N_p2 = p.u.sp_3d_r2c_e.para2.N_p2;
        calculate_mem_size(2, N2, M2, use_bs_2, s_r3, s_r5, N_p2, &twsize, &Bnsize, &worksize);
        p.u.sp_3d_r2c_e.para2.twsize = twsize;
        p.u.sp_3d_r2c_e.para2.Bnsize = Bnsize;
        int_tw_size  = (int_tw_size > twsize) ? int_tw_size : twsize;
        ext_tw_size += twsize;
        int_Bn_size  = (int_Bn_size > Bnsize) ? int_Bn_size : Bnsize;
        ext_Bn_size += Bnsize;
        work_size    = (work_size > worksize) ? work_size : worksize;

        /* evaluate the  3rd dimension */
        s_r3 = p.u.sp_3d_r2c_e.para3.s_r3;
        s_r5 = p.u.sp_3d_r2c_e.para3.s_r5;
        N_p2 = p.u.sp_3d_r2c_e.para3.N_p2;
        calculate_mem_size(3, N3, M3, use_bs_3, s_r3, s_r5, N_p2, &twsize, &Bnsize, &worksize);
        p.u.sp_3d_r2c_e.para3.twsize = twsize;
        p.u.sp_3d_r2c_e.para3.Bnsize = Bnsize;
        int_tw_size  = (int_tw_size > twsize) ? int_tw_size : twsize;
        ext_tw_size += twsize;
        int_Bn_size  = (int_Bn_size > Bnsize) ? int_Bn_size : Bnsize;
        ext_Bn_size += Bnsize;
        work_size    = (work_size > worksize) ? work_size : worksize;

        fft_omp_sp_plan_3d_r2c (N1, N2, N3, FFT_ECPY, plan_fxns, &p, ptr_x_i ,ptr_y_i, ptr_w_i, NULL);

        /* ---------------------------------------------------------------- */
        /* Compute the overhead of allocating and freeing EDMA              */
        /* ---------------------------------------------------------------- */

        p.edmaState = (*p.fftcout.ecpyRequest)(temp, FFT_NUM_EDMA_CH*FFT_MAX_EDMA_LINKS_3D*sizeof(float));
        (*p.fftcout.ecpyRelease)(p.edmaState);
        t_start = _itoll(TSCH, TSCL);
        p.edmaState = (*p.fftcout.ecpyRequest)(temp, FFT_NUM_EDMA_CH*FFT_MAX_EDMA_LINKS_3D*sizeof(float));
        (*p.fftcout.ecpyRelease)(p.edmaState);
        t_stop  = _itoll(TSCH, TSCL);
        t_stop  = _itoll(TSCH, TSCL);
        t_overhead = t_stop - t_start;
        printf("fft_dp_plan_3d_r2c is done\n");
        /* ---------------------------------------------------------------------- */
        /* Set the number of cores used                                           */
        /* ---------------------------------------------------------------------- */
        p.actualCoreNum = ACTIVE_THREAD_COUNT;

        t_start = _itoll(TSCH, TSCL);
        p.local = local_work;
        fft_execute (p);
        t_stop = _itoll(TSCH, TSCL);
        t_opt  = (t_stop - t_start) - t_overhead;

        /* ---------------------------------------------------------------- */
        /* compute difference and track max difference                      */
        /* ---------------------------------------------------------------- */
        diff = 0; max_diff = 0;
        for(i=0; i<2*(N1/2+1)*N2*N3; i++) {
          diff = _fabs(ptr_y_cn[i] - ptr_y_i[i]);
          if (diff > max_diff) max_diff = diff;
        }

        printf("fftlib_omp_sp_3d_r2c_ecpy\tsize=#: %d X %d X %d\t", N1, N2, N3);
        printf("max_diff = %f", max_diff);
        printf("\t N3 = %d\tCycle: %d\n\n", N3, t_opt);

    }
#endif
}

/* Function for generating Specialized sequence of twiddle factors */
void tw_gen_cn (float *w, int n1, int n2, int n3)
{
    int i, j, k;
    const double PI = 3.141592654;

    for (j = 1, k = 0; j <= n1 >> 2; j = j << 2)
    {
        for (i = 0; i < n1 >> 2; i += j)
        {
#ifdef _LITTLE_ENDIAN
            w[k]     = (float) sin (2 * PI * i / n1);
            w[k + 1] = (float) cos (2 * PI * i / n1);
            w[k + 2] = (float) sin (4 * PI * i / n1);
            w[k + 3] = (float) cos (4 * PI * i / n1);
            w[k + 4] = (float) sin (6 * PI * i / n1);
            w[k + 5] = (float) cos (6 * PI * i / n1);
#else
            w[k]     = (float)  cos (2 * PI * i / n1);
            w[k + 1] = (float) -sin (2 * PI * i / n1);
            w[k + 2] = (float)  cos (4 * PI * i / n1);
            w[k + 3] = (float) -sin (4 * PI * i / n1);
            w[k + 4] = (float)  cos (6 * PI * i / n1);
            w[k + 5] = (float) -sin (6 * PI * i / n1);
#endif
            k += 6;
        }
    }

    k = 2*n1;

    for (j = 1; j <= n2 >> 2; j = j << 2)
    {
        for (i = 0; i < n2 >> 2; i += j)
        {
#ifdef _LITTLE_ENDIAN
            w[k]     = (float) sin (2 * PI * i / n2);
            w[k + 1] = (float) cos (2 * PI * i / n2);
            w[k + 2] = (float) sin (4 * PI * i / n2);
            w[k + 3] = (float) cos (4 * PI * i / n2);
            w[k + 4] = (float) sin (6 * PI * i / n2);
            w[k + 5] = (float) cos (6 * PI * i / n2);
#else
            w[k]     = (float)  cos (2 * PI * i / n2);
            w[k + 1] = (float) -sin (2 * PI * i / n2);
            w[k + 2] = (float)  cos (4 * PI * i / n2);
            w[k + 3] = (float) -sin (4 * PI * i / n2);
            w[k + 4] = (float)  cos (6 * PI * i / n2);
            w[k + 5] = (float) -sin (6 * PI * i / n2);
#endif
            k += 6;
        }
    }

    k = 2*n1 + 2*n2;

    for (j = 1; j <= n3 >> 2; j = j << 2)
    {
        for (i = 0; i < n3 >> 2; i += j)
        {
#ifdef _LITTLE_ENDIAN
            w[k]     = (float) sin (2 * PI * i / n3);
            w[k + 1] = (float) cos (2 * PI * i / n3);
            w[k + 2] = (float) sin (4 * PI * i / n3);
            w[k + 3] = (float) cos (4 * PI * i / n3);
            w[k + 4] = (float) sin (6 * PI * i / n3);
            w[k + 5] = (float) cos (6 * PI * i / n3);
#else
            w[k]     = (float)  cos (2 * PI * i / n3);
            w[k + 1] = (float) -sin (2 * PI * i / n3);
            w[k + 2] = (float)  cos (4 * PI * i / n3);
            w[k + 3] = (float) -sin (4 * PI * i / n3);
            w[k + 4] = (float)  cos (6 * PI * i / n3);
            w[k + 5] = (float) -sin (6 * PI * i / n3);
#endif
            k += 6;
        }
    }
}
void dft (int N, float x[], float y[], int N1)
{
    int k, i, index;
    const double PI = 3.141592654;
    float *p_x;
    float arg, fx_0, fx_1, fy_0, fy_1, co, si;

    for (k = 0; k < N1; k++)
    {
        p_x = x;

        fy_0 = 0;
        fy_1 = 0;
        for (i = 0; i < N; i++)
        {
            fx_0 = p_x[0];
            fx_1 = p_x[1];
            p_x += 2;
            index = (i * k) % N;
            arg = 2 * PI * index / N;
            co = cos (arg);
            si = -sin (arg);
            fy_0 += ((fx_0 * co) - (fx_1 * si));
            fy_1 += ((fx_1 * co) + (fx_0 * si));
        }
        y[2 * k] = fy_0;
        y[2 * k + 1] = fy_1;
    }
}

/* Function for calculating 2d FFT */

void fft_3d_cmp (int N1, int rad1, int N2, int rad2, int N3, int rad3, float x[], float tw[], float y[])
{
    int k, i, j, m, n1, n2, n3;

    float *restrict p_x = x, *restrict p_y = y;

    /* calculate all the 2D fft along n1, n2 axis*/
    for (m = 0; m < N3; m++)
    {
      /* calculate N2 FFT size of N1 */
      for (k = 0; k < N2; k++)
      {
        p_x = x + k*2*N1 + m*2*N1*N2;
        p_y = y + k*2*N1 + m*2*N1*N2;
 //     DSPF_sp_fftSPxSP (N1, p_x, tw, p_y, NULL, rad1, 0, N1);
        dft (N1, p_x, p_y, N1);
      }
      /* matrix transpose, save only half of the spectrum */
      for(i = 0; i < 2*N2; i+=2)
        for(j = 0; j < N1/2+1; j++)
        {
            x[2*j*N2 + i + m*2*(N1/2+1)*N2]     = y[2*j + N1*i + m*2*N1*N2];
            x[2*j*N2 + i + m*2*(N1/2+1)*N2 + 1] = y[2*j + N1*i + m*2*N1*N2 + 1];
        }
#if 1
      /* calculate (N1/2+1) FFT size of N2 */
      for (k = 0; k < (N1/2+1); k++)
      {
        p_x = x + k*2*N2 + m*2*(N1/2+1)*N2;
        p_y = y + k*2*N2 + m*2*(N1/2+1)*N2;
//        DSPF_sp_fftSPxSP(N2, p_x, tw2, p_y, NULL, rad2, 0, N2);
        dft (N2, p_x, p_y, N2);

      }

      /* matrix transpose */
      for(i = 0; i < 2*(N1/2+1); i+=2)

        for(j = 0; j < N2; j++)
        {
            x[2*j*(N1/2+1) + m*2*(N1/2+1)*N2 + i]     = y[2*j + N2*i + m*2*(N1/2+1)*N2];
            x[2*j*(N1/2+1) + m*2*(N1/2+1)*N2 + i + 1] = y[2*j + N2*i + m*2*(N1/2+1)*N2 + 1];
        }
#endif
    }


    /* calculate 1D fft along n3 axis */

    /* first global transpose */
    for (n1 = 0; n1 < (N1/2+1); n1++)
    {
      for(n2 = 0; n2 < N2; n2++)
      {
        for(n3 = 0; n3 < N3; n3++)
        {
          y[2*n1*N3 + n2*2*(N1/2+1)*N3 + 2*n3 ]     = x[2*n1 + n2*2*(N1/2+1) + 2*(N1/2+1)*N2*n3];
          y[2*n1*N3 + n2*2*(N1/2+1)*N3 + 2*n3 + 1]  = x[2*n1 + n2*2*(N1/2+1) + 2*(N1/2+1)*N2*n3 + 1];
        }
      }
   }

#if 1
   for (m = 0; m < N2; m++)
   {
   /* calculate N1 FFT size of N3 */
     for (k = 0; k < (N1/2+1); k++)
     {
       p_x = x + k*2*N3 + m*2*N3*(N1/2+1);
       p_y = y + k*2*N3 + m*2*N3*(N1/2+1);
//        DSPF_sp_fftSPxSP(N3, p_x, tw+2*N1+2*N2, p_y, NULL, rad3, 0, N3);
       dft (N3, p_y, p_x, N3);
     }
   }

   /* global transpose back */
   for (n1 = 0; n1 < (N1/2+1); n1++)
   {
     for(n2 = 0; n2 < N2; n2++)
     {
       for(n3 = 0; n3 < N3; n3++)
       {
       y[2*n1 + n2*2*(N1/2+1) + 2*(N1/2+1)*N2*n3]     = x[2*n1*N3 + n2*2*(N1/2+1)*N3 + 2*n3 ];
       y[2*n1 + n2*2*(N1/2+1) + 2*(N1/2+1)*N2*n3 + 1] = x[2*n1*N3 + n2*2*(N1/2+1)*N3 + 2*n3 + 1];
       }
     }
  }

#endif
}
/* ======================================================================== */
/*  End of file:  fft_omp_sp_3d_r2c_d.c                                     */
/* ------------------------------------------------------------------------ */
/*            Copyright (c) 2013 Texas Instruments, Incorporated.           */
/*                           All Rights Reserved.                           */
/* ======================================================================== */
