/* ======================================================================= */
/*  TEXAS INSTRUMENTS, INC.                                                */
/*                                                                         */
/*  FFTLIB  FFT Library                                                    */
/*                                                                         */
/* Copyright (C) 2013 Texas Instruments Incorporated - http://www.ti.com/  */ 
/*                                                                         */
/*                                                                         */
/*  Redistribution and use in source and binary forms, with or without     */
/*  modification, are permitted provided that the following conditions     */
/*  are met:                                                               */
/*                                                                         */
/*    Redistributions of source code must retain the above copyright       */
/*    notice, this list of conditions and the following disclaimer.        */
/*                                                                         */
/*    Redistributions in binary form must reproduce the above copyright    */
/*    notice, this list of conditions and the following disclaimer in the  */
/*    documentation and/or other materials provided with the               */
/*    distribution.                                                        */
/*                                                                         */
/*    Neither the name of Texas Instruments Incorporated nor the names of  */
/*    its contributors may be used to endorse or promote products derived  */
/*    from this software without specific prior written permission.        */
/*                                                                         */
/*  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS    */
/*  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT      */
/*  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR  */
/*  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT   */
/*  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,  */
/*  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT       */
/*  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,  */
/*  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY  */
/*  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT    */
/*  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE  */
/*  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.   */
/*                                                                         */
/* ======================================================================= */

#include <xdc/std.h>
#include <stdio.h>
#include <time.h>
#include <stdlib.h>
#include <limits.h>
#include <math.h>
#include <c6x.h>

#include <ti/sdo/fc/ecpy/ecpy.h>

#include "fft_dp_2d_c2c.h"
#include <ti/fftlib/src/common/fft_edma.h>
#include <ti/dsplib/src/DSPF_sp_fftSPxSP/DSPF_sp_fftSPxSP.h>

extern cregister volatile unsigned int DNUM;

#define FFT_EDMA_STATE_INIT         0
#define FFT_EDMA_STATE_ALLOCATED    1
///#define ANYSIZE_SUPPORT 1

int fftEdmaState;
FFT_EDMA_Struct FFT_EDMA;

/* Function to trap exceptions */
void fft_assert(int statement, int node_id, const char *error)
{
  volatile int dbg_halt = 1;

  if(!statement) {
    printf("%s (%d)\n",error,node_id);
    while(dbg_halt);
  }
}

void *fft_assign_edma_resources(void)
{
  /*
   * The edmaInstances are indexes into the C6678_config[] array defined in
   * fft_c6678_config, which is used to specify how EDMA resources are
   * divided between cores.
   */

  if ( fftEdmaState != FFT_EDMA_STATE_ALLOCATED )
  {
    FFT_EDMA.num_channels = 0;

    while ( FFT_EDMA.num_channels < FFT_NUM_EDMA_CH )
    {
      FFT_EDMA.channel[FFT_EDMA.num_channels] = EdmaMgr_alloc(FFT_MAX_EDMA_LINKS);
    
      fft_assert( (FFT_EDMA.channel[FFT_EDMA.num_channels] != NULL), 0, "EdmaMgr_alloc() failed ");

      FFT_EDMA.num_channels++;
    }
  }
  fftEdmaState = FFT_EDMA_STATE_ALLOCATED;

  return ((void *)&FFT_EDMA);
}

void fft_free_edma_resources(void *edma)
{
  //FFT_EDMA_Struct *edmaState = (FFT_EDMA_Struct *)edma;
  /*
   * The edmaInstances are indexes into the C6678_config[] array defined in
   * fft_c6678_config, which is used to specify how EDMA resources are
   * divided between cores.
   */
    int ret_val;

    if ( fftEdmaState == FFT_EDMA_STATE_ALLOCATED )
    {
      while ( FFT_EDMA.num_channels > 0 )
      {
    	  FFT_EDMA.num_channels--;

        ret_val = EdmaMgr_free(FFT_EDMA.channel[FFT_EDMA.num_channels]);

        fft_assert( ret_val == EdmaMgr_SUCCESS, 0, "EDMA free failed!");

      }
    }
    fftEdmaState = FFT_EDMA_STATE_INIT;

}

void tw_gen_cn (double *w, int n1, int n2);
void fft_2d_cmp (int N1, int rad1, int N2, int rad2, double x[], double tw[], double y[]);
void dft (int N, double x[], double y[], int N1);

/* ======================================================================== */
/*  Kernel-specific alignments                                              */
/* ======================================================================== */
#pragma DATA_SECTION(sig_i, ".mem_ddr");
#pragma DATA_SECTION(workbuf_i, ".mem_ddr");
#pragma DATA_SECTION(sig_cn, ".mem_ddr");
#pragma DATA_SECTION(workbuf_cn, ".mem_ddr");
#pragma DATA_SECTION(w_cn, ".mem_ddr");
#pragma DATA_SECTION(w_i, ".mem_ddr");

#if ANYSIZE_SUPPORT
#pragma DATA_SECTION(Bn_i, ".mem_ddr");
#endif

#pragma DATA_ALIGN(sig_i,  8);
#pragma DATA_ALIGN(sig_cn, 8);

#pragma DATA_ALIGN(w_i, 8);
#pragma DATA_ALIGN(w_cn, 8);

#pragma DATA_ALIGN(workbuf_i,  8);
#pragma DATA_ALIGN(workbuf_cn, 8);

#pragma DATA_SECTION(sig_i_work, ".mem_l2");
#pragma DATA_SECTION(workbuf_i_work, ".mem_l2");
#pragma DATA_SECTION(workbuf_i_temp, ".mem_l2");
#pragma DATA_SECTION(w_i_work, ".mem_l2");

#if ANYSIZE_SUPPORT
#pragma DATA_SECTION(Bn_i_work, ".mem_l2");
#pragma DATA_SECTION(workbuf, ".mem_l2");
#pragma DATA_ALIGN(Bn_i, 8);
#pragma DATA_ALIGN(Bn_i_work,  8);
#pragma DATA_ALIGN(workbuf,    8);
#endif

#pragma DATA_ALIGN(sig_i_work,  64);
#pragma DATA_ALIGN(workbuf_i_work,  64);
#pragma DATA_ALIGN(w_i_work,  8);


/* ======================================================================== */
/*  Parameters of fixed dataset.                                            */
/* ======================================================================== */
#if ANYSIZE_SUPPORT
#define MAXN  (512)
#else
#define MAXN  (1024)
#endif

#define M     (2*MAXN*MAXN)
#define PAD   (0)

/* ======================================================================== */
/*  Initialized arrays with fixed test data.                                */
/* ======================================================================== */

double sig_i [M + 2 * PAD];
double sig_cn[M + 2 * PAD];

double w_cn[6*MAXN + 2 * PAD];

double workbuf_i [M + 2 * PAD];
double workbuf_cn[M + 2 * PAD];

double sig_i_work [4*MAXN*FFT_DP_2D_C2C_NUMOFLINEBUFS + 2 * PAD];
double workbuf_i_work [4*MAXN*FFT_DP_2D_C2C_NUMOFLINEBUFS + 2 * PAD];
double workbuf_i_temp [2*MAXN*FFT_DP_2D_C2C_NUMOFLINEBUFS + 2 * PAD];

#if ANYSIZE_SUPPORT
double Bn_i[2*8*MAXN + 2 * PAD];
double w_i[2*6*MAXN + 2 + 2 * PAD];
double w_i_work [6*MAXN + 2 + 2 * PAD];
double Bn_i_work[8*MAXN + 2 * PAD];
double workbuf  [6*MAXN + 2 * PAD];
#else
double w_i[6*MAXN + 2 + 2 * PAD];
double w_i_work [MAXN + 2 + 2 * PAD];
#endif

/* ======================================================================== */
/*  Generate pointers to skip beyond array padding                          */
/* ======================================================================== */
double *const ptr_sig_i  = sig_i  + PAD;
double *const ptr_sig_cn = sig_cn + PAD;

double *const ptr_w_cn = w_cn + PAD;
double *const ptr_w_i  = w_i + PAD;

double *const ptr_workbuf_i  = workbuf_i  + PAD;
double *const ptr_workbuf_cn = workbuf_cn + PAD;

double *const ptr_workbuf_i_temp  = workbuf_i_temp + PAD;
double *const ptr_workbuf_i_work  = workbuf_i_work + PAD;
double *const ptr_sig_i_work  = sig_i_work + PAD;
double *const ptr_w_i_work  = w_i_work + PAD;

#if ANYSIZE_SUPPORT
double *const ptr_Bn_i = Bn_i  + PAD;
double *const ptr_Bn_i_work = Bn_i_work + PAD;
double *const ptr_work      = workbuf + PAD;
#endif

/* ======================================================================== */
/*  define arrays for storing the test vectors and test results             */
/* ======================================================================== */
typedef struct {
  int dim1;
  int dim2;
} radix_dim_struct;
radix_dim_struct radix_r2_dim[] = {
	{64, 64},
	{64, 128},
	{64, 256},
	{64, 512},
	///{64, 1024},
	{128, 128},
	{128, 256},
	{128, 512},
	///{128, 1024},
	{256, 256},
	{256, 512},
	///{256, 1024},
	{512, 512},
	///{512, 1024},
	///{1024, 1024},
	{0, 0}
};

radix_dim_struct radix_r35_dim[] = {
	{96, 96},      //32*3
	{160, 160},    //32*5
	{288, 288},    //32*3*3
	{480, 480},    //32*3*5
	{800, 800},    //32*5*5
	{864, 864},    //32*3*3*3
	{0, 0},
};

radix_dim_struct radix_bs_dim[] = {
    {224, 224},      // 32*7
    {352, 352},      // 32*11
    {416, 416},      // 32*13
    {544, 544},      // 32*17
    {608, 608},      // 32*19
    {736, 736},      // 32*23
    {928, 928},      // 32*29
    {992, 992},      // 32*31
	{0, 0},
};

#define TEST_RESULT_BUFFER_SIZE   100
typedef struct {
  int dim1;
  int dim2;
  long long thread[4];
} test_result_sruct;
test_result_sruct test_result_r2[TEST_RESULT_BUFFER_SIZE];
test_result_sruct test_result_r35[TEST_RESULT_BUFFER_SIZE];
test_result_sruct test_result_bs[TEST_RESULT_BUFFER_SIZE];
int thread_loop = 0;
int dim_loop_r2 = 0;
int dim_loop_r35 = 0;
int dim_loop_bs = 0;
int report_loop = 0;
int try_loop = 0;
FILE *rpFd;

void fft_memory_request (int nbufs, FFTmemBuffer_t *bufs)
{
  int i;

  printf ("FFT memory buffers:\n");
  printf ("    Buffer    Size(twords)    Alignment\n");
  for (i = 0; i < nbufs; i++) {
    printf ("     %3d       %7d         %4d       \n", i, (int)bufs[i].size, (int)bufs[i].log2align);
  }
  bufs[0].base = ptr_sig_i;
  bufs[1].base = ptr_workbuf_i;
  bufs[2].base = ptr_w_i;

  bufs[4].base = ptr_sig_i_work;
  bufs[5].base = ptr_workbuf_i_work;
  bufs[6].base = ptr_w_i_work;
  
  bufs[9].base = ptr_workbuf_i_temp;

#if ANYSIZE_SUPPORT
  if (bufs[3].size != 0)
  {
    bufs[3].base = ptr_Bn_i;
    bufs[7].base = ptr_Bn_i_work;
  }
  else
  {
    bufs[3].base = NULL;
    bufs[7].base = NULL;
  }

  if (bufs[8].size != 0)
    bufs[8].base = ptr_work;
  else
    bufs[8].base = NULL;
#else
  bufs[3].base = NULL;
  bufs[7].base = NULL;
  bufs[8].base = NULL;
#endif  

} /* fft_memory_request */

void fft_memory_release (int nbufs, FFTmemBuffer_t *bufs)
{
  /* do nothing for now */
} /* fft_memory_request */

/* ======================================================================== */
/*  MAIN -- Top level driver for the test.                                  */
/* ======================================================================== */

int main ()
{
    int     i, j, k, rad1, rad2, N, L;
    long long t_start, t_stop, t_overhead, t_opt;
    long long t_start1, t_stop1, t_overhead1;
    long long t_start2, t_stop2, t_overhead2;
    double   diff, max_diff = 0;
    fft_plan_t p;
    fft_callout_t          plan_fxns;

    /* --------------------------------------------------------------------- */
    /* intialize hardware timers                                             */
    /* --------------------------------------------------------------------- */
    TSCL=0;TSCH=0;

    /* initalize callout functions */
    plan_fxns.memoryRequest   = fft_memory_request;
    plan_fxns.ecpyRequest     = fft_assign_edma_resources;
    plan_fxns.memoryRelease   = fft_memory_release;
    plan_fxns.ecpyRelease     = fft_free_edma_resources;
    
    /* initialize ECPY */ 
    fft_assert( (EdmaMgr_init(DNUM, NULL) == EdmaMgr_SUCCESS), DNUM, "EdmaMgr_init() return error!");
    fftEdmaState = FFT_EDMA_STATE_INIT;

    /* radix 2&4 testing */
    for (dim_loop_r2 = 0; radix_r2_dim[dim_loop_r2].dim1>0; dim_loop_r2++)
    {
        N = radix_r2_dim[dim_loop_r2].dim1;
        L = radix_r2_dim[dim_loop_r2].dim2;
        memset (sig_i,  0x55, sizeof (sig_i) );
        memset (sig_cn, 0x55, sizeof (sig_cn));

        /* ---------------------------------------------------------------- */
        /* Initialize input vector temporarily.                             */
        /* ---------------------------------------------------------------- */

        for (i = 0; i < N*L; i++) {
          sig_cn[PAD + 2*i]     = sin (2 * 3.1415 * 50 * i / (double) (L));
          sig_cn[PAD + 2*i + 1] = 0;
        }
        for (j = 0; j < N*L; j++) {
             sig_i [PAD + 2*j]      = sig_cn [ PAD + 2*j];
             sig_i [PAD + 2*j + 1]  = sig_cn [ PAD + 2*j + 1];
         }

        /* ---------------------------------------------------------------- */
        /* Force uninitialized arrays to fixed values.                      */
        /* ---------------------------------------------------------------- */
        memset (workbuf_i,  0xA5, sizeof (workbuf_i) );
        memset (workbuf_cn, 0xA5, sizeof (workbuf_cn));

        /* ---------------------------------------------------------------- */
        /* Generate twiddle factors.                                        */
        /* ---------------------------------------------------------------- */

        j = _norm(N);

        if (j%2 == 0)
            	rad1 = 4;
            else
            	rad1 = 2;

        j = _norm(L);

        if (j%2 == 0)
            	rad2 = 4;
            else
            	rad2 = 2;

        tw_gen_cn (ptr_w_cn, N, L);

        t_start = _itoll(TSCH, TSCL);
        ///fft_2d_cmp (L, rad2, N, rad1, ptr_sig_cn, ptr_w_cn, ptr_workbuf_cn);
        t_stop = _itoll(TSCH, TSCL);

        p = fft_dp_plan_2d_c2c (N, L, FFT_ECPY, plan_fxns);
        
        /* ----------------------------------------------------------------- */
        /* call the ecpyRequest/ecpyRelease twice to warm up the EDMA engine */
        /* ----------------------------------------------------------------- */
        p.edmaState = (*p.fftcout.ecpyRequest)();
        (*p.fftcout.ecpyRelease)(p.edmaState);
        t_start1 = _itoll(TSCH, TSCL);
        p.edmaState = (*p.fftcout.ecpyRequest)();
        t_stop1  = _itoll(TSCH, TSCL);
        t_start2 = _itoll(TSCH, TSCL);
        (*p.fftcout.ecpyRelease)(p.edmaState);
        t_stop2  = _itoll(TSCH, TSCL);

        /* ---------------------------------------------------------------------- */
        /* Compute the overhead of calling clock three times and use the average  */
        /* ---------------------------------------------------------------------- */
        t_start = _itoll(TSCH, TSCL);
        p.edmaState = (*p.fftcout.ecpyRequest)();
        (*p.fftcout.ecpyRelease)(p.edmaState);
        p.edmaState = (*p.fftcout.ecpyRequest)();
        (*p.fftcout.ecpyRelease)(p.edmaState);
        p.edmaState = (*p.fftcout.ecpyRequest)();
        (*p.fftcout.ecpyRelease)(p.edmaState);
        t_stop  = _itoll(TSCH, TSCL);
        t_overhead = (t_stop - t_start)/3;
        t_overhead1 = t_stop1 - t_start1;
        t_overhead2 = t_stop2 - t_start2;

        p.edmaState = (*p.fftcout.ecpyRequest)();

        t_opt = 0;
        for (try_loop=0; try_loop<100; try_loop++)
        {
            for (j = 0; j < N*L; j++) {
                 sig_i [PAD + 2*j]      = sig_cn [ PAD + 2*j];
                 sig_i [PAD + 2*j + 1]  = sig_cn [ PAD + 2*j + 1];
            }
            t_start = _itoll(TSCH, TSCL);
        	fft_execute (p);
            t_stop = _itoll(TSCH, TSCL);
            t_opt  += (t_stop - t_start);
        }

        (*p.fftcout.ecpyRelease)(p.edmaState);

        fft_destroy_plan (p);
        t_opt  = t_opt/100;

        /* ---------------------------------------------------------------- */
        /* compute difference and track max difference                      */  
        /* ---------------------------------------------------------------- */
        diff = 0; max_diff = 0;
        for(i=0; i<2*N*L; i++) {
          diff = _fabs(ptr_sig_cn[i] - ptr_sig_i[i]);
          if (diff > max_diff) max_diff = diff;
        }

        printf("fft_dp_2d_c2c_ecpy\tsize=#: %d\t", N);
        printf("max_diff = %f", max_diff);
        printf("\tL = %d\tCycle: %lld\n\n", L, t_opt);

        /* ---------------------------------------------------------------- */
        /* store the dimension and the cycle count                          */
        /* ---------------------------------------------------------------- */
        test_result_r2[dim_loop_r2].dim1 = N;
        test_result_r2[dim_loop_r2].dim2 = L;
        test_result_r2[dim_loop_r2].thread[0] = t_opt;
    }


#if ANYSIZE_SUPPORT
    /* radix-3,5 */
    for (dim_loop_r35 = 0; radix_r35_dim[dim_loop_r35].dim1>0; dim_loop_r35++)
    {
        N = radix_r35_dim[dim_loop_r35].dim1;
        L = radix_r35_dim[dim_loop_r35].dim2;
        memset (sig_i,  0x55, sizeof (sig_i) );
        memset (sig_cn, 0x55, sizeof (sig_cn));

        /* ---------------------------------------------------------------- */
        /* Initialize input vector temporarily.                             */
        /* ---------------------------------------------------------------- */

        for (i = 0; i < N*L; i++) {
             sig_cn[PAD + 2*i]     = sin (2 * 3.1415 * 50 * i / (double) (L));
             sig_cn[PAD + 2*i + 1] = 0;
           }
        for (j = 0; j < N*L; j++) {
             sig_i [PAD + 2*j]      = sig_cn [ PAD + 2*j];
             sig_i [PAD + 2*j + 1]  = sig_cn [ PAD + 2*j + 1];
         }
         
        /* ---------------------------------------------------------------- */
        /* Force uninitialized arrays to fixed values.                      */
        /* ---------------------------------------------------------------- */
        memset (workbuf_i,  0xA5, sizeof (workbuf_i) );
        memset (workbuf_cn, 0xA5, sizeof (workbuf_cn));

        /* ---------------------------------------------------------------- */
        /* Generate twiddle factors.                                        */
        /* ---------------------------------------------------------------- */

        j = _norm(N);

        if (j%2 == 0)
            	rad1 = 4;
            else
            	rad1 = 2;

        j = _norm(L);

        if (j%2 == 0)
            	rad2 = 4;
            else
            	rad2 = 2;

        //tw_gen_cn (ptr_w_cn, N, L);

        t_start = _itoll(TSCH, TSCL);
        ///fft_2d_cmp (L, rad2, N, rad1, ptr_sig_cn, ptr_w_cn, ptr_workbuf_cn);
        t_stop = _itoll(TSCH, TSCL);

        /* ecpy testing */
        plan_fxns.ecpyRequest   = fft_assign_edma_resources;
        plan_fxns.ecpyRelease   = fft_free_edma_resources;

        p = fft_dp_plan_2d_c2c (N, L, FFT_ECPY, plan_fxns);

        /* ----------------------------------------------------------------- */
        /* call the ecpyRequest/ecpyRelease twice to warm up the EDMA engine */
        /* ----------------------------------------------------------------- */
        p.edmaState = (*p.fftcout.ecpyRequest)();
        (*p.fftcout.ecpyRelease)(p.edmaState);
        p.edmaState = (*p.fftcout.ecpyRequest)();
        (*p.fftcout.ecpyRelease)(p.edmaState);

        /* ---------------------------------------------------------------------- */
        /* Compute the overhead of calling clock three times and use the average  */
        /* ---------------------------------------------------------------------- */
        t_start = _itoll(TSCH, TSCL);
        p.edmaState = (*p.fftcout.ecpyRequest)();
        p.edmaState = (*p.fftcout.ecpyRequest)();
        (*p.fftcout.ecpyRelease)(p.edmaState);
        p.edmaState = (*p.fftcout.ecpyRequest)();
        (*p.fftcout.ecpyRelease)(p.edmaState);
        (*p.fftcout.ecpyRelease)(p.edmaState);
        t_stop  = _itoll(TSCH, TSCL);
        t_overhead = (t_stop - t_start)/3;

        t_start = _itoll(TSCH, TSCL);
        fft_execute (p);
        t_stop = _itoll(TSCH, TSCL);
        fft_destroy_plan (p);
        t_opt  = (t_stop - t_start) - t_overhead;

        /* ---------------------------------------------------------------- */
        /* compute difference and track max difference                      */
        /* ---------------------------------------------------------------- */
        diff = 0; max_diff = 0;
        for(i=0; i<2*N*L; i++) {
          diff = _fabs(ptr_sig_cn[i] - ptr_sig_i[i]);
          if (diff > max_diff)
              max_diff = diff;
        }

        printf("fft_dp_2d_c2c_ecpy\tsize=#: %d\t", N);
        printf("max_diff = %f", max_diff);
        printf("\tL = %d\tCycle: %lld\n\n", L, t_opt);

        /* ---------------------------------------------------------------- */
        /* store the dimension and the cycle count                          */
        /* ---------------------------------------------------------------- */
        test_result_r35[dim_loop_r35].dim1 = N;
        test_result_r35[dim_loop_r35].dim2 = L;
        test_result_r35[dim_loop_r35].thread[0] = t_opt;
    }

    /* bluestein testing */
    for (dim_loop_bs = 0; radix_bs_dim[dim_loop_bs].dim1>0; dim_loop_bs++)
    {
        N = radix_bs_dim[dim_loop_bs].dim1;
        L = radix_bs_dim[dim_loop_bs].dim2;
        memset (sig_i,  0x55, sizeof (sig_i) );
        memset (sig_cn, 0x55, sizeof (sig_cn));

        /* ---------------------------------------------------------------- */
        /* Initialize input vector temporarily.                             */
        /* ---------------------------------------------------------------- */

        for (i = 0; i < N*L; i++) {
             sig_cn[PAD + 2*i]     = sin (2 * 3.1415 * 50 * i / (double) (L));
             sig_cn[PAD + 2*i + 1] = 0;
           }
        for (j = 0; j < N*L; j++) {
             sig_i [PAD + 2*j]      = sig_cn [ PAD + 2*j];
             sig_i [PAD + 2*j + 1]  = sig_cn [ PAD + 2*j + 1];
         }

        /* ---------------------------------------------------------------- */
        /* Force uninitialized arrays to fixed values.                      */
        /* ---------------------------------------------------------------- */
        memset (workbuf_i,  0xA5, sizeof (workbuf_i) );
        memset (workbuf_cn, 0xA5, sizeof (workbuf_cn));

        /* ---------------------------------------------------------------- */
        /* Generate twiddle factors.                                        */
        /* ---------------------------------------------------------------- */

        j = _norm(N);

        if (j%2 == 0)
            	rad1 = 4;
            else
            	rad1 = 2;

        j = _norm(L);

        if (j%2 == 0)
            	rad2 = 4;
            else
            	rad2 = 2;

        //tw_gen_cn (ptr_w_cn, N, L);

        t_start = _itoll(TSCH, TSCL);
        ///fft_2d_cmp (L, rad2, N, rad1, ptr_sig_cn, ptr_w_cn, ptr_workbuf_cn);
        t_stop = _itoll(TSCH, TSCL);

        /* ecpy testing */
        plan_fxns.ecpyRequest   = fft_assign_edma_resources;
        plan_fxns.ecpyRelease   = fft_free_edma_resources;

        p = fft_dp_plan_2d_c2c (N, L, FFT_ECPY, plan_fxns);

        /* ----------------------------------------------------------------- */
        /* call the ecpyRequest/ecpyRelease twice to warm up the EDMA engine */
        /* ----------------------------------------------------------------- */
        p.edmaState = (*p.fftcout.ecpyRequest)();
        (*p.fftcout.ecpyRelease)(p.edmaState);
        p.edmaState = (*p.fftcout.ecpyRequest)();
        (*p.fftcout.ecpyRelease)(p.edmaState);

        /* ---------------------------------------------------------------------- */
        /* Compute the overhead of calling clock three times and use the average  */
        /* ---------------------------------------------------------------------- */
        t_start = _itoll(TSCH, TSCL);
        p.edmaState = (*p.fftcout.ecpyRequest)();
        p.edmaState = (*p.fftcout.ecpyRequest)();
        (*p.fftcout.ecpyRelease)(p.edmaState);
        p.edmaState = (*p.fftcout.ecpyRequest)();
        (*p.fftcout.ecpyRelease)(p.edmaState);
        (*p.fftcout.ecpyRelease)(p.edmaState);
        t_stop  = _itoll(TSCH, TSCL);
        t_overhead = (t_stop - t_start)/3;

        t_start = _itoll(TSCH, TSCL);
        fft_execute (p);
        t_stop = _itoll(TSCH, TSCL);
        fft_destroy_plan (p);
        t_opt  = (t_stop - t_start) - t_overhead;

        /* ---------------------------------------------------------------- */
        /* compute difference and track max difference                      */
        /* ---------------------------------------------------------------- */
        diff = 0; max_diff = 0;
        for(i=0; i<2*N*L; i++) {
          diff = _fabs(ptr_sig_cn[i] - ptr_sig_i[i]);
          if (diff > max_diff)
              max_diff = diff;
        }
        
        printf("fft_dp_2d_c2c_ecpy\tsize=#: %d\t", N);
        printf("max_diff = %f", max_diff);
        printf("\tL = %d\tCycle: %lld\n\n", L, t_opt);

        /* ---------------------------------------------------------------- */
        /* store the dimension and the cycle count                          */
        /* ---------------------------------------------------------------- */
        test_result_bs[dim_loop_bs].dim1 = N;
        test_result_bs[dim_loop_bs].dim2 = L;
        test_result_bs[dim_loop_bs].thread[0] = t_opt;
    }
#endif

    rpFd = fopen("fft_dp_2d_c2c_benchmark.csv", "w");
    fprintf(rpFd, "Radix 2,,,\n");
    fprintf(rpFd, "N,M,1-thread,,,\n");

    for (report_loop = 0; report_loop<dim_loop_r2; report_loop++)
    {
      fprintf(rpFd, "%d,%d,%lld,,,\n",
      		test_result_r2[report_loop].dim1,
      		test_result_r2[report_loop].dim2,
      		test_result_r2[report_loop].thread[0]);
    }

#if ANYSIZE_SUPPORT
    fprintf(rpFd, ",,,,,,\n");
    fprintf(rpFd, "Radix 3&5,,,\n");
    fprintf(rpFd, "N,M,1-thread,,,\n");
    for (report_loop = 0; report_loop<dim_loop_r35; report_loop++)
    {
      fprintf(rpFd, "%d,%d,%lld,,,\n",
      		test_result_r35[report_loop].dim1,
      		test_result_r35[report_loop].dim2,
      		test_result_r35[report_loop].thread[0]);
    }

    fprintf(rpFd, ",,,,,,\n");
    fprintf(rpFd, "Bluestein,,,\n");
    fprintf(rpFd, "N,M,1-thread,,,\n");
    for (report_loop = 0; report_loop<dim_loop_bs; report_loop++)
    {
      fprintf(rpFd, "%d,%d,%lld,,,\n",
      		test_result_bs[report_loop].dim1,
      		test_result_bs[report_loop].dim2,
      		test_result_bs[report_loop].thread[0]);
    }
#endif
    fprintf(rpFd, ",,,,,,\n");
    fprintf(rpFd, "EdmaMgr Overhead,%lld,%lld,%lld\n", t_overhead, t_overhead1, t_overhead2);
    fclose(rpFd);
}

/* Function for generating Specialized sequence of twiddle factors */
void tw_gen_cn (double *w, int n1, int n2)
{
    int i, j, k;
    const double PI = 3.141592654;

    for (j = 1, k = 0; j <= n1 >> 2; j = j << 2)
    {
        for (i = 0; i < n1 >> 2; i += j)
        {
#ifdef _LITTLE_ENDIAN
            w[k]     = (double) sin (2 * PI * i / n1);
            w[k + 1] = (double) cos (2 * PI * i / n1);
            w[k + 2] = (double) sin (4 * PI * i / n1);
            w[k + 3] = (double) cos (4 * PI * i / n1);
            w[k + 4] = (double) sin (6 * PI * i / n1);
            w[k + 5] = (double) cos (6 * PI * i / n1);
#else
            w[k]     = (double)  cos (2 * PI * i / n1);
            w[k + 1] = (double) -sin (2 * PI * i / n1);
            w[k + 2] = (double)  cos (4 * PI * i / n1);
            w[k + 3] = (double) -sin (4 * PI * i / n1);
            w[k + 4] = (double)  cos (6 * PI * i / n1);
            w[k + 5] = (double) -sin (6 * PI * i / n1);
#endif
            k += 6;
        }
    }

    k = 2*n1;

    for (j = 1; j <= n2 >> 2; j = j << 2)
    {
        for (i = 0; i < n2 >> 2; i += j)
        {
#ifdef _LITTLE_ENDIAN
            w[k]     = (double) sin (2 * PI * i / n2);
            w[k + 1] = (double) cos (2 * PI * i / n2);
            w[k + 2] = (double) sin (4 * PI * i / n2);
            w[k + 3] = (double) cos (4 * PI * i / n2);
            w[k + 4] = (double) sin (6 * PI * i / n2);
            w[k + 5] = (double) cos (6 * PI * i / n2);
#else
            w[k]     = (double)  cos (2 * PI * i / n2);
            w[k + 1] = (double) -sin (2 * PI * i / n2);
            w[k + 2] = (double)  cos (4 * PI * i / n2);
            w[k + 3] = (double) -sin (4 * PI * i / n2);
            w[k + 4] = (double)  cos (6 * PI * i / n2);
            w[k + 5] = (double) -sin (6 * PI * i / n2);
#endif
            k += 6;
        }
    }
}

/* Function for calculating 2d FFT */
void fft_2d_cmp (int N1, int rad1, int N2, int rad2, double x[], double tw[], double y[])
{
    int k, i, j;

    double *restrict p_x = x, *restrict p_y = y; // *restrict tw2 = tw+2*N1;

    k = 0;
    /* calculate N1 FFT size of N2 */
    for (k = 0; k < N1; k++)
    {
      p_x = x + k*2*N2;
      p_y = y + k*2*N2;
 //     DSPF_sp_fftSPxSP (N2, p_x, tw2, p_y, NULL, rad2, 0, N2);
      dft (N2, p_x, p_y, N2);
    }
    /* matrix transpose */
    for(i = 0; i < 2*N1; i+=2)
        #pragma MUST_ITERATE(2,,)
        for(j = 0; j < N2; j++)
        {
            x[2*j*N1 + i]     = y[2*j + N2*i];
            x[2*j*N1 + i + 1] = y[2*j + N2*i + 1];
        }

    /* calculate N2 FFT size of N1 */
    for (k = 0; k < N2; k++)
    {
        p_x = x + k*2*N1;
        p_y = y + k*2*N1;
//        DSPF_sp_fftSPxSP(N1, p_x, tw, p_y, NULL, rad1, 0, N1);
        dft (N1, p_x, p_y, N1);

    }

    /* matrix transpose */
    for(i = 0; i < 2*N2; i+=2)
        #pragma MUST_ITERATE(2,,)
        for(j = 0; j < N1; j++)
        {
            x[2*j*N2 + i]     = y[2*j + N1*i];
            x[2*j*N2 + i + 1] = y[2*j + N1*i + 1];
        }
}

/* Function for calculating any size DFT */
void dft (int N, double x[], double y[], int N1)
{
    int k, i, index;
    const double PI = 3.141592654;
    double *p_x;
    double arg, fx_0, fx_1, fy_0, fy_1, co, si;

    for (k = 0; k < N1; k++)
    {
        p_x = x;

        fy_0 = 0;
        fy_1 = 0;
        for (i = 0; i < N; i++)
        {
            fx_0 = p_x[0];
            fx_1 = p_x[1];
            p_x += 2;
            index = (i * k) % N;
            arg = 2 * PI * index / N;
            co = cos (arg);
            si = -sin (arg);
            fy_0 += ((fx_0 * co) - (fx_1 * si));
            fy_1 += ((fx_1 * co) + (fx_0 * si));
        }
        y[2 * k] = fy_0;
        y[2 * k + 1] = fy_1;
    }
}

/* ======================================================================== */
/*  End of file:  fft_dp_2d_c2c_d.c                                         */
/* ------------------------------------------------------------------------ */
/*            Copyright (c) 2013 Texas Instruments, Incorporated.           */
/*                           All Rights Reserved.                           */
/* ======================================================================== */
