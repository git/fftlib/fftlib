
/* ======================================================================== *
 * FFTLIB -- TI FFT Library                                                 *
 *                                                                          *
 *   Doxygen generation header file                                         *
 *                                                                          *
 * Copyright (C) 2013 Texas Instruments Incorporated - http://www.ti.com/   *
 *                                                                          *
 *                                                                          *
 *  Redistribution and use in source and binary forms, with or without      *
 *  modification, are permitted provided that the following conditions      *
 *  are met:                                                                *
 *                                                                          *
 *    Redistributions of source code must retain the above copyright        *
 *    notice, this list of conditions and the following disclaimer.         *
 *                                                                          *
 *    Redistributions in binary form must reproduce the above copyright     *
 *    notice, this list of conditions and the following disclaimer in the   *
 *    documentation and/or other materials provided with the                *
 *    distribution.                                                         *
 *                                                                          *
 *    Neither the name of Texas Instruments Incorporated nor the names of   *
 *    its contributors may be used to endorse or promote products derived   *
 *    from this software without specific prior written permission.         *
 *                                                                          *
 *  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS     *
 *  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT       *
 *  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR   *
 *  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT    *
 *  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,   *
 *  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT        *
 *  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,   *
 *  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY   *
 *  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT     *
 *  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE   *
 *  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.    *
 * ======================================================================== */

/**
 * @mainpage FFTLIB Bundle Release Notes
 *
 * <hr size="2" width="50%" align="left">
 *
 * <h2><a name="Contents">Contents</a></h2>
 *  
 *  <ol>
 *    <li> <a href="#Introduction">Introduction</a>
 *    <li> <a href="#Starting">Getting Started</a>
 *    <li> <a href="#Documentation">Documentation</a>                          
 *    <li> <a href="#Whats_New">What's New</a>                                 
 *    <li> <a href="#Compatibility">Upgrade and Compatibility Information</a>  
 *    <li> <a href="#Device_Support">Device Support</a>                        
 *    <li> <a href="#Validation">Validation Information</a>                    
 *    <li> <a href="#Known_Issues">Known Issues</a>                            
 *    <li> <a href="#Support">Technical Support</a>                            
 *    <li> <a href="#Component">Component Release Notes</a>                    
 *  </ol>
 *  
 *  
 * <hr size="2" width="50%" align="left">
 *
 * <h2><a name="Introduction">Introduction</a></h2>
 *
 *  <p> The FFTLIB is an optimized library for computing the discrete Fourier transform (DFT).
 *  - Supports C66x TI DSP platform for little-endian
 *  - Supports single-precision and double-precision floating point
 *  - Supports complex inputs and real inputs
 *  - Supports 1D, 2D and 3D FFT
 *  - Supports Single-core and Multi-core
 *  - API similar to FFTW, includes FFT plan and FFT execute
 *  </p>
 *
 * <hr size="2" width="50%" align="left">
 *
 * <h2><a name="Starting">Getting Started</a></h2>
 *
 *  The <a href="./docs/FFTLIB_Users_Manual.html">FFTLIB User's Manual</a> provides 
 *  the documentation and references necessary to integrate FFTLIB Library routines 
 *  into a development platform.
 * 
 *
 * <hr size="2" width="50%" align="left">
 *
 * <h2><a name="Documentation">Documentation</a></h2>
 *
 *  The following documentation is available:
 *
 *  <table frame="box" rules="all" cellspacing="0" width="50%">
 *    <tr>
 *      <td valign="top"><b>User's Manual</b></td>
 *      <td align="center"><a href="./docs/FFTLIB_Users_Manual.html">[HTML]</a><br>
 *      </td>
 *    </tr>
 *    <tr>
 *      <td valign="top"><b>Software Manifest</b></td>
 *      <td align="center"><a href="./docs/FFTLIB_Software_Manifest.html">[HTML]</a><br>
 *      </td>
 *    </tr>
 *    <tr>
 *      <td valign="top"><b>Release Notes Archive</b></td>
 *      <td align="center"><a href="./docs/relnotes_archive">[LINK]</a>
 *      </td>
 *    </tr>
 *  </table>
 *
 *
 * <hr size="2" width="50%" align="left">
 *
 * <h2><a name="Whats_New">What's New</a></h2>
 *  
 *  The following new features were added:
 *  <ol>
 *    <li> Initial bundle release
 *  </ol>
 *
 *  The following bugs have been resolved:
 *  <ol>
 *    <li> N/A
 *  </ol>
 *
 *
 * <hr size="2" width="50%" align="left">
 *
 * <h2><a name="Compatibility">Upgrade and Compatibility Information</a></h2>
 *
 *  This version of <b>FFTLIB</b> is compatible with the last released 
 *  version: 
 *
 *  <table rules="all" frame="box" cellspacing="2" cellpadding="2" frame="void" width="10%">
 *    <tr>
 *      <th bgcolor="#cccccc">YES</th>
 *      <td align="center"> <b></b> </td>
 *    </tr>
 *    <tr>
 *      <th bgcolor="#cccccc">NO</th>
 *      <td align="center"> <b>X</b> </td>
 *    </tr>
 *  </table>
 * 
 *  <table bgcolor="#ffff99" frame="box" rules="none" cellspacing="0" width="80%">
 *    <tr>
 *      <td valign="top"><b>Note:</b></td>
 *      <td>
 *        <ol>
 *          <li> Package compatibility keys are independent of component release 
 *               versions. For more information visit: 
 *               <a href="#VersionCompat">Package Versioning and Compatibility</a>
 *        </ol>
 *      </td>
 *    </tr>
 *  </table>
 *        
 * 
 * <hr size="2" width="50%" align="left">
 *
 * <h2><a name="Device_Support">Device Support</a></h2>
 *
 *  This release supports the following device families: 
 *
 *  <ul>
 *    <li> C66x DSP (ELF, Little-Endian format)
 *  </ul>
 *
 *
 * <hr size="2" width="50%" align="left">
 *
 * <h2><a name="Validation">Validation Information</a></h2>
 *
 *  This release was built and validated using the following tools:
 *
 *  <ul>
 *    <li> XDC Tools version:  3.25.05.94
 *    <li> RTSC Packaging Standard version:  2.0
 *    <li> C6000 Code Generation Tools version:  7.4.2
 *    <li> Code Composer Studio version:  V5.4
 *    <li> GNU make version:  3.81
 *  </ul>
 *
 *
 * <hr size="2" width="50%" align="left">
 *
 * <h2><a name="Known_Issues">Known Issues</a></h2>
 *
 *  <ol>
 *    <li> N/A
 *  </ol>
 *
 *
 * <hr size="2" width="50%" align="left">
 *
 * <h2><a name="Support">Technical Support</a></h2>
 *
 *  Questions regarding the FFTLIB library should be directed to the 
 *  <a href="http://e2e.ti.com/support/embedded/tirtos/f/355.aspx">TI-RTOS forum</a>.
 *  Please include the text "FFTLIB" in the title 
 *  and add "C66x" and "FFTLIB" tags to your post.
 *
 * <hr size="2" width="50%" align="left">
 *   
 *  <h2><a name="Component">Component Release Notes</a></h2>
 * 
 *  <ol>
 *    <li> <a href="./packages/ti/fftlib/fftlib_3_1_0_0_release_notes.html">FFTLIB Release Notes</a>
 *  </ol>
 *
 *  <p></p>
 *
 * <hr size="2" width="50%" align="left">
 * <hr size="2" width="50%" align="left">
 *
 * <h2><a name="VersionCompat">Package Versioning and Compatibility</a></h2>
 *
 * <h3>Compatibility Keys</h3>
 *  
 *  Compatibility keys serve two primary purposes: 
 *   
 *  <ol>
 *    <li> Enable tooling to identify incompatibilities between components
 *    <li> Convey a level of compatibility between different releases to set 
 *         end user expectations. 
 *  </ol>
 * 
 *  Each compatibility key is composed of 4 comma-delimited numbers - represented here 
 *  by the letters M, S, R and P <kbd><b>[M,S,R,P]</b></kbd>.  The table below provides a descriptive reference for 
 *  compatibility key intention.
 *
 *  <table rules="all" frame="box" cellspacing="2" cellpadding="2" frame="void" width="80%">
 *    <caption align="bottom">Compatibility Key Reference</caption>
 *    <tr valign="top" bgcolor="#cccccc">
 *      <th width="10%"> Key </th>
 *      <th width="20%"> Meaning </th>
 *      <th width="70%"> Description </th>
 *    </tr>
 *    <tr valign="top">
 *      <td>1&nbsp;(<b>M</b>=Major)</td>
 *      <td>Break in compatibility</td>
 *      <td>The package consumer is required to re-write the calling source code to use the package</td>
 *    </tr>
 *    <tr valign="top">
 *      <td>2&nbsp;(<b>S</b>=Source)</td>
 *      <td>Source compatibility</td>
 *      <td>The package consumerís source code doesn't require change, but does require a recompile</td>
 *    </tr>
 *    <tr valign="top">
 *      <td>3&nbsp;(<b>R</b>=Radix)</td>
 *      <td>Introduction of new features</td>
 *      <td>Compatibility with previous interfaces is not broken. If libraries are provided by the package, 
 *          an application must re-link with the new libraries, but is not required to recompile its source</td>
 *    </tr>
 *    <tr valign="top">
 *      <td>4&nbsp;(<b>P</b>=Patch)</td>
 *      <td>Bug fixes only</td>
 *      <td>If libraries are provided by the package, an application must re-link with the new libraries, 
 *          but is not required to recompile its source. </td>
 *    </tr>
 *  </table>
 *
 *
 * <hr size="2" width="50%" align="left">
 *
 *
 * <h3>Versioning</h3>
 *  
 *  Each package version is composed of 4 period-delimited numbers - represented here 
 *  by the letters M, m, p and b <kbd><b>[M.m.p.b]</b></kbd>.  The table below provides a descriptive 
 *  reference regarding package version numbering.
 *
 *  <table rules="all" frame="box" cellspacing="4" cellpadding="4" frame="void" width="80%">
 *    <caption align="bottom">Package Version Reference</caption>
 *    <tr valign="top" bgcolor="#cccccc">
 *      <th width="10%"> Digit </th>
 *      <th width="20%"> Meaning </th>
 *      <th width="70%"> Description </th>
 *    </tr>
 *    <tr valign="top">
 *      <td>1&nbsp;(<b>M</b>=Major)</td>
 *      <td>Major revision</td>
 *      <td>Incremented when the new version is substantially different from the previous  
 *          For example, a new module added or an existing module's algorithm significantly 
 *          altered. </td>
 *    </tr>
 *    <tr valign="top">
 *      <td>2&nbsp;(<b>m</b>=minor)</td>
 *      <td>Minor revision</td>
 *      <td>Incremented when the new version has changed but not in a major way. For example, 
 *          some minor changes in the API or feature set. </td>
 *    </tr>
 *    <tr valign="top">
 *      <td>3&nbsp;(<b>p</b>=patch)</td>
 *      <td>Patch number</td>
 *      <td>Incremented for all other source code changes. This include any packaging support 
 *          code. </td>
 *    </tr>
 *    <tr valign="top">
 *      <td>4&nbsp;(<b>b</b>=build)</td>
 *      <td>Build number</td>
 *      <td>Incremented for each release delivery to CM. Reset for any change to M, m or p</td>
 *    </tr>
 *  </table>
 *
 */

