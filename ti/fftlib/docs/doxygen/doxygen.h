/* ======================================================================== *
 * FFTLIB -- TI FFT Library                                                *
 *                                                                          *
 *   Doxygen generation header file                                         *
 *                                                                          *
 * Copyright (C) 2014 Texas Instruments Incorporated - http://www.ti.com/   *
 *                                                                          *
 *                                                                          *
 *  Redistribution and use in source and binary forms, with or without      *
 *  modification, are permitted provided that the following conditions      *
 *  are met:                                                                *
 *                                                                          *
 *    Redistributions of source code must retain the above copyright        *
 *    notice, this list of conditions and the following disclaimer.         *
 *                                                                          *
 *    Redistributions in binary form must reproduce the above copyright     *
 *    notice, this list of conditions and the following disclaimer in the   *
 *    documentation and/or other materials provided with the                *
 *    distribution.                                                         *
 *                                                                          *
 *    Neither the name of Texas Instruments Incorporated nor the names of   *
 *    its contributors may be used to endorse or promote products derived   *
 *    from this software without specific prior written permission.         *
 *                                                                          *
 *  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS     *
 *  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT       *
 *  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR   *
 *  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT    *
 *  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,   *
 *  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT        *
 *  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,   *
 *  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY   *
 *  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT     *
 *  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE   *
 *  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.    *
 * ======================================================================== */

/**
 * @mainpage  FFTLIB Function Reference
 *
 *
 * @HLINE
 *
 *
 * @section content  Contents
 *  
 *  <ol>
 *    <li> @ref intro
 *    <li> @ref conventions
 *    <li> @ref funcref
 *      <ul>
 *        <li> @subpage one_dimension
 *        <li> @subpage two_dimensions
 *        <li> @subpage three_dimensions
 *      </ul>
 *  </ol>
 *  
 *  
 * @HLINE
 *
 *
 * @section intro  Introduction
 *
 *  FFTLIB provides a collection of C-callable high-performance routines consisting
 *  of 1d, 2d, & 3d double precision & single Precision Fast Fourier Transforms. 
 *  These functions exploit the high performance and multiprocessing capabilities of 
 *  Texas Instruments C66x DSPs. 
 *  
 *  All functions in FFTLIB have been developed for a little-endian memory model. 
 *  Some may work in a big-endian memory model as well, however, their functionality 
 *  is not guaranteed.
 *
 *
 * @HLINE
 *
 *
 * @section conventions  Notational Conventions
 *
 *  All functions within FFTLIB following the naming conventions described below.
 *  
 *    -#  Each function begins with the prefix <tt>fft_</tt> or <tt>ifft_</tt>.  
 *        This serves to prevent namespace conflicts.
 *    -#  The prefix is followed by a <tt>omp_</tt> for multiprocessor routines.  
 *        This descriptor highlights routines designed to run on multiple cores
 *        utilizing OpenMP.
 *    -#  Next, single <tt>sp_</tt> or double precision <tt>dp_</tt> is designated, 
 *        followed by the dimensions supported <tt>1d_</tt>, <tt>2d_</tt>, or <tt>3d_</tt>
 *        by the fft.
 *    -#  Finally, input and output are defined by real <tt>r</tt> or complex <tt>c</tt>.
 *          - Real to Complex:    <tt>r2c</tt>
 *          - Complex to Complex: <tt>c2c</tt>
 *          - Complex to Real:    <tt>c2r</tt>
 *
 *
  * @HLINE
 *
 *
 * @section funcref  Functional Reference
 *
 *  The following sections provide a detailed reference of all functions within 
 *  FFTLIB.  The functions are organized into the following three categories:
 *
 *  - @subpage one_dimension
 *  - @subpage two_dimensions
 *  - @subpage three_dimensions
 * 
 */
 
 /**
 * @page one_dimension 1-dimensional FFTs
 *
 *
 * @HLINE
 *
 *  - @ref FFT_DP_1D_C2C
 *  - @ref FFT_DP_1D_C2C_BATCH
 *  - @ref FFT_DP_1D_R2C
 *  - @ref FFT_DP_1D_R2C_BATCH
 *  - @ref FFT_OMP_DP_1D_C2C
 *  - @ref FFT_OMP_DP_1D_R2C
 *  - @ref FFT_OMP_SP_1D_C2C
 *  - @ref FFT_OMP_SP_1D_R2C
 *  - @ref FFT_SP_1D_C2C
 *  - @ref FFT_SP_1D_C2C_BATCH
 *  - @ref FFT_SP_1D_R2C
 *  - @ref FFT_SP_1D_R2C_BATCH
 *  - @ref IFFT_DP_1D_C2C
 *  - @ref IFFT_DP_1D_C2C_BATCH
 *  - @ref IFFT_DP_1D_C2R
 *  - @ref IFFT_DP_1D_C2R_BATCH
 *  - @ref IFFT_OMP_DP_1D_C2C
 *  - @ref IFFT_OMP_DP_1D_C2R
 *  - @ref IFFT_OMP_SP_1D_C2C
 *  - @ref IFFT_OMP_SP_1D_C2R
 *  - @ref IFFT_SP_1D_C2C
 *  - @ref IFFT_SP_1D_C2C_BATCH
 *  - @ref IFFT_SP_1D_C2R
 *  - @ref IFFT_SP_1D_C2R_BATCH
 *
 */
 
  
 /**
 * @page two_dimensions 2-dimensional FFTs
 *
 *
 * @HLINE
 *
 *  - @ref FFT_DP_2D_C2C
 *  - @ref FFT_DP_2D_R2C
 *  - @ref FFT_OMP_DP_2D_C2C
 *  - @ref FFT_OMP_DP_2D_R2C
 *  - @ref FFT_OMP_SP_2D_C2C
 *  - @ref FFT_OMP_SP_2D_R2C
 *  - @ref FFT_SP_2D_C2C
 *  - @ref FFT_SP_2D_R2C
 *  - @ref IFFT_DP_2D_C2C
 *  - @ref IFFT_OMP_DP_2D_C2C
 *  - @ref IFFT_OMP_SP_2D_C2C
 *  - @ref IFFT_SP_2D_C2C
 *
 */
 
  /**
 * @page three_dimensions 3-dimensional FFTs
 *
 *
 * @HLINE
 *
 *  - @ref FFT_DP_3D_C2C
 *  - @ref FFT_DP_3D_R2C
 *  - @ref FFT_OMP_DP_3D_C2C
 *  - @ref FFT_OMP_DP_3D_R2C
 *  - @ref FFT_OMP_SP_3D_C2C
 *  - @ref FFT_OMP_SP_3D_R2C
 *  - @ref FFT_SP_3D_C2C
 *  - @ref FFT_SP_3D_R2C
 *  - @ref IFFT_DP_3D_C2C
 *  - @ref IFFT_OMP_DP_3D_C2C
 *  - @ref IFFT_OMP_SP_3D_C2C
 *  - @ref IFFT_SP_3D_C2C
 *
 */
 
 