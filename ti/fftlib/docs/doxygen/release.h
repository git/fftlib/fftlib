/* ======================================================================== *
 * FFTLIB -- TI FFT Library                                                 *
 *                                                                          *
 * ======================================================================== */
  
/**
 *  @mainpage 
 *  <p align="center"> 
 *  <a href="#Introduction">Introduction</a>,
 *  <a href="#Documentation">Documentation</a>,
 *  <a href="#Whats_New">What's New</a>
 *  <a href="#Compatibility">Upgrade and Compatibility Information</a>,
 *  <a href="#Host_Support">Host Support</a>,
 *  <a href="#Device_Support">Device Support</a>,
 *  <a href="#Validation">Validation Information</a>,
 *  <a href="#Known_Issues">Known Issues</a>,
 *  <a href="#Version">Version Information</a>,
 *  <a href="#Support">Technical Support</a>
 *  </p>
 *
 *  <!--**************************************************************************************************-->
 *  <!--**************************************************************************************************-->
 *  <!--***********************************Introduction***************************************************-->
 *  <!--**************************************************************************************************-->
 *  <!--**************************************************************************************************-->
 *
 *
 *  <hr> 
 *  <h2><a name="General_Info">Introduction</a></h2>
 *  <p> The TI FFTLIB is an optimized library for computing the discrete Fourier transform (DFT).
 *  - Supports C66x TI DSP platform for little-endian
 *  - Supports single-precision and double-precision floating point
 *  - Supports complex inputs and real inputs
 *  - Supports 1D, 2D and 3D FFT
 *  - Supports Single-core and Multi-core
 *  - API similar to FFTW, includes FFT plan and FFT execute
 *  </p>
 *
 *  <a href="#XDC_TOP">back to top</a>
 *
 *  <!--**************************************************************************************************-->
 *  <!--**************************************************************************************************-->
 *  <!--***********************************Documentation**************************************************-->
 *  <!--**************************************************************************************************-->
 *  <!--**************************************************************************************************-->
 *
 *  <hr>
 *
 *  <h2><a name="Documentation">Documentation</a></h2>
 *  <p>The following documentation is available: </p> 
 *  - <a href="docs/doxygen/FFTLIB_Function_Reference.chm">FFTLIB Function Reference</a>
 *
 *  <p>Release notes from previous releases are also available in the
 *  <a href="docs/relnotes_archive">relnotes_archive</a>
 *  directory.</p>
 *
 *  <a href="#XDC_TOP">back to top</a> 
 *
 *  <!--**************************************************************************************************-->
 *  <!--**************************************************************************************************-->
 *  <!--*************************************What's New***************************************************-->
 *  <!--**************************************************************************************************-->
 *  <!--**************************************************************************************************-->
 *  <hr>
 *
 *
 *  <h2><a name="Whats_New">What's New</a></h2>
 *  <p> The following new features were added:  
 *  - Linux Build support
 *  </p>
 *
 *  <p> The following bugs have been resolved:
 *  - N/A
 *  </p>
 *
 *
 *  <a href="#XDC_TOP">back to top</a>
 *
 *  <!--**************************************************************************************************-->
 *  <!--**************************************************************************************************-->
 *  <!--************************Upgrade and Compatibility Information*************************************-->
 *  <!--**************************************************************************************************-->
 *  <!--**************************************************************************************************-->
 *  <hr>
 *
 *  <h2><a name="Compatibility">Upgrade and
 *  Compatibility Information</a></h2>
 *
 *  This <b>ti.fftlib</b> is the initial RTSC packaged version.  
 *
 *
 *  Please note that the package compatibility keys are independent of XDC product release numbers. 
 * 
 *  Package compatibility keys are intended to: 
 * 
 *   -# Enable tooling to identify incompatibilities between components, and 
 *   -# Convey a level of compatibility between different releases to set end user expectations. 
 * 
 * Package compatibility keys are composed of 4 comma-delimited numbers - M, S, R, P - where: 
 *
 * - <b>M = Major</b> - A difference in M indicates a break in compatibility. The package consumer is required to re-write its source code in order to use the package.
 * - <b>S = Source</b> - A difference in S indicates source compatibility. The package consumerís source code doesn't require change, but does require a recompile. 
 * - <b>R = Radix</b> - A difference in R indicates an introduction of new features, but compatibility with previous interfaces is not broken. If libraries are provided by the package, an application must re-link with the new libraries, but is not required to recompile its source.
 * - <b>P = Patch</b> - A difference in P indicates that only bugs have been fixed in the latest package and no new features have been introduced. If libraries are provided by the package, an application must re-link with the new libraries, but is not required to recompile its source. 
 *
 *  <a href="#XDC_TOP">back to top</a>
 *  <!--**************************************************************************************************-->
 *  <!--**************************************************************************************************-->
 *  <!--****************************************Host Support**********************************************-->
 *  <!--**************************************************************************************************-->
 *  <!--**************************************************************************************************-->

 *  <hr>
 *
 *  <h2><a name="Host_Support">Host Support</a></h2>
 *
 *  This release supports the following hosts:
 *
 *   - Windows XP 
 *   - Linux
 *
 *  <a href="#XDC_TOP">back to top</a>
  *  <!--**************************************************************************************************-->
 *  <!--**************************************************************************************************-->
 *  <!--**********************************Dependencies******************************************-->
 *  <!--**************************************************************************************************-->
 *  <!--**************************************************************************************************-->

 *  <hr>
 *
 *  <h2><a name="Dependencies">Dependencies</a></h2>
 *
 *  This release requires the following other software components and tools versions to successfully function
 *  - MCSDK 3.0.4.17
 *  - MCSDK-HPC 3.0.0.18
 *  - XDC Tools version 3.25.05.94
 *  - C6000 Code Generation Tools version 7.4.2
 *  - CCS version 5.4
 *
 *  <a href="#XDC_TOP">back to top</a>
 *  <!--**************************************************************************************************-->
 *  <!--**************************************************************************************************-->
 *  <!--*****************************************Device Support*******************************************-->
 *  <!--**************************************************************************************************-->
 *  <!--**************************************************************************************************-->
 *  <hr>
 *
 *  <h2><a name="Device_Support">Device Support</a></h2>
 *
 *  This release supports the following device families: 
 *  - C66x 
 *
 *  <a href="#XDC_TOP">back to top</a>
 *
 *  <!--**************************************************************************************************-->
 *  <!--**************************************************************************************************-->
 *  <!--**********************************Validation Information******************************************-->
 *  <!--**************************************************************************************************-->
 *  <!--**************************************************************************************************-->
 *

 *  <hr>
 *
 *  <h2><a name="Validation">Validation Information</a></h2>
 *
 *  This release was built and validated using CCS version 5.4.
 *
 *
 *  <a href="#XDC_TOP">back to top</a>
 *  <!--**************************************************************************************************-->
 *  <!--**************************************************************************************************-->
 *  <!--***********************************Known Issues***************************************************-->
 *  <!--**************************************************************************************************-->
 *  <!--**************************************************************************************************-->

 *  <hr>
 *
 *  <h2><a name="Known_Issues">Known Issues</a></h2>
 *   None
 *
 *
 *  <a href="#XDC_TOP">back to top</a>
 *
 *  <hr>
 *
 *  <!--**************************************************************************************************-->
 *  <!--**************************************************************************************************-->
 *  <!--**************************************** Versioning **********************************************-->
 *  <!--**************************************************************************************************-->
 *  <!--**************************************************************************************************-->

 *  <h2><a name="Version">Version Information</a></h2>
 *
 *  This product's version follows a version format, <b>M.m.p.b</b>,
 *  where <b>M</b> is a single digit Major number, <b>m</b> is single digit minor number,
 *  <b>p</b> is a single digit patch number and <b>b</b> is an unrestricted set of digits used as an incrementing build counter. 
 
 *  <p>Please note that version numbers and compatibility keys are
 *  NOT the same. For an explanation of compatibility keys, please refer to
 *  the '<a href="#Compatibility">Upgrade and Compatibility Information</a>' section.
 *
 *  <a href="#XDC_TOP">back to top</a>
 *  <!--**************************************************************************************************-->
 *  <!--**************************************************************************************************-->
 *  <!--*********************************** Technical Support ********************************************-->
 *  <!--**************************************************************************************************-->
 *  <!--**************************************************************************************************-->
 *  <hr>
 *  <h2><a name="Support">Technical Support</a></h2>
 *
 *  Questions regarding the FFTLIB library should be directed to the
 *  <a href="http://e2e.ti.com/support/embedded/tirtos/f/355.aspx">TI-RTOS forum</a>.
 *  Please include the text FFTLIB in the title and add FFTLIB tag to your post.
 *
 *  <a href="#XDC_TOP">back to top</a>
 *
 *
 *
 */
