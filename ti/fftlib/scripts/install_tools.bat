@echo off
:: *************************************************************************
::  FILE           : install_tools.bat
::  DESCRIPTION    :
::
::     This batch script installs all tools required to
::     build a FFTLIB release.
::
:: *************************************************************************

@echo Executing:  %~fn0

set PATH=c:/tools;%PATH%

echo %PATH% > %~dp0/../path.txt
:: *************************************************************************
:: *** Specify install locations
:: *************************************************************************
set fftdrive=%1
if not exist %fftdrive%\NUL (
    echo "Drive/directory %fftdrive% does not exist."
    set fftdrive=C:
)

if "%fftdrive%" EQU "" (
    set FFTLIB_WORKING_DIR=C:\FFTLIB_Install
    set FFTLIB_INSTALL_DIR=C:/FFTLIB_Tools
    set FFTLIB_INSTALL_DOS_DIR=C:\FFTLIB_Tools
) else (
    set FFTLIB_WORKING_DIR=%fftdrive%\FFTLIB_Install
    set FFTLIB_INSTALL_DIR=%fftdrive%/FFTLIB_Tools
    set FFTLIB_INSTALL_DOS_DIR=%fftdrive%\FFTLIB_Tools
)

:: *************************************************************************
:: *** Specify tool versions
:: *************************************************************************
set XDC_VERSION=3_25_05_94
set CGT_VER_UND=7_4_2
set CGT_VER_DOT=7.4.2
set CC5_VER_UND=5_4_0
set CC5_VER_DOT=5.4.0
set CC5_VER_FUL=5.4.0.00091
set IJM_VERSION=1_2_15
set CYG_VERSION=99-11-01
set PHP_VERSION=5_3_2_1000
set DOX_VERSION=1.5.1-p1
set GVZ_VERSION=2.12
set HHW_VERSION=10-01-2007
set TIT_VERSION=10-01-2007
set EPI_VERSION=20091203
set XDS_VERSION=7_23_00_06
set DSPLIB_VERSION=3_1_1_1
set MCSDK_VERSION=2_01_02_06
set MCSDK_VERSION_K2H=3_00_04_17
set BIOS_VERSION=6_37_00_20
set EDMA3_VERSION=02_11_11_15
set XDAIS_VERSION=7_24_00_04
set FRC_VERSION=3_30_01_08
set OMP_VERSION=2_01_12_00
set PDK_VERSION=1.1.2.6
set PDK_VERSION_K2H=3.0.4.17
set IPC_VERSION=3_00_04_29


:: *************************************************************************
:: *** Specify install locations
:: *************************************************************************
set XDC_INSTALL_DIR=%FFTLIB_INSTALL_DIR%/xdctools_%XDC_VERSION%
set BIOS_INSTALL_DIR=%FFTLIB_INSTALL_DIR%/bios_%BIOS_VERSION%
set EDMA3_INSTALL_DIR=%FFTLIB_INSTALL_DIR%
set CC5_INSTALL_DIR=%FFTLIB_INSTALL_DIR%/CCSV%CC5_VER_UND%
set CGT_INSTALL_DIR=%CC5_INSTALL_DIR%/ccsv5/tools/compiler/C6000
set DOX_INSTALL_DIR=%FFTLIB_INSTALL_DIR%
set DOX_INSTALL_DOS_DIR=%FFTLIB_INSTALL_DOS_DIR%
set IJM_INSTALL_DIR=%FFTLIB_INSTALL_DIR%
set CYG_INSTALL_DIR=%FFTLIB_INSTALL_DIR%
set PHP_INSTALL_DIR=%FFTLIB_INSTALL_DIR%
set IPC_INSTALL_DIR=%FFTLIB_INSTALL_DIR%
set EPI_INSTALL_DIR=%FFTLIB_INSTALL_DIR%
set DSPLIB_INSTALL_DIR=%FFTLIB_INSTALL_DIR%/dsplib_c66x_%DSPLIB_VERSION%
set LIBARCH_INSTALL_DIR=%FFTLIB_INSTALL_DIR%/ti-libarch-tree
set MCSDK_INSTALL_DIR=%FFTLIB_INSTALL_DIR%
set XDAIS_INSTALL_DIR=%FFTLIB_INSTALL_DIR%
set FRC_INSTALL_DIR=%FFTLIB_INSTALL_DIR%
set OMP_INSTALL_DIR=%FFTLIB_INSTALL_DIR%
set FFTLIB_TOOLS=%FFTLIB_INSTALL_DIR%


:: *************************************************************************
:: *** Specify WGET locations
:: *************************************************************************
::  set XDC_WGET_URL="http://software-dl.ti.com/dsps/dsps_public_sw/sdo_sb/targetcontent/rtsc/%XDC_VERSION%/exports"
set XDC_WGET_URL="http://www.sanb.design.ti.com/tisb_releases/XDCtools/%XDC_VERSION%/exports"
set CGT_WGET_URL="http://syntaxerror.dal.design.ti.com/release/releases/c60/rel%CGT_VER_UND%/build/install"
set CC5_WGET_URL="http://software-dl.ti.com/dsps/dsps_public_sw/sdo_ccstudio/CCSv5/CCS_%CC5_VER_UND%/exports"
set DOX_WGET_URL="http://tigt_qa.gt.design.ti.com/qacm/test_area/nightlytools/doxygen"
set IJM_WGET_URL="http://tigt_qa.gt.design.ti.com/qacm/test_area/nightlytools/installjammer"
set CYG_WGET_URL="http://tigt_qa.gt.design.ti.com/qacm/test_area/nightlytools/cygwin"
set PHP_WGET_URL="http://tigt_qa.gt.design.ti.com/qacm/test_area/nightlytools/php"
set EPI_WGET_URL="http://tigt_qa.gt.design.ti.com/qacm/test_area/nightlytools/xdc"
set DSPLIB_WGET_URL="http://software-dl.ti.com/sdoemb/sdoemb_public_sw/dsplib/%DSPLIB_VERSION%/exports"
set MCSDK_WGET_URL="http://software-dl.ti.com/sdoemb/sdoemb_public_sw/bios_mcsdk/0%MCSDK_VERSION%/exports"
set MCSDK_WGET_URL_K2H="http://tigt_qa.gt.design.ti.com/qacm/test_area/MCSDK/03.00.04/%MCSDK_VERSION_K2H%/exports"
::set MCSDK_WGET_URL_K2H="http://software-dl.ti.com/sdoemb/sdoemb_public_sw/mcsdk/latest/exports"
::set MCSDK_WGET_URL_K2H="http://software-dl.ti.com/sdoemb/sdoemb_public_sw/mcsdk/0%MCSDK_VERSION_K2H%/exports"
set XDAIS_WGET_URL="http://www.sanb.design.ti.com/tisb_releases/XDAIS/%XDAIS_VERSION%/exports"
set FRC_WGET_URL="http://www.sanb.design.ti.com/tisb_releases/Framework_Components/%FRC_VERSION%/exports"
:: set OMP_WGET_URL="http://tigt_dev.gt.design.ti.com/dev/OPENMP_RUNTIME_2.0/%OMP_VERSION%"
::set OMP_WGET_URL="http://tigt_dev.gt.design.ti.com/dev/OPENMP_RUNTIME_2.0/%OMP_VERSION%_BETA"
set OMP_WGET_URL="http://gtjenkins.itg.ti.com/nightly_builds/openmp-runtime/25-2014-04-04_14-56-03/artifacts/output/"
set BIOS_WGET_URL="http://software-dl.ti.com/dsps/dsps_public_sw/sdo_sb/targetcontent/bios/sysbios/%BIOS_VERSION%/exports"
set EDMA3_WGET_URL="http://bangsdowebsvr01.india.ti.com/edma3_lld/edma3-lld-bios6/%EDMA3_VERSION%/exports"
set IPC_WGET_URL="http://www.sanb.design.ti.com/tisb_releases/IPC/%IPC_VERSION%/exports"

:: *************************************************************************
:: *** Specify WGET packages
:: *************************************************************************
set XDC_WGET_PKG=xdctools_setupwin32_%XDC_VERSION%.exe
set CC5_WGET_PKG=ccs_setup_%CC5_VER_FUL%.exe
set CGT_WGET_PKG=ti_cgt_c6000_%CGT_VER_DOT%_setup_win32.exe
set DSPLIB_WGET_PKG=dsplib_c66x_%DSPLIB_VERSION%_Win32.exe
set MCSDK_WGET_PKG=bios_mcsdk_0%MCSDK_VERSION%_setupwin32.exe
set MCSDK_WGET_PKG_K2H=mcsdk_%MCSDK_VERSION_K2H%_setupwin32.exe
set DOX_WGET_PKG=doxygen.zip
set IJM_WGET_PKG=installjammer_v%IJM_VERSION%.zip
set CYG_WGET_PKG=cygwin-%CYG_VERSION%.zip
set PHP_WGET_PKG=PHP_%PHP_VERSION%.zip
set EPI_WGET_PKG=xdc_eclipse_plugin_gen.zip
set XDAIS_WGET_PKG=xdais_%XDAIS_VERSION%.tar.gz
:: set FRC_WGET_PKG=framework_components_%FRC_VERSION%_eng.tar.gz
set FRC_WGET_PKG=framework_components_%FRC_VERSION%.tar.gz
set OMP_WGET_PKG=openmp_dsp_%OMP_VERSION%.zip
set BIOS_WGET_PKG=bios_setupwin32_%BIOS_VERSION%.exe
set EDMA3_WGET_PKG=edma3_lld_%EDMA3_VERSION%.tar.gz
set IPC_WGET_PKG=ipc_%IPC_VERSION%.zip

:: *************************************************************************
:: ** These are required for local build without tools support (T: drive)
:: *************************************************************************
set CCSVERSION=CCSV5
set CCSV5_INSTALL_DIR=%CC5_INSTALL_DIR%/ccsv5
set CCSCGTVER=%CGT_VER_DOT%
set C66CODEGENTOOL=%CGT_INSTALL_DIR%
set C64CODEGENTOOL=%CGT_INSTALL_DIR%
set C674CODEGENTOOL=%CGT_INSTALL_DIR%
set TI_DOXYGEN_TEMPLATES=%DOX_INSTALL_DIR%/Doxygen/TI_Templates/%TIT_VERSION%
pushd

:: *************************************************************************
:: *************************************************************************
:: ** Start installation process
:: *************************************************************************
:: *************************************************************************
@echo .
@echo #######################################################################
@echo ##  Installing Required Tools
@echo #######################################################################
@echo .

:: *************************************************************************
:: *** Create installation directories
:: *************************************************************************
if not exist "%FFTLIB_WORKING_DIR%" mkdir "%FFTLIB_WORKING_DIR%"
pushd %FFTLIB_WORKING_DIR%

if not exist "%FFTLIB_INSTALL_DIR%" mkdir "%FFTLIB_INSTALL_DIR%"

:: *************************************************************************
:: ** Install MCSDK
:: *************************************************************************
if exist %MCSDK_WGET_PKG% goto mcsdk_install
@echo WGET:  %MCSDK_WGET_PKG%
wget -nc --no-proxy "%MCSDK_WGET_URL%/%MCSDK_WGET_PKG%"
ren "%MCSDK_WGET_PKG%*" "%MCSDK_WGET_PKG%"
if not exist %MCSDK_WGET_PKG% goto mcsdk_error
:mcsdk_install
@echo Install:  %MCSDK_WGET_PKG%
if exist %MCSDK_INSTALL_DIR%/mcsdk_%MCSDK_VERSION% goto mcsdk_exist
%MCSDK_WGET_PKG% --prefix %MCSDK_INSTALL_DIR% --mode silent
:mcsdk_exist
@echo ......... %MCSDK_WGET_PKG%  Installed
@echo .

if exist "%MCSDK_INSTALL_DIR%/openem_1_0_0_2" rmdir /s /q "%MCSDK_INSTALL_DIR%/openem_1_0_0_2"
if exist "c:/ti/openem_1_0_0_2" rmdir /s /q "c:/ti/openem_1_0_0_2"

if exist "%MCSDK_INSTALL_DIR%/ndk_2_21_01_38" rmdir /s /q "%MCSDK_INSTALL_DIR%/ndk_2_21_01_38"
if exist "c:/ti/ndk_2_21_01_38" rmdir /s /q "c:/ti/ndk_2_21_01_38"

:: *************************************************************************
:: ** Install MCSDK for Hawking
:: *************************************************************************
if exist %MCSDK_WGET_PKG_K2H% goto mcsdk_install_k2h
@echo WGET:  %MCSDK_WGET_PKG_K2H%
wget -nc --no-proxy "%MCSDK_WGET_URL_K2H%/%MCSDK_WGET_PKG_K2H%"
ren "%MCSDK_WGET_PKG_K2H%*" "%MCSDK_WGET_PKG_K2H%"
if not exist %MCSDK_WGET_PKG_K2H% goto mcsdk_error_k2h
:mcsdk_install_k2h
@echo Install:  %MCSDK_WGET_PKG_K2H%
if exist %MCSDK_INSTALL_DIR%/mcsdk_bios_%MCSDK_VERSION_K2H% goto mcsdk_exist_k2h
%MCSDK_WGET_PKG_K2H% --prefix %MCSDK_INSTALL_DIR% --mode silent
:mcsdk_exist_k2h
@echo ......... %MCSDK_WGET_PKG_K2H%  Installed
@echo .

:: *************************************************************************
:: ** Install XDC
:: *************************************************************************
if exist %XDC_WGET_PKG% goto xdc_install
@echo WGET:  %XDC_WGET_PKG%
wget -nc --no-proxy %XDC_WGET_URL%/%XDC_WGET_PKG%
if not exist %XDC_WGET_PKG% goto xdc_error
:xdc_install
@echo Install:  %XDC_WGET_PKG%
if exist %XDC_INSTALL_DIR% goto xdc_exist
%XDC_WGET_PKG% --prefix %XDC_INSTALL_DIR% --mode silent
:xdc_exist
@echo ......... %XDC_WGET_PKG%  Installed
@echo .

:: *************************************************************************
:: ** Set the PATH
:: *************************************************************************
set PATH=%XDC_INSTALL_DIR%;%PATH%
set PATH=%XDC_INSTALL_DIR%/bin;%PATH%
set PATH=%XDC_INSTALL_DIR%/bin;%PATH%

if "%fftdrive%" NEQ "" (
  sed  s/lC:/l%fftdrive%/ %~dp0\..\src\common\nonmp\lnk.cmd > %~dp0\..\src\common\nonmp\lnknew.cmd 2>&1
  sed  s/lC:/l%fftdrive%/ %~dp0\..\src\common\omp\lnk.cmd > %~dp0\..\src\common\omp\lnknew.cmd 2>&1
  del /s /q "%~dp0\..\src\common\*lnk.cmd
  ren "%~dp0\..\src\common\nonmp\lnknew.cmd" "lnk.cmd"
  ren "%~dp0\..\src\common\omp\lnknew.cmd" "lnk.cmd"
)

:: *************************************************************************
:: ** Install BIOS
:: *************************************************************************
if exist %BIOS_WGET_PKG% goto bios_install
@echo WGET:  %BIOS_WGET_PKG%
wget -nc --no-proxy %BIOS_WGET_URL%/%BIOS_WGET_PKG%
ren "%BIOS_WGET_PKG%*" "%BIOS_WGET_PKG%"
if not exist %BIOS_WGET_PKG% goto bios_error
:bios_install
@echo Install:  %BIOS_WGET_PKG%
if exist %BIOS_INSTALL_DIR% goto bios_exist
%BIOS_WGET_PKG% --prefix %FFTLIB_INSTALL_DIR% --mode unattended
:bios_exist
@echo ......... %BIOS_WGET_PKG%  Installed
@echo .

:: *************************************************************************
:: ** Install EDMA
:: *************************************************************************
if exist %EDMA3_WGET_PKG% goto edma3_install
@echo WGET:  %EDMA3_WGET_PKG%
::wget -nc --no-proxy %EDMA3_WGET_URL%/%EDMA3_WGET_PKG%
::if not exist %EDMA3_WGET_PKG% goto edma3_error
:edma3_install
@echo Install:  %EDMA3_WGET_PKG%
if exist %EDMA3_INSTALL_DIR%/edma3_lld_%EDMA3_VERSION% goto edma3_exist
%XDC_INSTALL_DIR%/bin/tar -C %EDMA3_INSTALL_DIR% -xf %EDMA3_WGET_PKG%
:edma3_exist
@echo ......... %EDMA3_WGET_PKG%  Installed
@echo .


:: *************************************************************************
:: ** Install IPC
:: *************************************************************************
if exist %IPC_WGET_PKG% goto ipc_install
@echo WGET:  %IPC_WGET_PKG%
wget -nc --no-proxy %IPC_WGET_URL%/%IPC_WGET_PKG%
if not exist %IPC_WGET_PKG% goto ipc_error
:ipc_install
@echo Install:  %IPC_WGET_PKG%
if exist %IPC_INSTALL_DIR%/ipc_%IPC_VERSION% goto ipc_exist
@unzip -o -d %IPC_INSTALL_DIR% %IPC_WGET_PKG%
:ipc_exist
@echo ......... %IPC_WGET_PKG%  Installed
@echo .


:: *************************************************************************
:: ** Install CCS
:: *************************************************************************
if exist %CC5_WGET_PKG% goto ccs_install
@echo WGET:  CCS%CC5_VER_FUL%_win32.zip
wget -nc --no-proxy %CC5_WGET_URL%/%CC5_WGET_PKG%
ren "%CC5_WGET_PKG%*" "%CC5_WGET_PKG%"
if not exist %CC5_WGET_PKG% goto ccs_error
:ccs_install
@echo Install:  ccs_setup_%CC5_VER_FUL%.exe
if exist %CC5_INSTALL_DIR% goto ccs_exist
ccs_setup_%CC5_VER_FUL%.exe --prefix %CC5_INSTALL_DIR% --mode silent
:ccs_exist
@echo ......... ccs_setup_%CC5_VER_FUL%.exe  Installed
@echo .


:: *************************************************************************
:: ** Install CGT
:: *************************************************************************
if exist %CGT_WGET_PKG% goto cgt_install
@echo WGET:  %CGT_WGET_PKG%
wget -nc --no-proxy %CGT_WGET_URL%/%CGT_WGET_PKG%
if not exist %CGT_WGET_PKG% goto cgt_error
:cgt_install
@echo Install:  %CGT_WGET_PKG%
if exist %CGT_INSTALL_DIR% goto cgt_exist
%CGT_WGET_PKG% --prefix %CGT_INSTALL_DIR% --mode silent
:cgt_exist
@echo ......... %CGT_WGET_PKG%  Installed
@echo .

:: *************************************************************************
:: ** Install DSPLIB
:: *************************************************************************
if exist %DSPLIB_WGET_PKG% goto dsplib_install
@echo WGET:  %DSPLIB_WGET_PKG%
wget -nc --no-proxy %DSPLIB_WGET_URL%/%DSPLIB_WGET_PKG%
ren "%DSPLIB_WGET_PKG%*" "%DSPLIB_WGET_PKG%"
if not exist %DSPLIB_WGET_PKG% goto dsplib_error
:dsplib_install
@echo Install:  %DSPLIB_WGET_PKG%
if exist %DSPLIB_INSTALL_DIR% goto dsplib_exist
%DSPLIB_WGET_PKG% --prefix %DSPLIB_INSTALL_DIR% --mode silent
:dsplib_exist
@echo ......... %DSPLIB_WGET_PKG%  Installed
@echo .

:: *************************************************************************
:: ** Install XDAIS
:: *************************************************************************
if exist %XDAIS_WGET_PKG% goto xdais_install
@echo WGET:  %XDAIS_WGET_PKG%
wget -nc --no-proxy %XDAIS_WGET_URL%/%XDAIS_WGET_PKG%
if not exist %XDAIS_WGET_PKG% goto xdais_error
:xdais_install
@echo Install:  %XDAIS_WGET_PKG%
if exist %XDAIS_INSTALL_DIR%/xdais_%XDAIS_VERSION% goto xdais_exist
%XDC_INSTALL_DIR%/bin/tar -C  %XDAIS_INSTALL_DIR% -xf %XDAIS_WGET_PKG%
:xdais_exist
@echo ......... %XDAIS_WGET_PKG%  Installed
@echo .


:: *************************************************************************
:: ** Install Framework Components
:: *************************************************************************
if exist %FRC_WGET_PKG% goto frc_install
@echo WGET:  %FRC_WGET_PKG%
wget -nc --no-proxy %FRC_WGET_URL%/%FRC_WGET_PKG%
ren "%FRC_WGET_PKG%*" "%FRC_WGET_PKG%"
if not exist %FRC_WGET_PKG% goto frc_error
:frc_install
@echo Install:  %FRC_WGET_PKG%
if exist %FRC_INSTALL_DIR%/framework_components_%FRC_VERSION% goto frc_exist
%XDC_INSTALL_DIR%/bin/tar -C  %FRC_INSTALL_DIR% -xf %FRC_WGET_PKG%
:frc_exist
@echo ......... %FRC_WGET_PKG%  Installed
@echo .


:: *************************************************************************
:: ** Install Doxygen
:: *************************************************************************
if exist %DOX_WGET_PKG% goto dox_install
@echo WGET:  %DOX_WGET_PKG%
wget -nc --no-proxy %DOX_WGET_URL%/%DOX_WGET_PKG%
if not exist %DOX_WGET_PKG% goto dox_error
:dox_install
@echo Install:  %DOX_WGET_PKG%
if exist %DOX_INSTALL_DIR%/Doxygen goto dox_exist
@unzip -o -d %DOX_INSTALL_DIR% %DOX_WGET_PKG%
:dox_exist
@echo ......... %DOX_WGET_PKG%  Installed
@echo .

:: *************************************************************************
:: ** Install OpenMP
:: *************************************************************************
if exist %OMP_WGET_PKG% goto omp_install
@echo WGET:  %OMP_WGET_PKG%
wget -nc --no-proxy --no-check-certificate %OMP_WGET_URL%/%OMP_WGET_PKG%
if not exist %OMP_WGET_PKG% goto omp_error
:omp_install
@echo Install:  %OMP_WGET_PKG%
if exist %OMP_INSTALL_DIR%/openmp_dsp_%OMP_VERSION% goto omp_exist
@unzip -o -d %OMP_INSTALL_DIR% %OMP_WGET_PKG%
:omp_exist
@echo ......... %OMP_WGET_PKG%  Installed
@echo .


:: *************************************************************************
:: ** Install InstallJammer
:: *************************************************************************
if exist %IJM_WGET_PKG% goto ijm_install
@echo WGET:  %IJM_WGET_PKG%
wget -nc --no-proxy %IJM_WGET_URL%/%IJM_WGET_PKG%
if not exist %IJM_WGET_PKG% goto ijm_error
:ijm_install
@echo Install:  %IJM_WGET_PKG%
if exist %IJM_INSTALL_DIR%/Installjammer goto ijm_exist
@unzip -o -d %IJM_INSTALL_DIR% %IJM_WGET_PKG%
:ijm_exist
@echo ......... %IJM_WGET_PKG%  Installed
@echo .


:: *************************************************************************
:: ** Install Cygwin
:: *************************************************************************
if exist %CYG_WGET_PKG% goto cyg_install
@echo WGET:  %CYG_WGET_PKG%
wget -nc --no-proxy %CYG_WGET_URL%/%CYG_WGET_PKG%
if not exist %CYG_WGET_PKG% goto cyg_error
:cyg_install
@echo Install:  %CYG_WGET_PKG%
if exist %CYG_INSTALL_DIR%/cygwin goto cyg_exist
@unzip -o -d %CYG_INSTALL_DIR% %CYG_WGET_PKG%
:cyg_exist
@echo ......... %CYG_WGET_PKG%  Installed
@echo .


:: *************************************************************************
:: ** Install PHP
:: *************************************************************************
if exist %PHP_WGET_PKG% goto php_install
@echo WGET:  PHP_%PHP_VERSION%.zip
wget -nc --no-proxy %PHP_WGET_URL%/%PHP_WGET_PKG%
if not exist %PHP_WGET_PKG% goto php_error
:php_install
@echo Install:  %PHP_WGET_PKG%
if exist %PHP_INSTALL_DIR%/PHP_%PHP_VERSION% goto php_exist
@unzip -o -d %PHP_INSTALL_DIR% %PHP_WGET_PKG%
:php_exist
@echo ......... %PHP_WGET_PKG%  Installed
@echo .


:: *************************************************************************
:: ** Install Eclipse Plugin Generator
:: *************************************************************************
if exist %EPI_WGET_PKG% goto epi_install
@echo WGET:  %EPI_WGET_PKG%
wget -nc --no-proxy %EPI_WGET_URL%/%EPI_WGET_PKG%
if not exist %EPI_WGET_PKG% goto epi_error
:epi_install
@echo Install:  %EPI_WGET_PKG%
if exist %EPI_INSTALL_DIR%/xdc_eclipse_plugin_gen goto epi_exist
@unzip -o -d %EPI_INSTALL_DIR% %EPI_WGET_PKG%
:epi_exist
@echo ......... %EPI_WGET_PKG%  Installed
@echo .
@echo #######################################################################
@echo ##  All Required Tools Installed 
@echo #######################################################################
@echo .

:: *************************************************************************
:: ** Set the PATH
:: *************************************************************************
set PATH=%SystemRoot%;%SystemRoot%/system32
set PATH=%XDC_INSTALL_DIR%;%PATH%
set PATH=%XDC_INSTALL_DIR%/jre/bin;%PATH%
set PATH=%DOX_INSTALL_DOS_DIR%\Doxygen\doxygen\%DOX_VERSION%\bin;%PATH%
set PATH=%DOX_INSTALL_DOS_DIR%\Doxygen\Graphviz\%GVZ_VERSION%\bin;%PATH%
set PATH=%DOX_INSTALL_DOS_DIR%\Doxygen\HTML_Help_Workshop\%HHW_VERSION%;%PATH%
set PATH=%TI_DOXYGEN_TEMPLATES%;%PATH%
set PATH=%IJM_INSTALL_DIR%/InstallJammer/v%IJM_VERSION%;%PATH%
set PATH=%CYG_INSTALL_DIR%/cygwin/bin;%PATH%
set PATH=%PHP_INSTALL_DIR%/PHP_%PHP_VERSION%;%PATH%


:: *************************************************************************
:: ** Create XDC environment variables 
:: *************************************************************************
set xdc=%XDC_INSTALL_DIR%\xdc.exe $*
set XDCPATH=%CC5_INSTALL_DIR%/ccsv5/packages
set XDCPATH=%XDCPATH%;%EPI_INSTALL_DIR%/xdc_eclipse_plugin_gen/%EPI_VERSION%
set XDCPATH=%XDCPATH%;%XDC_INSTALL_DIR%/packages
set XDCPATH=%FRC_INSTALL_DIR%/framework_components_%FRC_VERSION%/packages;%XDCPATH%
set XDCPATH=%OMP_INSTALL_DIR%/openmp_dsp_%OMP_VERSION%/packages;%XDCPATH%
set XDCPATH=%MCSDK_INSTALL_DIR%/pdk_keystone2_%MCSDK_VERSION_K2H%/packages;%XDCPATH%
set XDCPATH=%XDAIS_INSTALL_DIR%/xdais_%XDAIS_VERSION%/packages;%XDCPATH%
set XDCPATH=%BIOS_INSTALL_DIR%/packages;%XDCPATH%
set XDCPATH=%EDMA3_INSTALL_DIR%/edma3_lld_%EDMA3_VERSION%/packages;%XDCPATH%
set XDCPATH=%IPC_INSTALL_DIR%/ipc_%IPC_VERSION%/packages;%XDCPATH%





:: *************************************************************************
:: ** Create environment variables for XDC
:: *************************************************************************
@cd /D %~dp0
@cd ..
@echo "List all files in %FFTLIB_INSTALL_DOS_DIR%"
ls %FFTLIB_INSTALL_DOS_DIR%
@echo "Done listing files"
xs -f setxdcpath.js
if errorlevel 1 goto clean
call tempcfg
:clean
rm tempcfg.bat
rm path.txt
set XDCPATH=%XDCPATH%;%DSPLIB_INSTALL_DIR%/packages;%LIBARCH_INSTALL_DIR%/packages

pushd .
cd %~dp0/../platforms
pushd ompevm6678
xdc clean
xdc release
popd
pushd ompevmTCI6636K2H
xdc clean
xdc release
popd
set CCS_INSTALL_DOS_DIR=%FFTLIB_INSTALL_DOS_DIR%\CCSV%CC5_VER_UND%
xcopy /s /q /y *.* %CCS_INSTALL_DOS_DIR%\ccsv5\ccs_base\ti\fftlib\platforms\
popd


:: *************************************************************************
:: ** Show the build environment
:: *************************************************************************
@echo .
@echo #######################################################################
@echo ##  Build Environment Variables (Start)
@echo #######################################################################
@set
@echo #######################################################################
@echo ##  Build Environment Variables (Stop)
@echo #######################################################################
@echo .


:: *************************************************************************
:: ** Errors and cleanup
:: *************************************************************************
goto install_end

:xdc_error
@echo "Unable to find XDC installation:  %XDC_WGET_PKG%  exiting..."
goto install_end

:cgt_error
@echo "Unable to find Codegen Tools installation:  %CGT_WGET_PKG%  exiting..."
goto install_end

:dsplib_error
@echo "Unable to find dsplib Tools installation:  %DSPLIB_WGET_PKG%  exiting..."
goto install_end

:mcsdk_error
@echo "Unable to find mcsdk Tools installation:  %MCSDK_WGET_PKG%  exiting..."
goto install_end

:mcsdk_error_k2h
@echo "Unable to find mcsdk Tools installation:  %MCSDK_WGET_PKG_K2H%  exiting..."
goto install_end

:ccs_error
@echo "Unable to find CCS installation:  %CC5_WGET_PKG%  exiting..."
goto install_end

:dox_error
@echo "Unable to find Doxygen installation:  %DOX_WGET_PKG%  exiting..."
goto install_end

:omp_error
@echo "Unable to find OpenMP installation:  %OMP_WGET_PKG%  exiting..."
goto install_end

:ijm_error
@echo "Unable to find Installjammer installation:  %IJM_WGET_PKG%  exiting..."
goto install_end

:cyg_error
@echo "Unable to find Cygwin installation:  %CYG_WGET_PKG%  exiting..."
goto install_end

:php_error
@echo "Unable to find PHP installation:  %PHP_WGET_PKG%  exiting..."
goto install_end

:epi_error
@echo "Unable to find Eclipse Plugin Generator installation:  %EPI_WGET_PKG%  exiting..."
goto install_end

:xdais_error
@echo "Unable to find XDAIS installation:  %XDAIS_WGET_PKG%  exiting..."
goto install_end

:frc_error
@echo "Unable to find Framework Component installation:  %FRC_WGET_PKG%  exiting..."
goto install_end

:bios_error
@echo "Unable to find BIOS Component installation:  %BIOS_WGET_PKG%  exiting..."
goto install_end

:edma3_error
@echo "Unable to find EMDA3 Component installation:  %EDMA3_WGET_PKG%  exiting..."
goto install_end

:ipc_error
@echo "Unable to find IPC Component installation:  %IPC_WGET_PKG%  exiting..."
goto install_end


:: *************************************************************************
:: *** Cleanup and return
:: *************************************************************************
:install_end

set XDC_WGET_URL=
set CGT_WGET_URL=
set DSPLIB_WGET_URL=
set MCSDK_WGET_URL=
set MCSDK_WGET_URL_K2H=
set CC5_WGET_URL=
set DOX_WGET_URL=
set IJM_WGET_URL=
set CYG_WGET_URL=
set PHP_WGET_URL=
set EPI_WGET_URL=
set XDAIS_WGET_URL=
set FRC_WGET_URL=

set CGT_VER_UND=
set CGT_VER_DOT=
set CC5_VER_UND=
set CC5_VER_DOT=
set CC5_VER_FUL=
set IJM_VERSION=
set CYG_VERSION=
set PHP_VERSION=
set DOX_VERSION=
set GVZ_VERSION=
set HHW_VERSION=
set TIT_VERSION=
set EPI_VERSION=
set XDS_VERSION=
set MCSDK_VERSION=
set MCSDK_VERSION_K2H=



set XDC_WGET_PKG=
set CC5_WGET_PKG=
set CGT_WGET_PKG=
set DOX_WGET_PKG=
set IJM_WGET_PKG=
set CYG_WGET_PKG=
set PHP_WGET_PKG=
set EPI_WGET_PKG=
set DSPLIB_WGET_PKG=
set MCSDK_WGET_PKG=
set MCSDK_WGET_PKG_K2H=
set XDAIS_WGET_PKG=
set FRC_WGET_PKG=

popd

:end
GOTO:EOF

:: *************************************************************************
:: *** Nothing past this point
:: *************************************************************************
