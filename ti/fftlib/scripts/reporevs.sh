#!/bin/bash

## Set release name and directory
export git_name=fftlib
export release_dir=../artifacts

## Get latest commit ID and branch
export COMMIT_ID=`git log -n 1 | grep commit | grep -v 'this commit' | cut -d" " -f 2`
export git_branch=`git branch | grep '*' | cut -d" " -f 2`

## Write into the repo-revs info file
echo -n $git_name: > $release_dir/repo-revs.txt
echo -n ${COMMIT_ID}:$git_branch >> $release_dir/repo-revs.txt
echo :`git log -1 | grep "    "` >> $release_dir/repo-revs.txt

## Create a compressed tar of artifacts directory 
tar -cvzf $release_dir.tgz $release_dir

## Copy the artifacts zip back
cp $release_dir.tgz ../../../../..
 

## Nothing past this point
