@echo off
:: *************************************************************************
::  FILE           : build_release.bat
::  DESCRIPTION    :
::
::     This batch script creates the full FFTLIB release
::
:: *************************************************************************

@echo Executing:  %~fn0

:: *************************************************************************
:: *** Initialize build environment variables
:: *************************************************************************
set FFTLIB_HOME_DIR=%~dp0\..
set FFTLIB_BUILD_DIR=%FFTLIB_HOME_DIR%\artifacts
set FFTLIB_OUTPUT_DIR=%FFTLIB_BUILD_DIR%\output
set FFTLIB_LOG_DIR=%FFTLIB_BUILD_DIR%\logs
set FFTLIB_TEST_DIR=%FFTLIB_BUILD_DIR%\test
set FFTLIB_FILE_LIST=%FFTLIB_OUTPUT_DIR%\build_targets
set XDCOPTIONS=

set FFTLIB_BUILD_C66=0
set FFTLIB_BUILD_K2H=0
set FFTLIB_C66_TEST_RPT=FFTLIB_K1_TestReport.html
set FFTLIB_C66_SRC_LOG=%FFTLIB_LOG_DIR%\build_K1_src.log
set FFTLIB_C66_OBJ_LOG=%FFTLIB_LOG_DIR%\build_K1_obj.log
set FFTLIB_K2H_TEST_RPT=FFTLIB_K2H_TestReport.html
set FFTLIB_K2H_SRC_LOG=%FFTLIB_LOG_DIR%\build_K2H_src.log
set FFTLIB_K2H_OBJ_LOG=%FFTLIB_LOG_DIR%\build_K2H_obj.log

:: *************************************************************************
:: *** Clear artifacts list if it exists
:: *************************************************************************
if exist "%FFTLIB_FILE_LIST%" (
  del /F "%FFTLIB_FILE_LIST%"
)

:: *************************************************************************
:: *** Check command line input
:: *************************************************************************
if "%1" == ""      goto build_all
if "%1" == "all"   goto build_all
if "%1" == "C66"   goto build_C66
if "%1" == "K2H"   goto build_K2H

:: USAGE:
echo ERROR: Option "%1" supplied is invalid...
echo .
echo . Usage: %0 [all C66 k2h] (defaults to "all")
echo .
echo .     "all"  : Build all targets 
echo .     "C66"  : Build just the C66x FFTLIB targets 
echo .     "K2H"  : Build just the K2H FFTLIB targets 
goto end


:: *************************************************************************
:: *** Set environment variables based on "all"
:: *************************************************************************
:build_all
set FFTLIB_BUILD_C66=1
set FFTLIB_BUILD_K2H=1
goto build_process


:: *************************************************************************
:: *** Set environment variables based on a specific target
:: *************************************************************************
:build_C66
set FFTLIB_BUILD_C66=1
goto build_process

:build_K2H
set FFTLIB_BUILD_K2H=1
goto build_process


:build_process
:: :: *************************************************************************
:: :: *** Execute environment initialization
:: :: *************************************************************************
:: cd "%FFTLIB_HOME_DIR%"/../mas/swtools
:: call cygwinxdc.bat enable_write


:: *************************************************************************
:: *** Create temporary directory for installers 
:: *************************************************************************
if exist "%FFTLIB_BUILD_DIR%" (
  rmdir /S /Q "%FFTLIB_BUILD_DIR%"
)
mkdir "%FFTLIB_BUILD_DIR%"
mkdir "%FFTLIB_OUTPUT_DIR%"
mkdir "%FFTLIB_LOG_DIR%"
mkdir "%FFTLIB_TEST_DIR%"


 
:build_process_C66
:: *************************************************************************
:: *** C66x build process
:: *************************************************************************
if "%FFTLIB_BUILD_C66%" == "0" goto build_process_K2H

:: *************************************************************************
:: *** Build C66x FFTLIB Source delivery
:: *************************************************************************
cd "%FFTLIB_HOME_DIR%"
xdc clean > "%FFTLIB_C66_SRC_LOG%"
xdc XDCARGS="c66x src bundle install" >> "%FFTLIB_C66_SRC_LOG%" 2>&1

:: Verify if artifacts created successfully
for /f %%a in (buildArtifacts.log) do (
        if exist "%%a" (
          echo %%a: PASSED >> %FFTLIB_FILE_LIST%
        ) else (
          echo %%a: FAILED >> %FFTLIB_FILE_LIST%
	    )
)
copy "fftlib"*".bin" "%FFTLIB_OUTPUT_DIR%"
copy "fftlib"*".exe" "%FFTLIB_OUTPUT_DIR%"
copy "docs\manifest\Software_Manifest.html" "%FFTLIB_OUTPUT_DIR%"

:: echo %FFTLIB_C66_TEST_RPT% >> %FFTLIB_FILE_LIST%
:: copy "docs\bundle\%FFTLIB_C66_TEST_RPT%" "%FFTLIB_OUTPUT_DIR%"
:: ************************************************************************
:: ************************** Webgen **************************************
:: ************************************************************************

:: Copy Webgen folder 
if exist "%FFTLIB_BUILD_DIR%\webgen" (
  del /F "%FFTLIB_BUILD_DIR%\webgen"
)
cp -r "%FFTLIB_HOME_DIR%\webgen" "%FFTLIB_BUILD_DIR%\webgen"

:build_process_K2H
:: *************************************************************************
:: *** K2H build process
:: *************************************************************************
if "%FFTLIB_BUILD_K2H%" == "0" goto end

set ISK2H=k2h

:: *************************************************************************
:: *** Build K2H FFTLIB Source delivery
:: *************************************************************************
cd "%FFTLIB_HOME_DIR%"
xdc clean > "%FFTLIB_K2H_SRC_LOG%"
xdc XDCARGS="k2hx src bundle install" >> "%FFTLIB_K2H_SRC_LOG%" 2>&1

:: Verify if artifacts created successfully
for /f %%a in (buildArtifacts.log) do (
        if exist "%%a" (
          echo %%a: PASSED >> %FFTLIB_FILE_LIST%
        ) else (
          echo %%a: FAILED >> %FFTLIB_FILE_LIST%
	    )
)
copy "fftlib"*".bin" "%FFTLIB_OUTPUT_DIR%"
copy "fftlib"*".exe" "%FFTLIB_OUTPUT_DIR%"
copy "docs\manifest\Software_Manifest.html" "%FFTLIB_OUTPUT_DIR%"

:: echo %FFTLIB_K2H_TEST_RPT% >> %FFTLIB_FILE_LIST%
:: copy "docs\bundle\%FFTLIB_K2H_TEST_RPT%" "%FFTLIB_OUTPUT_DIR%"

:: ************************************************************************
:: ************************** Webgen **************************************
:: ************************************************************************

:: Copy Webgen folder 
if exist "%FFTLIB_BUILD_DIR%\webgen" (
  del /F "%FFTLIB_BUILD_DIR%\webgen"
)
cp -r "%FFTLIB_HOME_DIR%\webgen" "%FFTLIB_BUILD_DIR%\webgen"

set ISK2H=

:: *************************************************************************
:: *** Cleanup and return
:: *************************************************************************
:end

cd "%FFTLIB_HOME_DIR%"
set FFTLIB_HOME_DIR=
set FFTLIB_BUILD_DIR=
set FFTLIB_OUTPUT_DIR=
set FFTLIB_LOG_DIR=
set FFTLIB_TEST_DIR=
set FFTLIB_C66_TEST_RPT=
set FFTLIB_BUILD_C66=
set FFTLIB_C66_SRC_LOG=
set FFTLIB_C66_OBJ_LOG=
set FFTLIB_K2H_TEST_RPT=
set FFTLIB_BUILD_K2H=
set FFTLIB_K2H_SRC_LOG=
set FFTLIB_K2H_OBJ_LOG=
set XDCOPTIONS=

:: *************************************************************************
:: *** Nothing past this point
:: *************************************************************************

