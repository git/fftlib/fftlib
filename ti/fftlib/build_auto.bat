@echo off
:: *************************************************************************
::  FILE           : build_auto.bat
::  DESCRIPTION    :
::
::     This batch script installs all tools and creates all VLIB releases
::
:: *************************************************************************

:: *************************************************************************
:: *** Enter the scripts directory
:: *************************************************************************
pushd scripts


:: *************************************************************************
:: *** Install all tools (ARG2)
:: *************************************************************************
call install_tools %2
popd

:: *************************************************************************
:: *** Enter the scripts directory
:: *************************************************************************
pushd scripts


:: *************************************************************************
:: *** Check build target command line input (ARG1)
:: *************************************************************************
:check_target
if "%1" == ""      goto call_build
if "%1" == "all"   goto call_build
if "%1" == "C66"   goto call_build
if "%1" == "K2H"   goto call_build


:: USAGE:
echo ERROR: Option "%1" supplied is invalid...
echo .
echo . Usage: %0 [all C66] (defaults to "all") [<Drive Letter>:] (optional, defaults to C:)
echo .
echo .     "all"  : Build all targets 
echo .     "C66"  : Build just the C66x fftlib targets 
echo .     "K2H"  : Build just the K2H fftlib targets 
goto end


:: *************************************************************************
:: *** Build all artifacts
:: *************************************************************************
:call_build
call build_release %1 %2
popd


:: *************************************************************************
:: *** Archive and label all artifacts
:: *************************************************************************
:call_archive
pushd scripts
bash reporevs.sh



:end
popd
:: *************************************************************************
:: *** Nothing past this point
:: *************************************************************************

