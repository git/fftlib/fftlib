/* ======================================================================= */
/*  TEXAS INSTRUMENTS, INC.                                                */
/*                                                                         */
/*  FFTLIB  FFT Library                                                    */
/*                                                                         */
/* Copyright (C) 2014 Texas Instruments Incorporated - http://www.ti.com/  */
/*                                                                         */
/*                                                                         */
/*  Redistribution and use in source and binary forms, with or without     */
/*  modification, are permitted provided that the following conditions     */
/*  are met:                                                               */
/*                                                                         */
/*    Redistributions of source code must retain the above copyright       */
/*    notice, this list of conditions and the following disclaimer.        */
/*                                                                         */
/*    Redistributions in binary form must reproduce the above copyright    */
/*    notice, this list of conditions and the following disclaimer in the  */
/*    documentation and/or other materials provided with the               */
/*    distribution.                                                        */
/*                                                                         */
/*    Neither the name of Texas Instruments Incorporated nor the names of  */
/*    its contributors may be used to endorse or promote products derived  */
/*    from this software without specific prior written permission.        */
/*                                                                         */
/*  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS    */
/*  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT      */
/*  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR  */
/*  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT   */
/*  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,  */
/*  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT       */
/*  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,  */
/*  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY  */
/*  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT    */
/*  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE  */
/*  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.   */
/*                                                                         */
/* ======================================================================= */
#include <stdlib.h>
#include <stdio.h>
#include <time.h>
#include <assert.h>
#include <memory.h>
#include <math.h>
#include "fftw3.h"

#define FFT_STATS   1
#define IFFT_STATS -1

static const int N=1024;
static const double MAX_DELTA = 1e-9;

static int num_input_modified_failures = 0;

FILE *fout;
#define tick()  clock_gettime(CLOCK_MONOTONIC, &t0);
#define tock() (clock_gettime(CLOCK_MONOTONIC, &t1), \
                        t1.tv_sec - t0.tv_sec + (t1.tv_nsec - t0.tv_nsec) / 1e9)

static void report_flops(const char* str, double secs, int fftsize, int batch)
{
  fprintf(fout,"Time for %s: %8d, %6.6f ms\n",
          str, fftsize, secs*1000);
  fprintf(fout,"Performance for %s: %8d, %6.3f Gflops\n",
          str, fftsize, 2.5 * batch * fftsize * log2(fftsize) / (secs * 1e9) / 2);
}

static double get_delta_sum(int n, int batch, double* final, double* ref, int scale, int verbose) {
    double cumul_err = 0.0;
    int i;

    for ( i = 0; i < n*batch; i++ ) {
        double x = final[i];
        if (scale) {
          x = x/(double)n;
        }
        double delta_x = fabs(x - ref[i]);
        cumul_err += delta_x;

        if (verbose > 1) 
        {
            fprintf (fout,  "  %3d  %12f (%f)\n", i, x, ref[i]);
        }
    }
    if (verbose) fprintf(fout, "  cumul err: %12g\n", cumul_err);
    return cumul_err;
}

static int do_fft(int n, int batch, int in_place, int verbose, int stats_type) {
    int i;

    double *in, *in2, *in_golden;
    fftw_complex *out;
    fftw_plan plan_backward;
    fftw_plan plan_forward;
    int size[1] = {n};

    in = (double*) fftw_malloc(sizeof(double) * n * batch);
    assert(in);
    in_golden = (double*) malloc(sizeof(double) * n * batch);
    assert(in_golden);
    out = (fftw_complex*) fftw_malloc(sizeof(fftw_complex) * (n/2+1) * batch);
    assert(out);

    plan_forward = fftw_plan_many_dft_r2c(1, size, batch, in, size, 1, n, out, size, 1, n/2+1, FFTW_ESTIMATE);

    for (i = 0; i < n*batch; i++ ) {
        in[i] = (double)rand() / RAND_MAX;
    }

    /* Call to fftw_execute modifies input buffer, save it off */
    memcpy(in_golden, in, sizeof(double) * n * batch);

    if (verbose > 1) {
       fprintf (fout,  "Input Data:\n" );
       for ( i = 0; i < n*batch; i++ ) {
          fprintf (fout,  "  %3d  %12f\n", i, in[i]);
       }
    }

    if (verbose) fprintf(fout, "\nNow doing a forward FFT..\n");
    struct timespec t0, t1;
    tick();
    fftw_execute(plan_forward); /* repeat as needed */
    double secs = tock();

    if (stats_type == FFT_STATS)
      report_flops("fftw_execute (ARM and DSP)", secs, n, batch);

    if (verbose > 1) {
      fprintf (fout,  "Output FFT Coefficients:\n" );
      for ( i = 0; i < (n/2+1)*batch; i++ ) {
         fprintf (fout,  "  %3d  %12f  %12f\n", i, out[i][0], out[i][1] );
      }
    }

    in2 = (double *)fftw_malloc ( sizeof (double) * n * batch);
    assert(in2);
    if (verbose) fprintf(fout, "\nNow doing a backward FFT on the output buffer\n");
    plan_backward = fftw_plan_many_dft_c2r ( 1, size, batch, out, size, 1, n/2+1, in2, size, 1, n, FFTW_ESTIMATE );
    tick();
    fftw_execute ( plan_backward );
    secs = tock();
    if (stats_type == IFFT_STATS)
      report_flops("fftw_execute (ARM and DSP)", secs, n, batch);

    if (verbose) fprintf(fout, "\nNow checking if the FWD+BKWD gives the original data\n");
    if (verbose > 1) fprintf (fout,  "\nIN2(REF):\n" );
    double err = get_delta_sum(n, batch, in2, in_golden, 1, verbose);
    double accept_err = MAX_DELTA * n * batch;

    // now check if original array was modified
    double input_modified = 0.0;

    if (!in_place) {
      if (verbose) fprintf(fout, "\nNow checking if the INPUT buffer was modified\n");
      if (verbose > 1) fprintf (fout,  "\nIN(REF):\n" );
      input_modified = get_delta_sum(n, batch, in, in_golden, 0, verbose);
    }

    fftw_destroy_plan(plan_forward);
    fftw_destroy_plan(plan_backward);

    fftw_free(in); fftw_free(in2);
    if (!in_place) fftw_free(out);
    free(in_golden);

    int rc = 0;
    if (err > accept_err) {
        rc = 1;
        fprintf(fout, "TEST FAILED (cumul err=%12g, accept err=%12g)\n", err, accept_err);
    }
    if (input_modified > 0.0) {
        num_input_modified_failures++;
        fprintf(fout, "FFTW API VIOLATED: IN buffer modified (cumul err=%12g, accept err=0.0)\n", input_modified);
    }
    return (rc);
}

static int do_tests(int num_tests, int n, int batch, int in_place, int verbose, int stat_type) {
  const char* type = (in_place? "IN-PLACE":"OUT-OF-PLACE");
  fprintf(fout, "--------------------------------------------\n");
  fprintf(fout, "DOING %d TESTS %s with FFT size of %d\n", num_tests, type, n);
  int i;
  srand (123456789);
  int num_failed = 0;
  for (i=0; i<num_tests; i++) {
    fprintf(fout, "\nRunning test %d\n", i+1);
    int rc = do_fft(n, batch, in_place, verbose, stat_type);
    if (rc > 0) fprintf(fout, "*** Test %d FAILED ***\n", i+1);
    else fprintf(fout, "Test %d PASSED\n", i+1);
    num_failed += rc;
  }
  if (num_failed == 0) fprintf(fout, "\nAll %d FFT tests PASSED (SIZE=%d)\n", num_tests, n);
  else fprintf(fout, "\n**** %d/%d FFT TESTS FAILED (SIZE=%d) ****\n", num_failed, num_tests, n);
  fprintf(fout, "--------------------------------------------\n");
  return num_failed;
}

void print_sizes_tested(int argc, char **argv) {
  fprintf(fout, "Sizes tested: ");
  if (argc > 4) {
    int i;
    for (i=4; i<argc; i++)
      fprintf(fout, "%s ", argv[i]);
  }
  else {
    fprintf(fout, "%d ", N);
  }
  fprintf(fout, "\n");
}


int main(int argc, char **argv) {
  int verbose = 0;
  int n = N;
  fout = stdout;
  if (argc > 1) {
    verbose = atoi(argv[1]);
  }

  int in_place = 0; /* IN-PLACE is not supported for r2c c2r */
  if (argc > 2) {
    if (atoi(argv[2]))
      fprintf(fout, "IN-PLACE is not supported for r2c c2r\n");
  }
  
  int test_stat_type = FFT_STATS;

  if (argc > 3) {
    test_stat_type = atoi(argv[3]);
  }

  int num_tests = 3;
  if (argc > 4) {
    num_tests = atoi(argv[4]);
  }

  int batch = 100;
  if (argc > 5) {
    batch = atoi(argv[5]);
  }

  int tot_failed = 0;
  int tot_tests = 0;
  if (argc > 6) {
    int i;
    for (i=6; i<argc; i++) {
      n = atoi(argv[i]);
      tot_tests += num_tests;
      tot_failed += do_tests(num_tests, n, batch, in_place, verbose, test_stat_type);
    } 
  }
  else {
    tot_failed = do_tests(num_tests, N, batch, in_place, verbose, test_stat_type);
    tot_tests += num_tests;
  }
  static const char* PLACE;
  PLACE = (in_place?"IN-PLACE":"OUT-OF-PLACE");
  fprintf(fout, "SUMMARY\n");
  fprintf(fout, "=======\n");
  fprintf(fout, "%s Tests Run: %d\n", PLACE, tot_tests);
  fprintf(fout, "%s Tests Failed: %d\n", PLACE, tot_failed);
  if (num_input_modified_failures)
    fprintf(fout, "Warning: Input buffer modified in %d %s transforms\n", num_input_modified_failures, PLACE);
  print_sizes_tested(argc, argv);
  return (tot_failed);
}

