#!/bin/bash -e

SIZES2=${*:-"8 16 32 64 128 256 512 1024 2048 4096 8192"}
SIZES3=${*:-"48 72 96 144 192 288 384 576 768 1152 1536 2304 3072 4608 6144"}
SIZES5=${*:-"40 80 160 200 320 400 640 800 1280 1600 2560 3200 5120 6400 8000"}
SIZES35=${*:-"120 240 360 480 720 960 1440 1920 2880 3840 5760 7680"}

NUM_TESTS_PER_SIZE=10
BATCH_SIZE=2000
EXE="fft_static"

case $0 in
  *csv) sed_script='s/^[[:space:]]\+//; s/[[:space:]]\+/,/g' ;;
  *) sed_script='s/xyz//';;
esac

function run_sizes2
{
  printf "%20s %12s %12s %12s\n" "Size" "Mean" "Min" "Max"
  for size in $SIZES2; do
    printf "%20s " $size
    ./$EXE 0 $1 $2 $NUM_TESTS_PER_SIZE $BATCH_SIZE $size 2>&1 | grep ms | awk 'NR % 2 == 1' | awk '{print $8}' | sort -n | awk 'BEGIN{c=0;sum=0;} /^[^#]/{a[c++]=$1;sum+=$1;} END{ave=sum/c; if((c%2)==1){median=a[int(c/2)];} else{median=(a[c/2]+a[c/2-1])/2;} printf "%12.6f %12.6f %12.6f\n", ave,a[0],a[c-1]}'
    rm -f /tmp/opencl*
  done
}

function run_sizes3
{
  printf "%20s %12s %12s %12s\n" "Size" "Mean" "Min" "Max"
  for size in $SIZES3; do
    printf "%20s " $size
    ./$EXE 0 $1 $2 $NUM_TESTS_PER_SIZE $BATCH_SIZE $size 2>&1 | grep ms | awk 'NR % 2 == 1' | awk '{print $8}' | sort -n | awk 'BEGIN{c=0;sum=0;} /^[^#]/{a[c++]=$1;sum+=$1;} END{ave=sum/c; if((c%2)==1){median=a[int(c/2)];} else{median=(a[c/2]+a[c/2-1])/2;} printf "%12.6f %12.6f %12.6f\n", ave,a[0],a[c-1]}'
    rm -f /tmp/opencl*
  done
}

function run_sizes5
{
  printf "%20s %12s %12s %12s\n" "Size" "Mean" "Min" "Max"
  for size in $SIZES5; do
    printf "%20s " $size
    ./$EXE 0 $1 $2 $NUM_TESTS_PER_SIZE $BATCH_SIZE $size 2>&1 | grep ms | awk 'NR % 2 == 1' | awk '{print $8}' | sort -n | awk 'BEGIN{c=0;sum=0;} /^[^#]/{a[c++]=$1;sum+=$1;} END{ave=sum/c; if((c%2)==1){median=a[int(c/2)];} else{median=(a[c/2]+a[c/2-1])/2;} printf "%12.6f %12.6f %12.6f\n", ave,a[0],a[c-1]}'
    rm -f /tmp/opencl*
  done
}

function run_sizes35
{
  printf "%20s %12s %12s %12s\n" "Size" "Mean" "Min" "Max"
  for size in $SIZES35; do
    printf "%20s " $size
    ./$EXE 0 $1 $2 $NUM_TESTS_PER_SIZE $BATCH_SIZE $size 2>&1 | grep ms | awk 'NR % 2 == 1' | awk '{print $8}' | sort -n | awk 'BEGIN{c=0;sum=0;} /^[^#]/{a[c++]=$1;sum+=$1;} END{ave=sum/c; if((c%2)==1){median=a[int(c/2)];} else{median=(a[c/2]+a[c/2-1])/2;} printf "%12.6f %12.6f %12.6f\n", ave,a[0],a[c-1]}'
    rm -f /tmp/opencl*
  done
}

test -x ./$EXE || make $EXE

echo
echo Generating FFT stats using fft_static
echo For Power of 2 sizes $SIZES2
echo batch size is $BATCH_SIZE
echo Will run $NUM_TESTS_PER_SIZE for each size
echo "Numbers reported are in ms (lower is better)"

{
  export TI_FFTW_OFFLOAD=2 #
  echo; echo "OUT-OF-PLACE" ; run_sizes2 0 1

  export TI_FFTW_OFFLOAD=1 # to run DSP tests
  echo; echo "force DSP OUT-OF-PLACE" ; run_sizes2 0 1

  export TI_FFTW_OFFLOAD=0 # to run ARM tests
  echo; echo "force ARM OUT-OF-PLACE" ; run_sizes2 0 1
} | sed -e "$sed_script"

echo
echo Generating FFT stats using fft_static
echo For Radix-2,3 sizes $SIZES3
echo batch size is $BATCH_SIZE
echo Will run $NUM_TESTS_PER_SIZE for each size
echo "Numbers reported are in ms (lower is better)"

{
  export TI_FFTW_OFFLOAD=2 #
  echo; echo "OUT-OF-PLACE" ; run_sizes3 0 1

  export TI_FFTW_OFFLOAD=1 # to run DSP tests 
  echo; echo "force DSP OUT-OF-PLACE" ; run_sizes3 0 1

  export TI_FFTW_OFFLOAD=0 # to run ARM tests
  echo; echo "force ARM OUT-OF-PLACE" ; run_sizes3 0 1
} | sed -e "$sed_script"

echo
echo Generating FFT stats using fft_static
echo For Radix-2,5 sizes $SIZES5
echo batch size is $BATCH_SIZE
echo Will run $NUM_TESTS_PER_SIZE for each size
echo "Numbers reported are in ms (lower is better)"

{
  export TI_FFTW_OFFLOAD=2 #
  echo; echo "OUT-OF-PLACE" ; run_sizes5 0 1 

  export TI_FFTW_OFFLOAD=1 # to run DSP tests 
  echo; echo "force DSP OUT-OF-PLACE" ; run_sizes5 0 1

  export TI_FFTW_OFFLOAD=0 # to run ARM tests
  echo; echo "force ARM OUT-OF-PLACE" ; run_sizes5 0 1
} | sed -e "$sed_script"

echo
echo Generating FFT stats using fft_static
echo For Radix-2,3,5 sizes $SIZES35
echo batch size is $BATCH_SIZE
echo Will run $NUM_TESTS_PER_SIZE for each size
echo "Numbers reported are in ms (lower is better)"

{
  export TI_FFTW_OFFLOAD=2 #
  echo; echo "OUT-OF-PLACE" ; run_sizes35 0 1

  export TI_FFTW_OFFLOAD=1 # to run DSP tests
  echo; echo "force DSP OUT-OF-PLACE" ; run_sizes35 0 1

  export TI_FFTW_OFFLOAD=0 # to run ARM tests
  echo; echo "force ARM OUT-OF-PLACE" ; run_sizes35 0 1
} | sed -e "$sed_script"

echo %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

echo
echo Generating IFFT stats using fft_static
echo For Power of 2 sizes $SIZES2
echo batch size is $BATCH_SIZE
echo Will run $NUM_TESTS_PER_SIZE for each size
echo "Numbers reported are in ms (lower is better)"

{
  export TI_FFTW_OFFLOAD=2 #
  echo; echo "OUT-OF-PLACE" ; run_sizes2 0 -1

  export TI_FFTW_OFFLOAD=1 # to run DSP tests
  echo; echo "force DSP OUT-OF-PLACE" ; run_sizes2 0 -1

  export TI_FFTW_OFFLOAD=0 # to run ARM tests
  echo; echo "force ARM OUT-OF-PLACE" ; run_sizes2 0 -1
} | sed -e "$sed_script"

echo
echo Generating IFFT stats using fft_static
echo For Radix-2,3 sizes $SIZES3
echo batch size is $BATCH_SIZE
echo Will run $NUM_TESTS_PER_SIZE for each size
echo "Numbers reported are in ms (lower is better)"

{
  export TI_FFTW_OFFLOAD=2 #
  echo; echo "OUT-OF-PLACE" ; run_sizes3 0 -1

  export TI_FFTW_OFFLOAD=1 # to run DSP tests 
  echo; echo "force DSP OUT-OF-PLACE" ; run_sizes3 0 -1

  export TI_FFTW_OFFLOAD=0 # to run ARM tests
  echo; echo "force ARM OUT-OF-PLACE" ; run_sizes3 0 -1
} | sed -e "$sed_script"

echo
echo Generating IFFT stats using fft_static
echo For Radix-2,5 sizes $SIZES5
echo batch size is $BATCH_SIZE
echo Will run $NUM_TESTS_PER_SIZE for each size
echo "Numbers reported are in ms (lower is better)"

{
  export TI_FFTW_OFFLOAD=2 #
  echo; echo "OUT-OF-PLACE" ; run_sizes5 0 -1 

  export TI_FFTW_OFFLOAD=1 # to run DSP tests 
  echo; echo "force DSP OUT-OF-PLACE" ; run_sizes5 0 -1

  export TI_FFTW_OFFLOAD=0 # to run ARM tests
  echo; echo "force ARM OUT-OF-PLACE" ; run_sizes5 0 -1
} | sed -e "$sed_script"

echo
echo Generating IFFT stats using fft_static
echo For Radix-2,3,5 sizes $SIZES35
echo batch size is $BATCH_SIZE
echo Will run $NUM_TESTS_PER_SIZE for each size
echo "Numbers reported are in ms (lower is better)"

{
  export TI_FFTW_OFFLOAD=2 #
  echo; echo "OUT-OF-PLACE" ; run_sizes35 0 -1

  export TI_FFTW_OFFLOAD=1 # to run DSP tests
  echo; echo "force DSP OUT-OF-PLACE" ; run_sizes35 0 -1

  export TI_FFTW_OFFLOAD=0 # to run ARM tests
  echo; echo "force ARM OUT-OF-PLACE" ; run_sizes35 0 -1
} | sed -e "$sed_script"


