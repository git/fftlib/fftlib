test/README - Test harness documentation

Usage
=====
 
  There are two versions of this program. One statically linked with the
new accelerated library and one dynamically linked with the original fftw3
library. The latter is used to demonstrate truly transparent acceleration on
unmodified applications.

  $ ./fft_static <VERBOSE> <IN_PLACE> <FFT_TYPE> <NUM_TESTS> [N1..N2..N3..]
  $ ./fft <VERBOSE> <IN_PLACE> <FFT_TYPE> <NUM_TESTS> [N1..N2..N3..]

    VERBOSE defaults to 0. You can increase this for more verbose output.
    IN_PLACE defaults to 1. If set to 0, OUT_OF_PLACE transform will be done
    FFT_TYPE defaults to 1. If set to -1, IFFT will be done
    NUM_TESTS is the number of tests to run for EACH size
    N1, N2, N3.. are FFT sizes. If none are given N1=1024 is assumed.

Examples
========

This will run 3 tests each for 1024 and 2048 sizes. 

To run static version:
  $ ./fft_static 0 0 1 3 1024 2048 (out-of-place, FFT, 3 tests each for 1024,2048)

To run the dynamic version:
  $ LD_PRELOAD=../host/libfftw_acc.so ./fft 0 0 1 3 1024 2048

Compilation
===========

To build static version:
   $ make fft_static

To build the shared version:
   $ make fft

Testing
=======
  $ make test

Sample Output
=============

You will find a sample run output in sample.output in the same directory 
as this README

Data Generation
===============
$ make stats 
OR
$ make csv

If you'd like to choose, the sizes, you can invoke the script directly:

$ ./genstats [sizes of interest]..

If no sizes are given the sensible defaults are used. Sample genstats output
is in sample.genstats

To generate CSV, run ./genstats_csv (same arguments as genstats). Sample output
is in sample.genstats_csv

e.g., 
$ ./genstats 1024 2048 16384 65536
will generate stats for DSP and ARM for both in/out-of-place for the sizes mentioned.

