UNAME_M :=$(shell uname -m)
ifneq (,$(findstring 86, $(UNAME_M)))
    override CC = arm-linux-gnueabihf-gcc
endif

CC ?= gcc
CFLAGS = -ggdb -O2 -I$(TARGET_ROOTDIR)/usr/include
LD_FLAGS=-L$(TARGET_ROOTDIR)/lib -L$(TARGET_ROOTDIR)/usr/lib -Wl,-rpath-link,$(TARGET_ROOTDIR)/lib -Wl,-rpath-link,$(TARGET_ROOTDIR)/usr/lib

FFTW_DIR = $(TARGET_ROOTDIR)/usr/lib/
FFTWLIB = $(FFTW_DIR)libfftwf_acc.a -lOpenCL -locl_util -lrt -lm

%.o: %.c
	$(CC) -c $(CFLAGS) $(LD_FLAGS) $<
	@echo Compiling $<

$(EXE):

cross: $(EXE)

clean::
	@rm -f $(EXE) *.o *.obj *.out *.asm *.if *.opt *.bc *.objc *.map *.bin *.dsp_h

test: clean $(EXE)
	@echo Running   $(EXE)
	@./$(EXE)

