#!/bin/bash -e

SIZES=${*:-"64 96 128 160 256 288 480 512 800 864 960 1024"}

NUM_TESTS_PER_SIZE=10
EXE="fft_static"

case $0 in
  *csv) sed_script='s/^[[:space:]]\+//; s/[[:space:]]\+/,/g' ;;
  *) sed_script='s/xyz//';;
esac

function run_sizes
{
  printf "%20s %12s %12s %12s\n" "Size" "Mean" "Min" "Max"
  for size in $SIZES; do
    printf "%20s " $size
    ./$EXE 0 $1 $2 $NUM_TESTS_PER_SIZE $size 2>&1 | grep flops | awk 'NR % 2 == 1' | awk '{print $8}' | sort -n | awk 'BEGIN{c=0;sum=0;} /^[^#]/{a[c++]=$1;sum+=$1;} END{ave=sum/c; if((c%2)==1){median=a[int(c/2)];} else{median=(a[c/2]+a[c/2-1])/2;} printf "%12.6f %12.6f %12.6f\n", ave,a[0],a[c-1]}'
    rm -f /tmp/opencl*
  done
}

test -x ./$EXE || make $EXE

echo Generating FFT stats using fft_static
echo For sizes $SIZES
echo Will run $NUM_TESTS_PER_SIZE for each size
echo "Numbers reported are in Gflops (higher is better)"

{
  export TI_FFTW_OFFLOAD=2 #
  echo; echo "OUT-OF-PLACE" ; run_sizes 0 1
  echo; echo "IN-PLACE"     ; run_sizes 1 1

  export TI_FFTW_OFFLOAD=1 # to run DSP tests
  echo; echo "force DSP OUT-OF-PLACE" ; run_sizes 0 1
  echo; echo "force DSP IN-PLACE"     ; run_sizes 1 1

  export TI_FFTW_OFFLOAD=0 # to run ARM tests
  echo; echo "force ARM OUT-OF-PLACE" ; run_sizes 0 1
  echo; echo "force ARM IN-PLACE"     ; run_sizes 1 1
} | sed -e "$sed_script"

echo Generating IFFT stats using fft_static
echo For sizes $SIZES
echo Will run $NUM_TESTS_PER_SIZE for each size
echo "Numbers reported are in Gflops (higher is better)"

{
  export TI_FFTW_OFFLOAD=2 #
  echo; echo "OUT-OF-PLACE" ; run_sizes 0 -1
  echo; echo "IN-PLACE"     ; run_sizes 1 -1

  export TI_FFTW_OFFLOAD=1 # to run DSP tests
  echo; echo "force DSP OUT-OF-PLACE" ; run_sizes 0 -1
  echo; echo "force DSP IN-PLACE"     ; run_sizes 1 -1

  export TI_FFTW_OFFLOAD=0 # to run ARM tests
  echo; echo "force ARM OUT-OF-PLACE" ; run_sizes 0 -1
  echo; echo "force ARM IN-PLACE"     ; run_sizes 1 -1
} | sed -e "$sed_script"


