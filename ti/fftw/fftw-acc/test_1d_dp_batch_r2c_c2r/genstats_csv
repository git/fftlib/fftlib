#!/bin/bash -e

SIZES2=${*:-"8 16 32 64 128 256 512 1024 2048 4096 8192"}
SIZES3=${*:-"48 72 96 144 192 288 384 576 768 1152 1536 2304 3072 4608 6144"}
SIZES5=${*:-"40 80 160 200 320 400 640 800 1280 1600 2560 3200 5120 6400 8000"}
SIZES35=${*:-"120 240 360 480 720 960 1440 1920 2880 3840 5760 7680"}

NUM_TESTS_PER_SIZE=10
BATCH_SIZE=2000
EXE="fft_static"

case $0 in
  *csv) sed_script='s/^[[:space:]]\+//; s/[[:space:]]\+/,/g' ;;
  *) sed_script='s/xyz//';;
esac

function run_sizes2
{
  printf "%20s\t %12s\t %12s\t %12s\t %12s\t %12s\t %12s\t %12s\t %12s\n" "Name" "Type" "Size" "MeanGFlop" "MinGFlop" "MaxGFlop" "MeanMs" "MinMs" "MaxMs"
  for size in $SIZES2; do
    printf "%20s\t %12d\t %12s\t " "fft_1d_dp_batch_r2c" $3 $size
    ./$EXE 0 $1 $2 $NUM_TESTS_PER_SIZE $BATCH_SIZE $size 2>&1 | grep flops | awk 'NR % 2 == 1' | awk '{print $8}' | sort -n | awk 'BEGIN{c=0;sum=0;} /^[^#]/{a[c++]=$1;sum+=$1;} END{ave=sum/c; if((c%2)==1){median=a[int(c/2)];} else{median=(a[c/2]+a[c/2-1])/2;} printf "%12.6f\t %12.6f\t %12.6f\t ", ave,a[0],a[c-1]}'
    rm -f /tmp/opencl*
    ./$EXE 0 $1 $2 $NUM_TESTS_PER_SIZE $BATCH_SIZE $size 2>&1 | grep ms | awk 'NR % 2 == 1' | awk '{print $8}' | sort -n | awk 'BEGIN{c=0;sum=0;} /^[^#]/{a[c++]=$1;sum+=$1;} END{ave=sum/c; if((c%2)==1){median=a[int(c/2)];} else{median=(a[c/2]+a[c/2-1])/2;} printf "%12.6f\t %12.6f\t %12.6f\n", ave,a[0],a[c-1]}'
    rm -f /tmp/opencl*
  done
}

function run_sizes2_i
{
  printf "%20s\t %12s\t %12s\t %12s\t %12s\t %12s\t %12s\t %12s\t %12s\n" "Name" "Type" "Size" "MeanGFlop" "MinGFlop" "MaxGFlop" "MeanMs" "MinMs" "MaxMs"
  for size in $SIZES2; do
    printf "%20s\t %12d\t %12s\t " "ifft_1d_dp_batch_c2r" $3 $size
    ./$EXE 0 $1 $2 $NUM_TESTS_PER_SIZE $BATCH_SIZE $size 2>&1 | grep flops | awk 'NR % 2 == 1' | awk '{print $8}' | sort -n | awk 'BEGIN{c=0;sum=0;} /^[^#]/{a[c++]=$1;sum+=$1;} END{ave=sum/c; if((c%2)==1){median=a[int(c/2)];} else{median=(a[c/2]+a[c/2-1])/2;} printf "%12.6f\t %12.6f\t %12.6f\t ", ave,a[0],a[c-1]}'
    rm -f /tmp/opencl*
    ./$EXE 0 $1 $2 $NUM_TESTS_PER_SIZE $BATCH_SIZE $size 2>&1 | grep ms | awk 'NR % 2 == 1' | awk '{print $8}' | sort -n | awk 'BEGIN{c=0;sum=0;} /^[^#]/{a[c++]=$1;sum+=$1;} END{ave=sum/c; if((c%2)==1){median=a[int(c/2)];} else{median=(a[c/2]+a[c/2-1])/2;} printf "%12.6f\t %12.6f\t %12.6f\n", ave,a[0],a[c-1]}'
    rm -f /tmp/opencl*
  done
}

function run_sizes3
{
  printf "%20s\t %12s\t %12s\t %12s\t %12s\t %12s\t %12s\t %12s\t %12s\n" "Name" "Type" "Size" "MeanGFlop" "MinGFlop" "MaxGFlop" "MeanMs" "MinMs" "MaxMs"
  for size in $SIZES3; do
    printf "%20s\t %12d\t %12s\t " "fft_1d_dp_batch_r2c" $3 $size
    ./$EXE 0 $1 $2 $NUM_TESTS_PER_SIZE $BATCH_SIZE $size 2>&1 | grep flops | awk 'NR % 2 == 1' | awk '{print $8}' | sort -n | awk 'BEGIN{c=0;sum=0;} /^[^#]/{a[c++]=$1;sum+=$1;} END{ave=sum/c; if((c%2)==1){median=a[int(c/2)];} else{median=(a[c/2]+a[c/2-1])/2;} printf "%12.6f\t %12.6f\t %12.6f\t ", ave,a[0],a[c-1]}'
    rm -f /tmp/opencl*
    ./$EXE 0 $1 $2 $NUM_TESTS_PER_SIZE $BATCH_SIZE $size 2>&1 | grep ms | awk 'NR % 2 == 1' | awk '{print $8}' | sort -n | awk 'BEGIN{c=0;sum=0;} /^[^#]/{a[c++]=$1;sum+=$1;} END{ave=sum/c; if((c%2)==1){median=a[int(c/2)];} else{median=(a[c/2]+a[c/2-1])/2;} printf "%12.6f\t %12.6f\t %12.6f\n", ave,a[0],a[c-1]}'
    rm -f /tmp/opencl*
  done
}

function run_sizes3_i
{
  printf "%20s\t %12s\t %12s\t %12s\t %12s\t %12s\t %12s\t %12s\t %12s\n" "Name" "Type" "Size" "MeanGFlop" "MinGFlop" "MaxGFlop" "MeanMs" "MinMs" "MaxMs"
  for size in $SIZES3; do
    printf "%20s\t %12d\t %12s\t " "ifft_1d_dp_batch_c2r" $3 $size
    ./$EXE 0 $1 $2 $NUM_TESTS_PER_SIZE $BATCH_SIZE $size 2>&1 | grep flops | awk 'NR % 2 == 1' | awk '{print $8}' | sort -n | awk 'BEGIN{c=0;sum=0;} /^[^#]/{a[c++]=$1;sum+=$1;} END{ave=sum/c; if((c%2)==1){median=a[int(c/2)];} else{median=(a[c/2]+a[c/2-1])/2;} printf "%12.6f\t %12.6f\t %12.6f\t ", ave,a[0],a[c-1]}'
    rm -f /tmp/opencl*
    ./$EXE 0 $1 $2 $NUM_TESTS_PER_SIZE $BATCH_SIZE $size 2>&1 | grep ms | awk 'NR % 2 == 1' | awk '{print $8}' | sort -n | awk 'BEGIN{c=0;sum=0;} /^[^#]/{a[c++]=$1;sum+=$1;} END{ave=sum/c; if((c%2)==1){median=a[int(c/2)];} else{median=(a[c/2]+a[c/2-1])/2;} printf "%12.6f\t %12.6f\t %12.6f\n", ave,a[0],a[c-1]}'
    rm -f /tmp/opencl*
  done
}

function run_sizes5
{
  printf "%20s\t %12s\t %12s\t %12s\t %12s\t %12s\t %12s\t %12s\t %12s\n" "Name" "Type" "Size" "MeanGFlop" "MinGFlop" "MaxGFlop" "MeanMs" "MinMs" "MaxMs"
  for size in $SIZES5; do
    printf "%20s\t %12d\t %12s\t " "fft_1d_dp_batch_r2c" $3 $size
    ./$EXE 0 $1 $2 $NUM_TESTS_PER_SIZE $BATCH_SIZE $size 2>&1 | grep flops | awk 'NR % 2 == 1' | awk '{print $8}' | sort -n | awk 'BEGIN{c=0;sum=0;} /^[^#]/{a[c++]=$1;sum+=$1;} END{ave=sum/c; if((c%2)==1){median=a[int(c/2)];} else{median=(a[c/2]+a[c/2-1])/2;} printf "%12.6f\t %12.6f\t %12.6f\t ", ave,a[0],a[c-1]}'
    rm -f /tmp/opencl*
    ./$EXE 0 $1 $2 $NUM_TESTS_PER_SIZE $BATCH_SIZE $size 2>&1 | grep ms | awk 'NR % 2 == 1' | awk '{print $8}' | sort -n | awk 'BEGIN{c=0;sum=0;} /^[^#]/{a[c++]=$1;sum+=$1;} END{ave=sum/c; if((c%2)==1){median=a[int(c/2)];} else{median=(a[c/2]+a[c/2-1])/2;} printf "%12.6f\t %12.6f\t %12.6f\n", ave,a[0],a[c-1]}'
    rm -f /tmp/opencl*
  done
}

function run_sizes5_i
{
  printf "%20s\t %12s\t %12s\t %12s\t %12s\t %12s\t %12s\t %12s\t %12s\n" "Name" "Type" "Size" "MeanGFlop" "MinGFlop" "MaxGFlop" "MeanMs" "MinMs" "MaxMs"
  for size in $SIZES5; do
    printf "%20s\t %12d\t %12s\t " "ifft_1d_dp_batch_c2r" $3 $size
    ./$EXE 0 $1 $2 $NUM_TESTS_PER_SIZE $BATCH_SIZE $size 2>&1 | grep flops | awk 'NR % 2 == 1' | awk '{print $8}' | sort -n | awk 'BEGIN{c=0;sum=0;} /^[^#]/{a[c++]=$1;sum+=$1;} END{ave=sum/c; if((c%2)==1){median=a[int(c/2)];} else{median=(a[c/2]+a[c/2-1])/2;} printf "%12.6f\t %12.6f\t %12.6f\t ", ave,a[0],a[c-1]}'
    rm -f /tmp/opencl*
    ./$EXE 0 $1 $2 $NUM_TESTS_PER_SIZE $BATCH_SIZE $size 2>&1 | grep ms | awk 'NR % 2 == 1' | awk '{print $8}' | sort -n | awk 'BEGIN{c=0;sum=0;} /^[^#]/{a[c++]=$1;sum+=$1;} END{ave=sum/c; if((c%2)==1){median=a[int(c/2)];} else{median=(a[c/2]+a[c/2-1])/2;} printf "%12.6f\t %12.6f\t %12.6f\n", ave,a[0],a[c-1]}'
    rm -f /tmp/opencl*
  done
}

function run_sizes35
{
  printf "%20s\t %12s\t %12s\t %12s\t %12s\t %12s\t %12s\t %12s\t %12s\n" "Name" "Type" "Size" "MeanGFlop" "MinGFlop" "MaxGFlop" "MeanMs" "MinMs" "MaxMs"
  for size in $SIZES35; do
    printf "%20s\t %12d\t %12s\t " "fft_1d_dp_batch_r2c" $3 $size
    ./$EXE 0 $1 $2 $NUM_TESTS_PER_SIZE $BATCH_SIZE $size 2>&1 | grep flops | awk 'NR % 2 == 1' | awk '{print $8}' | sort -n | awk 'BEGIN{c=0;sum=0;} /^[^#]/{a[c++]=$1;sum+=$1;} END{ave=sum/c; if((c%2)==1){median=a[int(c/2)];} else{median=(a[c/2]+a[c/2-1])/2;} printf "%12.6f\t %12.6f\t %12.6f\t ", ave,a[0],a[c-1]}'
    rm -f /tmp/opencl*
    ./$EXE 0 $1 $2 $NUM_TESTS_PER_SIZE $BATCH_SIZE $size 2>&1 | grep ms | awk 'NR % 2 == 1' | awk '{print $8}' | sort -n | awk 'BEGIN{c=0;sum=0;} /^[^#]/{a[c++]=$1;sum+=$1;} END{ave=sum/c; if((c%2)==1){median=a[int(c/2)];} else{median=(a[c/2]+a[c/2-1])/2;} printf "%12.6f\t %12.6f\t %12.6f\n", ave,a[0],a[c-1]}'
    rm -f /tmp/opencl*
  done
}

function run_sizes35_i
{
  printf "%20s\t %12s\t %12s\t %12s\t %12s\t %12s\t %12s\t %12s\t %12s\n" "Name" "Type" "Size" "MeanGFlop" "MinGFlop" "MaxGFlop" "MeanMs" "MinMs" "MaxMs"
  for size in $SIZES35; do
    printf "%20s\t %12d\t %12s\t " "ifft_1d_dp_batch_c2r" $3 $size
    ./$EXE 0 $1 $2 $NUM_TESTS_PER_SIZE $BATCH_SIZE $size 2>&1 | grep flops | awk 'NR % 2 == 1' | awk '{print $8}' | sort -n | awk 'BEGIN{c=0;sum=0;} /^[^#]/{a[c++]=$1;sum+=$1;} END{ave=sum/c; if((c%2)==1){median=a[int(c/2)];} else{median=(a[c/2]+a[c/2-1])/2;} printf "%12.6f\t %12.6f\t %12.6f\t ", ave,a[0],a[c-1]}'
    rm -f /tmp/opencl*
    ./$EXE 0 $1 $2 $NUM_TESTS_PER_SIZE $BATCH_SIZE $size 2>&1 | grep ms | awk 'NR % 2 == 1' | awk '{print $8}' | sort -n | awk 'BEGIN{c=0;sum=0;} /^[^#]/{a[c++]=$1;sum+=$1;} END{ave=sum/c; if((c%2)==1){median=a[int(c/2)];} else{median=(a[c/2]+a[c/2-1])/2;} printf "%12.6f\t %12.6f\t %12.6f\n", ave,a[0],a[c-1]}'
    rm -f /tmp/opencl*
  done
}

test -x ./$EXE || make $EXE

{
  export TI_FFTW_OFFLOAD=2 #
  echo; run_sizes2 0 1 1

  export TI_FFTW_OFFLOAD=1 # to run DSP tests
  echo; run_sizes2 0 1 3

  export TI_FFTW_OFFLOAD=0 # to run ARM tests
  echo; run_sizes2 0 1 5
} | sed -e "$sed_script"

{
  export TI_FFTW_OFFLOAD=2 #
  echo; run_sizes2_i 0 -1 1

  export TI_FFTW_OFFLOAD=1 # to run DSP tests
  echo; run_sizes2_i 0 -1 3

  export TI_FFTW_OFFLOAD=0 # to run ARM tests
  echo; run_sizes2_i 0 -1 5
} | sed -e "$sed_script"

{
  export TI_FFTW_OFFLOAD=2 #
  echo; run_sizes3 0 1 1

  export TI_FFTW_OFFLOAD=1 # to run DSP tests
  echo; run_sizes3 0 1 3

  export TI_FFTW_OFFLOAD=0 # to run ARM tests
  echo; run_sizes3 0 1 5
} | sed -e "$sed_script"

{
  export TI_FFTW_OFFLOAD=2 #
  echo; run_sizes3_i 0 -1 1

  export TI_FFTW_OFFLOAD=1 # to run DSP tests
  echo; run_sizes3_i 0 -1 3

  export TI_FFTW_OFFLOAD=0 # to run ARM tests
  echo; run_sizes3_i 0 -1 5
} | sed -e "$sed_script"

{
  export TI_FFTW_OFFLOAD=2 #
  echo; run_sizes5 0 1 1

  export TI_FFTW_OFFLOAD=1 # to run DSP tests
  echo; run_sizes5 0 1 3

  export TI_FFTW_OFFLOAD=0 # to run ARM tests
  echo; run_sizes5 0 1 5
} | sed -e "$sed_script"

{
  export TI_FFTW_OFFLOAD=2 #
  echo; run_sizes5_i 0 -1 1

  export TI_FFTW_OFFLOAD=1 # to run DSP tests
  echo; run_sizes5_i 0 -1 3

  export TI_FFTW_OFFLOAD=0 # to run ARM tests
  echo; run_sizes5_i 0 -1 5
} | sed -e "$sed_script"

{
  export TI_FFTW_OFFLOAD=2 #
  echo; run_sizes35 0 1 1

  export TI_FFTW_OFFLOAD=1 # to run DSP tests
  echo; run_sizes35 0 1 3

  export TI_FFTW_OFFLOAD=0 # to run ARM tests
  echo; run_sizes35 0 1 5
} | sed -e "$sed_script"

{
  export TI_FFTW_OFFLOAD=2 #
  echo; run_sizes35_i 0 -1 1

  export TI_FFTW_OFFLOAD=1 # to run DSP tests
  echo; run_sizes35_i 0 -1 3

  export TI_FFTW_OFFLOAD=0 # to run ARM tests
  echo; run_sizes35_i 0 -1 5
} | sed -e "$sed_script"

