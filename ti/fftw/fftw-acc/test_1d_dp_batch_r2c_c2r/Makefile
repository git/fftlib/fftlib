include ../make.inc

ACCEL_LIB_STATIC=../host/libfftw_acc.a

CCP = g++

ifeq ($(TI_FFTW_DEBUG), 1)
  CFLAGS += -O1 -ggdb
else
  CFLAGS += -O1 -ggdb
# -DNDEBUG
endif

TARGETS = fft_static fft demo_static
ifeq (,$(findstring 86, $(UNAME_M)))
TARGETS += static shared
ALWAYSCCP = g++
else
ALWAYSCCP = arm-linux-gnueabihf-g++
endif

all: test

test: test-static test-shared

static test-static: fft_static
	@echo This test links against a statically built FFTW
	@echo This approach will work for both static and shared executables
	@echo "****************************************"
	@echo "Doing an OUT-OF-PLACE transform"
	./fft_static 0 0 1 3 100 1024 ; rm -rf /tmp/opencl*
	@echo
	@echo "Doing an IN-PLACE transform"
	./fft_static 0 1 -1 3 100 1024 ; rm -rf /tmp/opencl*
	@echo "****************************************"

shared test-shared: fft
	@echo This test uses the dlsym intercept approach for shared executables
	@echo "****************************************"
	@echo "Now doing forward FFT with offload and backward without offload"
	@echo "Doing an OUT-OF-PLACE transform"
	LD_PRELOAD=../host/libfftw_acc.so ./fft 0 0 1 3 100 1024 ; rm -rf /tmp/opencl*
	@echo
	@echo "Doing an IN-PLACE transform"
	LD_PRELOAD=../host/libfftw_acc.so ./fft 0 1 -1 3 100 1024 ; rm -rf /tmp/opencl*
	@echo "****************************************"

fft.o: fft.c
	$(CCP) $(CFLAGS) -I$(FFTW_PREFIX)/include -c fft.c 

fft: fft.o 
	$(CCP) $(CFLAGS) fft.o -L$(FFTW_PREFIX)/lib -Wl,-rpath,$(FFTW_PREFIX)/lib -lfftw3 -lm -lrt -o $@

fft_static: fft.o $(ACCEL_LIB_STATIC)
	$(CCP) $(CFLAGS) $< $(ACCEL_LIB_STATIC) $(LD_FLAGS) -lOpenCL -locl_util -lrt -lm -o $@

demo_static: demo.cpp $(ACCEL_LIB_STATIC)
	$(ALWAYSCCP) $(CFLAGS) -I$(FFTW_PREFIX)/include $< $(ACCEL_LIB_STATIC) $(LD_FLAGS) -lOpenCL -locl_util -lrt -lm -o $@

stats: fft_static genstats
	./genstats

statsraw: fft_static genstats
	./genstats_raw

csv: fft_static genstats_csv
	./genstats_csv

clean:
	rm -fv *.o fft fft_static demo_static
