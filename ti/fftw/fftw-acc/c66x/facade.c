/* ======================================================================= */
/*  TEXAS INSTRUMENTS, INC.                                                */
/*                                                                         */
/*  FFTLIB  FFT Library                                                    */
/*                                                                         */
/* Copyright (C) 2014 Texas Instruments Incorporated - http://www.ti.com/  */
/*                                                                         */
/*                                                                         */
/*  Redistribution and use in source and binary forms, with or without     */
/*  modification, are permitted provided that the following conditions     */
/*  are met:                                                               */
/*                                                                         */
/*    Redistributions of source code must retain the above copyright       */
/*    notice, this list of conditions and the following disclaimer.        */
/*                                                                         */
/*    Redistributions in binary form must reproduce the above copyright    */
/*    notice, this list of conditions and the following disclaimer in the  */
/*    documentation and/or other materials provided with the               */
/*    distribution.                                                        */
/*                                                                         */
/*    Neither the name of Texas Instruments Incorporated nor the names of  */
/*    its contributors may be used to endorse or promote products derived  */
/*    from this software without specific prior written permission.        */
/*                                                                         */
/*  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS    */
/*  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT      */
/*  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR  */
/*  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT   */
/*  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,  */
/*  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT       */
/*  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,  */
/*  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY  */
/*  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT    */
/*  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE  */
/*  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.   */
/*                                                                         */
/* ======================================================================= */
#include <stdio.h>
#include <time.h>
#include <stdlib.h>
#include <limits.h>
#include <math.h>
#include <xdc/std.h>

#ifdef TI_FFTW_ALLOW_ECPY
#include "ti/fftlib/fftlib.h"

int fftEdmaState[ACTIVE_THREAD_COUNT];
FFT_EDMA_Struct gEdmaState[ACTIVE_THREAD_COUNT];
lib_emt_ConfigLarge_t cfg;
IALG_MemRec memTab[FFT_NUM_EDMA_CH];

/* Function to trap exceptions */
void fft_assert(int statement, int node_id, const char *error)
{
  if(!statement) printf("%s (%d)\n",error,node_id);
}

void *fft_omp_assign_edma_resources(void *buf, int buf_size)
{
  /*
   * The edmaInstances are indexes into the C6678_config[] array defined in
   * fft_c6678_config, which is used to specify how EDMA resources are
   * divided between cores.
   */
  int   i, ret_val;
  void *ret = (void *) (&gEdmaState[0]);
  uint32_t *buf0 = buf;

  cfg.max_num_lines = FFT_MAX_EDMA_LINKS_3D;
  for (i = 0; i < FFT_NUM_EDMA_CH; i++) {
    ret_val = lib_emt_getSizesLarge(&cfg, &memTab[i]);
    fft_assert( (ret_val == LIB_EMT_SUCCESS), DNUM, "Error with lib_emt_getSizesLarge()");
    memTab[i].base = buf0;
    buf0 += memTab[i].size/sizeof(uint32_t);
  }

  fft_assert( ((memTab[0].size*FFT_NUM_EDMA_CH) <= buf_size), DNUM, "Cannot satisfy large channel memory requirements!");

  #pragma omp parallel
  {
    if ( fftEdmaState[DNUM] != FFT_EDMA_STATE_ALLOCATED )
    {
      gEdmaState[DNUM].num_channels = 0;

      while ( gEdmaState[DNUM].num_channels < FFT_NUM_EDMA_CH )
      {
        fft_assert( ((gEdmaState[DNUM].channel[gEdmaState[DNUM].num_channels]) = lib_emt_allocLarge(&cfg, &memTab[gEdmaState[DNUM].num_channels])) != NULL , DNUM, "lib_emt_alloc() failed ");

        gEdmaState[DNUM].num_channels++;
      }
    }
    fftEdmaState[DNUM] = FFT_EDMA_STATE_ALLOCATED;
  }

  return ret;
}

void fft_omp_free_edma_resources(void *edma)
{
  /*
   * The edmaInstances are indexes into the C6678_config[] array defined in
   * fft_c6678_config, which is used to specify how EDMA resources are
   * divided between cores.
   */
  int ret_val;

  #pragma omp parallel
  {
     if ( fftEdmaState[DNUM] == FFT_EDMA_STATE_ALLOCATED )
    {
      while ( gEdmaState[DNUM].num_channels > 0 )
      {
        gEdmaState[DNUM].num_channels--;

        ret_val = lib_emt_free(gEdmaState[DNUM].channel[gEdmaState[DNUM].num_channels]);

        fft_assert(ret_val == LIB_EMT_SUCCESS, DNUM, "EDMA free failed!");

      }
    }
    fftEdmaState[DNUM] = FFT_EDMA_STATE_INIT;
  }
}

/*ADD KERNEL: Add a function like below for each FFTW offload done using ECPY*/
void FFT_plan_omp_1d_dp_c2c_facade (int N, double *in, double *out, double *tw,
                                    int type, int mode, char *plan)
{
    fft_plan_t *p = (void*)plan;
    fft_callout_t plan_fxns;

    /* initalize callout functions */
    plan_fxns.memoryRequest   = 0;
    plan_fxns.ecpyRequest     = fft_omp_assign_edma_resources;
    plan_fxns.memoryRelease   = 0;
    plan_fxns.ecpyRelease     = fft_omp_free_edma_resources;

    int i;
    for (i=0;i<ACTIVE_THREAD_COUNT;++i) fftEdmaState[i] = FFT_EDMA_STATE_INIT;

    /* If type == 0 => call forward plan as below */
    /* If type == 1 => call ifft_omp_dp_plan_1d_c2c instead */
  if (type == 0){
    fft_omp_dp_plan_1d_c2c (N, FFT_ECPY, plan_fxns, p, in ,out, tw);
  }
  else{
    ifft_omp_dp_plan_1d_c2c (N, FFT_ECPY, plan_fxns, p, in ,out, tw);
  }

}

void FFT_plan_omp_1d_dp_r2c_facade (int N, double *in, double *out, double *tw,
                                    int type, int mode, char *plan)
{
    fft_plan_t *p = (void*)plan;
    fft_callout_t plan_fxns;

    /* initalize callout functions */
    plan_fxns.memoryRequest   = 0;
    plan_fxns.ecpyRequest     = fft_omp_assign_edma_resources;
    plan_fxns.memoryRelease   = 0;
    plan_fxns.ecpyRelease     = fft_omp_free_edma_resources;

    int i;
    for (i=0;i<ACTIVE_THREAD_COUNT;++i) fftEdmaState[i] = FFT_EDMA_STATE_INIT;

    /* this kernel supports only forward plan and ignore the value of type */
    fft_omp_dp_plan_1d_r2c (N, FFT_ECPY, plan_fxns, p, in ,out, tw);
}

void FFT_plan_omp_1d_dp_c2r_facade (int N, double *in, double *out, double *tw,
                                    int type, int mode, char *plan)
{
    fft_plan_t *p = (void*)plan;
    fft_callout_t plan_fxns;

    /* initalize callout functions */
    plan_fxns.memoryRequest   = 0;
    plan_fxns.ecpyRequest     = fft_omp_assign_edma_resources;
    plan_fxns.memoryRelease   = 0;
    plan_fxns.ecpyRelease     = fft_omp_free_edma_resources;

    int i;
    for (i=0;i<ACTIVE_THREAD_COUNT;++i) fftEdmaState[i] = FFT_EDMA_STATE_INIT;

    /* this kernel support only backward plan and ignore the value of type */
    ifft_omp_dp_plan_1d_c2r (N, FFT_ECPY, plan_fxns, p, in ,out, tw);
}

/* offload plan for 2d dp c2c */
void FFT_plan_omp_2d_dp_c2c_facade (int N1, int N2, double *in, double *out, double *tw,
                                    int type, int mode, char *plan)
{
    fft_plan_t *p = (void*)plan;
    fft_callout_t plan_fxns;
    double *ptr_scratch = NULL;  // scratch buffer for Bn calculation, only used for Bluestein 

    /* initalize callout functions */
    plan_fxns.memoryRequest   = 0;
    plan_fxns.ecpyRequest     = fft_omp_assign_edma_resources;
    plan_fxns.memoryRelease   = 0;
    plan_fxns.ecpyRelease     = fft_omp_free_edma_resources;

    int i;
    for (i=0;i<ACTIVE_THREAD_COUNT;++i) fftEdmaState[i] = FFT_EDMA_STATE_INIT;

    /* If type == 0 => call forward plan as below */
    /* If type == 1 => call ifft_omp_dp_plan_2d_c2c instead */

  /* get the size of the scratch buffer calculated in ARM */
#if 0
  /* anysize is not supported at this time */
  if ((p->u.dp_2d_c2c_e.para1.use_bs == 1) || (p->u.dp_2d_c2c_e.para2.use_bs == 1))
  { /* only allocate memory for scratch buffer when BlueStein is needed */
    ptr_scratch = malloc(sizeof(double)*p->u.dp_2d_c2c_e.para1.worksize);
  }
#endif
  if (type == 0)
      fft_omp_dp_plan_2d_c2c (N2, N1, FFT_ECPY, plan_fxns, p, in ,out, tw, ptr_scratch);
	else
	  ifft_omp_dp_plan_2d_c2c (N2, N1, FFT_ECPY, plan_fxns, p, in ,out, tw, ptr_scratch);

  if (ptr_scratch != NULL)
    free(ptr_scratch);
}

/* offload plan for 2d dp r2c */
void FFT_plan_omp_2d_dp_r2c_facade (int N1, int N2, double *in, double *out, double *tw,
                                    int type, int mode, char *plan)
{
    fft_plan_t *p = (void*)plan;
    fft_callout_t plan_fxns;
    double *ptr_scratch = NULL;  // scratch buffer for Bn calculation, only used for Bluestein 

    /* initalize callout functions */
    plan_fxns.memoryRequest   = 0;
    plan_fxns.ecpyRequest     = fft_omp_assign_edma_resources;
    plan_fxns.memoryRelease   = 0;
    plan_fxns.ecpyRelease     = fft_omp_free_edma_resources;

    int i;
    for (i=0;i<ACTIVE_THREAD_COUNT;++i) fftEdmaState[i] = FFT_EDMA_STATE_INIT;

#if 0
  /* get the size of the scratch buffer calculated in ARM */
  if ((p->u.dp_2d_r2c_e.para1.use_bs == 1) || (p->u.dp_2d_r2c_e.para2.use_bs == 1))
  { /* only allocate memory for scratch buffer when BlueStein is needed */
    ptr_scratch = malloc(sizeof(double)*p->u.dp_2d_r2c_e.para1.worksize);
  }
#endif

    fft_omp_dp_plan_2d_r2c (N2, N1, FFT_ECPY, plan_fxns, p, in , out, tw, ptr_scratch);

  if (ptr_scratch != NULL)
    free(ptr_scratch);
}

/* offload plan for 2d dp c2r */
void FFT_plan_omp_2d_dp_c2r_facade (int N1, int N2, double *in, double *out, double *tw,
                                    int type, int mode, char *plan)
{
    fft_plan_t *p = (void*)plan;
    fft_callout_t plan_fxns;
    double *ptr_scratch = NULL;  // scratch buffer for Bn calculation, only used for Bluestein 

    /* initalize callout functions */
    plan_fxns.memoryRequest   = 0;
    plan_fxns.ecpyRequest     = fft_omp_assign_edma_resources;
    plan_fxns.memoryRelease   = 0;
    plan_fxns.ecpyRelease     = fft_omp_free_edma_resources;

    int i;
    for (i=0;i<ACTIVE_THREAD_COUNT;++i) fftEdmaState[i] = FFT_EDMA_STATE_INIT;

    /* If type == 0 => call forward plan as below */
    /* If type == 1 => call ifft_omp_dp_plan_2d_c2c instead */

#if 0
  /* get the size of the scratch buffer calculated in ARM */
  if ((p->u.dp_2d_c2r_e.para1.use_bs == 1) || (p->u.dp_2d_c2r_e.para2.use_bs == 1))
  { /* only allocate memory for scratch buffer when BlueStein is needed */
    ptr_scratch = malloc(sizeof(double)*p->u.dp_2d_c2r_e.para1.worksize);
  }
#endif

    ifft_omp_dp_plan_2d_c2r (N2, N1, FFT_ECPY, plan_fxns, p, in ,out, tw, ptr_scratch);

  if (ptr_scratch != NULL)
    free(ptr_scratch);
}

/* offload plan for 3d dp c2c */
void FFT_plan_omp_3d_dp_c2c_facade (int N1, int N2, int N3, double *in, double *out, double *tw,
                                    int type, int mode, char *plan)
{
    fft_plan_t *p = (void*)plan;
    fft_callout_t plan_fxns;
	double *ptr_scratch = NULL;  // scratch buffer for Bn calculation, only used for Bluestein 

    /* initalize callout functions */
    plan_fxns.memoryRequest   = 0;
    plan_fxns.ecpyRequest     = fft_omp_assign_edma_resources;
    plan_fxns.memoryRelease   = 0;
    plan_fxns.ecpyRelease     = fft_omp_free_edma_resources;

    int i;
    for (i=0;i<ACTIVE_THREAD_COUNT;++i) fftEdmaState[i] = FFT_EDMA_STATE_INIT;


    /* If type == 0 => call forward plan as below */
    /* If type == 1 => call ifft_omp_dp_plan_2d_c2c instead */

	/* get the size of the scratch buffer calculated in ARM */
#if 0
	/* anysize is not supported at this time */
	if ((p->u.dp_3d_c2c_e.para1.use_bs == 1) || (p->u.dp_3d_c2c_e.para2.use_bs == 1) || (p->u.dp_3d_c2c_e.para3.use_bs == 1))
	{ /* only allocate memory for scratch buffer when BlueStein is needed */
	  ptr_scratch = malloc(sizeof(double)*p->u.dp_3d_c2c_e.para1.worksize);
	}
#endif
	if (type == 0)
      fft_omp_dp_plan_3d_c2c (N1, N2, N3, FFT_ECPY, plan_fxns, p, in ,out, tw, ptr_scratch);
	else
	  ifft_omp_dp_plan_3d_c2c (N1, N2, N3, FFT_ECPY, plan_fxns, p, in ,out, tw, ptr_scratch);

	if (ptr_scratch != NULL)
	  free(ptr_scratch);
}

/* offload plan for 3d dp r2c */
void FFT_plan_omp_3d_dp_r2c_facade (int N1, int N2, int N3, double *in, double *out, double *tw,
                                    int type, int mode, char *plan)
{
    fft_plan_t *p = (void*)plan;
    fft_callout_t plan_fxns;
	double *ptr_scratch = NULL;  // scratch buffer for Bn calculation, only used for Bluestein 

    /* initalize callout functions */
    plan_fxns.memoryRequest   = 0;
    plan_fxns.ecpyRequest     = fft_omp_assign_edma_resources;
    plan_fxns.memoryRelease   = 0;
    plan_fxns.ecpyRelease     = fft_omp_free_edma_resources;

    int i;
    for (i=0;i<ACTIVE_THREAD_COUNT;++i) fftEdmaState[i] = FFT_EDMA_STATE_INIT;


#if 0
	/* get the size of the scratch buffer calculated in ARM */
	if ((p->u.dp_3d_r2c_e.para1.use_bs == 1) || (p->u.dp_3d_r2c_e.para2.use_bs == 1) || (p->u.dp_3d_r2c_e.para3.use_bs == 1))
	{ /* only allocate memory for scratch buffer when BlueStein is needed */
	  ptr_scratch = malloc(sizeof(double)*p->u.dp_3d_r2c_e.para1.worksize);
	}
#endif

    fft_omp_dp_plan_3d_r2c (N1, N2, N3, FFT_ECPY, plan_fxns, p, in , out, tw, ptr_scratch);

	if (ptr_scratch != NULL)
	  free(ptr_scratch);
}

/* offload plan for 3d dp c2r */
void FFT_plan_omp_3d_dp_c2r_facade (int N1, int N2, int N3, double *in, double *out, double *tw,
                                    int type, int mode, char *plan)
{
    fft_plan_t *p = (void*)plan;
    fft_callout_t plan_fxns;
	double *ptr_scratch = NULL;  // scratch buffer for Bn calculation, only used for Bluestein 

    /* initalize callout functions */
    plan_fxns.memoryRequest   = 0;
    plan_fxns.ecpyRequest     = fft_omp_assign_edma_resources;
    plan_fxns.memoryRelease   = 0;
    plan_fxns.ecpyRelease     = fft_omp_free_edma_resources;

    int i;
    for (i=0;i<ACTIVE_THREAD_COUNT;++i) fftEdmaState[i] = FFT_EDMA_STATE_INIT;


    /* If type == 0 => call forward plan as below */
    /* If type == 1 => call ifft_omp_dp_plan_2d_c2c instead */

#if 0
	/* get the size of the scratch buffer calculated in ARM */
	if ((p->u.dp_3d_c2r_e.para1.use_bs == 1) || (p->u.dp_3d_c2r_e.para2.use_bs == 1) || (p->u.dp_3d_c2r_e.para3.use_bs == 1))
	{ /* only allocate memory for scratch buffer when BlueStein is needed */
	  ptr_scratch = malloc(sizeof(double)*p->u.dp_3d_c2r_e.para1.worksize);
	}
#endif

    ifft_omp_dp_plan_3d_c2r (N1, N2, N3, FFT_ECPY, plan_fxns, p, in ,out, tw, ptr_scratch);

	if (ptr_scratch != NULL)
	  free(ptr_scratch); 
}

/* offload plan for 1d dp c2c batched */
void FFT_plan_1d_dp_c2c_batch_facade (int N, double *in, double *out, double *tw,
                                      int type, int mode, char *plan)
{
    int i;
    fft_plan_t *p = (void*)plan;
    fft_callout_t plan_fxns;

    /* initalize callout functions */
    plan_fxns.memoryRequest   = 0;
    plan_fxns.ecpyRequest     = fft_omp_assign_edma_resources;
    plan_fxns.memoryRelease   = 0;
    plan_fxns.ecpyRelease     = fft_omp_free_edma_resources;

    for (i=0;i<ACTIVE_THREAD_COUNT;++i) fftEdmaState[i] = FFT_EDMA_STATE_INIT;

    /* If type == 0 => call forward plan as below */
    /* If type == 1 => call ifft_omp_dp_plan_2d_c2c instead */
    if (type == 0)
      fft_dp_plan_1d_c2c_batch (N, FFT_ECPY, plan_fxns, p, in ,out, tw, 0);
    else
      ifft_dp_plan_1d_c2c_batch (N, FFT_ECPY, plan_fxns, p, in ,out, tw, 0);
}

/* offload plan for 1d dp c2c batched */
void FFT_plan_1d_dp_r2c_batch_facade (int N, double *in, double *out, double *tw,
                                      int type, int mode, char *plan)
{
    int i;
    fft_plan_t *p = (void*)plan;
    fft_callout_t plan_fxns;

    /* initalize callout functions */
    plan_fxns.memoryRequest   = 0;
    plan_fxns.ecpyRequest     = fft_omp_assign_edma_resources;
    plan_fxns.memoryRelease   = 0;
    plan_fxns.ecpyRelease     = fft_omp_free_edma_resources;

    for (i=0;i<ACTIVE_THREAD_COUNT;++i) fftEdmaState[i] = FFT_EDMA_STATE_INIT;

    /* this kernel support only forward plan and ignore the value of type */
    fft_dp_plan_1d_r2c_batch (N, FFT_ECPY, plan_fxns, p, in ,out, tw, 0);
}

/* offload plan for 1d dp c2c batched */
void FFT_plan_1d_dp_c2r_batch_facade (int N, double *in, double *out, double *tw,
                                      int type, int mode, char *plan)
{
    int i;
    fft_plan_t *p = (void*)plan;
    fft_callout_t plan_fxns;

    /* initalize callout functions */
    plan_fxns.memoryRequest   = 0;
    plan_fxns.ecpyRequest     = fft_omp_assign_edma_resources;
    plan_fxns.memoryRelease   = 0;
    plan_fxns.ecpyRelease     = fft_omp_free_edma_resources;

    for (i=0;i<ACTIVE_THREAD_COUNT;++i) fftEdmaState[i] = FFT_EDMA_STATE_INIT;

    /* this kernel support only backward plan and ignore the value of type */
    ifft_dp_plan_1d_c2r_batch (N, FFT_ECPY, plan_fxns, p, in ,out, tw, 0);
}

void FFT_plan_omp_1d_sp_c2c_facade (int N, float *in, float *out, float *tw,
                                    int type, int mode, char *plan)
{
    fft_plan_t *p = (void*)plan;
    fft_callout_t plan_fxns;

    /* initalize callout functions */
    plan_fxns.memoryRequest   = 0;
    plan_fxns.ecpyRequest     = fft_omp_assign_edma_resources;
    plan_fxns.memoryRelease   = 0;
    plan_fxns.ecpyRelease     = fft_omp_free_edma_resources;

    int i;
    for (i=0;i<ACTIVE_THREAD_COUNT;++i) fftEdmaState[i] = FFT_EDMA_STATE_INIT;

    /* If type == 0 => call forward plan as below */
    /* If type == 1 => call ifft_omp_sp_plan_1d_c2c instead */
  if (type == 0){
    fft_omp_sp_plan_1d_c2c (N, FFT_ECPY, plan_fxns, p, in ,out, tw);
  }
  else{
    ifft_omp_sp_plan_1d_c2c (N, FFT_ECPY, plan_fxns, p, in ,out, tw);
  }

}

void FFT_plan_omp_1d_sp_r2c_facade (int N, float *in, float *out, float *tw,
                                    int type, int mode, char *plan)
{
    fft_plan_t *p = (void*)plan;
    fft_callout_t plan_fxns;

    /* initalize callout functions */
    plan_fxns.memoryRequest   = 0;
    plan_fxns.ecpyRequest     = fft_omp_assign_edma_resources;
    plan_fxns.memoryRelease   = 0;
    plan_fxns.ecpyRelease     = fft_omp_free_edma_resources;

    int i;
    for (i=0;i<ACTIVE_THREAD_COUNT;++i) fftEdmaState[i] = FFT_EDMA_STATE_INIT;

    /* this kernel supports only forward plan and ignore the value of type */
    fft_omp_sp_plan_1d_r2c (N, FFT_ECPY, plan_fxns, p, in ,out, tw);
}

void FFT_plan_omp_1d_sp_c2r_facade (int N, float *in, float *out, float *tw,
                                    int type, int mode, char *plan)
{
    fft_plan_t *p = (void*)plan;
    fft_callout_t plan_fxns;

    /* initalize callout functions */
    plan_fxns.memoryRequest   = 0;
    plan_fxns.ecpyRequest     = fft_omp_assign_edma_resources;
    plan_fxns.memoryRelease   = 0;
    plan_fxns.ecpyRelease     = fft_omp_free_edma_resources;

    int i;
    for (i=0;i<ACTIVE_THREAD_COUNT;++i) fftEdmaState[i] = FFT_EDMA_STATE_INIT;

    /* this kernel support only backward plan and ignore the value of type */
    ifft_omp_sp_plan_1d_c2r (N, FFT_ECPY, plan_fxns, p, in ,out, tw);
}

/* offload plan for 2d sp c2c */
void FFT_plan_omp_2d_sp_c2c_facade (int N1, int N2, float *in, float *out, float *tw,
                                    int type, int mode, char *plan)
{
    fft_plan_t *p = (void*)plan;
    fft_callout_t plan_fxns;
    float *ptr_scratch = NULL;  // scratch buffer for Bn calculation, only used for Bluestein 

    /* initalize callout functions */
    plan_fxns.memoryRequest   = 0;
    plan_fxns.ecpyRequest     = fft_omp_assign_edma_resources;
    plan_fxns.memoryRelease   = 0;
    plan_fxns.ecpyRelease     = fft_omp_free_edma_resources;

    int i;
    for (i=0;i<ACTIVE_THREAD_COUNT;++i) fftEdmaState[i] = FFT_EDMA_STATE_INIT;

    /* If type == 0 => call forward plan as below */
    /* If type == 1 => call ifft_omp_sp_plan_2d_c2c instead */

  /* get the size of the scratch buffer calculated in ARM */
#if 0
  /* anysize is not supported at this time */
  if ((p->u.sp_2d_c2c_e.para1.use_bs == 1) || (p->u.sp_2d_c2c_e.para2.use_bs == 1))
  { /* only allocate memory for scratch buffer when BlueStein is needed */
    ptr_scratch = malloc(sizeof(float)*p->u.sp_2d_c2c_e.para1.worksize);
  }
#endif
  if (type == 0)
      fft_omp_sp_plan_2d_c2c (N2, N1, FFT_ECPY, plan_fxns, p, in ,out, tw, ptr_scratch);
	else
	  ifft_omp_sp_plan_2d_c2c (N2, N1, FFT_ECPY, plan_fxns, p, in ,out, tw, ptr_scratch);

  if (ptr_scratch != NULL)
    free(ptr_scratch);
}

/* offload plan for 2d sp r2c */
void FFT_plan_omp_2d_sp_r2c_facade (int N1, int N2, float *in, float *out, float *tw,
                                    int type, int mode, char *plan)
{
    fft_plan_t *p = (void*)plan;
    fft_callout_t plan_fxns;
    float *ptr_scratch = NULL;  // scratch buffer for Bn calculation, only used for Bluestein 

    /* initalize callout functions */
    plan_fxns.memoryRequest   = 0;
    plan_fxns.ecpyRequest     = fft_omp_assign_edma_resources;
    plan_fxns.memoryRelease   = 0;
    plan_fxns.ecpyRelease     = fft_omp_free_edma_resources;

    int i;
    for (i=0;i<ACTIVE_THREAD_COUNT;++i) fftEdmaState[i] = FFT_EDMA_STATE_INIT;

#if 0
  /* get the size of the scratch buffer calculated in ARM */
  if ((p->u.sp_2d_r2c_e.para1.use_bs == 1) || (p->u.sp_2d_r2c_e.para2.use_bs == 1))
  { /* only allocate memory for scratch buffer when BlueStein is needed */
    ptr_scratch = malloc(sizeof(float)*p->u.sp_2d_r2c_e.para1.worksize);
  }
#endif

    fft_omp_sp_plan_2d_r2c (N2, N1, FFT_ECPY, plan_fxns, p, in , out, tw, ptr_scratch);

  if (ptr_scratch != NULL)
    free(ptr_scratch);
}

/* offload plan for 2d sp c2r */
void FFT_plan_omp_2d_sp_c2r_facade (int N1, int N2, float *in, float *out, float *tw,
                                    int type, int mode, char *plan)
{
    fft_plan_t *p = (void*)plan;
    fft_callout_t plan_fxns;
    float *ptr_scratch = NULL;  // scratch buffer for Bn calculation, only used for Bluestein 

    /* initalize callout functions */
    plan_fxns.memoryRequest   = 0;
    plan_fxns.ecpyRequest     = fft_omp_assign_edma_resources;
    plan_fxns.memoryRelease   = 0;
    plan_fxns.ecpyRelease     = fft_omp_free_edma_resources;

    int i;
    for (i=0;i<ACTIVE_THREAD_COUNT;++i) fftEdmaState[i] = FFT_EDMA_STATE_INIT;

    /* If type == 0 => call forward plan as below */
    /* If type == 1 => call ifft_omp_sp_plan_2d_c2c instead */

#if 0
  /* get the size of the scratch buffer calculated in ARM */
  if ((p->u.sp_2d_c2r_e.para1.use_bs == 1) || (p->u.sp_2d_c2r_e.para2.use_bs == 1))
  { /* only allocate memory for scratch buffer when BlueStein is needed */
    ptr_scratch = malloc(sizeof(float)*p->u.sp_2d_c2r_e.para1.worksize);
  }
#endif

    ifft_omp_sp_plan_2d_c2r (N2, N1, FFT_ECPY, plan_fxns, p, in ,out, tw, ptr_scratch);

  if (ptr_scratch != NULL)
    free(ptr_scratch);
}

/* offload plan for 3d sp c2c */
void FFT_plan_omp_3d_sp_c2c_facade (int N1, int N2, int N3, float *in, float *out, float *tw,
                                    int type, int mode, char *plan)
{
    fft_plan_t *p = (void*)plan;
    fft_callout_t plan_fxns;
	float *ptr_scratch = NULL;  // scratch buffer for Bn calculation, only used for Bluestein 

    /* initalize callout functions */
    plan_fxns.memoryRequest   = 0;
    plan_fxns.ecpyRequest     = fft_omp_assign_edma_resources;
    plan_fxns.memoryRelease   = 0;
    plan_fxns.ecpyRelease     = fft_omp_free_edma_resources;

    int i;
    for (i=0;i<ACTIVE_THREAD_COUNT;++i) fftEdmaState[i] = FFT_EDMA_STATE_INIT;


    /* If type == 0 => call forward plan as below */
    /* If type == 1 => call ifft_omp_sp_plan_2d_c2c instead */

	/* get the size of the scratch buffer calculated in ARM */
#if 0
	/* anysize is not supported at this time */
	if ((p->u.sp_3d_c2c_e.para1.use_bs == 1) || (p->u.sp_3d_c2c_e.para2.use_bs == 1) || (p->u.sp_3d_c2c_e.para3.use_bs == 1))
	{ /* only allocate memory for scratch buffer when BlueStein is needed */
	  ptr_scratch = malloc(sizeof(float)*p->u.sp_3d_c2c_e.para1.worksize);
	}
#endif
	if (type == 0)
          fft_omp_sp_plan_3d_c2c (N1, N2, N3, FFT_ECPY, plan_fxns, p, in ,out, tw);
	else
          ifft_omp_sp_plan_3d_c2c (N1, N2, N3, FFT_ECPY, plan_fxns, p, in ,out, tw);

	if (ptr_scratch != NULL)
	  free(ptr_scratch);
}

/* offload plan for 3d sp r2c */
void FFT_plan_omp_3d_sp_r2c_facade (int N1, int N2, int N3, float *in, float *out, float *tw,
                                    int type, int mode, char *plan)
{
    fft_plan_t *p = (void*)plan;
    fft_callout_t plan_fxns;
	float *ptr_scratch = NULL;  // scratch buffer for Bn calculation, only used for Bluestein 

    /* initalize callout functions */
    plan_fxns.memoryRequest   = 0;
    plan_fxns.ecpyRequest     = fft_omp_assign_edma_resources;
    plan_fxns.memoryRelease   = 0;
    plan_fxns.ecpyRelease     = fft_omp_free_edma_resources;

    int i;
    for (i=0;i<ACTIVE_THREAD_COUNT;++i) fftEdmaState[i] = FFT_EDMA_STATE_INIT;


#if 0
	/* get the size of the scratch buffer calculated in ARM */
	if ((p->u.sp_3d_r2c_e.para1.use_bs == 1) || (p->u.sp_3d_r2c_e.para2.use_bs == 1) || (p->u.sp_3d_r2c_e.para3.use_bs == 1))
	{ /* only allocate memory for scratch buffer when BlueStein is needed */
	  ptr_scratch = malloc(sizeof(float)*p->u.sp_3d_r2c_e.para1.worksize);
	}
#endif

    fft_omp_sp_plan_3d_r2c (N1, N2, N3, FFT_ECPY, plan_fxns, p, in , out, tw, ptr_scratch);

	if (ptr_scratch != NULL)
	  free(ptr_scratch);
}

/* offload plan for 3d sp c2r */
void FFT_plan_omp_3d_sp_c2r_facade (int N1, int N2, int N3, float *in, float *out, float *tw,
                                    int type, int mode, char *plan)
{
    fft_plan_t *p = (void*)plan;
    fft_callout_t plan_fxns;
	float *ptr_scratch = NULL;  // scratch buffer for Bn calculation, only used for Bluestein 

    /* initalize callout functions */
    plan_fxns.memoryRequest   = 0;
    plan_fxns.ecpyRequest     = fft_omp_assign_edma_resources;
    plan_fxns.memoryRelease   = 0;
    plan_fxns.ecpyRelease     = fft_omp_free_edma_resources;

    int i;
    for (i=0;i<ACTIVE_THREAD_COUNT;++i) fftEdmaState[i] = FFT_EDMA_STATE_INIT;


    /* If type == 0 => call forward plan as below */
    /* If type == 1 => call ifft_omp_sp_plan_2d_c2c instead */

#if 0
	/* get the size of the scratch buffer calculated in ARM */
	if ((p->u.sp_3d_c2r_e.para1.use_bs == 1) || (p->u.sp_3d_c2r_e.para2.use_bs == 1) || (p->u.sp_3d_c2r_e.para3.use_bs == 1))
	{ /* only allocate memory for scratch buffer when BlueStein is needed */
	  ptr_scratch = malloc(sizeof(float)*p->u.sp_3d_c2r_e.para1.worksize);
	}
#endif

    ifft_omp_sp_plan_3d_c2r (N1, N2, N3, FFT_ECPY, plan_fxns, p, in ,out, tw, ptr_scratch);

	if (ptr_scratch != NULL)
	  free(ptr_scratch); 
}

/* offload plan for 1d sp c2c batched */
void FFT_plan_1d_sp_c2c_batch_facade (int N, float *in, float *out, float *tw,
                                      int type, int mode, char *plan)
{
    int i;
    fft_plan_t *p = (void*)plan;
    fft_callout_t plan_fxns;

    /* initalize callout functions */
    plan_fxns.memoryRequest   = 0;
    plan_fxns.ecpyRequest     = fft_omp_assign_edma_resources;
    plan_fxns.memoryRelease   = 0;
    plan_fxns.ecpyRelease     = fft_omp_free_edma_resources;

    for (i=0;i<ACTIVE_THREAD_COUNT;++i) fftEdmaState[i] = FFT_EDMA_STATE_INIT;

    /* If type == 0 => call forward plan as below */
    /* If type == 1 => call ifft_omp_sp_plan_2d_c2c instead */
    if (type == 0)
      fft_sp_plan_1d_c2c_batch (N, FFT_ECPY, plan_fxns, p, in ,out, tw, 0);
    else
      ifft_sp_plan_1d_c2c_batch (N, FFT_ECPY, plan_fxns, p, in ,out, tw, 0);
}

/* offload plan for 1d sp c2c batched */
void FFT_plan_1d_sp_r2c_batch_facade (int N, float *in, float *out, float *tw,
                                      int type, int mode, char *plan)
{
    int i;
    fft_plan_t *p = (void*)plan;
    fft_callout_t plan_fxns;

    /* initalize callout functions */
    plan_fxns.memoryRequest   = 0;
    plan_fxns.ecpyRequest     = fft_omp_assign_edma_resources;
    plan_fxns.memoryRelease   = 0;
    plan_fxns.ecpyRelease     = fft_omp_free_edma_resources;

    for (i=0;i<ACTIVE_THREAD_COUNT;++i) fftEdmaState[i] = FFT_EDMA_STATE_INIT;

    /* this kernel support only forward plan and ignore the value of type */
    fft_sp_plan_1d_r2c_batch (N, FFT_ECPY, plan_fxns, p, in ,out, tw, 0);
}

/* offload plan for 1d sp c2c batched */
void FFT_plan_1d_sp_c2r_batch_facade (int N, float *in, float *out, float *tw,
                                      int type, int mode, char *plan)
{
    int i;
    fft_plan_t *p = (void*)plan;
    fft_callout_t plan_fxns;

    /* initalize callout functions */
    plan_fxns.memoryRequest   = 0;
    plan_fxns.ecpyRequest     = fft_omp_assign_edma_resources;
    plan_fxns.memoryRelease   = 0;
    plan_fxns.ecpyRelease     = fft_omp_free_edma_resources;

    for (i=0;i<ACTIVE_THREAD_COUNT;++i) fftEdmaState[i] = FFT_EDMA_STATE_INIT;

    /* this kernel support only backward plan and ignore the value of type */
    ifft_sp_plan_1d_c2r_batch (N, FFT_ECPY, plan_fxns, p, in ,out, tw, 0);
}

#endif /* TI_FFTW_ALLOW_ECPY */

void FFT_execute_facade(char *plan)
{
  fft_plan_t p;

  p = *(fft_plan_t*)plan;
  /* ADD KERNEL: Set as desired for your offloaded plan */
  p.actualCoreNum = ACTIVE_THREAD_COUNT;

  fft_execute(p);
}

void FFT_destroy_plan_facade(char *plan)
{
    fft_destroy_plan(*(fft_plan_t*)plan);
}

/* end of facade.c */
