/* ADD KERNEL: For each ecpy offload add a prototype and a kernel defintion*/

/* 1d FFT prototypes */
void FFT_plan_omp_1d_dp_c2c_facade (int N, global double *in, global double *out, 
				    global double *tw, int type, int mode, global char* plan);
void FFT_plan_omp_1d_dp_r2c_facade (int N, global double *in, global double *out, 
				    global double *tw, int type, int mode, global char* plan);
void FFT_plan_omp_1d_dp_c2r_facade (int N, global double *in, global double *out, 
				    global double *tw, int type, int mode, global char* plan);

kernel void ocl_FFT_plan_omp_1d_dp_c2c(
	int N, 
	global double *in, 
	global double *out, 
	global double *tw,
	int type, 
	int mode, 
	global char* plan)

{ 
    FFT_plan_omp_1d_dp_c2c_facade(N, in, out, tw, type, mode, plan); 
}

kernel void ocl_FFT_plan_omp_1d_dp_r2c(
	int N, 
	global double *in, 
	global double *out, 
	global double *tw,
	int type, 
	int mode, 
	global char* plan)

{ 
    FFT_plan_omp_1d_dp_r2c_facade(N, in, out, tw, type, mode, plan); 
}

kernel void ocl_FFT_plan_omp_1d_dp_c2r(
	int N, 
	global double *in, 
	global double *out, 
	global double *tw,
	int type, 
	int mode, 
	global char* plan)

{ 
    FFT_plan_omp_1d_dp_c2r_facade(N, in, out, tw, type, mode, plan); 
}

/* 1d batch FFT prototypes */
void FFT_plan_1d_dp_c2c_batch_facade (int N, global double *in, global double *out, 
				    global double *tw, int type, int mode, global char* plan);
void FFT_plan_1d_dp_r2c_batch_facade (int N, global double *in, global double *out, 
				    global double *tw, int type, int mode, global char* plan);
void FFT_plan_1d_dp_c2r_batch_facade (int N, global double *in, global double *out, 
				    global double *tw, int type, int mode, global char* plan);

kernel void ocl_FFT_plan_1d_dp_c2c_batch(
	int N, 
	global double *in, 
	global double *out, 
	global double *tw,
	int type, 
	int mode, 
	global char* plan)

{ 
    FFT_plan_1d_dp_c2c_batch_facade (N, in, out, tw, type, mode, plan); 
}

kernel void ocl_FFT_plan_1d_dp_c2r_batch(
	int N, 
	global double *in, 
	global double *out, 
	global double *tw,
	int type, 
	int mode, 
	global char* plan)

{ 
    FFT_plan_1d_dp_c2r_batch_facade (N, in, out, tw, type, mode, plan); 
}

kernel void ocl_FFT_plan_1d_dp_r2c_batch(
	int N, 
	global double *in, 
	global double *out, 
	global double *tw,
	int type, 
	int mode, 
	global char* plan)

{ 
    FFT_plan_1d_dp_r2c_batch_facade (N, in, out, tw, type, mode, plan); 
}

/* 2d FFT prototypes */
void FFT_plan_omp_2d_dp_c2c_facade (int N1, int N2, global double *in, global double *out, 
				    global double *tw, int type, int mode, global char* plan);
void FFT_plan_omp_2d_dp_r2c_facade (int N1, int N2, global double *in, global double *out, 
				    global double *tw, int type, int mode, global char* plan);
void FFT_plan_omp_2d_dp_c2r_facade (int N1, int N2, global double *in, global double *out, 
				    global double *tw, int type, int mode, global char* plan);

kernel void ocl_FFT_plan_omp_2d_dp_c2c(
	int N1, 
	int N2,
	global double *in, 
	global double *out, 
	global double *tw,
	int type, 
	int mode, 
	global char* plan)

{ 
    FFT_plan_omp_2d_dp_c2c_facade(N1, N2, in, out, tw, type, mode, plan); 
}

kernel void ocl_FFT_plan_omp_2d_dp_r2c(
	int N1, 
	int N2,
	global double *in, 
	global double *out, 
	global double *tw,
	int type, 
	int mode, 
	global char* plan)

{ 
    FFT_plan_omp_2d_dp_r2c_facade(N1, N2, in, out, tw, type, mode, plan); 
}
kernel void ocl_FFT_plan_omp_2d_dp_c2r(
	int N1, 
	int N2,
	global double *in, 
	global double *out, 
	global double *tw,
	int type, 
	int mode, 
	global char* plan)

{ 
    FFT_plan_omp_2d_dp_c2r_facade(N1, N2, in, out, tw, type, mode, plan); 
}

/* 3d FFT prototypes */
void FFT_plan_omp_3d_dp_c2c_facade (int N1, int N2, int N3, global double *in, global double *out, 
				    global double *tw, int type, int mode, global char* plan);
void FFT_plan_omp_3d_dp_r2c_facade (int N1, int N2, int N3, global double *in, global double *out, 
				    global double *tw, int type, int mode, global char* plan);
void FFT_plan_omp_3d_dp_c2r_facade (int N1, int N2, int N3, global double *in, global double *out, 
				    global double *tw, int type, int mode, global char* plan);

kernel void ocl_FFT_plan_omp_3d_dp_c2c(
	int N1, 
	int N2,
	int N3,
	global double *in, 
	global double *out, 
	global double *tw,
	int type, 
	int mode, 
	global char* plan)

{ 
    FFT_plan_omp_3d_dp_c2c_facade(N1, N2, N3, in, out, tw, type, mode, plan); 
}

kernel void ocl_FFT_plan_omp_3d_dp_r2c(
	int N1, 
	int N2,
	int N3,
	global double *in, 
	global double *out, 
	global double *tw,
	int type, 
	int mode, 
	global char* plan)

{ 
    FFT_plan_omp_3d_dp_r2c_facade(N1, N2, N3, in, out, tw, type, mode, plan); 
}

kernel void ocl_FFT_plan_omp_3d_dp_c2r(
	int N1, 
	int N2,
	int N3,
	global double *in, 
	global double *out, 
	global double *tw,
	int type, 
	int mode, 
	global char* plan)

{ 
    FFT_plan_omp_3d_dp_c2r_facade(N1, N2, N3, in, out, tw, type, mode, plan); 
}

/* 1d FFT prototypes */
void FFT_plan_omp_1d_sp_c2c_facade (int N, global float *in, global float *out, 
				    global float *tw, int type, int mode, global char* plan);
void FFT_plan_omp_1d_sp_r2c_facade (int N, global float *in, global float *out, 
				    global float *tw, int type, int mode, global char* plan);
void FFT_plan_omp_1d_sp_c2r_facade (int N, global float *in, global float *out, 
				    global float *tw, int type, int mode, global char* plan);

kernel void ocl_FFT_plan_omp_1d_sp_c2c(
	int N, 
	global float *in, 
	global float *out, 
	global float *tw,
	int type, 
	int mode, 
	global char* plan)

{ 
    FFT_plan_omp_1d_sp_c2c_facade(N, in, out, tw, type, mode, plan); 
}

kernel void ocl_FFT_plan_omp_1d_sp_r2c(
	int N, 
	global float *in, 
	global float *out, 
	global float *tw,
	int type, 
	int mode, 
	global char* plan)

{ 
    FFT_plan_omp_1d_sp_r2c_facade(N, in, out, tw, type, mode, plan); 
}

kernel void ocl_FFT_plan_omp_1d_sp_c2r(
	int N, 
	global float *in, 
	global float *out, 
	global float *tw,
	int type, 
	int mode, 
	global char* plan)

{ 
    FFT_plan_omp_1d_sp_c2r_facade(N, in, out, tw, type, mode, plan); 
}

/* 1d batch FFT prototypes */
void FFT_plan_1d_sp_c2c_batch_facade (int N, global float *in, global float *out, 
				    global float *tw, int type, int mode, global char* plan);
void FFT_plan_1d_sp_r2c_batch_facade (int N, global float *in, global float *out, 
				    global float *tw, int type, int mode, global char* plan);
void FFT_plan_1d_sp_c2r_batch_facade (int N, global float *in, global float *out, 
				    global float *tw, int type, int mode, global char* plan);

kernel void ocl_FFT_plan_1d_sp_c2c_batch(
	int N, 
	global float *in, 
	global float *out, 
	global float *tw,
	int type, 
	int mode, 
	global char* plan)

{ 
    FFT_plan_1d_sp_c2c_batch_facade (N, in, out, tw, type, mode, plan); 
}

kernel void ocl_FFT_plan_1d_sp_c2r_batch(
	int N, 
	global float *in, 
	global float *out, 
	global float *tw,
	int type, 
	int mode, 
	global char* plan)

{ 
    FFT_plan_1d_sp_c2r_batch_facade (N, in, out, tw, type, mode, plan); 
}

kernel void ocl_FFT_plan_1d_sp_r2c_batch(
	int N, 
	global float *in, 
	global float *out, 
	global float *tw,
	int type, 
	int mode, 
	global char* plan)

{ 
    FFT_plan_1d_sp_r2c_batch_facade (N, in, out, tw, type, mode, plan); 
}

/* 2d FFT prototypes */
void FFT_plan_omp_2d_sp_c2c_facade (int N1, int N2, global float *in, global float *out, 
				    global float *tw, int type, int mode, global char* plan);
void FFT_plan_omp_2d_sp_r2c_facade (int N1, int N2, global float *in, global float *out, 
				    global float *tw, int type, int mode, global char* plan);
void FFT_plan_omp_2d_sp_c2r_facade (int N1, int N2, global float *in, global float *out, 
				    global float *tw, int type, int mode, global char* plan);

kernel void ocl_FFT_plan_omp_2d_sp_c2c(
	int N1, 
	int N2,
	global float *in, 
	global float *out, 
	global float *tw,
	int type, 
	int mode, 
	global char* plan)

{ 
    FFT_plan_omp_2d_sp_c2c_facade(N1, N2, in, out, tw, type, mode, plan); 
}

kernel void ocl_FFT_plan_omp_2d_sp_r2c(
	int N1, 
	int N2,
	global float *in, 
	global float *out, 
	global float *tw,
	int type, 
	int mode, 
	global char* plan)

{ 
    FFT_plan_omp_2d_sp_r2c_facade(N1, N2, in, out, tw, type, mode, plan); 
}
kernel void ocl_FFT_plan_omp_2d_sp_c2r(
	int N1, 
	int N2,
	global float *in, 
	global float *out, 
	global float *tw,
	int type, 
	int mode, 
	global char* plan)

{ 
    FFT_plan_omp_2d_sp_c2r_facade(N1, N2, in, out, tw, type, mode, plan); 
}

/* 3d FFT prototypes */
void FFT_plan_omp_3d_sp_c2c_facade (int N1, int N2, int N3, global float *in, global float *out, 
				    global float *tw, int type, int mode, global char* plan);
void FFT_plan_omp_3d_sp_r2c_facade (int N1, int N2, int N3, global float *in, global float *out, 
				    global float *tw, int type, int mode, global char* plan);
void FFT_plan_omp_3d_sp_c2r_facade (int N1, int N2, int N3, global float *in, global float *out, 
				    global float *tw, int type, int mode, global char* plan);

kernel void ocl_FFT_plan_omp_3d_sp_c2c(
	int N1, 
	int N2,
	int N3,
	global float *in, 
	global float *out, 
	global float *tw,
	int type, 
	int mode, 
	global char* plan)

{ 
    FFT_plan_omp_3d_sp_c2c_facade(N1, N2, N3, in, out, tw, type, mode, plan); 
}

kernel void ocl_FFT_plan_omp_3d_sp_r2c(
	int N1, 
	int N2,
	int N3,
	global float *in, 
	global float *out, 
	global float *tw,
	int type, 
	int mode, 
	global char* plan)

{ 
    FFT_plan_omp_3d_sp_r2c_facade(N1, N2, N3, in, out, tw, type, mode, plan); 
}

kernel void ocl_FFT_plan_omp_3d_sp_c2r(
	int N1, 
	int N2,
	int N3,
	global float *in, 
	global float *out, 
	global float *tw,
	int type, 
	int mode, 
	global char* plan)

{ 
    FFT_plan_omp_3d_sp_c2r_facade(N1, N2, N3, in, out, tw, type, mode, plan); 
}
/* Don't touch below here */

void FFT_destroy_plan_facade (global char* plan);
void FFT_execute_facade (global char *plan);

kernel void ocl_FFT_execute(global char* plan, 
	global double *in, 
	global double *out, 
	global double *tw)
    { FFT_execute_facade(plan); }
kernel void ocl_FFT_destroy_plan(global char* plan)
    { FFT_destroy_plan_facade(plan); }
