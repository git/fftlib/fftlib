/* ======================================================================= */
/*  TEXAS INSTRUMENTS, INC.                                                */
/*                                                                         */
/*  FFTLIB  FFT Library                                                    */
/*                                                                         */
/* Copyright (C) 2014 Texas Instruments Incorporated - http://www.ti.com/  */
/*                                                                         */
/*                                                                         */
/*  Redistribution and use in source and binary forms, with or without     */
/*  modification, are permitted provided that the following conditions     */
/*  are met:                                                               */
/*                                                                         */
/*    Redistributions of source code must retain the above copyright       */
/*    notice, this list of conditions and the following disclaimer.        */
/*                                                                         */
/*    Redistributions in binary form must reproduce the above copyright    */
/*    notice, this list of conditions and the following disclaimer in the  */
/*    documentation and/or other materials provided with the               */
/*    distribution.                                                        */
/*                                                                         */
/*    Neither the name of Texas Instruments Incorporated nor the names of  */
/*    its contributors may be used to endorse or promote products derived  */
/*    from this software without specific prior written permission.        */
/*                                                                         */
/*  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS    */
/*  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT      */
/*  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR  */
/*  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT   */
/*  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,  */
/*  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT       */
/*  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,  */
/*  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY  */
/*  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT    */
/*  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE  */
/*  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.   */
/*                                                                         */
/* ======================================================================= */
#include "fftwf_acc.h"
#include "fftwf_acc_globals.h"

/* You should not need to edit the functions below when adding a new kernel */

/* 
  UNSAFE_OPT - Skip using events entirely. It looks like the spec allows this and everything completes
  in time. With a build with PROFILE on, using fft_static 1 3 1024 4096:

   Before: 
   	1024 test 3: 386us, .110GF
        4096 test 3: 484us, .435GF
   After:  
   	1024 test 3: 358us, .143GF (30%)
        4096 test 3: 465us, .529GF (22%)
   
   It works for me.
*/

//#define UNSAFE_OPT 1

void fftwf_execute(const fftwf_plan plan)
{
  TI_FFTWF_PROFILE_START();
  TI_FFTWF_DEBUG_PRINT("fftwf_execute(%p) intercepted\n", (void *)plan);

  element_t *elt = NULL; 
  _ti_fftwf_plan_info_t *plan_id = (_ti_fftwf_plan_info_t *) plan;
  cl_int err;
  cl_event *evt;
#ifndef UNSAFE_OPT
  cl_event e;
  evt = &e;
#else
  cl_event e = NULL;
  evt = NULL;
#endif
  fft_plan_t *p;
    p = (fft_plan_t *)plan_id->p;

  CDL_SEARCH_SCALAR(_ti_fftwf_tracked_plans,elt,item,plan); 
  if (elt == NULL) {
    TI_FFTWF_DEBUG_PRINT("  Calling FFTWF's fftwf_execute(%p) as no offload plan found\n", (void *)plan);
# ifdef TI_FFTWF_USE_DLSYM
    assert(_fptr_fftwf_execute);
    _fptr_fftwf_execute(plan);
# else
    __real_fftwf_execute(plan);
# endif
    TI_FFTWF_PROFILE_REPORT("  fftwf_execute (ARM) took %8.2f us\n",(float) clock_diff);
    return;
  }

  TI_FFTWF_DEBUG_PRINT("  Found plan for %p. Offloading fftwf_execute to DSP\n", (void *)plan);

  if ((plan_id->in != plan_id->out) && plan_id->copy_input) {
    TI_FFTWF_DEBUG_PRINT("This is an OUT-OF-PLACE transform. Copying IN into OUT, and then doing the execute\n");
    memcpy(plan_id->out, plan_id->in, plan_id->insize);
  }

  {
    TI_FFTWF_PROFILE_START();
    /* offload the execute to the DSP */
    /* Dispatch FFT_execute to device. See notes above, FFT_execute
        might contain OpenMP code and run parallel on cores. */
    err = clEnqueueTask(_ti_fftwf_ocl_Q, plan_id->FFT_execute, (cl_uint)0, NULL, evt);
    TI_FFTWF_OCL_CHKERROR("clEnqueueTask",err);
#ifndef UNSAFE_OPT
    err = clWaitForEvents(1, evt);
    TI_FFTWF_OCL_CHKERROR("clWaitForEvents",err);
    err  = clReleaseEvent(e);
    TI_FFTWF_OCL_CHKERROR("clReleaseEvent",err);
#endif
    TI_FFTWF_PROFILE_OCL_REPORT();
    TI_FFTWF_PROFILE_REPORT("  EnqueueTask of fftwf_execute (DSP) took %8.2f us\n",(float) clock_diff);
  }

  TI_FFTWF_DEBUG_PRINT("  Return from execute %p\n", (void *) plan);
  TI_FFTWF_PROFILE_REPORT("  Entire execute call took %8.2f us\n",(float) clock_diff);
  return;
}

/* end of fftwf_acc_execute.c */
