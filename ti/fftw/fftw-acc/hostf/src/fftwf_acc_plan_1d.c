/* ======================================================================= */
/*  TEXAS INSTRUMENTS, INC.                                                */
/*                                                                         */
/*  FFTLIB  FFT Library                                                    */
/*                                                                         */
/* Copyright (C) 2014 Texas Instruments Incorporated - http://www.ti.com/  */
/*                                                                         */
/*                                                                         */
/*  Redistribution and use in source and binary forms, with or without     */
/*  modification, are permitted provided that the following conditions     */
/*  are met:                                                               */
/*                                                                         */
/*    Redistributions of source code must retain the above copyright       */
/*    notice, this list of conditions and the following disclaimer.        */
/*                                                                         */
/*    Redistributions in binary form must reproduce the above copyright    */
/*    notice, this list of conditions and the following disclaimer in the  */
/*    documentation and/or other materials provided with the               */
/*    distribution.                                                        */
/*                                                                         */
/*    Neither the name of Texas Instruments Incorporated nor the names of  */
/*    its contributors may be used to endorse or promote products derived  */
/*    from this software without specific prior written permission.        */
/*                                                                         */
/*  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS    */
/*  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT      */
/*  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR  */
/*  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT   */
/*  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,  */
/*  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT       */
/*  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,  */
/*  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY  */
/*  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT    */
/*  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE  */
/*  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.   */
/*                                                                         */
/* ======================================================================= */
#include "fftwf_acc.h"
#include "fftwf_acc_globals.h"
#include "ti/fftlib/src/fft_omp_sp_1d_c2c/fft_omp_sp_1d_c2c.h"
#include "ti/fftlib/src/ifft_omp_sp_1d_c2c/ifft_omp_sp_1d_c2c.h"
#include "ti/fftlib/src/fft_omp_sp_1d_r2c/fft_omp_sp_1d_r2c.h"
#include "ti/fftlib/src/ifft_omp_sp_1d_c2r/ifft_omp_sp_1d_c2r.h"

/*************************************************************************/
/************** FFTW INTERCEPTED FUNCTIONS *******************************/
/*************************************************************************/

/*--------------------------------------------------------------------------------------------------------------------
                                rad and memory calculation for c2c
  --------------------------------------------------------------------------------------------------------------------*/

/* calculate radix for 1D array */
static void calculate_rad_c2c (int N, int *n1, int *n2, fft_para_mix_bs_t *fft_para1, 
                    fft_para_mix_bs_t *fft_para2, int *use_bs)
{
  int i, j;
  int s_r3, s_r5, s_r3_2, s_r5_2, dum, dum1;

  *use_bs = 1;

  s_r3 = 0;
  s_r5 = 0;

  dum = N;

  while (dum/3*3 == dum) {
    s_r3++;
    dum /= 3;
  }

  while (dum/5*5 == dum) {
    s_r5++;
    dum /= 5;
  }

  dum1 = dum >> 1;
  j    = 0;
  while (dum1 > 0) {
    dum1 = dum1 >> 1;
    j++;
  }
  j = 30 - fftwf_norm(dum);

  if ((dum >= 64) && (dum == (1 << j))) {
    *use_bs = 0;
    *n1 = 1 << (j-j/2);
    *n2 = 1 << (j/2);

    s_r5_2 = s_r5/2;
    s_r3_2 = s_r3/2;
    s_r3   = s_r3 - s_r3_2;
    s_r5   = s_r5 - s_r5_2;

    fft_para1->N_p2 = *n1;
    for (i = 0; i < s_r3; i++)
      *n1 *= 3;
    for (i = 0; i < s_r5; i++)
      *n1 *= 5;

    fft_para2->N_p2 = *n2;
    for (i = 0; i < s_r3_2; i++)
      *n2 *= 3;
    for (i = 0; i < s_r5_2; i++)
      *n2 *= 5;

    if(*n1 < *n2){
      dum = *n1;
      *n1 = *n2;
      *n2 = dum;
      dum = fft_para1->N_p2;
      fft_para1->N_p2 = fft_para2->N_p2;
      fft_para2->N_p2 = dum;
    }

    fft_para1->s_r3 = s_r3;
    fft_para1->s_r5 = s_r5;

    fft_para2->s_r3 = s_r3_2;
    fft_para2->s_r5 = s_r5_2;
  } else {
    //not supported, will trigger a plan failure at this point
    return;
  }
} /*calculate_rad ()*/

/* calculate twiddle size, Bn buffer size and workbuffer size */
static void calculate_mem_size_c2c(int N, int s_r3, int s_r5, int N_p2, int *twsize)
{
  int dum, n, i;

  /* Please note that Bn buffer and Workbuf are only used when doing Bluestein
   * meaning when use_bs == 1 */
    if ((s_r3 == 0) && (s_r5 == 0)) {
    /* When signal is power of 2 */
      *twsize   = 2;
    }
    else {
    /* When signal is power of 3 or 5 */
    dum = 0;
    n  = 4*N;
    for (i = 0; i < s_r3; i++) {
      dum += n/3;
      n  /= 3;
    }
    n *= 2;  /* radix-5 has 8 twiddle vs radix-3 4 twiddles */
    for (i = 0; i < s_r5; i++) {
      dum += n/5;
      n  /= 5;
    }
    *twsize = (2+dum);
  }
} /* calculate_mem_size()*/

/*--------------------------------------------------------------------------------------------------------------------
                                rad and memory calculation for r2c
  --------------------------------------------------------------------------------------------------------------------*/
/* calculate radix for 1D array */
static void calculate_rad_r2c (int N, int *n1, int *n2, fft_para_mix_bs_t *fft_para1,
                    fft_para_mix_bs_t *fft_para2, int *use_bs)
{
  int i, j;
  int s_r3, s_r5, s_r3_2, s_r5_2, dum;

  *use_bs = 1;

  s_r3 = 0;
  s_r5 = 0;

  dum = N;

  while (dum/3*3 == dum) {
    s_r3++;
    dum /= 3;
  }

  while (dum/5*5 == dum) {
    s_r5++;
    dum /= 5;
  }

  j = 30 - fftwf_norm(dum);
  if ((dum >= 64) && (dum == (1 << j))) {
    *use_bs = 0;
    if((j > 10)){
      *n1 = 1 << (j-j/2);
      *n2 = 1 << (j/2);
    }
    else {
      if(j > 6){
        *n2 = 1 << (j-4);
        *n1 = 16;
      }
      else {
        *n1 = 16;
        *n2 = 1 << (j-4);
      }
    }
    s_r5_2 = s_r5/2;
    s_r3_2 = s_r3/2;
    s_r3   = s_r3 - s_r3_2;
    s_r5   = s_r5 - s_r5_2;
    fft_para1->N_p2 = *n1/2;
    for (i = 0; i < s_r3; i++)
      *n1 *= 3;
    for (i = 0; i < s_r5; i++)
      *n1 *= 5;

    fft_para2->N_p2 = *n2;
    for (i = 0; i < s_r3_2; i++)
      *n2 *= 3;
    for (i = 0; i < s_r5_2; i++)
      *n2 *= 5;

    if(*n1 < *n2){
      dum = *n1;
      *n1 = *n2;
      *n2 = dum;
      dum = fft_para1->N_p2;
      fft_para1->N_p2 = fft_para2->N_p2/2;
      fft_para2->N_p2 = dum*2;

      dum    = s_r3;
      s_r3   = s_r3_2;
      s_r3_2 = dum;
      dum    = s_r5;
      s_r5   = s_r5_2;
      s_r5_2 = dum;

      if(fft_para1->N_p2 <= 4){
        fft_para1->N_p2 *= 2;
        fft_para2->N_p2 /=2;
        *n1 = *n1*2;
        *n2 = *n2/2;
      }
    }
    fft_para1->s_r3 = s_r3;
    fft_para1->s_r5 = s_r5;

    fft_para2->s_r3 = s_r3_2;
    fft_para2->s_r5 = s_r5_2;
  } else {
    //not supported, will trigger a plan failure at this point
    return;
  }
}

/* calculate twiddle size, Bn buffer size and workbuffer size */
static void calculate_mem_size_r2c(int dim, int N, int s_r3, int s_r5, int N_p2, int *twsize)
{
  int dum, n, i;

  /* Please note that Bn buffer and Workbuf are only used when doing Bluestein
   * meaning when use_bs == 1 */
    if ((s_r3 == 0) && (s_r5 == 0)) {
    /* When signal is power of 2 */
      if ( dim == 1)
        *twsize   = 2 + N/2;
      else
        *twsize   = 2;
    }
    else {
    /* When signal is power of 3 or 5 */
    dum = 0;
    if ( dim == 1)
      n = 2*N;
    else
      n = 4*N;
    for (i = 0; i < s_r3; i++) {
      dum += n/3;
      n  /= 3;
    }
    n *= 2;  /* radix-5 has 8 twiddle vs radix-3 4 twiddles */
    for (i = 0; i < s_r5; i++) {
      dum += n/5;
      n  /= 5;
    }
    if ( dim == 1)
      *twsize   = (2+N/2+dum);
    else
      *twsize   = (2+dum);
  }
} /* calculate_mem_size()*/


/*--------------------------------------------------------------------------------------------------------------------
                                rad and memory calculation for c2r
  --------------------------------------------------------------------------------------------------------------------*/
/* calculate radix for 1D array */
static void calculate_rad_c2r (int N, int *n1, int *n2, fft_para_mix_bs_t *fft_para1,
                    fft_para_mix_bs_t *fft_para2, int *use_bs)
{
  int i, j;
  int s_r3, s_r5, s_r3_2, s_r5_2, dum;

  *use_bs = 1;

  s_r3 = 0;
  s_r5 = 0;

  dum = N;

  while (dum/3*3 == dum) {
    s_r3++;
    dum /= 3;
  }

  while (dum/5*5 == dum) {
    s_r5++;
    dum /= 5;
  }

  j = 30 - fftwf_norm(dum);
  if ((dum >= 128) && (dum == (1 << j))) {
    *use_bs = 0;
    if((j > 10)){
      *n1 = 1 << (j-j/2);
      *n2 = 1 << (j/2);
    }
    else {
      if(j > 6){
        *n1 = 1 << (j-4);
        *n2 = 16;
      }
    }

    s_r5_2 = s_r5/2;
    s_r3_2 = s_r3/2;
    s_r3   = s_r3 - s_r3_2;
    s_r5   = s_r5 - s_r5_2;
    fft_para1->N_p2 = *n1;
    for (i = 0; i < s_r3; i++)
      *n1 *= 3;
    for (i = 0; i < s_r5; i++)
      *n1 *= 5;

    fft_para2->N_p2 = *n2/2;
    for (i = 0; i < s_r3_2; i++)
      *n2 *= 3;
    for (i = 0; i < s_r5_2; i++)
      *n2 *= 5;

    if(*n1 < *n2){
      /* exchange parameters for the 2 dimensions */
      dum = *n1;
      *n1 = *n2;
      *n2 = dum;
      dum = fft_para1->N_p2;
      fft_para1->N_p2 = fft_para2->N_p2*2;
      fft_para2->N_p2 = dum/2;
      dum  = s_r3;
      s_r3 = s_r3_2;
      s_r3_2 = dum;

      dum  = s_r5;
      s_r5 = s_r5_2;
      s_r5_2 = dum;

      /* every N_p2 has to be greater than 4 */
      if(fft_para2->N_p2 <= 4){
        *n2 *= 2;
        *n1 /= 2;
        fft_para2->N_p2 *= 2;
        fft_para1->N_p2 /= 2;
        if (*n1 < *n2) {
          /* make sure n1 > n2 */
          if (s_r3_2 > 0){
            s_r3_2 -= 1;
            s_r3   += 1;
            *n1    *= 3;
            *n2    /= 3;
          } else if (s_r5_2 > 0){
              s_r5_2 -= 1;
              s_r5   += 1;
              *n1    *= 5;
              *n2    /= 5;
          }
        }
      }
    }
    fft_para1->s_r3 = s_r3;
    fft_para1->s_r5 = s_r5;

    fft_para2->s_r3 = s_r3_2;
    fft_para2->s_r5 = s_r5_2;
  } else {
    //not supported, will trigger a plan failure at this point
    return;
  }
} /*calculate_rad ()*/

/* calculate twiddle size, Bn buffer size and workbuffer size */
static void calculate_mem_size_c2r(int dim, int N, int s_r3, int s_r5, int N_p2, int *twsize)
{
  int dum, n, i;

  /* Please note that Bn buffer and Workbuf are only used when doing Bluestein
   * meaning when use_bs == 1 */
    if ((s_r3 == 0) && (s_r5 == 0)) {
    /* When signal is power of 2 */
      if ( dim == 1)
        *twsize   = 2;
      else
        *twsize   = N/2;
    }
    else {
    /* When signal is power of 3 or 5 */
    dum = 0;
    if ( dim == 1)
      n = 4*N;
    else
      n = 2*N;
    for (i = 0; i < s_r3; i++) {
      dum += n/3;
      n  /= 3;
    }
    n *= 2;  /* radix-5 has 8 twiddle vs radix-3 4 twiddles */
    for (i = 0; i < s_r5; i++) {
      dum += n/5;
      n  /= 5;
    }
    if ( dim == 1)
      *twsize   = (2+dum);
    else
      *twsize   = (N/2+dum);
  }
} /* calculate_mem_size()*/


/* ADD KERNEL: Add a function for each new FFT kernel you want to offload */

/* plan for both fft and ifft 1d sp c2c */
fftwf_plan fftwf_plan_dft_1d(int n, fftwf_complex *in, fftwf_complex *out, int sign, unsigned flags)
{
  const int PLAN_TYPE = TI_FFTWF_FFT_1D_SP_C2C;
  fft_plan_t *p;
  int    n1, n2;
  int    int_tw_size, ext_tw_size, twsize, localsize;
  int    use_bs = 0;
  int    s_r3, s_r5, N_p2;

  if (!_ti_fftwf_init_done) _ti_fftwf_acc_init();
  TI_FFTWF_DEBUG_PRINT("%s(%d,0x%p,0x%p,%d,%d,%d) intercepted\n", _ti_fftwf_ocl_plans[PLAN_TYPE].fftwf_plan_name, n, (void *)in, (void *)out, sign, flags, PLAN_TYPE);

  int offload_type = _ti_fftwf_should_offload_1d_sp(PLAN_TYPE, n, sign, flags);
  if (offload_type) {
    p = (fft_plan_t *)_lib_extern_malloc(FFT_PLAN_T_SIZE);

    /* determine the rad for the first  & second dimensions */
    calculate_rad_c2c(n, &n1, &n2, &p->u.sp_1d_c2c_e.para1, &p->u.sp_1d_c2c_e.para2, &use_bs);
    p->u.sp_1d_c2c_e.n1 = n1;
    p->u.sp_1d_c2c_e.n2 = n2;

    /* not supported, plan failed */
    if (use_bs == 1) {
      TI_FFTWF_DEBUG_PRINT("Plan failed, FFT size not supported\n");
      __free_ddr(p);
      return NULL;
    }
    /* Calculate mem size for 2 dimensions for ECPY */
  
    /* evaluate the  1st dimension */
    s_r3 = p->u.sp_1d_c2c_e.para1.s_r3;
    s_r5 = p->u.sp_1d_c2c_e.para1.s_r5;
    N_p2 = p->u.sp_1d_c2c_e.para1.N_p2;
    calculate_mem_size_c2c(n1, s_r3, s_r5, N_p2, &twsize);
    p->u.sp_1d_c2c_e.para1.twsize = twsize;
    int_tw_size = ext_tw_size = twsize + 2*n2;
  
    /* evaluate the 2nd dimension */
    s_r3 = p->u.sp_1d_c2c_e.para2.s_r3;
    s_r5 = p->u.sp_1d_c2c_e.para2.s_r5;
    N_p2 = p->u.sp_1d_c2c_e.para2.N_p2;
    calculate_mem_size_c2c(n2, s_r3, s_r5, N_p2, &twsize);
    p->u.sp_1d_c2c_e.para2.twsize = twsize;
    int_tw_size  = (int_tw_size > twsize) ? int_tw_size : twsize;
    ext_tw_size += twsize;

    /* calculate local memory requirements */
    localsize = FFT_EDMA_CH_BUFSIZE+sizeof(float)*10*n1*FFT_OMP_SP_1D_C2C_NUMOFLINEBUFS + sizeof(float)*int_tw_size;
    
    return (_ti_fftwf_plan_offload_1d(PLAN_TYPE, offload_type, n, sizeof(fftwf_complex)*n, sizeof(fftwf_complex)*n, sizeof(float)*ext_tw_size, localsize, in, out, sign, flags, (void *)p));
  }
  else
    {
      TI_FFTWF_DEBUG_PRINT("  Using FFTWF3 for %s\n", _ti_fftwf_ocl_plans[PLAN_TYPE].fftwf_plan_name);
      fftwf_plan rc;
      TI_FFTWF_PROFILE_START();
#ifdef TI_FFTWF_USE_DLSYM
      assert(_fptr_fftwf_plan_dft_1d);
      rc = _fptr_fftwf_plan_dft_1d(n, in, out, sign, flags);
#else
      rc = __real_fftwf_plan_dft_1d(n, in, out, sign, flags);
#endif
      TI_FFTWF_PROFILE_REPORT("  fftwf_plan_dft_1d (ARM) took %8.2f us\n",(float) clock_diff);
      return rc;
    } 
}

/* plan for 1d sp r2c */
fftwf_plan fftwf_plan_dft_r2c_1d (int n, float *in, fftwf_complex *out, unsigned flags)
{
  const int PLAN_TYPE = TI_FFTWF_FFT_1D_SP_R2C;
  fft_plan_t *p;
  int    n1, n2;
  int    int_tw_size, ext_tw_size, twsize, localsize;
  int    use_bs = 0;
  int    sign = FFTW_FORWARD; /* only FFT in this case */
  int    s_r3, s_r5, N_p2;

  if (!_ti_fftwf_init_done) _ti_fftwf_acc_init();
  TI_FFTWF_DEBUG_PRINT("%s(%d,0x%p,0x%p,%d,%d,%d) intercepted\n", _ti_fftwf_ocl_plans[PLAN_TYPE].fftwf_plan_name, n, (void *)in, (void *)out, sign, flags, PLAN_TYPE);

  int offload_type = _ti_fftwf_should_offload_1d_sp(PLAN_TYPE, n, sign, flags);
  
  if (offload_type) {
    p = (fft_plan_t *)_lib_extern_malloc(FFT_PLAN_T_SIZE);

    /* determine the rad for the first  & second dimensions */
    calculate_rad_r2c(n, &n1, &n2, &p->u.sp_1d_r2c_e.para1, &p->u.sp_1d_r2c_e.para2, &use_bs);
    p->u.sp_1d_r2c_e.n1 = n1;
    p->u.sp_1d_r2c_e.n2 = n2;

    /* not supported, plan failed */
    if (use_bs == 1) {
      TI_FFTWF_DEBUG_PRINT("Plan failed, FFT size not supported\n");
      __free_ddr(p);
      return NULL;
    }

    /* Calculate mem size for 2 dimensions for ECPY */
  
    /* evaluate the  1st dimension */
    s_r3 = p->u.sp_1d_r2c_e.para1.s_r3;
    s_r5 = p->u.sp_1d_r2c_e.para1.s_r5;
    N_p2 = p->u.sp_1d_r2c_e.para1.N_p2;
    calculate_mem_size_r2c(1, n1, s_r3, s_r5, N_p2, &twsize);
    p->u.sp_1d_r2c_e.para1.twsize = twsize;
    int_tw_size = ext_tw_size = twsize + 2*n2;
  
    /* evaluate the 2nd dimension */
    s_r3 = p->u.sp_1d_r2c_e.para2.s_r3;
    s_r5 = p->u.sp_1d_r2c_e.para2.s_r5;
    N_p2 = p->u.sp_1d_r2c_e.para2.N_p2;
    calculate_mem_size_r2c(2, n2, s_r3, s_r5, N_p2, &twsize);
    p->u.sp_1d_r2c_e.para2.twsize = twsize;
    int_tw_size  = (int_tw_size > twsize) ? int_tw_size : twsize;
    ext_tw_size += twsize;
    
    /* calculate local memory requirements */
    localsize = FFT_EDMA_CH_BUFSIZE+sizeof(float)*10*n1*FFT_OMP_SP_1D_R2C_NUMOFLINEBUFS + sizeof(float)*int_tw_size;   
    
    return (_ti_fftwf_plan_offload_1d(PLAN_TYPE, offload_type, n, sizeof(float)*n, sizeof(fftwf_complex)*(n/2+1), sizeof(float)*ext_tw_size+sizeof(fftwf_complex)*n, localsize, in, out, sign, flags, (void *)p));
  }
  else
    {
      TI_FFTWF_DEBUG_PRINT("  Using FFTWF3 for %s\n", _ti_fftwf_ocl_plans[PLAN_TYPE].fftwf_plan_name);
      fftwf_plan rc;
      TI_FFTWF_PROFILE_START();
#ifdef TI_FFTWF_USE_DLSYM
      assert(_fptr_fftwf_plan_dft_r2c_1d);
      rc = _fptr_fftwf_plan_dft_r2c_1d (n, in, out, flags);
#else
      rc = __real_fftwf_plan_dft_r2c_1d(n, in, out, flags);
#endif
      TI_FFTWF_PROFILE_REPORT("  fftwf_plan_dft_r2c_1d (ARM) took %8.2f us\n",(float) clock_diff);
      return rc;
    } 
}

/* plan for 1d sp c2r */

fftwf_plan fftwf_plan_dft_c2r_1d (int n, fftwf_complex *in, float *out, unsigned flags)
{
  const int PLAN_TYPE = TI_FFTWF_FFT_1D_SP_C2R;
  fft_plan_t *p;
  int    n1, n2;
  int    int_tw_size, ext_tw_size, twsize, localsize;
  int    use_bs = 0;
  int    sign = FFTW_BACKWARD;  /* only IFFT in this case */
  int    s_r3, s_r5, N_p2;

  if (!_ti_fftwf_init_done) _ti_fftwf_acc_init();
  TI_FFTWF_DEBUG_PRINT("%s(%d,0x%p,0x%p,%d,%d,%d) intercepted\n", _ti_fftwf_ocl_plans[PLAN_TYPE].fftwf_plan_name, n, (void *)in, (void *)out, sign, flags, PLAN_TYPE);

  int offload_type = _ti_fftwf_should_offload_1d_sp(PLAN_TYPE, n, sign, flags);
  
  if (offload_type) {
    p = (fft_plan_t *)_lib_extern_malloc(FFT_PLAN_T_SIZE);

    /* determine the rad for the first  & second dimensions */
    calculate_rad_c2r(n, &n1, &n2, &p->u.sp_1d_c2r_e.para1, &p->u.sp_1d_c2r_e.para2, &use_bs);
    p->u.sp_1d_c2r_e.n1 = n1;
    p->u.sp_1d_c2r_e.n2 = n2;

    /* not supported, plan failed */
    if (use_bs == 1) {
      TI_FFTWF_DEBUG_PRINT("Plan failed, FFT size not supported\n");
      __free_ddr(p);
      return NULL;
    }

    /* Calculate mem size for 2 dimensions for ECPY */
    /* evaluate the  1st dimension */
    s_r3 = p->u.sp_1d_c2r_e.para1.s_r3;
    s_r5 = p->u.sp_1d_c2r_e.para1.s_r5;
    N_p2 = p->u.sp_1d_c2r_e.para1.N_p2;
    calculate_mem_size_c2r(1, n1, s_r3, s_r5, N_p2, &twsize);
    p->u.sp_1d_c2r_e.para1.twsize = twsize;
    int_tw_size = ext_tw_size = twsize + 2*n2;
  
    /* evaluate the 2nd dimension */
    s_r3 = p->u.sp_1d_c2r_e.para2.s_r3;
    s_r5 = p->u.sp_1d_c2r_e.para2.s_r5;
    N_p2 = p->u.sp_1d_c2r_e.para2.N_p2;
    calculate_mem_size_c2r(2, n2, s_r3, s_r5, N_p2, &twsize);
    p->u.sp_1d_c2r_e.para2.twsize = twsize;
    int_tw_size  = (int_tw_size > twsize) ? int_tw_size : twsize;
    ext_tw_size += twsize;
    
    /* calculate local memory requirements */
    localsize = FFT_EDMA_CH_BUFSIZE+sizeof(float)*10*n1*IFFT_OMP_SP_1D_C2R_NUMOFLINEBUFS + sizeof(float)*int_tw_size;
      
    return (_ti_fftwf_plan_offload_1d(PLAN_TYPE, offload_type, n, sizeof(fftwf_complex)*(n/2+1), sizeof(float)*n, sizeof(float)*ext_tw_size+sizeof(fftwf_complex)*n, localsize, in, out, sign, flags, (void *)p));
  }
  else
    {
      TI_FFTWF_DEBUG_PRINT("  Using FFTWF3 for %s\n", _ti_fftwf_ocl_plans[PLAN_TYPE].fftwf_plan_name);
      fftwf_plan rc;
      TI_FFTWF_PROFILE_START();
#ifdef TI_FFTWF_USE_DLSYM
      assert(_fptr_fftwf_plan_dft_c2r_1d);
      rc = _fptr_fftwf_plan_dft_c2r_1d (n, in, out, flags);
#else
      rc = __real_fftwf_plan_dft_c2r_1d (n, in, out, flags);
#endif
      TI_FFTWF_PROFILE_REPORT("  fftwf_plan_dft_c2r_1d (ARM) took %8.2f us\n",(float) clock_diff);
      return rc;
    } 
}

/* end of fftwf_acc_plan_1d.c */



