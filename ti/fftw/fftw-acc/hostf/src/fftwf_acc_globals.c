/* ======================================================================= */
/*	TEXAS	INSTRUMENTS, INC.																								 */
/*																																				 */
/*	FFTLIB	FFT	Library																										 */
/*																																				 */
/* Copyright (C) 2014	Texas	Instruments	Incorporated - http://www.ti.com/	 */
/*																																				 */
/*																																				 */
/*	Redistribution and use in	source and binary	forms, with	or without		 */
/*	modification,	are	permitted	provided that	the	following	conditions		 */
/*	are	met:																															 */
/*																																				 */
/*		Redistributions	of source	code must	retain the above copyright			 */
/*		notice,	this list	of conditions	and	the	following	disclaimer.				 */
/*																																				 */
/*		Redistributions	in binary	form must	reproduce	the	above	copyright		 */
/*		notice,	this list	of conditions	and	the	following	disclaimer in	the	 */
/*		documentation	and/or other materials provided	with the							 */
/*		distribution.																												 */
/*																																				 */
/*		Neither	the	name of	Texas	Instruments	Incorporated nor the names of	 */
/*		its	contributors may be	used to	endorse	or promote products	derived	 */
/*		from this	software without specific	prior	written	permission.				 */
/*																																				 */
/*	THIS SOFTWARE	IS PROVIDED	BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS		 */
/*	"AS	IS"	AND	ANY	EXPRESS	OR IMPLIED WARRANTIES, INCLUDING,	BUT	NOT			 */
/*	LIMITED	TO,	THE	IMPLIED	WARRANTIES OF	MERCHANTABILITY	AND	FITNESS	FOR	 */
/*	A	PARTICULAR PURPOSE ARE DISCLAIMED. IN	NO EVENT SHALL THE COPYRIGHT	 */
/*	OWNER	OR CONTRIBUTORS	BE LIABLE	FOR	ANY	DIRECT,	INDIRECT,	INCIDENTAL,	 */
/*	SPECIAL, EXEMPLARY,	OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT			 */
/*	LIMITED	TO,	PROCUREMENT	OF SUBSTITUTE	GOODS	OR SERVICES; LOSS	OF USE,	 */
/*	DATA,	OR PROFITS;	OR BUSINESS	INTERRUPTION)	HOWEVER	CAUSED AND ON	ANY	 */
/*	THEORY OF	LIABILITY, WHETHER IN	CONTRACT,	STRICT LIABILITY,	OR TORT		 */
/*	(INCLUDING NEGLIGENCE	OR OTHERWISE)	ARISING	IN ANY WAY OUT OF	THE	USE	 */
/*	OF THIS	SOFTWARE,	EVEN IF	ADVISED	OF THE POSSIBILITY OF	SUCH DAMAGE.	 */
/*																																				 */
/* ======================================================================= */
#include "fftwf_acc.h"
#include "fftwf_acc_globals.h"

/* Everything global below is also in:

   fftwf_acc_globals.h
   fftwf_acc_initfini.c

   Makes sure you change 'em all. */

/* global flags */

volatile int _ti_fftwf_init_done = 0; 
volatile int TI_FFTWF_OFFLOAD = -1;
#ifdef TI_FFTWF_DEBUG
volatile int _ti_fftwf_DISABLE_DEBUG;
#endif
volatile int _ti_fftwf_DISABLE_CMEM;

cl_context       _ti_fftwf_ocl_context;
cl_command_queue _ti_fftwf_ocl_Q;
cl_program       _ti_fftwf_ocl_program;
cl_kernel        _ti_fftwf_ocl_FFT_execute;
cl_kernel        _ti_fftwf_ocl_FFT_destroy_plan;
_ti_fftwf_ocl_plan_kernel_t _ti_fftwf_ocl_plans[TI_FFTWF_FFT_PLAN_MAX];
element_t *_ti_fftwf_tracked_plans;

/* ADD KERNEL: Add an entry in each of the arrays for each FFTWF offload */
const char* _ti_fftwf_PLAN_FUNCS[TI_FFTWF_FFT_PLAN_MAX] = {"fftwf_plan_dft_1d",
                                                           "fftwf_plan_dft_r2c_1d",
                                                           "fftwf_plan_dft_c2r_1d",
                                                           "fftwf_plan_dft_2d",
                                                           "fftwf_plan_dft_r2c_2d",
                                                           "fftwf_plan_dft_c2r_2d",
                                                           "fftwf_plan_dft_3d",
                                                           "fftwf_plan_dft_r2c_3d",
                                                           "fftwf_plan_dft_c2r_3d",
                                                           "fftwf_plan_many_dft",
                                                           "fftwf_plan_many_dft_r2c",
                                                           "fftwf_plan_many_dft_c2r"};
const char* _ti_fftwf_OCL_OMP_KERNELS[TI_FFTWF_FFT_PLAN_MAX] = {"ocl_FFT_plan_omp_1d_sp_c2c",
                                                                "ocl_FFT_plan_omp_1d_sp_r2c",
                                                                "ocl_FFT_plan_omp_1d_sp_c2r",
                                                                "ocl_FFT_plan_omp_2d_sp_c2c",
                                                                "ocl_FFT_plan_omp_2d_sp_r2c",
                                                                "ocl_FFT_plan_omp_2d_sp_c2r",
                                                                "ocl_FFT_plan_omp_3d_sp_c2c",
                                                                "ocl_FFT_plan_omp_3d_sp_r2c",
                                                                "ocl_FFT_plan_omp_3d_sp_c2r",
                                                                "ocl_FFT_plan_1d_sp_c2c_batch",
                                                                "ocl_FFT_plan_1d_sp_r2c_batch",
                                                                "ocl_FFT_plan_1d_sp_c2r_batch"};

#ifdef TI_FFTWF_USE_DLSYM
DLSYM_DEFN(fftwf_execute)
DLSYM_DEFN(fftwf_destroy_plan)
DLSYM_DEFN(fftwf_malloc)
DLSYM_DEFN(fftwf_free)
/* ADD KERNEL: Add a line like below, for each FFTWF plan fn your intercept */
DLSYM_DEFN(fftwf_plan_dft_1d)
DLSYM_DEFN(fftwf_plan_dft_r2c_1d)
DLSYM_DEFN(fftwf_plan_dft_c2r_1d)
DLSYM_DEFN(fftwf_plan_dft_2d)
DLSYM_DEFN(fftwf_plan_dft_r2c_2d)
DLSYM_DEFN(fftwf_plan_dft_c2r_2d)
DLSYM_DEFN(fftwf_plan_dft_3d)
DLSYM_DEFN(fftwf_plan_dft_r2c_3d)
DLSYM_DEFN(fftwf_plan_dft_c2r_3d)
DLSYM_DEFN(fftwf_plan_many_dft)
DLSYM_DEFN(fftwf_plan_many_dft_c2r)
DLSYM_DEFN(fftwf_plan_many_dft_r2c)
#endif /* TI_FFTWF_USE_DLSYM */

/* end of	fftwf_acc_globals.c */
