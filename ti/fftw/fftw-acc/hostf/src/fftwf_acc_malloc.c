/* ======================================================================= */
/*  TEXAS INSTRUMENTS, INC.                                                */
/*                                                                         */
/*  FFTLIB  FFT Library                                                    */
/*                                                                         */
/* Copyright (C) 2014 Texas Instruments Incorporated - http://www.ti.com/  */
/*                                                                         */
/*                                                                         */
/*  Redistribution and use in source and binary forms, with or without     */
/*  modification, are permitted provided that the following conditions     */
/*  are met:                                                               */
/*                                                                         */
/*    Redistributions of source code must retain the above copyright       */
/*    notice, this list of conditions and the following disclaimer.        */
/*                                                                         */
/*    Redistributions in binary form must reproduce the above copyright    */
/*    notice, this list of conditions and the following disclaimer in the  */
/*    documentation and/or other materials provided with the               */
/*    distribution.                                                        */
/*                                                                         */
/*    Neither the name of Texas Instruments Incorporated nor the names of  */
/*    its contributors may be used to endorse or promote products derived  */
/*    from this software without specific prior written permission.        */
/*                                                                         */
/*  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS    */
/*  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT      */
/*  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR  */
/*  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT   */
/*  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,  */
/*  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT       */
/*  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,  */
/*  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY  */
/*  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT    */
/*  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE  */
/*  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.   */
/*                                                                         */
/* ======================================================================= */
#include "fftwf_acc.h"
#include "fftwf_acc_globals.h"

#define MALLOC_NAME "__malloc_ddr"
#define FREE_NAME "__free_ddr"

/* Returns 1 if we should use CMEM, otherwise 0 */
static inline int _ti_fftwf_should_use_cmem(size_t n)
{
  if (_ti_fftwf_DISABLE_CMEM)
    return 0;
  /* add logic to decide whether to use CMEM or not */
  return 1;
}

void* fftwf_malloc(size_t n)
{
  if (!_ti_fftwf_init_done) 
    _ti_fftwf_acc_init();

  TI_FFTWF_DEBUG_PRINT("fftwf_malloc(%d) intercepted\n", n);
  void * p = NULL;
  if (!_ti_fftwf_should_use_cmem(n) || !(n < TI_FFTWF_MAX_CMEM_MALLOC)) {
    TI_FFTWF_DEBUG_PRINT("  Using FFTWF's malloc for %d bytes\n", n);
#ifdef TI_FFTWF_USE_DLSYM
    p = _fptr_fftwf_malloc(n);
#else
    p = __real_fftwf_malloc(n);
#endif
  }
  else {
    TI_FFTWF_DEBUG_PRINT("  Using %s for %d bytes\n", MALLOC_NAME, n);
    p = __malloc_ddr(n);
  }
  TI_FFTWF_DEBUG_PRINT("  malloc returned: %p\n", p);
  return p;
}

void fftwf_free(void *p)
{
  if (!_ti_fftwf_init_done)
    _ti_fftwf_acc_init();

  TI_FFTWF_DEBUG_PRINT("Request to free: %p\n", p);

  /* We need to add a check to see if we are doing a free of our allocation*/
  /* We need to actually de-allocate the OpenCL buffer */

  if (__is_in_malloced_region(p))
{
  TI_FFTWF_DEBUG_PRINT("  Found %p in cmem. Freeing using %s\n", p, FREE_NAME);
    /* add call to release cmem */
    __free_ddr(p);
  }
  else {
    TI_FFTWF_DEBUG_PRINT("  Calling FFTWF free on %p\n", p);
#   ifdef TI_FFTWF_USE_DLSYM
      _fptr_fftwf_free(p);
#   else
      __real_fftwf_free(p);
#   endif
  }
  return;
}
