/* ======================================================================= */
/*  TEXAS INSTRUMENTS, INC.                                                */
/*                                                                         */
/*  FFTLIB  FFT Library                                                    */
/*                                                                         */
/* Copyright (C) 2014 Texas Instruments Incorporated - http://www.ti.com/  */
/*                                                                         */
/*                                                                         */
/*  Redistribution and use in source and binary forms, with or without     */
/*  modification, are permitted provided that the following conditions     */
/*  are met:                                                               */
/*                                                                         */
/*    Redistributions of source code must retain the above copyright       */
/*    notice, this list of conditions and the following disclaimer.        */
/*                                                                         */
/*    Redistributions in binary form must reproduce the above copyright    */
/*    notice, this list of conditions and the following disclaimer in the  */
/*    documentation and/or other materials provided with the               */
/*    distribution.                                                        */
/*                                                                         */
/*    Neither the name of Texas Instruments Incorporated nor the names of  */
/*    its contributors may be used to endorse or promote products derived  */
/*    from this software without specific prior written permission.        */
/*                                                                         */
/*  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS    */
/*  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT      */
/*  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR  */
/*  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT   */
/*  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,  */
/*  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT       */
/*  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,  */
/*  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY  */
/*  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT    */
/*  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE  */
/*  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.   */
/*                                                                         */
/* ======================================================================= */
#include "fftwf_acc.h"
#include "fftwf_acc_globals.h"
#include "ti_fftw_kernel.dsp_h"
#include "fftw3.h"
#if (TI_FFTWF_NO_FAT_BINARY)
#include "ocl_util.h"
#endif

/* Hate to add this manually, but the macro is not 
 * getting picked from dlfcn.h
 */
#ifndef RTLD_NEXT
#define RTLD_NEXT  ((void *) -1l)
#endif

void fftwf_warmup(void) 
{
  const int n = TI_FFTWF_MIN_OFFLOAD_SIZE_3D_SP;
  int i, j;
  TI_FFTWF_PROFILE_PRINT(" -- Warmup begin -- \n");
  TI_FFTWF_DEBUG_PRINT("Warming up the library with 1D FORWARD with n = %d.\n",n); 
#ifdef TI_FFTWF_PROFILE
  for (i = 0; i < 2; i++) 
#else
  for (i = 0; i < 1; i++) 
#endif
    {
      fftwf_complex *in = (fftwf_complex*) fftwf_malloc(sizeof(fftwf_complex) *n*n*n);
      assert(in);
      memset(in,0x0,sizeof(fftwf_complex) *n*n*n);
      fftwf_complex *out = (fftwf_complex*) fftwf_malloc(sizeof(fftwf_complex) *n*n*n);
      assert(out);
      memset(out,0x0,sizeof(fftwf_complex) *n*n*n);
      fftwf_plan plan_forward = fftwf_plan_dft_3d(n, n, n, in, out, FFTW_FORWARD, FFTW_ESTIMATE);
      assert(plan_forward);
      for (j = 0; j < n; j++ ) {
        in[j][0] = out[j][0] = 0.0;
        in[j][1] = out[j][1] = 0.0;
      }
      fftwf_execute(plan_forward); /* repeat as needed */
      fftwf_destroy_plan(plan_forward);
      fftwf_free(out);
      fftwf_free(in);
    }
  TI_FFTWF_PROFILE_PRINT(" -- Warmup end -- \n");
}

/* Zero out all globals */

__attribute__ ((constructor)) void _ti_fftwf_acc_libinit(void) {
  _ti_fftwf_init_done = 0; 
  TI_FFTWF_OFFLOAD = -1;
#ifdef TI_FFTWF_DEBUG
  _ti_fftwf_DISABLE_DEBUG = 0;
#endif
  _ti_fftwf_DISABLE_CMEM = 0;
}

__attribute__ ((destructor)) void _ti_fftwf_acc_libfini(void) {
  /* Punt and use that one */
  _ti_fftwf_acc_libinit();
}


/* This function is called exactly once before doing anything else 
the first time the library is entered. */

void _ti_fftwf_acc_init(void)
{
  if (_ti_fftwf_init_done) 
    return;

  TI_FFTWF_PROFILE_START();

  TI_FFTWF_DEBUG_PRINT("Initializing wrapper library.\n");

  char *offload_env = getenv("TI_FFTWF_OFFLOAD");
  if (offload_env) {
    TI_FFTWF_OFFLOAD = atoi(offload_env);
    switch (TI_FFTWF_OFFLOAD) {
        case TI_FFTWF_OFFLOAD_FORCE_NONE:
          fprintf(stderr, "*** Force none as TI_FFTWF_OFFLOAD=%d\n", TI_FFTWF_OFFLOAD);
          break;
        case TI_FFTWF_OFFLOAD_FORCE_DSP:
#ifndef TI_FFTWF_ALLOW_ECPY
          fprintf(stderr, "*** Ignoring TI_FFTWF_OFFLOAD=%d as code built without ECPY support ***\n", TI_FFTWF_OFFLOAD);
          TI_FFTWF_OFFLOAD = TI_FFTWF_OFFLOAD_FORCE_NONE;
#else
          fprintf(stderr, "*** Forcing all offloads to DSP as TI_FFTWF_OFFLOAD=%d\n", TI_FFTWF_OFFLOAD);
#endif   
          break;
        case TI_FFTWF_OFFLOAD_FORCE_ARM:
          fprintf(stderr, "*** Disabling all offloads as TI_FFTWF_OFFLOAD=%d\n", TI_FFTWF_OFFLOAD);
          break;
        default:
          fprintf(stderr, "Ignoring unspported value for TI_FFTWF_OFFLOAD=%d\n", TI_FFTWF_OFFLOAD);
          TI_FFTWF_OFFLOAD = TI_FFTWF_OFFLOAD_FORCE_NONE;
          fprintf(stderr, "*** set to force none TI_FFTWF_OFFLOAD=%d\n", TI_FFTWF_OFFLOAD);
    }
  }

#ifdef TI_FFTWF_DEBUG
  char *no_debug_env = getenv("TI_FFTWF_NO_DEBUG");
  if (no_debug_env) {
    if (atoi(no_debug_env) > 0) {
      _ti_fftwf_DISABLE_DEBUG = 1;
    }
  }
#endif

  char *no_cmem_env = getenv("TI_FFTWF_NO_CMEM");
  if (no_cmem_env) {
    if (atoi(no_cmem_env) > 0) {
      _ti_fftwf_DISABLE_CMEM = 1;
    }
  }
  
#ifdef TI_FFTWF_USE_DLSYM
  DLSYM_INTERCEPT(fftwf_execute)
  DLSYM_INTERCEPT(fftwf_destroy_plan)
  DLSYM_INTERCEPT(fftwf_malloc)
  DLSYM_INTERCEPT(fftwf_free)
  /* ADD KERNEL: Add a line like below for each FFTWF plan you intercept */
  DLSYM_INTERCEPT(fftwf_plan_dft_1d)
  DLSYM_INTERCEPT(fftwf_plan_dft_r2c_1d)
  DLSYM_INTERCEPT(fftwf_plan_dft_c2r_1d)
  DLSYM_INTERCEPT(fftwf_plan_dft_2d)
  DLSYM_INTERCEPT(fftwf_plan_dft_r2c_2d)
  DLSYM_INTERCEPT(fftwf_plan_dft_c2r_2d)
  DLSYM_INTERCEPT(fftwf_plan_dft_3d)
  DLSYM_INTERCEPT(fftwf_plan_dft_r2c_3d)
  DLSYM_INTERCEPT(fftwf_plan_dft_c2r_3d)
  DLSYM_INTERCEPT(fftwf_plan_many_dft)
  DLSYM_INTERCEPT(fftwf_plan_many_dft_r2c)
  DLSYM_INTERCEPT(fftwf_plan_many_dft_c2r)
#endif /* TI_FFTWF_USE_DLSYM */

    /* Globals init */
  _ti_fftwf_tracked_plans = NULL;

  /* OpenCL init */
  /* Create an in-order command queue by default*/
  int queue_flags = 0;
#ifdef TI_FFTWF_PROFILE
  queue_flags |= CL_QUEUE_PROFILING_ENABLE; 
  /* queue_flags |= CL_QUEUE_OUT_OF_ORDER_EXEC_MODE_ENABLE; */
#endif

  /* Read the offline compiled kernel module if we need it, otherwise it's linked in */

#if (TI_FFTWF_NO_FAT_BINARY)
  char *bin;
  int len = ocl_read_binary("../c66x/ti_fftw_kernel.out", &bin);
#else
  const unsigned char *bin = (unsigned char *)ti_fftw_kernel_dsp_bin;
  const size_t len = ti_fftw_kernel_dsp_bin_len;
#endif

  cl_int err;
  cl_device_id device;
  _ti_fftwf_ocl_context = clCreateContextFromType(0,CL_DEVICE_TYPE_ACCELERATOR,0,0,&err);
  TI_FFTWF_OCL_CHKERROR("clCreateContextFromType",err);
  err = clGetDeviceIDs(0,CL_DEVICE_TYPE_ACCELERATOR,1,&device,0);
  TI_FFTWF_OCL_CHKERROR("clGetDeviceIDs",err);
  _ti_fftwf_ocl_Q = clCreateCommandQueue(_ti_fftwf_ocl_context, device, queue_flags, &err);
  TI_FFTWF_OCL_CHKERROR("clCreateCommandQueue",err);
  _ti_fftwf_ocl_program = clCreateProgramWithBinary(_ti_fftwf_ocl_context, 1, &device, &len, &bin, NULL, &err);
  TI_FFTWF_OCL_CHKERROR("clCreateProgramWithBinary",err);
  const char *compile_options = "";
  err = clBuildProgram(_ti_fftwf_ocl_program, 1, &device, compile_options, 0, 0);
  TI_FFTWF_OCL_CHKERROR("clBuildProgram",err);
  /*------------------------------------------------------------------------
   * dispatch a null kernel to force program load. (optional)
   *-----------------------------------------------------------------------*/
  _ti_fftwf_ocl_FFT_execute = clCreateKernel(_ti_fftwf_ocl_program, "ocl_FFT_execute",&err);
  TI_FFTWF_OCL_CHKERROR("clCreateKernel",err);
  _ti_fftwf_ocl_FFT_destroy_plan = clCreateKernel(_ti_fftwf_ocl_program, "ocl_FFT_destroy_plan",&err);
  TI_FFTWF_OCL_CHKERROR("clCreateKernel",err);

  int i;
  for (i=0;i<TI_FFTWF_FFT_PLAN_MAX;i++) {
    _ti_fftwf_ocl_plans[i].fftwf_plan_name = _ti_fftwf_PLAN_FUNCS[i];
#ifdef TI_FFTWF_ALLOW_ECPY
    if (_ti_fftwf_OCL_OMP_KERNELS[i]) {
      _ti_fftwf_ocl_plans[i].ecpy = clCreateKernel(_ti_fftwf_ocl_program,_ti_fftwf_OCL_OMP_KERNELS[i],&err);
      _ti_fftwf_ocl_plans[i].ecpy_name = _ti_fftwf_OCL_OMP_KERNELS[i];
      TI_FFTWF_OCL_CHKERROR("clCreateKernel",err);
    }
#endif
#ifdef TI_FFTWF_ALLOW_DIRECT
    if (_ti_fftwf_OCL_KERNELS[i]) {
      _ti_fftwf_ocl_plans[i].direct = clCreateKernel(_ti_fftwf_ocl_program,_ti_fftwf_OCL_KERNELS[i],&err);
      TI_FFTWF_OCL_CHKERROR("clCreateKernel",err);
      _ti_fftwf_ocl_plans[i].direct_name = _ti_fftwf_OCL_KERNELS[i];
    }
#endif
  }

#if (TI_FFTWF_NO_FAT_BINARY)
  delete [] ti_fftwf_kernel_dsp_bin;
#endif

  atexit(_ti_fftwf_acc_fini);

  _ti_fftwf_init_done = 1;
  TI_FFTWF_PROFILE_REPORT("Initialization took %8.2f us\n",(float)clock_diff);

  /* Now, warm it all up! */
  //fftwf_warmup();
}

void _ti_fftwf_acc_fini(void)
{
  int i;
  cl_int err;

  if(_ti_fftwf_init_done == 0)
    return;

  TI_FFTWF_DEBUG_PRINT("Finalizing wrapper library.\n");

  err = clReleaseKernel(_ti_fftwf_ocl_FFT_execute);
  TI_FFTWF_OCL_CHKERROR("clReleaseKernel",err);
  err = clReleaseKernel(_ti_fftwf_ocl_FFT_destroy_plan);
  TI_FFTWF_OCL_CHKERROR("clReleaseKernel",err);

  for (i=0;i<TI_FFTWF_FFT_PLAN_MAX;i++) {
    if (_ti_fftwf_OCL_OMP_KERNELS[i]) {
      err = clReleaseKernel(_ti_fftwf_ocl_plans[i].ecpy);
      TI_FFTWF_OCL_CHKERROR("clReleaseKernel",err);
    }
  }

  err = clReleaseProgram(_ti_fftwf_ocl_program);
  TI_FFTWF_OCL_CHKERROR("clReleaseProgram",err);

  err = clReleaseCommandQueue(_ti_fftwf_ocl_Q);
  TI_FFTWF_OCL_CHKERROR("clReleaseCommandQueue",err);

  err = clReleaseContext(_ti_fftwf_ocl_context);
  TI_FFTWF_OCL_CHKERROR("clReleaseContext",err);
}

/* end of fftwf_acc_initfini.c */
