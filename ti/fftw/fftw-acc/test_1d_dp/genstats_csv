#!/bin/bash -e

SIZES2=${*:-"1024 2048 4096 8192 16384 32768 65536 131072 262144 524288 1048576"}
SIZES3=${*:-"1536 3072 4608 9216 13824 27648 41472 82944 124416 248832 373248 746496"}
SIZES5=${*:-"1280 1600 3200 6400 8000 16000 32000 40000 80000 160000 200000 400000 800000 1000000"}
SIZES35=${*:-"1920 3840 5760 9600 15360 28800 57600 86400 115200 172800 230400 345600 691200"}

NUM_TESTS_PER_SIZE=10
EXE="fft_static"

case $0 in
  *csv) sed_script='s/^[[:space:]]\+//; s/[[:space:]]\+/,/g' ;;
  *) sed_script='s/xyz//';;
esac

function run_sizes2
{
  printf "%20s\t %12s\t %12s\t %12s\t %12s\t %12s\t %12s\t %12s\t %12s\n" "Name" "Type" "Size" "MeanGFlop" "MinGFlop" "MaxGFlop" "MeanMs" "MinMs" "MaxMs"
  for size in $SIZES2; do
    printf "%20s\t %12d\t %12s\t " "fft_1d_dp" $3 $size
    ./$EXE 0 $1 $2 $NUM_TESTS_PER_SIZE $size 2>&1 | grep flops | awk 'NR % 2 == 1' | awk '{print $8}' | sort -n | awk 'BEGIN{c=0;sum=0;} /^[^#]/{a[c++]=$1;sum+=$1;} END{ave=sum/c; if((c%2)==1){median=a[int(c/2)];} else{median=(a[c/2]+a[c/2-1])/2;} printf "%12.6f\t %12.6f\t %12.6f\t ", ave,a[0],a[c-1]}'
    rm -f /tmp/opencl*
    ./$EXE 0 $1 $2 $NUM_TESTS_PER_SIZE $size 2>&1 | grep ms | awk 'NR % 2 == 1' | awk '{print $8}' | sort -n | awk 'BEGIN{c=0;sum=0;} /^[^#]/{a[c++]=$1;sum+=$1;} END{ave=sum/c; if((c%2)==1){median=a[int(c/2)];} else{median=(a[c/2]+a[c/2-1])/2;} printf "%12.6f\t %12.6f\t %12.6f\n", ave,a[0],a[c-1]}'
    rm -f /tmp/opencl*
  done
}

function run_sizes2_i
{
  printf "%20s\t %12s\t %12s\t %12s\t %12s\t %12s\t %12s\t %12s\t %12s\n" "Name" "Type" "Size" "MeanGFlop" "MinGFlop" "MaxGFlop" "MeanMs" "MinMs" "MaxMs"
  for size in $SIZES2; do
    printf "%20s\t %12d\t %12s\t " "ifft_1d_dp" $3 $size
    ./$EXE 0 $1 $2 $NUM_TESTS_PER_SIZE $size 2>&1 | grep flops | awk 'NR % 2 == 1' | awk '{print $8}' | sort -n | awk 'BEGIN{c=0;sum=0;} /^[^#]/{a[c++]=$1;sum+=$1;} END{ave=sum/c; if((c%2)==1){median=a[int(c/2)];} else{median=(a[c/2]+a[c/2-1])/2;} printf "%12.6f\t %12.6f\t %12.6f\t ", ave,a[0],a[c-1]}'
    rm -f /tmp/opencl*
    ./$EXE 0 $1 $2 $NUM_TESTS_PER_SIZE $size 2>&1 | grep ms | awk 'NR % 2 == 1' | awk '{print $8}' | sort -n | awk 'BEGIN{c=0;sum=0;} /^[^#]/{a[c++]=$1;sum+=$1;} END{ave=sum/c; if((c%2)==1){median=a[int(c/2)];} else{median=(a[c/2]+a[c/2-1])/2;} printf "%12.6f\t %12.6f\t %12.6f\n", ave,a[0],a[c-1]}'
    rm -f /tmp/opencl*
  done
}

function run_sizes3
{
  printf "%20s\t %12s\t %12s\t %12s\t %12s\t %12s\t %12s\t %12s\t %12s\n" "Name" "Type" "Size" "MeanGFlop" "MinGFlop" "MaxGFlop" "MeanMs" "MinMs" "MaxMs"
  for size in $SIZES3; do
    printf "%20s\t %12d\t %12s\t " "fft_1d_dp" $3 $size
    ./$EXE 0 $1 $2 $NUM_TESTS_PER_SIZE $size 2>&1 | grep flops | awk 'NR % 2 == 1' | awk '{print $8}' | sort -n | awk 'BEGIN{c=0;sum=0;} /^[^#]/{a[c++]=$1;sum+=$1;} END{ave=sum/c; if((c%2)==1){median=a[int(c/2)];} else{median=(a[c/2]+a[c/2-1])/2;} printf "%12.6f\t %12.6f\t %12.6f\t ", ave,a[0],a[c-1]}'
    rm -f /tmp/opencl*
    ./$EXE 0 $1 $2 $NUM_TESTS_PER_SIZE $size 2>&1 | grep ms | awk 'NR % 2 == 1' | awk '{print $8}' | sort -n | awk 'BEGIN{c=0;sum=0;} /^[^#]/{a[c++]=$1;sum+=$1;} END{ave=sum/c; if((c%2)==1){median=a[int(c/2)];} else{median=(a[c/2]+a[c/2-1])/2;} printf "%12.6f\t %12.6f\t %12.6f\n", ave,a[0],a[c-1]}'
    rm -f /tmp/opencl*
  done
}

function run_sizes3_i
{
  printf "%20s\t %12s\t %12s\t %12s\t %12s\t %12s\t %12s\t %12s\t %12s\n" "Name" "Type" "Size" "MeanGFlop" "MinGFlop" "MaxGFlop" "MeanMs" "MinMs" "MaxMs"
  for size in $SIZES3; do
    printf "%20s\t %12d\t %12s\t " "ifft_1d_dp" $3 $size
    ./$EXE 0 $1 $2 $NUM_TESTS_PER_SIZE $size 2>&1 | grep flops | awk 'NR % 2 == 1' | awk '{print $8}' | sort -n | awk 'BEGIN{c=0;sum=0;} /^[^#]/{a[c++]=$1;sum+=$1;} END{ave=sum/c; if((c%2)==1){median=a[int(c/2)];} else{median=(a[c/2]+a[c/2-1])/2;} printf "%12.6f\t %12.6f\t %12.6f\t ", ave,a[0],a[c-1]}'
    rm -f /tmp/opencl*
    ./$EXE 0 $1 $2 $NUM_TESTS_PER_SIZE $size 2>&1 | grep ms | awk 'NR % 2 == 1' | awk '{print $8}' | sort -n | awk 'BEGIN{c=0;sum=0;} /^[^#]/{a[c++]=$1;sum+=$1;} END{ave=sum/c; if((c%2)==1){median=a[int(c/2)];} else{median=(a[c/2]+a[c/2-1])/2;} printf "%12.6f\t %12.6f\t %12.6f\n", ave,a[0],a[c-1]}'
    rm -f /tmp/opencl*
  done
}

function run_sizes5
{
  printf "%20s\t %12s\t %12s\t %12s\t %12s\t %12s\t %12s\t %12s\t %12s\n" "Name" "Type" "Size" "MeanGFlop" "MinGFlop" "MaxGFlop" "MeanMs" "MinMs" "MaxMs"
  for size in $SIZES5; do
    printf "%20s\t %12d\t %12s\t " "fft_1d_dp" $3 $size
    ./$EXE 0 $1 $2 $NUM_TESTS_PER_SIZE $size 2>&1 | grep flops | awk 'NR % 2 == 1' | awk '{print $8}' | sort -n | awk 'BEGIN{c=0;sum=0;} /^[^#]/{a[c++]=$1;sum+=$1;} END{ave=sum/c; if((c%2)==1){median=a[int(c/2)];} else{median=(a[c/2]+a[c/2-1])/2;} printf "%12.6f\t %12.6f\t %12.6f\t ", ave,a[0],a[c-1]}'
    rm -f /tmp/opencl*
    ./$EXE 0 $1 $2 $NUM_TESTS_PER_SIZE $size 2>&1 | grep ms | awk 'NR % 2 == 1' | awk '{print $8}' | sort -n | awk 'BEGIN{c=0;sum=0;} /^[^#]/{a[c++]=$1;sum+=$1;} END{ave=sum/c; if((c%2)==1){median=a[int(c/2)];} else{median=(a[c/2]+a[c/2-1])/2;} printf "%12.6f\t %12.6f\t %12.6f\n", ave,a[0],a[c-1]}'
    rm -f /tmp/opencl*
  done
}

function run_sizes5_i
{
  printf "%20s\t %12s\t %12s\t %12s\t %12s\t %12s\t %12s\t %12s\t %12s\n" "Name" "Type" "Size" "MeanGFlop" "MinGFlop" "MaxGFlop" "MeanMs" "MinMs" "MaxMs"
  for size in $SIZES5; do
    printf "%20s\t %12d\t %12s\t " "ifft_1d_dp" $3 $size
    ./$EXE 0 $1 $2 $NUM_TESTS_PER_SIZE $size 2>&1 | grep flops | awk 'NR % 2 == 1' | awk '{print $8}' | sort -n | awk 'BEGIN{c=0;sum=0;} /^[^#]/{a[c++]=$1;sum+=$1;} END{ave=sum/c; if((c%2)==1){median=a[int(c/2)];} else{median=(a[c/2]+a[c/2-1])/2;} printf "%12.6f\t %12.6f\t %12.6f\t ", ave,a[0],a[c-1]}'
    rm -f /tmp/opencl*
    ./$EXE 0 $1 $2 $NUM_TESTS_PER_SIZE $size 2>&1 | grep ms | awk 'NR % 2 == 1' | awk '{print $8}' | sort -n | awk 'BEGIN{c=0;sum=0;} /^[^#]/{a[c++]=$1;sum+=$1;} END{ave=sum/c; if((c%2)==1){median=a[int(c/2)];} else{median=(a[c/2]+a[c/2-1])/2;} printf "%12.6f\t %12.6f\t %12.6f\n", ave,a[0],a[c-1]}'
    rm -f /tmp/opencl*
  done
}

function run_sizes35
{
  printf "%20s\t %12s\t %12s\t %12s\t %12s\t %12s\t %12s\t %12s\t %12s\n" "Name" "Type" "Size" "MeanGFlop" "MinGFlop" "MaxGFlop" "MeanMs" "MinMs" "MaxMs"
  for size in $SIZES35; do
    printf "%20s\t %12d\t %12s\t " "fft_1d_dp" $3 $size
    ./$EXE 0 $1 $2 $NUM_TESTS_PER_SIZE $size 2>&1 | grep flops | awk 'NR % 2 == 1' | awk '{print $8}' | sort -n | awk 'BEGIN{c=0;sum=0;} /^[^#]/{a[c++]=$1;sum+=$1;} END{ave=sum/c; if((c%2)==1){median=a[int(c/2)];} else{median=(a[c/2]+a[c/2-1])/2;} printf "%12.6f\t %12.6f\t %12.6f\t ", ave,a[0],a[c-1]}'
    rm -f /tmp/opencl*
    ./$EXE 0 $1 $2 $NUM_TESTS_PER_SIZE $size 2>&1 | grep ms | awk 'NR % 2 == 1' | awk '{print $8}' | sort -n | awk 'BEGIN{c=0;sum=0;} /^[^#]/{a[c++]=$1;sum+=$1;} END{ave=sum/c; if((c%2)==1){median=a[int(c/2)];} else{median=(a[c/2]+a[c/2-1])/2;} printf "%12.6f\t %12.6f\t %12.6f\n", ave,a[0],a[c-1]}'
    rm -f /tmp/opencl*
  done
}

function run_sizes35_i
{
  printf "%20s\t %12s\t %12s\t %12s\t %12s\t %12s\t %12s\t %12s\t %12s\n" "Name" "Type" "Size" "MeanGFlop" "MinGFlop" "MaxGFlop" "MeanMs" "MinMs" "MaxMs"
  for size in $SIZES35; do
    printf "%20s\t %12d\t %12s\t " "ifft_1d_dp" $3 $size
    ./$EXE 0 $1 $2 $NUM_TESTS_PER_SIZE $size 2>&1 | grep flops | awk 'NR % 2 == 1' | awk '{print $8}' | sort -n | awk 'BEGIN{c=0;sum=0;} /^[^#]/{a[c++]=$1;sum+=$1;} END{ave=sum/c; if((c%2)==1){median=a[int(c/2)];} else{median=(a[c/2]+a[c/2-1])/2;} printf "%12.6f\t %12.6f\t %12.6f\t ", ave,a[0],a[c-1]}'
    rm -f /tmp/opencl*
    ./$EXE 0 $1 $2 $NUM_TESTS_PER_SIZE $size 2>&1 | grep ms | awk 'NR % 2 == 1' | awk '{print $8}' | sort -n | awk 'BEGIN{c=0;sum=0;} /^[^#]/{a[c++]=$1;sum+=$1;} END{ave=sum/c; if((c%2)==1){median=a[int(c/2)];} else{median=(a[c/2]+a[c/2-1])/2;} printf "%12.6f\t %12.6f\t %12.6f\n", ave,a[0],a[c-1]}'
    rm -f /tmp/opencl*
  done
}

test -x ./$EXE || make $EXE

{
  export TI_FFTW_OFFLOAD=2 #
  echo; run_sizes2 0 1 1
  echo; run_sizes2 1 1 2

  export TI_FFTW_OFFLOAD=1 # to run DSP tests
  echo; run_sizes2 0 1 3
  echo; run_sizes2 1 1 4

  export TI_FFTW_OFFLOAD=0 # to run ARM tests
  echo; run_sizes2 0 1 5
  echo; run_sizes2 1 1 6
} | sed -e "$sed_script"

{
  export TI_FFTW_OFFLOAD=2 #
  echo; run_sizes2_i 0 -1 1
  echo; run_sizes2_i 1 -1 2

  export TI_FFTW_OFFLOAD=1 # to run DSP tests
  echo; run_sizes2_i 0 -1 3
  echo; run_sizes2_i 1 -1 4

  export TI_FFTW_OFFLOAD=0 # to run ARM tests
  echo; run_sizes2_i 0 -1 5
  echo; run_sizes2_i 1 -1 6
} | sed -e "$sed_script"

{
  export TI_FFTW_OFFLOAD=2 #
  echo; run_sizes3 0 1 1
  echo; run_sizes3 1 1 2

  export TI_FFTW_OFFLOAD=1 # to run DSP tests
  echo; run_sizes3 0 1 3
  echo; run_sizes3 1 1 4

  export TI_FFTW_OFFLOAD=0 # to run ARM tests
  echo; run_sizes3 0 1 5
  echo; run_sizes3 1 1 6
} | sed -e "$sed_script"

{
  export TI_FFTW_OFFLOAD=2 #
  echo; run_sizes3_i 0 -1 1
  echo; run_sizes3_i 1 -1 2

  export TI_FFTW_OFFLOAD=1 # to run DSP tests
  echo; run_sizes3_i 0 -1 3
  echo; run_sizes3_i 1 -1 4

  export TI_FFTW_OFFLOAD=0 # to run ARM tests
  echo; run_sizes3_i 0 -1 5
  echo; run_sizes3_i 1 -1 6
} | sed -e "$sed_script"

{
  export TI_FFTW_OFFLOAD=2 #
  echo; run_sizes5 0 1 1
  echo; run_sizes5 1 1 2

  export TI_FFTW_OFFLOAD=1 # to run DSP tests
  echo; run_sizes5 0 1 3
  echo; run_sizes5 1 1 4

  export TI_FFTW_OFFLOAD=0 # to run ARM tests
  echo; run_sizes5 0 1 5
  echo; run_sizes5 1 1 6
} | sed -e "$sed_script"

{
  export TI_FFTW_OFFLOAD=2 #
  echo; run_sizes5_i 0 -1 1
  echo; run_sizes5_i 1 -1 2

  export TI_FFTW_OFFLOAD=1 # to run DSP tests
  echo; run_sizes5_i 0 -1 3
  echo; run_sizes5_i 1 -1 4

  export TI_FFTW_OFFLOAD=0 # to run ARM tests
  echo; run_sizes5_i 0 -1 5
  echo; run_sizes5_i 1 -1 6
} | sed -e "$sed_script"

{
  export TI_FFTW_OFFLOAD=2 #
  echo; run_sizes35 0 1 1
  echo; run_sizes35 1 1 2

  export TI_FFTW_OFFLOAD=1 # to run DSP tests
  echo; run_sizes35 0 1 3
  echo; run_sizes35 1 1 4

  export TI_FFTW_OFFLOAD=0 # to run ARM tests
  echo; run_sizes35 0 1 5
  echo; run_sizes35 1 1 6
} | sed -e "$sed_script"

{
  export TI_FFTW_OFFLOAD=2 #
  echo; run_sizes35_i 0 -1 1
  echo; run_sizes35_i 1 -1 2

  export TI_FFTW_OFFLOAD=1 # to run DSP tests
  echo; run_sizes35_i 0 -1 3
  echo; run_sizes35_i 1 -1 4

  export TI_FFTW_OFFLOAD=0 # to run ARM tests
  echo; run_sizes35_i 0 -1 5
  echo; run_sizes35_i 1 -1 6
} | sed -e "$sed_script"

