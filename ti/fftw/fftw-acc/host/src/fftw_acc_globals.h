/* ======================================================================= */
/*  TEXAS INSTRUMENTS, INC.                                                */
/*                                                                         */
/*  FFTLIB  FFT Library                                                    */
/*                                                                         */
/* Copyright (C) 2014 Texas Instruments Incorporated - http://www.ti.com/  */
/*                                                                         */
/*                                                                         */
/*  Redistribution and use in source and binary forms, with or without     */
/*  modification, are permitted provided that the following conditions     */
/*  are met:                                                               */
/*                                                                         */
/*    Redistributions of source code must retain the above copyright       */
/*    notice, this list of conditions and the following disclaimer.        */
/*                                                                         */
/*    Redistributions in binary form must reproduce the above copyright    */
/*    notice, this list of conditions and the following disclaimer in the  */
/*    documentation and/or other materials provided with the               */
/*    distribution.                                                        */
/*                                                                         */
/*    Neither the name of Texas Instruments Incorporated nor the names of  */
/*    its contributors may be used to endorse or promote products derived  */
/*    from this software without specific prior written permission.        */
/*                                                                         */
/*  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS    */
/*  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT      */
/*  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR  */
/*  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT   */
/*  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,  */
/*  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT       */
/*  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,  */
/*  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY  */
/*  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT    */
/*  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE  */
/*  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.   */
/*                                                                         */
/* ======================================================================= */
#ifndef _TI_FFTW_GLOBALS_H
#define _TI_FFTW_GLOBALS_H
/* Everything global below is also in:

   fftw_acc_globals.c
   fftw_acc_initfini.c
   fftw_acc_globals.h

   Change em all! */

/* global flags */
extern volatile int _ti_fftw_init_done;
extern volatile int TI_FFTW_OFFLOAD;
#ifdef TI_FFTW_DEBUG
extern volatile int _ti_fftw_DISABLE_DEBUG;
#endif
extern volatile int _ti_fftw_DISABLE_CMEM;

extern cl_context       _ti_fftw_ocl_context;
extern cl_command_queue _ti_fftw_ocl_Q;
extern cl_program       _ti_fftw_ocl_program;
extern cl_kernel        _ti_fftw_ocl_FFT_execute;
extern cl_kernel        _ti_fftw_ocl_FFT_destroy_plan;
extern _ti_fftw_ocl_plan_kernel_t _ti_fftw_ocl_plans[TI_FFTW_FFT_PLAN_MAX];
extern const char* _ti_fftw_PLAN_FUNCS[TI_FFTW_FFT_PLAN_MAX];
extern const char* _ti_fftw_OCL_OMP_KERNELS[TI_FFTW_FFT_PLAN_MAX];
extern element_t *_ti_fftw_tracked_plans;

#define EXTERN_REAL_DECLS(fname,params) \
  extern fftw_plan __real_##fname params ; 

#define DLSYM_DECLS(fname,params) \
  typedef fftw_plan (*_fptr_##fname##_t) params ; \
  extern _fptr_##fname##_t _fptr_##fname ; 

#define DLSYM_INTERCEPT(fname) \
  { \
    if (!_fptr_##fname) \
      _fptr_##fname = (_fptr_##fname##_t)dlsym(RTLD_NEXT, #fname); \
    if (!_fptr_##fname) { \
      TI_FFTW_ERROR_PRINT("Could not intercept %s from the FFTW library. Aborting now.\n", #fname); \
      abort(); \
    } \
  }

#define DLSYM_DEFN(fname) \
  _fptr_##fname##_t _fptr_##fname ;


#ifdef TI_FFTW_USE_DLSYM 
DLSYM_DECLS(fftw_execute,(const fftw_plan plan));
DLSYM_DECLS(fftw_destroy_plan,(const fftw_plan plan));
DLSYM_DECLS(fftw_malloc,(size_t n));
DLSYM_DECLS(fftw_free,(void *p));
/* ADD KERNEL: Add a line as below for each fftw plan you intercept */
DLSYM_DECLS(fftw_plan_dft_1d,(int n, fftw_complex *in, fftw_complex *out, int sign, unsigned flags))
DLSYM_DECLS(fftw_plan_dft_r2c_1d,(int n, double *in, fftw_complex *out, unsigned flags))
DLSYM_DECLS(fftw_plan_dft_c2r_1d,(int n, fftw_complex *in, double *out, unsigned flags))
DLSYM_DECLS(fftw_plan_dft_2d,(int n1, int n2, fftw_complex *in, fftw_complex *out, int sign, unsigned flags))
DLSYM_DECLS(fftw_plan_dft_r2c_2d,(int n1, int n2, double *in, fftw_complex *out, unsigned flags))
DLSYM_DECLS(fftw_plan_dft_c2r_2d,(int n1, int n2, fftw_complex *in, double *out, unsigned flags))
DLSYM_DECLS(fftw_plan_dft_3d,(int n1, int n2, int n3, fftw_complex *in, fftw_complex *out, int sign, unsigned flags))
DLSYM_DECLS(fftw_plan_dft_r2c_3d,(int n1, int n2, int n3, double *in, fftw_complex *out, unsigned flags))
DLSYM_DECLS(fftw_plan_dft_c2r_3d,(int n1, int n2, int n3, fftw_complex *in, double *out, unsigned flags))
DLSYM_DECLS(fftw_plan_many_dft,(int rank, const int *n, int howmany, fftw_complex *in, const int *inembed, int istride, int idist, fftw_complex *out, const int *onembed, int ostride, int odist, int sign, unsigned flags))
DLSYM_DECLS(fftw_plan_many_dft_r2c,(int rank, const int *n, int howmany, double *in, const int *inembed, int istride, int idist, fftw_complex *out, const int *onembed, int ostride, int odist, unsigned flags))
DLSYM_DECLS(fftw_plan_many_dft_c2r,(int rank, const int *n, int howmany, fftw_complex *in, const int *inembed, int istride, int idist, double *out, const int *onembed, int ostride, int odist, unsigned flags))
#else
EXTERN_REAL_DECLS(fftw_execute,(const fftw_plan plan));
EXTERN_REAL_DECLS(fftw_destroy_plan,(const fftw_plan plan));
EXTERN_REAL_DECLS(fftw_malloc,(size_t n));
EXTERN_REAL_DECLS(fftw_free,(void *p));
/* ADD KERNEL: Add a line as below for each fftw plan you intercept */
EXTERN_REAL_DECLS(fftw_plan_dft_1d,(int n, fftw_complex *in, fftw_complex *out, int sign, unsigned flags))
EXTERN_REAL_DECLS(fftw_plan_dft_r2c_1d,(int n, double *in, fftw_complex *out, unsigned flags))
EXTERN_REAL_DECLS(fftw_plan_dft_c2r_1d,(int n, fftw_complex *in, double *out, unsigned flags))
EXTERN_REAL_DECLS(fftw_plan_dft_2d,(int n1, int n2, fftw_complex *in, fftw_complex *out, int sign, unsigned flags))
EXTERN_REAL_DECLS(fftw_plan_dft_r2c_2d,(int n1, int n2, double *in, fftw_complex *out, unsigned flags))
EXTERN_REAL_DECLS(fftw_plan_dft_c2r_2d,(int n1, int n2, fftw_complex *in, double *out, unsigned flags))
EXTERN_REAL_DECLS(fftw_plan_dft_3d,(int n1, int n2, int n3, fftw_complex *in, fftw_complex *out, int sign, unsigned flags))
EXTERN_REAL_DECLS(fftw_plan_dft_r2c_3d,(int n1, int n2, int n3, double *in, fftw_complex *out, unsigned flags))
EXTERN_REAL_DECLS(fftw_plan_dft_c2r_3d,(int n1, int n2, int n3, fftw_complex *in, double *out, unsigned flags))
EXTERN_REAL_DECLS(fftw_plan_many_dft,(int rank, const int *n, int howmany, fftw_complex *in, const int *inembed, int istride, int idist, fftw_complex *out, const int *onembed, int ostride, int odist, int sign, unsigned flags))
EXTERN_REAL_DECLS(fftw_plan_many_dft_r2c,(int rank, const int *n, int howmany, double *in, const int *inembed, int istride, int idist, fftw_complex *out, const int *onembed, int ostride, int odist, unsigned flags))
EXTERN_REAL_DECLS(fftw_plan_many_dft_c2r,(int rank, const int *n, int howmany, fftw_complex *in, const int *inembed, int istride, int idist, double *out, const int *onembed, int ostride, int odist, unsigned flags))
#endif /* TI_FFTW_USE_DLSYM */

#endif /* _TI_FFTW_GLOBALS_H */

/* end of fftw_acc_globals.h */

