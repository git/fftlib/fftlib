/* ======================================================================= */
/*  TEXAS INSTRUMENTS, INC.                                                */
/*                                                                         */
/*  FFTLIB  FFT Library                                                    */
/*                                                                         */
/* Copyright (C) 2014 Texas Instruments Incorporated - http://www.ti.com/  */
/*                                                                         */
/*                                                                         */
/*  Redistribution and use in source and binary forms, with or without     */
/*  modification, are permitted provided that the following conditions     */
/*  are met:                                                               */
/*                                                                         */
/*    Redistributions of source code must retain the above copyright       */
/*    notice, this list of conditions and the following disclaimer.        */
/*                                                                         */
/*    Redistributions in binary form must reproduce the above copyright    */
/*    notice, this list of conditions and the following disclaimer in the  */
/*    documentation and/or other materials provided with the               */
/*    distribution.                                                        */
/*                                                                         */
/*    Neither the name of Texas Instruments Incorporated nor the names of  */
/*    its contributors may be used to endorse or promote products derived  */
/*    from this software without specific prior written permission.        */
/*                                                                         */
/*  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS    */
/*  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT      */
/*  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR  */
/*  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT   */
/*  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,  */
/*  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT       */
/*  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,  */
/*  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY  */
/*  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT    */
/*  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE  */
/*  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.   */
/*                                                                         */
/* ======================================================================= */
#include "fftw_acc.h"
#include "fftw_acc_globals.h"
#include "ti/fftlib/src/fft_dp_1d_c2c_batch/fft_dp_1d_c2c_batch.h"
#include "ti/fftlib/src/fft_dp_1d_r2c_batch/fft_dp_1d_r2c_batch.h"
#include "ti/fftlib/src/ifft_dp_1d_c2c_batch/ifft_dp_1d_c2c_batch.h"
#include "ti/fftlib/src/ifft_dp_1d_c2r_batch/ifft_dp_1d_c2r_batch.h"

/*************************************************************************/
/************** FFTW INTERCEPTED FUNCTIONS *******************************/
/*************************************************************************/

/*--------------------------------------------------------------------------------------------------------------------
                                rad and memory calculation for c2c
  --------------------------------------------------------------------------------------------------------------------*/

/* calculate radix for 1D array */
static void calculate_rad_c2c (int N, fft_para_mix_bs_t *fft_para)
{
  int n, j, M, rad;
  int s_r3, s_r5, N_p2,dum;
  int use_bs;

  use_bs = 1;

  n = (N<<1)-1;
  j = fftw_norm(n);
  M = 1 << (31-j);
  j = fftw_norm(M);

  N_p2 = N;
  s_r3 = 0;
  s_r5 = 0;

  /* if N is odd, bypass the rest of calculation, use Bluestein */
  if (((N>>1)<<1) == N)
  {
    /* if N is power of 2, no zero padding, regular radix 2 or 4 fft */
    if (N == (M>>1))
    { /* power of 2 */
      M = N;
      j = j+1;
      use_bs = 0;
    }
    else {
      dum = N;
      while (dum/3*3 == dum) {
        s_r3++;
        dum /= 3;
      }
      while (dum/5*5 == dum) {
        s_r5++;
        dum /= 5;
      }
      if ((dum >= 4) && (dum == (1 << (30-fftw_norm(dum))))) {
        /* mixed radix 2 and 3 */
        N_p2 = dum;
        use_bs = 0;
        M = N;
        j = fftw_norm(dum);
      }
    }
  }

  /* calculate radix for radix 2 or 4 c2c fft */
  if (j%2 == 0)
    rad = 4;
  else
    rad = 2;

  fft_para->N_p2   = N_p2;
  fft_para->s_r3   = s_r3;
  fft_para->s_r5   = s_r5;
  fft_para->M      = M;
  fft_para->rad    = rad;
  fft_para->use_bs = use_bs;

} /* calculate_rad */

/* calculate twiddle size, Bn buffer size and workbuffer size */
static void calculate_mem_size_c2c (int N, fft_para_mix_bs_t *fft_para, int *twsize, int *Bnsize, int *worksize)
{
  int dum, n, i;
  int use_bs = fft_para->use_bs;
  int M = fft_para->M;
  int s_r3 = fft_para->s_r3;
  int s_r5 = fft_para->s_r5;
  int N_p2 = fft_para->N_p2;

  /* Please note that Bn buffer and Workbuf are only used when doing Bluestein
   * meaning when use_bs == 1 */

  if (use_bs == 1)
  { /* when size is not power of 2 and not multiple of 3&5 */
    *twsize   = (2*M+2*N);
    *Bnsize   = (2*M);
    *worksize = (4*M);
  }
  else
  {
    if ((s_r3 == 0) && (s_r5 == 0)) {
      /* When signal is power of 2 */
      *twsize = N*2;
    }
    else {
      /* When signal is power of 3 or 5 */
      dum = 0;
      n   = 4*N;
      for (i = 0; i < s_r3; i++) {
        dum += n/3;
        n   /= 3;
      }
      n *= 2;  /* radix-5 has 8 twiddle vs radix-3 4 twiddles */
      for (i = 0; i < s_r5; i++) {
        dum += n/5;
        n   /= 5;
      }
      *twsize = (2*N_p2+dum);
    }
    *Bnsize   = 0;
    *worksize = 0;
  }
} /* calculate_mem_size */

/*--------------------------------------------------------------------------------------------------------------------
                                rad and memory calculation for r2c
  --------------------------------------------------------------------------------------------------------------------*/
static void calculate_rad_r2c (int N, fft_para_mix_bs_t *fft_para)
{
  int n, j, M, rad;
  int s_r3, s_r5, N_p2,dum;
  int use_bs;

  use_bs = 1;

  n = (N<<1)-1;
  j = fftw_norm(n);
  M = 1 << (31-j);

  N_p2 = N/2;
  s_r3 = 0;
  s_r5 = 0;

  /* if N is odd, bypass the rest of calculation, use Bluestein */
  if (((N>>1)<<1) == N)
  {
    /* if N is power of 2, no zero padding, regular radix 2 or 4 fft */
    if (N == (M>>1))
    { /* power of 2 */
      M = N;
      j = j+1;
      use_bs = 0;
    }
    else {
      dum = N/2;
      while (dum/3*3 == dum) {
        s_r3++;
        dum /= 3;
      }
      while (dum/5*5 == dum) {
        s_r5++;
        dum /= 5;
      }
      if ((dum >= 4) && (dum == (1 << (30-fftw_norm(dum))))) {
        /* mixed radix 2 and 3 */
        N_p2 = dum;
        use_bs = 0;
        M = N;
        j = fftw_norm(dum);
      }
    }
  }

  /* calculate radix for radix 2 or 4 r2c fft */
  if (j%2 == use_bs)
    rad = 4;
  else
    rad = 2;

  fft_para->N_p2   = N_p2;
  fft_para->s_r3   = s_r3;
  fft_para->s_r5   = s_r5;
  fft_para->M      = M;
  fft_para->rad    = rad;
  fft_para->use_bs = use_bs;

} /* calculate_rad */

/* calculate twiddle size, Bn buffer size and workbuffer size */
static void calculate_mem_size_r2c (int N, fft_para_mix_bs_t *fft_para, int *twsize, int *Bnsize, int *worksize)
{
  int dum, n, i;
  int use_bs = fft_para->use_bs;
  int M = fft_para->M;
  int s_r3 = fft_para->s_r3;
  int s_r5 = fft_para->s_r5;
  int N_p2 = fft_para->N_p2;

  /* Please note that Bn buffer and Workbuf are only used when doing Bluestein
   * meaning when use_bs == 1 */

  if (use_bs == 1)
  { /* when size is not power of 2 and not multiple of 3&5 */
    *twsize   = (2*M+2*N);
    *Bnsize   = (2*M);
    *worksize = (4*M);
  }
  else
  {
    if ((s_r3 == 0) && (s_r5 == 0)) {
      /* When signal is power of 2 */
      *twsize = N*3/2;
    }
    else {
    /* When signal is power of 3 or 5 */
      dum = 0;
      n   = 2*N;
      for (i = 0; i < s_r3; i++) {
        dum += n/3;
        n   /= 3;
      }
      n *= 2;  /* radix-5 has 8 twiddle vs radix-3 4 twiddles */
      for (i = 0; i < s_r5; i++) {
        dum += n/5;
        n   /= 5;
      }
      *twsize = (N/2+2*N_p2+dum);
    }
    *Bnsize   = 0;
    *worksize = 0;
  }
} /* calculate_mem_size */


/*--------------------------------------------------------------------------------------------------------------------
                                rad and memory calculation for c2r
  --------------------------------------------------------------------------------------------------------------------*/
static void calculate_rad_c2r (int N, fft_para_mix_bs_t *fft_para)
{
  int n, j, M, rad;
  int s_r3, s_r5, N_p2,dum;
  int use_bs;

  use_bs = 1;

  n = (N<<1)-1;
  j = fftw_norm(n);
  M = 1 << (31-j);

  N_p2 = N/2;
  s_r3 = 0;
  s_r5 = 0;

  /* if N is odd, bypass the rest of calculation, use Bluestein */
  if (((N>>1)<<1) == N)
  {
    /* if N is power of 2, no zero padding, regular radix 2 or 4 fft */
    if (N == (M>>1))
    { /* power of 2 */
      M = N;
      j = j+1;
      use_bs = 0;
    }
    else {
      dum = N/2;
      while (dum/3*3 == dum) {
        s_r3++;
        dum /= 3;
      }
      while (dum/5*5 == dum) {
        s_r5++;
        dum /= 5;
      }
      if ((dum >= 4) && (dum == (1 << (30-fftw_norm(dum))))) {
        /* mixed radix 2 and 3 */
        N_p2 = dum;
        use_bs = 0;
        M = N;
        j  = fftw_norm(dum);
      }
    }
  }

  /* calculate radix for radix 2 or 4 c2r fft */
  if (j%2 == use_bs)
    rad = 4;
  else
    rad = 2;

  fft_para->N_p2   = N_p2;
  fft_para->s_r3   = s_r3;
  fft_para->s_r5   = s_r5;
  fft_para->M      = M;
  fft_para->rad    = rad;
  fft_para->use_bs = use_bs;

} /* calculate_rad */

/* calculate twiddle size, Bn buffer size and workbuffer size */
static void calculate_mem_size_c2r (int N, fft_para_mix_bs_t *fft_para, int *twsize, int *Bnsize, int *worksize)
{
  int dum, n, i;
  int use_bs = fft_para->use_bs;
  int M = fft_para->M;
  int s_r3 = fft_para->s_r3;
  int s_r5 = fft_para->s_r5;
  int N_p2 = fft_para->N_p2;

  /* Please note that Bn buffer and Workbuf are only used when doing Bluestein
   * meaning when use_bs == 1 */

  if (use_bs == 1)
  { /* when size is not power of 2 and not multiple of 3&5 */
    *twsize   = (2*M+2*N);
    *Bnsize   = (2*M);
    *worksize = (4*M);
  }
  else
  {
    if ((s_r3 == 0) && (s_r5 == 0)) {
      /* When signal is power of 2 */
      *twsize = N*3;
    }
    else {
      /* When signal is power of 3 or 5 */
      dum = 0;
      n   = 2*N;
      for (i = 0; i < s_r3; i++) {
        dum += n/3;
        n   /= 3;
      }
      n *= 2;  /* radix-5 has 8 twiddle vs radix-3 4 twiddles */
      for (i = 0; i < s_r5; i++) {
        dum += n/5;
        n   /= 5;
      }
      *twsize = (N/2+2*N_p2+dum);
    }
    *Bnsize   = 0;
    *worksize = 0;
  }
} /* calculate_mem_size */

/* ADD KERNEL: Add a function for each new FFT kernel you want to offload */

/* plan for both fft and ifft 1d dp c2c batch */
fftw_plan fftw_plan_many_dft(int rank, const int *n, int howmany,
                             fftw_complex *in, const int *inembed,
                             int istride, int idist,
                             fftw_complex *out, const int *onembed,
                             int ostride, int odist,
                             int sign, unsigned flags)

{
  const int PLAN_TYPE = TI_FFTW_FFT_1D_DP_C2C_BATCH;
  fft_plan_t *p;
  int twsize, Bnsize, worksize, localsize;
  int N = n[0];

  if (!_ti_fftw_init_done) _ti_fftw_acc_init();
    TI_FFTW_DEBUG_PRINT("%s(%d,%d,%d,0x%p,0x%p,%d,%d,%d,%d,%d,%d,%d,%d,%d) intercepted\n", _ti_fftw_ocl_plans[PLAN_TYPE].fftw_plan_name, rank, N, howmany, (void *)in, (void *)out, inembed[0], istride, idist, onembed[0], ostride, odist, sign, flags, PLAN_TYPE);

  int offload_type = _ti_fftw_should_offload_1d_dp_batch(PLAN_TYPE, n, howmany, sign, flags, rank, inembed, istride, idist, onembed, ostride, odist);
  if (offload_type) {
    p = (fft_plan_t *)_lib_extern_malloc(FFT_PLAN_T_SIZE);

    /* determine the rad */
    p->u.dp_1d_c2c_batch_e.batch = howmany;
    calculate_rad_c2c(N, &p->u.dp_1d_c2c_batch_e.para1);

    /* Calculate mem size for ECPY */
    calculate_mem_size_c2c(N, &p->u.dp_1d_c2c_batch_e.para1, &twsize, &Bnsize, &worksize);
    p->u.dp_1d_c2c_batch_e.para1.twsize = twsize;
    p->u.dp_1d_c2c_batch_e.para1.Bnsize = Bnsize;

    /* calculate local memory requirements */
    localsize = 1024 + sizeof(double)*8*N + sizeof(double)*(twsize + Bnsize + worksize);
    return (_ti_fftw_plan_offload_1d_batch(PLAN_TYPE, offload_type, N, howmany, sizeof(fftw_complex)*N*howmany, 
               sizeof(fftw_complex)*N*howmany, sizeof(double)*twsize, localsize, in, out, sign, flags, (void *)p));
  }
  else
    {
      TI_FFTW_DEBUG_PRINT("  Using FFTW3 for %s\n", _ti_fftw_ocl_plans[PLAN_TYPE].fftw_plan_name);
      fftw_plan rc;
      TI_FFTW_PROFILE_START();
#ifdef TI_FFTW_USE_DLSYM
      assert(_fptr_fftw_plan_many_dft);
      rc = _fptr_fftw_plan_many_dft(rank, n, howmany, in, inembed, istride, idist, out, onembed, ostride, odist, sign, flags);
#else
      rc = __real_fftw_plan_many_dft(rank, n, howmany, in, inembed, istride, idist, out, onembed, ostride, odist, sign, flags);
#endif
      TI_FFTW_PROFILE_REPORT("  fftw_plan_many_dft (ARM) took %8.2f us\n",(float) clock_diff);
      return rc;
    } 
}

/* plan for both fft r2c batch */
fftw_plan fftw_plan_many_dft_r2c (int rank, const int *n, int howmany,
                             double *in, const int *inembed,
                             int istride, int idist,
                             fftw_complex *out, const int *onembed,
                             int ostride, int odist,
                             unsigned flags)

{
  const int PLAN_TYPE = TI_FFTW_FFT_1D_DP_R2C_BATCH;
  fft_plan_t *p;
  int twsize, Bnsize, worksize, localsize;
  int N = n[0];
  int inplace = 0;

  if ((void*) in == (void*) out)
    inplace = 1;

  if (!_ti_fftw_init_done) _ti_fftw_acc_init();
    TI_FFTW_DEBUG_PRINT("%s(%d,%d,%d,0x%p,0x%p,%d,%d,%d,%d,%d,%d,%d,%d) intercepted\n", _ti_fftw_ocl_plans[PLAN_TYPE].fftw_plan_name, rank, N, howmany, (void *)in, (void *)out, inembed[0], istride, idist, onembed[0], ostride, odist, flags, PLAN_TYPE);

  int offload_type = _ti_fftw_should_offload_1d_dp_batch_c2r_r2c(PLAN_TYPE, n, howmany, FFTW_FORWARD, flags, rank, inembed, 
                                                                 istride, idist, onembed, ostride, odist, inplace);
  if (offload_type) {
    p = (fft_plan_t *)_lib_extern_malloc(FFT_PLAN_T_SIZE);

    /* determine the rad */
    p->u.dp_1d_r2c_batch_e.batch = howmany;
    calculate_rad_r2c(N, &p->u.dp_1d_r2c_batch_e.para1);

    /* Calculate mem size for ECPY */
    calculate_mem_size_r2c(N, &p->u.dp_1d_r2c_batch_e.para1, &twsize, &Bnsize, &worksize);
    p->u.dp_1d_r2c_batch_e.para1.twsize = twsize;
    p->u.dp_1d_r2c_batch_e.para1.Bnsize = Bnsize;

    /* calculate local memory requirements */
    localsize = 1024 + sizeof(double)*6*N + sizeof(double)*(twsize + Bnsize + worksize);
    return (_ti_fftw_plan_offload_1d_batch(PLAN_TYPE, offload_type, N, howmany, sizeof(double)*N*howmany, 
               sizeof(fftw_complex)*(N/2+1)*howmany, sizeof(double)*twsize, localsize, in, out, FFTW_FORWARD, flags, (void *)p));
  }
  else
    {
      TI_FFTW_DEBUG_PRINT("  Using FFTW3 for %s\n", _ti_fftw_ocl_plans[PLAN_TYPE].fftw_plan_name);
      fftw_plan rc;
      TI_FFTW_PROFILE_START();
#ifdef TI_FFTW_USE_DLSYM
      assert(_fptr_fftw_plan_many_dft_r2c);
      rc = _fptr_fftw_plan_many_dft_r2c(rank, n, howmany, in, inembed, istride, idist, out, onembed, ostride, odist, flags);
#else
      rc = __real_fftw_plan_many_dft_r2c(rank, n, howmany, in, inembed, istride, idist, out, onembed, ostride, odist, flags);
#endif
      TI_FFTW_PROFILE_REPORT("  fftw_plan_many_dft_r2c (ARM) took %8.2f us\n",(float) clock_diff);
      return rc;
    } 
}

/* plan for both fft c2r batch */
fftw_plan fftw_plan_many_dft_c2r (int rank, const int *n, int howmany,
                             fftw_complex *in, const int *inembed,
                             int istride, int idist,
                             double *out, const int *onembed,
                             int ostride, int odist,
                             unsigned flags)

{
  const int PLAN_TYPE = TI_FFTW_FFT_1D_DP_C2R_BATCH;
  fft_plan_t *p;
  int twsize, Bnsize, worksize, localsize;
  int N = n[0];
  int inplace = 0;

  if ((void*) in == (void*) out)
    inplace = 1;

  if (!_ti_fftw_init_done) _ti_fftw_acc_init();
    TI_FFTW_DEBUG_PRINT("%s(%d,%d,%d,0x%p,0x%p,%d,%d,%d,%d,%d,%d,%d,%d) intercepted\n", _ti_fftw_ocl_plans[PLAN_TYPE].fftw_plan_name, rank, N, howmany, (void *)in, (void *)out, inembed[0], istride, idist, onembed[0], ostride, odist, flags, PLAN_TYPE);

  int offload_type = _ti_fftw_should_offload_1d_dp_batch_c2r_r2c(PLAN_TYPE, n, howmany, FFTW_FORWARD, flags, rank, inembed, 
                                                                 istride, idist, onembed, ostride, odist, inplace);
  if (offload_type) {
    p = (fft_plan_t *)_lib_extern_malloc(FFT_PLAN_T_SIZE);

    /* determine the rad */
    p->u.dp_1d_c2r_batch_e.batch = howmany;
    calculate_rad_c2r(N, &p->u.dp_1d_c2r_batch_e.para1);

    /* Calculate mem size for ECPY */
    calculate_mem_size_c2r(N, &p->u.dp_1d_c2r_batch_e.para1, &twsize, &Bnsize, &worksize);
    p->u.dp_1d_c2r_batch_e.para1.twsize = twsize;
    p->u.dp_1d_c2r_batch_e.para1.Bnsize = Bnsize;

    /* calculate local memory requirements */
    localsize = 1024 + sizeof(double)*6*N + sizeof(double)*(twsize + Bnsize + worksize);
    return (_ti_fftw_plan_offload_1d_batch(PLAN_TYPE, offload_type, N, howmany, sizeof(fftw_complex)*(N/2+1)*howmany, 
               sizeof(double)*N*howmany, sizeof(double)*twsize, localsize, in, out, FFTW_BACKWARD, flags, (void *)p));
  }
  else
    {
      TI_FFTW_DEBUG_PRINT("  Using FFTW3 for %s\n", _ti_fftw_ocl_plans[PLAN_TYPE].fftw_plan_name);
      fftw_plan rc;
      TI_FFTW_PROFILE_START();
#ifdef TI_FFTW_USE_DLSYM
      assert(_fptr_fftw_plan_many_dft_c2r);
      rc = _fptr_fftw_plan_many_dft_c2r(rank, n, howmany, in, inembed, istride, idist, out, onembed, ostride, odist, flags);
#else
      rc = __real_fftw_plan_many_dft_c2r(rank, n, howmany, in, inembed, istride, idist, out, onembed, ostride, odist, flags);
#endif
      TI_FFTW_PROFILE_REPORT("  fftw_plan_many_dft_c2r (ARM) took %8.2f us\n",(float) clock_diff);
      return rc;
    } 
}

/* end of fftw_acc_plan_1d_batch.c */




