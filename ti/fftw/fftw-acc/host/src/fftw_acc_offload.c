/* ======================================================================= */
/*  TEXAS INSTRUMENTS, INC.                                                */
/*                                                                         */
/*  FFTLIB  FFT Library                                                    */
/*                                                                         */
/* Copyright (C) 2014 Texas Instruments Incorporated - http://www.ti.com/  */
/*                                                                         */
/*                                                                         */
/*  Redistribution and use in source and binary forms, with or without     */
/*  modification, are permitted provided that the following conditions     */
/*  are met:                                                               */
/*                                                                         */
/*    Redistributions of source code must retain the above copyright       */
/*    notice, this list of conditions and the following disclaimer.        */
/*                                                                         */
/*    Redistributions in binary form must reproduce the above copyright    */
/*    notice, this list of conditions and the following disclaimer in the  */
/*    documentation and/or other materials provided with the               */
/*    distribution.                                                        */
/*                                                                         */
/*    Neither the name of Texas Instruments Incorporated nor the names of  */
/*    its contributors may be used to endorse or promote products derived  */
/*    from this software without specific prior written permission.        */
/*                                                                         */
/*  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS    */
/*  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT      */
/*  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR  */
/*  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT   */
/*  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,  */
/*  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT       */
/*  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,  */
/*  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY  */
/*  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT    */
/*  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE  */
/*  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.   */
/*                                                                         */
/* ======================================================================= */
#include "fftw_acc.h"
#include "fftw_acc_globals.h"

#define SOC_DBG
/* ADD KERNEL: For each FFTW type that you intercept add a function */ 

/* Add logic in the function below to decide whether to offload a kernel*/
/* Return values:
 * 0 => No offload
 * TI_FFTW_OFFLOAD_DIRECT, TI_FFTW_OFFLOAD_ECPY
 */
int _ti_fftw_should_offload_1d_dp(int type, int size, int sign, unsigned flags)
{
  int dum = size, j, dum1;

#ifdef SOC_DBG
    return 0;
#endif

  if (TI_FFTW_OFFLOAD == TI_FFTW_OFFLOAD_FORCE_ARM) 
    return 0;
 
  if (size <= 0) {
    TI_FFTW_DEBUG_PRINT("  Size must be positive.\n");
    return 0;
  }

  if (size > TI_FFTW_MAX_OFFLOAD_SIZE_1D_DP) {
    TI_FFTW_DEBUG_PRINT("  FFT size > %d. Skipping offload to DSP\n", TI_FFTW_MAX_OFFLOAD_SIZE_1D_DP);
    return 0;
  }

  switch (type) {
  case TI_FFTW_FFT_1D_DP_C2C:
    while (dum/3*3 == dum) {
      dum /= 3;
    }

    while (dum/5*5 == dum) {
      dum /= 5;
    }
  
    dum1 = dum >> 1;
    j    = 0;
    while (dum1 > 0) {
      dum1 = dum1 >> 1;
      j++;
    }

    if ((dum < 32) || (dum != (1 << j))) { 
      TI_FFTW_DEBUG_PRINT("  Unsupported size, kernel type %d. Skipping offload to DSP\n",type);
      return 0;
    }

#ifdef TI_FFTW_ALLOW_ECPY
    if (TI_FFTW_OFFLOAD == TI_FFTW_OFFLOAD_FORCE_DSP) return TI_FFTW_OFFLOAD_ECPY;
#endif

    if (size < TI_FFTW_MIN_OFFLOAD_SIZE_1D_DP) {
      TI_FFTW_DEBUG_PRINT("  FFT size < %d. Skipping offload to DSP\n", TI_FFTW_MIN_OFFLOAD_SIZE_1D_DP);
      return 0;
    }

    break;

    case TI_FFTW_FFT_1D_DP_R2C:
    while (dum/3*3 == dum) {
      dum /= 3;
    }

    while (dum/5*5 == dum) {
      dum /= 5;
    }
  
    dum1 = dum >> 1;
    j    = 0;
    while (dum1 > 0) {
      dum1 = dum1 >> 1;
      j++;
    }

    if ((dum < 64) || (dum != (1 << j))) {
      TI_FFTW_DEBUG_PRINT("  Unsupported size, kernel type %d. Skipping offload to DSP\n",type);
      return 0;
    }

#ifdef TI_FFTW_ALLOW_ECPY
    if (TI_FFTW_OFFLOAD == TI_FFTW_OFFLOAD_FORCE_DSP) return TI_FFTW_OFFLOAD_ECPY;
#endif

    if (size < TI_FFTW_MIN_OFFLOAD_SIZE_1D_DP_R2C) {
      TI_FFTW_DEBUG_PRINT("  FFT size < %d. Skipping offload to DSP\n", TI_FFTW_MIN_OFFLOAD_SIZE_1D_DP);
      return 0;
    }

    break;
    
  case TI_FFTW_FFT_1D_DP_C2R:

    while (dum/3*3 == dum) {
      dum /= 3;
    }

    while (dum/5*5 == dum) {
      dum /= 5;
    }
  
    dum1 = dum >> 1;
    j    = 0;
    while (dum1 > 0) {
      dum1 = dum1 >> 1;
      j++;
    }

    if ((dum < 64) || (dum != (1 << j))) {
      TI_FFTW_DEBUG_PRINT("  Unsupported size, kernel type %d. Skipping offload to DSP\n",type);
      return 0;
    }

#ifdef TI_FFTW_ALLOW_ECPY
    if (TI_FFTW_OFFLOAD == TI_FFTW_OFFLOAD_FORCE_DSP) return TI_FFTW_OFFLOAD_ECPY;
#endif

    if (size < TI_FFTW_MIN_OFFLOAD_SIZE_1D_DP_C2R) {
      TI_FFTW_DEBUG_PRINT("  FFT size < %d. Skipping offload to DSP\n", TI_FFTW_MIN_OFFLOAD_SIZE_1D_DP);
      return 0;
    }
    break;
  default:
    TI_FFTW_DEBUG_PRINT("  Unsupported kernel type %d. Skipping offload to DSP\n",type);
    return 0;
    break;
  }

#ifdef TI_FFTW_ALLOW_ECPY
    TI_FFTW_DEBUG_PRINT("  Offload to DSP ECPY\n");
    return TI_FFTW_OFFLOAD_ECPY;
#endif

  TI_FFTW_DEBUG_PRINT("  offload returns 0. Execute on ARM.\n");
  return 0;
}

/* 2d dp offload decision */
int _ti_fftw_should_offload_2d_dp(int type, int n1, int n2, int sign, unsigned flags)
{
  int dum, j, dum1;

#ifdef SOC_DBG
    return 0;
#endif

  if (TI_FFTW_OFFLOAD == TI_FFTW_OFFLOAD_FORCE_ARM) 
    return 0;
 
  if (n1 <= 0 || n2 <= 0) {
    TI_FFTW_DEBUG_PRINT("  Size must be positive.\n");
    return 0;
  }

  /* if not supporting anysize, only 2, 3, 4 and 5 are supported */
  dum = n1;
  while (dum/3*3 == dum) {
    dum /= 3;
  }

  while (dum/5*5 == dum) {
    dum /= 5;
  }
  j = 30 - fftw_norm (dum);

  if (dum != (1 << j)) {
    TI_FFTW_DEBUG_PRINT("  Unsupported n1 size, kernel type %d. Skipping offload to DSP\n",type);
    return 0;
  }

  dum = n2;
  while (dum/3*3 == dum) {
    dum /= 3;
  }

  while (dum/5*5 == dum) {
    dum /= 5;
  }
  j = 30 - fftw_norm (dum);
  if (dum != (1 << j)) {
    TI_FFTW_DEBUG_PRINT("  Unsupported n2 size, kernel type %d. Skipping offload to DSP\n",type);
    return 0;
  }

  if ((n1>TI_FFTW_MAX_OFFLOAD_SIZE_2D_DP) || (n2>TI_FFTW_MAX_OFFLOAD_SIZE_2D_DP)) {
    TI_FFTW_DEBUG_PRINT("  Unsupported n1*n2 size, kernel type %d. Skipping offload to DSP\n",type);
    return 0;
  }

  switch (type) {
  case TI_FFTW_FFT_2D_DP_C2C:
    /* check if the size of either dimension is not a multiple of 32 */
    dum = n1 >> 5;
    dum1 = n2 >> 5;
    if (((dum << 5) != n1) || ((dum1 << 5) != n2)){
      TI_FFTW_DEBUG_PRINT("  Unsupported n1*n2 size, kernel type %d. Skipping offload to DSP\n",type);
      return 0;
    }

#ifdef TI_FFTW_ALLOW_ECPY
    if (TI_FFTW_OFFLOAD == TI_FFTW_OFFLOAD_FORCE_DSP) return TI_FFTW_OFFLOAD_ECPY;
#endif

    if (n1*n2<TI_FFTW_MIN_OFFLOAD_SIZE_2D_DP*TI_FFTW_MIN_OFFLOAD_SIZE_2D_DP) {
      TI_FFTW_DEBUG_PRINT("  Unsupported n1*n2 size, kernel type %d. Skipping offload to DSP\n",type);
      return 0;
    }
    break;

  case TI_FFTW_FFT_2D_DP_R2C:
  case TI_FFTW_FFT_2D_DP_C2R:
    /* check if the size of either dimension is not a multiple of 32 */
    dum = n1 >> 5;
    dum1 = n2 >> 6;
    if (((dum << 5) != n1) || ((dum1 << 6) != n2)){
      TI_FFTW_DEBUG_PRINT("  Unsupported n1*n2 size, kernel type %d. Skipping offload to DSP\n",type);
      return 0;
    }

#ifdef TI_FFTW_ALLOW_ECPY
    if (TI_FFTW_OFFLOAD == TI_FFTW_OFFLOAD_FORCE_DSP) return TI_FFTW_OFFLOAD_ECPY;
#endif

    if (n1*n2<TI_FFTW_MIN_OFFLOAD_SIZE_2D_DP_R*TI_FFTW_MIN_OFFLOAD_SIZE_2D_DP_R) {
      TI_FFTW_DEBUG_PRINT("  Unsupported n1*n2 size, kernel type %d. Skipping offload to DSP\n",type);
      return 0;
    }
    break;
 default:
    TI_FFTW_DEBUG_PRINT("  Unsupported kernel type %d. Skipping offload to DSP\n",type);
    return 0;
    break;
  }

#ifdef TI_FFTW_ALLOW_ECPY
  TI_FFTW_DEBUG_PRINT("  Offload to DSP ECPY\n");
  return TI_FFTW_OFFLOAD_ECPY;
#endif

  TI_FFTW_DEBUG_PRINT("  offload returns 0. Execute on ARM.\n");
  return 0;
}
/* end of 2d dp */

/* 3d dp offload decision */
int _ti_fftw_should_offload_3d_dp(int type, int n1, int n2, int n3, int sign, unsigned flags)
{
  int dum, j, dum1, dum2;

#ifdef SOC_DBG
    return 0;
#endif

  if (TI_FFTW_OFFLOAD == TI_FFTW_OFFLOAD_FORCE_ARM) 
    return 0;
 
  if (n1 <= 0 || n2 <= 0 || n3 <= 0) {
    TI_FFTW_DEBUG_PRINT("  Size must be positive.\n");
    return 0;
  }

  /* check if the size of either dimension is not a multiple of 32 */
  dum = n1 >> 5;
  dum1 = n2 >> 5;
  dum2 = n3 >> 5;

  if (((dum << 5) != n1) || ((dum1 << 5) != n2)  || ((dum2 << 5) != n3)){
    TI_FFTW_DEBUG_PRINT("  Unsupported n1, n2 or n3 size, kernel type %d. Skipping offload to DSP\n",type);
    return 0;
  }

  /* if not supporting anysize, only Radix 2, 3, 4 and 5 are supported */
#if TI_FFTW_ANYSIZE_SUPPORT==0
  dum = n1;
  while (dum/3*3 == dum) {
    dum /= 3;
  }

  while (dum/5*5 == dum) {
    dum /= 5;
  }
  j = 30 - fftw_norm (dum);

  if (dum != (1 << j)) {
	TI_FFTW_DEBUG_PRINT("  Unsupported n1 size, kernel type %d. Skipping offload to DSP\n",type);
    return 0;
  }

  dum = n2;
  while (dum/3*3 == dum) {
    dum /= 3;
  }

  while (dum/5*5 == dum) {
    dum /= 5;
  }
  j = 30 - fftw_norm (dum);
  if (dum != (1 << j)) {
	TI_FFTW_DEBUG_PRINT("  Unsupported n2 size, kernel type %d. Skipping offload to DSP\n",type);
    return 0;
  }

  dum = n3;
  while (dum/3*3 == dum) {
    dum /= 3;
  }

  while (dum/5*5 == dum) {
    dum /= 5;
  }
  j = 30 - fftw_norm (dum);
  if (dum != (1 << j)) {
	TI_FFTW_DEBUG_PRINT("  Unsupported n3 size, kernel type %d. Skipping offload to DSP\n",type);
    return 0;
  }
#endif

  if ((n1 > TI_FFTW_MAX_OFFLOAD_SIZE_3D_DP) 
   || (n2 > TI_FFTW_MAX_OFFLOAD_SIZE_3D_DP)
   || (n3 > TI_FFTW_MAX_OFFLOAD_SIZE_3D_DP)) {
    TI_FFTW_DEBUG_PRINT("  Exceeds maximum size, kernel type %d. Skipping offload to DSP\n",type);
    return 0;
  }

  switch (type) {
  case TI_FFTW_FFT_3D_DP_C2C:

#ifdef TI_FFTW_ALLOW_ECPY
    if (TI_FFTW_OFFLOAD == TI_FFTW_OFFLOAD_FORCE_DSP) return TI_FFTW_OFFLOAD_ECPY;
#endif

    if (n1*n2*n3 < TI_FFTW_MIN_OFFLOAD_SIZE_3D_DP * TI_FFTW_MIN_OFFLOAD_SIZE_3D_DP * TI_FFTW_MIN_OFFLOAD_SIZE_3D_DP) {
      TI_FFTW_DEBUG_PRINT("  Unsupported n1*n2*n3 size, kernel type %d. Skipping offload to DSP\n",type);
      return 0;
    }
    break;
  case TI_FFTW_FFT_3D_DP_R2C:
  case TI_FFTW_FFT_3D_DP_C2R:

    dum = n3 >> 6;
    if ((dum << 6) != n3) {
      TI_FFTW_DEBUG_PRINT("  Unsupported n3 size, kernel type %d. Skipping offload to DSP\n",type);
      return 0;
    }
#ifdef TI_FFTW_ALLOW_ECPY
    if (TI_FFTW_OFFLOAD == TI_FFTW_OFFLOAD_FORCE_DSP) return TI_FFTW_OFFLOAD_ECPY;
#endif

    if (n1*n2*n3 < TI_FFTW_MIN_OFFLOAD_SIZE_3D_DP * TI_FFTW_MIN_OFFLOAD_SIZE_3D_DP * TI_FFTW_MIN_OFFLOAD_SIZE_3D_DP) {
      TI_FFTW_DEBUG_PRINT("  Unsupported n1*n2*n3 size, kernel type %d. Skipping offload to DSP\n",type);
	  return 0;
    }
    break;
      
 default:
    TI_FFTW_DEBUG_PRINT("  Unsupported kernel type %d. Skipping offload to DSP\n",type);
    return 0;
    break;
  }

#ifdef TI_FFTW_ALLOW_ECPY
  TI_FFTW_DEBUG_PRINT("  Offload to DSP ECPY\n");
  return TI_FFTW_OFFLOAD_ECPY;
#endif

  TI_FFTW_DEBUG_PRINT("  offload returns 0. Execute on ARM.\n");
  return 0;
}
/* end of 3d dp */

/* 1d dp batched offload decision */
int _ti_fftw_should_offload_1d_dp_batch(int type, const int *n, int batch, int sign, unsigned flags, int rank, 
                                        const int *inembed, int istride, int idist, const int *onembed,
                                        int ostride, int odist)
{
  int dum, j, dum1;
  int N = n[0];

#ifdef SOC_DBG
    return 0;
#endif

  if (TI_FFTW_OFFLOAD == TI_FFTW_OFFLOAD_FORCE_ARM) 
    return 0;
 
  if (N <= 0) {
    TI_FFTW_DEBUG_PRINT("  Size must be positive.\n");
    return 0;
  }

  /* check if the size is at least 8 and batch is at least 8  */
  if ((N < 4) || (batch < 8)){
    TI_FFTW_DEBUG_PRINT("  Unsupported size, kernel type %d. N = %d, batch = %d, Skipping offload to DSP\n",type,N,batch);
    return 0;
  }

  /* check if it is 1D batched FFT */
  if (rank != 1){
    TI_FFTW_DEBUG_PRINT("  Only supports 1D batched FFT, kernel type %d. Skipping offload to DSP\n",type);
    return 0;
  }

  /* check if memory is consecutive */
  if ((istride != 1) || (ostride != 1)){
    TI_FFTW_DEBUG_PRINT("  Only supports consecutive memory batched FFT, kernel type %d. Skipping offload to DSP\n",type);
    return 0;
  }

  if ((idist != N) || (odist != N)){
    TI_FFTW_DEBUG_PRINT("  Only supports consecutive memory batched FFT, kernel type %d. Skipping offload to DSP\n",type);
    return 0;
  }

  if (!(((inembed == n) || (inembed == NULL)) && ((onembed == n) || (onembed == NULL)))){
    TI_FFTW_DEBUG_PRINT("  Only supports consecutive memory batched FFT, kernel type %d. Skipping offload to DSP\n",type);
    return 0;
  }

  /* if not supporting anysize, only 2, 3, 4 and 5 are supported */
#if TI_FFTW_ANYSIZE_SUPPORT==0
  dum = N;
  while (dum/3*3 == dum) {
    dum /= 3;
  }

  while (dum/5*5 == dum) {
    dum /= 5;
  }
  j = 30 - fftw_norm (dum);

  if ((dum != (1 << j)) || (dum < 4)) {
    TI_FFTW_DEBUG_PRINT("  Unsupported n size, kernel type %d. N_p2 = %d, Skipping offload to DSP\n",type,dum);
    return 0;
  }
#endif

  if (N>TI_FFTW_MAX_OFFLOAD_1D_DP_BATCH) {
    TI_FFTW_DEBUG_PRINT("  n size exceeds limits, kernel type %d. Skipping offload to DSP\n",type);
    return 0;
  }

#ifdef TI_FFTW_ALLOW_ECPY
  if (TI_FFTW_OFFLOAD == TI_FFTW_OFFLOAD_FORCE_DSP) return TI_FFTW_OFFLOAD_ECPY;
#endif

  switch (type) {
  case TI_FFTW_FFT_1D_DP_C2C_BATCH:

    if (N<8 || N*sqrt(batch)<TI_FFTW_MIN_OFFLOAD_1D_DP_BATCH) {
      TI_FFTW_DEBUG_PRINT("  n size lower than limits, kernel type %d. Skipping offload to DSP\n",type);
      return 0;
    }
    break;
  default:
    TI_FFTW_DEBUG_PRINT("  Unsupported kernel type %d. Skipping offload to DSP\n",type);
    return 0;
    break;
  }

#ifdef TI_FFTW_ALLOW_ECPY
    TI_FFTW_DEBUG_PRINT("  Offload to DSP ECPY\n");
    return TI_FFTW_OFFLOAD_ECPY;
#endif

  TI_FFTW_DEBUG_PRINT("  offload returns 0. Execute on ARM.\n");
  return 0;
}

int _ti_fftw_should_offload_1d_dp_batch_c2r_r2c(int type, const int *n, int batch, int sign, unsigned flags, int rank, 
                                        const int *inembed, int istride, int idist, const int *onembed,
                                        int ostride, int odist, int inplace)
{
  int dum, j, dum1;
  int N = n[0];

#ifdef SOC_DBG
    return 0;
#endif

  if (TI_FFTW_OFFLOAD == TI_FFTW_OFFLOAD_FORCE_ARM) 
    return 0;

  /* inplace data format is not supported for c2r or r2c */
  if (inplace) 
    return 0;
 
  /* check if the size is at least 8 and batch is at least 8  */
  if ((N < 8) || (batch < 8)){
    TI_FFTW_DEBUG_PRINT("  Unsupported size, kernel type %d. N = %d, batch = %d, Skipping offload to DSP\n",type,N,batch);
    return 0;
  }

  /* check if it is 1D batched FFT */
  if (rank != 1){
    TI_FFTW_DEBUG_PRINT("  Only supports 1D batched FFT, kernel type %d. Skipping offload to DSP\n",type);
    return 0;
  }

  /* check if memory is consecutive */
  if ((istride != 1) || (ostride != 1)){
    TI_FFTW_DEBUG_PRINT("  Only supports consecutive memory batched FFT, kernel type %d. Skipping offload to DSP\n",type);
    return 0;
  }

  if (!(((inembed == n) || (inembed == NULL)) && ((onembed == n) || (onembed == NULL)))){
    TI_FFTW_DEBUG_PRINT("  Only supports consecutive memory batched FFT, kernel type %d. Skipping offload to DSP\n",type);
    return 0;
  }

  /* if not supporting anysize, only 2, 3, 4 and 5 are supported */
#if TI_FFTW_ANYSIZE_SUPPORT==0
  dum = N;
  while (dum/3*3 == dum) {
    dum /= 3;
  }

  while (dum/5*5 == dum) {
    dum /= 5;
  }
  j = 30 - fftw_norm (dum);

  if ((dum != (1 << j)) || (dum < 8)) {
    TI_FFTW_DEBUG_PRINT("  Unsupported n size, kernel type %d. N_p2 = %d, Skipping offload to DSP\n",type,dum);
    return 0;
  }
#endif

  if (N>TI_FFTW_MAX_OFFLOAD_1D_DP_BATCH) {
    TI_FFTW_DEBUG_PRINT("  n size exceeds limits, kernel type %d. Skipping offload to DSP\n",type);
    return 0;
  }

  switch (type) {
  case TI_FFTW_FFT_1D_DP_R2C_BATCH:
    if ((idist != N) || (odist != (N/2+1))){
      TI_FFTW_DEBUG_PRINT("  Only supports consecutive memory batched FFT, kernel type %d. Skipping offload to DSP\n",type);
      return 0;
    }       
    break;

  case TI_FFTW_FFT_1D_DP_C2R_BATCH:
    if ((odist != N) || (idist != (N/2+1))){
      TI_FFTW_DEBUG_PRINT("  Only supports consecutive memory batched FFT, kernel type %d. Skipping offload to DSP\n",type);
      return 0;
    }       
    break;

  default:
    TI_FFTW_DEBUG_PRINT("  Unsupported kernel type %d. Skipping offload to DSP\n",type);
    return 0;
    break;
  }

#ifdef TI_FFTW_ALLOW_ECPY
  if (TI_FFTW_OFFLOAD == TI_FFTW_OFFLOAD_FORCE_DSP) return TI_FFTW_OFFLOAD_ECPY;
#endif

  if (N<16 || N*sqrt(batch)<TI_FFTW_MIN_OFFLOAD_1D_DP_BATCH_R) {
    TI_FFTW_DEBUG_PRINT("  n size lower than limits, kernel type %d. Skipping offload to DSP\n",type);
    return 0;
  }

#ifdef TI_FFTW_ALLOW_ECPY
  TI_FFTW_DEBUG_PRINT("  Offload to DSP ECPY\n");
  return TI_FFTW_OFFLOAD_ECPY;
#endif

  TI_FFTW_DEBUG_PRINT("  offload returns 0. Execute on ARM.\n");
  return 0;
}
/* end of 1d dp batch */
/* end of fftw_acc_offload.c */
