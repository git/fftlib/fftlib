/* ======================================================================= */
/*  TEXAS INSTRUMENTS, INC.                                                */
/*                                                                         */
/*  FFTLIB  FFT Library                                                    */
/*                                                                         */
/* Copyright (C) 2014 Texas Instruments Incorporated - http://www.ti.com/  */
/*                                                                         */
/*                                                                         */
/*  Redistribution and use in source and binary forms, with or without     */
/*  modification, are permitted provided that the following conditions     */
/*  are met:                                                               */
/*                                                                         */
/*    Redistributions of source code must retain the above copyright       */
/*    notice, this list of conditions and the following disclaimer.        */
/*                                                                         */
/*    Redistributions in binary form must reproduce the above copyright    */
/*    notice, this list of conditions and the following disclaimer in the  */
/*    documentation and/or other materials provided with the               */
/*    distribution.                                                        */
/*                                                                         */
/*    Neither the name of Texas Instruments Incorporated nor the names of  */
/*    its contributors may be used to endorse or promote products derived  */
/*    from this software without specific prior written permission.        */
/*                                                                         */
/*  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS    */
/*  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT      */
/*  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR  */
/*  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT   */
/*  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,  */
/*  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT       */
/*  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,  */
/*  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY  */
/*  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT    */
/*  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE  */
/*  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.   */
/*                                                                         */
/* ======================================================================= */
#ifndef _TI_FFTW_ACC_H
#define _TI_FFTW_ACC_H

#include <unistd.h>
#include <stdlib.h>
#include <time.h>
#include <fftw3.h>
#include <math.h>
#include <assert.h>
#include <dlfcn.h>
#include <omp.h>
#include <memory.h>

#include <CL/cl.h>
#include <CL/cl_ext.h>

#include "ti/fftlib/src/common/fft_common.h"
#include "utlist.h"

/* define memory management functions */
#define _lib_extern_malloc  __malloc_ddr
#define _lib_extern_free 	  __free_ddr

#define FFT_PLAN_T_SIZE (sizeof(fft_plan_t))
#define FFT_EDMA_CH_BUFSIZE      256*6
static inline int fftw_norm ( int n)
{
  int dum1, j;

  dum1 = n >> 1;
  j    = 0;
  while (dum1 > 0) {
    dum1 = dum1 >> 1;
	j++;
  }
  j = 30 - j;
  return j;
}
#ifdef TI_FFTW_DEBUG
extern volatile int _ti_fftw_DISABLE_DEBUG;
#define TI_FFTW_DEBUG_PRINT(...) if (!_ti_fftw_DISABLE_DEBUG) { fprintf(stderr,"fftw_acc: Debug, "); fprintf(stdout, __VA_ARGS__); }
#else
#define TI_FFTW_DEBUG_PRINT(...)
#endif

#ifdef TI_FFTW_PROFILE
#define TI_FFTW_CLOCK CLOCK_REALTIME
#define clock_diff ((clock2.tv_sec-clock1.tv_sec)*1e6 + (clock2.tv_nsec-clock1.tv_nsec)/1e3)
#define TI_FFTW_PROFILE_PRINT(...) fprintf(stdout,"fftw_acc: Profile, "); fprintf(stdout, __VA_ARGS__);
#define TI_FFTW_PROFILE_START() struct timespec clock1; clock_gettime(TI_FFTW_CLOCK, &clock1);
#define TI_FFTW_PROFILE_REPORT(...) struct timespec clock2; clock_gettime(TI_FFTW_CLOCK, &clock2); TI_FFTW_PROFILE_PRINT(__VA_ARGS__)

#define TI_FFTW_PROFILE_OCL_REPORT()
#if 0
#define TI_FFTW_PROFILE_OCL_REPORT() { cl_ulong t_strt, t_end; float t_diff; clGetEventProfilingInfo(e, CL_PROFILING_COMMAND_START, sizeof(t_strt), &t_strt, NULL); clGetEventProfilingInfo(e, CL_PROFILING_COMMAND_END, sizeof(t_end), &t_end, NULL); t_diff = t_end-t_strt; TI_FFTW_PROFILE_PRINT("  CL profiling says that took %8.2f us\n",t_diff/1000.0); }
#endif

#else /* TI_FFTW_PROFILE */
#define TI_FFTW_PROFILE_PRINT(...)
#define TI_FFTW_PROFILE_START()
#define TI_FFTW_PROFILE_REPORT(...)
#define TI_FFTW_PROFILE_OCL_REPORT()
#endif /* TI_FFTW_PROFILE */

/* Constants used when user forces something */
#define TI_FFTW_OFFLOAD_FORCE_ARM  0
#define TI_FFTW_OFFLOAD_FORCE_DSP  1
#define TI_FFTW_OFFLOAD_FORCE_NONE 2

/* Thse values are returned by the function that decides
 * whether to offload or not, and if so to where*/
#define TI_FFTW_OFFLOAD_ECPY 1
#define TI_FFTW_OFFLOAD_DIRECT 2

/* These sizes need to be refined */
/* ADD KERNEL: For each FFTW type that you intercept add a new set of limits */ 
#ifdef SOC_K2H
#define TI_FFTW_MIN_OFFLOAD_SIZE_1D_DP     8192 
#define TI_FFTW_MIN_OFFLOAD_SIZE_1D_DP_R2C 14000 
#define TI_FFTW_MIN_OFFLOAD_SIZE_1D_DP_C2R 20000
#define TI_FFTW_MAX_OFFLOAD_SIZE_1D_DP     1024*1024  /* 1M points */
#define TI_FFTW_MIN_OFFLOAD_SIZE_2D_DP     96 
#define TI_FFTW_MIN_OFFLOAD_SIZE_2D_DP_R   160 
#if ANYSIZE_SUPPORT
#define TI_FFTW_MAX_OFFLOAD_SIZE_2D_DP     512        /* 512*512 when anysize is supported */
#else
#define TI_FFTW_MAX_OFFLOAD_SIZE_2D_DP     1024       /* 1024*1024 when anysize is not supported */
#endif
#define TI_FFTW_MIN_OFFLOAD_SIZE_3D_DP     32         /* 32*32*32 */
#define TI_FFTW_MAX_OFFLOAD_SIZE_3D_DP     192        /* 128*128*128 */
#define TI_FFTW_MIN_OFFLOAD_1D_DP_BATCH    1300 
#define TI_FFTW_MIN_OFFLOAD_1D_DP_BATCH_R  2100 
#define TI_FFTW_MAX_OFFLOAD_1D_DP_BATCH    8192 
#else /* SOC_AM572x */
#define TI_FFTW_MIN_OFFLOAD_SIZE_1D_DP     8192 
#define TI_FFTW_MIN_OFFLOAD_SIZE_1D_DP_R2C 14000 
#define TI_FFTW_MIN_OFFLOAD_SIZE_1D_DP_C2R 20000
#define TI_FFTW_MAX_OFFLOAD_SIZE_1D_DP     1024*1024  /* 1M points */
#define TI_FFTW_MIN_OFFLOAD_SIZE_2D_DP     96 
#define TI_FFTW_MIN_OFFLOAD_SIZE_2D_DP_R   160 
#if ANYSIZE_SUPPORT
#define TI_FFTW_MAX_OFFLOAD_SIZE_2D_DP     512        /* 512*512 when anysize is supported */
#else
#define TI_FFTW_MAX_OFFLOAD_SIZE_2D_DP     1024       /* 1024*1024 when anysize is not supported */
#endif
#define TI_FFTW_MIN_OFFLOAD_SIZE_3D_DP     32         /* 32*32*32 */
#define TI_FFTW_MAX_OFFLOAD_SIZE_3D_DP     192        /* 128*128*128 */
#define TI_FFTW_MIN_OFFLOAD_1D_DP_BATCH    1300 
#define TI_FFTW_MIN_OFFLOAD_1D_DP_BATCH_R  2100 
#define TI_FFTW_MAX_OFFLOAD_1D_DP_BATCH    8192 
#endif 

#define TI_FFTW_MAX_CMEM_MALLOC            1024*1024*1024 /* 1GB limit */

#define TI_FFTW_ERROR_PRINT(...) fprintf(stderr,"fftw_acc: Error, "); fprintf(stderr, __VA_ARGS__); fprintf(stderr,"\n"); 

#define TI_FFTW_OCL_CHKERROR(A, B) if (B != CL_SUCCESS) { TI_FFTW_ERROR_PRINT("opencl %s, error %d", A, B); }

/* Structure used to implement a set using utlist.h */
typedef struct element {
    void *item;
    struct element *prev; /* needed for a doubly-linked list only */
    struct element *next; /* needed for singly- or doubly-linked lists */
} element_t;

/* These MUST be maintained on a per-plan basis */
typedef struct {
  void *in;
  void *out;
  void *p;
  int sign;
  int insize;
  int outsize;
  int offload_type; /* TI_FFTW_OFFLOAD_ECPY or TI_FFTW_OFFLOAD_DIRECT */
  int copy_input;   /* this is to be removed after temp buffer allocated in DSP for all kernels */
  cl_mem bufX;
  cl_mem bufY;
  cl_mem bufW;
  cl_mem bufPlan;
  cl_kernel FFT_plan;
  cl_kernel FFT_execute;
  cl_kernel FFT_destroy_plan;
} _ti_fftw_plan_info_t;

typedef struct {
#ifdef TI_FFTW_ALLOW_ECPY
  cl_kernel ecpy;
  const char *ecpy_name;
#endif
#ifdef TI_FFTW_ALLOW_DIRECT
  cl_kernel direct;
  const char *direct_name;
#endif
  const char *fftw_plan_name;
} _ti_fftw_ocl_plan_kernel_t;

/* Internal function prototypes */
void _ti_fftw_acc_init(void); /* initialization function */
void _ti_fftw_acc_fini(void); /* finalization function */

/* ADD KERNEL: For each FFTW type that you intercept add a new function */ 
int _ti_fftw_should_offload_1d_dp(int type, int size, int sign, unsigned flags);
int _ti_fftw_should_offload_2d_dp(int type, int n1, int n2, int sign, unsigned flags);
int _ti_fftw_should_offload_1d_dp_batch(int type, const int *n, int batch, int sign, unsigned flags, int rank, 
                                        const int *inembed, int istride, int idist, const int *onembed,
                                        int ostride, int odist);
int _ti_fftw_should_offload_1d_dp_batch_c2r_r2c(int type, const int *n, int batch, int sign, unsigned flags, int rank, 
                                        const int *inembed, int istride, int idist, const int *onembed,
                                        int ostride, int odist, int inplace);
int _ti_fftw_should_offload_3d_dp(int type,int n1, int n2, int n3, int sign,unsigned flags);

/* ADD KERNEL: For each FFTW type that you intercept add a new function */ 
fftw_plan _ti_fftw_plan_offload_1d(const int plan_type, const int offload_type, int n,
                                   int insize, int outsize, int twsize, int localsize,
                                   void *in, void *out, int sign, unsigned flags, void *p);
fftw_plan _ti_fftw_plan_offload_2d(const int plan_type, const int offload_type, int n1, int n2,
                                   int insize, int outsize, int twsize, int localsize,
                                   void *in, void *out, int sign, unsigned flags, void *p);
fftw_plan _ti_fftw_plan_offload_3d(const int plan_type, const int offload_type, int n1, int n2, int n3,
                                   int insize, int outsize, int twsize, int localsize,
                                   void *in, void *out, int sign, unsigned flags, void *p);
fftw_plan _ti_fftw_plan_offload_1d_batch(const int plan_type, const int offload_type, int n,
                                   int batch, int insize, int outsize, int twsize, int localsize,
                                   void *in, void *out, int sign, unsigned flags, void *p);

/* ADD KERNEL: Add a define corresponding to below for each FFTW plan offload */
/*             As the define is used to index into an array, you should */
/*             increase it exactly by 1 for each new offload. */
/*             Increase TI_FFTW_FFT_PLAN_MAX by 1 correspondingly */

#define TI_FFTW_FFT_1D_DP_C2C 0
#define TI_FFTW_FFT_1D_DP_R2C 1
#define TI_FFTW_FFT_1D_DP_C2R 2
#define TI_FFTW_FFT_2D_DP_C2C 3
#define TI_FFTW_FFT_2D_DP_R2C 4
#define TI_FFTW_FFT_2D_DP_C2R 5
#define TI_FFTW_FFT_3D_DP_C2C 6
#define TI_FFTW_FFT_3D_DP_R2C 7
#define TI_FFTW_FFT_3D_DP_C2R 8
#define TI_FFTW_FFT_1D_DP_C2C_BATCH 9
#define TI_FFTW_FFT_1D_DP_R2C_BATCH 10
#define TI_FFTW_FFT_1D_DP_C2R_BATCH 11
#define TI_FFTW_FFT_PLAN_MAX  12

#endif /* _TI_FFTW_ACC_H */

/* end of fftw_acc.h */

