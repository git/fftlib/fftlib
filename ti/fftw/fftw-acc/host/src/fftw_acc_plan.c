/* ======================================================================= */
/*  TEXAS INSTRUMENTS, INC.                                                */
/*                                                                         */
/*  FFTLIB  FFT Library                                                    */
/*                                                                         */
/* Copyright (C) 2014 Texas Instruments Incorporated - http://www.ti.com/  */
/*                                                                         */
/*                                                                         */
/*  Redistribution and use in source and binary forms, with or without     */
/*  modification, are permitted provided that the following conditions     */
/*  are met:                                                               */
/*                                                                         */
/*    Redistributions of source code must retain the above copyright       */
/*    notice, this list of conditions and the following disclaimer.        */
/*                                                                         */
/*    Redistributions in binary form must reproduce the above copyright    */
/*    notice, this list of conditions and the following disclaimer in the  */
/*    documentation and/or other materials provided with the               */
/*    distribution.                                                        */
/*                                                                         */
/*    Neither the name of Texas Instruments Incorporated nor the names of  */
/*    its contributors may be used to endorse or promote products derived  */
/*    from this software without specific prior written permission.        */
/*                                                                         */
/*  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS    */
/*  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT      */
/*  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR  */
/*  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT   */
/*  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,  */
/*  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT       */
/*  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,  */
/*  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY  */
/*  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT    */
/*  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE  */
/*  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.   */
/*                                                                         */
/* ======================================================================= */
#include "fftw_acc.h"
#include "fftw_acc_globals.h"

/* ADD KERNEL: For each FFTW type that you intercept add a new function */ 
fftw_plan _ti_fftw_plan_offload_1d(const int plan_type, const int offload_type, int n, 
                                   int insize, int outsize, int twsize, int localsize,
                                   void *in,void *out,int sign, unsigned flags, void* p)
{
  TI_FFTW_PROFILE_START();
  TI_FFTW_DEBUG_PRINT("  Offloading %s to DSP\n", _ti_fftw_ocl_plans[plan_type].fftw_plan_name);

  /* Allocate a new _ti_fftw_plan_info_t structure so we can track the details
   * for the subsequent execute */
  _ti_fftw_plan_info_t *plan = (_ti_fftw_plan_info_t *)malloc(sizeof(_ti_fftw_plan_info_t));
  if (plan == NULL) {
    TI_FFTW_ERROR_PRINT("Could not malloc a _ti_fftw_plan_info_t.\n");
    return NULL;
  }
  memset(plan,0x0,sizeof(*plan));

  /* First copy in and out pointers as we will need them in the
     the offloaded execute */
  plan->in = in;
  plan->out = out;
  plan->sign = sign;
  plan->p = p;

  /* this is to be removed after temp buffer allocated in DSP for c2c kernels */
  plan->copy_input = 0;
  if (plan_type == TI_FFTW_FFT_1D_DP_C2C)
    plan->copy_input = 1;

  /* Store some state */
  plan->insize  = insize;
  plan->outsize = outsize;
  plan->offload_type = offload_type;

  cl_int err;

  /* We pass the out buffer as the FFTLIB generates
   * its output in bufX. For IN-PLACE transforms, 
   * OUT == IN, and we don't need to do anything more.
   * For, OUT-OF-PLACE transforms, we mempcpy IN into
   * OUT in intercepted execute, before calling the
   * the offloaded execute. This also ensures the
   * the FFTW3 semantics for out-of-place transforms
   * that IN is not modified is maintained */
  if (plan->copy_input == 1)
     plan->bufX = clCreateBuffer(_ti_fftw_ocl_context, 
                 CL_MEM_READ_WRITE|CL_MEM_USE_HOST_PTR,
                 plan->insize, out, &err);
  else
     plan->bufX = clCreateBuffer(_ti_fftw_ocl_context, 
                 CL_MEM_READ_WRITE|CL_MEM_USE_HOST_PTR,
                 plan->insize, in, &err);

  TI_FFTW_OCL_CHKERROR("clCreateBuffer",err);


  /* bufY is a separate scratch space for the ECPY kernel. It's not copied
   * from in the execute. It's just scratch space */
  if (plan->copy_input == 1)
     plan->bufY = clCreateBuffer(_ti_fftw_ocl_context, 
                CL_MEM_READ_WRITE|CL_MEM_ALLOC_HOST_PTR, plan->insize, NULL, &err);
  else
     plan->bufY = clCreateBuffer(_ti_fftw_ocl_context, 
                CL_MEM_READ_WRITE|CL_MEM_USE_HOST_PTR, plan->outsize, out, &err);

  TI_FFTW_OCL_CHKERROR("clCreateBuffer",err);
  
  plan->bufW = clCreateBuffer(_ti_fftw_ocl_context, CL_MEM_READ_WRITE, twsize, NULL, &err);
  TI_FFTW_OCL_CHKERROR("clCreateBuffer",err);
  plan->bufPlan = clCreateBuffer(_ti_fftw_ocl_context, CL_MEM_READ_WRITE|CL_MEM_USE_HOST_PTR, FFT_PLAN_T_SIZE, p, &err);
  TI_FFTW_OCL_CHKERROR("clCreateBuffer",err);


  if ((plan->bufW == NULL) || (plan->bufY == NULL) ||
      (plan->bufX == NULL) || (plan->bufPlan == NULL)) {
    cl_int err = CL_SUCCESS;
    TI_FFTW_ERROR_PRINT("Could not malloc an OCL Buffer, returning NULL plan.\n");
    if (plan->bufPlan) { err = clReleaseMemObject(plan->bufPlan);
      TI_FFTW_OCL_CHKERROR("clReleaseMemObject",err); }
    if (plan->bufW) { err = clReleaseMemObject(plan->bufW);
      TI_FFTW_OCL_CHKERROR("clReleaseMemObject",err); }
    if (plan->bufY) { err = clReleaseMemObject(plan->bufY);
      TI_FFTW_OCL_CHKERROR("clReleaseMemObject",err); }
    if (plan->bufX) { err = clReleaseMemObject(plan->bufX);
      TI_FFTW_OCL_CHKERROR("clReleaseMemObject",err); }
    memset(plan,0x0,sizeof(*plan));
    free(plan);
    return NULL;
  }
  
  cl_event e;
  {
#ifdef TI_FFTW_ALLOW_DIRECT
  if (offload_type == TI_FFTW_OFFLOAD_DIRECT) {
    memcpy(&plan->FFT_plan,&_ti_fftw_ocl_plans[plan_type].direct,sizeof(cl_kernel));
    TI_FFTW_DEBUG_PRINT("  Using DIRECT for plan type %d\n", plan_type);
  }
#endif
#ifdef TI_FFTW_ALLOW_ECPY
  if (offload_type == TI_FFTW_OFFLOAD_ECPY) {
    memcpy(&plan->FFT_plan,&_ti_fftw_ocl_plans[plan_type].ecpy,sizeof(cl_kernel));
    TI_FFTW_DEBUG_PRINT("  Using ECPY for plan type %d\n", plan_type);
  }
#endif

    /* For FFTW_FORWARD: fourth is set to 0 */
    /* FOR FFTW_BACKWARD: fourth is set to 1 */
    int fourth = (plan->sign == FFTW_FORWARD ? 0: 1);
    int fifth = (offload_type == TI_FFTW_OFFLOAD_DIRECT ? FFT_DIRECT:FFT_ECPY);

    err = clSetKernelArg(plan->FFT_plan, 0, sizeof(n), &n);
    err |= clSetKernelArg(plan->FFT_plan, 1, sizeof(plan->bufX), &plan->bufX);
    err |= clSetKernelArg(plan->FFT_plan, 2, sizeof(plan->bufY), &plan->bufY);
    err |= clSetKernelArg(plan->FFT_plan, 3, sizeof(plan->bufW), &plan->bufW);
    err |= clSetKernelArg(plan->FFT_plan, 4, sizeof(int), &fourth);
    err |= clSetKernelArg(plan->FFT_plan, 5, sizeof(int), &fifth);
    err |= clSetKernelArg(plan->FFT_plan, 6, sizeof(plan->bufPlan), &plan->bufPlan);
    TI_FFTW_OCL_CHKERROR("clSetKernelArg",err);
    
    memcpy(&plan->FFT_execute,&_ti_fftw_ocl_FFT_execute,sizeof(cl_kernel));
    err = clSetKernelArg(plan->FFT_execute, 0, sizeof(plan->bufPlan), &plan->bufPlan);
    err |= clSetKernelArg(plan->FFT_execute, 1, sizeof(plan->bufX), &plan->bufX);
    err |= clSetKernelArg(plan->FFT_execute, 2, sizeof(plan->bufY), &plan->bufY);
    err |= clSetKernelArg(plan->FFT_execute, 3, sizeof(plan->bufW), &plan->bufW);
    TI_FFTW_OCL_CHKERROR("clSetKernelArg",err);

    memcpy(&plan->FFT_destroy_plan,&_ti_fftw_ocl_FFT_destroy_plan,sizeof(cl_kernel));
    err = clSetKernelArg(plan->FFT_destroy_plan, 0, sizeof(plan->bufPlan), &plan->bufPlan);
    TI_FFTW_OCL_CHKERROR("clSetKernelArg",err);
    
    TI_FFTW_DEBUG_PRINT("  Enqueuing FFT_plan on DSP\n");
    err |= clEnqueueTask(_ti_fftw_ocl_Q, plan->FFT_plan, 0, 0, &e);
    TI_FFTW_OCL_CHKERROR("clEnqueueTask",err);
    /* Wait for kernel to finish */ 
    err |= clWaitForEvents(1, &e);
    TI_FFTW_OCL_CHKERROR("clWaitForEvents",err);
    err |= clReleaseEvent(e);
    TI_FFTW_OCL_CHKERROR("clReleaseEvent",err);
    TI_FFTW_PROFILE_OCL_REPORT();

    if (err != CL_SUCCESS) {
      cl_int err;
          bail:
      err = clReleaseMemObject(plan->bufX);    
      TI_FFTW_OCL_CHKERROR("clReleaseMemObject",err);   
      err = clReleaseMemObject(plan->bufY);    
      TI_FFTW_OCL_CHKERROR("clReleaseMemObject",err);   
      err = clReleaseMemObject(plan->bufW);
      TI_FFTW_OCL_CHKERROR("clReleaseMemObject",err);   
      err = clReleaseMemObject(plan->bufPlan);    
      TI_FFTW_OCL_CHKERROR("clReleaseMemObject",err);   
      free(plan); 
      return NULL;
    }
    element_t *t = (element_t *)malloc(sizeof(element_t)); 
    if (t == NULL) {
      TI_FFTW_ERROR_PRINT("Could not malloc a _ti_fftw_plan_info_t.\n"); 
      goto bail; 
    }
    t->item = (void *)plan; CDL_PREPEND(_ti_fftw_tracked_plans,t);
  }

  TI_FFTW_DEBUG_PRINT("  Returned plan ID: %p\n", (void *) plan);
  TI_FFTW_PROFILE_REPORT("  Entire plan_offload call took %8.2f us\n",(float) clock_diff);
  return (fftw_plan)(plan); 
} 
/* end of 1d offload plan*/

/* 2d offload plan */
fftw_plan _ti_fftw_plan_offload_2d(const int plan_type, const int offload_type, int n1, int n2,
                                   int insize, int outsize, int twsize, int localsize,
                                   void *in,void *out,int sign, unsigned flags, void* p)
{
  TI_FFTW_PROFILE_START();
  TI_FFTW_DEBUG_PRINT("  Offloading %s to DSP\n", _ti_fftw_ocl_plans[plan_type].fftw_plan_name);

  /* Allocate a new _ti_fftw_plan_info_t structure so we can track the details
   * for the subsequent execute */
  _ti_fftw_plan_info_t *plan = (_ti_fftw_plan_info_t *)malloc(sizeof(_ti_fftw_plan_info_t));
  if (plan == NULL) {
    TI_FFTW_ERROR_PRINT("Could not malloc a _ti_fftw_plan_info_t.\n");
    return NULL;
  }
  memset(plan,0x0,sizeof(*plan));

  /* First copy in and out pointers as we will need them in the
     the offloaded execute */
  plan->in = in;
  plan->out = out;
  plan->sign = sign;
  plan->p = p;
  /* this is to be removed after temp buffer allocated in DSP for c2c kernels */
  plan->copy_input = 0;
  if (plan_type == TI_FFTW_FFT_2D_DP_C2C)
    plan->copy_input = 1;

  /* Store some state */
  plan->insize  = insize;
  plan->outsize = outsize;
  plan->offload_type = offload_type;

  cl_int err;

  /* We pass the out buffer as the FFTLIB generates
   * its output in bufX. For IN-PLACE transforms, 
   * OUT == IN, and we don't need to do anything more.
   * For, OUT-OF-PLACE transforms, we mempcpy IN into
   * OUT in intercepted execute, before calling the
   * the offloaded execute. This also ensures the
   * the FFTW3 semantics for out-of-place transforms
   * that IN is not modified is maintained */
  if (plan->copy_input == 1)
    plan->bufX = clCreateBuffer(_ti_fftw_ocl_context, 
                 CL_MEM_READ_WRITE|CL_MEM_USE_HOST_PTR,
                 plan->insize, out, &err);
  else
    plan->bufX = clCreateBuffer(_ti_fftw_ocl_context, 
                 CL_MEM_READ_WRITE|CL_MEM_USE_HOST_PTR,
                 plan->insize, in, &err);

  TI_FFTW_OCL_CHKERROR("clCreateBuffer",err);


  /* bufY is a separate scratch space for the ECPY kernel. It's not copied
   * from in the execute. It's just scratch space */
  if (plan->copy_input == 1)
    plan->bufY = clCreateBuffer(_ti_fftw_ocl_context, 
                CL_MEM_READ_WRITE|CL_MEM_ALLOC_HOST_PTR, plan->insize, NULL, &err);
  else
    plan->bufY = clCreateBuffer(_ti_fftw_ocl_context, 
                CL_MEM_READ_WRITE|CL_MEM_USE_HOST_PTR, plan->outsize, out, &err);

  TI_FFTW_OCL_CHKERROR("clCreateBuffer",err);
  
  plan->bufW = clCreateBuffer(_ti_fftw_ocl_context, CL_MEM_READ_WRITE, twsize, NULL, &err);
  TI_FFTW_OCL_CHKERROR("clCreateBuffer",err);
  plan->bufPlan = clCreateBuffer(_ti_fftw_ocl_context, CL_MEM_READ_WRITE|CL_MEM_USE_HOST_PTR, FFT_PLAN_T_SIZE, p, &err);
  TI_FFTW_OCL_CHKERROR("clCreateBuffer",err);


  if ((plan->bufW == NULL) || (plan->bufY == NULL) ||
      (plan->bufX == NULL) || (plan->bufPlan == NULL)) {
    cl_int err = CL_SUCCESS;
    TI_FFTW_ERROR_PRINT("Could not malloc an OCL Buffer, returning NULL plan.\n");
    if (plan->bufPlan) { err = clReleaseMemObject(plan->bufPlan);
      TI_FFTW_OCL_CHKERROR("clReleaseMemObject",err); }
    if (plan->bufW) { err = clReleaseMemObject(plan->bufW);
      TI_FFTW_OCL_CHKERROR("clReleaseMemObject",err); }
    if (plan->bufY) { err = clReleaseMemObject(plan->bufY);
      TI_FFTW_OCL_CHKERROR("clReleaseMemObject",err); }
    if (plan->bufX) { err = clReleaseMemObject(plan->bufX);
      TI_FFTW_OCL_CHKERROR("clReleaseMemObject",err); }
    memset(plan,0x0,sizeof(*plan));
    free(plan);
    return NULL;
  }
  
  cl_event e;
  {
#ifdef TI_FFTW_ALLOW_DIRECT
  if (offload_type == TI_FFTW_OFFLOAD_DIRECT) {
    memcpy(&plan->FFT_plan,&_ti_fftw_ocl_plans[plan_type].direct,sizeof(cl_kernel));
    TI_FFTW_DEBUG_PRINT("  Using DIRECT for plan type %d\n", plan_type);
  }
#endif
#ifdef TI_FFTW_ALLOW_ECPY
  if (offload_type == TI_FFTW_OFFLOAD_ECPY) {
    memcpy(&plan->FFT_plan,&_ti_fftw_ocl_plans[plan_type].ecpy,sizeof(cl_kernel));
    TI_FFTW_DEBUG_PRINT("  Using ECPY for plan type %d\n", plan_type);
  }
#endif

    /* For FFTW_FORWARD: fourth is set to 0 */
    /* FOR FFTW_BACKWARD: fourth is set to 1 */
    int fourth = (plan->sign == FFTW_FORWARD ? 0: 1);
    int fifth = (offload_type == TI_FFTW_OFFLOAD_DIRECT ? FFT_DIRECT:FFT_ECPY);

    err = clSetKernelArg(plan->FFT_plan, 0, sizeof(n1), &n1);
    err |= clSetKernelArg(plan->FFT_plan, 1, sizeof(n2), &n2);
    err |= clSetKernelArg(plan->FFT_plan, 2, sizeof(plan->bufX), &plan->bufX);
    err |= clSetKernelArg(plan->FFT_plan, 3, sizeof(plan->bufY), &plan->bufY);
    err |= clSetKernelArg(plan->FFT_plan, 4, sizeof(plan->bufW), &plan->bufW);
    err |= clSetKernelArg(plan->FFT_plan, 5, sizeof(int), &fourth);
    err |= clSetKernelArg(plan->FFT_plan, 6, sizeof(int), &fifth);
    err |= clSetKernelArg(plan->FFT_plan, 7, sizeof(plan->bufPlan), &plan->bufPlan); 
    TI_FFTW_OCL_CHKERROR("clSetKernelArg",err);
    
    memcpy(&plan->FFT_execute,&_ti_fftw_ocl_FFT_execute,sizeof(cl_kernel));
    err = clSetKernelArg(plan->FFT_execute, 0, sizeof(plan->bufPlan), &plan->bufPlan);
    err |= clSetKernelArg(plan->FFT_execute, 1, sizeof(plan->bufX), &plan->bufX);
    err |= clSetKernelArg(plan->FFT_execute, 2, sizeof(plan->bufY), &plan->bufY);
    err |= clSetKernelArg(plan->FFT_execute, 3, sizeof(plan->bufW), &plan->bufW);
    err |= clSetKernelArg(plan->FFT_execute, 4, localsize, NULL);
    TI_FFTW_OCL_CHKERROR("clSetKernelArg",err);

    memcpy(&plan->FFT_destroy_plan,&_ti_fftw_ocl_FFT_destroy_plan,sizeof(cl_kernel));
    err = clSetKernelArg(plan->FFT_destroy_plan, 0, sizeof(plan->bufPlan), &plan->bufPlan);
    TI_FFTW_OCL_CHKERROR("clSetKernelArg",err);
    
    TI_FFTW_DEBUG_PRINT("  Enqueuing FFT_plan on DSP\n");
    err |= clEnqueueTask(_ti_fftw_ocl_Q, plan->FFT_plan, 0, 0, &e);
    TI_FFTW_OCL_CHKERROR("clEnqueueTask",err);
    /* Wait for kernel to finish */ 
    err |= clWaitForEvents(1, &e);
    TI_FFTW_OCL_CHKERROR("clWaitForEvents",err);
    err |= clReleaseEvent(e);
    TI_FFTW_OCL_CHKERROR("clReleaseEvent",err);
    TI_FFTW_PROFILE_OCL_REPORT();

    if (err != CL_SUCCESS) {
      cl_int err;
          bail:
      err = clReleaseMemObject(plan->bufX);    
      TI_FFTW_OCL_CHKERROR("clReleaseMemObject",err);   
      err = clReleaseMemObject(plan->bufY);    
      TI_FFTW_OCL_CHKERROR("clReleaseMemObject",err);   
      err = clReleaseMemObject(plan->bufW);
      TI_FFTW_OCL_CHKERROR("clReleaseMemObject",err);   
      err = clReleaseMemObject(plan->bufPlan);    
      TI_FFTW_OCL_CHKERROR("clReleaseMemObject",err);   
      free(plan); 
      return NULL;
    }
    element_t *t = (element_t *)malloc(sizeof(element_t)); 
    if (t == NULL) {
      TI_FFTW_ERROR_PRINT("Could not malloc a _ti_fftw_plan_info_t.\n"); 
      goto bail; 
    }
    t->item = (void *)plan; CDL_PREPEND(_ti_fftw_tracked_plans,t);
  }

  TI_FFTW_DEBUG_PRINT("  Returned plan ID: %p\n", (void *) plan);
  TI_FFTW_PROFILE_REPORT("  Entire plan_offload call took %8.2f us\n",(float) clock_diff);
  return (fftw_plan)(plan); 
} 
/* end of 2d offload plan */

fftw_plan _ti_fftw_plan_offload_3d(const int plan_type, const int offload_type, int n1, int n2, int n3,
                                   int insize, int outsize, int twsize, int localsize,
                                   void *in,void *out,int sign, unsigned flags, void* p)
{
  TI_FFTW_PROFILE_START();
  TI_FFTW_DEBUG_PRINT("  Offloading %s to DSP\n", _ti_fftw_ocl_plans[plan_type].fftw_plan_name);

  /* Allocate a new _ti_fftw_plan_info_t structure so we can track the details
   * for the subsequent execute */
  _ti_fftw_plan_info_t *plan = (_ti_fftw_plan_info_t *)malloc(sizeof(_ti_fftw_plan_info_t));
  if (plan == NULL) {
    TI_FFTW_ERROR_PRINT("Could not malloc a _ti_fftw_plan_info_t.\n");
    return NULL;
  }
  memset(plan,0x0,sizeof(*plan));

  /* First copy in and out pointers as we will need them in the
     the offloaded execute */
  plan->in = in;
  plan->out = out;
  plan->sign = sign;
  plan->p = p;
  plan->copy_input = 0;

  /* Store some state */
  plan->insize  = insize;
  plan->outsize = outsize;
  plan->offload_type = offload_type;

  cl_int err;

  /* We pass the out buffer as the FFTLIB generates
   * its output in bufX. For IN-PLACE transforms, 
   * OUT == IN, and we don't need to do anything more.
   * For, OUT-OF-PLACE transforms, we mempcpy IN into
   * OUT in intercepted execute, before calling the
   * the offloaded execute. This also ensures the
   * the FFTW3 semantics for out-of-place transforms
   * that IN is not modified is maintained */
  plan->bufX = clCreateBuffer(_ti_fftw_ocl_context, 
                 CL_MEM_READ_WRITE|CL_MEM_USE_HOST_PTR,
                 plan->insize, in, &err);
  TI_FFTW_OCL_CHKERROR("clCreateBuffer",err);

  /* bufY is a separate scratch space for the ECPY kernel. It's not copied
   * from in the execute. It's just scratch space */
  plan->bufY = clCreateBuffer(_ti_fftw_ocl_context, 
                CL_MEM_READ_WRITE|CL_MEM_USE_HOST_PTR, plan->outsize, out, &err);

  TI_FFTW_OCL_CHKERROR("clCreateBuffer",err);
  
  plan->bufW = clCreateBuffer(_ti_fftw_ocl_context, CL_MEM_READ_WRITE, twsize, NULL, &err);
  TI_FFTW_OCL_CHKERROR("clCreateBuffer",err);
  plan->bufPlan = clCreateBuffer(_ti_fftw_ocl_context, CL_MEM_READ_WRITE|CL_MEM_USE_HOST_PTR, FFT_PLAN_T_SIZE, p, &err);
  TI_FFTW_OCL_CHKERROR("clCreateBuffer",err);


  if ((plan->bufW == NULL) || (plan->bufY == NULL) ||
      (plan->bufX == NULL) || (plan->bufPlan == NULL)) {
    cl_int err = CL_SUCCESS;
    TI_FFTW_ERROR_PRINT("Could not malloc an OCL Buffer, returning NULL plan.\n");
    if (plan->bufPlan) { err = clReleaseMemObject(plan->bufPlan);
      TI_FFTW_OCL_CHKERROR("clReleaseMemObject",err); }
    if (plan->bufW) { err = clReleaseMemObject(plan->bufW);
      TI_FFTW_OCL_CHKERROR("clReleaseMemObject",err); }
    if (plan->bufY) { err = clReleaseMemObject(plan->bufY);
      TI_FFTW_OCL_CHKERROR("clReleaseMemObject",err); }
    if (plan->bufX) { err = clReleaseMemObject(plan->bufX);
      TI_FFTW_OCL_CHKERROR("clReleaseMemObject",err); }
    memset(plan,0x0,sizeof(*plan));
    free(plan);
    return NULL;
  }
  
  cl_event e;
  {
#ifdef TI_FFTW_ALLOW_DIRECT
  if (offload_type == TI_FFTW_OFFLOAD_DIRECT) {
    memcpy(&plan->FFT_plan,&_ti_fftw_ocl_plans[plan_type].direct,sizeof(cl_kernel));
    TI_FFTW_DEBUG_PRINT("  Using DIRECT for plan type %d\n", plan_type);
  }
#endif
#ifdef TI_FFTW_ALLOW_ECPY
  if (offload_type == TI_FFTW_OFFLOAD_ECPY) {
    memcpy(&plan->FFT_plan,&_ti_fftw_ocl_plans[plan_type].ecpy,sizeof(cl_kernel));
    TI_FFTW_DEBUG_PRINT("  Using ECPY for plan type %d\n", plan_type);
  }
#endif

    /* For FFTW_FORWARD: fourth is set to 0 */
    /* FOR FFTW_BACKWARD: fourth is set to 1 */
    int fourth = (plan->sign == FFTW_FORWARD ? 0: 1);
    int fifth = (offload_type == TI_FFTW_OFFLOAD_DIRECT ? FFT_DIRECT:FFT_ECPY);

    err = clSetKernelArg(plan->FFT_plan, 0, sizeof(n1), &n1);
    err |= clSetKernelArg(plan->FFT_plan, 1, sizeof(n2), &n2);
    err |= clSetKernelArg(plan->FFT_plan, 2, sizeof(n3), &n3);
    err |= clSetKernelArg(plan->FFT_plan, 3, sizeof(plan->bufX), &plan->bufX);
    err |= clSetKernelArg(plan->FFT_plan, 4, sizeof(plan->bufY), &plan->bufY);
    err |= clSetKernelArg(plan->FFT_plan, 5, sizeof(plan->bufW), &plan->bufW);
    err |= clSetKernelArg(plan->FFT_plan, 6, sizeof(int), &fourth);
    err |= clSetKernelArg(plan->FFT_plan, 7, sizeof(int), &fifth);
    err |= clSetKernelArg(plan->FFT_plan, 8, sizeof(plan->bufPlan), &plan->bufPlan);
    TI_FFTW_OCL_CHKERROR("clSetKernelArg",err);
    
    memcpy(&plan->FFT_execute,&_ti_fftw_ocl_FFT_execute,sizeof(cl_kernel));
    err = clSetKernelArg(plan->FFT_execute, 0, sizeof(plan->bufPlan), &plan->bufPlan);
    err |= clSetKernelArg(plan->FFT_execute, 1, sizeof(plan->bufX), &plan->bufX);
    err |= clSetKernelArg(plan->FFT_execute, 2, sizeof(plan->bufY), &plan->bufY);
    err |= clSetKernelArg(plan->FFT_execute, 3, sizeof(plan->bufW), &plan->bufW);
    err |= clSetKernelArg(plan->FFT_execute, 4, localsize, NULL);
    TI_FFTW_OCL_CHKERROR("clSetKernelArg",err);

    memcpy(&plan->FFT_destroy_plan,&_ti_fftw_ocl_FFT_destroy_plan,sizeof(cl_kernel));
    err = clSetKernelArg(plan->FFT_destroy_plan, 0, sizeof(plan->bufPlan), &plan->bufPlan);
    TI_FFTW_OCL_CHKERROR("clSetKernelArg",err);
    
    TI_FFTW_DEBUG_PRINT("  Enqueuing FFT_plan on DSP\n");
    err |= clEnqueueTask(_ti_fftw_ocl_Q, plan->FFT_plan, 0, 0, &e);
    TI_FFTW_OCL_CHKERROR("clEnqueueTask",err);
    /* Wait for kernel to finish */ 
    err |= clWaitForEvents(1, &e);
    TI_FFTW_OCL_CHKERROR("clWaitForEvents",err);
    err |= clReleaseEvent(e);
    TI_FFTW_OCL_CHKERROR("clReleaseEvent",err);
    TI_FFTW_PROFILE_OCL_REPORT();

    if (err != CL_SUCCESS) {
      cl_int err;
          bail:
      err = clReleaseMemObject(plan->bufX);    
      TI_FFTW_OCL_CHKERROR("clReleaseMemObject",err);   
      err = clReleaseMemObject(plan->bufY);    
      TI_FFTW_OCL_CHKERROR("clReleaseMemObject",err);   
      err = clReleaseMemObject(plan->bufW);
      TI_FFTW_OCL_CHKERROR("clReleaseMemObject",err);   
      err = clReleaseMemObject(plan->bufPlan);    
      TI_FFTW_OCL_CHKERROR("clReleaseMemObject",err);   
      free(plan); 
      return NULL;
    }
    element_t *t = (element_t *)malloc(sizeof(element_t)); 
    if (t == NULL) {
      TI_FFTW_ERROR_PRINT("Could not malloc a _ti_fftw_plan_info_t.\n"); 
      goto bail; 
    }
    t->item = (void *)plan; CDL_PREPEND(_ti_fftw_tracked_plans,t);
  }

  TI_FFTW_DEBUG_PRINT("  Returned plan ID: %p\n", (void *) plan);
  TI_FFTW_PROFILE_REPORT("  Entire plan_offload call took %8.2f us\n",(float) clock_diff);
  return (fftw_plan)(plan); 
} 

/* 1d batch offload plan */
fftw_plan _ti_fftw_plan_offload_1d_batch(const int plan_type, const int offload_type, int n,
                                   int batch, int insize, int outsize, int twsize, int localsize,
                                   void *in, void *out, int sign, unsigned flags, void* p)
{
  TI_FFTW_PROFILE_START();
  TI_FFTW_DEBUG_PRINT("  Offloading %s to DSP\n", _ti_fftw_ocl_plans[plan_type].fftw_plan_name);

  /* Allocate a new _ti_fftw_plan_info_t structure so we can track the details
   * for the subsequent execute */
  _ti_fftw_plan_info_t *plan = (_ti_fftw_plan_info_t *)malloc(sizeof(_ti_fftw_plan_info_t));
  if (plan == NULL) {
    TI_FFTW_ERROR_PRINT("Could not malloc a _ti_fftw_plan_info_t.\n");
    return NULL;
  }
  memset(plan,0x0,sizeof(*plan));

  /* First copy in and out pointers as we will need them in the
     the offloaded execute */
  plan->in = in;
  plan->out = out;
  plan->sign = sign;
  plan->p = p;
  /* this is to be removed after temp buffer allocated in DSP for c2c kernels */
  plan->copy_input = 0;

  /* Store some state */
  plan->insize  = insize;
  plan->outsize = outsize;
  plan->offload_type = offload_type;

  cl_int err;

  plan->bufX = clCreateBuffer(_ti_fftw_ocl_context, 
                 CL_MEM_READ_WRITE|CL_MEM_USE_HOST_PTR,
                 plan->insize, in, &err);
  TI_FFTW_OCL_CHKERROR("clCreateBuffer",err);

  plan->bufY = clCreateBuffer(_ti_fftw_ocl_context, 
                 CL_MEM_READ_WRITE|CL_MEM_USE_HOST_PTR,
                 plan->outsize, out, &err);
  TI_FFTW_OCL_CHKERROR("clCreateBuffer",err);
  
  plan->bufW = clCreateBuffer(_ti_fftw_ocl_context, CL_MEM_READ_WRITE, twsize, NULL, &err);
  TI_FFTW_OCL_CHKERROR("clCreateBuffer",err);
  plan->bufPlan = clCreateBuffer(_ti_fftw_ocl_context, CL_MEM_READ_WRITE|CL_MEM_USE_HOST_PTR, FFT_PLAN_T_SIZE, p, &err);
  TI_FFTW_OCL_CHKERROR("clCreateBuffer",err);


  if ((plan->bufW == NULL) || (plan->bufY == NULL) ||
      (plan->bufX == NULL) || (plan->bufPlan == NULL)) {
    cl_int err = CL_SUCCESS;
    TI_FFTW_ERROR_PRINT("Could not malloc an OCL Buffer, returning NULL plan.\n");
    if (plan->bufPlan) { err = clReleaseMemObject(plan->bufPlan);
      TI_FFTW_OCL_CHKERROR("clReleaseMemObject",err); }
    if (plan->bufW) { err = clReleaseMemObject(plan->bufW);
      TI_FFTW_OCL_CHKERROR("clReleaseMemObject",err); }
    if (plan->bufY) { err = clReleaseMemObject(plan->bufY);
      TI_FFTW_OCL_CHKERROR("clReleaseMemObject",err); }
    if (plan->bufX) { err = clReleaseMemObject(plan->bufX);
      TI_FFTW_OCL_CHKERROR("clReleaseMemObject",err); }
    memset(plan,0x0,sizeof(*plan));
    free(plan);
    return NULL;
  }
  
  cl_event e;
  {
#ifdef TI_FFTW_ALLOW_DIRECT
  if (offload_type == TI_FFTW_OFFLOAD_DIRECT) {
    memcpy(&plan->FFT_plan,&_ti_fftw_ocl_plans[plan_type].direct,sizeof(cl_kernel));
    TI_FFTW_DEBUG_PRINT("  Using DIRECT for plan type %d\n", plan_type);
  }
#endif
#ifdef TI_FFTW_ALLOW_ECPY
  if (offload_type == TI_FFTW_OFFLOAD_ECPY) {
    memcpy(&plan->FFT_plan,&_ti_fftw_ocl_plans[plan_type].ecpy,sizeof(cl_kernel));
    TI_FFTW_DEBUG_PRINT("  Using ECPY for plan type %d\n", plan_type);
  }
#endif

    /* For FFTW_FORWARD: fourth is set to 0 */
    /* FOR FFTW_BACKWARD: fourth is set to 1 */
    int fourth = (plan->sign == FFTW_FORWARD ? 0: 1);
    int fifth = (offload_type == TI_FFTW_OFFLOAD_DIRECT ? FFT_DIRECT:FFT_ECPY);

    err = clSetKernelArg(plan->FFT_plan, 0, sizeof(n), &n);
    err |= clSetKernelArg(plan->FFT_plan, 1, sizeof(plan->bufX), &plan->bufX);
    err |= clSetKernelArg(plan->FFT_plan, 2, sizeof(plan->bufY), &plan->bufY);
    err |= clSetKernelArg(plan->FFT_plan, 3, sizeof(plan->bufW), &plan->bufW);
    err |= clSetKernelArg(plan->FFT_plan, 4, sizeof(int), &fourth);
    err |= clSetKernelArg(plan->FFT_plan, 5, sizeof(int), &fifth);
    err |= clSetKernelArg(plan->FFT_plan, 6, sizeof(plan->bufPlan), &plan->bufPlan); 
    TI_FFTW_OCL_CHKERROR("clSetKernelArg",err);
    
    memcpy(&plan->FFT_execute,&_ti_fftw_ocl_FFT_execute,sizeof(cl_kernel));
    err = clSetKernelArg(plan->FFT_execute, 0, sizeof(plan->bufPlan), &plan->bufPlan);
    err |= clSetKernelArg(plan->FFT_execute, 1, sizeof(plan->bufX), &plan->bufX);
    err |= clSetKernelArg(plan->FFT_execute, 2, sizeof(plan->bufY), &plan->bufY);
    err |= clSetKernelArg(plan->FFT_execute, 3, sizeof(plan->bufW), &plan->bufW);
    err |= clSetKernelArg(plan->FFT_execute, 4, localsize, NULL);
    TI_FFTW_OCL_CHKERROR("clSetKernelArg",err);

    memcpy(&plan->FFT_destroy_plan,&_ti_fftw_ocl_FFT_destroy_plan,sizeof(cl_kernel));
    err = clSetKernelArg(plan->FFT_destroy_plan, 0, sizeof(plan->bufPlan), &plan->bufPlan);
    TI_FFTW_OCL_CHKERROR("clSetKernelArg",err);
    
    TI_FFTW_DEBUG_PRINT("  Enqueuing FFT_plan on DSP\n");
    err |= clEnqueueTask(_ti_fftw_ocl_Q, plan->FFT_plan, 0, 0, &e);
    TI_FFTW_OCL_CHKERROR("clEnqueueTask",err);
    /* Wait for kernel to finish */ 
    err |= clWaitForEvents(1, &e);
    TI_FFTW_OCL_CHKERROR("clWaitForEvents",err);
    err |= clReleaseEvent(e);
    TI_FFTW_OCL_CHKERROR("clReleaseEvent",err);
    TI_FFTW_PROFILE_OCL_REPORT();

    if (err != CL_SUCCESS) {
      cl_int err;
          bail:
      err = clReleaseMemObject(plan->bufX);    
      TI_FFTW_OCL_CHKERROR("clReleaseMemObject",err);   
      err = clReleaseMemObject(plan->bufY);    
      TI_FFTW_OCL_CHKERROR("clReleaseMemObject",err);   
      err = clReleaseMemObject(plan->bufW);
      TI_FFTW_OCL_CHKERROR("clReleaseMemObject",err);   
      err = clReleaseMemObject(plan->bufPlan);    
      TI_FFTW_OCL_CHKERROR("clReleaseMemObject",err);   
      free(plan); 
      return NULL;
    }
    element_t *t = (element_t *)malloc(sizeof(element_t)); 
    if (t == NULL) {
      TI_FFTW_ERROR_PRINT("Could not malloc a _ti_fftw_plan_info_t.\n"); 
      goto bail; 
    }
    t->item = (void *)plan; CDL_PREPEND(_ti_fftw_tracked_plans,t);
  }

  TI_FFTW_DEBUG_PRINT("  Returned plan ID: %p\n", (void *) plan);
  TI_FFTW_PROFILE_REPORT("  Entire plan_offload call took %8.2f us\n",(float) clock_diff);
  return (fftw_plan)(plan); 
}  /* end of 1d batch plan offload */
/* end of fftw_acc_plan.c */

