/* ======================================================================= */
/*  TEXAS INSTRUMENTS, INC.                                                */
/*                                                                         */
/*  FFTLIB  FFT Library                                                    */
/*                                                                         */
/* Copyright (C) 2014 Texas Instruments Incorporated - http://www.ti.com/  */
/*                                                                         */
/*                                                                         */
/*  Redistribution and use in source and binary forms, with or without     */
/*  modification, are permitted provided that the following conditions     */
/*  are met:                                                               */
/*                                                                         */
/*    Redistributions of source code must retain the above copyright       */
/*    notice, this list of conditions and the following disclaimer.        */
/*                                                                         */
/*    Redistributions in binary form must reproduce the above copyright    */
/*    notice, this list of conditions and the following disclaimer in the  */
/*    documentation and/or other materials provided with the               */
/*    distribution.                                                        */
/*                                                                         */
/*    Neither the name of Texas Instruments Incorporated nor the names of  */
/*    its contributors may be used to endorse or promote products derived  */
/*    from this software without specific prior written permission.        */
/*                                                                         */
/*  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS    */
/*  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT      */
/*  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR  */
/*  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT   */
/*  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,  */
/*  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT       */
/*  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,  */
/*  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY  */
/*  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT    */
/*  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE  */
/*  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.   */
/*                                                                         */
/* ======================================================================= */
#include "fftw_acc.h"
#include "fftw_acc_globals.h"

void fftw_destroy_plan(fftw_plan plan)
{
  TI_FFTW_PROFILE_START();
  TI_FFTW_DEBUG_PRINT("fftw_destroy_plan(%p) intercepted\n", (void *)plan);

  element_t *elt = NULL; CDL_SEARCH_SCALAR(_ti_fftw_tracked_plans,elt,item,plan); if (elt == NULL)
  {
    TI_FFTW_DEBUG_PRINT("  Invoking FFTW's fftw_destroy_plan as no offload plan for %p\n", (void *)plan);
#ifdef TI_FFTW_USE_DLSYM
    assert(_fptr_fftw_destroy_plan);
    _fptr_fftw_destroy_plan(plan);
#else
    __real_fftw_destroy_plan(plan);
#endif
    TI_FFTW_PROFILE_REPORT("  fftw_destroy (ARM) took %8.2f us\n",(float) clock_diff);
    return;
  }

  TI_FFTW_DEBUG_PRINT("  Found plan for %p. Offloading fftw_destroy to DSP\n", (void *)plan);
  _ti_fftw_plan_info_t *plan_id = (_ti_fftw_plan_info_t *) plan;

    /* remove from tracked plans as we have completed destroy */
    CDL_DELETE(_ti_fftw_tracked_plans,elt);
    memset(elt,0x0,sizeof(*elt));
    free(elt);
    
  cl_int err = clReleaseMemObject(plan_id->bufX);    
  TI_FFTW_OCL_CHKERROR("clReleaseMemObject",err);   
  err = clReleaseMemObject(plan_id->bufY);    
  TI_FFTW_OCL_CHKERROR("clReleaseMemObject",err);   
  err = clReleaseMemObject(plan_id->bufW);
  TI_FFTW_OCL_CHKERROR("clReleaseMemObject",err);   
  err = clReleaseMemObject(plan_id->bufPlan);    
  TI_FFTW_OCL_CHKERROR("clReleaseMemObject",err);   
  _lib_extern_free(plan_id->p); 
  free((_ti_fftw_plan_info_t *)plan);

  TI_FFTW_DEBUG_PRINT("  Return from destroy %p\n", (void *) plan);
  TI_FFTW_PROFILE_REPORT("  Entire destroy call took %8.2f us\n",(float) clock_diff);
  return;
}

