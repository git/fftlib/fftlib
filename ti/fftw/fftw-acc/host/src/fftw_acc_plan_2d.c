/* ======================================================================= */
/*  TEXAS INSTRUMENTS, INC.                                                */
/*                                                                         */
/*  FFTLIB  FFT Library                                                    */
/*                                                                         */
/* Copyright (C) 2014 Texas Instruments Incorporated - http://www.ti.com/  */
/*                                                                         */
/*                                                                         */
/*  Redistribution and use in source and binary forms, with or without     */
/*  modification, are permitted provided that the following conditions     */
/*  are met:                                                               */
/*                                                                         */
/*    Redistributions of source code must retain the above copyright       */
/*    notice, this list of conditions and the following disclaimer.        */
/*                                                                         */
/*    Redistributions in binary form must reproduce the above copyright    */
/*    notice, this list of conditions and the following disclaimer in the  */
/*    documentation and/or other materials provided with the               */
/*    distribution.                                                        */
/*                                                                         */
/*    Neither the name of Texas Instruments Incorporated nor the names of  */
/*    its contributors may be used to endorse or promote products derived  */
/*    from this software without specific prior written permission.        */
/*                                                                         */
/*  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS    */
/*  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT      */
/*  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR  */
/*  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT   */
/*  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,  */
/*  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT       */
/*  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,  */
/*  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY  */
/*  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT    */
/*  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE  */
/*  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.   */
/*                                                                         */
/* ======================================================================= */
#include "fftw_acc.h"
#include "fftw_acc_globals.h"
#include "ti/fftlib/src/fft_omp_dp_2d_c2c/fft_omp_dp_2d_c2c.h"
#include "ti/fftlib/src/ifft_omp_dp_2d_c2c/ifft_omp_dp_2d_c2c.h"
#include "ti/fftlib/src/fft_omp_dp_2d_r2c/fft_omp_dp_2d_r2c.h"
#include "ti/fftlib/src/ifft_omp_dp_2d_c2r/ifft_omp_dp_2d_c2r.h"

/*************************************************************************/
/************** FFTW INTERCEPTED FUNCTIONS *******************************/
/*************************************************************************/

/*--------------------------------------------------------------------------------------------------------------------
                                rad and memory calculation for c2c
  --------------------------------------------------------------------------------------------------------------------*/
/* calculate radix for 1D array */
/* calculate radix for 1D array */
static void calculate_rad_c2c (int N, fft_para_mix_bs_t *fft_para)
{
	int n, j, M, rad;
	int s_r3, s_r5, N_p2, dum;
	int use_bs;

	use_bs = 1;

    n = (N<<1)-1;

	j = fftw_norm(n);
    M = 1 << (31-j);

	j = fftw_norm(M);

    N_p2 = N;
    s_r3 = 0;
    s_r5 = 0;

    /* if N is odd, bypass the rest of calculation, use Bluestein */
    if (((N>>1)<<1) == N)
    {
      /* if N is power of 2, no zero padding, regular radix 2 or 4 fft */
      if (N == (M>>1))
      { /* power of 2 */
        M = N;
        j = j+1;
        use_bs = 0;
      }
      else {
          dum = N;
          while (dum/3*3 == dum) {
            s_r3++;
            dum /= 3;
          }
          while (dum/5*5 == dum) {
            s_r5++;
            dum /= 5;
          }
          if ((dum >= 4) && (dum == (1 << (30-fftw_norm(dum))))) {
          /* mixed radix 2 and 3 */
            N_p2   = dum;
            use_bs = 0;
            M = N;
            j  = fftw_norm(dum);
			 
          }
        }
      }

    /* calculate radix for radix 2 or 4 c2c fft */
    if (j%2 == 0)
      rad = 4;
    else
      rad = 2;

    fft_para->N_p2   = N_p2;
    fft_para->s_r3   = s_r3;
    fft_para->s_r5   = s_r5;
    fft_para->M      = M;
    fft_para->rad    = rad;
    fft_para->use_bs = use_bs;

} /*calculate_rad ()*/

/* calculate twiddle size, Bn buffer size and workbuffer size */
static void calculate_mem_size_c2c (int N, fft_para_mix_bs_t *fft_para, int *twsize, int *Bnsize, int *worksize)
{
  int dum, n, i;
  int use_bs = fft_para->use_bs;
  int M = fft_para->M;
  int s_r3 = fft_para->s_r3;
  int s_r5 = fft_para->s_r5;
  int N_p2 = fft_para->N_p2;

  /* Please note that Bn buffer and Workbuf are only used when doing Bluestein
   * meaning when use_bs == 1 */

  if (use_bs == 1)
  { /* when size is not power of 2 and not multiple of 3&5 */
	*twsize   = (2*M+2*N);
	*Bnsize   = (2*M);
	*worksize = (4*M);
  }
  else
  {
    if ((s_r3 == 0) && (s_r5 == 0)) {
    /* When signal is power of 2 */
      *twsize   = N*2;
    }
    else {
    /* When signal is power of 3 or 5 */
      dum = 0;
      n  = 4*N;
      for (i = 0; i < s_r3; i++) {
        dum += n/3;
        n  /= 3;
      }
      n *= 2;  /* radix-5 has 8 twiddle vs radix-3 4 twiddles */
      for (i = 0; i < s_r5; i++) {
        dum += n/5;
        n  /= 5;
      }
      *twsize = (2*N_p2+dum);
    }
    *Bnsize   = 0;
    *worksize = 0;
  }
}

/*--------------------------------------------------------------------------------------------------------------------
                                rad and memory calculation for r2c
  --------------------------------------------------------------------------------------------------------------------*/
/* calculate radix for 1D array */
static void calculate_rad_r2c (int dim, int N, fft_para_mix_bs_t *fft_para)
{
	int n, j, M, rad;
	int s_r3, s_r5, N_p2, dum;
	int use_bs;

	use_bs = 1;

    n = (N<<1)-1;

	j = fftw_norm(n);
	M = 1 << (31-j);

	j = fftw_norm(M);

    if (dim == 1)
      N_p2 = N/2;
    else
      N_p2 = N;
    s_r3 = 0;
    s_r5 = 0;

    /* if N is odd, bypass the rest of calculation, use Bluestein */
    if (((N>>1)<<1) == N)
    {
      /* if N is power of 2, no zero padding, regular radix 2 or 4 fft */
      if (N == (M>>1))
      { /* power of 2 */
        M = N;
        use_bs = 0;
        if ((dim != 1))
          j  +=1;
      }
      else {
        if (dim == 1)
          dum = N/2;
        else
          dum = N;

        while (dum/3*3 == dum) {
          s_r3++;
          dum /= 3;
        }
        while (dum/5*5 == dum) {
          s_r5++;
          dum /= 5;
        }
        if ((dum >= 4) && (dum == (1 << (30-fftw_norm(dum))))) {
        /* mixed radix 2 and 3 */
          N_p2                       = dum;
          use_bs = 0;
          M = N;
		  j = fftw_norm(dum);
        }
      }
    }

    /* calculate radix for radix 2 or 4 r2c or c2c fft */

    if (j%2 == 0)
      rad = 4;
    else
      rad = 2;

    fft_para->N_p2   = N_p2;
    fft_para->s_r3   = s_r3;
    fft_para->s_r5   = s_r5;
    fft_para->M      = M;
    fft_para->rad    = rad;
    fft_para->use_bs = use_bs;

} /*calculate_rad ()*/

/* calculate twiddle size, Bn buffer size and workbuffer size */
static void calculate_mem_size_r2c (int dim, int N, fft_para_mix_bs_t *fft_para, int *twsize, int *Bnsize, int *worksize)
{
  int dum, n, i;
  int use_bs = fft_para->use_bs;
  int M = fft_para->M;
  int s_r3 = fft_para->s_r3;
  int s_r5 = fft_para->s_r5;
  int N_p2 = fft_para->N_p2;

  /* Please note that Bn buffer and Workbuf are only used when doing Bluestein
   * meaning when use_bs == 1 */

  if (use_bs == 1)
  { /* when size is not power of 2 and not multiple of 3&5 */
	*twsize   = (2*M+2*N);
	*Bnsize   = (2*M);
	*worksize = (4*M);
  }
  else
  {
    if ((s_r3 == 0) && (s_r5 == 0)) {
    /* When signal is power of 2 */

      if (dim == 1)
      /* 1st dimension is r2c fft */
        *twsize   = N*3/2;
      else
      /* other dimension is c2c fft */
        *twsize   = N*2;
    }
    else {
    /* When signal is power of 3 or 5 */
      dum = 0;
      if (dim == 1)
    	n = 2*N;
      else
        n  = 4*N;
      for (i = 0; i < s_r3; i++) {
        dum += n/3;
        n  /= 3;
      }
      n *= 2;  /* radix-5 has 8 twiddle vs radix-3 4 twiddles */
      for (i = 0; i < s_r5; i++) {
        dum += n/5;
        n  /= 5;
      }
      if(dim == 1)
        *twsize = (N/2+2*N_p2+dum);
      else
        *twsize = (2*N_p2+dum);
    }
    *Bnsize   = 0;
    *worksize = 0;
  }
} /* calculate_mem_size() */

/*--------------------------------------------------------------------------------------------------------------------
                                rad and memory calculation for c2r
  --------------------------------------------------------------------------------------------------------------------*/

/* calculate radix for 1D array */
static void calculate_rad_c2r (int dim, int N, fft_para_mix_bs_t *fft_para)
{
	int n, j, M, rad;
	int s_r3, s_r5, N_p2, dum;
	int use_bs;

	use_bs = 1;

    n = (N<<1)-1;

	j = fftw_norm(n);
    M = 1 << (31-j);
	

    if (dim==1){
	  j = fftw_norm(M);
	}

    if (dim == 2)
    	N_p2 = N/2;
    else
    	N_p2 = N;
    s_r3 = 0;
    s_r5 = 0;

    /* if N is odd, bypass the rest of calculation, use Bluestein */
    if (((N>>1)<<1) == N)
    {
      /* if N is power of 2, no zero padding, regular radix 2 or 4 fft */
      if (N == (M>>1))
      { /* power of 2 */
        M = N;
        j = j+1;
        use_bs = 0;
      }
      else {
          if (dim == 2)
    	    dum = N/2;
          else
      	    dum = N;
          while (dum/3*3 == dum) {
            s_r3++;
            dum /= 3;
          }
          while (dum/5*5 == dum) {
            s_r5++;
            dum /= 5;
          }
          if ((dum >= 4) && (dum == (1 << (30-fftw_norm(dum))))) {
          /* mixed radix 2 and 3 */
            N_p2                       = dum;
            use_bs = 0;
            M = N;
			j = fftw_norm(dum);
          }
        }
      }

    /* calculate radix for radix 2 or 4 c2r fft */
    if (dim==2)
    {
      if (j%2 == use_bs)
        rad = 4;
      else
        rad = 2;
    } else
    {
        if (j%2 == 0)
          rad = 4;
        else
          rad = 2;
    }

    fft_para->N_p2 = N_p2;
    fft_para->s_r3 = s_r3;
    fft_para->s_r5 = s_r5;
    fft_para->M    = M;
    fft_para->rad  = rad;
    fft_para->use_bs = use_bs;

} /*calculate_rad ()*/

/* calculate twiddle size, Bn buffer size and workbuffer size */
static void calculate_mem_size_c2r (int dim, int N, fft_para_mix_bs_t *fft_para, int *twsize, int *Bnsize, int *worksize)
{
  int dum, n, i;
  int use_bs = fft_para->use_bs;
  int M = fft_para->M;
  int s_r3 = fft_para->s_r3;
  int s_r5 = fft_para->s_r5;
  int N_p2 = fft_para->N_p2;
  /* Please note that Bn buffer and Workbuf are only used when doing Bluestein
   * meaning when use_bs == 1 */

  if (use_bs == 1)
  { /* when size is not power of 2 and not multiple of 3&5 */
	*twsize   = (2*M+2*N);
	*Bnsize   = (2*M);
	*worksize = (4*M);
  }
  else
  {
    if ((s_r3 == 0) && (s_r5 == 0)) {
    /* When signal is power of 2 */
        if (dim == 2)
          *twsize   = N*3;
        else
          *twsize   = N*2;
    }
    else {
    /* When signal is power of 3 or 5 */
      dum = 0;
      if (dim == 2)
        n  = 2*N;
      else
        n  = 4*N;
      for (i = 0; i < s_r3; i++) {
        dum += n/3;
        n  /= 3;
      }
      n *= 2;  /* radix-5 has 8 twiddle vs radix-3 4 twiddles */
      for (i = 0; i < s_r5; i++) {
        dum += n/5;
        n  /= 5;
      }
      if (dim == 2)
        *twsize = (N/2+2*N_p2+dum);
      else
        *twsize = (2*N_p2+dum);
    }
    *Bnsize   = 0;
    *worksize = 0;
  }
} /* calculate_mem_size() */


/* ADD KERNEL: Add a function for each new FFT kernel you want to offload */

/* plan for fft/ifft 2d dp c2c */

fftw_plan fftw_plan_dft_2d (int N1, int N2, fftw_complex *in, fftw_complex *out, int sign, unsigned flags)
{
  const int PLAN_TYPE = TI_FFTW_FFT_2D_DP_C2C;
  fft_plan_t *p;
  int    N;
  int    int_tw_size, ext_tw_size, twsize, localsize;
  int    worksize, work_size;
  int    int_Bn_size, ext_Bn_size, Bnsize;

  if (!_ti_fftw_init_done) _ti_fftw_acc_init();
  TI_FFTW_DEBUG_PRINT("%s(%d,%d,0x%p,0x%p,%d,%d,%d) intercepted\n", _ti_fftw_ocl_plans[PLAN_TYPE].fftw_plan_name, N1, N2, (void *)in, (void *)out, sign, flags, PLAN_TYPE);

  int offload_type = _ti_fftw_should_offload_2d_dp(PLAN_TYPE, N1, N2, sign, flags);

  if (offload_type) {
    p = (fft_plan_t *)_lib_extern_malloc(FFT_PLAN_T_SIZE);
	N = (N1 > N2) ? N1 : N2;

    /* determine the rad for the first  & second dimensions */

    calculate_rad_c2c (N2, &p->u.dp_2d_c2c_e.para1);

    calculate_rad_c2c (N1, &p->u.dp_2d_c2c_e.para2);

    /* Calculate mem size for 2 dimensions for ECPY */
    /* evaluate the  1st dimension */

    calculate_mem_size_c2c (N2, &p->u.dp_2d_c2c_e.para1, &twsize, &Bnsize, &worksize);
    p->u.dp_2d_c2c_e.para1.twsize = twsize;
    p->u.dp_2d_c2c_e.para1.Bnsize = Bnsize;
    int_tw_size = ext_tw_size = twsize;
    int_Bn_size = ext_Bn_size = Bnsize;
    work_size   = worksize;

    /* evaluate the 2nd dimension */
    calculate_mem_size_c2c (N1, &p->u.dp_2d_c2c_e.para2, &twsize, &Bnsize, &worksize);
    p->u.dp_2d_c2c_e.para2.twsize = twsize;
    p->u.dp_2d_c2c_e.para2.Bnsize = Bnsize;
    int_tw_size  = (int_tw_size > twsize) ? int_tw_size : twsize;
    ext_tw_size += twsize;
    int_Bn_size  = (int_Bn_size > Bnsize) ? int_Bn_size : Bnsize;
    ext_Bn_size += Bnsize;
    work_size    = (work_size > worksize) ? work_size : worksize;

    p->u.dp_2d_c2c_e.para1.worksize = work_size;
    p->u.dp_2d_c2c_e.para2.worksize = work_size;
    /* calculate local memory requirements */
    localsize = FFT_EDMA_CH_BUFSIZE+sizeof(double)*10*N*FFT_OMP_DP_2D_C2C_NUMOFLINEBUFS + sizeof(double)*(int_tw_size + int_Bn_size + work_size);
    
    return (_ti_fftw_plan_offload_2d(PLAN_TYPE, offload_type, N1, N2, sizeof(fftw_complex)*N1*N2, sizeof(fftw_complex)*N1*N2, sizeof(double)*(ext_tw_size+ext_Bn_size)+sizeof(fftw_complex)*N1*N2, localsize, in, out, sign, flags, (void *)p));
  }
  else
    {
      TI_FFTW_DEBUG_PRINT("  Using FFTW3 for %s\n", _ti_fftw_ocl_plans[PLAN_TYPE].fftw_plan_name);
      fftw_plan rc;
      TI_FFTW_PROFILE_START();
#ifdef TI_FFTW_USE_DLSYM
      assert(_fptr_fftw_plan_dft_2d);
      rc = _fptr_fftw_plan_dft_2d(N1, N2, in, out, sign, flags);
#else
      rc = __real_fftw_plan_dft_2d(N1, N2, in, out, sign, flags);
#endif
      TI_FFTW_PROFILE_REPORT("  fftw_plan_dft_2d (ARM) took %8.2f us\n",(float) clock_diff);
      return rc;
    } 
}
/* end of plan 2d dp c2c */

/* plan for 2d dp r2c */

fftw_plan fftw_plan_dft_r2c_2d (int N1, int N2, double *in, fftw_complex *out, unsigned flags)
{
  const int PLAN_TYPE = TI_FFTW_FFT_2D_DP_R2C;
  fft_plan_t *p;
  int    N;
  int    int_tw_size, ext_tw_size, twsize, localsize;
  int    worksize, work_size;
  int    int_Bn_size, ext_Bn_size, Bnsize;
  int    sign = FFTW_FORWARD;

  if (!_ti_fftw_init_done) _ti_fftw_acc_init();
  TI_FFTW_DEBUG_PRINT("%s(%d,%d,0x%p,0x%p,%d,%d,%d) intercepted\n", _ti_fftw_ocl_plans[PLAN_TYPE].fftw_plan_name, N1, N2, (void *)in, (void *)out, sign, flags, PLAN_TYPE);

  int offload_type = _ti_fftw_should_offload_2d_dp(PLAN_TYPE, N1, N2, sign, flags);

  if ((void *)in == (void *)out)
	  offload_type = 0;

  if (offload_type) {
    p = (fft_plan_t *)_lib_extern_malloc(FFT_PLAN_T_SIZE);
	N = (N1 > N2) ? N1 : N2;
		
    /* determine the rad for the first  & second dimensions */
    calculate_rad_r2c (1, N2, &p->u.dp_2d_r2c_e.para1);

    calculate_rad_r2c (2, N1, &p->u.dp_2d_r2c_e.para2);

    /* Calculate mem size for 2 dimensions for ECPY */
    /* evaluate the  1st dimension */

    calculate_mem_size_r2c (1, N2, &p->u.dp_2d_r2c_e.para1, &twsize, &Bnsize, &worksize);
    p->u.dp_2d_r2c_e.para1.twsize = twsize;
    p->u.dp_2d_r2c_e.para1.Bnsize = Bnsize;
	int_tw_size = ext_tw_size = twsize;
    int_Bn_size = ext_Bn_size = Bnsize;
    work_size   = worksize;

    /* evaluate the 2nd dimension */

    calculate_mem_size_r2c (2, N1, &p->u.dp_2d_r2c_e.para2, &twsize, &Bnsize, &worksize);
    p->u.dp_2d_r2c_e.para2.twsize = twsize;
    p->u.dp_2d_r2c_e.para2.Bnsize = Bnsize;
    int_tw_size  = (int_tw_size > twsize) ? int_tw_size : twsize;
    ext_tw_size += twsize;
    int_Bn_size  = (int_Bn_size > Bnsize) ? int_Bn_size : Bnsize;
    ext_Bn_size += Bnsize;
    work_size    = (work_size > worksize) ? work_size : worksize;

	p->u.dp_2d_r2c_e.para1.worksize = work_size;
    p->u.dp_2d_r2c_e.para2.worksize = work_size;

    /* calculate local memory requirements */
    localsize = FFT_EDMA_CH_BUFSIZE+sizeof(double)*10*N*FFT_OMP_DP_2D_R2C_NUMOFLINEBUFS + sizeof(double)*(int_tw_size + int_Bn_size + work_size);

    return (_ti_fftw_plan_offload_2d(PLAN_TYPE, offload_type, N1, N2, sizeof(double)*N1*N2, sizeof(fftw_complex)*N1*(N2/2+1), sizeof(double)*(ext_tw_size+ext_Bn_size)+sizeof(fftw_complex)*N1*N2, localsize, in, out, sign, flags, (void *)p));
  }
  else
    {
      TI_FFTW_DEBUG_PRINT("  Using FFTW3 for %s\n", _ti_fftw_ocl_plans[PLAN_TYPE].fftw_plan_name);
      fftw_plan rc;
      TI_FFTW_PROFILE_START();
#ifdef TI_FFTW_USE_DLSYM
      assert(_fptr_fftw_plan_dft_r2c_2d);
      rc = _fptr_fftw_plan_dft_r2c_2d(N1, N2, in, out, flags);
#else
      rc = __real_fftw_plan_dft_r2c_2d(N1, N2, in, out, flags);
#endif
      TI_FFTW_PROFILE_REPORT("  fftw_plan_dft_r2c_2d (ARM) took %8.2f us\n",(float) clock_diff);
      return rc;
    } 
}
/* end of plan 2d dp r2c */

/* plan for 2d dp c2r */

fftw_plan fftw_plan_dft_c2r_2d (int N1, int N2, fftw_complex *in, double *out, unsigned flags)
{
  const int PLAN_TYPE = TI_FFTW_FFT_2D_DP_C2R;
  fft_plan_t *p;
  int    N;
  int    int_tw_size, ext_tw_size, twsize, localsize;
  int    worksize, work_size;
  int    int_Bn_size, ext_Bn_size, Bnsize;
  int    sign = FFTW_BACKWARD;  // only IFFT in this case

  if (!_ti_fftw_init_done) _ti_fftw_acc_init();
  TI_FFTW_DEBUG_PRINT("%s(%d,%d,0x%p,0x%p,%d,%d,%d) intercepted\n", _ti_fftw_ocl_plans[PLAN_TYPE].fftw_plan_name, N1, N2, (void *)in, (void *)out, sign, flags, PLAN_TYPE);

  int offload_type = _ti_fftw_should_offload_2d_dp(PLAN_TYPE, N1, N2, sign, flags);

  if ((void *)in == (void *)out)
	  offload_type = 0;

  if (offload_type) {
    p = (fft_plan_t *)_lib_extern_malloc(FFT_PLAN_T_SIZE);
	N = (N1 > N2) ? N1 : N2;
    /* determine the rad for the first  & second dimensions */


    /* determine the rad for the first  & second dimensions */
    calculate_rad_c2r (1, N2, &p->u.dp_2d_c2r_e.para1);

    calculate_rad_c2r (2, N1, &p->u.dp_2d_c2r_e.para2);
	
	/* Calculate mem size for 2 dimensions for ECPY */
    /* evaluate the  1st dimension */

    calculate_mem_size_c2r (1, N2, &p->u.dp_2d_c2r_e.para1, &twsize, &Bnsize, &worksize);
    p->u.dp_2d_c2r_e.para1.twsize = twsize;
    p->u.dp_2d_c2r_e.para1.Bnsize = Bnsize;
    int_tw_size = ext_tw_size = twsize;
    int_Bn_size = ext_Bn_size = Bnsize;
    work_size   = worksize;

    /* evaluate the 2nd dimension */

    calculate_mem_size_c2r (2, N1, &p->u.dp_2d_c2r_e.para2, &twsize, &Bnsize, &worksize);
    p->u.dp_2d_c2r_e.para2.twsize = twsize;
    p->u.dp_2d_c2r_e.para2.Bnsize = Bnsize;
    int_tw_size  = (int_tw_size > twsize) ? int_tw_size : twsize;
    ext_tw_size += twsize;
    int_Bn_size  = (int_Bn_size > Bnsize) ? int_Bn_size : Bnsize;
    ext_Bn_size += Bnsize;
    work_size    = (work_size > worksize) ? work_size : worksize;

	p->u.dp_2d_c2r_e.para1.worksize = work_size;
    p->u.dp_2d_c2r_e.para2.worksize = work_size;

    /* calculate local memory requirements */
    localsize = FFT_EDMA_CH_BUFSIZE+sizeof(double)*10*N*IFFT_OMP_DP_2D_C2R_NUMOFLINEBUFS + sizeof(double)*(int_tw_size + int_Bn_size + work_size);
		
    
    return (_ti_fftw_plan_offload_2d(PLAN_TYPE, offload_type, N1, N2, sizeof(fftw_complex)*(N1*(N2/2+1)), sizeof(double)*N1*N2, sizeof(double)*(ext_tw_size+ext_Bn_size)+sizeof(fftw_complex)*N1*N2, localsize, in, out, sign, flags, (void *)p));
  }
  else
    {
      TI_FFTW_DEBUG_PRINT("  Using FFTW3 for %s\n", _ti_fftw_ocl_plans[PLAN_TYPE].fftw_plan_name);
      fftw_plan rc;
      TI_FFTW_PROFILE_START();
#ifdef TI_FFTW_USE_DLSYM
      assert(_fptr_fftw_plan_dft_c2r_2d);
      rc = _fptr_fftw_plan_dft_c2r_2d(N1, N2, in, out, flags);
#else
      rc = __real_fftw_plan_dft_c2r_2d(N1, N2, in, out, flags);
#endif
      TI_FFTW_PROFILE_REPORT("  fftw_plan_dft_c2r_2d (ARM) took %8.2f us\n",(float) clock_diff);
      return rc;
    } 
}
/* end of plan 2d dp c2r */

/* end of fftw_acc_plan_2d.c */


