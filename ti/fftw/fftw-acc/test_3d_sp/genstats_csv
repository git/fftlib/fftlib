#!/bin/bash -e

SIZES=${*:-"64 128 192 256"}

NUM_TESTS_PER_SIZE=10
EXE="fft_static"

case $0 in
  *csv) sed_script='s/^[[:space:]]\+//; s/[[:space:]]\+/,/g' ;;
  *) sed_script='s/xyz//';;
esac

function run_sizes
{
  printf "%20s\t %12s\t %12s\t %12s\t %12s\t %12s\t %12s\t %12s\t %12s\n" "Name" "Type" "Size" "MeanGFlop" "MinGFlop" "MaxGFlop" "MeanMs" "MinMs" "MaxMs"
  for size in $SIZES; do
    printf "%20s\t %12d\t %12s\t " "fft_3d_sp" $3 $size
    ./$EXE 0 $1 $2 $NUM_TESTS_PER_SIZE $size 2>&1 | grep flops | awk 'NR % 2 == 1' | awk '{print $8}' | sort -n | awk 'BEGIN{c=0;sum=0;} /^[^#]/{a[c++]=$1;sum+=$1;} END{ave=sum/c; if((c%2)==1){median=a[int(c/2)];} else{median=(a[c/2]+a[c/2-1])/2;} printf "%12.6f\t %12.6f\t %12.6f\t ", ave,a[0],a[c-1]}'
    rm -f /tmp/opencl*
    ./$EXE 0 $1 $2 $NUM_TESTS_PER_SIZE $size 2>&1 | grep ms | awk 'NR % 2 == 1' | awk '{print $8}' | sort -n | awk 'BEGIN{c=0;sum=0;} /^[^#]/{a[c++]=$1;sum+=$1;} END{ave=sum/c; if((c%2)==1){median=a[int(c/2)];} else{median=(a[c/2]+a[c/2-1])/2;} printf "%12.6f\t %12.6f\t %12.6f\n", ave,a[0],a[c-1]}'
    rm -f /tmp/opencl*
  done
}

function run_sizes_i
{
  printf "%20s\t %12s\t %12s\t %12s\t %12s\t %12s\t %12s\t %12s\t %12s\n" "Name" "Type" "Size" "MeanGFlop" "MinGFlop" "MaxGFlop" "MeanMs" "MinMs" "MaxMs"
  for size in $SIZES; do
    printf "%20s\t %12d\t %12s\t " "ifft_3d_sp" $3 $size
    ./$EXE 0 $1 $2 $NUM_TESTS_PER_SIZE $size 2>&1 | grep flops | awk 'NR % 2 == 1' | awk '{print $8}' | sort -n | awk 'BEGIN{c=0;sum=0;} /^[^#]/{a[c++]=$1;sum+=$1;} END{ave=sum/c; if((c%2)==1){median=a[int(c/2)];} else{median=(a[c/2]+a[c/2-1])/2;} printf "%12.6f\t %12.6f\t %12.6f\t ", ave,a[0],a[c-1]}'
    rm -f /tmp/opencl*
    ./$EXE 0 $1 $2 $NUM_TESTS_PER_SIZE $size 2>&1 | grep ms | awk 'NR % 2 == 1' | awk '{print $8}' | sort -n | awk 'BEGIN{c=0;sum=0;} /^[^#]/{a[c++]=$1;sum+=$1;} END{ave=sum/c; if((c%2)==1){median=a[int(c/2)];} else{median=(a[c/2]+a[c/2-1])/2;} printf "%12.6f\t %12.6f\t %12.6f\n", ave,a[0],a[c-1]}'
    rm -f /tmp/opencl*
  done
}

test -x ./$EXE || make $EXE

{
  export TI_FFTWF_OFFLOAD=2 #
  echo; run_sizes 0 1 1
  echo; run_sizes 1 1 2

  export TI_FFTWF_OFFLOAD=1 # to run DSP tests
  echo; run_sizes 0 1 3
  echo; run_sizes 1 1 4

  export TI_FFTWF_OFFLOAD=0 # to run ARM tests
  echo; run_sizes 0 1 5
  echo; run_sizes 1 1 6
} | sed -e "$sed_script"

{
  export TI_FFTWF_OFFLOAD=2 #
  echo; run_sizes_i 0 -1 1
  echo; run_sizes_i 1 -1 2

  export TI_FFTWF_OFFLOAD=1 # to run DSP tests
  echo; run_sizes_i 0 -1 3
  echo; run_sizes_i 1 -1 4

  export TI_FFTWF_OFFLOAD=0 # to run ARM tests
  echo; run_sizes_i 0 -1 5
  echo; run_sizes_i 1 -1 6
} | sed -e "$sed_script"

