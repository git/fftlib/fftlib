/* ======================================================================= */
/*  TEXAS INSTRUMENTS, INC.                                                */
/*                                                                         */
/*  FFTLIB  FFT Library                                                    */
/*                                                                         */
/* Copyright (C) 2014 Texas Instruments Incorporated - http://www.ti.com/  */
/*                                                                         */
/*                                                                         */
/*  Redistribution and use in source and binary forms, with or without     */
/*  modification, are permitted provided that the following conditions     */
/*  are met:                                                               */
/*                                                                         */
/*    Redistributions of source code must retain the above copyright       */
/*    notice, this list of conditions and the following disclaimer.        */
/*                                                                         */
/*    Redistributions in binary form must reproduce the above copyright    */
/*    notice, this list of conditions and the following disclaimer in the  */
/*    documentation and/or other materials provided with the               */
/*    distribution.                                                        */
/*                                                                         */
/*    Neither the name of Texas Instruments Incorporated nor the names of  */
/*    its contributors may be used to endorse or promote products derived  */
/*    from this software without specific prior written permission.        */
/*                                                                         */
/*  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS    */
/*  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT      */
/*  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR  */
/*  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT   */
/*  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,  */
/*  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT       */
/*  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,  */
/*  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY  */
/*  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT    */
/*  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE  */
/*  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.   */
/*                                                                         */
/* ======================================================================= */
#include <stdlib.h>
#include <stdio.h>
#include <time.h>
#include <assert.h>
#include <memory.h>
#include <math.h>
#include "fftw3.h"

#define FFT_STATS          1
#define IFFT_STATS        -1
static const int N=128;
static const double MAX_DELTA = 1e-4;
static double get_delta_sum(int n1, int n2, float* final, float* ref, int scale, int verbose);

static int num_input_modified_failures = 0;

FILE *fout;
#define tick()  clock_gettime(CLOCK_MONOTONIC, &t0);
#define tock() (clock_gettime(CLOCK_MONOTONIC, &t1), \
                        t1.tv_sec - t0.tv_sec + (t1.tv_nsec - t0.tv_nsec) / 1e9)

static void report_flops(const char* str, double secs, int fftsize)
{
  fprintf(fout,"Time for %s: %8d, %6.6f ms\n",
          str, fftsize, secs*1000);
  fprintf(fout,"Performance for %s: %8d, %6.3f Gflops\n",
          str, fftsize, 2.5 * fftsize * log2(fftsize) / (secs * 1e9));
}

static int do_fft(int n1, int n2, int in_place, int verbose, int stats_type) {
    int i, j;

    fftwf_complex *out;
    float *in, *in2, *in_golden;
    fftwf_plan plan_backward;
    fftwf_plan plan_forward;

    if (in_place) {
    in = (float *) fftwf_malloc(sizeof(fftwf_complex) * n1 * (n2/2+1));
      out = (fftwf_complex *) in;
    in_golden = (float*) malloc(sizeof(fftwf_complex) * n1 * (n2/2+1));
    }
    else {
    in = (float *) fftwf_malloc(sizeof(float) * n1 * n2); 
      assert(in);
      out = (fftwf_complex*) fftwf_malloc(sizeof(fftwf_complex) * n1 * (n2/2+1));
      in_golden = (float*) malloc(sizeof(float) * n1 * n2);
    }
   
    assert(in_golden);
    assert(in);
    assert(out);

    plan_forward = fftwf_plan_dft_r2c_2d(n1, n2, in, out, FFTW_ESTIMATE);

    memset(out, 0, sizeof(fftwf_complex) * n1 * (n2/2+1));
    memset(in, 0, sizeof(float) * n1 * (n2));

    if (in_place){
      for (j = 0; j < n1; j++){
          for (i = 0; i < n2; i++ ) {
            in[i+j*(n2+2)] = (double)rand() / RAND_MAX;
          }/* extra memory space for in_place transform only  */
          in[(j+1)*(n2+2)-1] = in[(j+1)*(n2+2)-2] = 0;
      }
    }
    else{
        for (i = 0; i < n1*n2; i++ ) {
          in[i] = (double)rand() / RAND_MAX;
        }
    }
  
    /* Call to fftwf_execute modifies input buffer, save it off */
    if(in_place)
      memcpy(in_golden, in, sizeof(fftwf_complex) * n1 * (n2/2+1));
    else
      memcpy(in_golden, in, sizeof(float) * n1 * n2);

    if (verbose > 1) {
        fprintf (fout,  "Input Data:\n" );
       for ( i = 0; i < n1*n2; i++ ) {
          fprintf (fout,  "  %3d  %12f\n", i, in[i]);
       }
    } 

    if (verbose) fprintf(fout, "\nNow doing a forward FFT..\n");
    struct timespec t0, t1;
    tick();
    fftwf_execute(plan_forward); /* repeat as needed */
    double secs = tock();
    if (stats_type == FFT_STATS)
      report_flops("fftwf_execute (ARM and DSP)", secs, n1*n2); // need to adjust
  
    if (verbose > 1) {
       fprintf (fout,  "Output FFT Coefficients:\n" );
      for ( i = 0; i < n1*n2; i++ ) {
         fprintf (fout,  "  %3d  %e  %e\n", i, out[i][0], out[i][1] );
      }
    }

    if(in_place)
      in2 = in;
    else /* this buffer has to be the same size as the in buffer to ifft for the outplace senario */
      in2 = (float *)fftwf_malloc ( sizeof ( fftwf_complex ) *n1*n2); 

    assert(in2);

    if (verbose) fprintf(fout, "\nNow doing a backward FFT on the output buffer\n");
    plan_backward = fftwf_plan_dft_c2r_2d ( n1, n2, out, in2, FFTW_ESTIMATE );

    tick();
    fftwf_execute ( plan_backward );
    secs = tock();
    if (stats_type == IFFT_STATS)
      report_flops("fftwf_execute (ARM and DSP)", secs, n1*n2);


    if (verbose) fprintf(fout, "\nNow checking if the FWD+BKWD gives the original data\n");
    if (verbose > 1) fprintf (fout,  "\nIN2(REF):\n" );
  

    double err;
    if(in_place){
      /* zero the points between the real spectrums */
      for(j = 0; j < n1; j++){
        in2[(j+1)*(n2+2) - 1] = in2[(j+1)*(n2+2)-2] = 0;
      }
      err = get_delta_sum(n1, n2, in2, in_golden, 1, verbose);
    }
    else
      err = get_delta_sum(n1, n2, in2, in_golden, 1, verbose);
    double accept_err = MAX_DELTA * n1 * n2;

    // now check if original array was modified
    double input_modified = 0.0;

    if (!in_place) {
      if (verbose) fprintf(fout, "\nNow checking if the INPUT buffer was modified\n");
      if (verbose > 1) fprintf (fout,  "\nIN(REF):\n" );
      input_modified = get_delta_sum(n1, n2, in, in_golden, 0, verbose);
    }

    fftwf_destroy_plan(plan_forward);
    fftwf_destroy_plan(plan_backward);

    fftwf_free(in); 
    if (!in_place){
    fftwf_free(out);
    fftwf_free(in2);
  }
    free(in_golden);

    int rc = 0;
    if (err > accept_err) {
        rc = 1;
        fprintf(fout, "TEST FAILED (cumul err=%12g, accept err=%12g)\n", err, accept_err);
    }
    if (input_modified > 0.0) {
        num_input_modified_failures++;
        fprintf(fout, "FFTW API VIOLATED: IN buffer modified (cumul err=%12g, accept err=0.0)\n", input_modified);
    }
    return (rc);
}

static double get_delta_sum(int n1, int n2, float* final, float* ref, int scale,  int verbose) {
    double cumul_err = 0.0;
    int i, k;

    for ( k = 0; k < n1; k++ )
    for ( i = 0; i < n2; i++ ) {
        double x = final[k*n2+i];
        if (scale) {
          x = x/(double)(n1*n2);
        }
        double delta_x = fabs(x - ref[k*n2+i]);
        cumul_err += delta_x;

        if (verbose > 1) 
        {
            fprintf (fout,  "  %3d  %12f (%f)\n", i, x, ref[k*n2+i]);
        }
    }
    if (verbose) fprintf(fout, "  cumul err: %12g\n", cumul_err);
    return cumul_err;
}

static int do_tests(int num_tests, int n1, int n2, int in_place, int verbose, int stat_type) {
  const char* type = (in_place? "IN-PLACE":"OUT-OF-PLACE");
  fprintf(fout, "--------------------------------------------\n");
  fprintf(fout, "DOING %d TESTS %s with FFT size of %d by %d\n", num_tests, type, n1, n2);
  int i;
  srand (123456789);
  int num_failed = 0;
  for (i=0; i<num_tests; i++) {
    fprintf(fout, "\nRunning test %d\n", i+1);
    int rc = do_fft(n1, n2, in_place, verbose, stat_type);
    if (rc > 0) fprintf(fout, "*** Test %d FAILED ***\n", i+1);
    else fprintf(fout, "Test %d PASSED\n", i+1);
    num_failed += rc;
  }
  if (num_failed == 0) fprintf(fout, "\nAll %d FFT tests PASSED (SIZE=%d by %d)\n", num_tests, n1, n2);
  else fprintf(fout, "\n**** %d/%d FFT TESTS FAILED (SIZE=%d by %d) ****\n", num_failed, num_tests, n1, n2);
  fprintf(fout, "--------------------------------------------\n");
  return num_failed;
}

void print_sizes_tested(int argc, char **argv) {
  fprintf(fout, "Sizes tested: ");
  if (argc > 4) {
    int i;
    for (i=4; i<argc; i++)
      fprintf(fout, "%s ", argv[i]);
  }
  else {
    fprintf(fout, "%d ", N);
  }
  fprintf(fout, "\n");
}


int main(int argc, char **argv) {
  int verbose = 0;
  int n1 = N, n2 = N;
  fout = stdout;
  if (argc > 1) {
    verbose = atoi(argv[1]);
  }

  int in_place = 1; /* by default do an IN-PLACE as its faster */
  if (argc > 2) {
    in_place = atoi(argv[2]);
  }
  
  int test_stat_type = FFT_STATS; /* by default collect FFT stats */
  if (argc > 3) {
    test_stat_type = atoi(argv[3]);
  }

  int num_tests = 3;
  if (argc > 4) {
    num_tests = atoi(argv[4]);
  }

  int tot_failed = 0;
  int tot_tests = 0;
  if (argc > 5) {
    int i;
    for (i=5; i<argc; i+=2) {
      n1 = atoi(argv[i]);
      if((i+2) > argc)
        n2 = n1;
      else
        n2 = atoi(argv[i+1]);
      tot_tests += num_tests;
      tot_failed += do_tests(num_tests, n1, n2, in_place, verbose, test_stat_type);
    } 
  }
  else {
    tot_failed = do_tests(num_tests, N, N, in_place, verbose, test_stat_type);
    tot_tests += num_tests;
  }
  static const char* PLACE;
  PLACE = (in_place?"IN-PLACE":"OUT-OF-PLACE");
  fprintf(fout, "SUMMARY\n");
  fprintf(fout, "=======\n");
  fprintf(fout, "%s Tests Run: %d\n", PLACE, tot_tests);
  fprintf(fout, "%s Tests Failed: %d\n", PLACE, tot_failed);
  if (num_input_modified_failures)
    fprintf(fout, "Warning: Input buffer modified in %d %s transforms\n", num_input_modified_failures, PLACE);
  print_sizes_tested(argc, argv);
  return (tot_failed);
}

