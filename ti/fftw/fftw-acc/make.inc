# Prefix for installation. aka /usr
FFTW_PREFIX?=$(CURDIR)/../install/fftw

# Where the DSP sources are
TI_INSTALL_DIR?=/usr/src/dsp
TI_OCL_CGT_INSTALL?=/usr

# Debugging
TI_FFTW_DEBUG=0

# anysize support for 2D and 3D FFT
TI_FFTW_ANYSIZE_SUPPORT=0

# set to 1 to time offload, etc
TI_FFTW_PROFILE=1

# At least one of the following defines must be set to 1
# If both then both are compiled in. Note, DIRECT code
# is no longer maintained, and is here only to help with
# with debugging. It is recommended you sent, TI_FFTW_ALLOW_DIRECT
# to 0.
TI_FFTW_ALLOW_ECPY=1
TI_FFTW_ALLOW_DIRECT=0

# set to 1 if you want to load the kernels from disk
TI_FFTW_NO_FAT_BINARY=0

#
#
#

#*
#*
#* Copyright (C) 2013 Texas Instruments Incorporated - http://www.ti.com/ 
#* 
#* 
#*  Redistribution and use in source and binary forms, with or without 
#*  modification, are permitted provided that the following conditions 
#*  are met:
#*
#*    Redistributions of source code must retain the above copyright 
#*    notice, this list of conditions and the following disclaimer.
#*
#*    Redistributions in binary form must reproduce the above copyright
#*    notice, this list of conditions and the following disclaimer in the 
#*    documentation and/or other materials provided with the   
#*    distribution.
#*
#*    Neither the name of Texas Instruments Incorporated nor the names of
#*    its contributors may be used to endorse or promote products derived
#*    from this software without specific prior written permission.
#*
#* 
#* 
#*  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS 
#*  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT 
#*  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
#*  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT 
#*  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, 
#*  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT 
#*  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
#*  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
#*  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT 
#*  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE 
#*  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
#*

UNAME_M :=$(shell uname -m)

ifneq (,$(findstring 86, $(UNAME_M)))
  ifneq ($(MAKECMDGOALS),clean)
    ifeq ($(LINUX_DEVKIT_ROOT),)
      $(error ERROR - LINUX_DEVKIT_ROOT is required to be set path of the Arago Linux Devkit)
    endif
    ifeq ($(TARGET_ROOTDIR),)
      $(error ERROR - TARGET_ROOTDIR is required to be set to the path of EVM root NFS filesystem)
    endif
    ifeq ($(shell which clocl),)
      $(warning WARNING - Could not find clocl on the PATH!)

      CLOCL_PATH?=$(wildcard $(TARGET_ROOTDIR)/usr/share/ti/opencl/bin/x86/clocl)
      ifeq ($(CLOCL_PATH),)
        $(warning WARNING - Could not find clocl in default location on EVM root filesystem.)
        $(error ERROR - Please install OpenCL or add clocl to the PATH!)
      endif
      PATH:=$(PATH):$(patsubst %/clocl,%,$(CLOCL_PATH))
    endif
  endif
endif

ifeq (,$(wildcard $(TI_OCL_CGT_INSTALL)/bin/cl6x))
  $(error ERROR - TI_OCL_CGT_INSTALL is not defined! Please set to path of C6000 Code Gen Tools)
endif

PATH:=$(TI_OCL_CGT_INSTALL)/include/arm-linux-gnueabihf:$(PATH)

define FIND_DSP_PKG
  export $(1)?=$$(patsubst %/$(3),%,$$(lastword $$(sort $$(wildcard $$(TI_INSTALL_DIR)/$(2)/$(3)))))
  ifeq ($$($(1)),)
    $$(error ERROR - $(1) is not defined and could not be found in $(TI_INSTALL_DIR)/ )
  else
    ifeq ($$(wildcard $$($(1))/$(3)),)
      $$(error ERROR - "$(1) = $$($(1))" Is not valid!)
    endif
  endif
  $$(info Using $(1) = $$($(1)))
endef

ifneq (,$(findstring 86, $(UNAME_M)))
    override CCP = arm-linux-gnueabihf-gcc
    override CC = arm-linux-gnueabihf-gcc
    override AR = arm-linux-gnueabihf-ar
    override OBJCOPY = arm-linux-gnueabihf-objcopy
    override CCP_FLAGS += -I$(TARGET_ROOTDIR)/usr/include -I$(XDCTOOLS_DIR) -idirafter /usr/include
    override LD_FLAGS = -L$(TARGET_ROOTDIR)/lib -L$(TARGET_ROOTDIR)/usr/lib -Wl,-rpath-link,$(TARGET_ROOTDIR)/lib -Wl,-rpath-link,$(TARGET_ROOTDIR)/usr/lib 
endif

export UNAME_M CPP OBJCOPY CPP_FLAGS LD_FLAGS

